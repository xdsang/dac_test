
// MTView.cpp : CMTView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MT.h"
#endif

#include "MTDoc.h"
#include "MTView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMTView

IMPLEMENT_DYNCREATE(CMTView, CFormView)

BEGIN_MESSAGE_MAP(CMTView, CFormView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CMTView 构造/析构

CMTView::CMTView()
	: CFormView(IDD_MT_FORM)
{
	// TODO: 在此处添加构造代码

}

CMTView::~CMTView()
{
}

void CMTView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CMTView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CFormView::PreCreateWindow(cs);
}

void CMTView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

}

void CMTView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMTView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMTView 诊断

#ifdef _DEBUG
void CMTView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMTView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMTDoc* CMTView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMTDoc)));
	return (CMTDoc*)m_pDocument;
}
#endif //_DEBUG


// CMTView 消息处理程序
