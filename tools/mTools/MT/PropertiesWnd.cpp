
#include "stdafx.h"
#include <string>
#include <regex>
#include <vector>

#include "PropertiesWnd.h"
#include "Resource.h"
#include "MainFrm.h"
#include "MT.h"
#include "MSLnkU.h"
#include "MSLinkSerial.h"
#include "driverMpi.h"
using std::string;
using std::regex;
using std::smatch;
using std::vector;
// #using <System.dll>
// using namespace System;
// using namespace System::IO::Ports;
// using namespace System::ComponentModel;

extern CMSLinkU *puLnk;
extern CMSLinkSerial spLnk;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const CString LabelFontName = _T("Courier New");
const int LabelFontSize = 18;
// extern CMSLinkU *puLnk;
#define ID_IOTYPE 30
#define ID_ADDRESS 31
#define ID_SUBADDRESS 32
#define ID_VALUE 33
#define ID_BTN_READ 34
#define ID_BTN_WRITE 35
#define ID_BTN_CONNECT 36
#define ID_STATIC_STATUS 37

#define ID_BIT0 40
#define ID_BIT1 41
#define ID_BIT2 42
#define ID_BIT3 43
#define ID_BIT4 44
#define ID_BIT5 45
#define ID_BIT6 46
#define ID_BIT7 47
#define ID_BIT_ALL 48

#define ID_LABEL_REGADDR 50
#define ID_COMB_REGADDR 51
#define ID_LABEL_REGVALUE 52
#define ID_COMB_REGVALUE 53

#define ID_LABEL_LOG_RW 54
#define ID_LOG_R 55
#define ID_LOG_W 56

#define ID_BTN_CLEAR_LOG 57
#define ID_BTN_SAVE_LOG 58

// fill data define

const LPCTSTR connectArray[] = {
  _T("USB I2C"),
  _T("USB HID"),
  _T("MCU I2C"),
  _T("Virtual"),
};

const int connectTypeArray[] = {
  MSLNK_USB2I2C,
  MSLNK_USBHID,
  MSLNK_SERIAL,
  MSLNK_VIRTUAL,
};
const int INDEX_CONNECT_TYPE = 0;

const LPCTSTR I2C_ModeArray[] = {
  _T("16 Bits"),
  _T("8 Bits"),
};
enum  {
  INDEX_16_MODE = 0,
  INDEX_8_MODE = 1
};
const int DEFI_I2C_MODE = INDEX_16_MODE;
  
const BYTE I2C_AddrArray[] = {
  MS_CHIP_ADDR1,
  MS_CHIP_ADDR2
};
const int DEFI_I2C_ADDR = 0;
const LPCTSTR I2C_SpeedArray[] = {
  _T("20k"),
  _T("100k"),
  _T("400k"),
  _T("750k"),
};
const int I2C_SPEED_NUM = sizeof(I2C_SpeedArray) / sizeof(I2C_SpeedArray[0]);
const I2C_SPEED DEFI_I2C_SPEED = MS_USBIIC_SPEED_400K;
const int baudRateArray[] = {
  2400,
  4800,
  9600,
  14400,
  19200,
  28800,
  38400,
  57600,
  115200,
};
const int DEFI_BANDRATE = 2;
// fill data define end


typedef struct {
  E_CONFIG_TYPE type;
  LPCTSTR sectName;
  LPCTSTR keyName;
}STRT_CONFIG_MAP;

// note use E_CONFIG_TYPE nature sequence
const STRT_CONFIG_MAP CONFIGMAP[]= {
  {CFG_CONNECT_MODE, _T("SYS"), _T("CONNECT_MODE")},
  {CFG_I2C_MODE, _T("I2C"), _T("MODE")},
  {CFG_I2C_SPEED, _T("I2C"), _T("SPEED")},
};

bool isConnectSucc = false;
CMFCPropertyGridProperty* g_pUartPortNameProp;
CMFCPropertyGridProperty* g_pUartBaudRate;
CMFCPropertyGridProperty* g_pI2cAddrProp;
CMFCPropertyGridProperty* g_pI2cSpeedProp;
CMFCPropertyGridProperty* g_pI2cModeProp;
CIOCtrl* g_pI2cCtrl;
bool gFlagLogR = false;
bool gFlagLogW = false;

// funtion declare
CString toHexCString(int val);
int hexToInt(CString str);
string toString(CString cs);
CString toCString(string str);
bool justHasHexChar(CString cstr);

class CIOCtrl
{
public:
  CIOCtrl();
  void setAddr(BYTE addr) {
    m_addr = addr;
    puLnk->SetI2CAddr(m_addr);
  }
  BYTE getAddr() {return m_addr;}
  void setSpeed(I2C_SPEED type) {
    m_speedType = type;
    puLnk->SetIICSpeed(m_speedType);
  }
  I2C_SPEED getSpeedType() {return m_speedType;}
  void set16BitMode(bool on) { m_16bit = on;}
  bool is16BitMode() {return m_16bit;}
  void write(UINT16 regAddr, BYTE value);
  BYTE read(UINT16 regAddr);
  // serial port
  void setSerialPort(BYTE port);
  BYTE getCurSerialPort();
  int getCurBaudRate();
  void setBaudRate(int rate);
  vector<CString>* getSerialPortNames();

private:
  void initConfig();
  BYTE m_addr;
  I2C_SPEED m_speedType;
  bool m_16bit;
  BYTE m_serPort;
  int m_baudRate;
  bool m_spIsOpen;
  vector<CString> m_spNameVec;
};

CIOCtrl::CIOCtrl()
{
  m_addr = I2C_AddrArray[DEFI_I2C_ADDR];
  m_speedType = DEFI_I2C_SPEED;       // i2c speed mode
  m_16bit = true;
  initConfig();
}

void CIOCtrl::setSerialPort(BYTE port)
{
  m_serPort = port;
  puLnk->setSerialPort(port);
}
BYTE CIOCtrl::getCurSerialPort()
{
  return m_serPort;
}
void CIOCtrl::setBaudRate(int rate)
{
  m_baudRate = rate;
  puLnk->setBaudRate(rate);
}

int CIOCtrl::getCurBaudRate()
{
  return m_baudRate;
}  

vector<CString>* CIOCtrl::getSerialPortNames()
{
  m_spNameVec.clear();
  for (int i = 0; i < 20; ++i) {
    if (spLnk.isExistPort(i)) {
      CString portName;
      portName.Format(_T("COM%d"), i);
      m_spNameVec.push_back(portName);
    }
  }
  return &m_spNameVec;
}

void CIOCtrl::initConfig(void)
{
  puLnk->SetI2CAddr(m_addr);
  puLnk->SetIICSpeed(m_speedType);
}


void CIOCtrl::write(UINT16 regAddr, BYTE value)
{
  if (m_16bit) {
    puLnk->i2cw16(m_addr, regAddr, value);
    if (gFlagLogW) {
        CString str;
        str.Format(_T("i2cw16 %02X %04X %02X"), m_addr, regAddr, value);
        Log(str);
    }
  } else {
    puLnk->i2cw(m_addr, regAddr, value);
    if (gFlagLogW) {
        CString str;
        str.Format(_T("i2cw %02X %02X %02X"), m_addr, regAddr, value);
        Log(str);
    }
  }
}

BYTE CIOCtrl::read(UINT16 regAddr)
{
  BYTE value;
  if (m_16bit) {
    puLnk->i2cr16(m_addr, regAddr, &value);  
    if (gFlagLogR) {
        CString str;
        str.Format(_T("i2cr16 %02X %04X <- %02X"), m_addr, regAddr, value);
        Log(str);
    }
  } else {
    puLnk->i2cr(m_addr, (UINT8)regAddr, &value);
    if (gFlagLogR) {
        CString str;
        str.Format(_T("i2cr %02X %02X <- %02X"), m_addr, regAddr, value);
        Log(str);
    }
  }
  return value;
}
/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar

CPropertiesWnd::CPropertiesWnd()
{
  m_nComboHeight = 0;
  m_Value = 0;
  m_RegAddr = 0;
}

CPropertiesWnd::~CPropertiesWnd()
{
  delete m_pI2cCtrl;
}

BEGIN_MESSAGE_MAP(CPropertiesWnd, CDockablePane)
  ON_WM_CREATE()
  ON_WM_SIZE()
  ON_WM_PAINT()
  ON_COMMAND(ID_EXPAND_ALL, OnExpandAllProperties)
  ON_UPDATE_COMMAND_UI(ID_EXPAND_ALL, OnUpdateExpandAllProperties)
  ON_COMMAND(ID_SORTPROPERTIES, OnSortProperties)
  ON_UPDATE_COMMAND_UI(ID_SORTPROPERTIES, OnUpdateSortProperties)
  ON_COMMAND(ID_PROPERTIES1, OnProperties1)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES1, OnUpdateProperties1)
  ON_COMMAND(ID_PROPERTIES2, OnProperties2)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES2, OnUpdateProperties2)
  ON_WM_SETFOCUS()
  ON_WM_SETTINGCHANGE()
  ON_WM_CTLCOLOR()
  
  ON_BN_CLICKED(ID_BTN_CONNECT, OnConnect)
  ON_BN_CLICKED(ID_BTN_READ, OnRead)
  ON_BN_CLICKED(ID_BTN_WRITE, OnWrite)
  ON_BN_CLICKED(ID_BIT0, OnBit0)
  ON_BN_CLICKED(ID_BIT1, OnBit1)
  ON_BN_CLICKED(ID_BIT2, OnBit2)
  ON_BN_CLICKED(ID_BIT3, OnBit3)
  ON_BN_CLICKED(ID_BIT4, OnBit4)
  ON_BN_CLICKED(ID_BIT5, OnBit5)
  ON_BN_CLICKED(ID_BIT6, OnBit6)
  ON_BN_CLICKED(ID_BIT7, OnBit7)
  ON_BN_CLICKED(ID_BIT_ALL, OnBitAll)
  ON_BN_CLICKED(ID_LOG_R, OnLogR)
  ON_BN_CLICKED(ID_LOG_W, OnLogW)
  ON_BN_CLICKED(ID_BTN_CLEAR_LOG, OnClearLog)
  ON_BN_CLICKED(ID_BTN_SAVE_LOG, OnSaveLog)

  ON_UPDATE_COMMAND_UI(ID_BTN_CONNECT,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BTN_READ,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BTN_WRITE,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT0,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT1,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT2,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT3,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT4,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT5,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT6,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT7,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BIT_ALL,OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_LOG_R, OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_LOG_W, OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BTN_CLEAR_LOG, OnUpdateButton)
  ON_UPDATE_COMMAND_UI(ID_BTN_SAVE_LOG, OnUpdateButton)

  ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)
  ON_CBN_EDITCHANGE(ID_COMB_REGADDR, OnRegAddrEditChange)
  ON_CBN_SELCHANGE(ID_COMB_REGADDR, OnSelRegAddrCombo)
  ON_CBN_EDITCHANGE(ID_COMB_REGVALUE, OnRegValEditChange)
  ON_CBN_SELCHANGE(ID_COMB_REGVALUE, OnSelRegValCombo)
  ON_CBN_SELCHANGE(ID_IOTYPE, OnConnectModeChange)
  ON_WM_KEYUP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar 消息处理程序

void CPropertiesWnd::AdjustLayout()
{
  if (GetSafeHwnd () == NULL || (AfxGetMainWnd() != NULL && AfxGetMainWnd()->IsIconic()))
  {
    return;
  }

  CRect rectClient;
  GetClientRect(rectClient);
  int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

  m_cboxConnectMode.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), m_nComboHeight, SWP_NOACTIVATE | SWP_NOZORDER);
  m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top + m_nComboHeight, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
  int top;
  int height;

  top = rectClient.top + m_nComboHeight + cyTlb;
  height = 185;
  m_wndPropList.SetWindowPos(NULL, rectClient.left, top, rectClient.Width(), height, SWP_NOACTIVATE | SWP_NOZORDER);
  m_wndPropList.ExpandAll(m_bExpAllPropList);

  // set register address and value
  top += height +10;
  height = 20;
  m_lRegAddr.ShowWindow(SW_HIDE);
  m_lRegValue.ShowWindow(SW_HIDE);
  m_lRegAddr.SetWindowPos(NULL, rectClient.left, top, (int)(rectClient.Width() *0.67), height, SWP_NOACTIVATE | SWP_NOZORDER);
  m_edRegAddr.SetWindowPos(NULL, (int)(rectClient.Width() *0.5), top, (int)(rectClient.Width() *0.5), height, SWP_NOACTIVATE | SWP_NOZORDER);

  top += height;
  m_lRegValue.SetWindowPos(NULL, rectClient.left, top, (int)(rectClient.Width() *0.67), height, SWP_NOACTIVATE | SWP_NOZORDER);
  m_edRegValue.SetWindowPos(NULL, (int)(rectClient.Width() *0.5), top, (int)(rectClient.Width() *0.5), height, SWP_NOACTIVATE | SWP_NOZORDER);
  
  m_lRegAddr.ShowWindow(SW_SHOW);
  m_lRegValue.ShowWindow(SW_SHOW);
  // set checkbox list
  // top = rectClient.top + m_nComboHeight + cyTlb + 10;
  top += height + 10;
  height = 20;
  int left = rectClient.left + 10;
  int width = 15;
  int gap = 10;
  UINT flag = SWP_NOACTIVATE | SWP_NOZORDER;
  m_btnBit7.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit6.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit5.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit4.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit3.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit2.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit1.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  m_btnBit0.SetWindowPos(NULL, left, top, width, height, flag);
  
  left += width + gap;
  width = rectClient.Width() - left;
  m_btnBitAll.SetWindowPos(NULL, left, top, width, height, flag);
  
  // set static icon

  // set connect read write
  top += height + 15;
  height = 20;
  gap = 10;

  width = 20;
  left = rectClient.left;
  m_staticStatus.SetWindowPos(NULL, left, top, width, height, flag);

  
  width = 60;
  left += 20;
  height = 25;
  m_btnConnect.SetWindowPos(NULL, left, top, width, height, flag);

  left += width + gap;
  m_btnRead.SetWindowPos(NULL, left, top, width, height, flag);

  left += width + gap;
  m_btnWrite.SetWindowPos(NULL, left, top, width, height, flag);

  // set checkbox for log rw

  top += height + 10;
  // width = (int)(rectClient.Width() *0.33);
  width = 60;
  height = 20;
  gap = 10;
  flag = SWP_NOACTIVATE | SWP_NOZORDER;

  left = rectClient.left +20;
  m_lLogRW.ShowWindow(SW_HIDE);
  m_lLogRW.SetWindowPos(NULL, left, top, width, height, flag);
  m_lLogRW.ShowWindow(SW_SHOW);
  
  left += width + gap;
  width = 60;
  m_btnLogR.SetWindowPos(NULL, left, top, width, height, flag);

  left += width + gap;
  m_btnLogW.SetWindowPos(NULL, left, top, width, height, flag);

  //set save/clear log button
  top += 40;
  height = 25;
  width = 70;
  left = rectClient.left + 20;
  m_btnSave.SetWindowPos(NULL, left, top, width, height, flag);
  left += width + gap;
  width = 70;
  m_btnClear.SetWindowPos(NULL, left, top, width, height, flag);
}


int CPropertiesWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    return -1;

  CRect rectDummy;
  rectDummy.SetRectEmpty();

  // 创建组合: 
  const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_BORDER | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

  if (!m_cboxConnectMode.Create(dwViewStyle, rectDummy, this, ID_IOTYPE))
  {
    TRACE0("未能创建属性组合 \n");
    return -1;      // 未能创建
  }

  for (LPCTSTR item : connectArray) {
    m_cboxConnectMode.AddString(item);
  }
  m_cboxConnectMode.SetCurSel(INDEX_CONNECT_TYPE);

  CRect rectCombo;
  m_cboxConnectMode.GetClientRect (&rectCombo);

  m_nComboHeight = rectCombo.Height();

  if (!m_wndPropList.Create(WS_VISIBLE | WS_CHILD, rectDummy, this, 2))
  {
    TRACE0("未能创建属性网格\n");
    return -1;      // 未能创建
  }
  m_bExpAllPropList = TRUE;
  InitPropList();

  m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PROPERTIES);
  m_wndToolBar.LoadToolBar(IDR_PROPERTIES, 0, 0, TRUE /* 已锁定*/);
  m_wndToolBar.CleanUpLockedImages();
  m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_PROPERTIES_HC : IDR_PROPERTIES, 0, 0, TRUE /* 锁定*/);

  m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
  m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
  m_wndToolBar.SetOwner(this);


  // create bit check button list

  DWORD style = WS_VISIBLE | BS_CHECKBOX | WS_CHILD;
  m_btnBit7.Create(_T(""), style, rectDummy, this, ID_BIT7);
  m_btnBit6.Create(_T(""), style, rectDummy, this, ID_BIT6);
  m_btnBit5.Create(_T(""), style, rectDummy, this, ID_BIT5);
  m_btnBit4.Create(_T(""), style, rectDummy, this, ID_BIT4);
  m_btnBit3.Create(_T(""), style, rectDummy, this, ID_BIT3);
  m_btnBit2.Create(_T(""), style, rectDummy, this, ID_BIT2);
  m_btnBit1.Create(_T(""), style, rectDummy, this, ID_BIT1);
  m_btnBit0.Create(_T(""), style, rectDummy, this, ID_BIT0);
  m_btnLogR.Create(_T("Read"), style, rectDummy, this, ID_LOG_R);
  m_btnLogW.Create(_T("Write"), style, rectDummy, this, ID_LOG_W);
  
  // create icon static index connect status
  style = WS_VISIBLE | SS_CENTERIMAGE | SS_ICON;
  m_staticStatus.Create(_T("Status"), style, rectDummy, this, ID_STATIC_STATUS);
  m_staticStatus.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_DISCONNECT)));

  // create button list
  style = WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON;
  m_btnBitAll.Create(_T("ALL"), style, rectDummy, this, ID_BIT_ALL);
  
  m_btnConnect.Create(_T("Connect"), style, rectDummy, this, ID_BTN_CONNECT);
  m_btnRead.Create(_T("read"), style, rectDummy, this, ID_BTN_READ);
  m_btnWrite.Create(_T("write"), style, rectDummy, this, ID_BTN_WRITE);
  m_btnSave.Create(_T("Save Log"), style, rectDummy, this, ID_BTN_SAVE_LOG);
  m_btnClear.Create(_T("Clear Log"), style, rectDummy, this, ID_BTN_CLEAR_LOG);

  // set font
   m_labelFont.CreateFontW(
	  LabelFontSize, 0, 0, 0,
	  FW_NORMAL, FALSE, FALSE, 0,
	  0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
	  DEFAULT_PITCH, LabelFontName);
   

  // create reg addr and value edit
  style = WS_CHILD | WS_VISIBLE;
  m_lRegAddr.Create(_T("Register Address:"), style, rectDummy, this, ID_LABEL_REGADDR);
  m_lRegValue.Create(_T("Register Value:"), style, rectDummy, this, ID_LABEL_REGVALUE);
  m_lLogRW.Create(_T("Log:"), style, rectDummy, this, ID_LABEL_LOG_RW);
  m_lRegAddr.SetFont(&m_labelFont);
  m_lRegValue.SetFont(&m_labelFont);
  m_lLogRW.SetFont(&m_labelFont);

  
  style = WS_CHILD | WS_VISIBLE | CBS_DROPDOWN;
  m_edRegAddr.Create(style, rectDummy, this, ID_COMB_REGADDR);
  m_edRegValue.Create(style, rectDummy, this, ID_COMB_REGVALUE);
  m_edRegAddr.AddString(_T("00"));
  m_edRegValue.AddString(_T("00"));
  m_edRegValue.AddString(_T("FF"));
  m_edRegAddr.SetCurSel(0);
  m_edRegValue.SetCurSel(0);
  m_edRegAddr.LimitText(4);
  m_edRegValue.LimitText(2);

  // 所有命令将通过此控件路由，而不是通过主框架路由: 
  m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

  AdjustLayout();

  // puLnk->SetConnectType(MSLNK_USB2I2C); 
  m_pI2cCtrl = new CIOCtrl();
  g_pI2cCtrl = m_pI2cCtrl;
  loadConfigure();
  m_btnLogR.SetCheck(gFlagLogR);
  m_btnLogW.SetCheck(gFlagLogW);
  return 0;
}

void CPropertiesWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}


void CPropertiesWnd::OnPaint()
{
  CPaintDC dc(this); // device context for painting
  CRect rectClient;
  GetClientRect(rectClient);
  dc.FillSolidRect(rectClient, RGB(255, 255, 255));
}

void CPropertiesWnd::OnExpandAllProperties()
{
  m_bExpAllPropList = !m_bExpAllPropList;
  m_wndPropList.ExpandAll(m_bExpAllPropList);
}

void CPropertiesWnd::OnUpdateExpandAllProperties(CCmdUI* /* pCmdUI */)
{
}

void CPropertiesWnd::OnSortProperties()
{
	m_wndPropList.SetAlphabeticMode(!m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnUpdateSortProperties(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnProperties1()
{
	// TODO: 在此处添加命令处理程序代码
}

void CPropertiesWnd::OnUpdateProperties1(CCmdUI* /*pCmdUI*/)
{
  // TODO: 在此处添加命令更新 UI 处理程序代码
}

void CPropertiesWnd::OnProperties2()
{
	// TODO: 在此处添加命令处理程序代码
  BOOL status =  m_wndPropList.IsDescriptionArea();
  m_wndPropList.EnableDescriptionArea(!status);
}

void CPropertiesWnd::OnUpdateProperties2(CCmdUI* /*pCmdUI*/)
{
  // TODO: 在此处添加命令更新 UI 处理程序代码
}


vector<CString> getSerialPortName(void)
{
  if (g_pI2cCtrl != NULL) {
    return  *(g_pI2cCtrl->getSerialPortNames());
  } else {
    vector<CString> empty;
    return empty;
  }
}

void updateUartPortProperty(void)
{
  vector<CString> ports = getSerialPortName();
  if (!ports.empty()) {
    g_pUartPortNameProp->RemoveAllOptions();
    for (CString item : ports) {
      g_pUartPortNameProp->AddOption(item);
    }
    g_pUartPortNameProp->SetValue(ports[0]);
  } else {
    g_pUartPortNameProp->RemoveAllOptions();
    g_pUartPortNameProp->AddOption(_T("NULL"));
    g_pUartPortNameProp->SetValue(_T("NULL"));
  }  
}
void CMFCPropertyGridPropertyButton::OnClickName( CPoint /*point*/ )
{
  updateUartPortProperty();
  Log(_T("Update Uart done"));
}


void CMFCPropertyGridPropertyButton::OnClickButton(CPoint point)  
{ 
  // updateUartPortProperty();
  CMFCPropertyGridProperty::OnClickButton(point);
}


BOOL CMFCPropertyGridPropertyButton::OnClickValue( UINT uiMsg, CPoint point )
{
  updateUartPortProperty();
  return TRUE;
}
void CPropertiesWnd::InitPropList()
{
  SetPropListFont();

  m_wndPropList.EnableHeaderCtrl(FALSE);
  m_wndPropList.EnableDescriptionArea(FALSE);
  m_wndPropList.SetVSDotNetLook();
  //m_wndPropList.MarkModifiedProperties();

  CMFCPropertyGridProperty* pI2C_group = new CMFCPropertyGridProperty(_T("I2C 配置"), 0, TRUE);
  pI2C_group->AllowEdit(FALSE);
  
  CMFCPropertyGridProperty* pProp;

  CString converStr;
  converStr.Format(_T("%2X"), I2C_AddrArray[DEFI_I2C_ADDR]);
  pProp = new CMFCPropertyGridProperty(
    _T("I2C Address"), converStr, _T("设置I2C总线地址"));
  for (const BYTE item : I2C_AddrArray) {
    converStr.Format(_T("%2X"), item);
    pProp->AddOption(converStr);
  }
  g_pI2cAddrProp = pProp;
  pI2C_group->AddSubItem(pProp);

  pProp = new CMFCPropertyGridProperty(
    _T("I2C Speed"), I2C_SpeedArray[DEFI_I2C_SPEED], _T("设置I2C总线速度"));

  for (LPCTSTR item : I2C_SpeedArray) {
    pProp->AddOption(item);
  }
  g_pI2cSpeedProp = pProp;
  pProp->AllowEdit(FALSE);
  pI2C_group->AddSubItem(pProp);
  
  pProp = new CMFCPropertyGridProperty(
    _T("I2C Mode"), _T(""), _T("I2C 总线模式"));
  for (LPCTSTR item : I2C_ModeArray) {
    pProp->AddOption(item);
  }
  pProp->AllowEdit(FALSE);
  pProp->SetValue(I2C_ModeArray[DEFI_I2C_MODE]);
  pI2C_group->AddSubItem(pProp);
  g_pI2cModeProp = pProp;
  m_wndPropList.AddProperty(pI2C_group);

  CMFCPropertyGridProperty* pUart_group = new CMFCPropertyGridProperty(_T("串口配置"), 0, TRUE);
  pUart_group->SetDescription(_T("点击串口端口标签，刷新串口端口"));
  pUart_group->AllowEdit(FALSE);
  // pProp = new CMFCPropertyGridProperty(    
  pProp = new CMFCPropertyGridPropertyButton(
    _T("串口端口"), _T(""), _T("串口端口选择"));
  g_pUartPortNameProp = pProp;
  updateUartPortProperty();

  pProp->AllowEdit(FALSE);

  pUart_group->AddSubItem(pProp);
  
  converStr.Format(_T("%d"), baudRateArray[DEFI_BANDRATE]);
  pProp = new CMFCPropertyGridProperty(
    _T("波特率"), converStr, _T("波特率选择"));
  g_pUartBaudRate = pProp;
  for (int item : baudRateArray) {
    converStr.Format(_T("%d"), item);
    pProp->AddOption(converStr);
  }
  pProp->AllowEdit(FALSE);
  pUart_group->AddSubItem(pProp);
  m_wndPropList.AddProperty(pUart_group);
}

HBRUSH CPropertiesWnd::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
  HBRUSH hbr = CDockablePane::OnCtlColor(pDC, pWnd, nCtlColor);
  int id = pWnd->GetDlgCtrlID();
  if(id == ID_LABEL_REGVALUE || id == ID_LABEL_REGADDR
     || id == ID_LABEL_LOG_RW)
  {
    pDC->SetBkMode(TRANSPARENT);
    return CreateSolidBrush(RGB(255,255,255));
  }
  // TODO:  如果默认的不是所需画笔，则返回另一个画笔
  return hbr;
}


void CPropertiesWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndPropList.SetFocus();
}

void CPropertiesWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetPropListFont();
}

void CPropertiesWnd::SetPropListFont()
{
	::DeleteObject(m_fntPropList.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntPropList.CreateFontIndirect(&lf);

	m_wndPropList.SetFont(&m_fntPropList);
	m_cboxConnectMode.SetFont(&m_fntPropList);
}


LRESULT CPropertiesWnd::OnPropertyChanged(__in WPARAM wparam, __in LPARAM lparam )
{
  CMFCPropertyGridProperty* pProp = (CMFCPropertyGridProperty*) lparam;
  if (pProp == g_pI2cAddrProp) {    
    CString cStrAddr = pProp->GetValue().bstrVal; // get i2c addr string
    if (justHasHexChar(cStrAddr)) {
      m_pI2cCtrl->setAddr(hexToInt(cStrAddr));
    } else {
      pProp->SetValue(toHexCString(m_pI2cCtrl->getAddr()));
    }
  }
  else if (pProp == g_pI2cSpeedProp) {
    CString cStrSpeed = pProp->GetValue().bstrVal; 
    int i = 0;
    for (i = 0; i < I2C_SPEED_NUM; ++i) {
      if (cStrSpeed == I2C_SpeedArray[i]) {
        break;
      }
    }
    if (i < I2C_SPEED_NUM) {
      m_pI2cCtrl->setSpeed((I2C_SPEED)i);
      saveConfigure(CFG_I2C_SPEED, i);
    } else {
      AfxMessageBox(_T("Err Speed!"));
    }
    
  }
  else if (pProp == g_pI2cModeProp) {
    CString cStrMode = pProp->GetValue().bstrVal; // get mode
    bool is16Mode = (cStrMode == I2C_ModeArray[INDEX_16_MODE]);
    m_pI2cCtrl->set16BitMode(is16Mode);
    saveConfigure(CFG_I2C_MODE,
                  (is16Mode) ? 0 : 1);
  }
  else if (pProp == g_pUartBaudRate) {
    int baudRate = 0;
    CString cStrBaudRate = pProp->GetValue().bstrVal; 
    swscanf_s(cStrBaudRate, _T("%d"), &baudRate);
    m_pI2cCtrl->setBaudRate(baudRate);
  }
  else if (pProp == g_pUartPortNameProp) {
    int port = 0;
    CString portName = pProp->GetValue().bstrVal; 
    swscanf_s(portName, _T("COM%d"), &port);
    m_pI2cCtrl->setSerialPort((BYTE)port);
  }
  return 0;
}


void CPropertiesWnd::OnUpdateButton(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPropertiesWnd::OnBitAll()
{
  m_Value = (m_Value == 0) ? 0xFF : 0x00;
  SetCheckList(m_Value);
  CString str;
  str.Format(_T("%02X"), m_Value);
  m_edRegValue.SetWindowTextW(str);
}

void CPropertiesWnd::SetCheckList(BYTE value)
{
  BYTE mark = 0;
  for (int i = 0; i < 8; ++i) {
    mark = 1 << i;
    m_pbtnArray[i]->SetCheck(value & mark);
  }
}

void CPropertiesWnd::toggleCheckListBit(int index)
{
  if (index > 7) 
    return;
  BYTE mark = 1 << index;
  m_Value ^= mark;
  m_pbtnArray[index]->SetCheck(m_Value & mark);
  CString str;
  str.Format(_T("%02X"), m_Value);
  m_edRegValue.SetWindowTextW(str);
}

void CPropertiesWnd::OnBit7()
{
  toggleCheckListBit(7);
}

void CPropertiesWnd::OnBit6()
{
  toggleCheckListBit(6);
}

void CPropertiesWnd::OnBit5()
{
  toggleCheckListBit(5);
}

void CPropertiesWnd::OnBit4()
{
  toggleCheckListBit(4);
}

void CPropertiesWnd::OnBit3()
{
  toggleCheckListBit(3);
}

void CPropertiesWnd::OnBit2()
{
  toggleCheckListBit(2);
}
void CPropertiesWnd::OnBit1()
{
  toggleCheckListBit(1);
}

void CPropertiesWnd::OnBit0()
{
  toggleCheckListBit(0);
}

#define ID_INDICATOR_LOGO 3
#define SBPS_EX_FIXED	  0x0001
#define SBPS_EX_FLAT	  0x0002

void CPropertiesWnd::OnConnect()
{
  CString str, strConnection, strConnected;
  int sel = 0;
  BYTE connect_type = MSLNK_VIRTUAL;

  sel = m_cboxConnectMode.GetCurSel();

  switch (connectTypeArray[sel])
  {
#if MTOOLS_FOR_INTERNAL

  case MSLNK_SERIAL:
    connect_type = MSLNK_SERIAL;
    strConnection.Format(_T("MCUI2C Connection"));
    break;
  case MSLNK_VIRTUAL:
    connect_type = MSLNK_VIRTUAL;
    strConnection.Format(_T("Virtual Connection"));
    break;

  case MSLNK_USBHID:
      connect_type = MSLNK_USBHID;
      strConnection.Format(_T("USB HID Connection"));
      break;

#endif
  default:
  case MSLNK_USB2I2C:
    connect_type = MSLNK_USB2I2C;
    strConnection.Format(_T("USBI2C Connection"));
    break;
  }

  puLnk->SetConnectType(connect_type);
  isConnectSucc = (puLnk-> Connect () == 1);
  if (isConnectSucc) {
    if (isTargetChip()) {
      m_staticStatus.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_CONNECTED)));
      strConnected.Format(_T("McLink success!"));
      //AfxMessageBox(strConnected, MB_OK | MB_ICONINFORMATION);
	  Log(strConnected);
    } else {
      //AfxMessageBox(_T("You connected isn't ms chip!"));
	  Log(_T("!!!You connected isn't ms chip!!!"));
      strConnected.Format(_T("McLink fail! but I2C path is normal!"));
      m_staticStatus.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_CONNECTED_USB)));
    }
  } else {
      strConnected.Format(_T("!!!can't find USB device!!!"));
      //AfxMessageBox(strConnected);
	  Log(strConnected);
      m_staticStatus.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_DISCONNECT)));
  }
  CMainFrame* pMain = (CMainFrame*)AfxGetApp()->m_pMainWnd;
  CMFCStatusBar* pWndStatusBar = pMain->getStatusBar();
  int nIndex = pWndStatusBar->CommandToIndex(ID_INDICATOR_LOGO);
  CString notifStr = _T("MacroSilicon Software - ") + strConnection + _T(" - ") + strConnected;
  pWndStatusBar->SetTipText(0,notifStr);
  pWndStatusBar->SetPaneText(0, notifStr);
  
}

CString toHexCString(int val)
{
  CString str;
  if (val < 0x10) {
    str.Format(_T("%02X"), val);
  } else {
    str.Format(_T("%X"), val);
  }
  return str;
}

int hexToInt(CString str)
{
  int val = 0;
  int converNum = swscanf_s(str, _T("%x"), &val);
  return (converNum > 0) ? val : 0;
}

void CPropertiesWnd::pushRegAddr2Comb(void)
{
  CString str;
  m_edRegAddr.GetWindowTextW(str);
  if (m_edRegAddr.FindString(0, str) == CB_ERR)
  {
    m_edRegAddr.InsertString(0, str);
  }
}

void CPropertiesWnd::pushRegValue2Comb(void)
{
  CString str;
  m_edRegValue.GetWindowTextW(str);
  if (m_edRegValue.FindString(0, str) == CB_ERR)
  {
    m_edRegValue.InsertString(0, str);
  }
}

void CPropertiesWnd::OnRead()
{
  if (!isConnectSucc) {
    //AfxMessageBox(_T("Connect USB device first!"));
	Log(_T("!!!Connect USB device first!!!"));
    return;
  }
  
  m_Value = m_pI2cCtrl->read(m_RegAddr);
  m_edRegValue.SetWindowTextW(toHexCString(m_Value));
  SetCheckList(m_Value);
  pushRegAddr2Comb();
}

void CPropertiesWnd::OnWrite()
{
  if (!isConnectSucc) {
    //AfxMessageBox(_T("Connect USB device first!"));
	Log(_T("!!!Connect USB device first!!!"));
    return;
  }
  m_pI2cCtrl->write(m_RegAddr, m_Value);
  pushRegAddr2Comb();
  pushRegValue2Comb();
}

string toString(CString cs)
{
#ifdef _UNICODE  
  USES_CONVERSION;  
  string str(W2A(cs));  
  return str;  
#else  
  //如果是多字节工程   
  std::string str(cs.GetBuffer());  
  cs.ReleaseBuffer();  
  return str;   
#endif // _UNICODE   
}  
  
  
CString toCString(string str)
{  
#ifdef _UNICODE  
  USES_CONVERSION;  
  CString ans(str.c_str());  
  return ans;  
#else  
  //如果是多字节工程   
  CString ans;  
  ans.Format("%s", str.c_str());  
  return ans;   
#endif // _UNICODE    
}  


void CPropertiesWnd::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  // TODO: 在此添加消息处理程序代码和/或调用默认值

  CDockablePane::OnKeyUp(nChar, nRepCnt, nFlags);
}

bool justHasHexChar(CString cstr)
{
  string str;
  string patter("[^0-9a-fA-F]");
  regex r(patter);
  smatch match;
  str = toString(cstr);
  return !regex_search(str, match, r);
}

void CPropertiesWnd::OnRegAddrEditChange()
{
  CString regAddrStr;
  CWnd *pWndName;
  int newValue = 0;
  pWndName = GetDlgItem(ID_COMB_REGADDR);
  pWndName->GetWindowText(regAddrStr);
  if (regAddrStr.GetLength() == 0)
    return;
  if (justHasHexChar(regAddrStr)) {
    int converNum = swscanf_s(regAddrStr, _T("%x"), &newValue);
    m_RegAddr = (UINT16)newValue;
    //m_edRegAddr.AddString(regAddrStr);  //输入的字符(单个+连续)都会添加到下拉框,如输入501,会添加5, 50, 501
  } else {
    regAddrStr.Format(_T("%X"), m_RegAddr);
    m_edRegAddr.SetWindowTextW(regAddrStr);
    m_edRegAddr.SetEditSel(regAddrStr.GetLength(), -1);
  }
}

void CPropertiesWnd::OnSelRegAddrCombo()
{
    int nIndex = m_edRegAddr.GetCurSel();
    CString regAddrStr;
    int newValue = 0;
    m_edRegAddr.GetLBText(nIndex, regAddrStr);
    if (regAddrStr.GetLength() == 0)
        return;
    if (justHasHexChar(regAddrStr))
    {
        int converNum = swscanf_s(regAddrStr, _T("%x"), &newValue);
        m_RegAddr = (UINT16)newValue;
        //m_edRegAddr.AddString(regAddrStr);  //输入的字符(单个+连续)都会添加到下拉框,如输入501,会添加5, 50, 501
    }
    else
    {
        regAddrStr.Format(_T("%X"), m_RegAddr);
        m_edRegAddr.SetWindowTextW(regAddrStr);
        m_edRegAddr.SetEditSel(regAddrStr.GetLength(), -1);
    }
}
/*int nIndex = m_cbExample.GetCurSel();

CString strCBText;

m_cbExample.GetLBText( nIndex, strCBText);

这样，得到的内容就保存在 strCBText 中。

若要选取当前内容，可调用函数GetWindowText(strCBText)。
nIndex = m_cbTiming.GetCurSel();
CString strCBText;
CString strResult;
m_cbTiming.GetLBText( nIndex, strCBText);*/

void CPropertiesWnd::OnRegValEditChange()
{
  CString regValueStr;
  CWnd *pWndName;
  int newValue = 0;
  pWndName = GetDlgItem(ID_COMB_REGVALUE);
  pWndName->GetWindowText(regValueStr);
  if (regValueStr.GetLength() == 0)
    return;
  if (justHasHexChar(regValueStr)) {
    int converNum = swscanf_s(regValueStr, _T("%x"), &newValue);
    m_Value = (BYTE)newValue;
    SetCheckList(m_Value);
    //m_edRegValue.AddString(regValueStr);  //输入的字符都会添加到下拉框
  } else {
    regValueStr.Format(_T("%X"), m_Value);
    m_edRegValue.SetWindowTextW(regValueStr);
    m_edRegValue.SetEditSel(regValueStr.GetLength(), -1);
  }
}

void CPropertiesWnd::OnSelRegValCombo()
{
    int nIndex = m_edRegValue.GetCurSel();
    CString regValueStr;
    int newValue = 0;
    m_edRegValue.GetLBText(nIndex, regValueStr);
    if (regValueStr.GetLength() == 0)
        return;
    if (justHasHexChar(regValueStr))
    {
        int converNum = swscanf_s(regValueStr, _T("%x"), &newValue);
        m_Value = (BYTE)newValue;
        SetCheckList(m_Value);
        //m_edRegValue.AddString(regValueStr);  //输入的字符都会添加到下拉框
    }
    else
    {
        regValueStr.Format(_T("%X"), m_Value);
        m_edRegValue.SetWindowTextW(regValueStr);
        m_edRegValue.SetEditSel(regValueStr.GetLength(), -1);
    }
}

void CPropertiesWnd::saveConfigure(E_CONFIG_TYPE type, int value) const
{
  CWinApp* pApp = AfxGetApp();
  if (type >= CFG_NULL) {
    return;
  }
  CString strSection = CONFIGMAP[type].sectName;
  CString strIntItem = CONFIGMAP[type].keyName;
  pApp->WriteProfileInt(strSection, strIntItem, value);
}

void CPropertiesWnd::loadConfigure(void)
{
  CWinApp* pApp = AfxGetApp();
  CString strSection;
  CString strIntItem;
  int index;
  E_CONFIG_TYPE type = CFG_CONNECT_MODE;
  strSection = CONFIGMAP[type].sectName;
  strIntItem = CONFIGMAP[type].keyName;
  index = pApp->GetProfileInt(strSection, strIntItem, INDEX_CONNECT_TYPE);
  m_cboxConnectMode.SetCurSel(index);

  type = CFG_I2C_MODE;
  strSection = CONFIGMAP[type].sectName;
  strIntItem = CONFIGMAP[type].keyName;
  index = pApp->GetProfileInt(strSection, strIntItem, DEFI_I2C_MODE);
  if (g_pI2cModeProp != NULL) {
    g_pI2cModeProp->SetValue(I2C_ModeArray[index]);
    
  }

  type = CFG_I2C_SPEED;
  strSection = CONFIGMAP[type].sectName;
  strIntItem = CONFIGMAP[type].keyName;
  index = pApp->GetProfileInt(strSection, strIntItem, DEFI_I2C_SPEED);
  if (g_pI2cSpeedProp != NULL) {
    g_pI2cSpeedProp->SetValue(I2C_SpeedArray[index]);
  }
  
}

void CPropertiesWnd::OnConnectModeChange()
{
  int index = m_cboxConnectMode.GetCurSel();
  saveConfigure(CFG_CONNECT_MODE, index);
  OnConnect();
}

void CPropertiesWnd::OnLogR()
{
  gFlagLogR = !gFlagLogR;
  m_btnLogR.SetCheck(gFlagLogR);
}

void CPropertiesWnd::OnLogW()
{
  gFlagLogW = !gFlagLogW;
  m_btnLogW.SetCheck(gFlagLogW);
}

void CPropertiesWnd::OnClearLog()
{
	CMainFrame* pMain = (CMainFrame*)AfxGetApp()->GetMainWnd();
	pMain->getOutputWnd()->ClearLogWindow();
}

void CPropertiesWnd::OnSaveLog()
{
	CMainFrame* pMain = (CMainFrame*)AfxGetApp()->GetMainWnd();
	pMain->getOutputWnd()->SaveLog();
}