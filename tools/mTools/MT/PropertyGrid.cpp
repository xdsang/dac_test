
#include "stdafx.h"
#include "MainFrm.h"
#include "PropertyGrid.h"
#include "Resource.h"
#include "MT.h"

extern CMFCPropertyGridCtrl m_wndPropList;
extern PropertyGridProperty* m_pRegister;
extern CParseTXT m_parsetxt;

IMPLEMENT_DYNAMIC(PropertyGridProperty, CMFCPropertyGridProperty)


PropertyGridProperty::PropertyGridProperty(const CString& strGroupName, DWORD_PTR dwData /*= 0*/,
    BOOL bIsValueList /*= FALSE*/ )
    : CMFCPropertyGridProperty(strGroupName, dwData, bIsValueList)
{

}

PropertyGridProperty::PropertyGridProperty(const CString& strName, const COleVariant& varValue, LPCTSTR lpszDescr /*= NULL*/, DWORD_PTR dwData /*= 0*/, LPCTSTR lpszEditMask /*= NULL*/, 
    LPCTSTR lpszEditTemplate /*= NULL*/, LPCTSTR lpszValidChars /*= NULL*/ )
    : CMFCPropertyGridProperty(strName, varValue, lpszDescr, dwData, lpszEditMask, lpszEditTemplate, lpszValidChars)
{
    
}

PropertyGridProperty::~PropertyGridProperty()
{

}

CString PropertyGridProperty::FormatProperty()
{
	CString text;

	COleVariant value = GetValue();

	value.ChangeType(VT_UINT);
	DWORD i = value.uintVal;

	text.Format(_T("0x%04X"), i);

	return text;
}

DWORD_PTR click_table_index = 2000;
BOOL PropertyGridProperty::OnClickValue(UINT uiMsg, CPoint point)
{
	CString text;
	COleVariant value = GetValue();
	click_table_index = GetData();

	value.ChangeType(VT_UINT);
	DWORD i = value.uintVal;

	if (m_pWndSpin)
	{
		m_pWndSpin->SetBase(16);
		m_pWndSpin->SetPos(i);

	}
	return TRUE;
}

void PropertyGridProperty::OnClickName(CPoint /*point*/)
{
	click_table_index = GetData();
	UINT32 current_value = 0;
	WORD sel_addr;
	BYTE sel_st_bit;
	int sel_length;
	//读取所选项目的寄存器的实时数值
	m_pRegister = (PropertyGridProperty*)m_wndPropList.FindItemByData(click_table_index);
	sel_addr = m_parsetxt.reg_addr[click_table_index];
	sel_st_bit = m_parsetxt.reg_st[click_table_index];
	sel_length = m_parsetxt.reg_length[click_table_index];
	current_value = HAL_ReadRange(sel_addr, sel_st_bit, sel_length);
	m_pRegister->SetValue((_variant_t)current_value);
}

BOOL PropertyGridProperty::OnUpdateValue()
{
	CMFCPropertyGridProperty::OnUpdateValue();

	CString text;

	m_pWndInPlace->GetWindowText(text);
	text.Trim();
	::StrToIntEx(text.GetString(), STIF_SUPPORT_HEX, (int *)&_hexvalue);
#if 1
	m_varValue.ChangeType(VT_UINT);
	m_varValue.uintVal = _hexvalue;

	//SetValue(_lastValue);
#endif

	return TRUE;
}
VOID PropertyGridProperty::UpdateValue()
{
	CString text;
	m_pWndInPlace->GetWindowText(text);
	text.Trim();
	::StrToIntEx(text.GetString(), STIF_SUPPORT_HEX, (int *)&_hexvalue);
#if 1
	m_varValue.ChangeType(VT_UINT);
	m_varValue.uintVal = _hexvalue;
#endif
}