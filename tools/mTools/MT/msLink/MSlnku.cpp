#include "stdafx.h"
#include "MSLnkU.h"
#include "MSLinkSerial.h"
#include "MSLinkVirtual.h"
#include "MSLinkUSB2I2C.h"
#include "MSLinkUSBHID.h"

CMSLinkU msLink;
CMSLinkU *puLnk = &msLink;
CMSLinkSerial spLnk;
static CMSLinkVirtual vLnk;
static CMSLinkUSB2I2C ucLnk;
static CMSLinkUSBHID  uhidLnk;

CMSLinkU::CMSLinkU(void)
{
    bI2CAddr       = MS_CHIP_ADDR1;
    bConnectType   = MSLNK_VIRTUAL;
    m_bIsConnected = FALSE;
}

CMSLinkU::~CMSLinkU(void)
{
    
}


void CMSLinkU::SetI2CAddr(BYTE bAddr)
{
    this->bI2CAddr = bAddr;
}

BYTE CMSLinkU::GetI2CAddr()
{
    return bI2CAddr;
}

BYTE CMSLinkU::GetConnectType()
{
    return bConnectType;
}

void CMSLinkU::SetConnect(BOOL bConnected)
{
    m_bIsConnected = bConnected;
}

BOOL CMSLinkU::IsConnect()
{
    return m_bIsConnected;
}

void CMSLinkU::SetConnectType(BYTE bType)
{
  if(bConnectType == MSLNK_SERIAL && spLnk.isConnected()) 
  {
    spLnk.disconnect();
  }
  bConnectType = bType;
}

int CMSLinkU::Connect()
{
  int result = 0;
  static int com = 1;
  if(bConnectType == MSLNK_SERIAL) {
    char  pName[12];
    CStringA portName;
    portName.Format("COM%d", m_serPort);
    strcpy_s(pName, portName.GetLength() +1, portName);
    result = spLnk.connect(pName, m_baudRate);
    Sleep(50);
  }
  else if(bConnectType == MSLNK_USB2I2C) {
    result = ucLnk.Connect();
  }
  else if (bConnectType == MSLNK_USBHID) {
      result = uhidLnk.Connect();
  }
  else if(bConnectType == MSLNK_VIRTUAL) {
    result = 1;
  }
  return result;
}

BOOL CMSLinkU::SetIICSpeed(BYTE mode)
{
	int result = 0;

	 result = ucLnk.IIC_speed(mode);

	return result;
}

int CMSLinkU::i2cr(int DevID, int Addr , unsigned char *pData)
{
    int result = 0;

    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.i2cr(DevID , Addr , pData);
    }
	else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cr(DevID , Addr , pData);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        result = ucLnk.i2cr(DevID , Addr , pData);
    }
    else if (bConnectType == MSLNK_USBHID) {
        result = uhidLnk.i2cr(DevID, Addr, pData);
    }

    return result;
}

int CMSLinkU::i2cr(int DevID, int Addr , WORD repeat, unsigned char *pData)
{
    int result = 0;
	
    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.ior(Addr , repeat, pData);
    }
	else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.ior(Addr , repeat, pData);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        result = ucLnk.i2cr(DevID , Addr ,repeat, pData);
    }
    else if (bConnectType == MSLNK_USBHID) {
        result = uhidLnk.i2cr(DevID, Addr, repeat, pData);
    }
	
    return result;
}

int CMSLinkU::i2cw(int DevID, int Addr)
{
    int result = 0;

    if(bConnectType == MSLNK_USB2I2C) {
        ucLnk.i2cw(DevID , Addr);
    }

    return result;
}

int CMSLinkU::i2cw(int DevID, int Addr, int Data)
{
    int result = 0;

    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.i2cw(DevID , Addr , Data);
    }
	else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cw(DevID , Addr , Data);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        ucLnk.i2cw(DevID , Addr , Data);
    }
    else if (bConnectType == MSLNK_USBHID) {
        uhidLnk.i2cw(DevID, Addr, Data);
    }

    return result;
}


int CMSLinkU::i2cw(int DevID, int Addr, int Data,unsigned char mask)
{
  BYTE b;

  if(i2cr(DevID,Addr,&b))
    return !0;

  b &= mask;
  b |= Data&(~mask);

  return i2cw(DevID,Addr,b);
}


/*16bit mode*/
int CMSLinkU::i2cr16(int DevID, int Addr , unsigned char *pData)
{
    int result = 0;
    
    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.i2cr16(DevID , Addr , pData);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cr16(DevID , Addr , pData);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        result = ucLnk.i2cr16(DevID , Addr , pData);
    }
    else if (bConnectType == MSLNK_USBHID) {
        result = uhidLnk.i2cr16(DevID, Addr, pData);
    }
    
    return result;
}

/*16bit mode*/
int CMSLinkU::i2cr16(int DevID, int addr , WORD repeat, unsigned char *pData)
{
    int result = 0;
    
    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.ior16(addr , repeat, pData);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cr16(DevID , addr , repeat, pData);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        result = ucLnk.i2cr16(DevID , addr , repeat, pData);
    }
    else if (bConnectType == MSLNK_USBHID) {
        result = uhidLnk.i2cr16(DevID, addr, repeat, pData);
    }
    
    return result;
}

int CMSLinkU::i2cw16(int DevID, int Addr, int Data)
{
    int result = 0;
    
    if(bConnectType == MSLNK_SERIAL) {
        result = spLnk.i2cw16(DevID , Addr , Data);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cw16(DevID , Addr , Data);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        ucLnk.i2cw16(DevID , Addr , Data);
    }
    else if (bConnectType == MSLNK_USBHID) {
        uhidLnk.i2cw16(DevID, Addr, Data);
    }
    
    return result;
}

int CMSLinkU::i2cw16(int DevID, int Addr, int Data,unsigned char mask)
{
    BYTE b;
    
    if(i2cr16(DevID,Addr,&b))
        return !0;
    
    b &= mask;
    b |= Data&(~mask);
    
    return i2cw16(DevID,Addr,b);
}

int CMSLinkU::i2cw16(int DevID, int addr, WORD repeat, unsigned char * pData)
{
    int result = 0;

    if (bConnectType == MSLNK_SERIAL) {
        result = -1;
    }
    else if (bConnectType == MSLNK_VIRTUAL) {
        result = vLnk.i2cw16(DevID, addr, repeat, pData);
    }
    else if (bConnectType == MSLNK_USB2I2C) {
        result = ucLnk.i2cw16(DevID, addr, repeat, pData);
    }
    else if (bConnectType == MSLNK_USBHID) {
        result = uhidLnk.i2cw16(DevID, addr, repeat, pData);
    }

    return result;
}


int CMSLinkU::ior(BYTE port, BYTE *pValue)
{
    int nRet = 0;
    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.ior((BYTE)port, pValue);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.ior((BYTE)port,pValue);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.ior((BYTE)port,pValue);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.ior((BYTE)port, pValue);
    }

    return nRet;
}

int CMSLinkU::ior(BYTE port, WORD repeat, BYTE *pValue)
{
    int nRet = 0;

    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.ior((BYTE)port, (BYTE)repeat, pValue);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.ior((BYTE)port,repeat,pValue);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.ior((BYTE)port,repeat,pValue);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.ior((BYTE)port, repeat, pValue);
    }

    return nRet;
}

int CMSLinkU::iow(BYTE port, BYTE value)
{
    int nRet = 0;

    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.iow((BYTE)port, value);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.iow((BYTE)port,value);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.iow((BYTE)port,value);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.iow((BYTE)port, value);
    }

    return nRet;
}

int CMSLinkU::iow(BYTE port, BYTE value, BYTE mask)
{
    BYTE b;

    if(ior(port,&b))
        return !0;
    b &= mask;
    b |= value&(~mask);

    return iow(port , b);
}


/*16bit mode*/
int CMSLinkU::ior16(WORD port, BYTE *pValue)
{
    int nRet = 0;
    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.ior16((WORD)port, pValue);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.ior16((WORD)port,pValue);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.ior16((WORD)port,pValue);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.ior16((WORD)port, pValue);
    }
    
    return nRet;
}

int CMSLinkU::ior16(WORD port, WORD repeat, BYTE *pValue)
{
    int nRet = 0;
    
    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.ior16((WORD)port, (BYTE)repeat, pValue);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.ior16((WORD)port,repeat,pValue);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.ior16((WORD)port,repeat,pValue);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.ior16((WORD)port, repeat, pValue);
    }
    
    return nRet;
}

int CMSLinkU::iow16(WORD port, BYTE value)
{
    int nRet = 0;
    
    if(bConnectType == MSLNK_SERIAL) {
        nRet = spLnk.iow16((WORD)port, value);
    }
    else if(bConnectType == MSLNK_VIRTUAL) {
        nRet = vLnk.iow16((WORD)port,value);
    }
    else if(bConnectType == MSLNK_USB2I2C) {
        nRet = ucLnk.iow16((WORD)port,value);
    }
    else if (bConnectType == MSLNK_USBHID) {
        nRet = uhidLnk.iow16((WORD)port, value);
    }
    
    return nRet;
}

int CMSLinkU::iow16(WORD port, BYTE value, BYTE mask)
{
    BYTE b;
    
    if(ior16(port,&b))
        return !0;
    b &= mask;
    b |= value&(~mask);
    
    return iow16(port , b);
}

void CMSLinkU::setSerialPort(BYTE port)
{
  m_serPort = port;
}
void CMSLinkU::setBaudRate(int rate)
{
  m_baudRate = rate;
}  
