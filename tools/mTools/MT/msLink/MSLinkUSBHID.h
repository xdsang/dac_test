#pragma once


class CMSLinkUSBHID
{
public:
    CMSLinkUSBHID(void);
    ~CMSLinkUSBHID(void);

public:
    int Connect();
public:

    int i2cr(BYTE addr, BYTE subaddr, BYTE *pValue);
    int i2cr(BYTE addr, BYTE subaddr, WORD repeat, BYTE *pValue);
    int i2cw(BYTE addr, BYTE subaddr);
    int i2cw(BYTE addr, BYTE subaddr, BYTE value); 
    int i2cw(BYTE addr, BYTE subaddr, WORD repeat, BYTE *pValue); 

    int i2cr16(BYTE addr, WORD subaddr, BYTE *pValue);
    int i2cr16(BYTE addr, WORD subaddr, WORD repeat, BYTE *pValue);
    int i2cw16(BYTE addr, WORD subaddr, BYTE value); 
    int i2cw16(BYTE addr, WORD subaddr, WORD repeat, BYTE *pValue);

    /* low-level API interface */
    int ior(BYTE port, BYTE *pValue);
    int ior(BYTE port, WORD repeat, BYTE *pValue);
    int iow(BYTE port, BYTE value);

    int ior16(WORD port, BYTE *pValue);
    int ior16(WORD port, WORD repeat, BYTE *pValue);
    int iow16(WORD port, BYTE value);
    int ior16_burst(WORD port, WORD repeat, BYTE *pValue);
    int iow16_burst(WORD port, WORD repeat, BYTE *pValue);

private:
	BOOL connected;
    HANDLE mUsbHidHandle;
};
