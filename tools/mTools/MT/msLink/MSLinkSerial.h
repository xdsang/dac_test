// MSLinkSerial.h: interface for the CMSLinkSerial class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINKSERIAL_H__075910B7_3C88_4138_A08C_C56ACA4CFB76__INCLUDED_)
#define AFX_LINKSERIAL_H__075910B7_3C88_4138_A08C_C56ACA4CFB76__INCLUDED_

#include "tserial.h"    // Added by ClassView
#include "MSLnkU.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/* Machine state for MSLink module */
#define FSA_SYNC    0
#define FSA_CMD     1
#define FSA_LEN     2
#define FSA_DATA    3
#define FSA_CRC     4

#define TLNK_BUFSIZE            64
#define TLNK_SYNC               0x10

/* 
 * MS Link Command definition 
 * 00-2F    PC link control command, such Ack, NAck, ...
 * 30-4F    General command
 * 60-6F    I2C interface
 */
#define TLNKCMD_ACK             0x06
#define TLNKCMD_NACK            0x21



#define TLNKCMD_GETVERSION      0x30
#define TLNKCMD_QUERYBAUDRATE   0x31
#define TLNKCMD_SETBAUDRATE     0x32
#define TLNKCMD_DELAY           0x33
#define TLNKCMD_GETVERSIONEX    0x34
#define TLNKCMD_QUERYBUFSIZE    0x35

/* depend on bLen to decide ior <port> [index [repeat]] */
#define TLNKCMD_IOR             0x50

/* depend on bLen to decide iow <port> [index [mask]] */
#define TLNKCMD_IOW             0x51


/* depend on bLen to decide what i2cr or i2cw should do */
#define TLNKCMD_I2CR            0x60
#define TLNKCMD_I2CW            0x61

#define TLNKCMD_PORTR           0x62
#define TLNKCMD_PORTW           0x63


/* general I2C APIs */
#define TLNKCMD_GENI2CR         0x66
#define TLNKCMD_GENI2CW         0x67

#define TLNKCMD_GENI2CR16         0x68
#define TLNKCMD_GENI2CW16         0x69
/* flags definition for GEN I2C */
#define I2CRWF_RANDOMACCESS     0x80
#define DDCRWF_RANDOMACCESS     0x80

typedef struct {
    BYTE       bSync;
    BYTE       bCmd;
    BYTE       bLen;
    BYTE       bData[TLNK_BUFSIZE];
    BYTE       bCrc;
} TLNK_PACKET;

#define TLNK_DEFTIMEOUT     2000    /* milisecond */

#define GET_BYTE(x) {int result = getByte(&x); if (result!=TERR_OK) return result; }
#define GET_PK() { int result = getPk(); if (processError(result)!=0) return result; }


class CMSLinkSerial
{
public:
    
    CMSLinkSerial();
    virtual ~CMSLinkSerial();


    /*high-level APIs */
    int     version(BYTE *ver);

    /* low-level API interface */
    int     ior(BYTE port, BYTE *pValue);
    int     ior(BYTE port, BYTE repeat, BYTE *pValue);
    int     iow(BYTE port, BYTE value);

    int     ior16(WORD port, BYTE *pValue);
    int     ior16(WORD port, BYTE repeat, BYTE *pValue);
    int     iow16(WORD port, BYTE value);

    int     delay(WORD ms);

    /* aclink access */

    /* i2c access */
    int     i2cr(BYTE addr, BYTE subaddr, BYTE *pValue);
    int     i2cw(BYTE addr, BYTE subaddr, BYTE value);

    int     i2cr16(BYTE addr, WORD subaddr, BYTE *pValue);
    int     i2cw16(BYTE addr, WORD subaddr, BYTE value);

    /* mcu access */
    int     ppr(BYTE port, BYTE *pValue);
    int     ppr(BYTE port, BYTE pin, BYTE *pValue);
    int     ppw(BYTE port, BYTE value);
    int     ppw(BYTE port, BYTE pin, BYTE value);
    
  
    /* helper */
    DWORD       setTimeOut(DWORD to);
    int         connect(char *szPort, int baud);
    void        disconnect();
    int         detect();
    int         getByte(BYTE *pb);
    int         getPk();
    int         processError(int err);
    void        clearInBuffer();
    int         getErrorCode();
    BOOL        isConnected();
    int         GetBufferSize(int &iSize);
    void        CalCRC();
private:
    BYTE   m_I2CAddr;
    int         errorCode;
    BOOL        connected;
    BYTE        bFlags;
    DWORD       timeOut;
    Tserial     comm;
    TLNK_PACKET inPk, outPk;
    int         m_iBufferSize;
    BYTE m_serPort;
    int m_baudRate;
    BOOL m_isSpOpen;


public:
    Tserial *GetCommCtrl();
    BYTE getCurSerialPort();
    int getCurBaudRate();
    BOOL openPort(BYTE port, int rate);
    void closePort();
    BOOL isSerPortOpen();
    int isExistPort(BYTE port);
};

#endif // !defined(AFX_LINKSERIAL_H__075910B7_3C88_4138_A08C_C56ACA4CFB76__INCLUDED_)
