#include "stdafx.h"
#include "MSLnkU.h"
#include "MSLinkUSB2I2C.h"
#include "usbiox.h"
#include <direct.h>
#include <stdlib.h>
#include <stdio.h>

extern CMSLinkU *puLnk;

CMSLinkUSB2I2C::CMSLinkUSB2I2C(void)
{
    connected = FALSE;
}

CMSLinkUSB2I2C::~CMSLinkUSB2I2C(void)
{
	if (connected)
	{
        USBIO_CloseDevice(0);
		connected = FALSE;
	}
}

/* 
function: Connect()
if return 0, connect failed,
if return nonzero, connect success.

//  - Search Slave ID from 80H to F0H using i2c timing.
//  - If ack from any ID, exit and connected. Otherwise dis-connected.
*/

int CMSLinkUSB2I2C::Connect()
{
	int nRet = 1;

    if(USBIO_OpenDevice(0) == INVALID_HANDLE_VALUE)
	{
		//AfxMessageBox("CH341 is not here!");
		nRet = 0;
	}
    
	connected = TRUE;
    return nRet;
}

BOOL CMSLinkUSB2I2C::IIC_speed(BYTE mode)
{
	BOOL nRet = 0;

    if(USBIO_SetStream(0, mode))
	{
		nRet = 1;
	}

    return nRet;
}

int CMSLinkUSB2I2C::i2cr(BYTE addr, BYTE subaddr, BYTE *pValue)
{
	BYTE wBuf[2];

	wBuf[0] = addr;
	wBuf[1] = subaddr;

	if (!USBIO_StreamI2C(0, 2, wBuf, 1, pValue))
	{
		return TERR_HARDWAREBUSY;
	}

	return 0;
}

int CMSLinkUSB2I2C::i2cr(BYTE addr, BYTE subaddr, WORD repeat, BYTE *pValue)
{
	BYTE wBuf[2];
	wBuf[0] = addr;
	wBuf[1] = subaddr;
	if (!USBIO_StreamI2C(0, 2, wBuf, repeat, pValue))
	{
		return TERR_HARDWAREBUSY;
	}
	return 0;
}
int CMSLinkUSB2I2C::i2cw(BYTE addr, BYTE subaddr, BYTE value) 
{
    BYTE wBuf[3];

	wBuf[0] = addr;
	wBuf[1] = subaddr;
	wBuf[2] = value;

	if (!USBIO_StreamI2C(0, 3, wBuf, 0, 0))
	{
		return TERR_HARDWAREBUSY;
	}

	return 0;
}


int CMSLinkUSB2I2C::i2cw(BYTE addr, BYTE subaddr) 
{
    BYTE wBuf[3];

	wBuf[0] = addr;
	wBuf[1] = subaddr;

	if (!USBIO_StreamI2C(0, 2, wBuf, 0, 0))
	{
		return TERR_HARDWAREBUSY;
	}

	return 0;
}

int CMSLinkUSB2I2C::i2cw(BYTE addr, BYTE subaddr, WORD repeat, BYTE *pValue) 
{
    BYTE wBuf[2+256];

	wBuf[0] = addr;
	wBuf[1] = subaddr;
	for(int i=0; i<repeat; i++)
	{
		wBuf[2+i] = pValue[i];
	}
	
	if (!USBIO_StreamI2C(0, 2+repeat, wBuf, 0, 0))
	{
		return TERR_HARDWAREBUSY;
	}
	
	return 0;
}
int CMSLinkUSB2I2C::i2cr16(BYTE addr, WORD port, BYTE *pValue) 
{
    BYTE wBuf[3];
    BYTE bId = addr;
    
    wBuf[0] = bId;
    wBuf[1] = (BYTE)port;
    wBuf[2] = (BYTE)(port >> 8);
    
    if (!USBIO_StreamI2C(0, 3, wBuf, 1, pValue))
    {
        return TERR_HARDWAREBUSY;
    }
    
    return 0;
}

int CMSLinkUSB2I2C::i2cr16(BYTE addr, WORD port, WORD repeat, BYTE *pValue) 
{
    BYTE wBuf[3];
    BYTE bId = addr;
    
    wBuf[0] = bId;
    wBuf[1] = (BYTE)port;
    wBuf[2] = (BYTE)(port >> 8);
    
    if (!USBIO_StreamI2C(0, 3, wBuf, repeat, pValue))
    {
        return TERR_HARDWAREBUSY;
    }
    
    return 0;
}

int CMSLinkUSB2I2C::i2cw16(BYTE addr, WORD port, BYTE value) {
  BYTE wBuf[4];
  BYTE bId = addr;
    
  wBuf[0] = bId;
  wBuf[1] = (BYTE)port;
  wBuf[2] = (BYTE)(port >> 8);
  wBuf[3] = value;
    
  if (!USBIO_StreamI2C(0, 4, wBuf, 0, 0))
  {
    return TERR_HARDWAREBUSY;
  }
    
  return 0;
}

int CMSLinkUSB2I2C::i2cw16(BYTE addr, WORD port, WORD repeat, BYTE *pValue) {
	BYTE wBuf[65535 + 3];
	BYTE bId = puLnk->GetI2CAddr();
	WORD i;
	wBuf[0] = bId;
	wBuf[1] = (BYTE)port;
	wBuf[2] = (BYTE)(port >> 8);
	for (i = 0; i < repeat; i++)
	{
		wBuf[3 + i] = *pValue++;
	}

	if (!USBIO_StreamI2C(0, 3 + repeat, wBuf, 0, 0))
	{
		return TERR_HARDWAREBUSY;
	}

	return 0;
}

/*
 * IOR/IOW Interface
 */
int CMSLinkUSB2I2C::ior(BYTE port, BYTE *pValue) 
{
	return (i2cr(puLnk->GetI2CAddr(), port, pValue));
}

int CMSLinkUSB2I2C::ior(BYTE port, WORD repeat, BYTE *pValue) 
{
	BYTE wBuf[2];
	BYTE bId = puLnk->GetI2CAddr();

	wBuf[0] = bId;
	wBuf[1] = (BYTE)port;

	if (!USBIO_StreamI2C(0, 2, wBuf, repeat, pValue))
	{
		return TERR_HARDWAREBUSY;
	}

    return 0;
}

int CMSLinkUSB2I2C::iow(BYTE port, BYTE value) {
	return (i2cw(puLnk->GetI2CAddr(), port, value));
}

// For 16bit address I2C
int CMSLinkUSB2I2C::ior16(WORD port, BYTE *pValue) 
{
	BYTE wBuf[3];
	BYTE bId = puLnk->GetI2CAddr();
	
	wBuf[0] = bId;
	wBuf[1] = (BYTE)port;
	wBuf[2] = (BYTE)(port >> 8);
	
	if (!USBIO_StreamI2C(0, 3, wBuf, 1, pValue))
	{
		return TERR_HARDWAREBUSY;
	}
	
    return 0;
}

int CMSLinkUSB2I2C::ior16(WORD port, WORD repeat, BYTE *pValue) 
{
#if 1
    for (int i=0; i<repeat; i++)
    {
        ior16(port+i, pValue+i);
    }
#else
	BYTE wBuf[3];
	BYTE bId = puLnk->GetI2CAddr();
	
	wBuf[0] = bId;
	wBuf[1] = (BYTE)port;
	wBuf[2] = (BYTE)(port >> 8);
	
	if (!USBIO_StreamI2C(0, 3, wBuf, repeat, pValue))
	{
		return TERR_HARDWAREBUSY;
	}
#endif
	
    return 0;
}

int CMSLinkUSB2I2C::iow16(WORD port, BYTE value) {
	BYTE wBuf[4];
	BYTE bId = puLnk->GetI2CAddr();
	
	wBuf[0] = bId;
	wBuf[1] = (BYTE)port;
    wBuf[2] = (BYTE)(port >> 8);
	wBuf[3] = value;
	
	if (!USBIO_StreamI2C(0, 4, wBuf, 0, 0))
	{
		return TERR_HARDWAREBUSY;
	}
	
    return 0;
}

int CMSLinkUSB2I2C::ior16_burst(WORD port, WORD repeat, BYTE *pValue) 
{
#if 0
  for (int i=0; i<repeat; i++)
  {
    ior16(port+i, pValue+i);
  }
#else
  BYTE wBuf[3];
  BYTE bId = puLnk->GetI2CAddr();
	
  wBuf[0] = bId;
  wBuf[1] = (BYTE)port;
  wBuf[2] = (BYTE)(port >> 8);
	
  if (!USBIO_StreamI2C(0, 3, wBuf, repeat, pValue))
  {
    return TERR_HARDWAREBUSY;
  }
#endif
	
  return 0;
}


int CMSLinkUSB2I2C::iow16_burst(WORD port, WORD repeat, BYTE *pValue) {
  BYTE wBuf[65535+3];
  BYTE bId = puLnk->GetI2CAddr();
  WORD i;
  wBuf[0] = bId;
  wBuf[1] = (BYTE)port;
  wBuf[2] = (BYTE)(port >> 8);
  for(i = 0; i < repeat; i++)
  {
    wBuf[3 + i] = *pValue ++;
  }
	
  if (!USBIO_StreamI2C(0, 3+repeat, wBuf, 0, 0))
  {
    return TERR_HARDWAREBUSY;
  }
	
  return 0;
}
