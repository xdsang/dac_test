#include "stdafx.h"
#include "MSLinkVirtual.h"
#include "MSLnkU.h"

extern CMSLinkU *puLnk;

WORD sp = 0;

CMSLinkVirtual::CMSLinkVirtual(void)
{
  sp = 1; 
  m_bBank = 0;
  memset(m_RegLinearBuf, 0, sizeof(m_RegLinearBuf)/sizeof(BYTE));
  memset(m_RegBankBuf, 0, sizeof(m_RegBankBuf)/sizeof(BYTE));

  m_pMemoryBuf = (BYTE *)malloc(0x100*0x8000);
  ASSERT(m_pMemoryBuf != NULL);
  memset(m_pMemoryBuf, 0, 0x100*0x8000);

  // Build virtual ID for chip, ms chips.
  m_RegLinearBuf[MS_CHIP_ID_ADDR] = (BYTE)(MS_CHIP_ID >> 16);
  m_RegLinearBuf[MS_CHIP_ID_ADDR +1] = (BYTE)(MS_CHIP_ID >> 8);
  m_RegLinearBuf[MS_CHIP_ID_ADDR +2] = (BYTE)(MS_CHIP_ID & 0xFF);
}

CMSLinkVirtual::~CMSLinkVirtual(void)
{
    if (m_pMemoryBuf != NULL)
        free(m_pMemoryBuf);
}

int CMSLinkVirtual::i2cr(BYTE DevID, BYTE Addr , BYTE *pData)
{
	if (Addr >= 0xF0)
	{
		*pData = m_bBank;
	}
	else
	{
		*pData = m_RegBankBuf[m_bBank * 256 + Addr];
	}

	return 0;
}

int CMSLinkVirtual::i2cw(BYTE DevID, BYTE Addr, BYTE Data)
{
	if (Addr >= 0xF0)
	{
		m_bBank = Data;
	}
	else
	{
		m_RegBankBuf[m_bBank * 256 + Addr] = Data;
	}

	return 0;
}

int CMSLinkVirtual::i2cr16(BYTE DevID, WORD port, BYTE *pValue) {
    int ret = 0;
    
    *pValue = m_RegLinearBuf[port];
    return ret;
}

int CMSLinkVirtual::i2cr16(BYTE DevID, WORD port, WORD repeat, BYTE *pValue) {
    int ret = 0,i;
    
    for(i=0;i<repeat;i++)
        *pValue++ = m_RegLinearBuf[port++];
    return ret;
}

int CMSLinkVirtual::i2cw16(BYTE DevID, WORD port, BYTE value) {
    int ret = 0;
    
    m_RegLinearBuf[port] = value;
    return ret;
}

int CMSLinkVirtual::i2cw16(BYTE DevID, WORD port, WORD repeat, BYTE * pValue)
{
	int ret = 0, i;

	for (i = 0; i < repeat; i++)
		m_RegLinearBuf[port++] = *pValue++;
	return ret;
}

/*
 * IOR/IOW Interface
 */
int CMSLinkVirtual::ior(BYTE port, BYTE *pValue) {
	return (i2cr(puLnk->GetI2CAddr(), port, pValue));
}

int CMSLinkVirtual::ior(BYTE port, WORD repeat, BYTE *pValue) {
    int ret = 0,i;
	
    for(i=0;i<repeat;i++)
        *pValue++ = m_RegBankBuf[m_bBank * 256 + port++];
    return ret;
}

int CMSLinkVirtual::iow(BYTE port, BYTE value) {
	return (i2cw(puLnk->GetI2CAddr(), port, value));
}

// For 16bit address I2C
int CMSLinkVirtual::ior16(WORD port, BYTE *pValue) {
    int ret = 0;
	
	if (port < 0x8000)
    {
    *pValue = m_RegLinearBuf[port];
	}
	else
	{
		BYTE reg0010 = m_RegLinearBuf[0x0010];
		*pValue = m_pMemoryBuf[reg0010*0x8000+port-0x8000];
	}
    return ret;
}

int CMSLinkVirtual::ior16(WORD port, WORD repeat, BYTE *pValue) {
    int ret = 0,i;
	
    if (port < 0x8000)
    {
        for(i=0;i<repeat;i++)
        *pValue++ = m_RegLinearBuf[port++];
    }
    else
    {
        /* Memory read */
        BYTE reg0010 = m_RegLinearBuf[0x0010];
        for(i=0;i<repeat;i++)
        {
            *pValue++ = m_pMemoryBuf[reg0010*0x8000+port-0x8000+i];
        }
    }

    return ret;
}

int CMSLinkVirtual::iow16(WORD port, BYTE value) {
    int ret = 0;
    if (port < 0x8000)
    {
        m_RegLinearBuf[port] = value;
    }
    else
    {
        /* Memory write */
        BYTE reg0010 = m_RegLinearBuf[0x0010];
        m_pMemoryBuf[reg0010*0x8000+port-0x8000] = value;
    }
        
    return ret;
}
