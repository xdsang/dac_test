#pragma once
#include <WTYPES.H>
// 
#define MSLNK_SERIAL   0x00
#define MSLNK_USBHID   0x01
#define MSLNK_USB2I2C  0x02
#define MSLNK_VIRTUAL  0x03

//
#define TERR_OK                         0x00
#define TERR_ERROR                      0x01
#define TERR_UNSUPPORTED                0x02
#define TERR_INVALIDPARMS               0x03
#define TERR_MISALIGMENT                0x04
#define TERR_HARDWARENOTAVAILABLE       0x05
#define TERR_OUTOFRANGE                 0x06
#define TERR_UNSUPPORTEDFORMAT          0x07
#define TERR_HARDWAREBUSY               0x08
#define TERR_OPENFAILED                 0x09
#define TERR_CREATEFAILED               0x0A
#define TERR_LOSTFOCUS                  0x0B
#define TERR_NOPROCESS                  0x0C


#define TERR_TIMEOUT                    0x100
#define TERR_NOSYNCBYTE                 0x101
#define TERR_INVALIDCRC                 0x102
#define TERR_INVALIDLEN                 0x103

#define TERR_INVALIDHCAPTURE            0x600
#define TERR_NOVIDEOSIGNAL              0x601

#define TERR_INVALIDHOVERLAY            0x602

class CMSLinkU
{
public:
  CMSLinkU(void);
  ~CMSLinkU(void);

private:
  BYTE bI2CAddr;
  BYTE bConnectType;
  BOOL m_bIsConnected;
 
public:
  int  Connect();
  BOOL SetIICSpeed(BYTE mode);
  void SetConnect(BOOL);
  BOOL IsConnect();

  void SetI2CAddr(BYTE bAddr);
  void SetConnectType(BYTE bType);
  BYTE GetI2CAddr();
  BYTE GetConnectType();
  int i2cr(int DevID, int addr , unsigned char *pData);
  int i2cr(int DevID, int Addr , WORD repeat, unsigned char *pData);
  int i2cw(int DevID, int Addr);
  int i2cw(int DevID, int Addr, int Data);
  int i2cw(int DevID, int Addr, int Data,unsigned char mask);
  int i2cr16(int DevID, int addr , unsigned char *pData);
  int i2cr16(int DevID, int addr , WORD repeat, unsigned char *pData);
  int i2cw16(int DevID, int Addr, int Data);
  int i2cw16(int DevID, int Addr, int Data,unsigned char mask);
  int i2cw16(int DevID, int addr, WORD repeat, unsigned char *pData);
    
  /* low-level API interface */
  int ior(BYTE port, BYTE *pValue);
  int ior(BYTE port, WORD repeat, BYTE *pValue);

  int iow(BYTE port, BYTE value);
  int iow(BYTE port, BYTE value, BYTE mask);

  /* 16bit mode*/
  int ior16(WORD port, BYTE *pValue);
  int ior16(WORD port, WORD repeat, BYTE *pValue);
    
  int iow16(WORD port, BYTE value);
  int iow16(WORD port, BYTE value, BYTE mask);

  // serial
  void setSerialPort(BYTE port);
  void setBaudRate(int rate);
private:
  BYTE m_serPort;
  int m_baudRate;
};

extern CMSLinkU *puLnk;
