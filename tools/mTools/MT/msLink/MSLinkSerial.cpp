// MSLinkSerial.cpp: implementation of the CMSLinkSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MSLinkSerial.h"

extern CMSLinkU *puLnk;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMSLinkSerial::CMSLinkSerial()
{
  /* default is 2 sec timeout */
  setTimeOut(TLNK_DEFTIMEOUT);
  /* no error at all */
  errorCode     = 0;
  /* bit 0: 1 if enable error dlg, otherwise: silent mode */
  bFlags        = 1;
  connected     = FALSE;
  m_iBufferSize = 0;
  m_serPort = 1;
  m_baudRate = 9600;
}

CMSLinkSerial::~CMSLinkSerial()
{
    comm.disconnect();
}

/*
 * Set new timeout and return the current one.
 */
DWORD CMSLinkSerial::setTimeOut(DWORD to) {
    DWORD too = timeOut;
    timeOut = to;
    return too;
}

int CMSLinkSerial::getErrorCode() {
    return errorCode;
}

BOOL CMSLinkSerial::isConnected() {
    return connected;
}

int CMSLinkSerial::connect(char *szPort, int baud) {

    connected = FALSE;

    if (!comm.connect(szPort, baud, spNONE)) {
        /* 
         * Create COM connection is just half way, we need
         * to make sure it is connected.
         */
        BYTE    silent;
        BYTE    ver;
        int     result;
        silent = bFlags;
        bFlags = 0;
        result = version(&ver);
        bFlags = silent;
        if (result==0)
		{
            connected = TRUE;
			result = 1;
		}

        return result;
    }
    else
        return TERR_OPENFAILED;
}

int CMSLinkSerial::getByte(BYTE *pb) {
    DWORD timeSt = GetTickCount();
    DWORD timeSp;

    while (1) {
        if (comm.getNbrOfBytes()) {
            *pb = comm.getChar();
            return TERR_OK;
        }
        timeSp = GetTickCount();
        if ((timeSp - timeSt)> timeOut)
            break;
    }

    return TERR_TIMEOUT;
}

int CMSLinkSerial::getPk() {
        
    BYTE    crc;
    int     i;  
    
    GET_BYTE(inPk.bSync);   

    if (inPk.bSync != TLNK_SYNC) {
        //MessageBox(0, "No sync byte", "Error", MB_OK);
        return TERR_NOSYNCBYTE;
    }
    crc = inPk.bSync;
    GET_BYTE(inPk.bCmd);    
    crc += inPk.bCmd;
    GET_BYTE(inPk.bLen);
    if (inPk.bLen > ((m_iBufferSize > 0)?m_iBufferSize:TLNK_BUFSIZE)) {
        return TERR_INVALIDLEN;
    }
    crc += inPk.bLen;
    for (i=0; i< inPk.bLen; i++) {
        GET_BYTE(inPk.bData[i]);
        crc += inPk.bData[i];
    }
    GET_BYTE(inPk.bCrc);
    if (inPk.bCrc != crc) {
        return TERR_INVALIDCRC;
    }

    //filter
    return TERR_OK; 
}

/*
 * return err
 */
int CMSLinkSerial::processError(int err) {
    CString c;
    
    errorCode = err;

    switch (err) {
    case TERR_OK:
        if (inPk.bCmd == TLNKCMD_NACK) {
          c.Format(_T("NAck"));
            break;
        }
        else if ((inPk.bCmd == TLNKCMD_ACK) && inPk.bLen) {
            if (inPk.bData[0]) {
              c.Format(_T("Ack with error code %d"), inPk.bData[0]);
                break;
            }
        }
        return 0;

    case TERR_TIMEOUT:
      c.Format(_T("inPk: time out"));
        break;

    case TERR_INVALIDCRC:
      c.Format(_T("inPk: invalid CRC"));
        break;

    case TERR_INVALIDLEN:
      c.Format(_T("inPk: invalid PCL len"));
        break;

    case TERR_NOSYNCBYTE:
      c.Format(_T("inPk: no Sync byte detect"));
        break;

    default:
      c.Format(_T("Unknow error"));
        break;
    }
    if (bFlags & 1) {
        //MessageBox(0, "Error","aa",MB_OK);
        AfxMessageBox(c, MB_OK);
    }
    return err;
}

void CMSLinkSerial::clearInBuffer() {
    while (comm.getNbrOfBytes()) {
        comm.getChar();
    }
}

int CMSLinkSerial::i2cr(BYTE addr, BYTE subaddr, BYTE *pValue) {
    /* use general I2C APIs */
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CR;
    outPk.bLen  = 4;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = subaddr;

    crc  = TLNK_SYNC + TLNKCMD_GENI2CR + 4;
    crc += addr;
    crc += subaddr;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
    outPk.bData[4] = crc;
    comm.sendArray((char*)&outPk, 8);   
    if(connected) {
        GET_PK();
        *pValue++ = inPk.bData[1];
    }
	Sleep(10); //usb2iic 卡 需要delay，否则出错，但在其它方式串口线上不需要delay。
    return 0; 
};

int CMSLinkSerial::i2cw(BYTE addr, BYTE subaddr, BYTE value) {
    /* use general I2C APIs */

    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CW;
    outPk.bLen  = 5;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = subaddr;
    outPk.bData[4] = value;

    crc  = TLNK_SYNC + TLNKCMD_GENI2CW + 5;
    crc += addr;
    crc += subaddr;
    crc += value;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;

    outPk.bData[5] = crc;
    if(connected) {
        comm.sendArray((char*)&outPk, 9);       
        GET_PK();
    }
    return 0;
};

// For 16bit address
int CMSLinkSerial::i2cr16(BYTE addr, WORD port, BYTE *pVal) {
    //TODO
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CR16;
    outPk.bLen  = 5;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = (BYTE)port;
	outPk.bData[4] = (BYTE)(port>>8);

    crc  = TLNK_SYNC + TLNKCMD_GENI2CR16 + 5;
    crc += addr;
    crc += (BYTE)port + (BYTE)(port>>8);
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
    outPk.bData[5] = crc;
    comm.sendArray((char*)&outPk, 9);   
    if(connected) {
        GET_PK();
        *pVal++ = inPk.bData[1];
    }
	Sleep(10); //usb2iic 卡 需要delay，否则出错，但在其它方式串口线上不需要delay。
    return 0;
};


int CMSLinkSerial::i2cw16(BYTE addr, WORD port, BYTE value) {
    //TODO
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CW16;
    outPk.bLen  = 6;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = (BYTE)port;
	outPk.bData[4] = (BYTE)(port>>8);
    outPk.bData[5] = value;

    crc  = TLNK_SYNC + TLNKCMD_GENI2CW16 + 6;
    crc += addr;
    crc += (BYTE)port + (BYTE)(port>>8);
    crc += value;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;

    outPk.bData[6] = crc;
    if(connected) {
        comm.sendArray((char*)&outPk, 10);       
        GET_PK();
    }
    return 0;
};


/* Export APIs */
int CMSLinkSerial::ior(BYTE port, BYTE *pVal) {
    /* use general I2C APIs */
    BYTE        crc     = 0;
	BYTE addr = puLnk->GetI2CAddr();

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CR;
    outPk.bLen  = 4;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = port;
	
    crc  = TLNK_SYNC + TLNKCMD_GENI2CR + 4;
    crc += addr;
    crc += port;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
    outPk.bData[4] = crc;
    comm.sendArray((char*)&outPk, 8);   
    if(connected) {
        GET_PK();
        *pVal++ = inPk.bData[1];
    }
    return 0; 
};


int CMSLinkSerial::ior(BYTE port, BYTE repeat, BYTE *pValue) {
    BYTE        crc     = 0;
    int         i;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_IOR;
    outPk.bLen  = 2;
    outPk.bData[0] = (BYTE)port;   
    outPk.bData[1] = repeat;
    crc = TLNK_SYNC + TLNKCMD_IOR + 2;
    crc += (BYTE)port;
    crc += repeat;
    outPk.bData[2] = crc;

    if(connected) {
        comm.sendArray((char*)&outPk, 6);
        GET_PK();
        for (i=0; i<repeat; i++)
            *pValue++ = inPk.bData[1+i];
    }
    return 0;
}

int CMSLinkSerial::iow(BYTE port, BYTE value) {
    /* use general I2C APIs */
	
    BYTE        crc     = 0;
	BYTE addr = puLnk->GetI2CAddr();
	
    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CW;
    outPk.bLen  = 5;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = port;
    outPk.bData[4] = value;
	
    crc  = TLNK_SYNC + TLNKCMD_GENI2CW + 5;
    crc += addr;
    crc += port;
    crc += value;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
	
    outPk.bData[5] = crc;
    if(connected) {
        comm.sendArray((char*)&outPk, 9);       
        GET_PK();
    }
    return 0;
};

// For 16bit address
int CMSLinkSerial::ior16(WORD port, BYTE *pVal) {
    BYTE        crc     = 0;
	BYTE addr = puLnk->GetI2CAddr();
	
    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CR16;
    outPk.bLen  = 5;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = (BYTE)port;
	outPk.bData[4] = (BYTE)(port>>8);
	
    crc  = TLNK_SYNC + TLNKCMD_GENI2CR16 + 5;
    crc += addr;
    crc += (BYTE)port + (BYTE)(port>>8);
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
    outPk.bData[5] = crc;
    comm.sendArray((char*)&outPk, 9);   
    if(connected) {
        GET_PK();
        *pVal++ = inPk.bData[1];
    }
    return 0;
};


int CMSLinkSerial::ior16(WORD port, BYTE repeat, BYTE *pValue) {
    BYTE        crc     = 0;
    int         i;
	
    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_IOR;
    outPk.bLen  = 3;
    outPk.bData[0] = port>>8;
    outPk.bData[1] = (BYTE)port;   
    outPk.bData[2] = repeat;
    crc = TLNK_SYNC + TLNKCMD_IOR + 3;
    crc += (BYTE)port;
    crc += (BYTE)(port>>8);
    crc += repeat;
    outPk.bData[3] = crc;
	
    if(connected) {
        comm.sendArray((char*)&outPk, 7);
        GET_PK();
        for (i=0; i<repeat; i++)
            *pValue++ = inPk.bData[1+i];
    }
    return 0;
}

int CMSLinkSerial::iow16(WORD port, BYTE value) {
    BYTE        crc     = 0;
	BYTE addr = puLnk->GetI2CAddr();
	
    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GENI2CW16;
    outPk.bLen  = 6;
    outPk.bData[0] = I2CRWF_RANDOMACCESS;
    outPk.bData[1] = 1;
    outPk.bData[2] = addr;
    outPk.bData[3] = (BYTE)port;
	outPk.bData[4] = (BYTE)(port>>8);
    outPk.bData[5] = value;
	
    crc  = TLNK_SYNC + TLNKCMD_GENI2CW16 + 6;
    crc += addr;
    crc += (BYTE)port + (BYTE)(port>>8);
    crc += value;
    crc += 1;
    crc += I2CRWF_RANDOMACCESS;
	
    outPk.bData[6] = crc;
    if(connected) {
        comm.sendArray((char*)&outPk, 10);       
        GET_PK();
    }
    return 0;
};


int CMSLinkSerial::delay(WORD ms) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_DELAY;
    outPk.bLen  = 2;
    outPk.bData[0] = ms>>8;
    outPk.bData[1] = ms & 0xFF; 

    crc  = TLNK_SYNC + TLNKCMD_DELAY + 2;
    crc += ms>>8;
    crc += ms&0xFF;
    outPk.bData[2] = crc;
    if(connected) {
        comm.sendArray((char*)&outPk, 6);       
        GET_PK();
    }
    return 0;
};

/* high-level APIs */
int CMSLinkSerial::version(BYTE *ver) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_GETVERSION;
    outPk.bLen  = 0;
    crc = TLNK_SYNC + TLNKCMD_GETVERSION;
    outPk.bData[0] = crc;

    comm.sendArray((char*)&outPk, 4);
    GET_PK();
    *ver = inPk.bData[1];
    return 0;
};

void CMSLinkSerial::disconnect()
{
    comm.disconnect();
    connected = FALSE;
}

Tserial *CMSLinkSerial::GetCommCtrl()
{
    return &comm;
}

int CMSLinkSerial::ppr(BYTE port, BYTE *pValue) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_PORTR;
    outPk.bLen  = 1;
    outPk.bData[0] = port;
#if 1
    CalCRC();
#else
    crc = TLNK_SYNC + TLNKCMD_PORTR + 1;
    crc += port;
    outPk.bData[1] = crc;
#endif
    if(connected) {
        comm.sendArray((char*)&outPk, 5);
        GET_PK();
        *pValue = inPk.bData[1];
    }
    return 0;
}

int CMSLinkSerial::ppr(BYTE port, BYTE pin, BYTE *pValue) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_PORTR;
    outPk.bLen  = 2;
    outPk.bData[0] = port;
    outPk.bData[1] = pin;
#if 1
    CalCRC();
#else
    crc = TLNK_SYNC + TLNKCMD_PORTR + 2;
    crc += port;
    crc += pin;
    outPk.bData[2] = crc;
#endif
    if(connected) {
        comm.sendArray((char*)&outPk, 6);
        GET_PK();
        *pValue = inPk.bData[1];
    }
    return 0;
}

int CMSLinkSerial::ppw(BYTE port, BYTE value) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_PORTW;
    outPk.bLen  = 2;
    outPk.bData[0] = port;
    outPk.bData[1] = value;
#if 1 
    CalCRC();
#else
    crc = TLNK_SYNC + TLNKCMD_PORTW + 2;
    crc += port;
    crc += value;
    outPk.bData[2] = crc;
#endif
    if(connected) {
        comm.sendArray((char*)&outPk, 6);
        GET_PK();
    }
    return 0;
}

int CMSLinkSerial::ppw(BYTE port, BYTE pin, BYTE value) {

    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_PORTW;
    outPk.bLen  = 3;
    outPk.bData[0] = port;
    outPk.bData[1] = pin;
    outPk.bData[2] = value;

#if 1
    CalCRC();
#else
    crc = TLNK_SYNC + TLNKCMD_PORTW + 3;
    crc += port;
    crc += pin;
    crc += value;

    outPk.bData[3] = crc;
#endif
    if(connected) {
        comm.sendArray((char*)&outPk, 7);
        GET_PK();
    }
    return 0;
}

int CMSLinkSerial::GetBufferSize(int &iSize) {
    BYTE        crc     = 0;

    clearInBuffer();
    outPk.bSync = TLNK_SYNC;
    outPk.bCmd  = TLNKCMD_QUERYBUFSIZE;
    outPk.bLen  = 0;
    CalCRC();

    comm.sendArray((char*)&outPk, 4);
    GET_PK();
    iSize = inPk.bData[1];
    m_iBufferSize = iSize;
    return 0;
}

void CMSLinkSerial::CalCRC() {
    BYTE crc = 0;
    int  i   = 0;
    crc = outPk.bSync + outPk.bCmd + outPk.bLen;
    for(;i<outPk.bLen;i++) {
        crc += outPk.bData[i];
    }
    outPk.bData[outPk.bLen] = crc;
}

BYTE CMSLinkSerial::getCurSerialPort()
{
  return m_serPort;
}

int CMSLinkSerial::getCurBaudRate()
{
  return m_baudRate;
}

BOOL CMSLinkSerial::openPort(BYTE port, int rate)
{
  m_serPort = port;
  m_baudRate = rate;
  char pName[12];
  sprintf_s(pName, "COM%d", m_serPort);
  m_isSpOpen = comm.connect(pName, m_baudRate, spNONE);
  return m_isSpOpen;
}

void CMSLinkSerial::closePort()
{
  comm.disconnect();
}

BOOL CMSLinkSerial::isSerPortOpen()
{
  return m_isSpOpen;
}

BOOL CMSLinkSerial::isExistPort(BYTE port)
{
  if(port == m_serPort && m_isSpOpen) 
    return true;
  BOOL isExist = FALSE;
  char pName[12];
  sprintf_s(pName, "COM%d", port);
  if (!comm.connect(pName, m_baudRate, spNONE)) {
    isExist = TRUE;
  }
  return isExist;
}

