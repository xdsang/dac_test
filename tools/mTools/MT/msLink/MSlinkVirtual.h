#pragma once

#include "MSLnkU.h"

class CMSLinkVirtual
{
public:
    CMSLinkVirtual(void);

public:
    ~CMSLinkVirtual(void);
private:
    BYTE m_RegLinearBuf[0x10000];
	BYTE m_RegBankBuf[256*64];
	BYTE m_bBank;
    BYTE *m_pMemoryBuf;

public:
	int i2cr(BYTE DevID, BYTE Addr , BYTE *pData);
	int i2cw(BYTE DevID, BYTE Addr, BYTE Data);
    
    int i2cr16(BYTE DevID, WORD Addr , BYTE *pData);
    int i2cr16(BYTE DevID, WORD port, WORD repeat, BYTE *pValue);
    int i2cw16(BYTE DevID, WORD Addr, BYTE Data);
    int i2cw16(BYTE DevID, WORD port, WORD repeat, BYTE *pValue);

    /* low-level API interface */
    int     ior(BYTE port, BYTE *pValue);
    int     ior(BYTE port, WORD repeat, BYTE *pValue);
    int     iow(BYTE port, BYTE value);

    int     ior16(WORD port, BYTE *pValue);
    int     ior16(WORD port, WORD repeat, BYTE *pValue);
    int     iow16(WORD port, BYTE value);
};