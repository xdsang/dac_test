#ifndef  _INST_H
#define  _INST_H

#include "visa.h"
#include "visatype.h"
#include <string.h>

/************************************************************************/
/* LNA class                                                           */
/************************************************************************/

class LNA
{
public:
	CString	m_csCommand;
	char	m_strOutput[1024];
	char	m_strAddress[256];
	bool	m_need_delay;
	int		m_delay_ms;
public:
	LNA();
	void	setAddress(char *);
	bool	checkInstr();
	bool	setParam();
	bool	getParam();

	void	setDelay(bool need_delay, int delay_ms);
};


/************************************************************************/
/* Digital Multimeter                                                   */
/************************************************************************/

class DM : public LNA
{
private:

public:
	DM();
	double  getVoltage();
};

#endif
#pragma once
