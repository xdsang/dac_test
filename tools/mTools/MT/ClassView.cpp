
#include "stdafx.h"
#include "MainFrm.h"
#include "ClassView.h"
#include "Resource.h"
#include "MT.h"
#include"dependency\MSRegisterCheck\MSRegisterCheck.h"

extern bool gFlagLogR;
extern bool gFlagLogW;
extern DWORD_PTR click_table_index;

CMFCPropertyGridCtrl m_wndPropList;
PropertyGridProperty* m_pRegister;
CParseTXT m_parsetxt;

CMSRegisterCheck g_CmsRegCheck;

typedef struct _T_THREAD_PARAME_REGRW_
{
	CMSRegisterCheck*		pCmsRegCheck;
}THREADPARAMEREGRWP_T;

//////////////////////////////////////////////////////////////////////
// 构造/析构
//////////////////////////////////////////////////////////////////////

CMyEdit::~CMyEdit()
{
}

CClassView::CClassView()
{
}

CClassView::~CClassView()
{
}

//消息映射
BEGIN_MESSAGE_MAP(CClassView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_SEARCH, &CClassView::OnPackUp)
	ON_COMMAND(ID_TESTRW_BTN, &CClassView::OnTestRW)
	ON_COMMAND(ID_REFRESH_BTN, &CClassView::OnRefresh)
	ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)  //属性表格消息
//	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClassView 消息处理程序
int CClassView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//停靠窗口的创建是否成功
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty(); 

	// 创建视图: 
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	//创建GridProperty
	if (!m_wndPropList.Create(WS_VISIBLE | WS_CHILD | WS_BORDER, rectDummy, this, ID_PROPLIST))
	{
		TRACE0("未能创建属性网格\n");
		return -1;      // 未能创建
	}

	// 修改字体
	m_Font.CreateFont(
		m_LabelFontSize, 0, 0, 0,
		FW_NORMAL, FALSE, FALSE, 0,
		0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH, m_LabelFontName);

	load_txt_temp();

	//创建工具栏
	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_EDITBOX);
	m_wndToolBar.LoadToolBar(IDR_EDITBOX, 0, 0, TRUE /* 已锁定*/);

	//添加搜索框(在工具栏分隔符中添加)
	int index = 0;// 分隔符索引
	index = m_wndToolBar.CommandToIndex(ID_NULL);
	int width =178;// 分隔符宽度
	m_wndToolBar.SetButtonInfo(1, ID_NULL, TBBS_SEPARATOR, width);
	
	m_wndToolBar.GetItemRect(index, &m_rect_search);
	m_rect_search.right =200;
	m_edit.Create(WS_VISIBLE | WS_TABSTOP | WS_BORDER | WS_CHILD, m_rect_search, &m_wndToolBar, IDD_SEARCH_EDIT);
	m_edit.SetWindowText(_T("")); //添加编辑框(在工具栏分隔符中添加)

	// 添加刷新按钮
	int index_t = 0;
	index_t = m_wndToolBar.CommandToIndex(ID_CLASS_ADD_MEMBER_FUNCTION);
	int width_t = 10; //分隔符宽度
	m_wndToolBar.SetButtonInfo(3, ID_CLASS_ADD_MEMBER_FUNCTION, TBBS_SEPARATOR, width_t);
	m_wndToolBar.GetItemRect(index_t, &m_rect_refresh);
	m_rect_refresh.left = 230;
	m_rect_refresh.right = 290;
	m_btn_refresh.Create(_T("Refresh"), WS_VISIBLE | BS_DEFPUSHBUTTON, m_rect_refresh, &m_wndToolBar, ID_REFRESH_BTN);

	// 添加Test按钮
	index_t = m_wndToolBar.CommandToIndex(ID_CLASS_ADD_MEMBER_FUNCTION);
	m_wndToolBar.SetButtonInfo(4, ID_CLASS_ADD_MEMBER_FUNCTION, TBBS_SEPARATOR, width_t);
	m_wndToolBar.GetItemRect(index_t, &m_rect_testrw);
	m_rect_testrw.left = 300;
	m_rect_testrw.right = 360;
	m_btn_testrw.Create(_T("TestRW"), WS_VISIBLE | BS_DEFPUSHBUTTON, m_rect_testrw, &m_wndToolBar, ID_TESTRW_BTN);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// 所有命令将通过此控件路由，而不是通过主框架路由: 
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	//设置字体
	UpdateFonts();
	//调整页面布局
	AdjustLayout();
	return 0;
}

//调整页面布局
void CClassView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);  //获取窗口客户区的大小,排除标题栏和工具栏

	int height = rectClient.Height() / 3;
	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	//m_edit.SetWindowPos(NULL, rectClient.left, rectClient.top, (int)(rectClient.Width() *0.33), m_rect_search.Height(), SWP_NOACTIVATE | SWP_NOZORDER);
	//调节工具栏1的尺寸位置
	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	//调节Grid列表框的尺寸位置
	m_wndPropList.SetWindowPos(NULL, rectClient.left, rectClient.top + cyTlb + 1, rectClient.Width(), rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}


void CClassView::turn_on_log_print()
{
	gFlagLogR = true;
	gFlagLogW = true;
}

void CClassView::turn_off_log_print()
{
	gFlagLogR = false;
	gFlagLogW = false;
}

void CClassView::load_txt_temp()
{
	//解析 TXT
	char filepath[] = "./as7160a_reg.txt";
	FILE *fp_in = fopen(filepath, "r");
	if (fp_in == NULL)
	{
		//exit(1);
	}
	else
	{
		//关闭Log,否则程序执行异常,估计是Log过大
		m_parsetxt.parse_txt_file(fp_in);
		InitPropList();
		fclose(fp_in);
	}
}

void CClassView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

//设置字体
void CClassView::UpdateFonts()
{
	m_edit.SetFont(&m_Font);  //设置工具栏编辑框字体
	//m_btn_refresh.SetFont(&m_Font);
	m_wndPropList.SetFont(&m_Font);
}


void CClassView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rectClient;
	GetClientRect(rectClient);
	dc.FillSolidRect(rectClient, RGB(255, 255, 255));
}

void CClassView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndPropList.SetFocus();
}


BOOL CClassView::PreTranslateMessage(MSG* pMsg)
{
	if (WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST)
	{
		CWnd *pWnd = GetFocus();
		int focusID = pWnd->GetDlgCtrlID();
			
		if (focusID == IDD_SEARCH_EDIT)
		{
			UpdateData(TRUE);
			do_search();
		}
	}
	return CDockablePane::PreTranslateMessage(pMsg);  //必须将其他消息路由回基类!!!
}

//搜索操作
void CClassView::do_search()
{
	// TODO: 在此添加命令处理程序代码
	BOOL b_find = FALSE;
	int search_index = 0;
	m_edit.GetWindowTextW(m_edit_content);

	// CString -> string
	USES_CONVERSION;
	string str_search(W2A(m_edit_content));
	string::size_type idx;

	int group_count = m_wndPropList.GetPropertyCount();
	int group_index = 0;

	while (group_index < group_count)  //循环搜索group
	{
		m_module_group = (PropertyGridProperty*)m_wndPropList.GetProperty(group_index);
		int subitem_index = 0;
		int item_count = m_module_group->GetSubItemsCount();
		while (subitem_index < item_count)  //循环搜索group's items
		{
			m_pRegister = (PropertyGridProperty*)m_module_group->GetSubItem(subitem_index);
			CString subitem_name_cstr = m_pRegister->GetName();
			string subitem_name_str(W2A(subitem_name_cstr));
			idx = subitem_name_str.find(str_search);
			if (idx != string::npos)  //找到包含str_search字符串的寄存器名称字符串
			{
				b_find = TRUE;
				m_pRegister->Show(TRUE, FALSE);  // disable adjustlayout function
			}
			else
			{
				m_pRegister->Show(FALSE, FALSE);
			}
			subitem_index++;
		}
		/*不包含搜索关键字的项目组也隐藏起来*/
		if (b_find == FALSE)
		{
			m_module_group->Show(FALSE, FALSE);
		}
		else
		{
			b_find = FALSE;
			m_module_group->Show(TRUE, FALSE);
		}
		group_index++;
	}
	m_wndPropList.ExpandAll(TRUE);
}

//响应收起列表消息
void CClassView::OnPackUp()
{
	m_wndPropList.ExpandAll(FALSE);
}

void outputaddr(FILE *fp, WORD addr)
{
	if (addr < 0x10)
	{
		fprintf(fp, "0x000%X", addr);
	}
	else if (addr < 0x100)
	{
		fprintf(fp, "0x00%X", addr);
	}
	else if (addr < 0x1000)
	{
		fprintf(fp, "0x0%X", addr);
	}
	else 
	{
		fprintf(fp, "0x%X", addr);
	}
	fprintf(fp, " ");
}

BOOL CClassView::GeneratTxtForTest()
{
	WORD sel_addr;
	BYTE sel_st_bit;
	int sel_length;
	LONG64 sel_value;
	string sel_attr;

	int group_count = m_wndPropList.GetPropertyCount();
	int group_index = 0;

	char filepath[] = "./regsiter.txt";
	FILE *fp = fopen(filepath, "w+");

	if (fp == NULL)
	{
		MessageBox(_T("[RWTest] Can not create temp file!"));
		return FALSE;
	}

	while (group_index < group_count)  //循环搜索group
	{
		m_module_group = (PropertyGridProperty*)m_wndPropList.GetProperty(group_index);
		int subitem_index = 0;
		int item_count = m_module_group->GetSubItemsCount();
		
		//fprintf(fp, "#%s\n", m_module_group->GetName());
		while (subitem_index < item_count)  //循环搜索group's items
		{
			m_pRegister = (PropertyGridProperty*)m_module_group->GetSubItem(subitem_index);
			DWORD register_number = m_pRegister->GetData();
			sel_addr = m_parsetxt.reg_addr[register_number];
			sel_st_bit = m_parsetxt.reg_st[register_number];
			sel_length = m_parsetxt.reg_length[register_number];
			sel_value = m_parsetxt.reg_value[register_number];
			sel_attr = m_parsetxt.reg_attribute[register_number];

			while (sel_length > 16)
			{
				fprintf(fp, "MSREG ");
				outputaddr(fp, sel_addr);
				if (!strcmp(sel_attr.data(), "RO/WO"))
				{
					sel_attr = "NA";
				}
				fprintf(fp, "%s ", sel_attr.data());
				fprintf(fp, "%2d ", sel_st_bit);
				fprintf(fp, "16 ");
				outputaddr(fp, (WORD)(sel_value & 0xFFFF));
				sel_addr += 2;
				sel_length -= 16;
				sel_value >>= 16;
				fprintf(fp, "\n");
			}

			if (sel_length)
			{
				fprintf(fp, "MSREG ");
				outputaddr(fp, sel_addr);
				if (sel_attr == "RO/WO")
				{
					sel_attr = "NA";
				}
				fprintf(fp, "%s ", sel_attr.data());
				fprintf(fp, "%2d ", sel_st_bit);
				fprintf(fp, "%2d ", sel_length);
				outputaddr(fp, (WORD)(sel_value & 0xFFFF));
				fprintf(fp, "\n");
			}
			subitem_index++;
		}
		group_index++;
	}

	fclose(fp);
	fp = NULL;
	return TRUE;
}

DWORD WINAPI Proc_register_check(PVOID pParam)
{
	THREADPARAMEREGRWP_T *	pInfo = (THREADPARAMEREGRWP_T *)pParam;
	CMSRegisterCheck*		CmsRegCheck = (CMSRegisterCheck *)pInfo->pCmsRegCheck;

	UINT16 u16_regMapLen = CmsRegCheck->LoadRegisterMap();

	CmsRegCheck->CheckDefault();
	CmsRegCheck->CheckRW();
	CmsRegCheck->LogErr();
	CmsRegCheck->SetCheckState(0);

	return 0;
}


void CClassView::OnTestRW()
{
	if (m_b_testrw)
	{
		g_CmsRegCheck.SetCheckState(0);
		KillTimer(0);
		CString str;
		str.Format(_T("%3d %%"), 0);
		SetDlgItemText(IDC_STATIC_REG_RW_RATIO, str);
		UpdateData(TRUE);

		Log(_T("[RegisterRWTest] Stopped"));

		m_btn_testrw.SetWindowText(_T("TestRW"));
		m_b_testrw = false;
	}
	else
	{
		if (!GeneratTxtForTest()) return;

		Log(_T("[RegisterRWTest] Start...."));
		Log(_T("[RegisterRWTest] Press button again to stop"));
		Log(_T("[RegisterRWTest] Progress ... "));

		m_btn_testrw.SetWindowText(_T("Stop"));
		m_b_testrw = true;

		HANDLE handle = NULL;
		static THREADPARAMEREGRWP_T st_ThreadParam;
		CString strFilename;
		CString strReportFileName;

		strFilename.Format(_T("regsiter.txt"));
		strReportFileName.Format(_T("regsiter_check_log.txt"));

		CMSRegisterCheck CmsRegCheck(strFilename, strReportFileName);

		g_CmsRegCheck = CmsRegCheck;
		g_CmsRegCheck.SetCheckState(0);

		st_ThreadParam.pCmsRegCheck = &g_CmsRegCheck;
		g_CmsRegCheck.SetCheckState(1);
		handle = CreateThread(NULL, 0, Proc_register_check, &st_ThreadParam, 0, NULL);
		SetTimer(0, 200, 0);

		CloseHandle(handle);
	}
	
}

void CClassView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your control notification handler code here
	UINT16 u16_i;
	UINT16 u16_j;
	UINT16 u16_k;

	UINT16 u16RegTotalCount = 0;

	u16_i = g_CmsRegCheck.GetDefaultCheckCount();
	u16_j = g_CmsRegCheck.GetRWCheckCount();

	u16RegTotalCount = g_CmsRegCheck.GetRegTotalCount();
	if (u16RegTotalCount == 0)
	{
		u16RegTotalCount = 1;
	}
	u16_k = 100 * (u16_i + u16_j) / 2 / (u16RegTotalCount);
	CString str;
	if (u16_k > 100) u16_k = 100;
	str.Format(_T("[RegisterRWTest] Progress ... %3d %%"), u16_k);

	modifyLatestLog(str);

	UpdateData(FALSE);

	if (g_CmsRegCheck.GetCheckState() == 0)
	{
		KillTimer(0);
		m_b_testrw = false;

		Log(_T("[RegisterRWTest] Done"));
		m_btn_testrw.SetWindowText(_T("TestRW"));
	}

	CWnd::OnTimer(nIDEvent);
}


//刷新寄存器操作响应
void CClassView::OnRefresh()
{
	UINT32 current_value = 0;
	WORD sel_addr;
	BYTE sel_st_bit;
	int sel_length;
	if (click_table_index != 2000)
	{
		//读取所选项目的寄存器的实时数值
		m_pRegister = (PropertyGridProperty*)m_wndPropList.FindItemByData(click_table_index);
		sel_addr = m_parsetxt.reg_addr[click_table_index];
		sel_st_bit = m_parsetxt.reg_st[click_table_index];
		sel_length = m_parsetxt.reg_length[click_table_index];
		current_value = HAL_ReadRange(sel_addr, sel_st_bit, sel_length);
		m_pRegister->SetValue((_variant_t)current_value);
		click_table_index = 2000;
	}
	//刷新整个Table.该操作在第一次Load时需要进行,因为I2C未连接时,读取的寄存器数值是随机值,需要刷新整个列表
	else if (click_table_index == 2000)
	{
		MessageBox(_T("Register table updating, please wait..."));
		if (gFlagLogR && gFlagLogW)
		{
			turn_off_log_print(); //关闭Log,提升刷新速度
		}

		int group_count = m_wndPropList.GetPropertyCount();
		int group_index = 0;

		while (group_index < group_count)  //循环搜索group
		{
			m_module_group = (PropertyGridProperty*)m_wndPropList.GetProperty(group_index);
			int subitem_index = 0;
			int item_count = m_module_group->GetSubItemsCount();
			while (subitem_index < item_count)  //循环搜索group's items
			{
				m_pRegister = (PropertyGridProperty*)m_module_group->GetSubItem(subitem_index);
				DWORD register_number = m_pRegister->GetData();
				sel_addr = m_parsetxt.reg_addr[register_number];
				sel_st_bit = m_parsetxt.reg_st[register_number];
				sel_length = m_parsetxt.reg_length[register_number];
				current_value = HAL_ReadRange(sel_addr, sel_st_bit, sel_length);  //获取寄存器实时数值
				m_pRegister->SetValue((_variant_t)current_value);
				subitem_index++;
			}
			group_index++;
		}
		//turn_on_log_print();

		Log(_T("Register table update completed!"));
	}
}

//初始化属性列表框
void CClassView::InitPropList()
{
	/***************************列表基本设设置******************************/
	//表头使能控制,FALSE- 无表头
	m_wndPropList.EnableHeaderCtrl(TRUE, _T("Register"), _T("Control"));
	//属性栏描述窗口,默认 TRUE-使能描述窗口(区域)
	m_wndPropList.EnableDescriptionArea();
	//增大描述区域的初始高度
	m_wndPropList.SetDescriptionRows(8);
	m_wndPropList.SetVSDotNetLook();
	m_wndPropList.MarkModifiedProperties();
	
	/*****************************添加列表项*******************************/
	// 模块组编号
	int group_index = 0;
	int reg_number = 0;
	CString description_Format = _T("Description:\n\nAttribute:     %s\nAddress:       0X%lX[%d]\nLength:        %d\nDefault Value: 0X%X\nNote:\n%s\n");
	while (m_parsetxt.module_name[group_index] != "\0")  //获取的模块名称存在
	{
		m_group_name = m_parsetxt.module_name[group_index].c_str();
		m_module_group = new PropertyGridProperty(m_group_name, 1700, 0);

		// 创建各个分组的子项目(各组模块所包含的寄存器)
		while (m_parsetxt.reg_module_name[reg_number] == m_parsetxt.module_name[group_index])  //判断寄存器是否属于该模块
		{
			m_reg_address = m_parsetxt.reg_addr[reg_number];
			m_reg_name = m_parsetxt.reg_name[reg_number];
			m_reg_description = m_parsetxt.reg_description[reg_number];
			m_reg_length = m_parsetxt.reg_length[reg_number];
			m_reg_attribute = m_parsetxt.reg_attribute[reg_number];
			m_reg_start_bit = m_parsetxt.reg_st[reg_number];

			// 在load的时候读取寄存器的数值,切记需要关闭在Log窗口的打印,否则程序会中断
			//m_reg_current_value = HAL_ReadRange(m_reg_address, m_reg_start_bit, m_reg_length);  //get register current value
			m_value_field = (int)m_parsetxt.reg_value[reg_number];
			// string -> CString
			m_str_attribute = m_reg_attribute.c_str();
			m_str_name_field = m_reg_name.c_str();
			m_str_description_convert = m_reg_description.c_str();
			// set spin range
			m_spin_range = pow(2, m_reg_length) - 1;
			// set register description
			m_str_description_field.Format(description_Format, m_str_attribute, m_reg_address, m_reg_start_bit, m_reg_length, m_value_field, m_str_description_convert);
			// create register table
			m_pRegister = new PropertyGridProperty(m_str_name_field, (_variant_t)m_value_field, m_str_description_field, reg_number);
			m_pRegister->EnableSpinControl(TRUE, 0, m_spin_range);
			m_module_group->AddSubItem(m_pRegister);
			reg_number++;
		}

		m_wndPropList.AddProperty(m_module_group);
		m_wndPropList.ExpandAll(FALSE);  // do not expand all property Grids
		group_index++;
	}
}

LRESULT CClassView::OnPropertyChanged(WPARAM wParam, LPARAM lParam)
{
	//在此处设置数值与寄存器联通
	PropertyGridProperty* pProp = (PropertyGridProperty*)lParam;
	pProp->UpdateValue();
	CString sname = pProp->GetName();  //被改变的参数名
	COleVariant value = pProp->GetValue(); //改变之后的值
	value.ChangeType(VT_UINT);
	DWORD i = value.uintVal;
	CString t2 = pProp->GetOriginalValue().bstrVal;  //改变之前的值
	m_reg_num = pProp->GetData();
	HAL_WriteRange(m_parsetxt.reg_addr[m_reg_num], m_parsetxt.reg_st[m_reg_num], m_parsetxt.reg_length[m_reg_num], i);

	return 0;
}


//HBRUSH CClassView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
//{
//	HBRUSH hbr = CDockablePane::OnCtlColor(pDC, pWnd, nCtlColor);
//
//	// TODO:  在此更改 DC 的任何特性
//	int id = pWnd->GetDlgCtrlID();
//	if (id == ID_PROPLIST || id == ID_REFRESH_BTN)
//	{
//		pDC->SetTextColor(RGB(0, 0, 0));//设置文本颜色为黑色
//		return CreateSolidBrush(RGB(255, 255, 255));
//	}
//	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
//	return hbr;
//}

