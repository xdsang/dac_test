// CMS1858Register.cpp: implementation of the CMS1858Register class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "TxtProc16.h"
#include "MSLnkU.h"
#include "mTools.h"
#include "MS1858Register.h"
#include "IoCmd.h"
#include "CParseTXT.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//extern CTxtproc16 txtProc16;
extern CMSLinkU *puLnk;
extern CIOCmd g_IoCmd;

CMS1858Register MS1858REG;
CMSRegister *pMSREG = &MS1858REG;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static void print_comment(char* comment, FILE* fp)
{
    int i = 75;
    
    while (i) {
        fprintf(fp, " ");
        i--;
    }
    fprintf(fp, "%s\n", comment);
}

CMS1858Register::CMS1858Register()
{
}

CMS1858Register::~CMS1858Register()
{
}

BYTE CMS1858Register::get_bank()
{
    return (0);
}

static int write_bat_segment(FILE* fp, int start, int end, char *name)
{
    int i;
    BYTE temp;
    
    fprintf(fp, "\n%s\n", name);

    for(i=start; i<=end; i++)
    {
        puLnk->i2cr16(puLnk->GetI2CAddr(), i, &temp);
        //trriger bit, set 0
        // removed...

        fprintf(fp, "i2crw16 %02x %04x %02x\n", puLnk->GetI2CAddr(), i, temp);
    }
    
    return (end + 1 - start);
}

TCHAR * CMS1858Register::get_chipname()
{
    return _T("ms1858");
}

void CMS1858Register::save_bat(FILE* fp)
{
    fprintf(fp, "# ms1858(CM8858A) register table\n");
    m_parsetxt.save_bat_file(fp, "CM8858A");

    fprintf(fp, "\n");
    fprintf(fp, "# triggers\n");
    //trriger bit, set 1
    //adpllad_lat                     1         1         RW      014e[0] # addpll trigger bit
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x014e, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x014e, 1, 0x01);
    //pllvm_trigger                   0         1         RW      018e[0] # PLLVM Trigger bit.
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x018e, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x018e, 1, 0x01);
    //trig_enable                     0         1         RW      01cb[0] # PLLV trigger bit
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x01cb, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x01cb, 1, 0x01);
    //pllvm_trigger                   0         1         RW      010e[0] # PLLVM Trigger bit.
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x010e, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x010e, 1, 0x01);
    //osd_win_trig                    0         1         WO      0a00[0] # trigger bit of osd window
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x0a00, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x0a00, 1, 0x01);
    //sdn_triggle                     0         1         WO      0400[0] # Trigger bit for scaling down register program.If scaling ratio or input / output resolution changed,
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x0400, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x0400, 1, 0x01);
    //timgen_trig                     0         1         WO / RO   0800[0] # write 1 to trigger registers related video window position, osd, viideo scaler up ratio registers
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x8000, 0, 0x01);
    fprintf(fp, "i2crw16 %02x %04x %02x %02x\n", puLnk->GetI2CAddr(), 0x8000, 1, 0x01);
}


void CMS1858Register::save_txt(FILE* fp)
{
#if MTOOLS_FOR_INTERNAL
    m_parsetxt.save_txt_file(fp, "CM8858A");
#endif
}

void CMS1858Register::load_txt(FILE* fp)
{
    int i;
    CParseTXT *pTxt = new CParseTXT();
    pTxt->parse_txt_file(fp);

    for (i = 0; i <= MAX_RANGE; i++)
    {
        if (0 == pTxt->reg_addr[i])
            break;
        if (0 == pTxt->reg_length[i]) \
        {
            char temp[1024];
            sprintf(temp, "Wrong register: name = %s (Module %s)", pTxt->reg_name[i].c_str(), pTxt->reg_module_name[i].c_str());
            CString c(temp);
            Log(c);
        }
        else
        {
            HAL_WriteRange(pTxt->reg_addr[i], pTxt->reg_st[i], pTxt->reg_length[i], pTxt->reg_value[i]);
        }
    }

    delete pTxt;
    return;
}

bool CMS1858Register::check_chipid()
{    
    BYTE temp_1, temp_2, temp_3;

    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0000, &temp_1);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0001, &temp_2); 
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0002, &temp_3);

    if(temp_1==0xB8 && temp_2==0x85 && temp_3==0x8A)  // MS1858 MT_app_config.h
    {
        return (true);
    }
    
    return (false);
}


void CMS1858Register::load_bat(FILE *pFile)
{
    char    buf[MAX_PATH], buf1[MAX_PATH];
    char    cmd[MAX_PATH];
    char    where0[MAX_PATH], where1[MAX_PATH];
    int     lineno = 0;
    int     lines = 0;
    int     i = 0;
    BOOL    bFind = FALSE, t_flag;
    CString c;
    FILE    *fp = NULL;

    while (!feof(pFile))
    {
        fgets(buf, 255, pFile);
        lines++;
    }
    fseek(pFile, 0, SEEK_SET);

    t_flag = TRUE;

    CProgressWnd wnd(AfxGetMainWnd(), _T("Write to target machine"));
    wnd.SetRange(0, lines);

    while (!feof(pFile))
    {
        if (bFind == FALSE)
        {
            //get 1 line, if 
            if (!fgets(buf, 255, pFile))
                break;

            lineno++;
            c.Format(_T("Writing line %d - %S"), lineno, buf);
            /*说明: c.Format(_T("Writing line %d - %s"), lineno, buf);在运行后会出现乱码,将小s改成S后正常,unicode环境下格式化ansi字符串要用 %S  ,默认编码是unicode  ，char定义的字符数组为ASCII编码，用Format之后就会造成乱码，也可以用应该用CStringW关键字进行转换*/
            //Log(c);
            wnd.SetText(c);
            wnd.StepIt();
            wnd.PeekAndPump();
            if (wnd.Cancelled())
                break;

            sscanf(buf, "%s %s", &cmd, &where0);
            /* process here */
            if (!_strcmpi(cmd, "goto"))
            {
                bFind = TRUE;
                if (t_flag)
                    fseek(pFile, 0, SEEK_SET);
                else break;
                i = strlen(where0);
                where0[i] = ':';
                where0[i + 1] = '\0';
            }
            else if (!_strcmpi(cmd, "call"))
            {
                fp = fopen(where0, "rt");

                if (fp != NULL)
                {
                    load_bat(fp);
                }
            }
            else
            {
                i = strlen(cmd);
                if (cmd[i - 1] == ':')
                {
                    i = 0;
                    while (buf[i] != '\0')
                    {
                        buf1[i] = buf[strlen(cmd) + 1 + i];
                        i++;
                    }
                    g_IoCmd.executeLine(buf1);
                }
                g_IoCmd.executeLine(buf);
            }
        }
        else
        {
            //get 1 line, if 
            if (!fgets(buf, 255, pFile))
                break;

            sscanf(buf, "%s %s", &cmd, &where1);
            /* process here */
            if (!_strcmpi(where0, cmd))
            {
                bFind = FALSE;
                i = 0;
                while (buf[i] != '\0')
                {
                    buf1[i] = buf[strlen(cmd) + 1 + i];
                    i++;
                }
                g_IoCmd.executeLine(buf1);
            }
        }
    }
}

