// MSRegister.h: interface for the CMSRegister class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSREGISTER_H__D9049F58_FBFD_4B42_BB43_535360486E0D__INCLUDED_)
#define AFX_MSREGISTER_H__D9049F58_FBFD_4B42_BB43_535360486E0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "assert.h"
//#include "TextReg16.h"

class CMSRegister
{
public:
    virtual TCHAR* get_chipname() { assert(0); return NULL; };
    virtual void save_bat(FILE* fp) {assert(0);};
    virtual void load_bat(FILE *) { assert(0);};
    virtual void save_txt(FILE*) {assert(0);};
    virtual void load_txt(FILE*) {assert(0);};
    virtual bool check_chipid() {assert(0); return false;};
    virtual BYTE get_bank() {assert(0); return 0xFF;};
    CMSRegister() {};

protected:

    //virtual void write_text_reg(long value,const CTextReg16 txtReg) {assert(0);};
    //virtual long read_text_reg(const CTextReg16 txtReg) {assert(0); return(0);};
    //virtual void save_txt_reg(const CTextReg16, FILE*) {assert(0);};

    //virtual int read_memory(DWORD startAddr, DWORD endAddr, BYTE *pValue) {assert(0); return(0);};
    //virtual int write_memory(DWORD startAddr, DWORD endAddr, BYTE *pValue) {assert(0); return(0);};
};

extern CMSRegister *pMSREG;

#endif // !defined(AFX_MSREGISTER_H__D9049F58_FBFD_4B42_BB43_535360486E0D__INCLUDED_)
