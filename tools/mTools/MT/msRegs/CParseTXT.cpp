#include "stdafx.h"
#include "mTools.h"
#include "MSLnkU.h"

#include "CParseTXT.h"

#include "MainFrm.h"
#ifdef PROGRESS_WIN
#include "ProgressWnd.h"
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

#define OCT_NUM_P        0
#define HEX_NUM_P        1

/* line info macro define */
#define EMPTY_LINE_P        0
#define COMMAND_LINE_P      1
#define FIRST_SEGMENT_P     2
#define NEXT_SEGMENT_P      3
#define VALID_LINE_P        4
#define VALID_END_P         5
#define UNVALID_END_P       6
#define REG_TITLE_P      7


/* text line status macro define */
#define TXT_START_P         0
#define REG_NAME_P          1
#define REG_VALUE_P         2
#define REG_LENGTH_P        3
#define REG_ATTRIBUTE_P   4
#define REG_ADDR_P          5
#define COMMENT_P           6
#define CHECK_MODULE_P         7
#define TXT_FINAL_P         8

CParseTXT::CParseTXT()
{
}


CParseTXT::~CParseTXT()
{
}

void CParseTXT::init_data()
{
	int i;

    module_num = 0;
    reg_num = 0;
	for (i = 0; i<MAX_RANGE; i++)
	{
		reg_value[i] = 0;
		reg_addr[i] = 0;
		reg_st[i] = 0;
		reg_length[i] = 0;
	}
}

void CParseTXT::parse_txt_file(FILE* fp)
{
#if MTOOLS_FOR_INTERNAL
	line_count = 0;
	init_data();

	rewind(fp);  //将文件指针重新放置到文件开始处

	while (1)
	{
		switch (check_txt_lines(fp))
		{
		case REG_TITLE_P:
			save_module_name();
			break;
		case VALID_LINE_P:
			save_reg_to_rom();
			break;
		case VALID_END_P:
			save_reg_to_rom();
		case UNVALID_END_P:
			return;
		}
	}
#endif
}

int CParseTXT::save_bat_file(FILE *fp, char *title)
{
    int i;
    BYTE temp;

    int length = 0;
    WORD last_addr = reg_addr[0];
    string module_name = "";

    for (i = 0; i <= MAX_RANGE; i++)
    {
        if (module_name != reg_module_name[i])
        {
            module_name = reg_module_name[i];
            fprintf(fp, "# ");
            fprintf(fp, "%s\n", module_name.c_str());
        }

        if (last_addr != reg_addr[i])
        {
            for (int j = 0; j < (length+7) / 8; j++)
            {
                puLnk->i2cr16(puLnk->GetI2CAddr(), last_addr+j, &temp);
                fprintf(fp, "i2crw16 %02x %04x %02x\n", puLnk->GetI2CAddr(), last_addr+j, temp);
            }
            last_addr = reg_addr[i];
            length = reg_length[i];
        }
        else
        {
            length += reg_length[i];
        }

        if (0 == last_addr)
            break;
    }

    return 0;
}

int CParseTXT::save_txt_file(FILE *fp, char *title)
{
    int i;

    WORD last_addr = 0;
    string module_name = "";

    for (i = 0; i <= MAX_RANGE; i++)
    {
        if (0 == reg_addr[i])
            break;

        if (module_name != reg_module_name[i])
        {
            module_name = reg_module_name[i];
            fprintf(fp, "\n# %s Module ", title);
            fprintf(fp, "%s Register Definition\n", module_name.c_str());
        }

        LONG value;
        if (0 == reg_length[i])
        {
            value = -1;
        }
        else 
        {
            value = HAL_ReadRange(reg_addr[i], reg_st[i], reg_length[i]);
        }

        fprintf(fp, "%-32s%-10llx%-10x%-8s%04x[%d] ", reg_name[i].c_str(), (LONG64)value,
            reg_length[i], reg_attribute[i].c_str(), reg_addr[i], reg_st[i]);
        
        size_t start = 0;
        int pos = reg_description[i].find('\n', start);
        if (string::npos == pos)
        {
            fprintf(fp, "#%s\n", reg_description[i].substr(start, string::npos).c_str());
            return 0;
        }

        fprintf(fp, "#%s\n", reg_description[i].substr(start, pos - start).c_str());
        start = (pos + 1);

        while (start < reg_description[i].length())
        {
            pos = reg_description[i].find('\n', start);
            if (string::npos == pos)
            {
                fprintf(fp, "%69s%s\n", "#", reg_description[i].substr(start, string::npos).c_str());
                break;
            }

            fprintf(fp, "%69s%s\n", "#", reg_description[i].substr(start, pos - start).c_str());
            start = (pos + 1);
        }
    }

    return 0;
}


int CParseTXT::check_txt_lines(FILE* fp_in)
{
	int status = TXT_START_P;
	int temp = EMPTY_LINE_P;
	char *line = temp_line;  //char temp_line[512];
	char c1, c2;

	strcpy_s(temp_line, strlen("\0") + 1, "\0");  //清空数组(均为0),以'\0'为结束符,防止乱码出现
	fgets(temp_line, 1024, fp_in);

	temp_module_name = "\0";
	temp_name = "\0";
	temp_description = "\0";
	temp_value = -1;
	temp_length = -1;
	temp_attribute = "\0";
	temp_port = 0xFFFF;
	temp_bit = 0xFF;

	while ((*line != 0x0) && (*line != '\n'))
	{
		switch (status)
		{
		case TXT_START_P:
			if (*line == '#')
			{
				status = CHECK_MODULE_P;
				line += 16;  // move to module name directly
			}
			else if ((*line != 0x20) && (*line != 0x09))  //0x20 --> space; 0x09 --> Tab
			{
				line--;
				status = REG_NAME_P;
			}
			else if ((*line == 0x20) || (*line == 0x09))
			{
				line += 68;
				status = COMMENT_P;
				temp = VALID_LINE_P;
			}
			break;
		case REG_NAME_P:
			if (*line == '#')
			{
				status = TXT_FINAL_P;
			}
			else if ((*line == 0x20) || (*line == 0x09))
			{
				status = REG_VALUE_P;
			}
			else if ((*line != 0x20) || (*line != 0x09))
			{
				temp_name += *line;
			}

			break;
		case REG_VALUE_P:
			if (*line == '#')
			{
				status = TXT_FINAL_P;
			}
			else if ((*line != 0x20) && (*line != 0x09))
			{
				if (temp_value < 0)
				{
					temp_value = add_value(0l, *line, HEX_NUM_P);
				}
				else
				{
					temp_value = add_value(temp_value, *line, HEX_NUM_P);
				}
			}
			else if (temp_value >= 0)
			{
				status = REG_LENGTH_P;
			}
			break;
		case REG_LENGTH_P:
			if (*line == '#')
			{
				status = TXT_FINAL_P;
			}
			else if ((*line != 0x20) && (*line != 0x09))
			{
				if (temp_length < 0)
				{
					temp_length = (int)add_value(0l, *line, HEX_NUM_P);
				}
				else
				{
					temp_length = (int)add_value(temp_length, *line, HEX_NUM_P);
				}
			}
			else if (temp_length >= 0)
			{
				//status = REG_ADDR;
				status = REG_ATTRIBUTE_P;
			}
			break;
		case REG_ATTRIBUTE_P:
			if (*line == '#')
			{
				status = TXT_FINAL_P;
			}
			else if ((*line != 0x20) && (*line != 0x09))
			{
				temp_attribute += *line;
			}
			else if (temp_attribute != "\0")
			{
				status = REG_ADDR_P;
			}
			break;
		case REG_ADDR_P:
			if (*line == '#')
			{
				if (temp_port == 0xFFFF)
				{
					status = TXT_FINAL_P;
				}
				else
				{
					status = COMMENT_P;
				}
			}
			else if ((*line != 0x20) && (*line != 0x09))
			{
				if (temp_port == 0xFFFF)
				{
#if 0
					sscanf_s(line, "%04x%01c%01o%01c", &temp_port, &c1, &temp_bit, &c2);
#else
					sscanf_s(line, "%04hx", &temp_port);
					sscanf_s(&line[4], "%01c", &c1, 1);
					sscanf_s(&line[5], "%01hho", &temp_bit);
					sscanf_s(&line[6], "%01c", &c2, 1);
#endif
				}
				else
				{
					temp = VALID_LINE_P;
					status = COMMENT_P;
					line += 7;  //line += 6 --> 7为了去掉注释的'#'
				}
			}
			break;
		case CHECK_MODULE_P:
			if (*line != 0x20)
			{
				temp_module_name += *line;
			}
			else
			{
				status = COMMENT_P;  //处理垃圾字符
			}
			temp = REG_TITLE_P;  //不保存数据
			break;
		case COMMENT_P:
			if ((temp_port != 0xFFFF) || (temp_line[0] == 0x20) || (temp_line[0] == 0x09))
			{
				temp_description += *line;
			}
			break;
		case TXT_FINAL_P:
			break;
		}

		line++;
	}

	if (*line == 0)
	{
		if (temp == VALID_LINE_P)
		{
			temp = VALID_END_P;
		}
		else
		{
			temp = UNVALID_END_P;
		}
	}
	temp_description += '\n';
	return temp;
}

void CParseTXT::save_reg_to_rom()
{
	if (temp_port != 0xFFFF)
	{
		// register name
		reg_name[reg_num] = temp_name;
		// register value
		reg_value[reg_num] = temp_value;
		// register length
		reg_length[reg_num] = temp_length;
		// register attribute
		reg_attribute[reg_num] = temp_attribute;
		// register address
		reg_addr[reg_num] = temp_port;
		//register start bit
		reg_st[reg_num] = temp_bit;
		// register description
		reg_description[reg_num] = temp_description;
		// register of Module ID
		reg_module_name[reg_num] = module_name[module_num-1];
	}
	else
	{
		reg_num--;
		reg_description[reg_num] += temp_description;
	}
	reg_num++;
}

void CParseTXT::save_module_name()
{
	module_name[module_num] = temp_module_name;
	module_num++;
}

LONG64 CParseTXT::add_value(LONG64 value, char key, int Hex_en)
{
	LONG64 acc;

	switch (key)
	{
	case '0':
		acc = 0;
		break;
	case '1':
		acc = 1;
		break;
	case '2':
		acc = 2;
		break;
	case '3':
		acc = 3;
		break;
	case '4':
		acc = 4;
		break;
	case '5':
		acc = 5;
		break;
	case '6':
		acc = 6;
		break;
	case '7':
		acc = 7;
		break;
	case '8':
		acc = 8;
		break;
	case '9':
		acc = 9;
		break;
	case 'a':
	case 'A':
		acc = 10;
		break;
	case 'b':
	case 'B':
		acc = 11;
		break;
	case 'c':
	case 'C':
		acc = 12;
		break;
	case 'd':
	case 'D':
		acc = 13;
		break;
	case 'e':
	case 'E':
		acc = 14;
		break;
	case 'f':
	case 'F':
		acc = 15;
		break;
	default:
		return 0l;
	}

	if (Hex_en)
	{
		return (value << 4) + acc;
	}
	else
	{
		return (value << 3) + acc;
	}
}