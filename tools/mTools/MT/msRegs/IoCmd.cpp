// IoCmd.cpp: implementation of the IOCmd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MSLnkU.h"

#include "IoCmd.h"

extern CMSLinkU *puLnk;
CIOCmd g_IoCmd;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CIOCmd::CIOCmd()
{
}


BOOL CIOCmd::executeLine(char *pLine)
{
    CString stat;
    char    cmd[20];
    int     fields;
    BOOL    result;
    IOTYPE_T  iot;
    int len;
    int i = 0;
    char *pCpy = pLine;

    /* remove newline (if available) */
    while (strlen(pLine))
    {
        if ((*(pLine + strlen(pLine) - 1) == 0x0D) ||
            (*(pLine + strlen(pLine) - 1) == 0x0A))

            *(pLine + strlen(pLine) - 1) = 0;
        else
            break;
    }

    /* do nothing is line is empty */
    if (!strlen(pLine))
        return TRUE;

    len = strlen(pLine);
    while (i < len)
    {
        if (*pCpy == ' ' || *pCpy == '	')
        {
            pCpy++;
        }
        else if (*pCpy == '#' || *pCpy == '/')
        {
            return FALSE;
        }
        else
        {
            break;
        }

        i++;
    }

    /* scan all parms */
    memset(&iot, 0, sizeof(iot));
    fields = sscanf(pLine, "%s %lx %lx %lx %lx %lx", &cmd, &iot.parm1, &iot.parm2, &iot.parm3, &iot.parm4, &iot.parm5);

    /* no field may be caused from empty line */
    if (!fields || (fields == EOF))
        return TRUE;

    /*
    * pre-process
    * - DOS utility i2crw use 2 parm for read and 3 parm for write
    * so we convert to i2cr and i2cw.
    */
    if (!_strcmpi(cmd, "i2crw"))
    {
        if (fields == 3)
            strcpy(cmd, "i2cr");
        else if (fields == 4 || fields == 5)
            strcpy(cmd, "i2cw");
        else
            return FALSE;
    }
    else if (!_strcmpi(cmd, "i2crw16"))
    {
        if (fields == 3)
            strcpy(cmd, "i2cr16");
        else if (fields >= 4)
            strcpy(cmd, "i2cw16");
        else
            return FALSE;
    }

    iot.fields = fields - 1;

    /* main process */
    if (!_strcmpi(cmd, "ior16"))
        result = processIOR16(&iot);
    else if (!_strcmpi(cmd, "iow16"))
    {
        result = processIOW16(&iot);
    }
    else if (!_strcmpi(cmd, "i2cr16"))
    {
        result = processI2CR16(&iot);
    }
    else if (!_strcmpi(cmd, "i2cw16"))
    {
        result = processI2CW16(&iot);
    }
    else if (!_strcmpi(cmd, "ior"))
        result = processIOR(&iot);
    else if (!_strcmpi(cmd, "iow"))
    {
        result = processIOW(&iot);
    }
    else if (!_strcmpi(cmd, "i2cr"))
    {
        result = processI2CR(&iot);
    }
    else if (!_strcmpi(cmd, "i2cw"))
    {
        result = processI2CW(&iot);
    }
    else if (!_strcmpi(cmd, "rem") || !_strcmpi(cmd, "#") || !_strcmpi(cmd, ";"))
    {
        /* treat this as comment */
        result = TRUE;
    }
    else if (!_strcmpi(cmd, "pause"))
    {
        /* if no comment, display "press.." otherwise will display it */
        if (strlen(pLine) <= 5)
        {
            if (AfxMessageBox(_T("Press OK to continue"), MB_ICONINFORMATION | MB_OKCANCEL) != IDOK)
            {
                //this->t_flag = false;
            }
        }
        else
        {
            char *info = pLine + 5;
            CString str(pLine + 5);
            if (AfxMessageBox(str, MB_ICONINFORMATION | MB_OKCANCEL) != IDOK)
            {
                //this->t_flag = false;
            }
        }
    }
    else if (!_strcmpi(cmd, "delay"))
    {

        Sleep(iot.parm1);
        result = TRUE;
    }
    else
    {
        if (*pLine == '#')
        {
            /* Comment line. Just skip! */
            result = TRUE;
        }
        else
        {
            stat.Format(_T("*** Unknown cmd %s ***"), pLine);
            Log(stat);
            result = FALSE;
        }
    }

    return result;

}

BOOL CIOCmd::processIOR(IOTYPE_T *pIO)
{
    int     i;
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 1)
    {
        /* ior <port> */
        pIO->parm3 = 0;
        if (!puLnk->ior((BYTE)pIO->parm1, (BYTE*)&pIO->parm3))
            stat.Format(_T("ior %02x -> %02x"), pIO->parm1, pIO->parm3);
        else
            stat.Format(_T("ior %02x -> failed"), pIO->parm1);
        Log(stat);
    }
    else if (pIO->fields == 2)
    {
        /* ior <port> <repeat>, for shell only */
        for (i = 0; i<(int)pIO->parm2; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->ior((WORD)pIO->parm1 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("ior %02x -> %02x"), pIO->parm1 + i, pIO->parm4);
            else
                stat.Format(_T("ior %02x -> failed"), pIO->parm1 + i);
            Log(stat);
        }
    }
    else
    {
        stat.Format(_T("*** ior parameters missing ***"));
        Log(stat);
        result = FALSE;
    }

    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processIOW(IOTYPE_T *pIO)
{
    CString stat;
    BOOL result = TRUE;

    if (pIO->fields == 2)
    {
        /* iow <port> <value> */
        if (!puLnk->iow((BYTE)pIO->parm1, (BYTE)pIO->parm2))
            stat.Format(_T("iow %02x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("iow %02x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* iow <port> <value> <mask> */
        if (!puLnk->iow((BYTE)pIO->parm1, (BYTE)pIO->parm2, (BYTE)~pIO->parm3))
            stat.Format(_T("iow %02x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("iow %02x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** ior parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processI2CR(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;
    int     i, j;
    BYTE pdata[0xffff];

    if (pIO->fields == 2)
    {
        /* i2cr <addr> <subaddr> */
        pIO->parm3 = 0;
        if (!puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2, (BYTE*)&pIO->parm3))
            stat.Format(_T("i2cr %02x %02x -> %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cr %02x %02x -> failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cr <addr> <subaddr> <brust> */
        for (i = 0; i<(int)pIO->parm3; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("i2cr %02x %02x -> %02x"), pIO->parm1, pIO->parm2 + i, pIO->parm4);
            else
                stat.Format(_T("i2cr %02x %02x -> failed"), pIO->parm1, pIO->parm2 + i);
            Log(stat);
        }
    }
    else if (pIO->fields == 4)
    {
        /* i2cr <addr> <subaddr> <brust> <repeat> */
        for (j = 0; j<(int)pIO->parm3; j++)
        {
            puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2 + j, (byte)pIO->parm4, pdata);
            for (i = 0; i<(int)pIO->parm4; i++)
            {
                stat.Format(_T("i2cr %02x %02x -> byte: %02x %02x"), pIO->parm1, pIO->parm2 + j, i, pdata[i]);
                Log(stat);
                //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
            }
        }
    }
    else
    {
        stat.Format(_T("*** i2cr parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processI2CW(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 2)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2))
            stat.Format(_T("i2cw %02x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("i2cw %02x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3))
            stat.Format(_T("i2cw %02x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cw %02x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else if (pIO->fields == 4)
    {
        /* i2cw <addr> <subaddr> <value> <mask>*/
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3, (BYTE)~pIO->parm4))
            stat.Format(_T("i2cw %02x %02x %02x, mask = %02x"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        else
            stat.Format(_T("i2cw %02x %02x %02x, mask = %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** i2cw parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

//16bit mode
BOOL CIOCmd::processI2CR16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;
    int     i, j;
    BYTE pdata[0xffff];

    if (pIO->fields == 2)
    {
        /* i2cr <addr> <subaddr> */
        pIO->parm3 = 0;
        if (!puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2, (BYTE*)&pIO->parm3))
            stat.Format(_T("i2cr16 %02x %04x -> %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cr16 %02x %04x -> failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cr <addr> <subaddr> <repeat> */
        for (i = 0; i<(int)pIO->parm3; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("i2cr16 %02x %04x -> %02x"), pIO->parm1, pIO->parm2 + i, pIO->parm4);
            else
                stat.Format(_T("i2cr16 %02x %04x -> failed"), pIO->parm1, pIO->parm2 + i);
            Log(stat);
        }
    }
    else if (pIO->fields == 4)
    {
        /* i2cr <addr> <subaddr> <brust> <repeat> */
        for (j = 0; j<(int)pIO->parm3; j++)
        {
            puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2 + j, (byte)pIO->parm4, pdata);
            for (i = 0; i<(int)pIO->parm4; i++)
            {
                stat.Format(_T("i2cr %04x %02x -> byte: %02x %02x"), pIO->parm1, pIO->parm2 + j, i, pdata[i]);
                Log(stat);
                //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
            }
        }
    }
    else
    {
        stat.Format(_T("*** i2cr16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processI2CW16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 3)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw16((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3))
            stat.Format(_T("i2cw16 %02x %04x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cw16 %02x %04x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else if (pIO->fields == 4)
    {
        /* i2cw <addr> <subaddr> <value> <mask>*/
        if (!puLnk->i2cw16((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3, (BYTE)~pIO->parm4))
            stat.Format(_T("i2cw16 %02x %04x %02x, mask = %02x"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        else
            stat.Format(_T("i2cw16 %02x %04x %02x, mask = %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** i2cw16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    // m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processIOR16(IOTYPE_T *pIO)
{
    int     i;
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 1)
    {
        /* ior <port> */
        pIO->parm3 = 0;
        if (!puLnk->ior16((WORD)pIO->parm1, (BYTE*)&pIO->parm3))
            stat.Format(_T("ior16 %04x -> %02x"), pIO->parm1, pIO->parm3);
        else
            stat.Format(_T("ior16 %04x -> failed"), pIO->parm1);
        Log(stat);
    }
    else if (pIO->fields == 2)
    {
        /* ior <port> <repeat>, for shell only */
        for (i = 0; i<(int)pIO->parm2; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->ior16((WORD)(pIO->parm1 + i), (BYTE*)&pIO->parm4))
                stat.Format(_T("ior16 %04x -> %02x"), pIO->parm1 + i, pIO->parm4);
            else
                stat.Format(_T("ior16 %04x -> failed"), pIO->parm1 + i);
            Log(stat);
        }
    }
    else
    {
        stat.Format(_T("*** ior16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }

    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CIOCmd::processIOW16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL result = TRUE;

    if (pIO->fields == 2)
    {
        /* iow <port> <value> */
        if (!puLnk->iow16((WORD)pIO->parm1, (BYTE)pIO->parm2))
            stat.Format(_T("iow16 %04x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("iow16 %04x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* iow <port> <value> <mask> */
        if (!puLnk->iow16((WORD)pIO->parm1, (BYTE)pIO->parm2, (BYTE)~pIO->parm3))
            stat.Format(_T("iow16 %04x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("iow16 %04x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** iow16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}
