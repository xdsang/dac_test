// IoCmd.h: interface for the CTextReg16 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_IO_CMD_H_)
#define _IO_CMD_H_

typedef struct
{
    int     fields;
    DWORD   parm1;      //port
    DWORD   parm2;      //index
    DWORD   parm3;      //value
    DWORD   parm4;      //mask or count
    DWORD   parm5;      //
} IOTYPE_T;

class CIOCmd  
{
public:
    CIOCmd();

    BOOL executeLine(char *pLine);
    BOOL processIOR(IOTYPE_T *pIO);
    BOOL processIOW(IOTYPE_T *pIO);
    BOOL processI2CR(IOTYPE_T *pIO);
    BOOL processI2CW(IOTYPE_T *pIO);
    BOOL processIOR16(IOTYPE_T *pIO);
    BOOL processIOW16(IOTYPE_T *pIO);
    BOOL processI2CR16(IOTYPE_T *pIO);
    BOOL processI2CW16(IOTYPE_T *pIO);
};

extern CIOCmd g_IoCmd;

#endif //_IO_CMD_H_
