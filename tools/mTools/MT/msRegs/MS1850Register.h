// MS1820Register.h: interface for the CMS1850Register class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MS1820REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_)
#define AFX_MS1820REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MSRegister.h"
#include "TextReg16.h"

typedef struct
{
    int     fields;
    DWORD   parm1;      //port
    DWORD   parm2;      //index
    DWORD   parm3;      //value
    DWORD   parm4;      //mask or count
    DWORD   parm5;      //
} IOTYPE_T;

class CMS1850Register : public CMSRegister
{
public:
    virtual void save_bat(FILE* fp);
    virtual void executeBatFile(FILE *);
    virtual void save_txt(FILE*);
    virtual void load_txt(FILE*);
    virtual bool check_chipid();
    virtual BYTE get_bank();
    CMS1850Register();
    ~CMS1850Register();
    
//protected:
    virtual long read_text_reg(const CTextReg16 txtReg);
    virtual void write_text_reg(long value, const CTextReg16 txtReg);
    virtual void save_txt_reg(const CTextReg16 txtReg, FILE * fp);

    BOOL executeLine(char *pLine);
    BOOL processIOR(IOTYPE_T *pIO);
    BOOL processIOW(IOTYPE_T *pIO);
    BOOL processI2CR(IOTYPE_T *pIO);
    BOOL processI2CW(IOTYPE_T *pIO);
    BOOL processIOR16(IOTYPE_T *pIO);
    BOOL processIOW16(IOTYPE_T *pIO);
    BOOL processI2CR16(IOTYPE_T *pIO);
    BOOL processI2CW16(IOTYPE_T *pIO);
};

extern CMS1850Register MS1820REG;
extern CMS1850Register *pMSREG;

#endif // !defined(AFX_MS1820REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_)
