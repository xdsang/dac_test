// CMS1850Register.cpp: implementation of the CMS1850Register class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TxtProc16.h"
#include "MSLnkU.h"
#include "mTools.h"
#include "MS1850Register.h"

extern CMTApp theApp;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CTxtproc16 txtProc16;
extern CMSLinkU *puLnk;
CMS1850Register MS1820REG;
CMS1850Register *pMSREG;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static void print_comment(char* comment, FILE* fp)
{
    int i = 75;
    
    while (i) {
        fprintf(fp, " ");
        i--;
    }
    fprintf(fp, "%s\n", comment);
}

CMS1850Register::CMS1850Register()
{
}

CMS1850Register::~CMS1850Register()
{
}

BYTE CMS1850Register::get_bank()
{
    return (0);
}

static int write_bat_segment(FILE* fp, int start, int end, char *name)
{
    int i;
    BYTE temp;
    
    fprintf(fp, "\n%s\n", name);

    for(i=start; i<=end; i++)
    {
        puLnk->i2cr16(puLnk->GetI2CAddr(), i, &temp);
        //trriger bit, set 0
        if (i == 0x610f)
            temp &= 0xdf;
        else if (i == 0x6112)
            temp &= 0xdf;
        else if (i == 0x6113)
            temp &= 0xdf;
        else if (i == 0x6330)
            temp = 0x10;
        else if (i == 0x616f)
            temp &= 0xf7;
        else if (i == 0x6172)
            temp &= 0xfe;
        else if (i == 0x61ca)
            temp &= 0xfe;
        else if (i == 0x6434)
            temp &= 0xfe;
        else if (i == 0x64c2)
            temp &= 0xfe;
        fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), i, temp);
    }
    
    return (end + 1 - start);
}

void CMS1850Register::save_bat(FILE* fp)
{
    int  count_line =0;
    CString c;
    int lines = 499;
    BYTE temp;
    
    fprintf(fp, "#MS1820 register table\n");
    write_bat_segment(fp, 0x64d0, 0x64e7, "#topadc 64d0-64e7");
    fprintf(fp, "\n#adcabist 64f0-650b\n");
    write_bat_segment(fp, 0x64b0, 0x64c2, "#addpll 64b0-64c2");
    write_bat_segment(fp, 0x6430, 0x6430, "#adc_clkmux");
    write_bat_segment(fp, 0x6431, 0x6435, "#pa  6431-6435");
    write_bat_segment(fp, 0x643c, 0x643f, "#auto offset 643c~643f");
    write_bat_segment(fp, 0x6449, 0x644f, "#adc test, auto_phase 6449~644f");
    write_bat_segment(fp, 0x6450, 0x648f, "#sp 6450~648f");
    write_bat_segment(fp, 0x6490, 0x649f, "#decimation 6490~649f");
    write_bat_segment(fp, 0x6160, 0x6172, "#pllvm 6160~6172");
    write_bat_segment(fp, 0x6180, 0x6191, "#pad cntrl 6180~6191");
    write_bat_segment(fp, 0x6100, 0x6103, "#dactop 6100~6103");
    write_bat_segment(fp, 0x6104, 0x6106, "#reset_top 6104~6106");
    write_bat_segment(fp, 0x610b, 0x6119, "#dac_mux 610b-6119,613a");
    write_bat_segment(fp, 0x613a, 0x613a, "");
    write_bat_segment(fp, 0x6200, 0x621b, "#auto detect 6200~621b");
    write_bat_segment(fp, 0x61d0, 0x61f5, "#if_hd_byps 61d0-61f5");
    write_bat_segment(fp, 0x61cd, 0x61cf, "#din_mux 61cd-61cf");
    write_bat_segment(fp, 0x61a0, 0x61ca, "#input formtat 61a0~61ca, 626e~626f");
    write_bat_segment(fp, 0x626e, 0x626f, "");
    write_bat_segment(fp, 0x6230, 0x626d, "#deint 6230-626d");
    write_bat_segment(fp, 0x6330, 0x634f, "#memory contoller 6330~634f,6220-6225");
    write_bat_segment(fp, 0x6220, 0x6225, "");
    write_bat_segment(fp, 0x6371, 0x638b, "#wff&rff 6371-638b");
    write_bat_segment(fp, 0x6350, 0x6370, "#cap&pb 6350~6370");
    write_bat_segment(fp, 0x6418, 0x641e, "#adc2mem 6418~641e");
    write_bat_segment(fp, 0x6391, 0x640f, "#osd 6391~6417");
    write_bat_segment(fp, 0x6410, 0x641e, "#alpha");
    write_bat_segment(fp, 0x6270, 0x62ff, "#vds 6270~62ff");
    write_bat_segment(fp, 0x6300, 0x6324, "#vds matrix 6300~6324");
    write_bat_segment(fp, 0x6510, 0x658f, "#tvenc 6510~658f");

    fprintf(fp, "\n");
    fprintf(fp, "#NEXTSEG\n");
    //trriger bit, set 1
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x610f, &temp);
    temp |= 0x20;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x610f, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6112, &temp);
    temp |= 0x20;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x6112, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6113, &temp);
    temp |= 0x20;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x6113, temp);

    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x6330, 0x82);

    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x616f, &temp);
    temp |= 0x08;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x616f, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6172, &temp);
    temp |= 0x01;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x6172, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x61ca, &temp);
    temp |= 0x01;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x61ca, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6434, &temp);
    temp |= 0x01;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x6434, temp);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x64c2, &temp);
    temp |= 0x01;
    fprintf(fp, "i2crw16 %02x %02x %02x\n", puLnk->GetI2CAddr(), 0x64c2, temp);

    
}


#include "ms1850_register.def"
void CMS1850Register::save_txt(FILE* fp)
{
#if MTOOLS_FOR_INTERNAL
    #include "ms1850_txtsave.def"  
#endif
}

void CMS1850Register::load_txt(FILE* fp)
{
    txtProc16.execute_txt_file(fp);
}

bool CMS1850Register::check_chipid()
{    
    BYTE temp_1, temp_2, temp_3;

#if 1        
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0000, &temp_1);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0001, &temp_2); 
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x0002, &temp_3);

    if(temp_1==0x0A && temp_2==0x85 && temp_3==0xA8)  // ms1850
#else
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6080, &temp_1);
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6081, &temp_2); 
    puLnk->i2cr16(puLnk->GetI2CAddr(), 0x6082, &temp_3);

    if(temp_1==0x1a && temp_2==0x60 && temp_3==0x01)  // ms1820
#endif
    {
        return (true);
    }
    
    return (false);
}

long CMS1850Register::read_text_reg(const CTextReg16 txtReg)
{
    long value = 0l;
    WORD  reg_count = 0;
    WORD i = 0;
    BYTE st_bit;
    BYTE sp_bit;
    long bValue = 0;
    BYTE bits = txtReg.length + txtReg.bit;
    BYTE mask;
    BYTE bits_inone;
    BYTE j;

    CString log;
    
    reg_count = bits / 8;
    reg_count += (bits % 8) ? 1 : 0;
    
    for (i = txtReg.port; i < (txtReg.port + reg_count); i++)
    {
        bValue = 0;
        st_bit = (i == txtReg.port) ? txtReg.bit : 0;
        sp_bit = (bits >= 8) ? 7 : (bits - 1);
        bits_inone = sp_bit - st_bit + 1;
        mask = txtProc16.sort_mask(st_bit, sp_bit);
        puLnk->ior16(i, (BYTE *)&bValue);
        log.Format(_T("ior16 %02x %04x -> %02x"), puLnk->GetI2CAddr(), i, (BYTE)bValue);            
        Log(log);
        bValue &= ~mask;
        bValue >>= st_bit;
        if (i != txtReg.port)
        {
            j = (txtReg.bit) ? (i - txtReg.port - 1) : (i - txtReg.port - 1); 
            bValue <<= (8 - txtReg.bit) + j * 8;
        }
        value |= bValue;
        bits -= 8;
        
    }
    
    return value;
}

void CMS1850Register::write_text_reg(long value, const CTextReg16 txtReg)
{
    WORD  reg_count = 0;
    WORD i = 0;
    BYTE st_bit;
    BYTE sp_bit;
    BYTE bValue = 0;
    BYTE bits = txtReg.length + txtReg.bit;
    BYTE mask;
    
    CString log;
    
    reg_count = bits / 8;
    reg_count += (bits % 8) ? 1 : 0;
    
    for (i = txtReg.port; i < (txtReg.port + reg_count); i++)
    {
        st_bit = (i == txtReg.port) ? txtReg.bit : 0;
        sp_bit = (bits >= 8) ? 7 : (bits - 1);
        bits -= 8;
        mask = txtProc16.sort_mask(st_bit, sp_bit);
        bValue = (BYTE) (value & ((~mask) >> st_bit));
        value >>= (8-st_bit);
        bValue <<= st_bit;
        puLnk->iow16(i, bValue, mask);
        if(0 == ~mask)
        {
            log.Format(_T("iow16 %02x %04x %02x"), puLnk->GetI2CAddr(), i, (BYTE)bValue);
        }
        else
        {
            log.Format(_T("iow16 %02x %04x %02x, mask = %02x"), puLnk->GetI2CAddr(), i, (BYTE)bValue, (BYTE)~mask);
        }
        
        Log(log);
    }
}


void CMS1850Register::save_txt_reg(const CTextReg16 txtReg, FILE * fp)
{
    long value;

    value = read_text_reg(txtReg);

    int i = 0;
    int j = 0;

    fprintf(fp,"%s%x", txtReg.name, value);
    if (value < 0x10)
    {
        i = 8;
    }
    else if (value >= 0x10 && value < 0x100)
    {
        i = 7;
    }
    else if (value >= 0x100 && value < 0x1000)
    {
        i = 6;
    }
    else if (value >= 0x1000 && value <= 0xFFFF)
    {
        i = 5;
    }
    else if (value >= 0x10000 && value <= 0xFFFFF)
    {
        i = 4;
    }
    else if (value >= 0x100000 && value <= 0xFFFFFF)
    {
        i = 3;
    }
    else if (value >= 0x1000000 && value <= 0xFFFFFFF)
    {
        i = 2;
    }
    else if (value >= 0x10000000 && value <= 0xFFFFFFFF)
    {
        i = 1;
    }

    for (j = 0;j < i;j++)
    {
        fprintf(fp," ");
    }
    fprintf(fp,"%x", txtReg.length);

    if (txtReg.length < 0x10)
    {
        i = 8;
    }
    else if (txtReg.length >= 0x10 && txtReg.length < 0x100)
    {
        i = 7;
    }

    for (j = 0;j < i;j++)
    {
        fprintf(fp," ");
    }

    fprintf(fp,"%04x[%d] %s\n", txtReg.port,
                                     txtReg.bit,
                                     txtReg.comment);

//  CString log;

//  log.Format("read register: %s, value = %lx", txtReg.name, value);
//  theApp.log(log);
}


void CMS1850Register::executeBatFile(FILE *pFile)
{
    char    buf[MAX_PATH], buf1[MAX_PATH];
    char    cmd[MAX_PATH];
    char    where0[MAX_PATH], where1[MAX_PATH];
    int     lineno = 0;
    int     lines = 0;
    int     i = 0;
    BOOL    bFind = FALSE, t_flag;
    CString c;
    FILE    *fp = NULL;

    while (!feof(pFile))
    {
        fgets(buf, 255, pFile);
        lines++;
    }
    fseek(pFile, 0, SEEK_SET);

    t_flag = TRUE;

    while (!feof(pFile))
    {
        if (bFind == FALSE)
        {
            //get 1 line, if 
            if (!fgets(buf, 255, pFile))
                break;

            lineno++;
            c.Format(_T("Writing line %d - %S"), lineno, buf);
            /*说明: c.Format(_T("Writing line %d - %s"), lineno, buf);在运行后会出现乱码,将小s改成S后正常,unicode环境下格式化ansi字符串要用 %S  ,默认编码是unicode  ，char定义的字符数组为ASCII编码，用Format之后就会造成乱码，也可以用应该用CStringW关键字进行转换*/
            //Log(c);
            sscanf(buf, "%s %s", &cmd, &where0);
            /* process here */
            if (!_strcmpi(cmd, "goto"))
            {
                bFind = TRUE;
                if (t_flag)
                    fseek(pFile, 0, SEEK_SET);
                else break;
                i = strlen(where0);
                where0[i] = ':';
                where0[i + 1] = '\0';
            }
            else if (!_strcmpi(cmd, "call"))
            {
                fp = fopen(where0, "rt");

                if (fp != NULL)
                {
                    executeBatFile(fp);
                }
            }
            else
            {
                i = strlen(cmd);
                if (cmd[i - 1] == ':')
                {
                    i = 0;
                    while (buf[i] != '\0')
                    {
                        buf1[i] = buf[strlen(cmd) + 1 + i];
                        i++;
                    }
                    executeLine(buf1);
                }
                executeLine(buf);
            }
        }
        else
        {
            //get 1 line, if 
            if (!fgets(buf, 255, pFile))
                break;

            sscanf(buf, "%s %s", &cmd, &where1);
            /* process here */
            if (!_strcmpi(where0, cmd))
            {
                bFind = FALSE;
                i = 0;
                while (buf[i] != '\0')
                {
                    buf1[i] = buf[strlen(cmd) + 1 + i];
                    i++;
                }
                executeLine(buf1);
            }
        }
    }
}


BOOL CMS1850Register::executeLine(char *pLine)
{
    CString stat;
    char    cmd[20];
    int     fields;
    BOOL    result;
    IOTYPE_T  iot;
    int len;
    int i = 0;
    char *pCpy = pLine;

    /* remove newline (if available) */
    while (strlen(pLine))
    {
        if ((*(pLine + strlen(pLine) - 1) == 0x0D) ||
            (*(pLine + strlen(pLine) - 1) == 0x0A))

            *(pLine + strlen(pLine) - 1) = 0;
        else
            break;
    }

    /* do nothing is line is empty */
    if (!strlen(pLine))
        return TRUE;

    len = strlen(pLine);
    while (i < len)
    {
        if (*pCpy == ' ' || *pCpy == '	')
        {
            pCpy++;
        }
        else if (*pCpy == '#' || *pCpy == '/')
        {
            return FALSE;
        }
        else
        {
            break;
        }

        i++;
    }

    /* scan all parms */
    memset(&iot, 0, sizeof(iot));
    fields = sscanf(pLine, "%s %lx %lx %lx %lx %lx", &cmd, &iot.parm1, &iot.parm2, &iot.parm3, &iot.parm4, &iot.parm5);

    /* no field may be caused from empty line */
    if (!fields || (fields == EOF))
        return TRUE;

    /*
    * pre-process
    * - DOS utility i2crw use 2 parm for read and 3 parm for write
    * so we convert to i2cr and i2cw.
    */
    if (!_strcmpi(cmd, "i2crw"))
    {
        if (fields == 3)
            strcpy(cmd, "i2cr");
        else if (fields == 4 || fields == 5)
            strcpy(cmd, "i2cw");
        else
            return FALSE;
    }
    else if (!_strcmpi(cmd, "i2crw16"))
    {
        if (fields == 3)
            strcpy(cmd, "i2cr16");
        else if (fields >= 4)
            strcpy(cmd, "i2cw16");
        else
            return FALSE;
    }

    iot.fields = fields - 1;

    /* main process */
    if (!_strcmpi(cmd, "ior16"))
        result = processIOR16(&iot);
    else if (!_strcmpi(cmd, "iow16"))
    {
        result = processIOW16(&iot);
    }
    else if (!_strcmpi(cmd, "i2cr16"))
    {
        result = processI2CR16(&iot);
    }
    else if (!_strcmpi(cmd, "i2cw16"))
    {
        result = processI2CW16(&iot);
    }
    else if (!_strcmpi(cmd, "ior"))
        result = processIOR(&iot);
    else if (!_strcmpi(cmd, "iow"))
    {
        result = processIOW(&iot);
    }
    else if (!_strcmpi(cmd, "i2cr"))
    {
        result = processI2CR(&iot);
    }
    else if (!_strcmpi(cmd, "i2cw"))
    {
        result = processI2CW(&iot);
    }
    else if (!_strcmpi(cmd, "rem") || !_strcmpi(cmd, "#") || !_strcmpi(cmd, ";"))
    {
        /* treat this as comment */
        result = TRUE;
    }
    else if (!_strcmpi(cmd, "pause"))
    {
        /* if no comment, display "press.." otherwise will display it */
        if (strlen(pLine) <= 5)
        {
            if (AfxMessageBox(_T("Press OK to continue"), MB_ICONINFORMATION | MB_OKCANCEL) != IDOK)
            {
                //this->t_flag = false;
            }
        }
        else
        {
            char *info = pLine + 5;
            CString str(pLine + 5);
            if (AfxMessageBox(str, MB_ICONINFORMATION | MB_OKCANCEL) != IDOK)
            {
                //this->t_flag = false;
            }
        }
    }
    else if (!_strcmpi(cmd, "delay"))
    {

        Sleep(iot.parm1);
        result = TRUE;
    }
    else
    {
        if (*pLine == '#')
        {
            /* Comment line. Just skip! */
            result = TRUE;
        }
        else
        {
            if (*pLine == 'l' || *pLine == 'L')
            {
                /* log */
                stat.Format(_T("***** %S *****"), pLine + 1);
                Log(stat);
                result = TRUE;
            }
            else
            {
                stat.Format(_T("*** Unknown cmd %S ***"), pLine);
                Log(stat);
                result = FALSE;
            }   
        }
    }

    return result;

}

BOOL CMS1850Register::processIOR(IOTYPE_T *pIO)
{
    int     i;
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 1)
    {
        /* ior <port> */
        pIO->parm3 = 0;
        if (!puLnk->ior((BYTE)pIO->parm1, (BYTE*)&pIO->parm3))
            stat.Format(_T("ior %02x -> %02x"), pIO->parm1, pIO->parm3);
        else
            stat.Format(_T("ior %02x -> failed"), pIO->parm1);
        Log(stat);
    }
    else if (pIO->fields == 2)
    {
        /* ior <port> <repeat>, for shell only */
        for (i = 0; i<(int)pIO->parm2; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->ior((WORD)pIO->parm1 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("ior %02x -> %02x"), pIO->parm1 + i, pIO->parm4);
            else
                stat.Format(_T("ior %02x -> failed"), pIO->parm1 + i);
            Log(stat);
        }
    }
    else
    {
        stat.Format(_T("*** ior parameters missing ***"));
        Log(stat);
        result = FALSE;
    }

    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processIOW(IOTYPE_T *pIO)
{
    CString stat;
    BOOL result = TRUE;

    if (pIO->fields == 2)
    {
        /* iow <port> <value> */
        if (!puLnk->iow((BYTE)pIO->parm1, (BYTE)pIO->parm2))
            stat.Format(_T("iow %02x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("iow %02x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* iow <port> <value> <mask> */
        if (!puLnk->iow((BYTE)pIO->parm1, (BYTE)pIO->parm2, (BYTE)~pIO->parm3))
            stat.Format(_T("iow %02x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("iow %02x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** ior parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processI2CR(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;
    int     i, j;
    BYTE pdata[0xffff];

    if (pIO->fields == 2)
    {
        /* i2cr <addr> <subaddr> */
        pIO->parm3 = 0;
        if (!puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2, (BYTE*)&pIO->parm3))
            stat.Format(_T("i2cr %02x %02x -> %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cr %02x %02x -> failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cr <addr> <subaddr> <brust> */
        for (i = 0; i<(int)pIO->parm3; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("i2cr %02x %02x -> %02x"), pIO->parm1, pIO->parm2 + i, pIO->parm4);
            else
                stat.Format(_T("i2cr %02x %02x -> failed"), pIO->parm1, pIO->parm2 + i);
            Log(stat);
        }
    }
    else if (pIO->fields == 4)
    {
        /* i2cr <addr> <subaddr> <brust> <repeat> */
        for (j = 0; j<(int)pIO->parm3; j++)
        {
            puLnk->i2cr((int)pIO->parm1, (int)pIO->parm2 + j, (byte)pIO->parm4, pdata);
            for (i = 0; i<(int)pIO->parm4; i++)
            {
                stat.Format(_T("i2cr %02x %02x -> byte: %02x %02x"), pIO->parm1, pIO->parm2 + j, i, pdata[i]);
                Log(stat);
                //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
            }
        }
    }
    else
    {
        stat.Format(_T("*** i2cr parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processI2CW(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 2)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2))
            stat.Format(_T("i2cw %02x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("i2cw %02x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3))
            stat.Format(_T("i2cw %02x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cw %02x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else if (pIO->fields == 4)
    {
        /* i2cw <addr> <subaddr> <value> <mask>*/
        if (!puLnk->i2cw((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3, (BYTE)~pIO->parm4))
            stat.Format(_T("i2cw %02x %02x %02x, mask = %02x"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        else
            stat.Format(_T("i2cw %02x %02x %02x, mask = %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** i2cw parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

//16bit mode
BOOL CMS1850Register::processI2CR16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;
    int     i, j;
    BYTE pdata[0xffff];

    if (pIO->fields == 2)
    {
        /* i2cr <addr> <subaddr> */
        pIO->parm3 = 0;
        if (!puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2, (BYTE*)&pIO->parm3))
            stat.Format(_T("i2cr16 %02x %04x -> %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cr16 %02x %04x -> failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* i2cr <addr> <subaddr> <repeat> */
        for (i = 0; i<(int)pIO->parm3; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2 + i, (BYTE*)&pIO->parm4))
                stat.Format(_T("i2cr16 %02x %04x -> %02x"), pIO->parm1, pIO->parm2 + i, pIO->parm4);
            else
                stat.Format(_T("i2cr16 %02x %04x -> failed"), pIO->parm1, pIO->parm2 + i);
            Log(stat);
        }
    }
    else if (pIO->fields == 4)
    {
        /* i2cr <addr> <subaddr> <brust> <repeat> */
        for (j = 0; j<(int)pIO->parm3; j++)
        {
            puLnk->i2cr16((int)pIO->parm1, (int)pIO->parm2 + j, (byte)pIO->parm4, pdata);
            for (i = 0; i<(int)pIO->parm4; i++)
            {
                stat.Format(_T("i2cr %04x %02x -> byte: %02x %02x"), pIO->parm1, pIO->parm2 + j, i, pdata[i]);
                Log(stat);
                //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
            }
        }
    }
    else
    {
        stat.Format(_T("*** i2cr16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processI2CW16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 3)
    {
        /* i2cw <addr> <subaddr> <value> */
        if (!puLnk->i2cw16((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3))
            stat.Format(_T("i2cw16 %02x %04x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("i2cw16 %02x %04x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else if (pIO->fields == 4)
    {
        /* i2cw <addr> <subaddr> <value> <mask>*/
        if (!puLnk->i2cw16((int)pIO->parm1, (int)pIO->parm2, (int)pIO->parm3, (BYTE)~pIO->parm4))
            stat.Format(_T("i2cw16 %02x %04x %02x, mask = %02x"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        else
            stat.Format(_T("i2cw16 %02x %04x %02x, mask = %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3, pIO->parm4);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** i2cw16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    // m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processIOR16(IOTYPE_T *pIO)
{
    int     i;
    CString stat;
    BOOL    result = TRUE;

    if (pIO->fields == 1)
    {
        /* ior <port> */
        pIO->parm3 = 0;
        if (!puLnk->ior16((WORD)pIO->parm1, (BYTE*)&pIO->parm3))
            stat.Format(_T("ior16 %04x -> %02x"), pIO->parm1, pIO->parm3);
        else
            stat.Format(_T("ior16 %04x -> failed"), pIO->parm1);
        Log(stat);
    }
    else if (pIO->fields == 2)
    {
        /* ior <port> <repeat>, for shell only */
        for (i = 0; i<(int)pIO->parm2; i++)
        {
            pIO->parm4 = 0;
            if (!puLnk->ior16((WORD)(pIO->parm1 + i), (BYTE*)&pIO->parm4))
                stat.Format(_T("ior16 %04x -> %02x"), pIO->parm1 + i, pIO->parm4);
            else
                stat.Format(_T("ior16 %04x -> failed"), pIO->parm1 + i);
            Log(stat);
        }
    }
    else
    {
        stat.Format(_T("*** ior16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }

    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}

BOOL CMS1850Register::processIOW16(IOTYPE_T *pIO)
{
    CString stat;
    BOOL result = TRUE;

    if (pIO->fields == 2)
    {
        /* iow <port> <value> */
        if (!puLnk->iow16((WORD)pIO->parm1, (BYTE)pIO->parm2))
            stat.Format(_T("iow16 %04x %02x"), pIO->parm1, pIO->parm2);
        else
            stat.Format(_T("iow16 %04x %02x failed"), pIO->parm1, pIO->parm2);
        Log(stat);
    }
    else if (pIO->fields == 3)
    {
        /* iow <port> <value> <mask> */
        if (!puLnk->iow16((WORD)pIO->parm1, (BYTE)pIO->parm2, (BYTE)~pIO->parm3))
            stat.Format(_T("iow16 %04x %02x %02x"), pIO->parm1, pIO->parm2, pIO->parm3);
        else
            stat.Format(_T("iow16 %04x %02x %02x failed"), pIO->parm1, pIO->parm2, pIO->parm3);
        Log(stat);
    }
    else
    {
        stat.Format(_T("*** iow16 parameters missing ***"));
        Log(stat);
        result = FALSE;
    }
    //m_Log.EnsureVisible(m_Log.GetItemCount() - 1, TRUE);
    return result;
}