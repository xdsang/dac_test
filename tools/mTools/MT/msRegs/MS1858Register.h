// MS1858Register.h: interface for the CMS1858Register class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MS1858REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_)
#define AFX_MS1858REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MSRegister.h"
//#include "TextReg16.h"
#include "ProgressWnd.h"

class CMS1858Register : public CMSRegister
{
public:
    virtual TCHAR *get_chipname();
    virtual void save_bat(FILE* fp);
    virtual void load_bat(FILE *);
    virtual void save_txt(FILE*);
    virtual void load_txt(FILE*);
    virtual bool check_chipid();
    virtual BYTE get_bank();
    CMS1858Register();
    ~CMS1858Register();
    
protected:

};

#endif // !defined(AFX_MS1858REGISTER_H__618F22A5_63A4_447D_8A4B_B1CECBAF4064__INCLUDED_)
