#pragma once
#include <string>
using namespace std;

#define MAX_RANGE (0x10000)

class CParseTXT
{
public:
    CParseTXT();
    ~CParseTXT();

public:
    // functions
    void parse_txt_file(FILE*);

    int save_bat_file(FILE*, char* title);
    int save_txt_file(FILE*, char* title);

private:
    LONG64 add_value(LONG64, char, int);
    void save_reg_to_rom();
    void save_module_name();
    int check_txt_lines(FILE*);
    void init_data();

public:
    // temp rom
    string module_name[MAX_RANGE];  //chip module name
    string reg_name[MAX_RANGE];  // register name
    LONG64 reg_value[MAX_RANGE];  //register value
    BYTE reg_st[MAX_RANGE];  //register start bit
    int reg_length[MAX_RANGE];  //register length
    string reg_attribute[MAX_RANGE];  //regitser attribute
    WORD reg_addr[MAX_RANGE];  //register address
    string reg_description[MAX_RANGE]; // register description information
    string reg_module_name[MAX_RANGE];  // The module ID of the register

private:
    // temp line value
    int  line_count;
    LONG64  temp_value;
    int   temp_length;
    string temp_module_name;
    string  temp_name;
    string temp_description;
    string temp_attribute;
    WORD  temp_port;
    BYTE  temp_bit;

    // temp line
    char temp_line[1024];

    WORD module_num;
    WORD reg_num;
};

extern CParseTXT m_parsetxt;
