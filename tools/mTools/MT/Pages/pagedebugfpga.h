#if !defined(AFX_PAGEDEBUGFPGA_H__5499D717_78CB_4CB1_81BF_EDD1E3B0B1A1__INCLUDED_)
#define AFX_PAGEDEBUGFPGA_H__5499D717_78CB_4CB1_81BF_EDD1E3B0B1A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// pagedebugfpga.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageDebugFPGA form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPageDebugFPGA : public CFormView
{
protected:
    CPageDebugFPGA();           // protected constructor used by dynamic creation
    DECLARE_DYNCREATE(CPageDebugFPGA)

    // Form Data
public:
    //{{AFX_DATA(CPageDebugFPGA)
    enum { IDD = IDD_PAGE_DEBUG_FPGA };
    // NOTE: the ClassWizard will add data members here
//}}AFX_DATA

// Attributes
public:

    // Operations
public:

    // Overrides
        // ClassWizard generated virtual function overrides
        //{{AFX_VIRTUAL(CPageDebugFPGA)
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    virtual ~CPageDebugFPGA();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

    // Generated message map functions
    //{{AFX_MSG(CPageDebugFPGA)
    afx_msg void OnFpgaSysSetup();
    afx_msg void OnFpgaSysOutresSwitch();
    afx_msg void OnFpgaSysStart();
    afx_msg void OnFpgaSysVideoMuteEnable();
    afx_msg void OnFpgaSysMutePattern();
    afx_msg void OnFpgaSysBrightnessEnable();
    afx_msg void OnFpgaSysContrastEnable();
    afx_msg void OnFpgaSysSaturationEnable();
    afx_msg void OnFpgaSysHueEnable();
    afx_msg void OnFpgaSysBrightnessDec();
    afx_msg void OnFpgaSysBrightnessInc();
    afx_msg void OnFpgaSysContrastDec();
    afx_msg void OnFpgaSysContrastInc();
    afx_msg void OnFpgaSysSaturationDec();
    afx_msg void OnFpgaSysSaturationInc();
    afx_msg void OnFpgaSysHueDec();
    afx_msg void OnFpgaSysHueInc();
    afx_msg void OnFpgaSysPurdahOff();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnFpgaSysPurdahOn();
    afx_msg void OnFpgaSysScreen43();
    afx_msg void OnFpgaSysScreen169();
    afx_msg void OnFpgaSysScreen1610();
    afx_msg void OnFpgaSysScreenFull();
    afx_msg void OnFpgaSysMultiWin4();
    afx_msg void OnFpgaSysMultiWin9();
    afx_msg void OnFpgaSysMultiWin16();
    afx_msg void OnFpgaSysPopEnable();
    afx_msg void OnFpgaSysPopSizeSw();
    afx_msg void OnFpgaSysPopPosSw();
    afx_msg void OnChangeFpgaSysVdsClk();
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnFpgaSysReadBinfileVersion();
    afx_msg void OnFpgaSysPixelShiftDec();
    afx_msg void OnFpgaSysPixelShiftInc();
    afx_msg void OnFpgaSysInport();
    afx_msg void OnFpgaSysLineShiftDec();
    afx_msg void OnFpgaSysLineShiftInc();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEDEBUGFPGA_H__5499D717_78CB_4CB1_81BF_EDD1E3B0B1A1__INCLUDED_)
