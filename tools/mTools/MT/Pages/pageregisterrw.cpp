// pageregisterrw.cpp : implementation file
//

#include "stdafx.h"
#include "mtools.h"

#include "MSRegister.h"
#include "MSLnkU.h"
#include "IoCmd.h"

#include "PageRegisterRW.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageRegisterRW

IMPLEMENT_DYNCREATE(CPageRegisterRW, CFormView)

CPageRegisterRW::CPageRegisterRW()
    : CFormView(CPageRegisterRW::IDD)
{
    //{{AFX_DATA_INIT(CPageRegisterRW)
    //}}AFX_DATA_INIT
}

CPageRegisterRW::~CPageRegisterRW()
{
}

void CPageRegisterRW::DoDataExchange(CDataExchange* pDX)
{
    CFormView::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CPageRegisterRW)
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageRegisterRW, CFormView)
    //{{AFX_MSG_MAP(CPageRegisterRW)
    ON_WM_SHOWWINDOW()
    ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SUB_ADDRESS, OnDeltaposSpinSubAddress)
    ON_EN_CHANGE(IDC_EDIT_CHIP_ADDRESS, OnChangeEditChipAddress)
    ON_EN_CHANGE(IDC_EDIT_SUB_ADDRESS, OnChangeEditSubAddress)
    ON_BN_CLICKED(IDC_RADIO_8BITS, OnRadio8bits)
    ON_BN_CLICKED(IDC_RADIO_16BITS, OnRadio16bits)
    ON_EN_CHANGE(IDC_REGISTER_VALUE, OnChangeRegisterValue)
    ON_BN_CLICKED(IDC_CHECK_B0, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B1, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B2, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B3, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B4, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B5, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B6, OnCheckBITs)
    ON_BN_CLICKED(IDC_CHECK_B7, OnCheckBITs)
    ON_BN_CLICKED(IDC_BUTTON_ALL, OnButtonAll)
    ON_EN_SETFOCUS(IDC_EDIT_SUB_ADDRESS, OnSetfocusEditSubAddress)
    ON_EN_SETFOCUS(IDC_REGISTER_VALUE, OnSetfocusRegisterValue)
    ON_BN_CLICKED(IDC_BUTTON_READ, OnButtonRead)
    ON_BN_CLICKED(IDC_BUTTON_WRITE, OnButtonWrite)
    ON_EN_SETFOCUS(IDC_EDIT_COMMAND, OnSetfocusEditCommand)
    ON_BN_CLICKED(IDC_BUTTON_ENTER, OnButtonEnter)
    ON_EN_CHANGE(IDC_EDIT_COMMAND, OnChangeEditCommand)
    ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
    ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageRegisterRW diagnostics

#ifdef _DEBUG
void CPageRegisterRW::AssertValid() const
{
    CFormView::AssertValid();
}

void CPageRegisterRW::Dump(CDumpContext& dc) const
{
    CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageRegisterRW message handlers

void CPageRegisterRW::OnInitDialog()
{
    CString str;
    CButton * p_btn8bits;
    CButton * p_btn16bits;

    p_btn8bits = (CButton *)GetDlgItem(IDC_RADIO_8BITS);
    p_btn16bits = (CButton *)GetDlgItem(IDC_RADIO_16BITS);
    if(m_register_is_16bit)
    {
        p_btn8bits->SetCheck(0);
        p_btn16bits->SetCheck(1);
        ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetLimitText(4);
    }
    else
    {
        p_btn8bits->SetCheck(1);
        p_btn16bits->SetCheck(0);
        ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetLimitText(2);
    }

    str.Format(_T("%X"), m_regiter_sub_address);
    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetWindowText(str);

    m_regiter_chip_address = puLnk->GetI2CAddr();
    ((CEdit *)GetDlgItem(IDC_EDIT_CHIP_ADDRESS))->SetLimitText(2);
    str.Format(_T("%X"), m_regiter_chip_address);
    ((CEdit *)GetDlgItem(IDC_EDIT_CHIP_ADDRESS))->SetWindowText(str);


    ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetLimitText(2);
    str.Format(_T("%X"), m_register_value);
    ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetWindowText(str);

    ((CEdit *)GetDlgItem(IDC_EDIT_COMMAND))->SetWindowText(m_str_command);
    
    SetCheckBITs();
}

void CPageRegisterRW::OnShowWindow(BOOL bShow, UINT nStatus) 
{
    CFormView::OnShowWindow(bShow, nStatus);
    
    // TODO: Add your message handler code here
    OnInitDialog();

}

void CPageRegisterRW::OnDeltaposSpinSubAddress(NMHDR* pNMHDR, LRESULT* pResult) 
{
    NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
    // TODO: Add your control notification handler code here
    CString str;
    int sub_address_max = (m_register_is_16bit) ? 0xffff : 0xff;
    
    if(pNMUpDown->iDelta == 1)        // 如果此值为1 , 说明点击了Spin的往下箭头	
    {
        if(m_regiter_sub_address > 0)
            m_regiter_sub_address--;
    }
    else if(pNMUpDown->iDelta == -1) // 如果此值为-1 , 说明点击了Spin的往上箭头	
    {
        if(m_regiter_sub_address < sub_address_max)
            m_regiter_sub_address++;
    }

    str.Format(_T("%X"), m_regiter_sub_address);
    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetWindowText(str);

    *pResult = 0;
}

void CPageRegisterRW::OnChangeEditChipAddress() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CFormView::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    CString str;
    int iValue = 0;
    int iRet = 0;
    
    ((CEdit *)GetDlgItem(IDC_EDIT_CHIP_ADDRESS))->GetWindowText(str);
    
    iRet = _stscanf(str.GetBuffer(20), _T("%X"), &iValue);
    str.ReleaseBuffer();
    
    for (int i=0; i<str.GetLength(); i++)
    {
        if (!((str[i] >= '0' && str[i]<='9') ||
            (str[i] >= 'A' && str[i]<='F')))
        {
            AfxMessageBox(_T("Invalid parameters"));
            str.Format(_T("%X"), iValue);
            ((CEdit *)GetDlgItem(IDC_EDIT_CHIP_ADDRESS))->SetWindowText(str);
            return;
        }
    }
    
    m_regiter_chip_address = (BYTE)iValue;
}

void CPageRegisterRW::OnChangeEditSubAddress() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CFormView::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    CString str;
    int iValue = 0;
    int iRet = 0;
    
    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->GetWindowText(str);
    
    iRet = _stscanf(str.GetBuffer(20), _T("%X"), &iValue);
    str.ReleaseBuffer();
    
    for (int i=0; i<str.GetLength(); i++)
    {
        if (!((str[i] >= '0' && str[i]<='9') ||
            (str[i] >= 'A' && str[i]<='F')))
        {
            AfxMessageBox(_T("Invalid parameters"));
            str.Format(_T("%X"), iValue);
            ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetWindowText(str);
            return;
        }
    }
    
    m_regiter_sub_address = iValue;
}

void CPageRegisterRW::OnRadio8bits() 
{
    m_register_is_16bit = FALSE;

    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetLimitText(2);
    
    CString str;
    str.Format(_T("%X"), (BYTE)m_regiter_sub_address);
    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetWindowText(str);
}

void CPageRegisterRW::OnRadio16bits() 
{
    // TODO: Add your control notification handler code here
    m_register_is_16bit = TRUE;

    ((CEdit *)GetDlgItem(IDC_EDIT_SUB_ADDRESS))->SetLimitText(4);
}

void CPageRegisterRW::OnChangeRegisterValue() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CFormView::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    CString str;
    int iValue = 0;
    int iRet = 0;
    
    ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->GetWindowText(str);

    //iRet = sscanf(str.GetBuffer(20), _T("%X"), &iValue);  //会出错
    iRet = _stscanf(str.GetBuffer(20), _T("%X"), &iValue);  //将buffer中的数据以给定格式写入到指定的变量中
    str.ReleaseBuffer();

    for (int i=0; i<str.GetLength(); i++)
    {
        if (!((str[i] >= '0' && str[i]<='9') ||
            (str[i] >= 'A' && str[i]<='F')))
        {
            AfxMessageBox(_T("Invalid parameters"));
            str.Format(_T("%x"), iValue);
            ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetWindowText(str);
            return;
        }
    }
    
    m_register_value = (BYTE)iValue;
    SetCheckBITs();
}

void CPageRegisterRW::SetCheckBITs()
{
    ((CButton *)GetDlgItem(IDC_CHECK_B0))->SetCheck(m_register_value & 0x01);
    ((CButton *)GetDlgItem(IDC_CHECK_B1))->SetCheck(m_register_value & 0x02);
    ((CButton *)GetDlgItem(IDC_CHECK_B2))->SetCheck(m_register_value & 0x04);
    ((CButton *)GetDlgItem(IDC_CHECK_B3))->SetCheck(m_register_value & 0x08);
    ((CButton *)GetDlgItem(IDC_CHECK_B4))->SetCheck(m_register_value & 0x10);
    ((CButton *)GetDlgItem(IDC_CHECK_B5))->SetCheck(m_register_value & 0x20);
    ((CButton *)GetDlgItem(IDC_CHECK_B6))->SetCheck(m_register_value & 0x40);
    ((CButton *)GetDlgItem(IDC_CHECK_B7))->SetCheck(m_register_value & 0x80);
}

void CPageRegisterRW::OnCheckBITs() 
{
    CString str;

    m_register_value = 0;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B0))->GetCheck() ? 0x01 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B1))->GetCheck() ? 0x02 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B2))->GetCheck() ? 0x04 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B3))->GetCheck() ? 0x08 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B4))->GetCheck() ? 0x10 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B5))->GetCheck() ? 0x20 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B6))->GetCheck() ? 0x40 : 0x00;
    m_register_value |= ((CButton *)GetDlgItem(IDC_CHECK_B7))->GetCheck() ? 0x80 : 0x00;

    str.Format(_T("%02X"), m_register_value);
    ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetWindowText(str);
}

void CPageRegisterRW::OnButtonAll() 
{
    CString str;
    
    m_register_value = (m_register_value != 0xFF) ? 0xFF : 0x00;
    SetCheckBITs();
    
    str.Format(_T("%02X"), m_register_value);
    ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetWindowText(str);
}

void CPageRegisterRW::OnSetfocusEditSubAddress() 
{
    ((CDialog *) this)->SetDefID(IDC_BUTTON_READ);
}

void CPageRegisterRW::OnSetfocusRegisterValue() 
{
    ((CDialog *) this)->SetDefID(IDC_BUTTON_WRITE);
}

void CPageRegisterRW::OnButtonRead() 
{
    CString str;
    IOTYPE_T  iot = {0};
    BOOL    result = FALSE;

    iot.fields = 2;
    iot.parm1 = m_regiter_chip_address;
    iot.parm2 = m_regiter_sub_address;
    if (m_register_is_16bit)
    {
        result = g_IoCmd.processI2CR16(&iot);
    }
    else
    {
        result = g_IoCmd.processI2CR(&iot);
    }

    if (result)
    {
        m_register_value = (BYTE)iot.parm3;
        
        SetCheckBITs();
        str.Format(_T("%02X"), m_register_value);
        ((CEdit *)GetDlgItem(IDC_REGISTER_VALUE))->SetWindowText(str);
    }
}

void CPageRegisterRW::OnButtonWrite() 
{
    IOTYPE_T  iot;
    BOOL    result = FALSE;

    iot.fields = 3;
    iot.parm1 = m_regiter_chip_address;
    iot.parm2 = m_regiter_sub_address;
    iot.parm3 = m_register_value;
    
    if (m_register_is_16bit)
    {
        result = g_IoCmd.processI2CW16(&iot);
    }
    else
    {
        result = g_IoCmd.processI2CW(&iot);
    }

}

void CPageRegisterRW::OnSetfocusEditCommand() 
{
    ((CDialog *) this)->SetDefID(IDC_BUTTON_ENTER);
}

void CPageRegisterRW::OnButtonEnter() 
{
    size_t origsize = wcslen(m_str_command) + 1;
    size_t convertedChars = 0;
    const size_t newsize = origsize * 2;
    char *nstring = new char[newsize];
    wcstombs_s(&convertedChars, nstring, newsize, m_str_command, _TRUNCATE);

    g_IoCmd.executeLine(nstring);
    delete(nstring);
}

void CPageRegisterRW::OnChangeEditCommand() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CFormView::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    ((CEdit *)GetDlgItem(IDC_EDIT_COMMAND))->GetWindowText(m_str_command);
}

void CPageRegisterRW::OnButtonLoad() 
{
    CString strDefaultName;
    strDefaultName = pMSREG->get_chipname();

    CString filename, extension;
    //char szFilter[] = "All Files (*.*)|*.*|Text File(*.txt)|*.txt|Batch File(*.bat)|*.bat|";
    LPCTSTR szFilter = _T("All Files (*.*)|*.*|Text File(*.txt)|*.txt|Batch File(*.bat)|*.bat|");
    CFileDialog filedlg(TRUE, _T("txt"), strDefaultName, OFN_FILEMUSTEXIST , szFilter, AfxGetMainWnd());
    FILE *fp  = NULL;
    CString log;
    
    if (filedlg.DoModal()==IDOK) {
        filename = filedlg.GetPathName();
        extension = filedlg.GetFileExt();

        size_t origsize_f = wcslen(filename) + 1;
        size_t convertedChars_f = 0;
        const size_t newsize_f = origsize_f * 2;
        char *nstring_f = new char[newsize_f];
        wcstombs_s(&convertedChars_f, nstring_f, newsize_f, filename, _TRUNCATE);
        fopen_s(&fp, nstring_f, "rt");

        if (fp) 
        {
            size_t origsize_e = wcslen(extension) + 1;
            size_t convertedChars_e = 0;
            const size_t newsize_e = origsize_e * 2;
            char *nstring_e = new char[newsize_e];
            wcstombs_s(&convertedChars_e, nstring_e, newsize_e, extension, _TRUNCATE);

            log = filedlg.GetPathName();
            GetDlgItem(IDC_LOAD_FILE_PATH)->SetWindowText(log);
            Log(log);

            if(!_strcmpi(nstring_e, "txt"))
            {
                log.Format(_T("start to load txt file -- "));
                log = log + filename;
                Log(log);
                pMSREG->load_txt(fp);
                fclose(fp);
                log.Format(_T("#### OK ####  load txt file done -- "));
                log = log + filename;
                Log(log);
            }
            else if(!_strcmpi(nstring_e, "bat"))
            {
                log.Format(_T("start to load bat file -- "));
                log = log + filename;
                Log(log);
                pMSREG->load_bat(fp);
                fclose(fp);
                log.Format(_T("#### OK ####  load bat file done -- "));
                log = log + filename;
                Log(log);
            }
        }
        else 
        {
            MessageBox(TEXT("Open file failed"), TEXT("Error"), MB_OK);
        }
    }
    return;
}

void CPageRegisterRW::OnButtonSave() 
{
    CString strDefaultName;
    strDefaultName = _T("Save");

    CString filename, extension;
    FILE* fp = NULL;
    CString log;
    
    //char szFilter[] = "Text File(*.txt)|*.txt|Batch File(*.bat)|*.bat|";	
    LPCTSTR szFilter = _T("All Files (*.*)|*.*|Text File(*.txt)|*.txt|Batch File(*.bat)|*.bat|");
    CFileDialog filedlg(false, _T("txt"), strDefaultName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT , szFilter, AfxGetMainWnd());
    
    if (filedlg.DoModal()==IDOK) {
        filename = filedlg.GetPathName();
        extension = filedlg.GetFileExt();

        size_t origsize_f = wcslen(filename) + 1;
        size_t convertedChars_f = 0;
        const size_t newsize_f = origsize_f * 2;
        char *nstring_f = new char[newsize_f];
        wcstombs_s(&convertedChars_f, nstring_f, newsize_f, filename, _TRUNCATE);
        fopen_s(&fp, nstring_f, "wt+");
        delete(nstring_f);

        if (fp) 
        {
            size_t origsize_e = wcslen(extension) + 1;
            size_t convertedChars_e = 0;
            const size_t newsize_e = origsize_e * 2;
            char *nstring_e = new char[newsize_e];
            wcstombs_s(&convertedChars_e, nstring_e, newsize_e, extension, _TRUNCATE);
            log = filedlg.GetPathName();
            GetDlgItem(IDC_SAVE_FILE_PATH)->SetWindowText(log);
            Log(log);

            if(!_strcmpi(nstring_e, "txt"))
            {
                log.Format(_T("save to txt file -- "));
                log = log + filename;
                Log(log);
                
                pMSREG->save_txt(fp);
                
                fclose(fp);
                log.Format(_T("save to txt file done -- "));
                log = log + filename;
                Log(log);
            }
            else if(!_strcmpi(nstring_e, "bat"))
            {
                log.Format(_T("save to bat file -- "));
                log = log + filename;
                Log(log);
                
                pMSREG->save_bat(fp);
                
                fclose(fp);
                log.Format(_T("save to bat file done -- "));
                log = log + filename;
                Log(log);
            }
            delete(nstring_e);
        }
        else 
        {
            MessageBox(TEXT("Open file failed"), TEXT("Error"), MB_OK);
        }
    }
}
