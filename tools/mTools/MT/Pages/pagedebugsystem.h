#if !defined(AFX_PAGEDEBUGSYSTEM_H__68A49A37_723F_41D0_BD4C_528ED09DE1CD__INCLUDED_)
#define AFX_PAGEDEBUGSYSTEM_H__68A49A37_723F_41D0_BD4C_528ED09DE1CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// pagedebugsystem.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageDebugSystem form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPageDebugSystem : public CFormView
{
protected:
	CPageDebugSystem();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageDebugSystem)

// Form Data
public:
	//{{AFX_DATA(CPageDebugSystem)
	enum { IDD = IDD_PAGE_DEBUG_SYSTEM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageDebugSystem)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageDebugSystem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageDebugSystem)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEDEBUGSYSTEM_H__68A49A37_723F_41D0_BD4C_528ED09DE1CD__INCLUDED_)
