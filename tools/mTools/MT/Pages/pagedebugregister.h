#if !defined(AFX_PAGEDEBUGREGISTER_H__99A8FFFB_0D49_4FD0_8A23_07C342068123__INCLUDED_)
#define AFX_PAGEDEBUGREGISTER_H__99A8FFFB_0D49_4FD0_8A23_07C342068123__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// pagedebugregister.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageDebugRegister form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "..\\RegRW\\ms1858regrw.h"

class CPageDebugRegister : public CFormView
{
protected:
	CPageDebugRegister();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageDebugRegister)

// Form Data
public:
	//{{AFX_DATA(CPageDebugRegister)
	enum { IDD = IDD_PAGE_DEBUG_REGISTER };
	CProgressCtrl	m_control_progress;
	CEdit	m_EditName;
	CString	m_SheetName;
	//}}AFX_DATA

// Attributes
public:

private:
	CMS1858RegRW * pRegRW;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageDebugRegister)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageDebugRegister();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageDebugRegister)
	afx_msg void OnButtonRegisterCheck();
	afx_msg void OnButtonRegisterCheckStop();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheckRegisterFileSelect();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnButtonRegisterSaveTemlate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEDEBUGREGISTER_H__99A8FFFB_0D49_4FD0_8A23_07C342068123__INCLUDED_)
