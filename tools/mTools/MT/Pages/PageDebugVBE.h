#pragma once



// CPageDebugVBE 窗体视图

class CPageDebugVBE : public CFormView
{
    DECLARE_DYNCREATE(CPageDebugVBE)

protected:
    CPageDebugVBE();           // 动态创建所使用的受保护的构造函数
    virtual ~CPageDebugVBE();
    virtual void OnInitialUpdate(); //进入page只执行一次

public:
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_PAGE_DEBUG_VBE };
#endif
#ifdef _DEBUG
    virtual void AssertValid() const;
#ifndef _WIN32_WCE
    virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnCbnSelchangeComboMuxC();
    afx_msg void OnCbnSelchangeComboMuxY();
    afx_msg void OnCbnSelchangeComboMuxCvbs();
    afx_msg void OnBnClickedCheckBgpwr();
    afx_msg void OnBnClickedCheckClkpwr();
    afx_msg void OnBnClickedCheckCpwr();
    afx_msg void OnBnClickedCheckVdacypwr();
    afx_msg void OnBnClickedCheckVdaccvbspwr();
	afx_msg void OnBnClickedButtonTveUpdate();

	int m_vdac_c_used;
	int m_vdac_y_used;
	int m_vdac_cvbs_used;
	BOOL m_vdac_bgpwr;
	BOOL m_vdac_clkpwr;
	BOOL m_vdac_cpwr;
	BOOL m_vdac_ypwr;
	BOOL m_vdac_cvbspwr;

	CString m_str_dac_test_data;
	afx_msg void OnEnChangeEditDacRegTest();
	afx_msg void OnDeltaposSpinDacRegTest(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCheckVdacTestData3ff();
	afx_msg void OnBnClickedCheckVdacTestData000();
	BOOL m_dac_reg_data_3ff;
	BOOL m_dac_reg_data_000;
	afx_msg void OnBnClickedButton1();
	CString m_dac_reg_addr;
	afx_msg void OnEnChangeEditDacRegAddr();
	afx_msg void OnEnChangeEditMultimeterIpAddr();
	CString m_mulitmeter_ip_addr;
	afx_msg void OnBnClickedButtonSetMultimeterIpAddr();
};
