#if !defined(AFX_PAGEREGISTERRW_H__1071A8B2_0A31_4CAB_9AFA_45E702BFF8F1__INCLUDED_)
#define AFX_PAGEREGISTERRW_H__1071A8B2_0A31_4CAB_9AFA_45E702BFF8F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// pageregisterrw.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageRegisterRW form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPageRegisterRW : public CFormView
{
protected:
    CPageRegisterRW();           // protected constructor used by dynamic creation
    DECLARE_DYNCREATE(CPageRegisterRW)

// Form Data
public:
    //{{AFX_DATA(CPageRegisterRW)
    enum { IDD = IDD_PAGE_REGISTER_RW };
    //}}AFX_DATA

// Attributes
public:

private:
    CString m_str_command;
    BYTE m_regiter_chip_address;
    int m_regiter_sub_address = 0;
    BYTE m_register_value = 0;
    BOOL m_register_is_16bit = TRUE;

// Operations
public:

protected:

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CPageRegisterRW)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    void OnInitDialog();
    void SetCheckBITs();


    virtual ~CPageRegisterRW();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

    // Generated message map functions
    //{{AFX_MSG(CPageRegisterRW)
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnDeltaposSpinSubAddress(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnChangeEditChipAddress();
    afx_msg void OnChangeEditSubAddress();
    afx_msg void OnRadio8bits();
    afx_msg void OnRadio16bits();
    afx_msg void OnChangeRegisterValue();
    afx_msg void OnCheckBITs();
    afx_msg void OnButtonAll();
    afx_msg void OnSetfocusEditSubAddress();
    afx_msg void OnSetfocusRegisterValue();
    afx_msg void OnButtonRead();
    afx_msg void OnButtonWrite();
    afx_msg void OnSetfocusEditCommand();
    afx_msg void OnButtonEnter();
    afx_msg void OnChangeEditCommand();
    afx_msg void OnButtonLoad();
    afx_msg void OnButtonSave();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEREGISTERRW_H__1071A8B2_0A31_4CAB_9AFA_45E702BFF8F1__INCLUDED_)
