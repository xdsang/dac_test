// pagedebugregister.cpp : implementation file
//

#include "stdafx.h"
#include "..\mtools.h"
#include "pagedebugregister.h"

BOOL g_b_check_over = 1;
FILE *g_pf_reg_log;


///////////////////////////////////////
BOOL g_b_register_file_flag = 0;


#define REG_BUF_SIZE 0x10000
REGATTR_T g_st_arr_reg_buf[REG_BUF_SIZE]; 

REGATTR_T * p_st_register_buf;

#define internal_register_buf_size (sizeof(st_RegArray)/sizeof(st_RegArray[0]))

UINT16 g_u16_register_file_len = 0;
UINT16 g_u16_register_check_len = 0;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageDebugRegister

IMPLEMENT_DYNCREATE(CPageDebugRegister, CFormView)

CPageDebugRegister::CPageDebugRegister()
	: CFormView(CPageDebugRegister::IDD)
{
	//{{AFX_DATA_INIT(CPageDebugRegister)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPageDebugRegister::~CPageDebugRegister()
{
	CMS1858CheckRegRW Cms1858CheckRW;
	Cms1858CheckRW.CheckStart(0);
}

void CPageDebugRegister::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPageDebugRegister)
	DDX_Control(pDX, IDC_PROGRESS1, m_control_progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageDebugRegister, CFormView)
	//{{AFX_MSG_MAP(CPageDebugRegister)
	ON_BN_CLICKED(IDC_BUTTON_REGISTER_CHECK, OnButtonRegisterCheck)
	ON_BN_CLICKED(IDC_BUTTON_REGISTER_CHECK_STOP, OnButtonRegisterCheckStop)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_REGISTER_FILE_SELECT, OnCheckRegisterFileSelect)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_REGISTER_SVAE_TEMLATE, OnButtonRegisterSaveTemlate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageDebugRegister diagnostics

#ifdef _DEBUG
void CPageDebugRegister::AssertValid() const
{
	CFormView::AssertValid();
}

void CPageDebugRegister::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageDebugRegister message handlers

#define REG_TOP_BASE   0x0000
#define REG_PLLV_BASE  0x0180
#define REG_PLLVM_BASE 0x0100
#define REG_VADC_BASE  0x0080
#define REG_MEM_DMC_BASE     0x0500
#define REG_MEM_DFI_BASE     0x0580
#define REG_MEM_CAPPB_BASE   0x0600
#define REG_VDS_TIMGEN_BASE  0x0800
#define REG_VDS_SU_BASE      0x0800
#define REG_VDS_ENHANC_BASE  0x0880

#ifndef REG_DEINT_BASE
#define REG_DEINT_BASE		 0x0300
#endif
#ifndef REG_DI_RWFF_BASE
#define REG_DI_RWFF_BASE	 0x0380
#endif
#ifndef REG_SCALE_DOWN_BASE
#define REG_SCALE_DOWN_BASE	 0x0400
#endif

REGATTR_T st_RegArray[] = 
{

/*Top module registers*/
//clock & reset
	{REG_TOP_BASE+0x00, RO, 0, 8, 0xB8},
	{REG_TOP_BASE+0x01, RO, 0, 8, 0x85},
	{REG_TOP_BASE+0x02, RO, 0, 8, 0x8A},
#if 0
    {REG_TOP_BASE+0x03, RW, 0, 8, 0xA0},
    {REG_TOP_BASE+0x04, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x05, RW, 0, 1, 0x00},
    {REG_TOP_BASE+0x05, RW, 3, 5, 0x00},
    {REG_TOP_BASE+0x06, RW, 0, 4, 0x00},
    {REG_TOP_BASE+0x06, RW, 4, 3, 0x01},
    {REG_TOP_BASE+0x07, RW, 0, 8, 0x28},
    {REG_TOP_BASE+0x08, RW, 0, 8, 0x23},
    {REG_TOP_BASE+0x09, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x0a, RW, 0, 3, 0x00},
    {REG_TOP_BASE+0x0a, RW, 7, 1, 0x00},
    {REG_TOP_BASE+0x0b, RW, 0, 1, 0x00},
//misc
	{REG_TOP_BASE+0x10, RW, 0, 8, 0x00},
	{REG_TOP_BASE+0x11, RW, 0, 3, 0x00},
	{REG_TOP_BASE+0x26, RW, 0, 6, 0x00},
    {REG_TOP_BASE+0x27, RW, 0, 5, 0x00},
    {REG_TOP_BASE+0x27, RW, 7, 1, 0x00},
    {REG_TOP_BASE+0x28, RW, 0, 5, 0x00},
    {REG_TOP_BASE+0x39, RW, 0, 2, 0x00},
    {REG_TOP_BASE+0x3a, RW, 0, 1, 0x00},
    {REG_TOP_BASE+0x3b, RW, 0, 1, 0x00},
    {REG_TOP_BASE+0x3c, RW, 0, 1, 0x00},
    {REG_TOP_BASE+0x3d, RW, 0, 1, 0x00},
    {REG_TOP_BASE+0x41, RW, 0, 7, 0x00},
    {REG_TOP_BASE+0x42, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x43, RO, 0, 2, 0x00},
    {REG_TOP_BASE+0x44, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x45, RW, 0, 8, 0xFF},
    {REG_TOP_BASE+0x48, RW, 0, 8, 0x11},
    {REG_TOP_BASE+0x4f, RW, 0, 2, 0x00},
//freqm
	{REG_TOP_BASE+0x29, RW, 0, 4, 0x00},
	{REG_TOP_BASE+0x2a, RW, 0, 8, 0x00},
	{REG_TOP_BASE+0x2b, RW, 0, 5, 0x00},
    {REG_TOP_BASE+0x2c, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x2d, RW, 0, 5, 0x00},
    {REG_TOP_BASE+0x2e, RO, 0, 1, 0x00},
    {REG_TOP_BASE+0x2e, RO, 3, 5, 0x00},
    {REG_TOP_BASE+0x2f, RO, 0, 8, 0x00},
//mbist
    {REG_TOP_BASE+0x30, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x31, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x32, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x33, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x34, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x35, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x36, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x37, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x38, RO, 0, 8, 0x00},
//pad
    {REG_TOP_BASE+0x12, RW, 0, 8, 0x11},
    {REG_TOP_BASE+0x13, RW, 0, 8, 0x11},
    {REG_TOP_BASE+0x14, RW, 0, 8, 0x11},
    {REG_TOP_BASE+0x15, RW, 0, 8, 0x11},
    {REG_TOP_BASE+0x16, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x17, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x18, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x19, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x1a, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x1b, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x1c, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x1d, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x1e, RW, 0, 8, 0x7F},
    {REG_TOP_BASE+0x1f, RW, 0, 8, 0x80},
    {REG_TOP_BASE+0x20, RW, 0, 2, 0x00},
    {REG_TOP_BASE+0x20, RW, 4, 4, 0x00},
    {REG_TOP_BASE+0x21, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x4a, RO, 0, 8, 0x00},
    {REG_TOP_BASE+0x4b, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x4c, RW, 0, 8, 0x00},
    {REG_TOP_BASE+0x4d, RW, 0, 8, 0xFF},
    {REG_TOP_BASE+0x4e, RW, 0, 8, 0x00},
#endif

#if 0
//pllv
    {(REG_PLLV_BASE+0x00),  RW,  0,  8,  0x56},
    {(REG_PLLV_BASE+0x01),  RW,  0,  8,  0x07},
    {(REG_PLLV_BASE+0x02),  RW,  0,  12, 0x00},
    {(REG_PLLV_BASE+0x04),  RW,  0,  8,  0x00},
    {(REG_PLLV_BASE+0x05),  RW,  0,  8,  0x00},
    {(REG_PLLV_BASE+0x06),  RW,  0,  5,  0x00},
    {(REG_PLLV_BASE+0x07),  RW,  0,  8,  0x00},
    {(REG_PLLV_BASE+0x08),  RW,  0,  8,  0x00},
    {(REG_PLLV_BASE+0x09),  RW,  0,  6,  0x00},
    {(REG_PLLV_BASE+0x09),  RW,  7,  1,  0x01},
    {(REG_PLLV_BASE+0x0a),  RW,  0,  8,  0xe0},
    {(REG_PLLV_BASE+0x0b),  RW,  0,  8,  0x00},
    {(REG_PLLV_BASE+0x0c),  RW,  0,  6,  0x00},
    {(REG_PLLV_BASE+0x0d),  RW,  0,  7,  0x00},
    {(REG_PLLV_BASE+0x0e),  RW,  0,  1,  0x00},
//pllvm
    {(REG_PLLVM_BASE+0x00),  RW,  0,  8,  0x56},
    {(REG_PLLVM_BASE+0x01),  RW,  0,  8,  0x07},
    {(REG_PLLVM_BASE+0x02),  RW,  0,  12, 0x00},
    {(REG_PLLVM_BASE+0x04),  RW,  0,  8,  0x00},
    {(REG_PLLVM_BASE+0x05),  RW,  0,  8,  0x00},
    {(REG_PLLVM_BASE+0x06),  RW,  0,  5,  0x00},
    {(REG_PLLVM_BASE+0x07),  RW,  0,  8,  0x00},
    {(REG_PLLVM_BASE+0x08),  RW,  0,  8,  0x00},
    {(REG_PLLVM_BASE+0x09),  RW,  0,  6,  0x00},
    {(REG_PLLVM_BASE+0x09),  RW,  7,  1,  0x01},
    {(REG_PLLVM_BASE+0x0a),  RW,  0,  8,  0xe0},
    {(REG_PLLVM_BASE+0x0b),  RW,  0,  8,  0x00},
    {(REG_PLLVM_BASE+0x0c),  RW,  0,  6,  0x00},
    {(REG_PLLVM_BASE+0x0d),  RW,  0,  7,  0x00},
    {(REG_PLLVM_BASE+0x0e),  RW,  0,  1,  0x00},
//vadc
    {(REG_VADC_BASE+0x00),  RW,  0,  5,  0x10},
    {(REG_VADC_BASE+0x01),  RW,  0,  5,  0x10},
    {(REG_VADC_BASE+0x02),  RW,  0,  6,  0x24},
    {(REG_VADC_BASE+0x03),  RW,  0,  6,  0x24},
    {(REG_VADC_BASE+0x04),  RW,  0,  8,  0xc3},
    {(REG_VADC_BASE+0x05),  RW,  0,  4,  0x00},
    {(REG_VADC_BASE+0x06),  RW,  0,  2,  0x00},
    {(REG_VADC_BASE+0x06),  RW,  4,  4,  0x03},
    {(REG_VADC_BASE+0x07),  RW,  0,  2,  0x00},
    {(REG_VADC_BASE+0x07),  RW,  4,  4,  0x00},
    {(REG_VADC_BASE+0x08),  RW,  0,  5,  0x0b},
    {(REG_VADC_BASE+0x09),  RW,  0,  8,  0x00},
    {(REG_VADC_BASE+0x0a),  RW,  0,  8,  0xcc},
    {(REG_VADC_BASE+0x0b),  RW,  0,  3,  0x07},
    {(REG_VADC_BASE+0x0b),  RW,  4,  3,  0x07},
    {(REG_VADC_BASE+0x0c),  RW,  0,  4,  0x0f},
    {(REG_VADC_BASE+0x0d),  RW,  0,  1,  0x00},
    {(REG_VADC_BASE+0x0d),  RW,  2,  6,  0x00},
    {(REG_VADC_BASE+0x0e),  RW,  0,  3,  0x01},
    {(REG_VADC_BASE+0x0e),  RW,  4,  2,  0x00},
    {(REG_VADC_BASE+0x0f),  RW,  0,  2,  0x00},
#endif

//VFP
#if 0
	//De-interlace
	{(REG_DEINT_BASE+0x00), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x00), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x00), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x01), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x01), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x01), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x02), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x02), RW, 1, 2, 0x0},
	{(REG_DEINT_BASE+0x02), RW, 3, 1, 0x0},
	{(REG_DEINT_BASE+0x02), RW, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x03), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x03), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x04), RW, 0, 4, 0xc},
	{(REG_DEINT_BASE+0x04), RW, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x05), RW, 0, 7, 0x4},
	{(REG_DEINT_BASE+0x06), RW, 0, 1, 0x1},
	{(REG_DEINT_BASE+0x06), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x06), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x06), RW, 3, 1, 0x0},
	{(REG_DEINT_BASE+0x06), RW, 4, 2, 0x3},
	{(REG_DEINT_BASE+0x07), RW, 0, 7, 0x18},
	{(REG_DEINT_BASE+0x08), RW, 0, 7, 0x7f},
	{(REG_DEINT_BASE+0x09), RW, 0, 4, 0xf},
	{(REG_DEINT_BASE+0x09), RW, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x0a), RW, 0, 7, 0x4},
	{(REG_DEINT_BASE+0x0b), RW, 0, 5, 0x1a},
	{(REG_DEINT_BASE+0x0c), RW, 0, 5, 0x1a},
	{(REG_DEINT_BASE+0x0c), RW, 5, 1, 0x0},
	{(REG_DEINT_BASE+0x0d), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x0d), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x0e), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x0e), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x0e), RW, 4, 3, 0x4},
	{(REG_DEINT_BASE+0x0f), RW, 0, 7, 0xe},
	{(REG_DEINT_BASE+0x10), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x10), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x10), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x10), RW, 4, 3, 0x0},
	{(REG_DEINT_BASE+0x11), RW, 0, 8, 0x0},
	{(REG_DEINT_BASE+0x12), RW, 0, 8, 0x0},
	{(REG_DEINT_BASE+0x13), RW, 0, 2, 0x0},
	{(REG_DEINT_BASE+0x14), RW, 0, 8, 0x0},
	{(REG_DEINT_BASE+0x15), RW, 0, 8, 0x10},
	{(REG_DEINT_BASE+0x16), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x16), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x16), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x17), RW, 0, 4, 0x4},
	{(REG_DEINT_BASE+0x17), RW, 4, 2, 0x2},
	{(REG_DEINT_BASE+0x18), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x18), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x18), RW, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x19), RW, 0, 4, 0x2},
	{(REG_DEINT_BASE+0x19), RW, 4, 2, 0x0},
	{(REG_DEINT_BASE+0x1a), RW, 0, 8, 0x10},
	{(REG_DEINT_BASE+0x1b), RW, 0, 1, 0x1},
	{(REG_DEINT_BASE+0x1b), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x1c), RW, 0, 4, 0x5},
	{(REG_DEINT_BASE+0x1c), RW, 4, 3, 0x2},
	{(REG_DEINT_BASE+0x1d), RW, 0, 1, 0x1},
	{(REG_DEINT_BASE+0x1d), RW, 1, 1, 0x1},
	{(REG_DEINT_BASE+0x1d), RW, 3, 1, 0x0},
	{(REG_DEINT_BASE+0x1d), RW, 4, 1, 0x1},
	{(REG_DEINT_BASE+0x1e), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x1e), RW, 2, 2, 0x0},
	{(REG_DEINT_BASE+0x1e), RW, 4, 2, 0x0},
	{(REG_DEINT_BASE+0x1f), RW, 0, 7, 0x0},
	{(REG_DEINT_BASE+0x20), RW, 0, 4, 0x0},
	{(REG_DEINT_BASE+0x20), RW, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x21), RW, 0, 8, 0x5},
	{(REG_DEINT_BASE+0x22), RW, 0, 4, 0x0},
	{(REG_DEINT_BASE+0x22), RW, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x23), RW, 0, 8, 0x5},
	{(REG_DEINT_BASE+0x24), RW, 0, 8, 0x7},
	{(REG_DEINT_BASE+0x25), RW, 0, 1, 0x1},
	{(REG_DEINT_BASE+0x25), RW, 1, 1, 0x1},
	{(REG_DEINT_BASE+0x26), RW, 0, 4, 0x4},
	{(REG_DEINT_BASE+0x26), RW, 4, 3, 0x3},
	{(REG_DEINT_BASE+0x27), RW, 0, 4, 0x0},
	{(REG_DEINT_BASE+0x27), RW, 4, 3, 0x0},
	{(REG_DEINT_BASE+0x28), RW, 0, 4, 0x4},
	{(REG_DEINT_BASE+0x28), RW, 4, 4, 0x4},
	{(REG_DEINT_BASE+0x29), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x29), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x29), RW, 2, 1, 0x1},
	{(REG_DEINT_BASE+0x29), RW, 3, 1, 0x0},
	{(REG_DEINT_BASE+0x29), RW, 6, 1, 0x1},
	{(REG_DEINT_BASE+0x29), RW, 7, 1, 0x0},
	{(REG_DEINT_BASE+0x2a), RW, 0, 8, 0x3},
	{(REG_DEINT_BASE+0x2b), RW, 0, 8, 0xb},
	{(REG_DEINT_BASE+0x2c), RW, 0, 1, 0x1},
	{(REG_DEINT_BASE+0x2c), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x2c), RW, 4, 2, 0x0},
	{(REG_DEINT_BASE+0x2d), RW, 0, 3, 0x0},
	
	{(REG_DEINT_BASE+0x2f), RO, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 2, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 3, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 4, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 5, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 6, 1, 0x0},
	{(REG_DEINT_BASE+0x2f), RO, 7, 1, 0x0},
	{(REG_DEINT_BASE+0x30), RW, 0, 1, 0x0},
	{(REG_DEINT_BASE+0x30), RW, 1, 1, 0x0},
	{(REG_DEINT_BASE+0x31), RW, 0, 11, 0x0},
	{(REG_DEINT_BASE+0x33), RW, 0, 11, 0x0},

	//RWFF
	{(REG_DI_RWFF_BASE+0x00), RW, 0, 8, 0x0},
	{(REG_DI_RWFF_BASE+0x01), RW, 0, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x01), RW, 1, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x01), RW, 2, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x01), RW, 3, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x01), RW, 4, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x01), RW, 5, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x01), RW, 6, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x01), RW, 7, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x02), RW, 0, 8, 0x40},
	{(REG_DI_RWFF_BASE+0x03), RW, 0, 8, 0xfe},
	{(REG_DI_RWFF_BASE+0x04), RW, 0, 8, 0x7d},
	{(REG_DI_RWFF_BASE+0x05), RW, 0, 5, 0x09},
	{(REG_DI_RWFF_BASE+0x06), RW, 0, 8, 0xfe},
	{(REG_DI_RWFF_BASE+0x07), RW, 0, 8, 0x7d},
	{(REG_DI_RWFF_BASE+0x08), RW, 0, 5, 0x09},
	{(REG_DI_RWFF_BASE+0x09), RW, 0, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x09), RW, 3, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x09), RW, 4, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x09), RW, 7, 1, 0x0},
	
	{(REG_DI_RWFF_BASE+0x0c), RW, 0, 4, 0x0},
	{(REG_DI_RWFF_BASE+0x0c), RW, 4, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x0c), RW, 5, 2, 0x3},
	{(REG_DI_RWFF_BASE+0x0c), RW, 7, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x0d), RW, 0, 6, 0x20},
	{(REG_DI_RWFF_BASE+0x0e), RW, 0, 6, 0x3e},
	{(REG_DI_RWFF_BASE+0x0f), RW, 0, 4, 0x0},
	{(REG_DI_RWFF_BASE+0x0f), RW, 4, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x0f), RW, 5, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x0f), RW, 6, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x0f), RW, 7, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x10), RW, 0, 8, 0x00},
	{(REG_DI_RWFF_BASE+0x11), RW, 0, 8, 0x54},
	{(REG_DI_RWFF_BASE+0x12), RW, 0, 5, 0x06},
	{(REG_DI_RWFF_BASE+0x13), RW, 0, 8, 0x00},
	{(REG_DI_RWFF_BASE+0x14), RW, 0, 8, 0x54},
	{(REG_DI_RWFF_BASE+0x15), RW, 0, 5, 0x07},
	{(REG_DI_RWFF_BASE+0x16), RW, 0, 10, 0xb4},
	{(REG_DI_RWFF_BASE+0x18), RW, 0, 10, 0x5a},
	{(REG_DI_RWFF_BASE+0x1a), RW, 7, 1, 0x1},
	{(REG_DI_RWFF_BASE+0x1b), RO, 0, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x1b), RO, 1, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x1b), RO, 2, 1, 0x0},
	{(REG_DI_RWFF_BASE+0x1b), RO, 3, 1, 0x0},
#endif
#if 0
	//SDN
	{(REG_SCALE_DOWN_BASE+0x00),       WO, 0,   1,   0x0000},
	{(REG_SCALE_DOWN_BASE+0x00),       RW, 6,   2,   0x0000},
    {(REG_SCALE_DOWN_BASE+0x01),       RW, 0,   8,   0x004a},
    {(REG_SCALE_DOWN_BASE+0x02),       RW, 0,   16,  0x1000},
    {(REG_SCALE_DOWN_BASE+0x04),       RW, 0,   12,  0x02d0},
    {(REG_SCALE_DOWN_BASE+0x06),       RW, 0,   12,  0x02d0},
    {(REG_SCALE_DOWN_BASE+0x08),       RW, 0,   1,   0x0001},
	{(REG_SCALE_DOWN_BASE+0x08),       RW, 2,   2,   0x0000},
    {(REG_SCALE_DOWN_BASE+0x08),       RW, 4,   1,   0x0001},
    {(REG_SCALE_DOWN_BASE+0x08),       RW, 7,   1,   0x0000},
    {(REG_SCALE_DOWN_BASE+0x09),       RW, 0,   3,   0x0000},
	{(REG_SCALE_DOWN_BASE+0x09),       RW, 3,   1,   0x0000},
    {(REG_SCALE_DOWN_BASE+0x09),       RW, 4,   4,   0x0000},
    {(REG_SCALE_DOWN_BASE+0x0a),       RW, 0,   16,  0x1000},
    {(REG_SCALE_DOWN_BASE+0x0c),       RW, 0,   12,  0x01e0},
    {(REG_SCALE_DOWN_BASE+0x0e),       RW, 0,   12,  0x01e0},
#endif

#if 0
/*MEM module registers*/
//dmc
	{(REG_MEM_DMC_BASE+0x00),  WO,  0,  1,  0x00},
	{(REG_MEM_DMC_BASE+0x00),  RW,  1,  7,  0x70},
	{(REG_MEM_DMC_BASE+0x01),  RW,  0,  3,  0x00},
	{(REG_MEM_DMC_BASE+0x01),  RW,  4,  2,  0x00},
	{(REG_MEM_DMC_BASE+0x02),  RW,  0,  8,  0x30},
	{(REG_MEM_DMC_BASE+0x03),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x04),  RW,  0,  3,  0x00},
	{(REG_MEM_DMC_BASE+0x04),  RW,  4,  2,  0x00},
	{(REG_MEM_DMC_BASE+0x05),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x06),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x07),  RW,  0,  8,  0x04},
	{(REG_MEM_DMC_BASE+0x08),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x09),  RW,  0,  8,  0x24},
	{(REG_MEM_DMC_BASE+0x0A),  RW,  0,  2,  0x01},
	{(REG_MEM_DMC_BASE+0x0A),  RW,  4,  4,  0x00},
	{(REG_MEM_DMC_BASE+0x0B),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x0C),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x0D),  RW,  0,  8,  0x11},
	{(REG_MEM_DMC_BASE+0x0E),  RW,  0,  8,  0x11},
	{(REG_MEM_DMC_BASE+0x0F),  RW,  0,  8,  0x05},
	{(REG_MEM_DMC_BASE+0x10),  RW,  0,  4,  0x01},
	{(REG_MEM_DMC_BASE+0x10),  RW,  4,  2,  0x01},
	{(REG_MEM_DMC_BASE+0x11),  RW,  0,  8,  0x03},
	{(REG_MEM_DMC_BASE+0x12),  RW,  0,  8,  0x03},
	{(REG_MEM_DMC_BASE+0x13),  RW,  0,  8,  0x03},
	{(REG_MEM_DMC_BASE+0x14),  RW,  0,  8,  0x02},
	{(REG_MEM_DMC_BASE+0x15),  RW,  0,  5,  0x01},
	{(REG_MEM_DMC_BASE+0x16),  RW,  0,  5,  0x02},
	{(REG_MEM_DMC_BASE+0x17),  RW,  0,  5,  0x03},
	{(REG_MEM_DMC_BASE+0x18),  RW,  0,  5,  0x04},
	{(REG_MEM_DMC_BASE+0x19),  RW,  0,  5,  0x05},
	{(REG_MEM_DMC_BASE+0x1A),  RW,  0,  5,  0x06},
	{(REG_MEM_DMC_BASE+0x1B),  RW,  0,  5,  0x07},
	{(REG_MEM_DMC_BASE+0x1C),  RW,  0,  5,  0x08},
	{(REG_MEM_DMC_BASE+0x1D),  RW,  0,  5,  0x09},
	{(REG_MEM_DMC_BASE+0x1E),  RW,  0,  5,  0x0a},
	{(REG_MEM_DMC_BASE+0x1F),  RW,  0,  5,  0x0b},
	{(REG_MEM_DMC_BASE+0x20),  RW,  0,  5,  0x0c},
	{(REG_MEM_DMC_BASE+0x24),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x36),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x37),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x38),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x39),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3A),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3B),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3C),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3D),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3E),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x3F),  RO,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x40),  RW,  0,  8,  0x00},
	{(REG_MEM_DMC_BASE+0x41),  RW,  0,  3,  0x00},
	{(REG_MEM_DMC_BASE+0x41),  WO,  3,  1,  0x00},
	{(REG_MEM_DMC_BASE+0x41),  RW,  4,  4,  0x00}, 
//dfi
	{(REG_MEM_DFI_BASE+0x00),  WO,  0,  2,  0x00},
	{(REG_MEM_DFI_BASE+0x00),  RW,  2,  6,  0x00},
	{(REG_MEM_DFI_BASE+0x01),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x02),  RW,  0,  8,  0x30},
	{(REG_MEM_DFI_BASE+0x03),  RW,  0,  4,  0x00},
	{(REG_MEM_DFI_BASE+0x04),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x05),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x06),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x07),  RW,  0,  8,  0x03},
	{(REG_MEM_DFI_BASE+0x08),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x09),  RW,  0,  8,  0x2d},
	{(REG_MEM_DFI_BASE+0x0A),  RW,  0,  8,  0x22},
	{(REG_MEM_DFI_BASE+0x0B),  RW,  0,  4,  0x02},
	{(REG_MEM_DFI_BASE+0x0C),  RW,  0,  4,  0x02},
	{(REG_MEM_DFI_BASE+0x0D),  RW,  0,  4,  0x02},
	{(REG_MEM_DFI_BASE+0x0E),  RW,  0,  4,  0x02},
	{(REG_MEM_DFI_BASE+0x0F),  RW,  0,  8,  0x22},
	{(REG_MEM_DFI_BASE+0x14),  RW,  4,  4,  0x00},
	{(REG_MEM_DFI_BASE+0x15),  RW,  0,  8,  0x33},
	{(REG_MEM_DFI_BASE+0x16),  RW,  0,  8,  0x03},
	{(REG_MEM_DFI_BASE+0x17),  RW,  0,  8,  0x00},
	{(REG_MEM_DFI_BASE+0x18),  RO,  0, 16,  0x00},
	{(REG_MEM_DFI_BASE+0x1A),  RO,  0, 16,  0x00},
	{(REG_MEM_DFI_BASE+0x1C),  RO,  0, 16,  0x00},
	{(REG_MEM_DFI_BASE+0x1E),  RO,  0, 16,  0x00},
	{(REG_MEM_DFI_BASE+0x20),  WO,  0,  1,  0x00},
	{(REG_MEM_DFI_BASE+0x20),  RO,  1,  1,  0x00},
	{(REG_MEM_DFI_BASE+0x20),  WO,  4,  4,  0x00},
	{(REG_MEM_DFI_BASE+0x21),  RW,  0,  8,  0x30},
	{(REG_MEM_DFI_BASE+0x22),  RW,  0,  5,  0x00},
	{(REG_MEM_DFI_BASE+0x23),  RW,  0,  5,  0x1f},
	{(REG_MEM_DFI_BASE+0x24),  RW,  0,  5,  0x07},
	{(REG_MEM_DFI_BASE+0x25),  RW,  0,  8,  0x50},
	{(REG_MEM_DFI_BASE+0x26),  RW,  0,  5,  0x00},
	{(REG_MEM_DFI_BASE+0x27),  RW,  0,  5,  0x1f},
	{(REG_MEM_DFI_BASE+0x28),  RW,  0,  5,  0x03},
	{(REG_MEM_DFI_BASE+0x2A),  RW,  0,  8,  0xa0},
	{(REG_MEM_DFI_BASE+0x2E),  RW,  0,  8,  0x18},
	{(REG_MEM_DFI_BASE+0x2F),  RW,  0,  8,  0x18}, 
	{(REG_MEM_DFI_BASE+0x32),  RW,  0,  8,  0x00}, 
//cappb
	{(REG_MEM_CAPPB_BASE+0x00),  RW,  0,  3,  0x00},
	{(REG_MEM_CAPPB_BASE+0x00),  RW,  5,  3,  0x00},
	{(REG_MEM_CAPPB_BASE+0x01),  RW,  0,  8,  0x0a},
	{(REG_MEM_CAPPB_BASE+0x02),  RW,  0,  8,  0x02},
	{(REG_MEM_CAPPB_BASE+0x03),  RW,  0,  8,  0x20},
	{(REG_MEM_CAPPB_BASE+0x04),  RW,  0,  8,  0x00},
	{(REG_MEM_CAPPB_BASE+0x05),  RW,  0, 12,  0x00},
	{(REG_MEM_CAPPB_BASE+0x07),  RW,  0,  8,  0x00},
	{(REG_MEM_CAPPB_BASE+0x08),  RW,  0, 12,  0x00},
	{(REG_MEM_CAPPB_BASE+0x0A),  RO,  0,  4,  0x00},
	{(REG_MEM_CAPPB_BASE+0x0B),  RW,  0,  6,  0x20},
	{(REG_MEM_CAPPB_BASE+0x0B),  RW,  7,  1,  0x00},
	{(REG_MEM_CAPPB_BASE+0x0C),  RW,  0,  6,  0x10},
	{(REG_MEM_CAPPB_BASE+0x0D),  RW,  0,  6,  0x00},
	{(REG_MEM_CAPPB_BASE+0x0E),  RW,  0,  7,  0x00},
	{(REG_MEM_CAPPB_BASE+0x0F),  RW,  0,  4,  0x00},
	{(REG_MEM_CAPPB_BASE+0x11),  RW,  0,  8,  0x00},
	{(REG_MEM_CAPPB_BASE+0x12),  RW,  0,  8,  0x00},
	{(REG_MEM_CAPPB_BASE+0x13),  RW,  0,  5,  0x00},
	{(REG_MEM_CAPPB_BASE+0x14),  RW,  0,  8,  0x00},
	{(REG_MEM_CAPPB_BASE+0x15),  RW,  0,  8,  0xb0},
	{(REG_MEM_CAPPB_BASE+0x16),  RW,  0,  5,  0x02},
	{(REG_MEM_CAPPB_BASE+0x17),  RW,  0, 10,  0xb8},
	{(REG_MEM_CAPPB_BASE+0x19),  RW,  0, 10,  0xb4},
	{(REG_MEM_CAPPB_BASE+0x1B),  RW,  0,  8,  0xc5},
	{(REG_MEM_CAPPB_BASE+0x1C),  RW,  0, 11,  0x00},
	{(REG_MEM_CAPPB_BASE+0x1E),  RW,  0, 12,  0x00},
#endif

#if 0
/*VDS module registers*/
//timing generation
	{(REG_VDS_TIMGEN_BASE+0x00),  WO,  0,  1,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x01),  RW,  0,  8,  0x0c},
	{(REG_VDS_TIMGEN_BASE+0x02),  RW,  0, 12, 0x35a},
	{(REG_VDS_TIMGEN_BASE+0x04),  RW,  0, 11, 0x20d},
	{(REG_VDS_TIMGEN_BASE+0x06),  RW,  0, 12,  0x3e},
	{(REG_VDS_TIMGEN_BASE+0x08),  RW,  0, 11,  0x06},
	{(REG_VDS_TIMGEN_BASE+0x0A),  RW,  0, 12,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x0C),  RW,  0, 11,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x0E),  RW,  0, 12,  0x7a},
	{(REG_VDS_TIMGEN_BASE+0x10),  RW,  0, 12, 0x34a},
	{(REG_VDS_TIMGEN_BASE+0x12),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x14),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x16),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x18),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x1A),  RW,  0, 12,  0x7a},
	{(REG_VDS_TIMGEN_BASE+0x1C),  RW,  0, 12, 0x34a},
	{(REG_VDS_TIMGEN_BASE+0x1E),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x20),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x22),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x24),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x26),  RW,  0, 12,  0x7a},
	{(REG_VDS_TIMGEN_BASE+0x28),  RW,  0, 12, 0x34a},
	{(REG_VDS_TIMGEN_BASE+0x2A),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x2C),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x2E),  RW,  0, 11,  0x23},
	{(REG_VDS_TIMGEN_BASE+0x30),  RW,  0, 11, 0x203},
	{(REG_VDS_TIMGEN_BASE+0x32),  RW,  0, 13,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x34),  RW,  0, 12,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x36),  RW,  0, 13,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x38),  RW,  0, 12,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x40),  RW,  0, 13,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x42),  RW,  0, 12,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x44),  RW,  0, 13,  0x00},
	{(REG_VDS_TIMGEN_BASE+0x46),  RW,  0, 12,  0x00},
//scaling up
	{(REG_VDS_SU_BASE+0x3A),  RW,  2,  5,  0x00},
	{(REG_VDS_SU_BASE+0x3B),  RW,  0, 13,0x1000},
	{(REG_VDS_SU_BASE+0x3D),  RW,  0, 13,0x1000},
	{(REG_VDS_SU_BASE+0x3F),  RW,  0,  6,  0x04},
	{(REG_VDS_SU_BASE+0x48),  RW,  0,  8,  0x10},
	{(REG_VDS_SU_BASE+0x49),  RW,  0,  8,  0x80},
	{(REG_VDS_SU_BASE+0x4A),  RW,  0,  8,  0x80},
//enhance
	{(REG_VDS_ENHANC_BASE+0x00),  RW,  0,  1,  0x00},
	{(REG_VDS_ENHANC_BASE+0x04),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x05),  RW,  4,  4,  0x00},
	{(REG_VDS_ENHANC_BASE+0x05),  RW,  1,  1,  0x00},
	{(REG_VDS_ENHANC_BASE+0x06),  RW,  0,  6,  0x01},
	{(REG_VDS_ENHANC_BASE+0x07),  RW,  0,  4,  0x00},
	{(REG_VDS_ENHANC_BASE+0x08),  RW,  0,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x08),  RW,  4,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x09),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x0A),  RW,  0, 16,  0x00},
	{(REG_VDS_ENHANC_BASE+0x0C),  RW,  0, 16,  0x00},
	{(REG_VDS_ENHANC_BASE+0x0E),  RW,  0,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x0E),  RW,  4,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x0F),  RW,  0,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x0F),  RW,  4,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x10),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x11),  RW,  0,  8,  0x80},
	{(REG_VDS_ENHANC_BASE+0x12),  RW,  0,  8,  0x80},
	{(REG_VDS_ENHANC_BASE+0x13),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x39),  RW,  4,  4,  0x00},
	{(REG_VDS_ENHANC_BASE+0x39),  RW,  0,  1,  0x00},
	{(REG_VDS_ENHANC_BASE+0x3A),  RW,  0,  5,  0x01},
	{(REG_VDS_ENHANC_BASE+0x3B),  RW,  0, 11,  0x00},
	{(REG_VDS_ENHANC_BASE+0x3D),  RW,  0, 10,  0x00},
	{(REG_VDS_ENHANC_BASE+0x43),  RW,  0,  1,  0x01},
	{(REG_VDS_ENHANC_BASE+0x44),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x45),  RW,  0,  8,  0x00},
	{(REG_VDS_ENHANC_BASE+0x46),  RW,  0,  8,  0x00},	
#endif

#if 0
	//P2I
	{(0x0870+0x00),       RW, 0,   1,  0x0000},
	{(0x0870+0x02),       RW, 0,   4,  0x0000},
	{(0x0870+0x03),       RW, 0,   3,  0x0000},
	{(0x0870+0x03),       RW, 4,   2,  0x0001},
#endif
#if 0
	//HDMI TX SHELL
	{(0x0b00+0x00),       RO, 0,   8,  0x0000},
	{(0x0b00+0x01),       RW, 0,   8,  0x0000},
	{(0x0b00+0x02),       RO, 0,   8,  0x0000},
	{(0x0b00+0x03),       RW, 0,   1,  0x0000},
	{(0x0b00+0x06),       RW, 0,   3,  0x0000},
	{(0x0b00+0x07),       RW, 0,   3,  0x0006}, //mark
	{(0x0b00+0x08),       RW, 0,   3,  0x0000},
	{(0x0b00+0x08),       RW, 4,   4,  0x0000},
	{(0x0b00+0x09),       RW, 0,   8,  0x0000},
	{(0x0b00+0x0a),       RW, 0,   8,  0x0000},
	{(0x0b00+0x0b),       RW, 0,   8,  0x0000},
	{(0x0b00+0x0c),       WO, 0,   8,  0x0000},
	{(0x0b00+0x0d),       WO, 0,   8,  0x0000},
	{(0x0b00+0x0e),       RW, 0,   8,  0x0000},
	{(0x0b00+0x0f),       RW, 0,   5,  0x0000},
	{(0x0b00+0x10),       WO, 0,   8,  0x0000}, //can be R/W after 0xb13 set 1
	{(0x0b00+0x11),       WO, 0,   8,  0x0000}, //can be R/W after 0xb13 set 1
	{(0x0b00+0x12),       WO, 0,   8,  0x0000}, //can be R/W after 0xb13 set 1
	{(0x0b00+0x13),       RW, 0,   1,  0x0000},
	{(0x0b00+0x14),       RW, 0,   3,  0x0001}, //mark
	{(0x0b00+0x15),       RW, 0,   1,  0x0000},
	{(0x0b00+0x16),       RW, 0,   4,  0x0000},
	{(0x0b00+0x17),       RO, 0,   8,  0x0000},
	{(0x0b00+0x18),       RW, 0,   5,  0x0000},
	{(0x0b00+0x19),       RW, 0,   1,  0x0000},
	{(0x0b00+0x1a),       RW, 0,   4,  0x0000},
	{(0x0b00+0x1a),       RO, 4,   1,  0x0000},
	{(0x0b00+0x1b),       RO, 0,   8,  0x0000},
	{(0x0b00+0x1c),       RO, 0,   8,  0x0000},
	{(0x0b00+0x1d),       RO, 0,   5,  0x0000},
	{(0x0b00+0x24),       RW, 3,   1,  0x0000},
	{(0x0b00+0x24),       RW, 4,   4,  0x0000},
	{(0x0b00+0x25),       RW, 0,   7,  0x0003}, //mark
	{(0x0b00+0x26),       RW, 0,   8,  0x0000},
	{(0x0b00+0x27),       WO, 0,   8,  0x0000}, //mark, can be R/W after reg0xb24[3] = 1, reg0x0a[2] = 1
	{(0x0b00+0x28),       WO, 0,   8,  0x0000}, //mark, can be R/W after reg0xb24[3] = 1, reg0x0a[2] = 1
	{(0x0b00+0x29),       WO, 0,   8,  0x0000}, //mark, can be R/W after reg0xb24[3] = 1, reg0x0a[2] = 1
	{(0x0b00+0x2a),       WO, 0,   8,  0x0000}, //mark, can be R/W after reg0xb24[3] = 1, reg0x0a[2] = 1 
	{(0x0b00+0x31),       RW, 7,   1,  0x0000},
	{(0x0b00+0x32),       RW, 0,   8,  0x0081},
	{(0x0b00+0x33),       RW, 0,   2,  0x0000},
	{(0x0b00+0x33),       RW, 4,   1,  0x0000},
	{(0x0b00+0x33),       RW, 6,   2,  0x0000},
	{(0x0b00+0x34),       RW, 0,   3,  0x0000},
	{(0x0b00+0x35),       RW, 0,   8,  0x0042},
	{(0x0b00+0x36),       RW, 1,   7,  0x0040},
	{(0x0b00+0x37),       RW, 0,   8,  0x0088}, //mark
	{(0x0b00+0x38),       RW, 0,   8,  0x0086},
	{(0x0b00+0x39),       RW, 0,   8,  0x0068}, //mark
	{(0x0b00+0x3a),       RW, 0,   8,  0x0000},
	{(0x0b00+0x3b),       RW, 0,   1,  0x0000},
	{(0x0b00+0x3b),       RW, 7,   1,  0x0000},
	{(0x0b00+0x3c),       RW, 0,   8,  0x0000},
	{(0x0b00+0x70),       RW, 0,   1,  0x0000},
	{(0x0b00+0x70),       RW, 7,   1,  0x0001},
	{(0x0b00+0x71),       RW, 0,   8,  0x0000},
	{(0x0b00+0x72),       RW, 0,   8,  0x0000},
	{(0x0b00+0x73),       RW, 0,   8,  0x0000},
	{(0x0b00+0x74),       RW, 0,   8,  0x0000},
	{(0x0b00+0x75),       RW, 0,   8,  0x0000},
	{(0x0b00+0x76),       RO, 0,   1,  0x0000},
	{(0x0b00+0x80),       RO, 0,   8,  0x0000},
	{(0x0b00+0x81),       RW, 0,   2,  0x0000},
	{(0x0b00+0x81),       WO, 3,   1,  0x0000},
	{(0x0b00+0x81),       RW, 4,   1,  0x0000},
	{(0x0b00+0x81),       RW, 6,   3,  0x0000},
	{(0x0b00+0x82),       RW, 0,   8,  0x0000},
	{(0x0b00+0x83),       RW, 0,   8,  0x0000},
	{(0x0b00+0x84),       RW, 0,   8,  0x0000},
	{(0x0b00+0x85),       RW, 0,   8,  0x0000},
	{(0x0b00+0x86),       RW, 0,   8,  0x0000},
	{(0x0b00+0x87),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x88),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x89),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8a),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8b),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8c),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8d),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8e),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0xb8e
	{(0x0b00+0x8f),       RO, 0,   8,  0x0000},
	{(0x0b00+0x94),       RO, 0,   8,  0x0000},
	{(0x0b00+0x95),       RO, 0,   8,  0x0000},
	{(0x0b00+0x96),       WO, 0,   8,  0x0000},
	{(0x0b00+0x97),       WO, 0,   8,  0x0000},
	{(0x0b00+0x98),       RW, 0,   8,  0x0000},
	{(0x0b00+0x99),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9a),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9b),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9c),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9d),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9e),       RW, 0,   8,  0x0000},
	{(0x0b00+0x9f),       RW, 0,   8,  0x0000},
	{(0x0b00+0xa0),       RW, 0,   8,  0x0000},
	{(0x0b00+0xa1),       RW, 0,   8,  0x0000},
	{(0x0b00+0xa2),       RW, 0,   8,  0x0000},
	{(0x0b00+0xa3),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa4),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa5),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa6),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa7),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa8),       RO, 0,   8,  0x0000},
	{(0x0b00+0xa9),       RO, 0,   8,  0x0000},
	{(0x0b00+0xaa),       RO, 0,   8,  0x0000},
	{(0x0b00+0xab),       RO, 0,   8,  0x0000},
	{(0x0b00+0xac),       RO, 0,   8,  0x0000},
	{(0x0b00+0xad),       RO, 0,   8,  0x0000},
	{(0x0b00+0xae),       RO, 0,   8,  0x0000},
	{(0x0b00+0xaf),       RO, 0,   8,  0x0000},
	{(0x0b00+0xb0),       RO, 0,   8,  0x0000},
	{(0x0b00+0xb1),       RO, 0,   8,  0x0000},
	{(0x0b00+0xb2),       RO, 0,   8,  0x0000},
	{(0x0b00+0xb3),       RO, 0,   8,  0x0000},
	{(0x0b00+0xb4),       RW, 0,   8,  0x0000},
	{(0x0b00+0xb5),       RW, 0,   8,  0x0000},
	{(0x0b00+0xb6),       RW, 0,   4,  0x005},
	{(0x0b00+0xb6),       RW, 4,   2,  0x0003},
	{(0x0b00+0xb7),       RW, 0,   2,  0x0002},
#endif
#if 1
	//HDMI TX PHY
	{(0x01c0+0x00),       RW, 0,   1,  0x0000},
	{(0x01c0+0x00),       RW, 4,   1,  0x0000},
	{(0x01c0+0x01),       WO, 0,   2,  0x0001}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x01),       RW, 4,   1,  0x0000},
	{(0x01c0+0x02),       RW, 0,   1,  0x0001},
	{(0x01c0+0x03),       WO, 0,   8,  0x0007}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x04),       WO, 0,  12,  0x013b}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x06),       RW, 0,   8,  0x0001},
	{(0x01c0+0x07),       WO, 0,   4,  0x0005}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x07),       RW, 5,   3,  0x0000},
	{(0x01c0+0x08),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x09),       WO, 0,   8,  0x0000}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x0a),       WO, 0,   4,  0x0000}, //mark, can be R/W after trigger reg0x1cb[0] = 1
	{(0x01c0+0x0a),       RW, 4,   2,  0x0002},
	{(0x01c0+0x0b),       RW, 0,   1,  0x0000},
	#if (MS1858_CHIP_TYPE == 1) //BM8858A chip
	{(0x01c0+0x0c),       RW, 0,   3,  0x0000},
	{(0x01c0+0x0c),       RW, 4,   4,  0x0000},
	#else ////AM8858A chip
	{(0x01c0+0x0c),       RW, 0,   2,  0x0000},
	{(0x01c0+0x0c),       RW, 4,   1,  0x0000},
	#endif
	{(0x01c0+0x0d),       RW, 0,   8,  0x0010},
	{(0x01c0+0x0e),       RW, 0,   3,  0x0007},
	{(0x01c0+0x0e),       RW, 4,   3,  0x0000},
	{(0x01c0+0x0f),       RW, 0,   7,  0x0002},
	{(0x01c0+0x10),       RW, 0,   8,  0x0032},
	{(0x01c0+0x11),       RW, 0,   3,  0x0003},
	{(0x01c0+0x11),       RW, 4,   4,  0x0000},
	{(0x01c0+0x12),       RW, 0,   8,  0x0000},
	{(0x01c0+0x13),       RW, 0,   3,  0x0000},
	{(0x01c0+0x13),       RW, 4,   4,  0x0000},
	{(0x01c0+0x14),       RW, 0,   3,  0x0000},
	{(0x01c0+0x14),       RW, 4,   3,  0x0000},
	{(0x01c0+0x15),       RW, 0,   3,  0x0005},
	{(0x01c0+0x15),       RW, 4,   3,  0x0005},
	{(0x01c0+0x16),       RW, 0,   3,  0x0005},
	#if (MS1858_CHIP_TYPE == 1) //BM8858A chip
	{(0x01c0+0x17),       RW, 0,   2,  0x0000},
	#endif
#endif
};


BOOL _register_executeLine(char *pLine, REGATTR_T* reg)
{
	UINT16      u16index;
    UINT8       u8flag;
    UINT8       u8start;
    UINT8       u8bits;
    UINT16      u16default;

    char    cmd[20];
	char    cmd1[20];
    int     fields;
	int len;
	int i;
	char *pCpy = pLine;

    /* remove newline (if available) */
    while (strlen(pLine)) {
        if ((*(pLine+strlen(pLine)-1)==0x0D) ||
            (*(pLine+strlen(pLine)-1)==0x0A))

            *(pLine+strlen(pLine)-1) = 0;
        else
            break;
    }

    /* do nothing is line is empty */
    if (!strlen(pLine))
        return FALSE;
	
	len = strlen(pLine);
	while(i < len)
	{
		if (*pCpy == ' ' || *pCpy == '	')
		{
			pCpy++;
		}
		else if (*pCpy == '#' || *pCpy == '/')
		{
			return FALSE;
		}
		else
		{
			break;
		}
		
		i++;
	}
	
	//cmd: MSREG 0000 RO 0 8 00
    fields = sscanf(pLine, "%s  %lx %s %ld %ld %lx", &cmd, &u16index, &cmd1, &u8start, &u8bits, &u16default);
	
    /* no field may be caused from empty line */
    if (!fields || (fields==EOF))
        return FALSE;
	
	if (u8start > 16)
	{
		u8start = 0;
	}

	if (u8bits > 16)
	{
		u8bits = 1;
	}

    if (_strcmpi(cmd, "MSREG")) 
	{
            return FALSE;
    }

    if (!_strcmpi(cmd1, "NA"))
	{
		u8flag = 0;
	}
    else if (!_strcmpi(cmd1, "RO"))
	{
        u8flag = 1;
    }
    else if (!_strcmpi(cmd1, "WO"))
	{
        u8flag = 2;
    }
    else if (!_strcmpi(cmd1, "RW"))
	{
        u8flag = 3;
    }
    else
	{
        u8flag = 3;
    }

	reg->u16index = u16index;
	reg->u8flag = u8flag;
	reg->u8start = u8start;
	reg->u8bits = u8bits;
	reg->u32default = u16default;

    return TRUE;
}

UINT16 _register_map_exexecute_file(FILE* pFile, REGATTR_T* reg)
{
	char buf[260];
	UINT16 len = 0;

    while (!feof(pFile)) 
	{
        //get 1 line, if 
        if (!fgets(buf, 255, pFile)) 
		{
			break;
		}
        
        if (_register_executeLine(buf, reg))
		{
			len++;
			reg++;
		}
    }
	
	return len;
}


DWORD WINAPI Proc_register_check(PVOID pParam)
{
	CMS1858CheckRegRW Cms1858CheckRW(g_pf_reg_log, p_st_register_buf, g_u16_register_check_len);
	
	Cms1858CheckRW.CheckStart(1);

	Cms1858CheckRW.CheckDefault();
	Cms1858CheckRW.CheckRW();
	Cms1858CheckRW.LogErr();
	fclose(g_pf_reg_log);
	Cms1858CheckRW.CheckStart(0);

	g_b_check_over = 1;
	return 0;
}

void CPageDebugRegister::OnButtonRegisterCheck() 
{
	// TODO: Add your control notification handler code here
	BOOL b_flag;

	if (g_b_check_over == 0)
	{
		return;
	}

	CString filename, extension;
	if (g_b_register_file_flag)
	{
		FILE* fp = NULL;
		char szFilter[] = _T("Text File(*.txt)|*.txt|All Files (*.*)|*.*|");
		CFileDialog filedlg(TRUE, _T("txt"), _T("register_map.txt"), OFN_FILEMUSTEXIST , szFilter, AfxGetMainWnd());
		
		if (filedlg.DoModal()==IDOK) 
		{
			filename = filedlg.GetFileName();
			extension = filedlg.GetFileExt();
        
			fp = fopen(filename, "rt");
			if(fp)
			{
				g_u16_register_file_len = _register_map_exexecute_file(fp, g_st_arr_reg_buf);
				// do here
				fclose(fp);
			}
			else
			{
				MessageBox(TEXT("Open txt file failed"), TEXT("Error"), MB_OK);
				return;
			}
		}
	}
	
	g_u16_register_check_len = g_b_register_file_flag ? g_u16_register_file_len : internal_register_buf_size; 
	p_st_register_buf = g_b_register_file_flag ? g_st_arr_reg_buf : st_RegArray;
	
	if (g_b_register_file_flag == 0)
	{
		char szFiltertxt[] = _T("Text File(*.txt)|*.txt|");    
		CFileDialog filedlgtxt(false, _T("txt"), _T("Check Log.txt"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT , szFiltertxt, AfxGetMainWnd());

		if (filedlgtxt.DoModal()==IDOK) 
		{
		   filename = filedlgtxt.GetPathName();
		   extension = filedlgtxt.GetFileExt();

		   b_flag = 1;
		}
		else
		{
			b_flag = 0;
		}
	}
	else
	{
		 b_flag = 1;
		 filename = filename + "_check_log.txt";
	}
	
    if("" == filename) return;
	g_pf_reg_log = fopen(filename, "wt+");
	if (b_flag && g_pf_reg_log) 
	{
		g_b_check_over = 0;
		SetTimer(0, 1000, 0);
		HANDLE h_proc=CreateThread(NULL,0,Proc_register_check,NULL,0,NULL); 
		CloseHandle(h_proc); 
	}
	else
	{
		MessageBox(TEXT("Save txt file failed"), TEXT("Error"), MB_OK);
		return;
	}
}

void CPageDebugRegister::OnButtonRegisterCheckStop() 
{
	// TODO: Add your control notification handler code here
	CMS1858CheckRegRW Cms1858CheckRW;
	Cms1858CheckRW.CheckStart(0);
}

void CPageDebugRegister::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (g_b_check_over == 1)
	{
		KillTimer(0);
		MessageBox("Check registers done!");
		return;
	}

	UINT16 u16_i;
	UINT16 u16_j;
	UINT16 u16_k;

	CMS1858CheckRegRW Cms1858CheckRW;
	
	u16_i = Cms1858CheckRW.GetDefaultCheckCount();
	u16_j = Cms1858CheckRW.GetRWCheckCount();
	
	if (g_u16_register_check_len == 0) g_u16_register_check_len = 1; 
	u16_k = 100 * (u16_j) / (g_u16_register_check_len);

	CString c;
	if (u16_k > 100) u16_k = 100;
	c.Format("%3d %%", u16_k);
	SetDlgItemText(IDC_STATIC_REG_RW_RATIO, c); 
    m_control_progress.SetPos(u16_k);
	CFormView::OnTimer(nIDEvent);
}

void CPageDebugRegister::OnCheckRegisterFileSelect() 
{
	// TODO: Add your control notification handler code here
	if(1 == ((CButton *)GetDlgItem(IDC_CHECK_REGISTER_FILE_SELECT))->GetCheck())
	{
		g_b_register_file_flag = 1;
	}
	else
	{
		g_b_register_file_flag = 0;
	}	
}

void CPageDebugRegister::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFormView::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	((CButton *)GetDlgItem(IDC_CHECK_REGISTER_FILE_SELECT))->SetCheck(g_b_register_file_flag);

}

void CPageDebugRegister::OnButtonRegisterSaveTemlate() 
{
	// TODO: Add your control notification handler code here
	FILE* fp = NULL;
	CString filename, extension;
	char szFiltertxt[] = _T("Text File(*.txt)|*.txt|");    
	CFileDialog filedlgtxt(false, _T("txt"), _T("register_template.txt"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT , szFiltertxt, AfxGetMainWnd());

	if (filedlgtxt.DoModal()==IDOK) 
	{
        filename = filedlgtxt.GetFileName();
        extension = filedlgtxt.GetFileExt();
        
		fp = fopen(filename, "wt+");
		if(fp)
		{
			for (int i = 0; i < internal_register_buf_size; i++)
			{
				fprintf(fp, "MSREG ");
				fprintf(fp, "0x%04x ", st_RegArray[i].u16index);
	
				if (st_RegArray[i].u8flag == 0)
				{
					fprintf(fp, "NA ");
				}
				else if (st_RegArray[i].u8flag == 1)
				{
					fprintf(fp, "RO ");
				}
				else if (st_RegArray[i].u8flag == 2)
				{
					fprintf(fp, "WO ");
				}
				else if (st_RegArray[i].u8flag == 3)
				{
					fprintf(fp, "RW ");
				}
				else
				{
					fprintf(fp, "RW ");
				}

				fprintf(fp, "%02d ", st_RegArray[i].u8start);
				fprintf(fp, "%02d ", st_RegArray[i].u8bits);
				fprintf(fp, "0x%04x ", st_RegArray[i].u32default);
				fprintf(fp, "%\r\n");
				
			}
			fclose(fp);
		}
		else
		{
			MessageBox(TEXT("Save txt file failed"), TEXT("Error"), MB_OK);
			return;
		}
	}

	MessageBox("Save register template done!");
}

void CPageDebugRegister::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	m_control_progress.SetRange(0, 100);
    m_control_progress.SetPos(0);
}
