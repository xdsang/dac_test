// PageSubPage.cpp : implementation file
//

#include "stdafx.h"
#include "MT.h"
#include "PageDebugVBE.h"

#define REG_DAC_BASE 0xf160
#define REG_DAC_MUX_BASE 0xf031

const int SPIN_RIGHT = -1;
const int SPIN_LEFT = 1;

INT16 g_16_test_data;
UINT16 g_u16_dac_reg_addr = 0xF000;

// VDAC
typedef enum _E_DAC_OUTTYPE_
{
    DAC_OUT_Y = 0,   /*output as svideo Y*/
    DAC_OUT_C = 1,   /*output as svideo C*/
    DAC_OUT_CVBS = 3,   /*output as cvbs*/
    DAC_OUT_NULL = 0xff /*not used*/
}DACOUT_E;


// CPageDebugVBE

IMPLEMENT_DYNCREATE(CPageDebugVBE, CFormView)

CPageDebugVBE::CPageDebugVBE()
    : CFormView(IDD_PAGE_DEBUG_VBE)
	, m_vdac_c_used(0)
	, m_vdac_y_used(0)
	, m_vdac_cvbs_used(0)
	, m_vdac_bgpwr(FALSE)
	, m_vdac_clkpwr(FALSE)
	, m_vdac_cpwr(FALSE)
	, m_vdac_ypwr(FALSE)
	, m_vdac_cvbspwr(FALSE)

	, m_str_dac_test_data(_T("000"))
	, m_dac_reg_data_3ff(FALSE)
	, m_dac_reg_data_000(FALSE)
	, m_dac_reg_addr(_T("F033"))
	, m_mulitmeter_ip_addr(_T("TCPIP0::192.168.188.119::inst0::INSTR"))
{

}

CPageDebugVBE::~CPageDebugVBE()
{
}

void CPageDebugVBE::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	// VDAC
	DDX_CBIndex(pDX, IDC_COMBO_MUX_C, m_vdac_c_used);
	DDX_CBIndex(pDX, IDC_COMBO_MUX_Y, m_vdac_y_used);
	DDX_CBIndex(pDX, IDC_COMBO_MUX_CVBS, m_vdac_cvbs_used);

	DDX_Check(pDX, IDC_CHECK_BGPWR, m_vdac_bgpwr);
	DDX_Check(pDX, IDC_CHECK_CLKPWR, m_vdac_clkpwr);
	DDX_Check(pDX, IDC_CHECK_CPWR, m_vdac_cpwr);
	DDX_Check(pDX, IDC_CHECK_VDACYPWR, m_vdac_ypwr);
	DDX_Check(pDX, IDC_CHECK_VDACCVBSPWR, m_vdac_cvbspwr);

	DDX_Text(pDX, IDC_EDIT_DAC_REG_TEST, m_str_dac_test_data);
	DDX_Check(pDX, IDC_CHECK_VDAC_TEST_DATA_3FF, m_dac_reg_data_3ff);
	DDX_Check(pDX, IDC_CHECK_VDAC_TEST_DATA_000, m_dac_reg_data_000);
	DDX_Text(pDX, IDC_EDIT_DAC_REG_ADDR, m_dac_reg_addr);
	DDX_Text(pDX, IDC_EDIT_MULTIMETER_IP_ADDR, m_mulitmeter_ip_addr);
}

BEGIN_MESSAGE_MAP(CPageDebugVBE, CFormView)
    ON_CBN_SELCHANGE(IDC_COMBO_MUX_C, &CPageDebugVBE::OnCbnSelchangeComboMuxC)
    ON_CBN_SELCHANGE(IDC_COMBO_MUX_Y, &CPageDebugVBE::OnCbnSelchangeComboMuxY)
    ON_CBN_SELCHANGE(IDC_COMBO_MUX_CVBS, &CPageDebugVBE::OnCbnSelchangeComboMuxCvbs)
    ON_BN_CLICKED(IDC_CHECK_BGPWR, &CPageDebugVBE::OnBnClickedCheckBgpwr)
    ON_BN_CLICKED(IDC_CHECK_CLKPWR, &CPageDebugVBE::OnBnClickedCheckClkpwr)
    ON_BN_CLICKED(IDC_CHECK_CPWR, &CPageDebugVBE::OnBnClickedCheckCpwr)
    ON_BN_CLICKED(IDC_CHECK_VDACYPWR, &CPageDebugVBE::OnBnClickedCheckVdacypwr)
    ON_BN_CLICKED(IDC_CHECK_VDACCVBSPWR, &CPageDebugVBE::OnBnClickedCheckVdaccvbspwr)
	ON_EN_CHANGE(IDC_EDIT_DAC_REG_TEST, &CPageDebugVBE::OnEnChangeEditDacRegTest)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DAC_REG_TEST, &CPageDebugVBE::OnDeltaposSpinDacRegTest)
	ON_BN_CLICKED(IDC_CHECK_VDAC_TEST_DATA_3FF, &CPageDebugVBE::OnBnClickedCheckVdacTestData3ff)
	ON_BN_CLICKED(IDC_CHECK_VDAC_TEST_DATA_000, &CPageDebugVBE::OnBnClickedCheckVdacTestData000)
	ON_BN_CLICKED(IDC_BUTTON1, &CPageDebugVBE::OnBnClickedButton1)
	ON_EN_CHANGE(IDC_EDIT_DAC_REG_ADDR, &CPageDebugVBE::OnEnChangeEditDacRegAddr)
	ON_EN_CHANGE(IDC_EDIT_MULTIMETER_IP_ADDR, &CPageDebugVBE::OnEnChangeEditMultimeterIpAddr)
	ON_BN_CLICKED(IDC_BUTTON_SET_MULTIMETER_IP_ADDR, &CPageDebugVBE::OnBnClickedButtonSetMultimeterIpAddr)
END_MESSAGE_MAP()


// CPageDebugVBE 诊断

#ifdef _DEBUG
void CPageDebugVBE::AssertValid() const
{
    CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPageDebugVBE::Dump(CDumpContext& dc) const
{
    CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CPageDebugVBE::OnInitialUpdate()
{
    CFormView::OnInitialUpdate();

    // TODO: Add your message handler code here

	OnBnClickedButtonTveUpdate();

	UINT8 u8_value;

	u8_value = HAL_ReadByte(REG_DAC_BASE);
	m_vdac_bgpwr = (u8_value & MSRT_BIT0) ? 1 : 0;
	m_vdac_ypwr = (u8_value & MSRT_BIT1) ? 1 : 0;
	m_vdac_cvbspwr = (u8_value & MSRT_BIT2) ? 1 : 0;
	m_vdac_cpwr = (u8_value & MSRT_BIT3) ? 1 : 0;

	u8_value = HAL_ReadByte(0xf004);
	m_vdac_clkpwr = (u8_value & MSRT_BIT7) ? 1 : 0;

	g_16_test_data = HAL_ReadWord(REG_DAC_MUX_BASE + 2);
	m_str_dac_test_data.Format(_T("%03X"), g_16_test_data);

    UpdateData(FALSE);
}


// CPageDebugVBE 消息处理程序

// VDAC Module ///////////////////////////////////////////////
void CPageDebugVBE::OnCbnSelchangeComboMuxC()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
    switch (m_vdac_c_used)
    {
    case 0:
		HAL_ModBits(0xf031, MSRT_BITS5_4, 0x00);
		HAL_ModBits(0xf032, MSRT_BITS1_0, 0x00);
        break;
    case 1:
		HAL_ModBits(0xf031, MSRT_BITS5_4, 0x10);
		HAL_ModBits(0xf032, MSRT_BITS3_2, 0x00);
        break;
    case 2:
		HAL_ModBits(0xf031, MSRT_BITS5_4, 0x30);
		HAL_ModBits(0xf032, MSRT_BITS7_6, 0x00);
        break;
    default:
        break;
    }
}


void CPageDebugVBE::OnCbnSelchangeComboMuxY()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
    switch (m_vdac_y_used)
    {
    case 0:
		HAL_ModBits(0xf031, MSRT_BITS3_2, 0x00);
		HAL_ModBits(0xf032, MSRT_BITS1_0, 0x00);
        break;
    case 1:
		HAL_ModBits(0xf031, MSRT_BITS3_2, 0x04);
		HAL_ModBits(0xf032, MSRT_BITS3_2, 0x00);
        break;
    case 2:
		HAL_ModBits(0xf031, MSRT_BITS3_2, 0x0c);
		HAL_ModBits(0xf032, MSRT_BITS7_6, 0x00);
        break;
    default:
        break;
    }
}


void CPageDebugVBE::OnCbnSelchangeComboMuxCvbs()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
    switch (m_vdac_cvbs_used)
    {
    case 0:
		HAL_ModBits(0xf031, MSRT_BITS1_0, 0x00);
		HAL_ModBits(0xf032, MSRT_BITS1_0, 0x00);
        break;
    case 1:
		HAL_ModBits(0xf031, MSRT_BITS1_0, 0x01);
		HAL_ModBits(0xf032, MSRT_BITS3_2, 0x00);
        break;
    case 2:
		HAL_ModBits(0xf031, MSRT_BITS1_0, 0x03);
		HAL_ModBits(0xf032, MSRT_BITS7_6, 0x00);
        break;
    default:
        break;
    }
	
}


void CPageDebugVBE::OnBnClickedCheckBgpwr()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	HAL_ModBits(REG_DAC_BASE, MSRT_BIT0, (m_vdac_bgpwr) ? MSRT_BIT0 : 0x00);
	m_vdac_bgpwr = !m_vdac_bgpwr;
}


void CPageDebugVBE::OnBnClickedCheckClkpwr()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	HAL_ModBits(0xf004, MSRT_BIT7, (m_vdac_clkpwr) ? MSRT_BIT7 : 0x00);
	m_vdac_clkpwr = !m_vdac_clkpwr;
}


void CPageDebugVBE::OnBnClickedCheckCpwr()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	HAL_ModBits(REG_DAC_BASE, MSRT_BIT3, (m_vdac_cpwr) ? MSRT_BIT3 : 0x00);
	m_vdac_cpwr = !m_vdac_cpwr;
}


void CPageDebugVBE::OnBnClickedCheckVdacypwr()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	HAL_ModBits(REG_DAC_BASE, MSRT_BIT1, (m_vdac_ypwr) ? MSRT_BIT1 : 0x00);
	m_vdac_ypwr = !m_vdac_ypwr;
}


void CPageDebugVBE::OnBnClickedCheckVdaccvbspwr()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	HAL_ModBits(REG_DAC_BASE, MSRT_BIT2, (m_vdac_cvbspwr) ? MSRT_BIT2 : 0x00);
	m_vdac_cvbspwr = !m_vdac_cvbspwr;
}

// TVE Module ////////////////////////////////////////////////////////////



void CPageDebugVBE::OnBnClickedButtonTveUpdate()
{
    // TODO: 在此添加控件通知处理程序代码
	UpdateData(FALSE);
}


void CPageDebugVBE::OnEnChangeEditDacRegTest()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CFormView::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	size_t origsize_bd = wcslen(m_str_dac_test_data) + 1;
	size_t convertedChars_bd = 0;
	const size_t newsize_bd = origsize_bd * 2;
	char *m_dac_test_data = new char[newsize_bd];
	wcstombs_s(&convertedChars_bd, m_dac_test_data, newsize_bd, m_str_dac_test_data, _TRUNCATE);

	g_16_test_data = strtoul(m_dac_test_data, NULL, 16);
	HAL_WriteWord(REG_DAC_MUX_BASE + 2, (UINT16)g_16_test_data);

}


void CPageDebugVBE::OnDeltaposSpinDacRegTest(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;

	int i_sign = 0;
	UpdateData(TRUE);

	if (pNMUpDown->iDelta == 1) {
		i_sign = -1;
	}
	else if (pNMUpDown->iDelta == -1) {
		i_sign = 1;
	}

	if (i_sign == 1)
	{
		g_16_test_data++;
		if (g_16_test_data >= 0x3ff)
			g_16_test_data = 0x3ff;
		m_str_dac_test_data.Format(_T("%03X"), g_16_test_data);
		HAL_WriteWord(REG_DAC_MUX_BASE + 2, (UINT16)g_16_test_data);
	}
	else
	{
		g_16_test_data--;
		if (g_16_test_data <= 0)
			g_16_test_data = 0;
		m_str_dac_test_data.Format(_T("%03X"), g_16_test_data);
		HAL_WriteWord(REG_DAC_MUX_BASE + 2, (UINT16)g_16_test_data);
	}
	UpdateData(FALSE);
}


void CPageDebugVBE::OnBnClickedCheckVdacTestData3ff()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	HAL_WriteWord(0xf033, 0x3ff);

	if (m_dac_reg_data_3ff)
		m_dac_reg_data_000 = FALSE;
	UpdateData(FALSE);
	m_dac_reg_data_3ff = !m_dac_reg_data_3ff;

}


void CPageDebugVBE::OnBnClickedCheckVdacTestData000()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	HAL_WriteWord(0xf033, 0x0);

	if (m_dac_reg_data_000)
		m_dac_reg_data_3ff = FALSE;
	UpdateData(FALSE);
	m_dac_reg_data_000 = !m_dac_reg_data_000;

}

// CString to char[]
void CStringToChar(CString str, char ch[]) 
{ 
	char* tmpch;	
	int wLen = WideCharToMultiByte(CP_ACP, 0, str, -1, NULL, 0, NULL, NULL);	
	tmpch = new char[wLen + 1];	
	WideCharToMultiByte(CP_ACP, 0, str, -1, tmpch, wLen, NULL, NULL);	
	for (int i = 0; i < wLen; ++i)
		ch[i] = tmpch[i]; 
}


void CPageDebugVBE::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	DM	DAC;
	char dm_instr[50];
	double current_voltage = { 0.0 };
	FILE* fp_dac_voltage = NULL;
	UINT16 u16_dac_register_val = 0;
	UINT16 i = 0;

	CPageDebugVBE::OnEnChangeEditDacRegAddr();
	fp_dac_voltage = fopen("dac_voltage.txt", "w+");
	//sprintf_s(dm_instr, "TCPIP0::192.168.188.119::inst0::INSTR");
	CStringToChar(m_mulitmeter_ip_addr, dm_instr);
	DAC.setAddress(dm_instr); //set mulitmeter ip address
	
	//set dac channel data from register
	HAL_WriteByte(g_u16_dac_reg_addr - 2, 0xff);
	HAL_WriteByte(g_u16_dac_reg_addr - 1, 0xff);

	for (i = 0; i < 10; i++)
	{
		HAL_WriteWord(g_u16_dac_reg_addr, u16_dac_register_val++);
		current_voltage = DAC.getVoltage();
		Log1(_T("The Register "), i);
		Log2(_T("Voltage = "), current_voltage);
		fprintf(fp_dac_voltage, "%lf", current_voltage);
		fprintf(fp_dac_voltage, "\n");
	}

	fclose(fp_dac_voltage);
	Log(_T("The Voltage Read Done!"));
	Log(_T("Read voltage in dac_voltage.txt!"));

	UpdateData(FALSE);
}


void CPageDebugVBE::OnEnChangeEditDacRegAddr()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CFormView::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	size_t origsize_bd = wcslen(m_dac_reg_addr) + 1;
	size_t convertedChars_bd = 0;
	const size_t newsize_bd = origsize_bd * 2;
	char *c_dac_reg_addr = new char[newsize_bd];
	wcstombs_s(&convertedChars_bd, c_dac_reg_addr, newsize_bd, m_dac_reg_addr, _TRUNCATE);

	g_u16_dac_reg_addr = strtoul(c_dac_reg_addr, NULL, 16);
	UpdateData(FALSE);
}


void CPageDebugVBE::OnEnChangeEditMultimeterIpAddr()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CFormView::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

}


void CPageDebugVBE::OnBnClickedButtonSetMultimeterIpAddr()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	DM	DAC;
	char dm_instr[50];
	bool b_multimeter_status = FALSE;

	//sprintf_s(dm_instr, "TCPIP0::192.168.188.119::inst0::INSTR");
	CStringToChar(m_mulitmeter_ip_addr, dm_instr);
	DAC.setAddress(dm_instr); //set mulitmeter ip address
	b_multimeter_status = DAC.checkInstr();

	b_multimeter_status ? Log(_T("The mulitmeter connect!")) : Log(_T("The mulitmeter disconnect!"));

	UpdateData(FALSE);
}
