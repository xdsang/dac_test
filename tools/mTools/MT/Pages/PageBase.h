#if !defined(AFX_PAGEBASE_H__312BB01A_403E_4661_938D_E0552FA2A6DB__INCLUDED_)
#define AFX_PAGEBASE_H__312BB01A_403E_4661_938D_E0552FA2A6DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PageBase.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageBase form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "ScrollBarEx.h"
#include "TextReg16.h"

class CMsSBarInfo
{
public:
    CMsSBarInfo() {reg = NULL; regh = NULL; nId = 0;};
    UINT nId;
    CScrollBarEx sbar;
    CTextReg16   *reg;
    CTextReg16   *regh;
    
    void operator = (CMsSBarInfo& p)
    {
        reg = p.reg;
        nId = p.nId;
    };
};

class CMsCheckBtnInfo
{
public:
    CMsCheckBtnInfo() {reg = NULL; nId = 0; fgRevert = FALSE;};
    UINT nId;
    CTextReg16   *reg;
    BOOL fgRevert;
    
    void operator = (CMsCheckBtnInfo& p)
    {
        reg = p.reg;
        nId = p.nId;
        fgRevert = p.fgRevert;
    };
};

class CMsRadioBtnInfo
{
public:
    CMsRadioBtnInfo() {reg = NULL; memset(pId, -1, 16);};
    CTextReg16   *reg;
    int pId[16];
    
    void operator = (CMsRadioBtnInfo& p)
    {
        reg = p.reg;
        memcpy(pId, p.pId, 16);
    };
};

class CPageBase : public CFormView
{
protected:
	CPageBase();           // protected constructor used by dynamic creation
    CPageBase(UINT nIDTemplate);
	DECLARE_DYNCREATE(CPageBase)

// Form Data
public:
	//{{AFX_DATA(CPageBase)
	enum { IDD = IDD_MS_TODO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
    CList<CMsSBarInfo, CMsSBarInfo&> m_sbarList;
    CList<CMsCheckBtnInfo, CMsCheckBtnInfo&> m_cbList;
    CList<CMsRadioBtnInfo, CMsRadioBtnInfo&> m_rbList;

// Operations
public:
    //Scroll Bar
    void createScrollBar(int id, CScrollBarEx *psbar, int value, long inMinPos, long inMaxPos);

    void createScrollBar(int id, CTextReg16 *reg);
    void createScrollBar(int id, CTextReg16 *reg, long inMinPos, long inMaxPos);

    void createScrollBar(int id, CTextReg16 *reg, CTextReg16 *regh);
    void createScrollBar(int id, CTextReg16 *reg, CTextReg16 *regh, long inMinPos, long inMaxPos);
        
    void readScrollBar();
    void updateScrollBar();
    void saveScrollBar(FILE *fp);

    CScrollBarEx * getScrollBar(int id);

    //Check Button
    void createCheckBtn(int id, CTextReg16 *reg, BOOL fgRevert = FALSE);
    void readCheckBtn();
    void updateCheckBtn();
    void saveCheckBtn(FILE *fp);

    //Radio Button
    void createRadioBtn(CTextReg16 *reg, int id0, int id1, int id2=-1, int id3=-1,
                                         int id4 =-1, int id5 =-1, int id6=-1, int id7=-1,
                                         int id8 =-1, int id9 =-1, int ida=-1, int idb=-1,
                                         int idc =-1, int idd =-1, int ide=-1, int idf=-1);
    void readRadioBtn();
    void updateRadioBtn();
    void saveRadioBtn(FILE *fp);

    //Read,Update,Save
    void readAll();
    void updateAll();
    void saveAll(FILE *fp);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageBase)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageBase();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

    // Generated message map functions
    //{{AFX_MSG(CPageBase)

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEBASE_H__312BB01A_403E_4661_938D_E0552FA2A6DB__INCLUDED_)
