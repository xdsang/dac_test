﻿// DebugDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MT.h"
#include "DebugDlg.h"
#include "driverMpi.h"
#include "stdlib.h"
#include "afx.h"
#include "regrwconvert.h"
#include "mon51rom.h"



// CDebugDlg

IMPLEMENT_DYNCREATE(CDebugDlg, CFormView)



CDebugDlg::CDebugDlg()
	: CFormView(IDD_DEBUG)
	, str_hdrxaddr(_T("8000"))
	//, str_hdrx_data_hh(_T(""))
	//, str_hdrx_data_h(_T(""))
	//, str_hdrx_data_l(_T(""))
	, str_hdrx_data_ll(_T(""))
	, uint_hdrx_length(10)
	, str_hdrx_edit_box(_T(""))
	, m_regaccessmode(0)
	, m_romaccessmode(0)
{	
	UINT8 i=0;
	i = HAL_ReadByte(0xE000);
	if ((i & 0x01) == 1)
	{
		m_romaccessmode = 1;
	}
	i = HAL_ReadByte(0xE001);
	if ((i & 0x01) == 0)
	{
		m_regaccessmode = 1;
	}
}

CDebugDlg::~CDebugDlg()
{
}

void CDebugDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//  DDX_Text(pDX, IDC_EDIT1, hrx_addr);
	DDX_Text(pDX, IDC_EDIT_HDRXADDR, str_hdrxaddr);
	DDV_MaxChars(pDX, str_hdrxaddr, 4);
	//DDX_Text(pDX, IDC_EDIT_HDRXDATA_HH, str_hdrx_data_hh);
	//DDV_MaxChars(pDX, str_hdrx_data_hh, 2);
	//DDX_Text(pDX, IDC_EDIT_HDRXDATA_H, str_hdrx_data_h);
	//DDV_MaxChars(pDX, str_hdrx_data_h, 2);
	//DDX_Text(pDX, IDC_EDIT_HDRXDATA_L, str_hdrx_data_l);
	//DDV_MaxChars(pDX, str_hdrx_data_l, 2);
	DDX_Text(pDX, IDC_EDIT_HDRXDATA_LL, str_hdrx_data_ll);
	DDV_MaxChars(pDX, str_hdrx_data_ll, 2);
	DDX_Text(pDX, IDC_EDIT_HDRX_LENGTH, uint_hdrx_length);
	DDX_Text(pDX, IDC_EDIT1, str_hdrx_edit_box);
	DDX_Radio(pDX, IDC_INT_VRADIO12, m_regaccessmode);
	DDX_Radio(pDX, IDC_INT_VRADIO14, m_romaccessmode);
}

BEGIN_MESSAGE_MAP(CDebugDlg, CFormView)
  ON_CBN_SELCHANGE(IDC_COMBOBOXEX1, &CDebugDlg::OnCbnSelchangeComboboxex1)
  ON_BN_CLICKED(IDC_BUTTON1, &CDebugDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDCANCEL, &CDebugDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON2, &CDebugDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CDebugDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &CDebugDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON4, &CDebugDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON6, &CDebugDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CDebugDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CDebugDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON10, &CDebugDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON9, &CDebugDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON11, &CDebugDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, &CDebugDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_INT_VRADIO12, &CDebugDlg::OnBnClickedIntVradio12)
	ON_BN_CLICKED(IDC_INT_VRADIO13, &CDebugDlg::OnBnClickedIntVradio13)
	ON_BN_CLICKED(IDC_INT_VRADIO14, &CDebugDlg::OnBnClickedIntVradio14)
	ON_BN_CLICKED(IDC_INT_VRADIO15, &CDebugDlg::OnBnClickedIntVradio15)
	ON_BN_CLICKED(IDC_BUTTON14, &CDebugDlg::OnBnClickedButton14)
END_MESSAGE_MAP()


// CDebugDlg 诊断

#ifdef _DEBUG
void CDebugDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDebugDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDebugDlg 消息处理程序


void CDebugDlg::OnCbnSelchangeComboboxex1()
{
  // TODO: 在此添加控件通知处理程序代码
}

//int CStringHexToInt(CString cstr)
//{
//	int temp = 0;
//
//	//char *addrpt = (LPSTR)(LPCTSTR)cstr;  // 将Csting 转换为char 											  //Log(_T("test info"));
//
//	//sscanf(addrpt, "%x", &temp);
//	//temp = atoi(addrpt);
//	//temp = _ttoi(cstr);
//	//char *addrpt = (LPSTR)(LPCTSTR)cstr.GetBuffer();
//	temp = _tstoi(cstr);
//	return temp;
//}

//int CStringHexToInt(CString str)
//{
//	int nRet = 0;
//	int count = 1;
//	for (int i = str.GetLength() - 1; i >= 0; --i)
//	{
//		int nNum = 0;
//		char chTest;
//		chTest = str.GetAt(i);       //CString一般没有这种用法，但本程序不会有问题
//		if (chTest >= '0' && chTest <= '9')
//		{
//			nNum = chTest - '0';
//		}
//		else if (chTest >= 'Á' && chTest <= 'F')
//		{
//			nNum = chTest - 'A' + 10;
//		}
//		else if (chTest >= 'a' && chTest <= 'f')
//		{
//			nNum = chTest - 'a' + 10;
//		}
//		nRet += nNum*count;
//		count *= 16;
//
//	}
//	return nRet;
//}



void CDebugDlg::OnBnClickedButton1()
{
  // TODO: 在此添加控件通知处理程序代码
	UINT8 Temp;
	UINT32 i;
	int hdrx_addr=0;
	UpdateData(TRUE);
	//char *addrpt = (LPSTR)(LPCTSTR)str_hdrxaddr;  // 将Csting 转换为char 
	////Log(_T("test info"));
	//sscanf(addrpt,"%x", &hdrx_addr);

	hdrx_addr = CStringHexToInt(str_hdrxaddr);
	str_hdrx_data_ll.Format(_T("%02x"), HAL_ReadByte(hdrx_addr));
	//str_hdrx_data_l.Format(_T("%02x"), HAL_ReadByte(hdrx_addr+1));
	//str_hdrx_data_h.Format(_T("%02x"), HAL_ReadByte(hdrx_addr+2));
	//str_hdrx_data_hh.Format(_T("%02x"), HAL_ReadByte(hdrx_addr+3));
	Log(_T("reg ")+ str_hdrxaddr+ _T(" read done"));
	//GetDlgItem(IDC_EDIT_HDRXADDR)
	UpdateData(FALSE);
}


void CDebugDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	debugLog(_T("test debug info"));
}


void CDebugDlg::OnBnClickedButton2()
{
	int hdrx_addr = 0;
	int temp = 0;
	UpdateData(TRUE);
	hdrx_addr = CStringHexToInt(str_hdrxaddr);
	HAL_WriteByte(hdrx_addr, CStringHexToInt(str_hdrx_data_ll));
	//HAL_WriteByte(hdrx_addr+1, CStringHexToInt(str_hdrx_data_l));
	//HAL_WriteByte(hdrx_addr+2, CStringHexToInt(str_hdrx_data_h));
	//HAL_WriteByte(hdrx_addr+3, CStringHexToInt(str_hdrx_data_hh));
	Log(_T("reg ") + str_hdrxaddr + _T(" write done"));
	// TODO: 在此添加控件通知处理程序代码
}

UINT8 HAL_ReadWords(UINT16 u16_index)
{
	return 0;

}



void CDebugDlg::OnBnClickedButton3()
{
	UpdateData(TRUE);
	int hdrx_addr = 0;
	CString str_temp, str_temp_hh, str_temp_h, str_temp_l, str_temp_ll;
	int i;
	hdrx_addr = CStringHexToInt(str_hdrxaddr);
	str_hdrx_edit_box = "addr:\t data \r\n";
	for (i = 0; i < uint_hdrx_length; i++)
	{
		str_temp.Format(_T("%04x:\t"), (hdrx_addr + i));
		str_hdrx_edit_box.Append(str_temp);
		str_temp_ll.Format(_T(" %02x"), HAL_ReadByte(hdrx_addr + i));
		str_hdrx_edit_box.Append(str_temp_ll + _T("\r\n"));
	}
	UpdateData(FALSE);
	Log(_T("Read xbytes done "));
	// TODO: 在此添加控件通知处理程序代码
}


void CDebugDlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	str_hdrx_edit_box = "";
	UpdateData(FALSE);
}


void CDebugDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	CString str_temp, str_temp_addr, str_temp_ll;
	int i=1,position=0,length;
	int hdrx_addr,hdrx_data_ll;
	str_temp = str_hdrx_edit_box;
	str_temp.Remove(' ');// 去掉空格
	str_temp.Remove('\t');// 去掉tab
	length = str_temp.GetLength();
	while (1)
	{
		position = (str_temp.Find('\n'));
		str_temp = str_temp.Mid(position+1);
		if (str_temp.GetLength() < 7)
		{
			break;
		}
		else
		{
			hdrx_addr = CStringHexToInt(str_temp.Mid(0, 4));
			hdrx_data_ll = CStringHexToInt(str_temp.Mid(5, 2));
			HAL_WriteByte((UINT16)hdrx_addr, (UINT8)hdrx_data_ll);
			if ((UINT16)hdrx_addr == 0)
			{
				hdrx_addr = 0;
			}

		}
	}
	Log(_T("Write xbytes done "));
}


void CDebugDlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	//UINT8 Temp;
	//UINT32 i;
	//int hdrx_addr = 0;
	//UpdateData(TRUE);
	////char *addrpt = (LPSTR)(LPCTSTR)str_hdrxaddr;  // 将Csting 转换为char 
	//////Log(_T("test info"));
	////sscanf(addrpt,"%x", &hdrx_addr);

	//hdrx_addr = CStringHexToInt(str_hdrxaddr);

	//i = HAL_ReadDWord(hdrx_addr);
	//str_hdrx_data_ll.Format(_T("%02x"), (UINT8)i);
	//str_hdrx_data_l.Format(_T("%02x"), (UINT8)(i >> 8));
	//str_hdrx_data_h.Format(_T("%02x"), (UINT8)(i >> 16));
	//str_hdrx_data_hh.Format(_T("%02x"), (UINT8)(i >> 24));

	//Log(_T("reg ") + str_hdrxaddr + _T(" read done"));
	////GetDlgItem(IDC_EDIT_HDRXADDR)
	//UpdateData(FALSE);
	UpdateData(TRUE);
	int hdrx_addr = 0;
	CString str_temp, str_temp_hh, str_temp_h, str_temp_l, str_temp_ll;
	int i;
	//hdrx_addr = CStringHexToInt(str_hdrxaddr);
	hdrx_addr = 0;
	str_hdrx_edit_box = "addr:\t data\r\n";
	str_hdrx_edit_box = "";
	for (i = 0; i < 4168; i++)
	{
		//str_temp.Format(_T("%04x:\t"), (hdrx_addr + 4 * i));
		str_temp.Format(_T("%04x:\t"), (hdrx_addr + i));
		str_hdrx_edit_box.Append(str_temp);

		str_temp_ll.Format(_T(" %02x"), HAL_ReadByte(hdrx_addr + i));
		/*str_temp_ll.Format(_T(" %02x"), HAL_ReadByte(hdrx_addr+4*i));*/
		//str_temp_l.Format(_T(" %02x"), HAL_ReadByte(hdrx_addr + 4 * i+1));
		//str_temp_h.Format(_T(" %02x"), HAL_ReadByte(hdrx_addr + 4 * i+2));
		//str_temp_hh.Format(_T("%02x"), HAL_ReadByte(hdrx_addr + 4 * i+3));

		//str_hdrx_edit_box.Append(str_temp_hh+ str_temp_h+ str_temp_l+ str_temp_ll + _T("\r\n"));
		str_hdrx_edit_box.Append(str_temp_ll + _T("\r\n"));
	}
	UpdateData(FALSE);
	Log(_T("Read xbytes done "));

}


void CDebugDlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	//int hdrx_addr = 0;
	int temp = 0;
	//UINT32 Dworddata;
	UpdateData(TRUE);
	//hdrx_addr = CStringHexToInt(str_hdrxaddr);
	//Dworddata = CStringHexToInt(str_hdrx_data_ll) + CStringHexToInt(str_hdrx_data_l) * 0x100 + CStringHexToInt(str_hdrx_data_l) * 0x10000 + CStringHexToInt(str_hdrx_data_hh) * 0x1000000;
	//HAL_WriteDWord(hdrx_addr, Dworddata);
	//HAL_WriteByte(hdrx_addr, CStringHexToInt(str_hdrx_data_ll));
	//HAL_WriteByte(hdrx_addr + 1, CStringHexToInt(str_hdrx_data_l));
	//HAL_WriteByte(hdrx_addr + 2, CStringHexToInt(str_hdrx_data_h));
	//HAL_WriteByte(hdrx_addr + 3, CStringHexToInt(str_hdrx_data_hh));
	for (temp = 0;temp<4168; temp++)
	{
		HAL_WriteByte(temp, mon51hex[temp]);
	}

	//Log(_T("reg ") + str_hdrxaddr + _T(" write done"));
	Log(_T(" write rom done"));
}


void CDebugDlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog *lpszOpenFile;
	CStdioFile file;
	CString filePathName;
	lpszOpenFile = new CFileDialog(TRUE, 
		NULL, 
		NULL,
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
		_T("文件类型（*.hex)|*.hex|所有文件（*.*)|*.*"),
		//NULL,
		NULL,
		0,
		TRUE);
	
	if (lpszOpenFile->DoModal() == IDOK)
	{
		filePathName = lpszOpenFile->GetPathName();
	}
	if (filePathName == "") return;

	if (!file.Open(filePathName, CFile::modeRead))
	{
		MessageBox(_T("can not open file"));
		return;
	}
	char *pBuf;
	DWORD dwFileLen;
	dwFileLen = file.GetLength();
	pBuf = new char[dwFileLen+1];
	pBuf[dwFileLen] = 0;
	file.Read(pBuf, dwFileLen);
	file.Close();
	delete lpszOpenFile;

	CString str_temp;
	str_temp = ("%s", pBuf);
	 

	//str_temp = str_hdrx_edit_box;

	UINT32 length1, length2=0,addr1,k, total;
	UINT32 position;
	UINT8 szBin[0x10000];
	while (true)
	{
		position = (str_temp.Find(':'));
		str_temp = str_temp.Mid(position + 1); //去掉冒号之前的数据
		if (str_temp.Mid(0, 8) == "00000001")//数据结束
		{
			break;
		}
		if (str_temp.GetLength() < 5)
		{
			break;
		}
		if (str_temp.Mid(6, 2) == "00")
		{
			length1 = CStringHexToInt(str_temp.Mid(0, 2));//将已HEX显示的文本数据转换为数值
			addr1 = CStringHexToInt(str_temp.Mid(2, 4));
			for (k = 0; k < length1; k++)
			{
				szBin[addr1 + k] = CStringHexToInt(str_temp.Mid(2 * k + 8, 2));//有效数据从第8 byte开始.不含冒号
	//			szBin[addr1 + k] = (byte)Int32.Parse(szLine.Substring(2 * k + 9, 2), System.Globalization.NumberStyles.HexNumber);  //有效数据从第9 byte开始
			}
			total = length1 + addr1;
			if (total > length2)
			{
				length2 = total;
			}
		}		
	}

	//uint_hdrx_length = length2;
	uint_hdrx_length = length2+4;  // 因为MS2160 RAM 以32 bit位单位写，所以加4个Bytes,保证最后一Byte正确写
	int i;
	str_hdrx_edit_box = "addr:\t data \r\n";
	for (i = 0; i < uint_hdrx_length; i++)
	{
		str_temp.Format(_T("%04x:\t"), i);
		str_hdrx_edit_box.Append(str_temp);
		str_temp.Format(_T(" %02x"), szBin[i]);
		str_hdrx_edit_box.Append(str_temp + _T("\r\n"));
	}
	Log(_T(" hex file open done"));
	UpdateData(FALSE);
}


void CDebugDlg::OnBnClickedButton10()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog *lpszOpenFile;
	CFile file;
	CString filePathName;
	lpszOpenFile = new CFileDialog(TRUE,
		NULL,
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T("文件类型（*.txt)|*.txt|所有文件（*.*)|*.*"),
		//NULL,
		NULL,
		0,
		TRUE);

	if (lpszOpenFile->DoModal() == IDOK)
	{
		filePathName = lpszOpenFile->GetPathName();
	}
	if (filePathName == "") return;

	if (!file.Open(filePathName, CFile::modeRead))
	{
		MessageBox(_T("can not open file"));
		return;
	}
	char *pBuf;
	DWORD dwFileLen;
	dwFileLen = file.GetLength();
	//pBuf = new char[dwFileLen];
	pBuf = new char[dwFileLen + 1];
	pBuf[dwFileLen] = 0;
	file.Read(pBuf, dwFileLen);
	file.Close();
	delete lpszOpenFile;

	CString str_temp(pBuf) ;
	str_hdrx_edit_box = str_temp;

	UpdateData(FALSE);

}

//Save text
void CDebugDlg::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	CFileDialog fileDlg(FALSE,NULL,_T("ms2160"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL);


	fileDlg.m_ofn.lpstrFilter = _T("Text Files(*.txt)\0*.txt\0ALL Files(*.*)\0*.*\0\0");
	fileDlg.m_ofn.lpstrDefExt = _T("txt");
	if (IDOK == fileDlg.DoModal())
	{
		CFile file(fileDlg.GetPathName(),CFile::modeCreate | CFile::modeWrite);
		file.Write(str_hdrx_edit_box, str_hdrx_edit_box.GetLength()*2);//*2 why ????	
		file.Close();
	}
}




void CDebugDlg::OnBnClickedButton11()
{
	// TODO: 在此添加控件通知处理程序代码
	UINT16 i;

	UpdateData(TRUE);
	for (i = 0; i < uint_hdrx_length; i++)
	{
		HAL_WriteByte(i, (UINT8)i);
	}

	Log(_T("Write %4d done ", uint_hdrx_length));
 

}


void CDebugDlg::OnBnClickedButton12()
{
	// TODO: 在此添加控件通知处理程序代码
	UINT16 i;
	UpdateData(TRUE);
	for (i = 0; i < uint_hdrx_length; i++)
	{
		if (i % 2)
		{
			HAL_WriteByte(i, 0x00);
		}
		else
		{
			HAL_WriteByte(i, 0xff);
		}
		
	}

	Log(_T("Write %4d done ", uint_hdrx_length));
}


void CDebugDlg::OnBnClickedIntVradio12()
{
	// TODO: 在此添加控件通知处理程序代码
	HAL_WriteByte(0xe001, 1);
	UpdateData(TRUE);
}


void CDebugDlg::OnBnClickedIntVradio13()
{
	// TODO: 在此添加控件通知处理程序代码
	HAL_WriteByte(0xe001, 0);
	UpdateData(TRUE);
}


void CDebugDlg::OnBnClickedIntVradio14()
{
	// TODO: 在此添加控件通知处理程序代码
	HAL_WriteByte(0xe000, 0);
	UpdateData(TRUE);
}


void CDebugDlg::OnBnClickedIntVradio15()
{
	// TODO: 在此添加控件通知处理程序代码
	HAL_WriteByte(0xe000, 1);
	UpdateData(TRUE);
}

#define USB2IIC_BUF_MAX_SIZE (2000)

void CDebugDlg::OnBnClickedButton14()
{
    // TODO: 在此添加控件通知处理程序代码
    // TODO: 在此添加控件通知处理程序代码
    UpdateData(TRUE);
    m_romaccessmode = 1;
    HAL_WriteByte(0xe000, 1);
    UpdateData(FALSE);
    CString str_temp, str_temp_addr, str_temp_ll;
    int i = 1, position = 0, length;
    int hdrx_addr, hdrx_data_ll;
    str_temp = str_hdrx_edit_box;
    str_temp.Remove(' ');// 去掉空格
    str_temp.Remove('\t');// 去掉tab
    length = str_temp.GetLength();
    UINT16 u16_len = 0;
    UINT8 u8_buf[1024*64];
    
    while (1)
    {
        position = (str_temp.Find('\n'));
        str_temp = str_temp.Mid(position + 1);
        if (str_temp.GetLength() < 7)
        {
            break;
        }
        else
        {
            hdrx_addr = CStringHexToInt(str_temp.Mid(0, 4));
            hdrx_data_ll = CStringHexToInt(str_temp.Mid(5, 2));
            //HAL_WriteByte((UINT16)hdrx_addr, (UINT8)hdrx_data_ll);
            u8_buf[u16_len] = (UINT8)hdrx_data_ll;
            u16_len ++;
            
            if ((UINT16)hdrx_addr == 0)
            {
                hdrx_addr = 0;
            }

        }
    }
    MS2160_LOG2("u16_len = ", u16_len);

    UINT16 num = u16_len / (USB2IIC_BUF_MAX_SIZE);
    UINT16 den = u16_len % (USB2IIC_BUF_MAX_SIZE);
    UINT16 j = 0;
    UINT16 u16_pos = 0;
    
    for (j = 0; j < num; j++)
    {
        mculib_i2c_burstwrite_16bidx8bval(0x16, u16_pos, USB2IIC_BUF_MAX_SIZE, &u8_buf[u16_pos]);
        u16_pos += USB2IIC_BUF_MAX_SIZE;
    }

    if (den > 0)
    {
        mculib_i2c_burstwrite_16bidx8bval(0x16, u16_pos, den, &u8_buf[u16_pos]);
    }

    
    Log(_T("Write rom code  done "));
}
