#pragma once



// CDebugDlg 窗体视图

class CDebugDlg : public CFormView
{
	DECLARE_DYNCREATE(CDebugDlg)


protected:
	CDebugDlg();           // 动态创建所使用的受保护的构造函数
	virtual ~CDebugDlg();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEBUG };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnCbnSelchangeComboboxex1();
  afx_msg void OnBnClickedButton1();
  afx_msg void OnBnClickedOk();
  afx_msg void OnBnClickedCancel();
//  UINT hrx_addr;
  CString str_hdrxaddr;
//  CString hdrx_hh;
  //CString str_hdrx_data_hh;
  //CString str_hdrx_data_h;
  //CString str_hdrx_data_l;
  CString str_hdrx_data_ll;
  afx_msg void OnBnClickedButton2();
  UINT uint_hdrx_length;
  afx_msg void OnBnClickedButton3();
  CString str_hdrx_edit_box;
  afx_msg void OnBnClickedButton5();
  afx_msg void OnBnClickedButton4();
  afx_msg void OnBnClickedButton6();
  afx_msg void OnBnClickedButton7();
  afx_msg void OnBnClickedButton8();
  afx_msg void OnBnClickedButton10();
  afx_msg void OnBnClickedButton9();
  afx_msg void OnBnClickedButton11();
  afx_msg void OnBnClickedButton12();
  int m_regaccessmode;
  afx_msg void OnBnClickedIntVradio12();
  afx_msg void OnBnClickedIntVradio13();
  int m_romaccessmode;
  afx_msg void OnBnClickedIntVradio14();
  afx_msg void OnBnClickedIntVradio15();
  afx_msg void OnBnClickedButton14();
};


