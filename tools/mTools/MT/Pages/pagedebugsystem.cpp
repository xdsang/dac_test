// pagedebugsystem.cpp : implementation file
//

#include "stdafx.h"
#include "..\mtools.h"
#include "pagedebugsystem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageDebugSystem

IMPLEMENT_DYNCREATE(CPageDebugSystem, CFormView)

CPageDebugSystem::CPageDebugSystem()
	: CFormView(CPageDebugSystem::IDD)
{
	//{{AFX_DATA_INIT(CPageDebugSystem)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPageDebugSystem::~CPageDebugSystem()
{
}

void CPageDebugSystem::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPageDebugSystem)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageDebugSystem, CFormView)
	//{{AFX_MSG_MAP(CPageDebugSystem)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageDebugSystem diagnostics

#ifdef _DEBUG
void CPageDebugSystem::AssertValid() const
{
	CFormView::AssertValid();
}

void CPageDebugSystem::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageDebugSystem message handlers
