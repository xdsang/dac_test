// PageBase.cpp : implementation file
//

#include "stdafx.h"
#include "..\mtools.h"
#include "PageBase.h"

#include "MainFrm.h"
#include "MSRegister.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageBase

IMPLEMENT_DYNCREATE(CPageBase, CFormView)

CPageBase::CPageBase()
	: CFormView(CPageBase::IDD)
{
	//{{AFX_DATA_INIT(CPageBase)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPageBase::CPageBase(UINT nIDTemplate)
    : CFormView(nIDTemplate)
{
}

CPageBase::~CPageBase()
{
    m_sbarList.RemoveAll();
    m_cbList.RemoveAll();
    m_rbList.RemoveAll();
}

void CPageBase::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPageBase)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageBase, CFormView)
	//{{AFX_MSG_MAP(CPageBase)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageBase diagnostics

#ifdef _DEBUG
void CPageBase::AssertValid() const
{
	CFormView::AssertValid();
}

void CPageBase::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/*
 * Scroll Bar
 */
void CPageBase::createScrollBar(int id, CScrollBarEx *psbar, int value, long inMinPos, long inMaxPos)
{
    CWnd* pScrollFrame = GetDlgItem(id);    
    if (pScrollFrame != NULL) {
        CRect   theScrollRect;
        
        pScrollFrame->GetWindowRect(&theScrollRect) ;
        ScreenToClient(&theScrollRect) ;
        theScrollRect.right;
        
        psbar->SetBackgroundColor(RGB(0xF0, 0xF0, 0xF0)) ;
        psbar->Create(WS_CHILD | WS_HSCROLL | WS_VISIBLE, theScrollRect, this, id) ;
        psbar->SetScrollRange(inMinPos, inMaxPos) ;
        psbar->SetScrollPos(value, TRUE) ;
        psbar->SetThumbSliderSize(CScrollBarEx::EThumbSliderSize::THUMBSLIDERSIZE_RELATIVEPCT) ;
        psbar->SetThumbStyle(CScrollBarEx::EScrollThumbStyle::SCROLLTHUMBSTYLE_VALUE);
    }
}

void CPageBase::createScrollBar(int id, CTextReg16 *reg, long inMinPos, long inMaxPos)
{
    CMsSBarInfo info;
    m_sbarList.AddTail(info);
    m_sbarList.GetTail().nId = id;
    m_sbarList.GetTail().reg = reg;
    m_sbarList.GetTail().regh = NULL;

    createScrollBar(id, &(m_sbarList.GetTail().sbar), 0, inMinPos, inMaxPos);
}


void CPageBase::createScrollBar( int id, CTextReg16 *reg )
{
    createScrollBar(id, reg, 0, (1<<(reg->length))-1);
}

void CPageBase::createScrollBar(int id, CTextReg16 *reg, CTextReg16 *regh, long inMinPos, long inMaxPos)
{
    CMsSBarInfo info;
    m_sbarList.AddTail(info);
    m_sbarList.GetTail().nId = id;
    m_sbarList.GetTail().reg = reg;
    m_sbarList.GetTail().regh = regh;
    
    createScrollBar(id, &(m_sbarList.GetTail().sbar), 0, inMinPos, inMaxPos);
}

void CPageBase::createScrollBar( int id, CTextReg16 *reg, CTextReg16 *regh )
{
    createScrollBar(id, reg, regh, 0, (1<<(reg->length + regh->length))-1);
}

void CPageBase::readScrollBar()
{ 
    int count = m_sbarList.GetCount();

    POSITION pos = m_sbarList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsSBarInfo &info = (CMsSBarInfo &)m_sbarList.GetNext(pos);
        if (NULL == info.regh)
        {
            info.sbar.SetScrollPos(pMSREG->read_text_reg(*(info.reg)), TRUE);
        }
        else
        {
            DWORD lsb = pMSREG->read_text_reg(*(info.reg));
            DWORD msb = pMSREG->read_text_reg(*(info.regh));

            info.sbar.SetScrollPos((msb << info.reg->length) + lsb, TRUE);
        }
        
    }
}

void CPageBase::updateScrollBar()
{
    int count = m_sbarList.GetCount();
    
    POSITION pos = m_sbarList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsSBarInfo &info = (CMsSBarInfo &)m_sbarList.GetNext(pos);
        if (NULL == info.regh)
        {
            pMSREG->write_text_reg(info.sbar.GetScrollPos(), *(info.reg));
        }
        else
        {
            DWORD value = info.sbar.GetScrollPos();
            pMSREG->write_text_reg(value >> info.reg->length, *(info.regh));
            pMSREG->write_text_reg(value, *(info.reg));
        }
        
    }
}

void CPageBase::saveScrollBar(FILE *fp)
{
    int count = m_sbarList.GetCount();
    
    POSITION pos = m_sbarList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsSBarInfo &info = (CMsSBarInfo &)m_sbarList.GetNext(pos);
        pMSREG->save_txt_reg(*(info.reg), fp);
        if (NULL != info.regh)
        {
            pMSREG->save_txt_reg(*(info.regh), fp);
        }
    }
}

CScrollBarEx * CPageBase::getScrollBar( int id )
{
    int count = m_sbarList.GetCount();
    
    POSITION pos = m_sbarList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsSBarInfo &info = (CMsSBarInfo &)m_sbarList.GetNext(pos);
        if (info.nId == (UINT)id)
        {
            return (&(info.sbar));
        }
    }
    return (NULL);
}

/*
 * Check Button
 */
void CPageBase::createCheckBtn( int id, CTextReg16 *reg, BOOL fgRevert /*= FALSE*/ )
{
    CMsCheckBtnInfo info;
    m_cbList.AddTail(info);
    
    m_cbList.GetTail().nId = id;
    m_cbList.GetTail().reg = reg;
    m_cbList.GetTail().fgRevert = fgRevert;
}

void CPageBase::readCheckBtn()
{
    int count = m_cbList.GetCount();
    
    POSITION pos = m_cbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsCheckBtnInfo &info = (CMsCheckBtnInfo &)m_cbList.GetNext(pos);

        CButton *btn = (CButton *)GetDlgItem(info.nId);
        if (info.fgRevert)
        {
            btn->SetCheck(!(pMSREG->read_text_reg(*(info.reg))));
        }
        else
        {
            btn->SetCheck((pMSREG->read_text_reg(*(info.reg))));
        }
    }
}

void CPageBase::updateCheckBtn()
{
    int count = m_cbList.GetCount();
    
    POSITION pos = m_cbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsCheckBtnInfo &info = (CMsCheckBtnInfo &)m_cbList.GetNext(pos);
        
        CButton *btn = (CButton *)GetDlgItem(info.nId);
        if (info.fgRevert)
        {
            pMSREG->write_text_reg(!(btn->GetCheck()), *(info.reg));
        }
        else
        {
            pMSREG->write_text_reg((btn->GetCheck()), *(info.reg));
        }
    }
}

void CPageBase::saveCheckBtn( FILE *fp )
{
    int count = m_cbList.GetCount();
    
    POSITION pos = m_cbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsCheckBtnInfo &info = (CMsCheckBtnInfo &)m_cbList.GetNext(pos);
        pMSREG->save_txt_reg(*(info.reg), fp);
    }
}

/*
 * Radio Button
 */
void CPageBase::createRadioBtn( CTextReg16 *reg, int id0, int id1, int id2/*=-1*/, int id3/*=-1*/, int id4 /*=-1*/, int id5 /*=-1*/, int id6/*=-1*/, int id7/*=-1*/, int id8 /*=-1*/, int id9 /*=-1*/, int ida/*=-1*/, int idb/*=-1*/, int idc /*=-1*/, int idd /*=-1*/, int ide/*=-1*/, int idf/*=-1*/ )
{
    CMsRadioBtnInfo info;
    m_rbList.AddTail(info);
    
    m_rbList.GetTail().reg = reg;
    m_rbList.GetTail().pId[0] = id0;
    m_rbList.GetTail().pId[1] = id1;
    m_rbList.GetTail().pId[2] = id2;
    m_rbList.GetTail().pId[3] = id3;
    m_rbList.GetTail().pId[4] = id4;
    m_rbList.GetTail().pId[5] = id5;
    m_rbList.GetTail().pId[6] = id6;
    m_rbList.GetTail().pId[7] = id7;
    m_rbList.GetTail().pId[8] = id8;
    m_rbList.GetTail().pId[9] = id9;
    m_rbList.GetTail().pId[10] = ida;
    m_rbList.GetTail().pId[11] = idb;
    m_rbList.GetTail().pId[12] = idc;
    m_rbList.GetTail().pId[13] = idd;
    m_rbList.GetTail().pId[14] = ide;
    m_rbList.GetTail().pId[15] = idf;
}

void CPageBase::readRadioBtn()
{
    int count = m_rbList.GetCount();
    
    POSITION pos = m_rbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsRadioBtnInfo &info = (CMsRadioBtnInfo &)m_rbList.GetNext(pos);
        int value = pMSREG->read_text_reg(*(info.reg));
        ASSERT(value<16);
        ASSERT(info.pId[value] != -1);

        for (int j=0; j<16; j++)
        {
            if (info.pId[j] == -1)
                break;
            ((CButton *)GetDlgItem(info.pId[j]))->SetCheck(0);
        }

        ((CButton *)GetDlgItem(info.pId[value]))->SetCheck(1);
    }
}

void CPageBase::updateRadioBtn()
{
    int count = m_rbList.GetCount();
    
    POSITION pos = m_rbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsRadioBtnInfo &info = (CMsRadioBtnInfo &)m_rbList.GetNext(pos);
        for (int j=0; j<16; j++)
        {
            if (info.pId[j] == -1)
                break;
            if (((CButton *)GetDlgItem(info.pId[j]))->GetCheck())
            {
                pMSREG->write_text_reg(j, *(info.reg));
            }
        }
    }
}

void CPageBase::saveRadioBtn(FILE *fp)
{
    int count = m_rbList.GetCount();
    
    POSITION pos = m_rbList.GetHeadPosition();
    for (int i=0; i<count; i++)
    {
        CMsRadioBtnInfo &info = (CMsRadioBtnInfo &)m_rbList.GetNext(pos);
        pMSREG->save_txt_reg(*(info.reg), fp);
    }
}

/*
 * Read, Update, Save
 */
void CPageBase::readAll()
{
    readCheckBtn();
    readRadioBtn();
    readScrollBar();
}

void CPageBase::updateAll()
{
    updateCheckBtn();
    updateRadioBtn();
    updateScrollBar();
}

void CPageBase::saveAll( FILE *fp )
{
    saveCheckBtn(fp);
    saveRadioBtn(fp);
    saveScrollBar(fp);
}
/////////////////////////////////////////////////////////////////////////////
// CPageBase message handlers

BOOL CPageBase::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
#if 0 /* NOT Update When Click */
    WORD code = HIWORD(wParam);
    if(code == BN_CLICKED)
    {
        WORD nID = LOWORD(wParam);
        
        int count = m_cbList.GetCount();
    
        POSITION pos = m_cbList.GetHeadPosition();
        for (int i=0; i<count; i++)
        {
            CMsCheckBtnInfo &info = (CMsCheckBtnInfo &)m_cbList.GetNext(pos);
            if (nID == info.nId)
            {
                CButton *btn = (CButton *)GetDlgItem(nID);
                if (info.fgRevert)
                {
                    pMSREG->write_text_reg(!(btn->GetCheck()), *(info.reg));
                }
                else
                {
                    pMSREG->write_text_reg((btn->GetCheck()), *(info.reg));
                }
            }
        }
    }
#endif
    
	return CFormView::OnCommand(wParam, lParam);
}




