// PageMSWelcom.cpp : implementation file
//

#include "stdafx.h"
#include "mTools.h"
#include "PageMSWelcom.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageMSWelcom

IMPLEMENT_DYNCREATE(CPageMSWelcom, CFormView)

CPageMSWelcom::CPageMSWelcom()
	: CFormView(CPageMSWelcom::IDD)
{
	//{{AFX_DATA_INIT(CPageMSWelcom)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPageMSWelcom::~CPageMSWelcom()
{
}

void CPageMSWelcom::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPageMSWelcom)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageMSWelcom, CFormView)
	//{{AFX_MSG_MAP(CPageMSWelcom)
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageMSWelcom diagnostics

#ifdef _DEBUG
void CPageMSWelcom::AssertValid() const
{
	CFormView::AssertValid();
}

void CPageMSWelcom::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageMSWelcom message handlers

void CPageMSWelcom::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	((CMainFrame*)AfxGetMainWnd())->m_ppageWelcome = NULL;
}


void CPageMSWelcom::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFormView::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
    CString temp;
    temp.LoadString(IDR_MAINFRAME);
	((CStatic *)GetDlgItem(IDC_STATIC_NOTICE))->SetWindowText(temp);
}
