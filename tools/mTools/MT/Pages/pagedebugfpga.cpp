// pagedebugfpga.cpp : implementation file
//

#include "stdafx.h"
#include "..\mtools.h"
#include "pagedebugfpga.h"

#define MS928X_I2C_ID (0x92)
#define MS1850_I2C_ID (0xb6)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//=========================
typedef enum _E_INPORT_
{
    IN_PORT_AV0 = 0,
    IN_PORT_AV1 = 1,
    IN_PORT_SV = 2,
    IN_PORT_VGA = 3
} INPORT_E;

//================================================================
typedef enum _E_MS1850_ASPECTRATIO_
{
    MS1850_RATIO_4_3 = 0,
    MS1850_RATIO_16_9 = 1,
    MS1850_RATIO_16_10 = 2,
    MS1850_RATIO_FULL
} MS1850_ASPECTRATIO_E;

typedef enum _E_MS1850_MULTI_PICTURE_
{
    MS1850_MULTI_PIC_4 = 0,
    MS1850_MULTI_PIC_9 = 1,
    MS1850_MULTI_PIC_16 = 2
} MS1850_MULTI_PIC_E;
//================================================================

typedef enum _E_MS1850_POP_WINDOW_POSITION_
{
    MS1850_POP_CENTER = 0,
    MS1850_POP_LEFT_DOWN = 1,
    MS1850_POP_LEFT_TOP = 2,
    MS1850_POP_RIGHT_DOWN = 3,
    MS1850_POP_RIGHT_TOP = 4
} MS1850_POP_WIN_POSITION_E;

typedef enum _E_MS1850_POP_WINDOW_SIZE_
{
    MS1850_POP_SIZE_0 = 0, //relative 1 time
    MS1850_POP_SIZE_1 = 1, //relative 1.5 times
    MS1850_POP_SIZE_2 = 2  //relative 2 times
} MS1850_POP_WIN_SIZE_E;

UINT16 g_u16_fpga_vds_clk = 5400;
//#define FPAG_CLK            ((UINT16) 5400)
#define PLLVM_MAX_CLK       ((UINT16) 16500)

UINT8 g_u8_inport = IN_PORT_AV0;

UINT8 g_u8_vga_in_mode = VFMT_INVALID;
VIDEOTIMING_T g_st_vga_in_timing;


UINT8 g_u8_tv_mode = MS1850_TVMODE_INVALID;
VIDEOTIMING_T g_st_tv_in_timing;

UINT8 g_u8_output_mode = VFMT_CEA_02_720x480P_60HZ;
VIDEOTIMING_T g_st_output_timing;

UINT8 g_u8_output_index = 0;

#define OUTPOUT_MAX _countof(g_arrOutputTable)

VIDEOSIZE_T g_st_in_size;
VIDEOSIZE_T g_st_mem_size;
VIDEOSIZE_T g_st_out_size;

//
UINT8 g_u8_multi_id = 0;
VIDEOSIZE_T g_st_multi_win_size;
UINT8 g_u8_multi_pic = MS1850_MULTI_PIC_4;
//

UINT8 g_u8_pattern = MS1850_PATTERN_PURE_BLACK;

UINT8 g_u8_brightness = 50;
UINT8 g_u8_contrast = 50;
UINT8 g_u8_saturation = 50;
UINT8 g_u8_hue = 50;

UINT16 g_u16_win_purdah_cnt = 0;
MS1850POPWIN_T g_st_pop_win;
MS1850WINBORDER_T g_st_display;
MS1850WINBORDER_T g_st_internal;

UINT8 g_u8_aspect_ratio = MS1850_RATIO_FULL;

//
UINT8 g_u8_pop_phase = 15;
UINT8 g_u8_pop_size_mode = MS1850_POP_SIZE_0;
UINT8 g_u8_pop_position_mode = MS1850_POP_CENTER;
//
INT16 g_i16_pixel_shift = 0;
INT16 g_i16_line_shift = 0;

UINT8 g_arrOutputTable[] =
{
    VFMT_CEA_02_720x480P_60HZ,   //0
    //VFMT_CEA_17_720x576P_50HZ,

    //
    VFMT_VESA_64_640X480_60,    //1
    VFMT_VESA_66_800X600_60,    //2
    VFMT_VESA_68_800X600_75,    //3
    VFMT_VESA_71_1024X768_60,   //4
    VFMT_VESA_73_1024X768_75,   //5
    VFMT_VESA_84_1280X768_60,   //6
    VFMT_VESA_96_1280X1024_60,  //7
    VFMT_VESA_97_1280X1024_75,  //8
    VFMT_VESA_103_1400X1050_60,    //9
    VFMT_VESA_107_1440X900_60_DMT, //10
    VFMT_VESA_115_1600X1200_60,    //11
    VFMT_VESA_120_1680X1050_60,    //12
    VFMT_VESA_129_1920X1080_60_DMT,    //13
    VFMT_VESA_132_1920X1200_60_CVT, //14
};

/////////////////////////////////////////////////////////////////////////////
// CPageDebugFPGA

IMPLEMENT_DYNCREATE(CPageDebugFPGA, CFormView)

CPageDebugFPGA::CPageDebugFPGA()
    : CFormView(CPageDebugFPGA::IDD)
{
    //{{AFX_DATA_INIT(CPageDebugFPGA)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
}

CPageDebugFPGA::~CPageDebugFPGA()
{
}

void CPageDebugFPGA::DoDataExchange(CDataExchange* pDX)
{
    CFormView::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CPageDebugFPGA)
        // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageDebugFPGA, CFormView)
    //{{AFX_MSG_MAP(CPageDebugFPGA)
    ON_BN_CLICKED(IDC_FPGA_SYS_SETUP, OnFpgaSysSetup)
    ON_BN_CLICKED(IDC_FPGA_SYS_OUTRES_SWITCH, OnFpgaSysOutresSwitch)
    ON_BN_CLICKED(IDC_FPGA_SYS_START, OnFpgaSysStart)
    ON_BN_CLICKED(IDC_FPGA_SYS_VIDEO_MUTE_ENABLE, OnFpgaSysVideoMuteEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_MUTE_PATTERN, OnFpgaSysMutePattern)
    ON_BN_CLICKED(IDC_FPGA_SYS_BRIGHTNESS_ENABLE, OnFpgaSysBrightnessEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_CONTRAST_ENABLE, OnFpgaSysContrastEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_SATURATION_ENABLE, OnFpgaSysSaturationEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_HUE_ENABLE, OnFpgaSysHueEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_BRIGHTNESS_DEC, OnFpgaSysBrightnessDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_BRIGHTNESS_INC, OnFpgaSysBrightnessInc)
    ON_BN_CLICKED(IDC_FPGA_SYS_CONTRAST_DEC, OnFpgaSysContrastDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_CONTRAST_INC, OnFpgaSysContrastInc)
    ON_BN_CLICKED(IDC_FPGA_SYS_SATURATION_DEC, OnFpgaSysSaturationDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_SATURATION_INC, OnFpgaSysSaturationInc)
    ON_BN_CLICKED(IDC_FPGA_SYS_HUE_DEC, OnFpgaSysHueDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_HUE_INC, OnFpgaSysHueInc)
    ON_BN_CLICKED(IDC_FPGA_SYS_PURDAH_OFF, OnFpgaSysPurdahOff)
    ON_WM_TIMER()
    ON_BN_CLICKED(IDC_FPGA_SYS_PURDAH_ON, OnFpgaSysPurdahOn)
    ON_BN_CLICKED(IDC_FPGA_SYS_SCREEN_4_3, OnFpgaSysScreen43)
    ON_BN_CLICKED(IDC_FPGA_SYS_SCREEN_16_9, OnFpgaSysScreen169)
    ON_BN_CLICKED(IDC_FPGA_SYS_SCREEN_16_10, OnFpgaSysScreen1610)
    ON_BN_CLICKED(IDC_FPGA_SYS_SCREEN_FULL, OnFpgaSysScreenFull)
    ON_BN_CLICKED(IDC_FPGA_SYS_MULTI_WIN_4, OnFpgaSysMultiWin4)
    ON_BN_CLICKED(IDC_FPGA_SYS_MULTI_WIN_9, OnFpgaSysMultiWin9)
    ON_BN_CLICKED(IDC_FPGA_SYS_MULTI_WIN_16, OnFpgaSysMultiWin16)
    ON_BN_CLICKED(IDC_FPGA_SYS_POP_ENABLE, OnFpgaSysPopEnable)
    ON_BN_CLICKED(IDC_FPGA_SYS_POP_SIZE_SW, OnFpgaSysPopSizeSw)
    ON_BN_CLICKED(IDC_FPGA_SYS_POP_POS_SW, OnFpgaSysPopPosSw)
    ON_EN_CHANGE(IDC_FPGA_SYS_VDS_CLK, OnChangeFpgaSysVdsClk)
    ON_WM_SHOWWINDOW()
    ON_BN_CLICKED(IDC_FPGA_SYS_READ_BINFILE_VERSION, OnFpgaSysReadBinfileVersion)
    ON_BN_CLICKED(IDC_FPGA_SYS_PIXEL_SHIFT_DEC, OnFpgaSysPixelShiftDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_PIXEL_SHIFT_INC, OnFpgaSysPixelShiftInc)
    ON_BN_CLICKED(IDC_FPGA_SYS_INPORT, OnFpgaSysInport)
    ON_BN_CLICKED(IDC_FPGA_SYS_LINE_SHIFT_DEC, OnFpgaSysLineShiftDec)
    ON_BN_CLICKED(IDC_FPGA_SYS_LINE_SHIFT_INC, OnFpgaSysLineShiftInc)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageDebugFPGA diagnostics

#ifdef _DEBUG
void CPageDebugFPGA::AssertValid() const
{
    CFormView::AssertValid();
}

void CPageDebugFPGA::Dump(CDumpContext& dc) const
{
    CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageDebugFPGA message handlers

VOID sys_switch_inport(UINT8 u8_inport)
{
    switch (u8_inport)
    {
    case IN_PORT_AV0:
#if MS1850_FPGA_VERIFY
        ms928x_set_clamp(TRUE);
#endif
        ms1850_vga_in_bypass_enable(FALSE);
        ms1850_set_input_port(IN_ANALOG_CVBS0);
        break;
    case IN_PORT_AV1:
#if MS1850_FPGA_VERIFY
        ms928x_set_clamp(TRUE);
#endif
        ms1850_vga_in_bypass_enable(FALSE);
        ms1850_set_input_port(IN_ANALOG_CVBS1);
        break;
    case IN_PORT_SV:
#if MS1850_FPGA_VERIFY
        ms928x_set_clamp(FALSE);
#endif
        ms1850_vga_in_bypass_enable(FALSE);
        ms1850_set_input_port(IN_ANALOG_SVIDEO);
        break;
    case IN_PORT_VGA:
        ms1850_vga_in_bypass_enable(TRUE);
        break;
    }
}

void sys_set_pop_win(UINT8 u8_size_mode, UINT8 u8_position_mode)
{
    MS1850POPWIN_T st_pop_win;
    VIDEOSIZE_T st_pop_size;
    UINT8 u8_tmp = 4;
    UINT16 u16_hst;
    UINT16 u16_vst;

    switch (u8_size_mode)
    {
    case MS1850_POP_SIZE_0:
        u8_tmp = 4;
        break;

    case MS1850_POP_SIZE_1:
        u8_tmp = 3;
        break;

    case MS1850_POP_SIZE_2:
        u8_tmp = 2;
        break;
    }

    st_pop_size.u16_h = g_st_vga_in_timing.u16_hactive / u8_tmp;
    st_pop_size.u16_v = g_st_vga_in_timing.u16_vactive / u8_tmp;

    switch (u8_position_mode)
    {
    case MS1850_POP_CENTER:
        u16_hst = (g_st_vga_in_timing.u16_hactive - st_pop_size.u16_h) / 2;
        u16_vst = (g_st_vga_in_timing.u16_vactive - st_pop_size.u16_v) / 2;
        break;

    case MS1850_POP_LEFT_DOWN:
        u16_hst = 20;
        u16_vst = g_st_vga_in_timing.u16_vactive - 20 - st_pop_size.u16_v;
        break;

    case MS1850_POP_LEFT_TOP:
        u16_hst = 20;
        u16_vst = 20;
        break;

    case MS1850_POP_RIGHT_DOWN:
        u16_hst = g_st_vga_in_timing.u16_hactive - 20 - st_pop_size.u16_h;
        u16_vst = g_st_vga_in_timing.u16_vactive - 20 - st_pop_size.u16_v;
        break;

    case MS1850_POP_RIGHT_TOP:
        u16_hst = g_st_vga_in_timing.u16_hactive - 20 - st_pop_size.u16_h;
        u16_vst = 20;
        break;
    }

    st_pop_win.u16_hst = u16_hst + g_st_vga_in_timing.u16_hoffset;
    st_pop_win.u16_hsp = st_pop_win.u16_hst + st_pop_size.u16_h;
    //
    st_pop_win.u16_vst = u16_vst + g_st_vga_in_timing.u16_voffset;
    st_pop_win.u16_vsp = st_pop_win.u16_vst + st_pop_size.u16_v;

    MS1850_LOG2("st_pop_win.u16_hst = ", st_pop_win.u16_hst);
    MS1850_LOG2("st_pop_win.u16_hsp = ", st_pop_win.u16_hsp);
    MS1850_LOG2("st_pop_win.u16_vst = ", st_pop_win.u16_vst);
    MS1850_LOG2("st_pop_win.u16_vsp = ", st_pop_win.u16_vsp);

    ms1850_set_pop_win(&st_pop_win);
    ms1850_set_video_zoom(g_st_in_size, st_pop_size);
}

void sys_switch_ouput_aspect_ratio(MS1850_ASPECTRATIO_E e_ratio)
{
    UINT8  u8_tmp = 10; //default 16:10
    UINT16 u16_tmp;

    if (e_ratio == MS1850_RATIO_FULL)
    {
        g_st_out_size.u16_h = g_st_output_timing.u16_hactive;
        g_st_out_size.u16_v = g_st_output_timing.u16_vactive;
    }
    else
    {
        if (e_ratio == MS1850_RATIO_4_3)
        {
            u8_tmp = 12; //(4*4) : (4*3)
        }
        else if (e_ratio == MS1850_RATIO_16_9)
        {
            u8_tmp = 9;
        }

        u16_tmp = 16 * g_st_output_timing.u16_vactive / u8_tmp;

        if (g_st_output_timing.u16_hactive >= u16_tmp)
        {
            g_st_out_size.u16_h = u16_tmp;
            g_st_out_size.u16_v = g_st_output_timing.u16_vactive;
        }
        else
        {
            g_st_out_size.u16_h = g_st_output_timing.u16_hactive;
            g_st_out_size.u16_v = u8_tmp * g_st_output_timing.u16_hactive / 16;
        }
    }
}

void sys_switch_ouput_mode(UINT8 u8_index, BOOL b_mute)
{
    ms1850_get_std_timing(g_arrOutputTable[u8_index], &g_st_output_timing);

#if MS1850_FPGA_VERIFY
    g_st_output_timing.u16_htotal = (UINT32)g_st_output_timing.u16_htotal * g_u16_fpga_vds_clk / g_st_output_timing.u16_pixclk;
    g_st_output_timing.u16_hsyncwidth = (UINT32)g_st_output_timing.u16_hsyncwidth * g_u16_fpga_vds_clk / g_st_output_timing.u16_pixclk;
    g_st_output_timing.u16_hoffset = (UINT32)g_st_output_timing.u16_hoffset * g_u16_fpga_vds_clk / g_st_output_timing.u16_pixclk;
    g_st_output_timing.u16_hactive = (UINT32)g_st_output_timing.u16_hactive * g_u16_fpga_vds_clk / g_st_output_timing.u16_pixclk;
#endif

    MS1850_LOG2("g_st_output_timing.u16_hactive = ", g_st_output_timing.u16_hactive);
    MS1850_LOG2("g_st_output_timing.u16_vactive = ", g_st_output_timing.u16_vactive);

    g_st_out_size.u16_h = g_st_output_timing.u16_hactive;
    g_st_out_size.u16_v = g_st_output_timing.u16_vactive;
    ms1850_video_mute_enable(b_mute);
    ms1850_shutdown_output_enable(TRUE);
    ms1850_set_video_output_timing(&g_st_output_timing);
    g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
    ms1850_shutdown_output_enable(FALSE);
    ms1850_video_mute_enable(b_mute);
}

void Init_Global(void)
{
#if MS1850_FPGA_VERIFY
    HAL_SetChipAddr(MS928X_I2C_ID);
    if (HAL_ReadByte(0x01) == 0x28)
    {
        MS1850_LOG("ms9281b chip connect.");
    }
    else
    {
        MS1850_LOG("ms9281b chip disconnect.");
    }

    HAL_SetChipAddr(MS1850_I2C_ID);
    if (HAL_ReadByte(0x01) == 0x85)
    {
        MS1850_LOG("ms1850 chip connect.");
    }
    else
    {
        MS1850_LOG("ms1850 chip disconnect.");
    }
    //for fpga system fast set up.
    //ms1850_init_for_fpga();
#endif

    // initialization of variable
    g_u8_vga_in_mode = VFMT_INVALID;
    g_u8_tv_mode = MS1850_TVMODE_INVALID;
    g_u8_output_index = 0;
    g_st_in_size.u16_h = 720;
    g_st_in_size.u16_v = 480;
    g_st_display.i16_top = 0;
    g_st_display.i16_bottom = 0;
    g_st_display.i16_left = 0;
    g_st_display.i16_right = 0;
    g_st_internal = g_st_display;

#if MS1850_FPGA_VERIFY
    ms928x_adc_init_for_fpga(FALSE);
#endif

    // #############for mem init issue Waring##############
    // resolve in 20141210
    //ms1850drv_cap_en(FALSE);
    //ms1850drv_pb_en(FALSE);
    //#####################################################

    //#############first patch for tvd & compout##############
    //20150114
    //HAL_SetBits(0x207, MSRT_BIT6);
    //HAL_SetBits(0x2b2, MSRT_BIT6);
//	HAL_SetBits(0xe, MSRT_BIT4);
//	HAL_SetBits(0xc01, MSRT_BITS2_1);
    //#####################################################

    ms1850_init();
#if 0
    HAL_SetBits(0x2b2, MSRT_BIT6);
#endif
    sys_switch_inport(g_u8_inport);
    sys_switch_ouput_mode(g_u8_output_index, TRUE);
    Delay_ms(10);
    MS1850_LOG("Init Done.");
}

void Media_Service(void)
{
    UINT8 u8_vga_in_mode = VFMT_INVALID;
    UINT8 u8_mode = ms1850_get_tv_mode();
    BOOL b_vga_valid = ms1850_get_vga_input_timing(&g_st_vga_in_timing);

    if (b_vga_valid)
    {
        MS1850_LOG2("Is Progr      = ", (g_st_vga_in_timing.u8_polarity & MSRT_BIT0) ? 1 : 0);
        MS1850_LOG2("Hsync Pol     = ", (g_st_vga_in_timing.u8_polarity & MSRT_BIT1) ? 1 : 0);
        MS1850_LOG2("Vsync Pol     = ", (g_st_vga_in_timing.u8_polarity & MSRT_BIT2) ? 1 : 0);
        MS1850_LOG2("u16detHTotal  = ", g_st_vga_in_timing.u16_htotal);
        MS1850_LOG2("u16detVTotal  = ", g_st_vga_in_timing.u16_vtotal);
        MS1850_LOG2("u16detHW      = ", g_st_vga_in_timing.u16_hsyncwidth);
        MS1850_LOG2("u16detVW      = ", g_st_vga_in_timing.u16_vsyncwidth);
        //HSyncFreq / 10
        MS1850_LOG2("u16detHFreq   = ", (UINT16)(g_st_vga_in_timing.u16_pixclk * 1000UL / g_st_vga_in_timing.u16_htotal));
        MS1850_LOG2("u16detVFreq   = ", g_st_vga_in_timing.u16_vfreq);
        u8_vga_in_mode = ms1850_match_std_timing(&g_st_vga_in_timing);
    }

    if (g_u8_vga_in_mode != u8_vga_in_mode)
    {
        g_u8_vga_in_mode = u8_vga_in_mode;
        MS1850_LOG2("u8_vga_in_mode = ", u8_vga_in_mode);
        if (u8_vga_in_mode == VFMT_INVALID)
        {
            MS1850_LOG("vga signal lost.");
        }
        else
        {
            MS1850_LOG("vga signal change.");
            ms1850_set_vga_input_timing(&g_st_vga_in_timing);
        }
    }

    if (g_u8_tv_mode != u8_mode)
    {
        g_u8_tv_mode = u8_mode;
        MS1850_LOG2("g_u8_tv_mode = ", g_u8_tv_mode);
        if (g_u8_tv_mode == MS1850_TVMODE_INVALID)
        {
            MS1850_LOG("tv signal lost.");
            ms1850_video_mute_enable(TRUE);
        }
        else
        {
            MS1850_LOG("tv signal change.");
            ms1850_video_mute_enable(TRUE);
            ms1850_get_tv_input_timing(g_u8_tv_mode, &g_st_tv_in_timing);
            ms1850_set_tv_mode(g_u8_tv_mode);
            g_st_in_size.u16_h = g_st_tv_in_timing.u16_hactive;
            g_st_in_size.u16_v = g_st_tv_in_timing.u16_vactive;
            g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
            ms1850_video_mute_enable(FALSE);
        }
    }
}

void CPageDebugFPGA::OnFpgaSysSetup()
{
    // TODO: Add your control notification handler code here
    Init_Global();
    Media_Service();
}

void CPageDebugFPGA::OnFpgaSysOutresSwitch()
{
    // TODO: Add your control notification handler code here
    g_u8_output_index++;
    if (g_u8_output_index > OUTPOUT_MAX)
    {
        g_u8_output_index = 0;
    }
    MS1850_LOG2("g_u8_output_index = ", g_u8_output_index);
    sys_switch_ouput_mode(g_u8_output_index, FALSE);
}

void CPageDebugFPGA::OnFpgaSysStart()
{
    // TODO: Add your control notification handler code here

}

void CPageDebugFPGA::OnFpgaSysVideoMuteEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_VIDEO_MUTE_ENABLE))->GetCheck())
    {
        ms1850_video_mute_enable(TRUE);
    }
    else
    {
        ms1850_video_mute_enable(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysMutePattern()
{
    // TODO: Add your control notification handler code here
    g_u8_pattern++;
    if (g_u8_pattern > MS1850_PATTERN_INTERLACE_BLUE)
    {
        g_u8_pattern = MS1850_PATTERN_PURE_BLACK;
    }
    ms1850_set_video_mute_pattern(g_u8_pattern);
}

void CPageDebugFPGA::OnFpgaSysBrightnessEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_BRIGHTNESS_ENABLE))->GetCheck())
    {
        ms1850drv_vds_switch_brightness(TRUE);
    }
    else
    {
        ms1850drv_vds_switch_brightness(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysContrastEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_CONTRAST_ENABLE))->GetCheck())
    {
        ms1850drv_vds_switch_contrast(TRUE);
    }
    else
    {
        ms1850drv_vds_switch_contrast(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysSaturationEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_SATURATION_ENABLE))->GetCheck())
    {
        ms1850drv_vds_switch_saturation(TRUE);
    }
    else
    {
        ms1850drv_vds_switch_saturation(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysHueEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_HUE_ENABLE))->GetCheck())
    {
        ms1850drv_vds_switch_hue(TRUE);
    }
    else
    {
        ms1850drv_vds_switch_hue(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysBrightnessDec()
{
    // TODO: Add your control notification handler code here
    INT8 i8_val;
    if (g_u8_brightness > 0)
    {
        g_u8_brightness--;
    }
    i8_val = g_u8_brightness * 256 / 100 - 128;
    CString c;
    c.Format(_T("%d"), g_u8_brightness);
    SetDlgItemText(IDC_STATIC_STATUS_BRIGHTNESS, c);
    ms1850_set_video_brightness(i8_val);
}

void CPageDebugFPGA::OnFpgaSysBrightnessInc()
{
    // TODO: Add your control notification handler code here
    INT8 i8_val;
    if (g_u8_brightness < 100)
    {
        g_u8_brightness++;
    }
    i8_val = g_u8_brightness * 256 / 100 - 128;
    CString c;
    c.Format(_T("%d"), g_u8_brightness);
    SetDlgItemText(IDC_STATIC_STATUS_BRIGHTNESS, c);
    ms1850_set_video_brightness(i8_val);
}

void CPageDebugFPGA::OnFpgaSysContrastDec()
{
    // TODO: Add your control notification handler code here
    UINT8 u8_val;
    if (g_u8_contrast > 0)
    {
        g_u8_contrast--;
    }
    u8_val = g_u8_contrast * 256 / 100;
    CString c;
    c.Format(_T("%d"), g_u8_contrast);
    SetDlgItemText(IDC_STATIC_STATUS_CONTRAST, c);
    ms1850_set_video_contrast(u8_val);
}

void CPageDebugFPGA::OnFpgaSysContrastInc()
{
    // TODO: Add your control notification handler code here
    UINT8 u8_val;
    if (g_u8_contrast < 100)
    {
        g_u8_contrast++;
    }
    u8_val = g_u8_contrast * 256 / 100;
    CString c;
    c.Format(_T("%d"), g_u8_contrast);
    SetDlgItemText(IDC_STATIC_STATUS_CONTRAST, c);
    ms1850_set_video_contrast(u8_val);
}

void CPageDebugFPGA::OnFpgaSysSaturationDec()
{
    // TODO: Add your control notification handler code here
    UINT8 u8_val;
    if (g_u8_saturation > 0)
    {
        g_u8_saturation--;
    }
    u8_val = g_u8_saturation * 256 / 100;
    CString c;
    c.Format(_T("%d"), g_u8_saturation);
    SetDlgItemText(IDC_STATIC_STATUS_SATURATION, c);
    ms1850_set_video_saturation(u8_val);
}

void CPageDebugFPGA::OnFpgaSysSaturationInc()
{
    // TODO: Add your control notification handler code here
    UINT8 u8_val;
    if (g_u8_saturation < 100)
    {
        g_u8_saturation++;
    }
    u8_val = g_u8_saturation * 256 / 100;
    CString c;
    c.Format(_T("%d"), g_u8_saturation);
    SetDlgItemText(IDC_STATIC_STATUS_SATURATION, c);
    ms1850_set_video_saturation(u8_val);
}

void CPageDebugFPGA::OnFpgaSysHueDec()
{
    // TODO: Add your control notification handler code here
    INT8 i8_val;
    if (g_u8_hue > 0)
    {
        g_u8_hue--;
    }
    i8_val = g_u8_hue * 256 / 100 - 128;
    CString c;
    c.Format(_T("%d"), g_u8_hue);
    SetDlgItemText(IDC_STATIC_STATUS_HUE, c);
    ms1850_set_video_hue(i8_val);
}

void CPageDebugFPGA::OnFpgaSysHueInc()
{
    // TODO: Add your control notification handler code here
    INT8 i8_val;
    if (g_u8_hue < 100)
    {
        g_u8_hue++;
    }
    i8_val = g_u8_hue * 256 / 100 - 128;
    CString c;
    c.Format(_T("%d"), g_u8_hue);
    SetDlgItemText(IDC_STATIC_STATUS_HUE, c);
    ms1850_set_video_hue(i8_val);
}

void CPageDebugFPGA::OnFpgaSysPurdahOff()
{
    // TODO: Add your control notification handler code here
    BOOL b_over = FALSE;
    g_st_display.i16_top = 0;
    g_st_display.i16_bottom = 0;
    g_st_display.i16_left = 0;
    g_st_display.i16_right = 0;
    g_st_internal = g_st_display;
    g_u16_win_purdah_cnt = 0;
    //SetTimer(0, 30, 0);

    while (1)
    {
        g_st_display.i16_left += 20;
        g_st_display.i16_right += 20;
        g_st_internal.i16_left -= 20;
        if (g_st_display.i16_left >= g_st_output_timing.u16_hactive / 2)
        {
            g_st_display.i16_left = g_st_output_timing.u16_hactive / 2;
            g_st_display.i16_right = g_st_output_timing.u16_hactive - g_st_display.i16_left;
            g_st_internal.i16_left = -g_st_display.i16_left;
            b_over = TRUE;
        }
        ms1850_set_display_win_border(&g_st_display);
        ms1850_set_internal_win_border(&g_st_internal);
        ms1850_set_win_border_trigger();
        if (b_over)
        {
            break;
        }
        Delay_ms(30);
    }
}

void CPageDebugFPGA::OnTimer(UINT nIDEvent)
{
    // TODO: Add your message handler code here and/or call default
    UINT16 u16_h_st;
    UINT16 u16_v_st;
    g_u8_multi_id++;

    if (g_u8_multi_pic == MS1850_MULTI_PIC_4)
    {
        if (g_u8_multi_id > 3)
        {
            g_u8_multi_id = 0;
        }

        u16_h_st = 10 + (g_st_multi_win_size.u16_h + 10) * (g_u8_multi_id % 2);
        u16_v_st = 10 + (g_st_multi_win_size.u16_v + 10) * (g_u8_multi_id / 2);
    }
    else if (g_u8_multi_pic == MS1850_MULTI_PIC_9)
    {
        if (g_u8_multi_id > 8)
        {
            g_u8_multi_id = 0;
        }

        u16_h_st = 10 + (g_st_multi_win_size.u16_h + 10) * (g_u8_multi_id % 3);
        u16_v_st = 10 + (g_st_multi_win_size.u16_v + 10) * (g_u8_multi_id / 3);
    }
    else if (g_u8_multi_pic == MS1850_MULTI_PIC_16)
    {
        if (g_u8_multi_id > 15)
        {
            g_u8_multi_id = 0;
        }

        u16_h_st = 10 + (g_st_multi_win_size.u16_h + 10) * (g_u8_multi_id % 4);
        u16_v_st = 10 + (g_st_multi_win_size.u16_v + 10) * (g_u8_multi_id / 4);
    }

    // do
    ms1850_set_multi_win_position(u16_h_st, u16_v_st);

    CFormView::OnTimer(nIDEvent);
}

void CPageDebugFPGA::OnFpgaSysPurdahOn()
{
    // TODO: Add your control notification handler code here
    BOOL b_over = FALSE;
    g_st_display.i16_top = 0;
    g_st_display.i16_bottom = 0;
    g_st_display.i16_left = g_st_output_timing.u16_hactive / 2;
    g_st_display.i16_right = g_st_output_timing.u16_hactive - g_st_display.i16_left;
    g_st_internal.i16_top = 0;
    g_st_internal.i16_bottom = 0;
    g_st_internal.i16_left = -g_st_display.i16_left;
    g_st_internal.i16_right = 0;

    //SetTimer(0, 30, 0);

    while (1)
    {
        g_st_display.i16_left -= 20;
        g_st_display.i16_right -= 20;
        g_st_internal.i16_left += 20;
        if (g_st_display.i16_left <= 0)
        {
            g_st_display.i16_left = 0;
            g_st_display.i16_right = 0;
            g_st_internal.i16_left = 0;
            b_over = TRUE;
        }
        ms1850_set_display_win_border(&g_st_display);
        ms1850_set_internal_win_border(&g_st_internal);
        ms1850_set_win_border_trigger();
        if (b_over)
        {
            break;
        }
        Delay_ms(30);
    }
}

void CPageDebugFPGA::OnFpgaSysScreen43()
{
    // TODO: Add your control notification handler code here
    sys_switch_ouput_aspect_ratio(MS1850_RATIO_4_3);
    g_st_internal.i16_top = (g_st_output_timing.u16_vactive - g_st_out_size.u16_v) / 2;
    g_st_internal.i16_bottom = g_st_display.i16_top;
    g_st_internal.i16_left = (g_st_output_timing.u16_hactive - g_st_out_size.u16_h) / 2;
    g_st_internal.i16_right = g_st_internal.i16_left;
    ms1850_set_internal_win_border(&g_st_internal);
    ms1850_set_win_border_trigger();
    g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
}

void CPageDebugFPGA::OnFpgaSysScreen169()
{
    // TODO: Add your control notification handler code here
    sys_switch_ouput_aspect_ratio(MS1850_RATIO_16_9);
    g_st_internal.i16_top = (g_st_output_timing.u16_vactive - g_st_out_size.u16_v) / 2;
    g_st_internal.i16_bottom = g_st_display.i16_top;
    g_st_internal.i16_left = (g_st_output_timing.u16_hactive - g_st_out_size.u16_h) / 2;
    g_st_internal.i16_right = g_st_internal.i16_left;
    ms1850_set_internal_win_border(&g_st_internal);
    ms1850_set_win_border_trigger();
    g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
}

void CPageDebugFPGA::OnFpgaSysScreen1610()
{
    // TODO: Add your control notification handler code here
    sys_switch_ouput_aspect_ratio(MS1850_RATIO_16_10);
    g_st_internal.i16_top = (g_st_output_timing.u16_vactive - g_st_out_size.u16_v) / 2;
    g_st_internal.i16_bottom = g_st_display.i16_top;
    g_st_internal.i16_left = (g_st_output_timing.u16_hactive - g_st_out_size.u16_h) / 2;
    g_st_internal.i16_right = g_st_internal.i16_left;
    ms1850_set_internal_win_border(&g_st_internal);
    ms1850_set_win_border_trigger();
    g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
}

void CPageDebugFPGA::OnFpgaSysScreenFull()
{
    // TODO: Add your control notification handler code here
    sys_switch_ouput_aspect_ratio(MS1850_RATIO_FULL);
    g_st_internal.i16_top = (g_st_output_timing.u16_vactive - g_st_out_size.u16_v) / 2;
    g_st_internal.i16_bottom = g_st_display.i16_top;
    g_st_internal.i16_left = (g_st_output_timing.u16_hactive - g_st_out_size.u16_h) / 2;
    g_st_internal.i16_right = g_st_internal.i16_left;
    ms1850_set_internal_win_border(&g_st_internal);
    ms1850_set_win_border_trigger();
    g_st_mem_size = ms1850_set_video_zoom(g_st_in_size, g_st_out_size);
}

void CPageDebugFPGA::OnFpgaSysMultiWin4()
{
    // TODO: Add your control notification handler code here
    KillTimer(0);
    g_u8_multi_id = 0;
    g_u8_multi_pic = MS1850_MULTI_PIC_4;
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_4))->GetCheck())
    {
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_9))->SetCheck(0);
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_16))->SetCheck(0);
        ms1850_multi_win_init();
        g_st_multi_win_size.u16_h = (g_st_mem_size.u16_h - 30) / 2;
        g_st_multi_win_size.u16_v = (g_st_mem_size.u16_v - 30) / 2;
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_multi_win_size);
        ms1850_set_multi_win_position(10, 10);
        SetTimer(0, 5000, 0);
    }
    else
    {
        KillTimer(0);
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_mem_size);
        ms1850_multi_win_release();
    }
}

void CPageDebugFPGA::OnFpgaSysMultiWin9()
{
    // TODO: Add your control notification handler code here
    KillTimer(0);
    g_u8_multi_id = 0;
    g_u8_multi_pic = MS1850_MULTI_PIC_9;
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_9))->GetCheck())
    {
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_4))->SetCheck(0);
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_16))->SetCheck(0);
        ms1850_multi_win_init();
        g_st_multi_win_size.u16_h = (g_st_mem_size.u16_h - 40) / 3;
        g_st_multi_win_size.u16_v = (g_st_mem_size.u16_v - 40) / 3;
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_multi_win_size);
        ms1850_set_multi_win_position(10, 10);
        SetTimer(0, 5000, 0);
    }
    else
    {
        KillTimer(0);
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_mem_size);
        ms1850_multi_win_release();
    }
}

void CPageDebugFPGA::OnFpgaSysMultiWin16()
{
    // TODO: Add your control notification handler code here
    KillTimer(0);
    g_u8_multi_id = 0;
    g_u8_multi_pic = MS1850_MULTI_PIC_16;
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_16))->GetCheck())
    {
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_4))->SetCheck(0);
        ((CButton *)GetDlgItem(IDC_FPGA_SYS_MULTI_WIN_9))->SetCheck(0);
        ms1850_multi_win_init();
        g_st_multi_win_size.u16_h = (g_st_mem_size.u16_h - 50) / 4;
        g_st_multi_win_size.u16_v = (g_st_mem_size.u16_v - 50) / 4;
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_multi_win_size);
        ms1850_set_multi_win_position(10, 10);
        SetTimer(0, 5000, 0);
    }
    else
    {
        KillTimer(0);
        ms1850_video_mute_enable(TRUE);
        ms1850_set_multi_win_zoom(g_st_in_size, g_st_mem_size);
        ms1850_multi_win_release();
        ms1850_video_mute_enable(FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysPopEnable()
{
    // TODO: Add your control notification handler code here
    if (1 == ((CButton *)GetDlgItem(IDC_FPGA_SYS_POP_ENABLE))->GetCheck())
    {
        sys_set_pop_win(g_u8_pop_size_mode, g_u8_pop_position_mode);
#if MS1850_FPGA_VERIFY
        ms1850_tv_in_video_freeze(TRUE);
#endif
        ms1850_tv_pop_enable(TRUE);
#if MS1850_FPGA_VERIFY
        ms928x_adc_init_for_fpga(TRUE);
        ms928x_addpll_config_for_fpga();
#endif
    }
    else
    {
        ms1850_tv_pop_enable(FALSE);
#if MS1850_FPGA_VERIFY
        ms1850_tv_in_video_freeze(FALSE);
        ms928x_adc_init_for_fpga(FALSE);
#endif
        sys_switch_ouput_mode(g_u8_output_index, FALSE);
    }
}

void CPageDebugFPGA::OnFpgaSysPopSizeSw()
{
    // TODO: Add your control notification handler code here
    if (g_u8_pop_size_mode < MS1850_POP_SIZE_2)
    {
        g_u8_pop_size_mode++;
    }
    else
    {
        g_u8_pop_size_mode = MS1850_POP_SIZE_0;
    }
    MS1850_LOG2("g_u8_pop_size_mode = ", g_u8_pop_size_mode);
    sys_set_pop_win(g_u8_pop_size_mode, g_u8_pop_position_mode);
}

void CPageDebugFPGA::OnFpgaSysPopPosSw()
{
    // TODO: Add your control notification handler code here
    if (g_u8_pop_position_mode < MS1850_POP_RIGHT_TOP)
    {
        g_u8_pop_position_mode++;
    }
    else
    {
        g_u8_pop_position_mode = MS1850_POP_CENTER;
    }
    MS1850_LOG2("g_u8_pop_position_mode = ", g_u8_pop_position_mode);
    sys_set_pop_win(g_u8_pop_size_mode, g_u8_pop_position_mode);
}

void CPageDebugFPGA::OnChangeFpgaSysVdsClk()
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CFormView::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.

    // TODO: Add your control notification handler code here
    CString str;
    int iValue = 5400;
    int iRet = 0;

    ((CEdit *)GetDlgItem(IDC_FPGA_SYS_VDS_CLK))->GetWindowText(str);

    size_t origsize = wcslen(str.GetBuffer(20)) + 1;
    size_t convertedChars = 0;
    const size_t newsize = origsize * 2;
    char *nstring = new char[newsize];
    wcstombs_s(&convertedChars, nstring, newsize, str.GetBuffer(20), _TRUNCATE);
    iRet = sscanf_s(nstring, "%d", &iValue);
    str.ReleaseBuffer();

    for (int i = 0; i < str.GetLength(); i++)
    {
        if (!((str[i] >= '0' && str[i] <= '9')))
        {
            AfxMessageBox(_T("Invalid parameters"));
            str.Format(_T("%d"), iValue);
            ((CEdit *)GetDlgItem(IDC_FPGA_SYS_VDS_CLK))->SetWindowText(str);
            return;
        }
    }

    g_u16_fpga_vds_clk = iValue;
}

void CPageDebugFPGA::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CFormView::OnShowWindow(bShow, nStatus);

    // TODO: Add your message handler code here
    CString str;
    str.Format(_T("%d"), g_u16_fpga_vds_clk);
    ((CEdit *)GetDlgItem(IDC_FPGA_SYS_VDS_CLK))->SetWindowText(str);

    OnFpgaSysReadBinfileVersion();
}

void CPageDebugFPGA::OnFpgaSysReadBinfileVersion()
{
    // TODO: Add your control notification handler code here
    CString str0;
    CString str1;

    UINT32 temp = (UINT32)HAL_ReadByte(0x7b);
    temp <<= 8;
    temp = temp | (UINT32)HAL_ReadByte(0x7c);
    temp <<= 8;
    temp = temp | (UINT32)HAL_ReadByte(0x7d);
    temp <<= 8;
    temp = temp | (UINT32)HAL_ReadByte(0x7e);

    str0.Format(_T("%08x"), temp);
    ((CEdit *)GetDlgItem(IDC_STATIC_BINFILE_VERSION0))->SetWindowText(str0);
    temp = (UINT32)HAL_ReadByte(0x7f);
    str1.Format(_T("%02x"), (UINT8)temp);
    ((CEdit *)GetDlgItem(IDC_STATIC_BINFILE_VERSION1))->SetWindowText(str1);
}

void CPageDebugFPGA::OnFpgaSysPixelShiftDec()
{
    // TODO: Add your control notification handler code here
    MS1850WINBORDER_T st_int;

    if (g_i16_pixel_shift > (0 - (INT16)g_st_out_size.u16_h))
    {
        g_i16_pixel_shift--;
        MS1850_LOG2("g_i16_pixel_shift = ", g_i16_pixel_shift);
    }

    st_int.i16_left = g_st_internal.i16_left + g_i16_pixel_shift;
    st_int.i16_right = g_st_internal.i16_right - g_i16_pixel_shift;
    st_int.i16_top = g_st_internal.i16_top;
    st_int.i16_bottom = g_st_internal.i16_bottom;

    MS1850_LOG2("st_int.i16_left = ", st_int.i16_left);
    MS1850_LOG2("st_int.i16_right = ", st_int.i16_right);
    ms1850_set_internal_win_border(&st_int);
    ms1850_set_win_border_trigger();
}

void CPageDebugFPGA::OnFpgaSysPixelShiftInc()
{
    // TODO: Add your control notification handler code here
    MS1850WINBORDER_T st_int;

    if (g_i16_pixel_shift < (0 + (INT16)g_st_out_size.u16_h))
    {
        g_i16_pixel_shift++;
        MS1850_LOG2("g_i16_pixel_shift = ", g_i16_pixel_shift);
    }

    st_int.i16_left = g_st_internal.i16_left + g_i16_pixel_shift;
    st_int.i16_right = g_st_internal.i16_right - g_i16_pixel_shift;
    st_int.i16_top = g_st_internal.i16_top;
    st_int.i16_bottom = g_st_internal.i16_bottom;

    MS1850_LOG2("st_int.i16_left = ", st_int.i16_left);
    MS1850_LOG2("st_int.i16_right = ", st_int.i16_right);
    ms1850_set_internal_win_border(&st_int);
    ms1850_set_win_border_trigger();
}

void CPageDebugFPGA::OnFpgaSysInport()
{
    // TODO: Add your control notification handler code here
    if (g_u8_inport < IN_PORT_VGA)
    {
        g_u8_inport++;
    }
    else
    {
        g_u8_inport = IN_PORT_AV0;
    }
    ms1850_set_input_port(g_u8_inport);
    MS1850_LOG2("g_u8_inport = ", g_u8_inport);
}

void CPageDebugFPGA::OnFpgaSysLineShiftDec()
{
    // TODO: Add your control notification handler code here
    MS1850WINBORDER_T st_int;

    if (g_i16_line_shift > (0 - (INT16)g_st_out_size.u16_v))
    {
        g_i16_line_shift--;
        MS1850_LOG2("g_i16_line_shift = ", g_i16_line_shift);
    }

    st_int.i16_left = g_st_internal.i16_left;
    st_int.i16_right = g_st_internal.i16_right;
    st_int.i16_top = g_st_internal.i16_top + g_i16_line_shift;
    st_int.i16_bottom = g_st_internal.i16_bottom - g_i16_line_shift;

    MS1850_LOG2("st_int.i16_top = ", st_int.i16_top);
    MS1850_LOG2("st_int.i16_bottom = ", st_int.i16_bottom);
    ms1850_set_internal_win_border(&st_int);
    ms1850_set_win_border_trigger();
}

void CPageDebugFPGA::OnFpgaSysLineShiftInc()
{
    // TODO: Add your control notification handler code here
    MS1850WINBORDER_T st_int;

    if (g_i16_line_shift < (0 + (INT16)g_st_out_size.u16_v))
    {
        g_i16_line_shift++;
        MS1850_LOG2("g_i16_line_shift = ", g_i16_line_shift);
    }

    st_int.i16_left = g_st_internal.i16_left;
    st_int.i16_right = g_st_internal.i16_right;
    st_int.i16_top = g_st_internal.i16_top + g_i16_line_shift;
    st_int.i16_bottom = g_st_internal.i16_bottom - g_i16_line_shift;

    MS1850_LOG2("st_int.i16_top = ", st_int.i16_top);
    MS1850_LOG2("st_int.i16_bottom = ", st_int.i16_bottom);
    ms1850_set_internal_win_border(&st_int);
    ms1850_set_win_border_trigger();
}
