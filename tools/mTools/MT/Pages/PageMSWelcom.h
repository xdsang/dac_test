#if !defined(AFX_PAGEMSWELCOM_H__D7163996_8B35_4E8A_AB70_99C8AE1CB4E3__INCLUDED_)
#define AFX_PAGEMSWELCOM_H__D7163996_8B35_4E8A_AB70_99C8AE1CB4E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PageMSWelcom.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageMSWelcom form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPageMSWelcom : public CFormView
{
protected:
	CPageMSWelcom();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageMSWelcom)

// Form Data
public:
	//{{AFX_DATA(CPageMSWelcom)
	enum { IDD = IDD_MS_WELCOME_PAGE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageMSWelcom)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageMSWelcom();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageMSWelcom)
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEMSWELCOM_H__D7163996_8B35_4E8A_AB70_99C8AE1CB4E3__INCLUDED_)
