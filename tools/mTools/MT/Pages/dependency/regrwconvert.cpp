﻿#include "stdafx.h"
#include "regrwconvert.h"
#include "MT.h"
#include "driverMpi.h"
//#include "stdlib.h"
//#include "afx.h"




int CStringHexToInt(CString str)
{
	int nRet = 0;
	int count = 1;
	for (int i = str.GetLength() - 1; i >= 0; --i)
	{
		int nNum = 0;
		char chTest;
		chTest = str.GetAt(i);       //CString一般没有这种用法，但本程序不会有问题
		if (chTest >= '0' && chTest <= '9')
		{
			nNum = chTest - '0';
		}
		else if (chTest >= 'Á' && chTest <= 'F')
		{
			nNum = chTest - 'A' + 10;
		}
		else if (chTest >= 'a' && chTest <= 'f')
		{
			nNum = chTest - 'a' + 10;
		}
		nRet += nNum*count;
		count *= 16;

	}
	return nRet;
}

UINT32 HAL_Read_Dwords(int addr)
{
	UINT8 Temp[4];
	UINT32 i;
	//HAL_ReadBytes(addr, 4, Temp);
	for (i = 0; i < 4; i++)
	{
		Temp[i] = HAL_ReadByte(addr+i);
	}
	i = Temp[0] + 0x100 * Temp[1] + 0x10000 * Temp[2] + 0x1000000 * Temp[3];
	return i;
}

void HAL_Write_Dwords(int addr,UINT32 value)
{

	UINT32 i;
	for (i = 0; i < 4; i++)
	{
		HAL_WriteByte(addr + i, (value >>( i * 8)));
	}

}

