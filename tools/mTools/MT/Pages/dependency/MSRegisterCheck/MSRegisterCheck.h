#if !defined(MSREGISTERCHECK_H_)
#define MSREGISTERCHECK_H_


#define MAX_REGISTER_NUM    (0x1000)
#define MAX_ERROR_COUNT     (1000)

#define ICTestReadRange   HAL_ReadRange
#define ICTestWriteRange  HAL_WriteRange


#define NA                  0
#define RO                  1
#define WO                  2
#define RW                  3

typedef struct _register_attributes
{
    UINT16      u16index;
    UINT8       u8flag;
    UINT8       u8start;
    UINT8       u8bits;
    UINT32      u32default;
}REGATTR_T;

class CMSRegisterCheck
{
public:
    CMSRegisterCheck(CString strFilename, CString strReportPathname);
    CMSRegisterCheck();
    ~CMSRegisterCheck();

protected:
    REGATTR_T   RegArray[MAX_REGISTER_NUM];
    int         g_i_check_stop;
    CString     g_strFilename;
    CString     g_strReportPathname;
    UINT16      g_u16RegTotalCount;
    UINT16      g_u16DefaultErrCount;
    UINT16      g_u16ReadOnlyErrCount;
    UINT16      g_u16WroteOnlyErrCount;
    UINT16      g_u16ReadWroteErrCount;

    UINT16      g_u16DefaultCheckCount;
    UINT16      g_u16ReadWroteCheckCount;

    
    int _Reg_ExecuteLine(char *pLine, REGATTR_T* reg);
    int _Reg_CheckDefault(REGATTR_T st_RegAttr);
    int _Reg_CheckReadOnly(REGATTR_T st_RegAttr);
    int _Reg_CheckWriteOnly(REGATTR_T st_RegAttr);
    int _Reg_CheckReadWrite(REGATTR_T st_RegAttr);
    void _ErrLog(REGATTR_T* pst_RegAttr, UINT16* pu16Write, UINT16* pu16Rea, bool b_Flog = FALSE);

public:
    UINT16  LoadRegisterMap(void);
    void    SetCheckState(int i_start);
    int     GetCheckState(void);
    void    CheckDefault(void);
    void    CheckRW();
    UINT16  GetDefaultCheckCount(void);
    UINT16  GetRWCheckCount(void);
    UINT16  GetRegTotalCount(void);
    void    LogErr();
};

#endif