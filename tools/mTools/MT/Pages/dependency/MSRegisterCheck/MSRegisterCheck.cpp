
#include "stdafx.h"
#include <iostream>
#include "MSRegisterCheck.h"

#include <regex>

UINT16 __CODE arrTestPat[] =
{
    0xAA, 0x55, 0xA5, 0x5A,         //pattern 0
    0x0000, 0xFFFF,                 //pattern 1
    0x0002, 0x0001, 0x0004, 0x0003, 0x0008, 0x0007, //pattern 2 & 3
    0x0010, 0x000F, 0x0020, 0x001F, 0x0040, 0x003F, 0x0080, 0x007F,
    0x0100, 0x00FF, 0x0200, 0x01FF, 0x0400, 0x03ff, 0x0800, 0x07FF,
    0x1000, 0x0FFF, 0x2000, 0x1FFF, 0x4000, 0x3FFF, 0x8000, 0x7FFF,
};

CMSRegisterCheck::CMSRegisterCheck(CString strFilename, CString strReportPathname)
{
    g_u16RegTotalCount = 0;
    g_u16DefaultErrCount = 0;
    g_u16ReadOnlyErrCount = 0;
    g_u16WroteOnlyErrCount = 0;
    g_u16ReadWroteErrCount = 0;

    g_u16DefaultCheckCount = 0;
    g_u16ReadWroteCheckCount = 0;

    this->g_i_check_stop = 1;
    this->g_strFilename = strFilename;
    this->g_strReportPathname = strReportPathname;
}

CMSRegisterCheck::CMSRegisterCheck()
{

}

CMSRegisterCheck::~CMSRegisterCheck()
{

}

UINT16 CMSRegisterCheck::LoadRegisterMap(void)
{
    char        buf[260];
    UINT16      len = 0;

    FILE  *fp = NULL;
    _wfopen_s(&fp, g_strFilename, L"r");
    if (fp)
    {
        while (!feof(fp))
        {
            //get 1 line, if 
            if (!fgets(buf, 255, fp))
            {
                break;
            }
            if (_Reg_ExecuteLine(buf, &RegArray[len]))
            {
                len++;
            }
        }
        fclose(fp);
        _wfopen_s(&fp, g_strReportPathname, L"w+");
        if (fp)
        {
            fclose(fp);
        }
        else
        {
            return 0;
        }
    }
    g_u16RegTotalCount = len;
    return len;
}

void CMSRegisterCheck::SetCheckState(int i_start)
{
    g_i_check_stop = i_start ? 0 : 1;
}

int CMSRegisterCheck::GetCheckState(void)
{
    return g_i_check_stop ? 0 : 1;
}

UINT16 CMSRegisterCheck::GetDefaultCheckCount(void)
{
    return g_u16DefaultCheckCount;
}

UINT16 CMSRegisterCheck::GetRWCheckCount(void)
{
    return g_u16ReadWroteCheckCount;
}

UINT16  CMSRegisterCheck::GetRegTotalCount(void)
{
    return g_u16RegTotalCount;
}

void CMSRegisterCheck::CheckDefault(void)
{
    UINT16 i;
    FILE  *fp = NULL;

    _wfopen_s(&fp, g_strReportPathname, L"a");
    if (fp)
    {
        fprintf(fp, "\nCheck Default\r\n");
        fclose(fp);
        for (i = 0; i < g_u16RegTotalCount; i++)
        {
            if (g_i_check_stop == 1)
            {
                return;
            }
            g_u16DefaultCheckCount = i + 1;
            if (_Reg_CheckDefault(RegArray[i]))
            {
                this->g_u16DefaultErrCount++;
            }
        }
    }
}

void CMSRegisterCheck::CheckRW()
{
    UINT16 i;
    FILE  *fp = NULL;

    _wfopen_s(&fp, g_strReportPathname, L"a");
    if (fp)
    {
        fprintf(fp, "\nCheck ReadWrite\r\n");
        fclose(fp);
        for (i = 0; i < g_u16RegTotalCount; i++)
        {
            if (g_i_check_stop == 1)
            {
                return;
            }
            g_u16ReadWroteCheckCount = i + 1;
            switch (RegArray[i].u8flag)
            {
            case NA:
                _ErrLog(&RegArray[i], NULL, NULL, TRUE);
                break;
            case RO:
                if (_Reg_CheckReadOnly(RegArray[i]))
                {
                    this->g_u16ReadOnlyErrCount++;
                }
                break;
            case WO:
                if (_Reg_CheckWriteOnly(RegArray[i]))
                {
                    this->g_u16WroteOnlyErrCount++;
                }
                break;
            case RW:
                if (_Reg_CheckReadWrite(RegArray[i]))
                {
                    this->g_u16ReadWroteErrCount++;
                }
                break;
            default:
                break;
            }
        }
    }
}

void CMSRegisterCheck::_ErrLog(REGATTR_T* pst_RegAttr, UINT16* pu16Write, UINT16* pu16Read, bool b_Flog)
{
    FILE  *fp = NULL;
    char Attr[3];
    _wfopen_s(&fp, g_strReportPathname, L"a");
    if (fp)
    {
        if (((pu16Write == NULL) || (pu16Read == NULL)) && !b_Flog)
        {
            fprintf(fp, "\n");
        }
        if (pst_RegAttr != NULL)
        {
            switch (pst_RegAttr->u8flag)
            {
            case NA:
                sscanf_s("NA", "%s", &Attr, 3);
                break;

            case RO:
                sscanf_s("RO", "%s", &Attr, 3);
                break;

            case WO:
                sscanf_s("WO", "%s", &Attr, 3);
                break;

            case RW:
                sscanf_s("RW", "%s", &Attr, 3);
                break;
            }
            
            fprintf(fp, "\t{0x%04x, %s, %02u, %02u, 0x%04x}", pst_RegAttr->u16index,
                Attr,
                pst_RegAttr->u8start,
                pst_RegAttr->u8bits,
                pst_RegAttr->u32default);
        }
        if (pu16Write != NULL)
        {
            fprintf(fp, "\t\tWriteValue = 0x%04x", *pu16Write);
        }
        if (pu16Read != NULL)
        {
            fprintf(fp, "\tReadValue = 0x%04x \n", *pu16Read);
        }
        if (b_Flog)
        {
            fprintf(fp, "\tOK!");
        }
        if ((pu16Write == NULL) && (pu16Read == NULL))
        {
            fprintf(fp, "\n");
        }
        fclose(fp);
    }
    else
    {
        //AfxMessageBox(_T("Open Report file error!"));
        //cout << a << endl;
    }
}

void CMSRegisterCheck::LogErr()
{
    FILE  *fp = NULL;

    _wfopen_s(&fp, g_strReportPathname, L"a");
    if (fp)
    {
        fprintf(fp, "\nRegisters Total: %d \r\n", g_u16RegTotalCount);
        fprintf(fp, "Check Default Number: %d \r\n", g_u16DefaultCheckCount);
        fprintf(fp, "Check Read/Write Number: %d \r\n", g_u16ReadWroteCheckCount);

        fprintf(fp, "\t- %d\tDefault error(s)\r\n", g_u16DefaultErrCount);

        fprintf(fp, "\t- %d\tReadWrite error(s) ,%d RO + %d Wo + %d RW\r\n", (g_u16ReadOnlyErrCount +
            g_u16WroteOnlyErrCount +
            g_u16ReadWroteErrCount),
            g_u16ReadOnlyErrCount,
            g_u16WroteOnlyErrCount,
            g_u16ReadWroteErrCount);
        fclose(fp);
    }
    else
    {
        //AfxMessageBox(_T("Open Report file error!"));
        //cout << a << endl;
    }
}

int CMSRegisterCheck::_Reg_ExecuteLine(char *pLine, REGATTR_T* reg)
{
    char    cmd[6];
    char    cmd1[3];

    int     fields;
    int     i = 0;

    /* do nothing is line is empty */
    if (!strlen(pLine))
        return FALSE;

    while (i < strlen(pLine))
    {
        if (*pLine == ' ' || *pLine == '    ')
        {
            pLine++;
        }
        else if (*pLine == '#' || *pLine == '/')
        {
            return FALSE;
        }
        else
        {
            break;
        }

        i++;
    }

    //cmd: MSREG 0x0000 RO 0 8 0x0000
    fields = sscanf_s(pLine, "%5s 0x%4x %2s %2u %2u 0x%4x",
        &cmd,  6,
        &reg->u16index,
        &cmd1,3,
        &reg->u8start,
        &reg->u8bits,
        &reg->u32default
    );
    /* no field may be caused from empty line */
    if (!fields || (fields == EOF))
        return FALSE;

    if (reg->u8start > 16)
    {
        reg->u8start = 1;
    }

    if (reg->u8bits > 16)
    {
        reg->u8bits = 1;
    }

    if (!_strcmpi(cmd1, "NA"))
    {
        reg->u8flag = 0;
    }
    else if (!_strcmpi(cmd1, "RO"))
    {
        reg->u8flag = 1;
    }
    else if (!_strcmpi(cmd1, "WO"))
    {
        reg->u8flag = 2;
    }
    else if (!_strcmpi(cmd1, "RW"))
    {
        reg->u8flag = 3;
    }
    else
    {
        reg->u8flag = 3;
    }
    return TRUE;
}


int CMSRegisterCheck::_Reg_CheckDefault(REGATTR_T st_RegAttr)
{
    UINT16  u16mask = ~(0xffff << st_RegAttr.u8bits);

    // Save the original reg value
    UINT16  u16Read = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);

    if (u16Read != (st_RegAttr.u32default & u16mask))
    {
        _ErrLog(&st_RegAttr, NULL, &u16Read, FALSE);
        return TRUE;
    }
    else
    {
        _ErrLog(&st_RegAttr, NULL, NULL, TRUE);
        return FALSE;
    }
}

int CMSRegisterCheck::_Reg_CheckReadOnly(REGATTR_T st_RegAttr)
{
    UINT16  i, j;
    UINT16  u16Read = 0;
    UINT16  u16Write = 0;
    UINT16  u16errs = 0;
    UINT16  u16mask = ~(0xffff << st_RegAttr.u8bits);
    UINT16  u16Start = ((st_RegAttr.u8bits == 1) || (st_RegAttr.u8bits == 2)) ? 4 : 0;
    UINT16  u16Stop = 4 + st_RegAttr.u8bits * 2;
    UINT16  u16Save = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);

    for (i = 0; i < 2; i++)
    {
        for (j = u16Start; j < u16Stop; j++)
        {
            u16Write = (arrTestPat[j] & u16mask);
            ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Write);
            u16Read = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);
            if (u16Read != (st_RegAttr.u32default & u16mask))
            {
                if (u16errs == 0)
                {
                    _ErrLog(&st_RegAttr, NULL, NULL);
                }
                u16errs++;
                _ErrLog(NULL, &u16Write, &u16Read);
            }
            if (u16errs >= MAX_ERROR_COUNT)
            {
                break;
            }
        }
    }
    // Restore it back
    ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Save);
    if (u16errs > 0)
    {
        return TRUE;
    }
    else
    {
        _ErrLog(&st_RegAttr, NULL, NULL, TRUE);
        return FALSE;
    }
}

int CMSRegisterCheck::_Reg_CheckWriteOnly(REGATTR_T st_RegAttr)
{
    UINT16  i, j;
    UINT16  u16Read = 0;
    UINT16  u16Write = 0;
    UINT16  u16errs = 0;
    UINT16  u16mask = ~(0xffff << st_RegAttr.u8bits);
    UINT16  u16Start = ((st_RegAttr.u8bits == 1) || (st_RegAttr.u8bits == 2)) ? 4 : 0;
    UINT16  u16Stop = 4 + st_RegAttr.u8bits * 2;
    UINT16  u16Save = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);

    for (i = 0; i < 2; i++)
    {
        for (j = u16Start; j < u16Stop; j++)
        {
            u16Write = (arrTestPat[j] & u16mask);
            ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Write);
            u16Read = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);
            //if (u16Read != (st_RegAttr.u16default & u16mask))
            if (u16Read != u16Save)
            {
                if (u16errs == 0)
                {
                    _ErrLog(&st_RegAttr, NULL, NULL);
                }
                u16errs++;
                _ErrLog(NULL, &u16Write, &u16Read);
            }
            if (u16errs >= MAX_ERROR_COUNT)
            {
                break;
            }
        }
    }
    // Restore it back
    ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Save);
    if (u16errs > 0)
    {
        return TRUE;
    }
    else
    {
        _ErrLog(&st_RegAttr, NULL, NULL, TRUE);
        return FALSE;
    }
}

int CMSRegisterCheck::_Reg_CheckReadWrite(REGATTR_T st_RegAttr)
{
    UINT16  i, j;
    UINT16  u16Read = 0;
    UINT16  u16Write = 0;
    UINT16  u16errs = 0;
    UINT16  u16mask = ~(0xffff << st_RegAttr.u8bits);
    UINT16  u16Start = ((st_RegAttr.u8bits == 1) || (st_RegAttr.u8bits == 2)) ? 4 : 0;
    UINT16  u16Stop = 4 + st_RegAttr.u8bits * 2;
    UINT16  u16Save = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);

    for (i = 0; i < 2; i++)
    {
        for (j = u16Start; j < u16Stop; j++)
        {
            u16Write = (arrTestPat[j] & u16mask);
            ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Write);
            u16Read = ICTestReadRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits);
            if (u16Read != u16Write)
            {
                if (u16errs == 0)
                {
                    _ErrLog(&st_RegAttr, NULL, NULL);
                }
                u16errs++;
                if (i == 0)
                {
                    _ErrLog(NULL, &u16Write, &u16Read);
                }
            }
            if (u16errs >= (MAX_ERROR_COUNT - 1))
            {
                break;
            }
        }
    }
    // Restore it back
    ICTestWriteRange(st_RegAttr.u16index, st_RegAttr.u8start, st_RegAttr.u8bits, u16Save);
    if (u16errs > 0)
    {
        return TRUE;
    }
    else
    {
        _ErrLog(&st_RegAttr, NULL, NULL, TRUE);
        return FALSE;
    }
}


