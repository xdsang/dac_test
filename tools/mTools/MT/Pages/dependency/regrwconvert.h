#pragma once

#ifndef __REGRWCONVERT_H__
#define __REGRWCONVERT_H__

#define HDMIRX_BASEADDR 0x8000


int CStringHexToInt(CString str);
UINT32 HAL_Read_Dwords(int addr);
void HAL_Write_Dwords(int addr, UINT32 value);
#endif