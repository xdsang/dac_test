
#pragma once

#include "ViewTree.h"
#include "CParseTXT.h"
#include "PropertyGrid.h"

//?????
class CClassToolBar : public CMFCToolBar //, public CToolBarCtrl
{
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

//?????????????
class CMyEdit :public CEdit
{
public:
	virtual ~CMyEdit();
};

//View Class
class CClassView : public CDockablePane
{
public:
	CClassView();
	virtual ~CClassView();
	void AdjustLayout();
	const CString m_LabelFontName = _T("Courier New");
	const int m_LabelFontSize = 15;

protected:
	CClassToolBar m_wndToolBar;
	CViewTree m_wndClassView;
	CMyEdit m_edit;
	CRect m_rect_search;
	CRect m_rect_refresh;
	CRect m_rect_testrw;
	CButton m_btn_refresh;
	CButton m_btn_testrw;
	PropertyGridProperty* m_module_group;
	CString m_edit_content;
	CString m_group_name;
	string m_reg_name;
	string m_reg_description;
	int m_reg_length;
	BYTE m_reg_start_bit;
	string m_reg_attribute;
	WORD m_reg_address;
	CString m_str_name_field;
	CString m_str_description_convert;
	CString m_str_attribute;
	WORD m_reg_num;
	UINT32 m_reg_current_value;
	
	int m_spin_range;
	UINT32 m_value_field;
	CString m_str_description_field;

	BOOL m_b_refresh = false;
	BOOL m_b_testrw = false;

private:
	CFont m_Font;

// ??
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPackUp();
	afx_msg void OnTestRW();
	afx_msg void OnRefresh();
	LRESULT OnPropertyChanged(__in WPARAM wparam, __in LPARAM lparam);

protected:
	void UpdateFonts();
	BOOL GeneratTxtForTest();
	void InitPropList();
	BOOL PreTranslateMessage(MSG* pMsg);
	//void SetVSDotNetLook(BOOL bSet)
	//{
	//	m_wndPropList.SetVSDotNetLook(bSet);
	//	m_wndPropList.SetGroupNameFullWidth(bSet);
	//}
	void load_txt_temp();
	void turn_on_log_print();
	void turn_off_log_print();
	void do_search();
public:
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

