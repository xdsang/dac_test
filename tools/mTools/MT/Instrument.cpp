#include "stdafx.h"
#include "Instrument.h"

/************************************************************************/
/* LNA                                                                 */
/************************************************************************/


LNA::LNA()
{
	m_need_delay = false;
	m_delay_ms = 0;
}

void LNA::setAddress(char *strAdd)
{
	strcpy_s(m_strAddress, strAdd);
}

void LNA::setDelay(bool need_delay, int delay_ms)
{
	m_need_delay = need_delay;
	m_delay_ms = delay_ms;
}

bool LNA::checkInstr()
{
	static	ViSession defaultRM, instr;
	static	ViStatus status;

	status = viOpenDefaultRM(&defaultRM);
	if (status < VI_SUCCESS)
	{
		printf("Could not open a session to the VISA Resource Manager!\n");
		return FALSE;
	}

	status = viOpen(defaultRM, m_strAddress, VI_NULL, VI_NULL, &instr);
	if (status < VI_SUCCESS)
	{
		printf("An error occurred opening the session to %s\n", m_strAddress);
		viClose(defaultRM);
		return FALSE;
	}

	status = viClose(instr);
	status = viClose(defaultRM);

	return TRUE;
}

bool LNA::setParam()
{
	static char outputBuffer[VI_FIND_BUFLEN];
	static	ViSession defaultRM, instr;
	static	ViStatus status;
	static	ViUInt32 count;
	static	ViUInt16 portNo;
	int		num;

	status = viOpenDefaultRM(&defaultRM);
	if (status < VI_SUCCESS)
	{
		printf("Could not open a session to the VISA Resource Manager!\n");
		//exit (EXIT_FAILURE);
		return FALSE;
	}

	status = viOpen(defaultRM, m_strAddress, VI_NULL, VI_NULL, &instr);
	if (status < VI_SUCCESS)
	{
		printf("An error occurred opening the session to %s\n", m_strAddress);
		viClose(defaultRM);
		//exit (EXIT_FAILURE);
		return FALSE;
	}
	num = m_csCommand.GetLength();
	status = viWrite(instr, (ViBuf)m_csCommand.GetBuffer(1), num, &count);

	if (status < VI_SUCCESS)
	{
		printf("viRead failed with error code %x \n", status);
		viClose(defaultRM);
		//exit (EXIT_FAILURE);
		return FALSE;
	}

	if (m_need_delay)
	{
		//m_csCommand.Format("*OPC?");
		m_csCommand.Format(_T("*OPC?"));
		num = m_csCommand.GetLength();
		status = viWrite(instr, (ViBuf)m_csCommand.GetBuffer(1), num, &count);

		if (status < VI_SUCCESS)
		{
			printf("viRead failed with error code %x \n", status);
			viClose(defaultRM);
			//exit (EXIT_FAILURE);
			return FALSE;
		}

		m_strOutput[0] = '\0';
		do
		{
			status = viRead(instr, (ViBuf)m_strOutput, 1024, &count);

			TRACE("\t*OPC? = %c\n", m_strOutput[0]);
		} while (m_strOutput[0] != '1');
	}

	status = viClose(instr);
	status = viClose(defaultRM);

	return TRUE;
}

bool LNA::getParam()
{
	static	ViSession defaultRM, instr;
	static	ViStatus status;
	static	ViUInt32 count;
	static	ViUInt16 portNo;
	int		num;

	status = viOpenDefaultRM(&defaultRM);
	if (status < VI_SUCCESS)
	{
		printf("Could not open a session to the VISA Resource Manager!\n");
		//exit (EXIT_FAILURE);
		return FALSE;
	}

	status = viOpen(defaultRM, m_strAddress, VI_NULL, VI_NULL, &instr);
	if (status < VI_SUCCESS)
	{
		printf("An error occurred opening the session to %s\n", m_strAddress);
		viClose(defaultRM);
		//exit (EXIT_FAILURE);
		return FALSE;
	}

	num = 2 * m_csCommand.GetLength();
	status = viWrite(instr, (ViBuf)m_csCommand.GetBuffer(1), num, &count);
	if (status < VI_SUCCESS)
	{
		printf("viRead failed with error code %x \n", status);
		viClose(defaultRM);
		//exit (EXIT_FAILURE);
		return FALSE;
	}
	status = viRead(instr, (ViBuf)m_strOutput, 1024, &count);
	num = count;
	m_strOutput[num] = '\0';
	status = viClose(instr);
	status = viClose(defaultRM);

	return TRUE;
}


/************************************************************************/
/* DM                                                                     */
/************************************************************************/


DM::DM()
{
	setAddress("TCPIP0::192.168.188.81::inst0::INSTR");
}

double DM::getVoltage()
{
	m_csCommand = (LPCTSTR)("MEAS:VOLT:DC?");
	getParam();

	return (atof(m_strOutput));
}