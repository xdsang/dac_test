#ifndef MT_APP_CONFIG_H
#define MT_APP_CONFIG_H

/* need config by project */
#define MS_CHIP_ID       0xB7160A
#define MS_CHIP_ID_ADDR  0x00
#define MS_CHIP_SUBGROUP 0x00
#define MS_CHIP_NAME     "MS2160"

/* default I2C address us MS_CHIP_ADDR1 */
#define MS_CHIP_ADDR1  0x16
#define MS_CHIP_ADDR2  0xB4

#define MTOOLS_FOR_INTERNAL (1)
#define MTOOLS_MCU_VERIFY   (0)


#if MTOOLS_FOR_INTERNAL

#endif

typedef enum
{
  MS_USBIIC_SPEED_20K = 0,
  MS_USBIIC_SPEED_100K,
  MS_USBIIC_SPEED_400K,
  MS_USBIIC_SPEED_750K,
}I2C_SPEED;

typedef struct
{
  DWORD m_chip;
  BYTE  m_bConnect;
  BYTE  m_bMSID;
  BYTE  m_bIICSpeed;
}_SYS_INFO;

extern _SYS_INFO  g_SysInfo;


#endif /* MT_APP_CONFIG_H */
