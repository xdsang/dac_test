// TBComboBoxEx.cpp : implementation file
//

#include "stdafx.h"
#include "MT.h"
#include "TBComboBoxEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTBComboBoxEx
IMPLEMENT_CLONE(CTBComboBoxEx,CSYTBComboBox)

CTBComboBoxEx::CTBComboBoxEx()
{
	m_pImage = NULL;
}

CTBComboBoxEx::~CTBComboBoxEx()
{
}


/////////////////////////////////////////////////////////////////////////////
// CTBComboBoxEx message handlers

void CTBComboBoxEx::DrawItem(LPDRAWITEMSTRUCT lpDIS) 
{
	// TODO: Add your code to draw the specified item
	ASSERT( lpDIS->CtlType == ODT_COMBOBOX );

	if (lpDIS->itemID == -1 || lpDIS->itemID >= (UINT)(this->GetCount()) )
		return;

	BOOL bSelected = (lpDIS->itemState & ODS_SELECTED);
	int nImage = (int) lpDIS->itemData;
	int nIndex = lpDIS->itemID;
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);
	CRect rcItem = CRect(lpDIS->rcItem);
	CRect rcImage = rcItem; rcImage.right = rcImage.left + rcImage.Height();
	CRect rcText = rcItem; rcText.left = rcImage.right + 4;


	COLORREF crBk = GetSysColor(bSelected ? COLOR_HIGHLIGHT : COLOR_WINDOW);
	COLORREF crText;
	if (lpDIS->itemState & ODS_COMBOBOXEDIT)
	{
		BOOL bGrayed = ((m_dwState & SYITEM_STATE_GRAYED) && !CSYGUIMgr::S_IsCustomizeMode());
		crText = bGrayed ? g_pDrawMgr->m_crGrayed : m_crText;
	}
	else if (bSelected)
		crText = GetSysColor(COLOR_HIGHLIGHTTEXT);
	else 
		crText = m_crText;

	COLORREF crOldText = pDC->SetTextColor(crText);

	CString strText;
	GetLBText(nIndex,strText);
	if (!(lpDIS->itemState & ODS_COMBOBOXEDIT))
	{
		if (nImage >= 0 && m_pImage != NULL)
			m_pImage->Draw(pDC,nImage,rcImage.TopLeft(),ILD_TRANSPARENT);
		else if (nImage == -2)
			rcText = rcItem;

		pDC->FillSolidRect(rcText,crBk);
		CRect rcText1 = rcText;
		rcText1.left += 4;
		pDC->DrawText(strText,rcText1,DT_LEFT|DT_VCENTER|DT_NOCLIP|DT_SINGLELINE);
	}
	else
	{
		int nOldMode = pDC->SetBkMode(TRANSPARENT);
		CFont* pOldFont = pDC->SelectObject(m_pFont);
		
		rcText = rcItem;
		rcText.DeflateRect(2,0);
		pDC->DrawText(m_strSel,rcText,DT_LEFT|DT_VCENTER|DT_SINGLELINE);

		pDC->SelectObject(pOldFont);
		pDC->SetBkMode(nOldMode);
	}

	pDC->SetTextColor( crOldText );

	if (lpDIS->itemState & ODS_FOCUS)
		pDC->DrawFocusRect(rcText);	
}

void CTBComboBoxEx::Copy(CSYItemObj *pDest)
{
	CSYTBComboBox::Copy(pDest);

	CTBComboBoxEx* pCB = (CTBComboBoxEx*)pDest;

	pCB->m_pImage = m_pImage;
}
