// PwInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MT.h"
#include "PwInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPwInputDlg dialog


CPwInputDlg::CPwInputDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CPwInputDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CPwInputDlg)
    m_csValue = _T("0");
    //}}AFX_DATA_INIT
}


void CPwInputDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CPwInputDlg)
    DDX_Text(pDX, IDC_INPUT_VALUE, m_csValue);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPwInputDlg, CDialog)
    //{{AFX_MSG_MAP(CPwInputDlg)
    ON_WM_CTLCOLOR()
    ON_EN_CHANGE(IDC_INPUT_VALUE, OnChangeInputValue)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPwInputDlg message handlers

HBRUSH CPwInputDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
    HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    
    // TODO: Change any attributes of the DC here
    
    // TODO: Return a different brush if the default is not desired
    return hbr;
}

BOOL CPwInputDlg::OnInitDialog() 
{
    CDialog::OnInitDialog();
    
    // TODO: Add extra initialization here
    m_csValue = "";

    UpdateData(FALSE);
    
    CEdit *pEdit = (CEdit*)GetDlgItem(IDC_INPUT_VALUE);
    if(pEdit)
    {
        int i = m_csValue.GetLength();
        pEdit->SetFocus();
        for( ; i > 0; i--)
            pEdit->SendMessage(WM_KEYDOWN, WPARAM(VK_RIGHT), 0);
    }

    return FALSE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

void CPwInputDlg::OnChangeInputValue() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialog::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    UpdateData(TRUE);
}

void CPwInputDlg::OnOK() 
{
    // TODO: Add extra validation here
    if(checkValue())
        CDialog::OnOK();
}

BOOL CPwInputDlg::checkValue()
{
    BOOL bResult = TRUE;

    return bResult;
}


int CPwInputDlg::DoModal() 
{
    // TODO: Add your specialized code here and/or call the base class
    return CDialog::DoModal();
}
