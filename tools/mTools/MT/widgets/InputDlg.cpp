// InputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MT.h"
#include "InputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputDlg dialog


CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CInputDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CInputDlg)
    m_csValue = _T("0");
    //}}AFX_DATA_INIT
    m_nVal = 0;
    m_nMin = 0;
    m_nMax = 0;
}


void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CInputDlg)
    DDX_Text(pDX, IDC_INPUT_VALUE, m_csValue);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInputDlg, CDialog)
    //{{AFX_MSG_MAP(CInputDlg)
    ON_WM_CTLCOLOR()
    ON_EN_CHANGE(IDC_INPUT_VALUE, OnChangeInputValue)
    ON_BN_CLICKED(IDC_INPUT_DEC, OnInputDec)
    ON_BN_CLICKED(IDC_INPUT_HEX, OnInputHex)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputDlg message handlers

HBRUSH CInputDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
    HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    
    // TODO: Change any attributes of the DC here
    
    // TODO: Return a different brush if the default is not desired
    return hbr;
}

BOOL CInputDlg::OnInitDialog() 
{
    CDialog::OnInitDialog();
    
    // TODO: Add extra initialization here
    ((CButton*)GetDlgItem(IDC_INPUT_HEX))->SetCheck(1);

    m_bDec = FALSE;

    m_csValue.Format(_T("%lx"), m_nVal);    

    UpdateData(FALSE);
    
    CEdit *pEdit = (CEdit*)GetDlgItem(IDC_INPUT_VALUE);
    if(pEdit)
    {
        int i = m_csValue.GetLength();
        pEdit->SetFocus();
        for( ; i > 0; i--)
            pEdit->SendMessage(WM_KEYDOWN, WPARAM(VK_RIGHT), 0);
    }


    return FALSE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

void CInputDlg::OnChangeInputValue() 
{
    // TODO: If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialog::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.
    
    // TODO: Add your control notification handler code here
    UpdateData(TRUE);
    if(m_csValue.IsEmpty()){
        m_nVal = 0;
    }
    else
    {        
        if(((CButton*)GetDlgItem(IDC_INPUT_DEC))->GetCheck())
          swscanf_s(m_csValue.GetString(), _T("%ld"), &m_nVal);
        else if(((CButton*)GetDlgItem(IDC_INPUT_HEX))->GetCheck())
          swscanf_s(m_csValue.GetString(), _T("%lx"),  &m_nVal);
    }
    
    if(m_nMin == 0)
        checkValue();
}

void CInputDlg::OnOK() 
{
    // TODO: Add extra validation here
    if(checkValue())
        CDialog::OnOK();
}

BOOL CInputDlg::checkValue()
{
    BOOL bResult = TRUE;
    CString str;
    if(m_nVal < m_nMin || m_nVal > m_nMax)
    {
        if(((CButton*)GetDlgItem(IDC_INPUT_DEC))->GetCheck())
          str.Format(_T("The value should range from %ld to %ld"), m_nMin, m_nMax);
        else if(((CButton*)GetDlgItem(IDC_INPUT_HEX))->GetCheck())
          str.Format(_T("The value should range from 0x%lX to 0x%lX"), m_nMin, m_nMax);        
        AfxMessageBox(str, MB_OK | MB_ICONWARNING);

        bResult = FALSE;
    }

    return bResult;
}

void CInputDlg::OnInputDec() 
{
    // TODO: Add your control notification handler code here
    if(m_bDec == FALSE)
    {
      m_csValue.Format(_T("%ld"), m_nVal);
        UpdateData(false);
    }
    m_bDec = TRUE;
}

void CInputDlg::OnInputHex() 
{
    // TODO: Add your control notification handler code here
    if(m_bDec == TRUE)
    {
      m_csValue.Format(_T("%lX"), m_nVal);
        UpdateData(false);
    }
    m_bDec = FALSE;
}

int CInputDlg::DoModal() 
{
    // TODO: Add your specialized code here and/or call the base class
    return CDialog::DoModal();
}
