// HexEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "HexEditCtrl.h"

// CHexEditCtrl
IMPLEMENT_DYNAMIC(CHexEditCtrl, CEdit)

CHexEditCtrl::CHexEditCtrl()
{
}

CHexEditCtrl::~CHexEditCtrl()
{
}

BEGIN_MESSAGE_MAP(CHexEditCtrl, CEdit)
END_MESSAGE_MAP()

// CHexEditCtrl message handlers

LRESULT CHexEditCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class

	//??????
	//?? ?? 0-F Backspace
	if(message == WM_CHAR)
	{
		CHAR szInput[2] = {(CHAR)wParam, 0};
		_strupr_s(szInput);
		CString szValidItem=_T(" 0123456789ABCDEF\b"); 
		if(szValidItem.Find(CString(szInput)) == -1)
		{
			MessageBeep(-1);//???
			return 0;
		}
		wParam = szInput[0];
	}

	if(WM_INPUTLANGCHANGEREQUEST == message) //???????   
	{  
		return 0;  //?????  
		//m_Saved_KbdLayout = 0; //?????????????   
	}  

	return CEdit::WindowProc(message, wParam, lParam);
}


