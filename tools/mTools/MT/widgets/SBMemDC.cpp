/////////////////////////////////////////////////////////////////////////////
//
//  MemDC.cpp : implementation file
//
//  Copyright (c) 2000-2003 Barefoot Productions, Inc.
//  All Rights Reserved.
//
//  Author: Don Metzler
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "resource.h"
#include "SBMemDC.h"

/////////////////////////////////////////////////////////////////////////////
// CMemDC class

CSBMemDC::CSBMemDC()
{
    m_ValidFlag = FALSE;
    m_OldBmp = NULL;
}

CSBMemDC::~CSBMemDC()
{
    // Automatically release and draw on exit
    Release() ;
}

/////////////////////////////////////////////////////////////////////////////
//
//  Create: Creates the memory DC and image bitmap
//
/////////////////////////////////////////////////////////////////////////////

void CSBMemDC::Create(CDC* inDC, CRect inSrcRect)
{
    ASSERT(inDC != NULL) ;

    if (!m_ValidFlag)
    {
        // create the memory DC
        CreateCompatibleDC(inDC) ;

        // save the source DC
        m_SrcDC = inDC ;

        // keep track of the destination rectangle
        m_SrcRect.CopyRect(inSrcRect) ;

        // create a bitmap for the memory bitmap image
        m_MemBmp.CreateCompatibleBitmap(inDC, inSrcRect.Width(), inSrcRect.Height()) ;

        // select the memory image into the memory DC
        m_OldBmp = SelectObject(&m_MemBmp) ;

        m_ValidFlag = TRUE ;
    }
}

/////////////////////////////////////////////////////////////////////////////
//
//  Release: Releases the memory DC and image bitmap and optionally copies the image
//
/////////////////////////////////////////////////////////////////////////////

void CSBMemDC::Release(BOOL inCopyToSourceFlag)
{
    // copy the offscreen buffer to the sourceDC passed in Create()

    if (m_ValidFlag)
    {
        // blit to source DC to the m_SrcRect
        if ((inCopyToSourceFlag) && (m_SrcDC != NULL))
            m_SrcDC->BitBlt(m_SrcRect.left, m_SrcRect.top, m_SrcRect.Width(), m_SrcRect.Height(), this, 0, 0, SRCCOPY) ;

        // de-select the memory image from the DC
        SelectObject(m_OldBmp) ;

        // delete the memory bitmap image
        m_MemBmp.DeleteObject() ;

        // delete the memory DC
        DeleteDC() ;

        m_ValidFlag = FALSE ;
        m_OldBmp = NULL ;
    }
}

/////////////////////////////////////////////////////////////////////////////
//
//  CopySourceImage: Copies the source image from the m_SrcRect into the memory DC/image.
//
/////////////////////////////////////////////////////////////////////////////

void CSBMemDC::CopySourceImage()
{
    if (m_ValidFlag)
    {
        // copy the image from the source rectangle to the offscreen image
        if (m_SrcDC != NULL)
            this->BitBlt(0, 0, m_SrcRect.Width(), m_SrcRect.Height(), m_SrcDC, m_SrcRect.left, m_SrcRect.top, SRCCOPY) ;
    }
}

/////////////////////////////////////////////////////////////////////////////
//
//  ConvertRect: Converts a rectangle based on the source coordinates 
//               to one based on the memory image's coordinates.
//
/////////////////////////////////////////////////////////////////////////////

CRect CSBMemDC::ConvertRect(CRect inDrawRect)
{
    CRect   theRect ;

    theRect.CopyRect(inDrawRect) ;
    theRect.OffsetRect(-m_SrcRect.left, -m_SrcRect.top) ;

    return (theRect) ;
}

/////////////////////////////////////////////////////////////////////////////
//
//  ConvertPoint: Converts a point based on the source coordinates to 
//                one based on the memory image's coordinates.
//
/////////////////////////////////////////////////////////////////////////////

CPoint CSBMemDC::ConvertPoint(CPoint inDrawPoint)
{
    CPoint  thePoint ;

    // Convert the point relative to this DC
    thePoint = inDrawPoint ;
    thePoint.x -= m_SrcRect.left ;
    thePoint.y -= m_SrcRect.top ;

    return (thePoint) ;
}

/////////////////////////////////////////////////////////////////////////////
//
//  SetRect: Sets the drawing/output bounding rectangle.
//
/////////////////////////////////////////////////////////////////////////////

void CSBMemDC::SetRect(CRect inRect)
{
    m_SrcRect = inRect ;
}

