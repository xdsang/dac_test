/////////////////////////////////////////////////////////////////////////////
//
//  File: SBMemDC.h : implementation file
//
//  Copyright (c) 2000-2003 Barefoot Productions, Inc.
//  All Rights Reserved.
//
//  Author: Don Metzler
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _SBMEMDC_H_
#define _SBMEMDC_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
//
//  CMemDC: Subclassed CDC for drawing offscreen
//
/////////////////////////////////////////////////////////////////////////////

class CSBMemDC : public CDC
{
public:

    protected:

        BOOL        m_ValidFlag ;       // TRUE if we can successfully release/copy the offscreen image to the screen
        CRect       m_SrcRect ;         // Bounding rectangle for drawing
        CBitmap     m_MemBmp ;          // Offscreen bitmap image
        CBitmap*    m_OldBmp ;          // Previous bitmap in the offscreen DC
        CDC*        m_SrcDC ;           // Source device context for final blit

    public:

        void SetRect(CRect inRect) ;

        // Construction

        CSBMemDC() ;
        virtual ~CSBMemDC() ;

        // Access

        CPoint  ConvertPoint(CPoint inDrawPoint) ;
        CRect   ConvertRect(CRect inDrawRect) ;
        void    CopySourceImage() ;
        void    Create(CDC* inDC, CRect inSrcRect) ;

        void    Release(BOOL inCopyToSourceFlag = TRUE) ;

} ;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _MEMDC_H_

