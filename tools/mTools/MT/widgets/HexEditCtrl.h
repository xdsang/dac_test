#pragma once

// CHexEditCtrl

class CHexEditCtrl : public CEdit
{
	DECLARE_DYNAMIC(CHexEditCtrl)

public:
	CHexEditCtrl();
	virtual ~CHexEditCtrl();

protected:
	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

//???
protected:  
//    HKL m_DefaultHKL;  
//    HKL m_Saved_KbdLayout;  
//    HKL GetDefault_KbdLayout();  

};
