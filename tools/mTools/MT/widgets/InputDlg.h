#pragma once

class CInputDlg : public CDialog
{
// Construction
public:
    BOOL checkValue();
    CInputDlg(CWnd* pParent = NULL);   // standard constructor

    enum { IDD = IDD_INPUT };

    CString    m_csValue;
    INT64 m_nMin;
    INT64 m_nVal;
    INT64 m_nMax;
    BOOL m_bDec;

    public:
    virtual int DoModal();
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:

    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    virtual BOOL OnInitDialog();
    afx_msg void OnChangeInputValue();
    virtual void OnOK();
    afx_msg void OnInputDec();
    afx_msg void OnInputHex();
    DECLARE_MESSAGE_MAP()
};

