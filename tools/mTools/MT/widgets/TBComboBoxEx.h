
#if !defined(AFX_TBCOMBOBOXEX_H__44C957EE_931E_4FA0_92D0_3E2BC5A1E42C__INCLUDED_)
#define AFX_TBCOMBOBOXEX_H__44C957EE_931E_4FA0_92D0_3E2BC5A1E42C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TBComboBoxEx.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CTBComboBoxEx window

class CTBComboBoxEx : public CSYTBComboBox
{
	DECLARE_CLONE(CTBComboBoxEx)
// Construction
public:
	CTBComboBoxEx();

// Attributes
public:
	CImageList* m_pImage;

// Operations
public:
// Overrides
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);

// Implementation
public:
	void Copy(CSYItemObj *pDest);
	virtual ~CTBComboBoxEx();

	// Generated message map functions
protected:

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TBCOMBOBOXEX_H__44C957EE_931E_4FA0_92D0_3E2BC5A1E42C__INCLUDED_)
