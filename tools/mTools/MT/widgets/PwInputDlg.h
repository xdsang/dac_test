#pragma once

/////////////////////////////////////////////////////////////////////////////
// CPwInputDlg dialog

class CPwInputDlg : public CDialog
{
// Construction
public:
    BOOL checkValue();
    CPwInputDlg(CWnd* pParent = NULL);   // standard constructor


    enum { IDD = IDD_PW };


    CString    m_csValue;

    public:
    virtual int DoModal();
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
protected:

    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    virtual BOOL OnInitDialog();
    afx_msg void OnChangeInputValue();
    virtual void OnOK();

    DECLARE_MESSAGE_MAP()
};

