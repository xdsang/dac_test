/**
******************************************************************************
* @file    ms928x_drv_hdmi_tx.c
* @author  
* @version V1.0.0
* @date    31-May-2016
* @brief   hdmi tx module driver source file
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#if MS2160_FPGA_VERIFY

#include "common.h"
#include "ms928x_adc.h"
#include "ms928x_drv_hdmi_tx.h"


#if MS928X_HDMI_TX_ENABLE

#define HDMI_DDC_set_speed      mculib_i2c_set_speed
#define HDMI_DDC_ReadByte       mculib_i2c_read_8bidx8bval
#define HDMI_DDC_WriteByte      mculib_i2c_write_8bidx8bval
#define HDMI_DDC_ReadBytes      mculib_i2c_burstread_8bidx8bval


//hdmi tx module power on/off
//b_enable: if true turn on, else turn off
VOID ms928xdrv_hdmi_tx_phy_power_enable(BOOL b_enable)
{
    MS928X_ToggleBits(MS928X_HDMI_TX_PHY_PDZ_REG, MSRT_BIT4 | MSRT_BIT0, b_enable);
    MS928X_ToggleBits(MS928X_HDMI_TX_PHY_BGTX_PDZ_REG, MSRT_BIT4 | MSRT_BIT0, b_enable);
}

VOID ms928xdrv_hdmi_tx_phy_output_enable(BOOL b_enable)
{
#if (MS9281C_CHIP == MS928X_CHIP_TYPE)
    MS928X_ToggleBits(MS928X_HDMI_TX_PHY_BGTX_PDZ_REG, MSRT_BIT1, b_enable);
#else //MS9281b chip issue
    if (b_enable)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_PHY_DMPC_REG, 0x03);
        MS928X_SetBits(MS928X_HDMI_TX_SHELL_DOUT_SEL_REG, MSRT_BITS1_0);
    }
    else
    {
        MS928X_ClrBits(MS928X_HDMI_TX_PHY_DMPC_REG, MSRT_BITS2_0);
        MS928X_ClrBits(MS928X_HDMI_TX_SHELL_DOUT_SEL_REG, MSRT_BITS3_0);
    }
#endif
}

//hdmi tx phy module init
VOID ms928xdrv_hdmi_tx_phy_init(VOID)
{
    //MS928X_WriteByte(MS928X_HDMI_TX_PHY_PDZ_REG, 0x00); //
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_KS_REG, 0x12);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_CKEN_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_N2_REG, 0x05);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_M0_REG, 0x73);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_M8_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_CKIS_REG, 0x0f); //only need program when MS9281C
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_ICPI_REG, 0x05);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_FN0_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_FN8_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_FN16_REG, 0x20);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_TRIG_REG, 0x01); //only need program when MS9281C
    //MS928X_WriteByte(MS928X_HDMI_TX_PHY_BGTX_PDZ_REG, 0x00); //
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_DCC_REG, 0x27);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_MDRV0_REG, 0x02);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_MDRV1_REG, 0x32);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_DMPC_REG, 0x03);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_PCD1_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_PCD2_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_PCPD0_REG, 0x00);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_PCPD1_REG, 0x55);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_PCPD2_REG, 0x05);   
}

BOOL ms928xdrv_hdmi_tx_phy_clk_set_divide(VOID)
{
    UINT8 ks = MS928X_ReadByte(MS928X_HDMI_TX_PHY_KS_REG) & MSRT_BITS1_0;
    if (ks >= 3)
    {
        MS928X_LOG("pll clk divide error.");
        return FALSE;
    }
    
    MS928X_ModBits(MS928X_HDMI_TX_PHY_KS_REG, MSRT_BITS1_0, ks + 1);   // KS[1:0]

    return TRUE;
}

//hdmi tx phy module set video clk
//u16_clk: uint 10000Hz
VOID ms928xdrv_hdmi_tx_phy_set_clk(UINT16 u16_clk)
{
    UINT8  i;
    UINT16 u16val;
    UINT16 u16X = 0;
    UINT16 u16Y = 0;
    BOOL   bFound = FALSE;  

    // 
    //Calculate the N2[7:0]
    u16val = u16_clk / 600 - 1;

    MS928X_WriteByte(MS928X_HDMI_TX_PHY_N2_REG, (UINT8)u16val);

    u16X = u16val;
    // Calculate the PLLV vco.
    u16val = u16_clk;
    for (i = 0; i < 4; i ++)
    {
        // Change from 1400 -> 1200
        if ((u16val >= 18000) && (u16val <= 24000))  // 10K as a unit.
        {
            MS928X_ModBits(MS928X_HDMI_TX_PHY_KS_REG, MSRT_BITS1_0, i);   // KS[1:0]
            MS928X_ClrBits(MS928X_HDMI_TX_PHY_KS_REG, MSRT_BIT4);         // "fs = 0"
            u16Y = 1 << i;
            bFound = TRUE;
            break;
        }
        u16val = u16val << 1;
    }

    if (!bFound)
    {
        bFound = FALSE;
        u16val = u16_clk;
        for (i = 0; i < 4; i ++)
        {
            if ((u16val >= 12000) && (u16val <= 18000))
            {
                MS928X_ModBits(MS928X_HDMI_TX_PHY_KS_REG, MSRT_BITS1_0, i);
                MS928X_SetBits(MS928X_HDMI_TX_PHY_KS_REG, MSRT_BIT4);
                u16Y = 1 << i;
                bFound = TRUE;
                break;
            }
            u16val = u16val << 1;
        }
    }
    // Calculate the pllv m[11:0]
    if (bFound)
    {
        u16val = (u16X + 1) * u16Y * 5 - 5;
        MS928X_WriteWord(MS928X_HDMI_TX_PHY_M0_REG, u16val);
    }
}

//hdmi tx phy module trigger
VOID ms928xdrv_hdmi_tx_phy_trigger(VOID)
{
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_CKEN_REG, 0);                // CKO7EN=0.
    MS928X_ClrBits(MS928X_HDMI_TX_PHY_DCC_REG, MSRT_BIT5);           // REG_DCCEN1= 0
    MS928X_ClrBits(MS928X_HDMI_TX_PHY_CKINV_REG, MSRT_BIT3);
    MS928X_SetBits(MS928X_HDMI_TX_PHY_CKINV_REG, MSRT_BIT3);
    MS928X_WriteByte(MS928X_HDMI_TX_PHY_CKEN_REG, MSRT_BIT0);        // CKO7EN=1.
    MS928X_SetBits(MS928X_HDMI_TX_PHY_DCC_REG, MSRT_BIT5);           // REG_DCCEN1= 1
}

VOID ms928xdrv_hdmi_tx_shell_video_mute_enable(BOOL b_enable)
{
    MS928X_ToggleBits(MS928X_HDMI_TX_SHELL_AVMUTE_REG, MSRT_BIT1, b_enable);
}

VOID ms928xdrv_hdmi_tx_shell_audio_mute_enable(BOOL b_enable)
{
    MS928X_ToggleBits(MS928X_HDMI_TX_SHELL_AVMUTE_REG, MSRT_BIT2, b_enable);
}

//hdmi tx shell module set hdmi out
//b_hdmi_out: FALSE = dvi out;  TRUE = hdmi out��
VOID ms928xdrv_hdmi_tx_shell_set_hdmi_out(BOOL b_hdmi_out)
{
    MS928X_ToggleBits(MS928X_HDMI_TX_SHELL_DVI_REG, MSRT_BIT0, !b_hdmi_out);
}

//hdmi tx shell module set out clk repeat times, u8_times: enum refer to MS928X_HDMI_CLK_RPT_E
VOID ms928xdrv_hdmi_tx_shell_set_clk_repeat(UINT8 u8_times)
{
    MS928X_ModBits(MS928X_HDMI_TX_SHELL_MODE_REG, MSRT_BITS5_4, u8_times << 4);
}

//set hdmi shell color space
VOID ms928xdrv_hdmi_tx_shell_set_color_space(UINT8 u8_out_cs)
{
    MS928X_ModBits(MS928X_HDMI_TX_SHELL_MODE_REG, MSRT_BITS7_6, u8_out_cs << 6);
}

//hdmi tx shell module video color depth. 10bits, 12bits, 16bits no support in ms928x
//u8_depth: enum refer to MS928X_HDMI_COLOR_DEPTH_E
VOID ms928xdrv_hdmi_tx_shell_set_color_depth(UINT8 u8_depth)
{
    MS928X_ToggleBits(MS928X_HDMI_TX_SHELL_MODE_REG, MSRT_BIT2, u8_depth > 0);
    MS928X_ModBits(MS928X_HDMI_TX_SHELL_MODE_REG, MSRT_BITS1_0, u8_depth);
}

#define ms928xdrv_hdmi_tx_misc_set_colorimetry(u8_colorimetry)  MS928X_ToggleBits(MS928X_HDMI_TX_MISC_SRC_CTRL_REG, MSRT_BIT2, u8_colorimetry == MS928X_HDMI_COLORIMETRY_709)

//hdmi tx shell module set audio mode. HBR, DSD mode no support in ms928x
//u8_audio_mode: enum refer to MS928X_HDMI_AUDIO_MODE_E
VOID ms928xdrv_hdmi_tx_shell_set_audio_mode(UINT8 u8_audio_mode)
{
    MS928X_SetBits(MS928X_HDMI_TX_MISC_HDMI_CLK_REG, MSRT_BIT0);  // HDMI_ACK_E
    // 20140918 set audio enable
    MS928X_SetBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BIT7);
    
    switch (u8_audio_mode)
    {
    case MS928X_HDMI_AUD_MODE_I2S:
        MS928X_ClrBits(MS928X_HDMI_TX_MISC_HDMI_CLK_REG, MSRT_BIT4);  // I2S clk
        MS928X_ClrBits(MS928X_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);      // Audio data selection.
        break;
    case MS928X_HDMI_AUD_MODE_SPDIF:
        MS928X_SetBits(MS928X_HDMI_TX_MISC_HDMI_CLK_REG, MSRT_BIT4);  // SPDIF clk
        MS928X_WriteByte(MS928X_HDMI_TX_MISC_AUDIO_SPDIF_REV0_REG, MSRT_BIT2);    // 
        MS928X_SetBits(MS928X_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);      // SPDIF    
        break;
    case MS928X_HDMI_AUD_MODE_HBR:
    case MS928X_HDMI_AUD_MODE_DSD:
    default:
        break;
    }
}

//hdmi tx shell module set audio I2S rate
//u8_audio_rate: enum refer to MS928X_HDMI_AUDIO_RATE_E
VOID ms928xdrv_hdmi_tx_shell_set_audio_rate(UINT8 u8_audio_rate)
{
    switch (u8_audio_rate)
    {
    case MS928X_HDMI_AUD_RATE_48K:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT2);
        break;
        
    case MS928X_HDMI_AUD_RATE_44K1:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT5);
        break;
        
    case MS928X_HDMI_AUD_RATE_96K:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT1);
        break;
        
    case MS928X_HDMI_AUD_RATE_32K:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT6);
        break;
        
    case MS928X_HDMI_AUD_RATE_88K2:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT4);
        break;
        
    case MS928X_HDMI_AUD_RATE_176K4: 
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT3);
        break;
        
    case MS928X_HDMI_AUD_RATE_192K:
        MS928X_ModBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT0);
        break;        
        
    default:
        break; 
    }
}

//hdmi tx shell module set audio bit length
//u8_audio_bits: enum refer to MS928X_HDMI_AUDIO_LENGTH_E
VOID ms928xdrv_hdmi_tx_shell_set_audio_bits(UINT8 u8_audio_bits)
{
    MS928X_ModBits(MS928X_HDMI_TX_AUDIO_CFG_I2S_REG, MSRT_BITS5_4, u8_audio_bits ? 0x00 : MSRT_BIT4);
}

//hdmi tx shell module set audio channels
//u8_audio_channels: enum refer to MS928X_HDMI_AUDIO_CHN_E
VOID ms928xdrv_hdmi_tx_shell_set_audio_channels(UINT8 u8_audio_channels)
{
    if (u8_audio_channels <= MS928X_HDMI_AUD_2CH)
    {
        u8_audio_channels = 0x01;
    }
    else if (u8_audio_channels <= MS928X_HDMI_AUD_4CH)
    {
        u8_audio_channels = 0x03;
    }
    else if (u8_audio_channels <= MS928X_HDMI_AUD_6CH)
    {
        u8_audio_channels = 0x07;
    }
    else
    {
        u8_audio_channels = 0x0f;
    }
    
    MS928X_ModBits(MS928X_HDMI_TX_AUDIO_CH_EN_REG, MSRT_BITS3_0, u8_audio_channels);
}

//hdmi tx shell module set audio lrck
//b_left: if true left channel in, else right channel in
VOID ms928xdrv_hdmi_tx_shell_set_audio_lrck(BOOL b_left)
{
    MS928X_ModBits(MS928X_HDMI_TX_AUDIO_CFG_I2S_REG, MSRT_BITS1_0, b_left ? MSRT_BIT1 : 0x00);
}

static UINT8 g_arrInfoFrameBuffer[0x0E];

VOID ms928xdrv_hdmi_tx_shell_set_video_infoframe(MS928X_HDMITX_CONFIG_T *pt_hdmi_tx)
{
    UINT8 i;
    UINT8 u8tmp = 0;

    MS928X_ClrBits(MS928X_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT6); // "AVI packet is enabled, active is high"
    memset(g_arrInfoFrameBuffer, 0, 0x0E);

    u8tmp = pt_hdmi_tx->u8_color_space;
    g_arrInfoFrameBuffer[1] = (u8tmp << 5) & MSRT_BITS6_5; 

    g_arrInfoFrameBuffer[1] |= 0x10;     //Active Format Information Present, Active
    
    // 2013-10-18, Change the SCAN INFO @ CKL's request. 
    // it should Overscan in a CE format and underscan in a IT format.
    g_arrInfoFrameBuffer[1] |= (pt_hdmi_tx->u8_scan_info & 0x03);

    if (pt_hdmi_tx->u8_colorimetry == MS928X_HDMI_COLORIMETRY_601)
    {
        g_arrInfoFrameBuffer[2] = MSRT_BIT6;
    }
    else if ((pt_hdmi_tx->u8_colorimetry == MS928X_HDMI_COLORIMETRY_709) || (pt_hdmi_tx->u8_colorimetry == MS928X_HDMI_COLORIMETRY_1120))
    {
        g_arrInfoFrameBuffer[2] = MSRT_BIT7;
    }
    else if (pt_hdmi_tx->u8_colorimetry == MS928X_HDMI_COLORIMETRY_XVYCC601)
    {
        g_arrInfoFrameBuffer[2] = MSRT_BITS7_6;
        g_arrInfoFrameBuffer[3] &= ~MSRT_BITS6_4;
    }
    else if (pt_hdmi_tx->u8_colorimetry == MS928X_HDMI_COLORIMETRY_XVYCC709)
    {
        g_arrInfoFrameBuffer[2] = MSRT_BITS7_6;
        g_arrInfoFrameBuffer[3] = MSRT_BIT4;
    }
    else
    {
        g_arrInfoFrameBuffer[2] &= ~MSRT_BITS7_6;
        g_arrInfoFrameBuffer[3] &= ~MSRT_BITS6_4; 
    }

    //others set to 0
    if (pt_hdmi_tx->u8_aspect_ratio == MS928X_HDMI_4X3)
    {
        g_arrInfoFrameBuffer[2] |= 0x10;
    }
    else if (pt_hdmi_tx->u8_aspect_ratio == MS928X_HDMI_16X9)
    {
        g_arrInfoFrameBuffer[2] |= 0x20;
    }

    //R3...R0, default to 0x08
    g_arrInfoFrameBuffer[2] |= 0x08; // Same as coded frame aspect ratio.

    // According to HDMI1.3 spec. VESA mode should be set to 0
    if (pt_hdmi_tx->u8_vic >= 64) //VESA
    {
        g_arrInfoFrameBuffer[4] = 0;    // 0 for vesa modes.
    }
    else
    {
        g_arrInfoFrameBuffer[4] = pt_hdmi_tx->u8_vic;
    }
    g_arrInfoFrameBuffer[5] = pt_hdmi_tx->u8_clk_rpt;

    //Calculate the check-sum
    g_arrInfoFrameBuffer[0] = 0x82 + 0x02 + 0x0D;    
    for (i = 1; i < 0x0E; i ++)
    {
        g_arrInfoFrameBuffer[0] += g_arrInfoFrameBuffer[i];
    }
    g_arrInfoFrameBuffer[0] = 0x100 - g_arrInfoFrameBuffer[0];
    // Write data into hdmi shell.
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_TYPE_REG, 0x82);
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_VER_REG, 0x02);
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_LEN_REG, 0x0D);

    for (i = 0; i < 0x0E; i ++)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_PACK_REG, g_arrInfoFrameBuffer[i]);
    }
    MS928X_SetBits(MS928X_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT6);
}

VOID ms928xdrv_hdmi_tx_shell_set_audio_infoframe(MS928X_HDMITX_CONFIG_T *pt_hdmi_tx)
{
    UINT8 i;
    // 
    MS928X_ClrBits(MS928X_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT5);
    
    memset(g_arrInfoFrameBuffer, 0, 0x0E);
    g_arrInfoFrameBuffer[0] = 0x84;
    g_arrInfoFrameBuffer[1] = 0x01;
    g_arrInfoFrameBuffer[2] = 0x0A;
    g_arrInfoFrameBuffer[3] = 0x84 + 0x01 + 0x0A;
    // 20140917 fixed bug for audio inforframe channel config
    g_arrInfoFrameBuffer[4] = pt_hdmi_tx->u8_audio_channels;
    g_arrInfoFrameBuffer[5] = 0;
    g_arrInfoFrameBuffer[7] = pt_hdmi_tx->u8_audio_speaker_locations;
    // Calculate the checksum
    for (i = 4; i < 0x0E; i ++)
    {
        g_arrInfoFrameBuffer[3] += g_arrInfoFrameBuffer[i];
    }
    g_arrInfoFrameBuffer[3] = 0x100 - g_arrInfoFrameBuffer[3];
    // Write packet info.
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_TYPE_REG, g_arrInfoFrameBuffer[0]);
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_VER_REG, g_arrInfoFrameBuffer[1]);
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_LEN_REG, g_arrInfoFrameBuffer[2]);
    
    for (i = 3; i < 0x0E; i ++)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INFO_PACK_REG, g_arrInfoFrameBuffer[i]);
    }
    // open the 'cfg_audio_en_o", tell the hw infoframe is ready.
    MS928X_SetBits(MS928X_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT5);
}

//hdmi tx shell module init
VOID ms928xdrv_hdmi_tx_shell_init(VOID)
{
    //video and audio clk enable, 
    //20170412, add video clk for mistable
    //MS928X_SetBits(MS928X_HDMI_TX_MISC_HDMI_CLK_REG, MSRT_BIT6 | MSRT_BIT5);
    
    //audio
    //MS928X_ClrBits(MS928X_HDMI_TX_MISC_HDMI_CTS_CLK_REG, MSRT_BITS2_1);  // CTS_SEL, register default
    //ms928xdrv_hdmi_tx_shell_set_audio_lrck(TRUE); //register default

    //video and audio release reset
    //MS928X_SetBits(MS928X_HDMI_TX_MISC_HDMI_RST_REG, MSRT_BIT2);

    //20140918 set audio enable
    //MS928X_SetBits(MS928X_HDMI_TX_AUDIO_RATE_REG, MSRT_BIT7);

    //Set interrupt mask reg.
    MS928X_WriteByte(MS928X_HDMI_TX_SHELL_INT_MASK_REG, 0x98);
}

//hdmi tx shell module reset
//b_en, if true reset hdmi video and audio
VOID ms928xdrv_hdmi_tx_shell_reset_enable(BOOL b_en)
{
    //video and audio reset
    MS928X_ToggleBits(MS928X_HDMI_TX_MISC_HDMI_RST_REG, MSRT_BITS1_0, !b_en);
}

VOID ms928xdrv_hdmi_tx_dvin_init(VOID)
{
    //top misc
    MS928X_WriteByte(0x03, 0x06);
    MS928X_WriteByte(0x04, 0x0B);
    MS928X_WriteByte(0x0B, 0x02);

      
}

VOID ms928xdrv_hdmi_tx_dvin_clk_phase_config(BOOL b_bypass, UINT8 u8_phase)
{   
    //sw_rstb_phase relase
    MS928X_SetBits(0x08, MSRT_BIT3);
    //if use PA module need config VADC bg power
    MS928X_WriteByte(0x27B, 0x1F);
    //PA power enable, ICP
    MS928X_SetBits(0x295, MSRT_BIT0); 
    MS928X_ModBits(0x295, MSRT_BITS7_4, 3 << 4);

    //
    MS928X_ToggleBits(0x295, MSRT_BIT1, b_bypass);
    if (!b_bypass)
    {
        MS928X_WriteByte(0x296, u8_phase);
    }
}

VOID ms928xdrv_hdmi_tx_dvin_config(UINT8 u8_dvin_sync_type, UINT8 u8_dvin_data_type, UINT32 u32_flag)
{
    switch(u8_dvin_sync_type)
    {
    case MS928X_DVIN_PORT_SYNC_TYPE_HSVS:
    case MS928X_DVIN_PORT_SYNC_TYPE_HSVSDE:
    case MS928X_DVIN_PORT_SYNC_TYPE_VSDE:
    case MS928X_DVIN_PORT_ITU_SDR_BT601:
        MS928X_ModBits(MS928X_VP_DGIN_CFG_REG, MSRT_BITS3_1, 0x00);
        break;

    case MS928X_DVIN_PORT_SYNC_TYPE_EMBEDED:
    case MS928X_DVIN_PORT_ITU_SDR_BT656:
        MS928X_ModBits(MS928X_VP_DGIN_CFG_REG, MSRT_BITS3_1, MSRT_BIT2);
        break;

    case MS928X_DVIN_PORT_ITU_SDR_BT1120:
        MS928X_ModBits(MS928X_VP_DGIN_CFG_REG, MSRT_BITS3_1, MSRT_BITS3_2);
        break;

    case MS928X_DVIN_PORT_ITU_DDR:
        MS928X_ModBits(MS928X_VP_DGIN_CFG_REG, MSRT_BITS3_1, MSRT_BITS2_1);
        break;
    }

    //dgin_mode
    //according to design, RGB and YUV444input, same set register dgin_mode to 0
    if (u8_dvin_data_type == MS928X_DVIN_24BIT_RGB) u8_dvin_data_type = MS928X_DVIN_24BIT_YUV444;
    MS928X_ModBits(MS928X_VP_DGIN_CFG_REG, MSRT_BITS6_4, (u8_dvin_data_type - 1) << 4);

    //data_sel
    MS928X_ModBits(MS928X_VP_DGIN_SWAP_REG, MSRT_BITS2_0, u32_flag);

    //data_swap
    MS928X_ToggleBits(MS928X_VP_DGIN_SWAP_REG, MSRT_BIT3, u32_flag & 0x0008);

    //dgin_ddr_edge_inv  
    MS928X_ToggleBits(MS928X_VP_DDR_CTL_REG, MSRT_BIT0, u32_flag & 0x0010);

    //dgin_ddr_yc_swap  
    MS928X_ToggleBits(MS928X_VP_DDR_CTL_REG, MSRT_BIT1, u32_flag & 0x0020);

    //yc_swap
    MS928X_ToggleBits(MS928X_VP_CSC_SEL_REG, MSRT_BIT0, u32_flag & 0x0100);

    //uv_flip
    MS928X_ToggleBits(MS928X_VP_CSC_SEL_REG, MSRT_BIT3, u32_flag & 0x0200);

    //pa set
    ms928xdrv_hdmi_tx_dvin_clk_phase_config(!(u32_flag & 0x0800), u32_flag >> 12);
}

//dvin color space conversion, u8_out_cs refer to MS928X_HDMI_CS_E
VOID ms928xdrv_hdmi_tx_dvin_csc(UINT8 u8_dvin_data_type, UINT32 u32_flag, UINT8 u8_out_cs)
{
    //input color space 
    MS928X_ToggleBits(MS928X_VP_INOUTSEL_REG, MSRT_BIT1, u8_dvin_data_type == MS928X_DVIN_24BIT_RGB);

    //output color space
    if (u8_out_cs == MS928X_HDMI_RGB) 
    {
        MS928X_ModBits(MS928X_VP_INOUTSEL_REG, MSRT_BITS3_2, MSRT_BIT2);   // "sel_rgb_out=1"
    }
    else if (u8_out_cs == MS928X_HDMI_YCBCR444 || u8_out_cs == MS928X_HDMI_XVYCC444)
    {
        MS928X_ClrBits(MS928X_VP_INOUTSEL_REG, MSRT_BITS3_2);              // "sel_rgb_out=0"
    }
    else if (u8_out_cs == MS928X_HDMI_YCBCR422)
    {
        MS928X_ModBits(MS928X_VP_INOUTSEL_REG, MSRT_BITS3_2, MSRT_BIT3);   // "sel_422_out=1"
    }

    //yuv_range_sel  
    MS928X_ToggleBits(MS928X_VP_INOUTSEL_REG, MSRT_BIT4, u32_flag & 0x0040);

    //yuv_709_sel  
    MS928X_ToggleBits(MS928X_VP_INOUTSEL_REG, MSRT_BIT5, u32_flag & 0x0080);
}


VOID ms928xdrv_hdmi_tx_dvin_timing_config(BOOL b_pixel_doulbe, VIDEOTIMING_T *pt_timing, UINT8 u8_clk_repeat)
{
    UINT16 u16_vde_odd_st;
    UINT16 u16_vde_odd_sp;
    UINT16 u16_vde_even_st;
    UINT16 u16_vde_even_sp;
    UINT8 u8_val;

    u8_val = b_pixel_doulbe ? 1 : 0;
    
    //VP
    MS928X_SetBits(MS928X_VP_DGIN_CFG_REG, MSRT_BIT0); //de_regen_en = 1
    MS928X_SetBits(MS928X_VP_DEREG_EN_REG, MSRT_BIT5); //hvtotal_use_auto = 1
    //waring: only support MS928X_HDMI_X1CLK, MS928X_HDMI_X2CLK;
    MS928X_ToggleBits(MS928X_VP_DEREG_EN_REG, MSRT_BIT4, u8_clk_repeat == MS928X_HDMI_X2CLK);
    
    //
    MS928X_WriteWord(MS928X_VP_HTOTAL_L_REG, pt_timing->u16_htotal << u8_val);
    MS928X_WriteWord(MS928X_VP_VTOTAL_L_REG, pt_timing->u16_vtotal);
    MS928X_WriteWord(MS928X_VP_HS_L_REG, pt_timing->u16_hsyncwidth << u8_val);
    MS928X_WriteWord(MS928X_VP_VS_L_REG, pt_timing->u16_vsyncwidth);
    MS928X_WriteWord(MS928X_VP_HDE_ST_L_REG, pt_timing->u16_hoffset << u8_val);
    MS928X_WriteWord(MS928X_VP_HDE_SP_L_REG, (pt_timing->u16_hoffset + pt_timing->u16_hactive) << u8_val);

    u16_vde_odd_st = pt_timing->u16_voffset;

    if (pt_timing->u8_polarity & MSRT_BIT0) //prog
    {
        u16_vde_odd_sp = u16_vde_odd_st + pt_timing->u16_vactive;
        u16_vde_even_st = u16_vde_odd_st;
        u16_vde_even_sp = u16_vde_odd_sp;
    }
    else
    {
        u16_vde_odd_sp = u16_vde_odd_st + pt_timing->u16_vactive / 2;
        u16_vde_even_st = u16_vde_odd_st + (pt_timing->u16_vtotal + 1) / 2;
        u16_vde_even_sp = u16_vde_even_st + pt_timing->u16_vactive / 2;
    }

    MS928X_WriteWord(MS928X_VP_VDE_O_ST_L_REG, u16_vde_odd_st);
    MS928X_WriteWord(MS928X_VP_VDE_O_SP_L_REG, u16_vde_odd_sp);
    MS928X_WriteWord(MS928X_VP_VDE_E_ST_L_REG, u16_vde_even_st);
    MS928X_WriteWord(MS928X_VP_VDE_E_SP_L_REG, u16_vde_even_sp);
}

VOID ms928xdrv_hdmi_tx_config(MS928X_HDMITX_CONFIG_T *pt_hdmi_tx)
{
    BOOL dvin_clk_double = FALSE;
    UINT16 u16_clk;
    
    dvin_clk_double = pt_hdmi_tx->u32_dvin_flag & 0x0400;
    u16_clk = pt_hdmi_tx->pt_dvin_timing.u16_pixclk;
    u16_clk = dvin_clk_double ? (u16_clk * 2): u16_clk;
    
    MS928X_SetChipAddr(0x9A);

    //dvin config
    ms928xdrv_hdmi_tx_dvin_init();
    ms928xdrv_hdmi_tx_dvin_config(pt_hdmi_tx->u8_dvin_sync_type, pt_hdmi_tx->u8_dvin_data_type, pt_hdmi_tx->u32_dvin_flag);
    ms928xdrv_hdmi_tx_dvin_csc(pt_hdmi_tx->u8_dvin_data_type, pt_hdmi_tx->u32_dvin_flag, pt_hdmi_tx->u8_color_space);
    ms928xdrv_hdmi_tx_dvin_timing_config(dvin_clk_double, &(pt_hdmi_tx->pt_dvin_timing), pt_hdmi_tx->u8_clk_rpt);
    
    //disable output
    //ms928xdrv_hdmi_tx_phy_output_enable(FALSE);

    //PHY
    ms928xdrv_hdmi_tx_phy_init();
    ms928xdrv_hdmi_tx_phy_power_enable(TRUE); //
    ms928xdrv_hdmi_tx_phy_set_clk(u16_clk);
    if (dvin_clk_double) ms928xdrv_hdmi_tx_phy_clk_set_divide();
    //delay > 100us for PLLV clk stable
    Delay_ms(10);
    ms928xdrv_hdmi_tx_phy_trigger();
    
    //SHELL
    ms928xdrv_hdmi_tx_shell_reset_enable(TRUE);
    ms928xdrv_hdmi_tx_shell_init();
    ms928xdrv_hdmi_tx_shell_set_hdmi_out(pt_hdmi_tx->u8_hdmi_out);
    ms928xdrv_hdmi_tx_shell_set_clk_repeat(pt_hdmi_tx->u8_clk_rpt);
    ms928xdrv_hdmi_tx_shell_set_color_space(pt_hdmi_tx->u8_color_space);
    ms928xdrv_hdmi_tx_shell_set_color_depth(pt_hdmi_tx->u8_color_depth);
    ms928xdrv_hdmi_tx_misc_set_colorimetry(pt_hdmi_tx->u8_colorimetry);
    //
    ms928xdrv_hdmi_tx_shell_set_audio_mode(pt_hdmi_tx->u8_audio_mode);
    ms928xdrv_hdmi_tx_shell_set_audio_rate(pt_hdmi_tx->u8_audio_rate);
    ms928xdrv_hdmi_tx_shell_set_audio_bits(pt_hdmi_tx->u8_audio_bits);
    ms928xdrv_hdmi_tx_shell_set_audio_channels(pt_hdmi_tx->u8_audio_channels);
    //
    ms928xdrv_hdmi_tx_shell_reset_enable(FALSE);
    //
    ms928xdrv_hdmi_tx_shell_set_video_infoframe(pt_hdmi_tx);
    ms928xdrv_hdmi_tx_shell_set_audio_infoframe(pt_hdmi_tx);
    
    //enable output
    //ms928xdrv_hdmi_tx_shell_video_mute_enable(FALSE);
    //ms928xdrv_hdmi_tx_shell_audio_mute_enable(FALSE);
    //ms928xdrv_hdmi_tx_phy_output_enable(TRUE);
}

VOID ms928xdrv_hdmi_tx_set_pixel_shift(INT16 i16value)
{
    if (i16value < 0)
    {
        i16value = HAL_ReadWord(MS928X_VP_HTOTAL_L_REG) + i16value;
    }

    HAL_WriteWord(MS928X_VP_PXL_SHIFT_L_REG, i16value);
}

VOID ms928xdrv_hdmi_tx_set_line_shift(INT16 i16value)
{
    if (i16value < 0)
    {
        i16value = HAL_ReadWord(MS928X_VP_VTOTAL_L_REG) + i16value;
    }

    HAL_WriteWord(MS928X_VP_LIN_SHIFT_L_REG, i16value);
}

VOID ms928xdrv_hdmi_tx_phy_power_down(VOID)
{
    ms928xdrv_hdmi_tx_phy_power_enable(FALSE);
}

BOOL ms928xdrv_hdmi_tx_shell_hpd(VOID)
{
    return (MS928X_ReadByte(MS928X_HDMI_TX_SHELL_STATUS_REG) & MSRT_BIT0);
}
 
VOID ms928xdrv_hdmi_tx_ddc_enable(BOOL b_enable)
{
    if (b_enable)
    {
        HDMI_DDC_set_speed(0); // Default to 20 kbit/s
        // Prepare the GPIO
        MS928X_WriteByte(MS928X_HDMI_TX_MISC_DDC_CTRL_REG, 0xFF);   
        MS928X_ClrBits(MS928X_HDMI_TX_MISC_DDC_ENABLE_REG, MSRT_BIT0);
    }
    else
    {
        MS928X_SetBits(MS928X_HDMI_TX_MISC_DDC_ENABLE_REG, MSRT_BIT0);
        MS928X_WriteByte(MS928X_HDMI_TX_MISC_DDC_CTRL_REG, 0x11);
        HDMI_DDC_set_speed(1); // Default to 100 kbit/s
    }
}

//
#if 1

#define HDCP_RX_SLAVE_ADDR      0x74
#define REG_HDCP_RX_BKSV0       0x00
#define REG_HDCP_RX_AKSV0       0x10
#define REG_HDCP_RX_AN0         0x18
#define REG_HDCP_RX_RI0         0x08
#define REG_HDCP_RX_RI1         0x09

#define AKSV_SIZE               5
#define NUM_OF_ONES_IN_KSV      20
#define BYTE_SIZE               0x08
#define TX_KEY_LEN              280

BOOL ms928xdrv_hdmi_tx_hdcp_get_bksv_from_rx(UINT8 *p_data)
{
    UINT8 NumOfOnes = 0;
    UINT8 i, j;
    UINT8 u8_temp;
    
    ms928xdrv_hdmi_tx_ddc_enable(TRUE);
    HDMI_DDC_ReadBytes(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_BKSV0, AKSV_SIZE, p_data);
    
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        u8_temp = p_data[i];
        MS928X_LOG1("bksv = ", u8_temp);
        for (j = 0; j < BYTE_SIZE; j++)
        {
            if (u8_temp & 0x01)
            {
                NumOfOnes++;
            }
            u8_temp >>= 1;
        }
    }
    ms928xdrv_hdmi_tx_ddc_enable(FALSE);
    
    if (NumOfOnes != NUM_OF_ONES_IN_KSV)
    {
        return FALSE;
    }

    MS928X_LOG("bksv ok.");
    
    return TRUE;
}

VOID ms928xdrv_hdmi_tx_hdcp_set_bksv_to_tx(UINT8 *p_data)
{
    UINT8 i;
    
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_BKSV0_REG + i, p_data[i]);
    }
    
    //HPD is OK, TX/RX is connected
    //MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x02);
    MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CFG1_REG, 0x03);
    //MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x42);
    MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x4a);   
}

//AN is random number
static UINT8 __CODE g_HDCP_AN_DATA[] =
{
    0x4d,
    0xdb,
    0xd1,
    0x91,
    0xc9,
    0x0d,
    0x0e,
    0xe7,
};

VOID ms928xdrv_hdmi_tx_hdcp_set_an(VOID)
{
    UINT8 i;

    for (i = 0; i < 8; i ++)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_AN0_REG + i, g_HDCP_AN_DATA[i]);
    }

    ms928xdrv_hdmi_tx_ddc_enable(TRUE);
    for (i = 0; i < 8; i ++)
    {
        HDMI_DDC_WriteByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_AN0 + i, g_HDCP_AN_DATA[i]);
    }
    ms928xdrv_hdmi_tx_ddc_enable(FALSE);
}

VOID ms928xdrv_hdmi_tx_hdcp_set_aksv_to_rx(UINT8 *p_u8_ksv)
{
    UINT8 i;
    
    ms928xdrv_hdmi_tx_ddc_enable(TRUE);
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        HDMI_DDC_WriteByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_AKSV0 + i, p_u8_ksv[i]);
    }
    ms928xdrv_hdmi_tx_ddc_enable(FALSE);
}

VOID ms928xdrv_hdmi_tx_hdcp_set_key_to_tx(UINT8 *p_u8_key)
{
    UINT16 i;
    
    for (i = 0; i < TX_KEY_LEN; i ++)
    {
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_KEY_REG, p_u8_key[i]);
    }
}

VOID ms928xdrv_hdmi_tx_hdcp_enable(BOOL b_enable)
{
    if (b_enable)
    {
        //MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x4a);
        //delay >= 100us for wait m0/ks/r0 is calculated 
        //Delay_us(100);
        //MS928X_LOG1("KM calc done = ", MS928X_ReadByte(MS928X_HDMI_TX_HDCP_STATUS_REG) & MSRT_BIT2);
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x0b);
    }
    else
    {
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x04);
        MS928X_WriteByte(MS928X_HDMI_TX_HDCP_CONTROL_REG, 0x00);
    }
}  

BOOL ms928xdrv_hdmi_tx_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv)
{
    UINT8 u8_arr_aksv_buf[AKSV_SIZE];

    if (ms928xdrv_hdmi_tx_hdcp_get_bksv_from_rx(u8_arr_aksv_buf))
    {
        ms928xdrv_hdmi_tx_hdcp_set_bksv_to_tx(u8_arr_aksv_buf);
        ms928xdrv_hdmi_tx_hdcp_set_an();
        ms928xdrv_hdmi_tx_hdcp_set_aksv_to_rx(p_u8_ksv);
        ms928xdrv_hdmi_tx_hdcp_set_key_to_tx(p_u8_key);

        return TRUE;
    }
    
    return FALSE;
}

UINT8 ms928xdrv_hdmi_tx_hdcp_get_status(VOID)
{
    UINT8 i;
    UINT8 TX_Ri0;
    UINT8 TX_Ri1;
    UINT8 RX_Ri0;
    UINT8 RX_Ri1;
    
    for (i = 0; i < 3; i ++)
    {
        TX_Ri0 = MS928X_ReadByte(MS928X_HDMI_TX_HDCP_RI0_REG);
        TX_Ri1 = MS928X_ReadByte(MS928X_HDMI_TX_HDCP_RI1_REG);
        ms928xdrv_hdmi_tx_ddc_enable(TRUE);
        RX_Ri0 = HDMI_DDC_ReadByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_RI0);
        RX_Ri1 = HDMI_DDC_ReadByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_RI1);
        ms928xdrv_hdmi_tx_ddc_enable(FALSE);
        MS928X_LOG1("TX_Ri0 = ", TX_Ri0);
        MS928X_LOG1("TX_Ri1 = ", TX_Ri1);
        MS928X_LOG1("RX_Ri0 = ", RX_Ri0);
        MS928X_LOG1("RX_Ri1 = ", RX_Ri1);
        if (TX_Ri0 == RX_Ri0 && TX_Ri1 == RX_Ri1)
        {
            //MS928X_LOG("HDCP success.");
            return 0x01;
        }
        else
        {
            Delay_ms(20);
        }
    }

    //MS928X_LOG("HDCP failed.");
    return 0x00;
}

#endif // MS928X_HDMI_HDCP


//
// 
#if MS928X_HDMI_EDID

//
// For EDID Information
//
#define EDID_TAG_ADDR                   (0x00)
#define EDID_REV_ADDR                   (0x01)
#define EDID_TAG_IDX                    (0x02)
#define LONG_DESCR_PTR_IDX              (0x02)
#define MISC_SUPPORT_IDX                (0x03)
// Values
#define EDID_EXTENSION_TAG              (0x02)
#define EDID_REV_THREE                  (0x03)
#define EDID_DATA_START                 (0x04)

//
// Offsets within a Long Descriptors Block
//
#define PIX_CLK_OFFSET                  (0)
#define H_ACTIVE_OFFSET                 (2)
#define V_ACTIVE_OFFSET                 (5)
#define THREE_LSBITS                    (0x07)
#define FOUR_LSBITS                     (0x0F)

// Data Block Tag Codes
#define AUDIO_D_BLOCK                   (0x01)
#define VIDEO_D_BLOCK                   (0x02)
#define VENDOR_SPEC_D_BLOCK             (0x03)
#define SPKR_ALLOC_D_BLOCK              (0x04)
#define USE_EXTENDED_TAG                (0x07)


#define MAX_EDID_BUF_SIZE                 (28)
#define MIN_EDID_BUF_SIZE                 (16)

#define EXTENSION_FLAG                    (0x7E)
#define EDID_BLOCK_1                      (0x80)

//
static UINT8 g_u8EDIDBuf[MAX_EDID_BUF_SIZE] = {0};

// Checking EDID header only
static BOOL EDID_CheckHeader(UINT8* pHeader)
{
    UINT8 i = 0;
    if ((pHeader[0] != 0) || (pHeader[7] != 0))
    {
        return FALSE;
    }

    for (i = 1; i <= 6; i ++)
    {
        if (pHeader[i] != 0xFF)    // Must be 0xFF.
            return FALSE;
    }
    return TRUE;
}

// Support EDID 1.3 only. Pls refer to HDMI 1.3a specification.
static BOOL EDID_Parse861Extensions(MS928X_HDMI_EDID_FLAG_T *pt_edid)
{
    UINT8 u8tagCode;
    UINT8 u8tagLength;
    
    UINT8 u8dataIdx;
    UINT8 u8longDescriptor = 0;
    
    //
    memset(g_u8EDIDBuf, 0, MAX_EDID_BUF_SIZE);
    HDMI_DDC_ReadBytes(0xA0, EDID_BLOCK_1, MAX_EDID_BUF_SIZE, g_u8EDIDBuf);
    
    // 02 03
    if (g_u8EDIDBuf[EDID_TAG_ADDR] != EDID_EXTENSION_TAG)
    {
        return TRUE; //20170331, update according to si9022 hdmi tx sdk.
    }
    if (g_u8EDIDBuf[EDID_REV_ADDR] != EDID_REV_THREE) //20170330, other version set to DVI device
    {
        return TRUE;    
    }
    
    // Support the misc supported-features.
    pt_edid->u8_color_space  = g_u8EDIDBuf[MISC_SUPPORT_IDX] & 0xF0;
    
    u8dataIdx = 0;
    u8longDescriptor = g_u8EDIDBuf[LONG_DESCR_PTR_IDX];
    while (u8dataIdx < u8longDescriptor)
    {
        memset(g_u8EDIDBuf, 0, MAX_EDID_BUF_SIZE);
        // Read data through E2prom
        HDMI_DDC_ReadBytes(0xA0, EDID_BLOCK_1 + EDID_DATA_START + u8dataIdx, MAX_EDID_BUF_SIZE, g_u8EDIDBuf);
        
        // $NOTE:
        // BIT: 7    6    5   4   3   2   1   0
        //      | Tag Code| Length of block   |
        //  
        u8tagCode   = (g_u8EDIDBuf[0] >> 5)& 0x07;
        u8tagLength = (g_u8EDIDBuf[0]) & 0x1F;
        
        if ((u8dataIdx + u8tagLength) > u8longDescriptor)
        {
            return FALSE;
        }
        
        switch (u8tagCode)
        {
        case VIDEO_D_BLOCK:         
        case AUDIO_D_BLOCK:        
        case SPKR_ALLOC_D_BLOCK:
        case USE_EXTENDED_TAG:           
            u8dataIdx += u8tagLength;    // Increase the size.
            u8dataIdx ++;
            break;
            
        case VENDOR_SPEC_D_BLOCK:
            if ((g_u8EDIDBuf[1] == 0x03) && (g_u8EDIDBuf[2] == 0x0C) && (g_u8EDIDBuf[3] == 0x00))
            {
                pt_edid->u8_hdmi_sink = 0x01;    // Yes, it is HDMI sink
                return TRUE;
            }
            break;               
        }  
    } 
    return FALSE;   
}

BOOL ms928xdrv_hdmi_tx_parse_edid(MS928X_HDMI_EDID_FLAG_T *pt_edid)
{      
    BOOL bSucc = TRUE;
    // 
    ms928xdrv_hdmi_tx_ddc_enable(TRUE); 

    pt_edid->u8_hdmi_sink = 0;      // Reset variable
    pt_edid->u8_color_space = 0;    // Reset variable
    
    // 
    memset(g_u8EDIDBuf, 0, 8);
    HDMI_DDC_ReadBytes(0xA0, 0, 8, g_u8EDIDBuf);     
        
    if (! EDID_CheckHeader(g_u8EDIDBuf))
    {
        MS928X_LOG("EDID header failed.");
        bSucc = FALSE;
        goto NEXT;
    }
    //     
    memset(g_u8EDIDBuf, 0, MIN_EDID_BUF_SIZE);
    // Read more one byte for extension flag.
    g_u8EDIDBuf[0] = HDMI_DDC_ReadByte(0xA0,  EXTENSION_FLAG);   
    // Checking if the EDID extension
    if (g_u8EDIDBuf[0] != 0)    
    {
        bSucc = EDID_Parse861Extensions(pt_edid);
    }
    
NEXT:
    ms928xdrv_hdmi_tx_ddc_enable(FALSE);
    return bSucc;
}
#endif //#if MS928X_HDMI_EDID

#endif //#if MS928X_HDMI
#endif
