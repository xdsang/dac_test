/**
******************************************************************************
* @file    ms928x_adc.h
* @author  
* @version V1.0.0
* @date    15-Oct-2014
* @brief   MacroSilicon(MS183X) ADC driver
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS928X_DRV_HDMI_PHY_H__
#define __MACROSILICON_MS928X_DRV_HDMI_PHY_H__


#ifdef __cplusplus
extern "C" {
#endif

#if MS2160_FPGA_VERIFY
VOID ms928x_hdmi_phy_init_for_fpga(VOID);
BOOL ms928x_hdmi_phy_auto_config_for_fpga(VOID);


BOOL ms928x_hdmi_phy_config_for_fpga(UINT16 u16_pixle_clk);
#else

#define ms928x_hdmi_phy_init_for_fpga
#define ms928x_hdmi_phy_auto_config_for_fpga
#define ms928x_hdmi_phy_config_for_fpga(x)

#endif

#ifdef __cplusplus
}
#endif

#endif // __MACROSILICON_MS928X_DRV_HDMI_PHY_H__