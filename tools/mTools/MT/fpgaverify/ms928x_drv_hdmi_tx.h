/**
******************************************************************************
* @file    ms928x_drv_hdmi_tx.h
* @author  
* @version V1.0.0
* @date    31-May-2016
* @brief   hdmi tx module driver declare
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS928X_DRV_HDMI_TX_H__
#define __MACROSILICON_MS928X_DRV_HDMI_TX_H__

//chip type
#define MS9281B_CHIP    (0xB9)
#define MS9281C_CHIP    (0xC9)


// macro enum structure definitions
#define MS928X_HDMI_TX_ENABLE (1)

#define MS928X_CHIP_TYPE MS9281B_CHIP


// register definitions


//video process moudle
#define MS928X_VP_REGBASE                        (0x110)
#define MS928X_VP_DGIN_CFG_REG                   (MS928X_VP_REGBASE + 0x0)
#define MS928X_VP_DGIN_SWAP_REG                  (MS928X_VP_REGBASE + 0x1)
#define MS928X_VP_DDR_CTL_REG                    (MS928X_VP_REGBASE + 0x2)
#define MS928X_VP_DGIN_STATUS_REG                (MS928X_VP_REGBASE + 0x3)
#define MS928X_VP_INOUTSEL_REG                   (MS928X_VP_REGBASE + 0x4)
#define MS928X_VP_CSC_SEL_REG                    (MS928X_VP_REGBASE + 0x5)
#define MS928X_VP_DEREG_EN_REG                   (MS928X_VP_REGBASE + 0x6)
#define MS928X_VP_SYNC_FLIP_REG                  (MS928X_VP_REGBASE + 0x7)
#define MS928X_VP_PXL_SHIFT_L_REG                (MS928X_VP_REGBASE + 0x8)
#define MS928X_VP_PXL_SHIFT_H_REG                (MS928X_VP_REGBASE + 0x9)
#define MS928X_VP_LIN_SHIFT_L_REG                (MS928X_VP_REGBASE + 0xA)
#define MS928X_VP_LIN_SHIFT_H_REG                (MS928X_VP_REGBASE + 0xB)
#define MS928X_VP_HTOTAL_L_REG                   (MS928X_VP_REGBASE + 0xC)
#define MS928X_VP_HTOTAL_H_REG                   (MS928X_VP_REGBASE + 0xD)
#define MS928X_VP_VTOTAL_L_REG                   (MS928X_VP_REGBASE + 0xE)
#define MS928X_VP_VTOTAL_H_REG                   (MS928X_VP_REGBASE + 0xF)
#define MS928X_VP_HS_L_REG                       (MS928X_VP_REGBASE + 0x10)
#define MS928X_VP_HS_H_REG                       (MS928X_VP_REGBASE + 0x11)
#define MS928X_VP_VS_L_REG                       (MS928X_VP_REGBASE + 0x12)
#define MS928X_VP_VS_H_REG                       (MS928X_VP_REGBASE + 0x13)
#define MS928X_VP_HDE_ST_L_REG                   (MS928X_VP_REGBASE + 0x14)
#define MS928X_VP_HDE_ST_H_REG                   (MS928X_VP_REGBASE + 0x15)
#define MS928X_VP_HDE_SP_L_REG                   (MS928X_VP_REGBASE + 0x16)
#define MS928X_VP_HDE_SP_H_REG                   (MS928X_VP_REGBASE + 0x17)
#define MS928X_VP_VDE_O_ST_L_REG                 (MS928X_VP_REGBASE + 0x18)
#define MS928X_VP_VDE_O_ST_H_REG                 (MS928X_VP_REGBASE + 0x19)
#define MS928X_VP_VDE_O_SP_L_REG                 (MS928X_VP_REGBASE + 0x1A)
#define MS928X_VP_VDE_O_SP_H_REG                 (MS928X_VP_REGBASE + 0x1B)
#define MS928X_VP_VDE_E_ST_L_REG                 (MS928X_VP_REGBASE + 0x1C)
#define MS928X_VP_VDE_E_ST_H_REG                 (MS928X_VP_REGBASE + 0x1D)
#define MS928X_VP_VDE_E_SP_L_REG                 (MS928X_VP_REGBASE + 0x1E)
#define MS928X_VP_VDE_E_SP_H_REG                 (MS928X_VP_REGBASE + 0x1F)
#define MS928X_VP_DYN_EN_REG                     (MS928X_VP_REGBASE + 0x20)
#define MS928X_VP_YG_GAIN_REG                    (MS928X_VP_REGBASE + 0x21)
#define MS928X_VP_UB_GAIN_REG                    (MS928X_VP_REGBASE + 0x22)
#define MS928X_VP_VR_GAIN_REG                    (MS928X_VP_REGBASE + 0x23)
#define MS928X_VP_YG_OFT_REG                     (MS928X_VP_REGBASE + 0x24)
#define MS928X_VP_UB_OFT_REG                     (MS928X_VP_REGBASE + 0x25)
#define MS928X_VP_VR_OFT_REG                     (MS928X_VP_REGBASE + 0x26)
#define MS928X_VP_TST_SEL_REG                    (MS928X_VP_REGBASE + 0x27)
#define MS928X_VP_DGO_EN_REG                     (MS928X_VP_REGBASE + 0x28)
#define MS928X_VP_DGO_SWAP_REG                   (MS928X_VP_REGBASE + 0x29)
#define MS928X_VP_HTOTAL_RD_L_REG                (MS928X_VP_REGBASE + 0x2A)
#define MS928X_VP_HTOTAL_RD_H_REG                (MS928X_VP_REGBASE + 0x2B)
#define MS928X_VP_VTOTAL_RD_L_REG                (MS928X_VP_REGBASE + 0x2C)
#define MS928X_VP_VTOTAL_RD_H_REG                (MS928X_VP_REGBASE + 0x2D)
//
#define MS928X_VP_CRC_REG                        (MS928X_VP_REGBASE + 0x30)
#define MS928X_VP_CRC_RDY_REG                    (MS928X_VP_REGBASE + 0x31)
#define MS928X_VP_CRC_1_REG                      (MS928X_VP_REGBASE + 0x32)
#define MS928X_VP_CRC_2_REG                      (MS928X_VP_REGBASE + 0x33)
#define MS928X_VP_CRC_3_REG                      (MS928X_VP_REGBASE + 0x34)
#define MS928X_VP_CRC_4_REG                      (MS928X_VP_REGBASE + 0x35)
#define MS928X_VP_CRC_5_REG                      (MS928X_VP_REGBASE + 0x36)
#define MS928X_VP_CRC_6_REG                      (MS928X_VP_REGBASE + 0x37)
#define MS928X_VP_CRC_7_REG                      (MS928X_VP_REGBASE + 0x38)
#define MS928X_VP_CRC_8_REG                      (MS928X_VP_REGBASE + 0x39)
#define MS928X_VP_CRC_9_REG                      (MS928X_VP_REGBASE + 0x3A)
#define MS928X_VP_CRC_10_REG                     (MS928X_VP_REGBASE + 0x3B)
#define MS928X_VP_CRC_11_REG                     (MS928X_VP_REGBASE + 0x3C)
#define MS928X_VP_CRC_12_REG                     (MS928X_VP_REGBASE + 0x3D)


//hdmi tx phy module
#define MS928X_HDMI_TX_PHY_REGBASE               (0x0210)
#define MS928X_HDMI_TX_PHY_PDZ_REG               (MS928X_HDMI_TX_PHY_REGBASE + 0x0)
#define MS928X_HDMI_TX_PHY_KS_REG                (MS928X_HDMI_TX_PHY_REGBASE + 0x1)
#define MS928X_HDMI_TX_PHY_CKEN_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x2)
#define MS928X_HDMI_TX_PHY_N2_REG                (MS928X_HDMI_TX_PHY_REGBASE + 0x3)
#define MS928X_HDMI_TX_PHY_M0_REG                (MS928X_HDMI_TX_PHY_REGBASE + 0x4)
#define MS928X_HDMI_TX_PHY_M8_REG                (MS928X_HDMI_TX_PHY_REGBASE + 0x5)
#define MS928X_HDMI_TX_PHY_CKIS_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x6)
#define MS928X_HDMI_TX_PHY_ICPI_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x7)
#define MS928X_HDMI_TX_PHY_FN0_REG               (MS928X_HDMI_TX_PHY_REGBASE + 0x8)
#define MS928X_HDMI_TX_PHY_FN8_REG               (MS928X_HDMI_TX_PHY_REGBASE + 0x9)
#define MS928X_HDMI_TX_PHY_FN16_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0xA)
#define MS928X_HDMI_TX_PHY_TRIG_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0xB)
#define MS928X_HDMI_TX_PHY_BGTX_PDZ_REG          (MS928X_HDMI_TX_PHY_REGBASE + 0xC)
#define MS928X_HDMI_TX_PHY_CKINV_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0xD)
#define MS928X_HDMI_TX_PHY_DCC_REG               (MS928X_HDMI_TX_PHY_REGBASE + 0xE)
#define MS928X_HDMI_TX_PHY_MDRV0_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0xF)
#define MS928X_HDMI_TX_PHY_MDRV1_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0x10)
#define MS928X_HDMI_TX_PHY_DMPC_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x11)
#define MS928X_HDMI_TX_PHY_PCD1_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x12)
#define MS928X_HDMI_TX_PHY_PCD2_REG              (MS928X_HDMI_TX_PHY_REGBASE + 0x13)
#define MS928X_HDMI_TX_PHY_PCPD0_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0x14)
#define MS928X_HDMI_TX_PHY_PCPD1_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0x15)
#define MS928X_HDMI_TX_PHY_PCPD2_REG             (MS928X_HDMI_TX_PHY_REGBASE + 0x16)



//hdmi tx shell module
#define MS928X_HDMI_TX_SHELL_REGBASE             (0x0150)
#define MS928X_HDMI_TX_SHELL_INTERRUPT_REG       (MS928X_HDMI_TX_SHELL_REGBASE + 0x0)
#define MS928X_HDMI_TX_SHELL_INT_MASK_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x01)
#define MS928X_HDMI_TX_SHELL_STATUS_REG          (MS928X_HDMI_TX_SHELL_REGBASE + 0x02)
#define MS928X_HDMI_TX_SHELL_DVI_REG             (MS928X_HDMI_TX_SHELL_REGBASE + 0x03)
#define MS928X_HDMI_TX_SHELL_SWRST_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x06)
#define MS928X_HDMI_TX_SHELL_AVMUTE_REG          (MS928X_HDMI_TX_SHELL_REGBASE + 0x07)
#define MS928X_HDMI_TX_SHELL_MODE_REG            (MS928X_HDMI_TX_SHELL_REGBASE + 0x08)
#define MS928X_HDMI_TX_SHELL_INFO_VER_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x09)
#define MS928X_HDMI_TX_SHELL_INFO_TYPE_REG       (MS928X_HDMI_TX_SHELL_REGBASE + 0x0A)
#define MS928X_HDMI_TX_SHELL_INFO_LEN_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x0B)
#define MS928X_HDMI_TX_SHELL_NCTS_REG            (MS928X_HDMI_TX_SHELL_REGBASE + 0x0C)
#define MS928X_HDMI_TX_SHELL_INFO_PACK_REG       (MS928X_HDMI_TX_SHELL_REGBASE + 0x0D)
#define MS928X_HDMI_TX_SHELL_CTRL_REG            (MS928X_HDMI_TX_SHELL_REGBASE + 0x0E)
#define MS928X_HDMI_TX_SHELL_CFG_REG             (MS928X_HDMI_TX_SHELL_REGBASE + 0x0F)
#define MS928X_HDMI_TX_SHELL_R_REG               (MS928X_HDMI_TX_SHELL_REGBASE + 0x10)
#define MS928X_HDMI_TX_SHELL_G_REG               (MS928X_HDMI_TX_SHELL_REGBASE + 0x11)
#define MS928X_HDMI_TX_SHELL_B_REG               (MS928X_HDMI_TX_SHELL_REGBASE + 0x12)
#define MS928X_HDMI_TX_SHELL_RGBEN_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x13)
#define MS928X_HDMI_TX_SHELL_BURST_PREAM_REG     (MS928X_HDMI_TX_SHELL_REGBASE + 0x14)
#define MS928X_HDMI_TX_SHELL_PARRAY_REG          (MS928X_HDMI_TX_SHELL_REGBASE + 0x15)
#define MS928X_HDMI_TX_SHELL_CFG_FLT_REG         (MS928X_HDMI_TX_SHELL_REGBASE + 0x16)
#define MS928X_HDMI_TX_SHELL_INFO_STATUS_REG     (MS928X_HDMI_TX_SHELL_REGBASE + 0x17)
#define MS928X_HDMI_TX_SHELL_SHIFT1_REG          (MS928X_HDMI_TX_SHELL_REGBASE + 0x18)
#define MS928X_HDMI_TX_SHELL_CTS_CTRL_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x19)
#define MS928X_HDMI_TX_SHELL_CTS_SHIFT_REG       (MS928X_HDMI_TX_SHELL_REGBASE + 0x1A)
#define MS928X_HDMI_TX_SHELL_CTS_OLD1_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x1B)      // RO 
#define MS928X_HDMI_TX_SHELL_CTS_OLD2_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x1C)      // RO
#define MS928X_HDMI_TX_SHELL_CTS_STABLE_REG      (MS928X_HDMI_TX_SHELL_REGBASE + 0x1D)      // RO BIT7-BIT4
#define MS928X_HDMI_TX_SHELL_TBUS_SEL_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x24)
#define MS928X_HDMI_TX_SHELL_DOUT_SEL_REG        (MS928X_HDMI_TX_SHELL_REGBASE + 0x25)
#define MS928X_HDMI_TX_SHELL_CNTRL_DATA_REG      (MS928X_HDMI_TX_SHELL_REGBASE + 0x26)
#define MS928X_HDMI_TX_SHELL_LFSR0_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x27)
#define MS928X_HDMI_TX_SHELL_LFSR1_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x28)
#define MS928X_HDMI_TX_SHELL_LFSR2_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x29)
#define MS928X_HDMI_TX_SHELL_LFSR3_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x2A)


//audio module register
#define MS928X_HDMI_TX_AUDIO_REGBASE             (0x0180)
#define MS928X_HDMI_TX_AUDIO_CFG_REG             (MS928X_HDMI_TX_AUDIO_REGBASE + 0x0)
#define MS928X_HDMI_TX_AUDIO_RATE_REG            (MS928X_HDMI_TX_AUDIO_REGBASE + 0x1)
#define MS928X_HDMI_TX_AUDIO_CH_EN_REG           (MS928X_HDMI_TX_AUDIO_REGBASE + 0x2)
#define MS928X_HDMI_TX_AUDIO_BURST_PREAMBLE_REG  (MS928X_HDMI_TX_AUDIO_REGBASE + 0x3)
#define MS928X_HDMI_TX_AUDIO_OUTSYNC_REG         (MS928X_HDMI_TX_AUDIO_REGBASE + 0x4)
#define MS928X_HDMI_TX_AUDIO_CFG_I2S_REG         (MS928X_HDMI_TX_AUDIO_REGBASE + 0x5)
#define MS928X_HDMI_TX_AUDIO_MODESEL_REG         (MS928X_HDMI_TX_AUDIO_REGBASE + 0x6)
#define MS928X_HDMI_TX_AUDIO_CH_SWITCH_0_REG     (MS928X_HDMI_TX_AUDIO_REGBASE + 0x7)
#define MS928X_HDMI_TX_AUDIO_CH_SWITCH_1_REG     (MS928X_HDMI_TX_AUDIO_REGBASE + 0x8)
#define MS928X_HDMI_TX_AUDIO_CH_SWITCH_2_REG     (MS928X_HDMI_TX_AUDIO_REGBASE + 0x9)
#define MS928X_HDMI_TX_AUDIO_LR_SWTICH_REG       (MS928X_HDMI_TX_AUDIO_REGBASE + 0xA)
#define MS928X_HDMI_TX_AUDIO_SPDIF_SEL_REG       (MS928X_HDMI_TX_AUDIO_REGBASE + 0xB)
#define MS928X_HDMI_TX_AUDIO_SPDIF_DIV_REG       (MS928X_HDMI_TX_AUDIO_REGBASE + 0xC)

#define MS928X_HDMI_TX_AUDIO_BYPASSVOL_REG       (MS928X_HDMI_TX_AUDIO_REGBASE + 0x40)
#define MS928X_HDMI_TX_AUDIO_VOL_0_REG           (MS928X_HDMI_TX_AUDIO_REGBASE + 0x41)
#define MS928X_HDMI_TX_AUDIO_VOL_1_REG           (MS928X_HDMI_TX_AUDIO_REGBASE + 0x42)
#define MS928X_HDMI_TX_AUDIO_VOL_2_REG           (MS928X_HDMI_TX_AUDIO_REGBASE + 0x43)
#define MS928X_HDMI_TX_AUDIO_VS_0_REG            (MS928X_HDMI_TX_AUDIO_REGBASE + 0x44)
#define MS928X_HDMI_TX_AUDIO_VS_1_REG            (MS928X_HDMI_TX_AUDIO_REGBASE + 0x45)
#define MS928X_HDMI_TX_AUDIO_ASYNC_REG           (MS928X_HDMI_TX_AUDIO_REGBASE + 0x46)

#define MS928X_HDMI_TX_MISC_HDMI_CLK_REG         (0x0006)
#define MS928X_HDMI_TX_MISC_HDMI_CTS_CLK_REG     (0x0006)
#define MS928X_HDMI_TX_MISC_HDMI_RST_REG         (0x0008)

#define MS928X_HDMI_TX_MISC_DDC_ENABLE_REG       (0x000C)
#define MS928X_HDMI_TX_MISC_DDC_CTRL_REG         (0x000D)

#define MS928X_HDMI_TX_MISC_AUDIO_SPDIF_REV0_REG   (0x0015)

#define MS928X_HDMI_TX_MISC_SRC_CTRL_REG          (0x0026)


//HDCP
#define MS928X_HDMI_TX_HDCP_STATUS_REG           (MS928X_HDMI_TX_SHELL_REGBASE + 0x80)
#define MS928X_HDMI_TX_HDCP_CONTROL_REG          (MS928X_HDMI_TX_SHELL_REGBASE + 0x81)
#define MS928X_HDMI_TX_HDCP_BKSV0_REG            (MS928X_HDMI_TX_SHELL_REGBASE + 0x82)
//
#define MS928X_HDMI_TX_HDCP_AN0_REG              (MS928X_HDMI_TX_SHELL_REGBASE + 0x87)
//
#define MS928X_HDMI_TX_HDCP_RI0_REG              (MS928X_HDMI_TX_SHELL_REGBASE + 0x94)
#define MS928X_HDMI_TX_HDCP_RI1_REG              (MS928X_HDMI_TX_SHELL_REGBASE + 0x95)
#define MS928X_HDMI_TX_HDCP_KEY_REG              (MS928X_HDMI_TX_SHELL_REGBASE + 0x96)
//
#define MS928X_HDMI_TX_HDCP_CFG1_REG             (MS928X_HDMI_TX_SHELL_REGBASE + 0xB7)




#ifdef __cplusplus
extern "C" {
#endif


//dvin sync type
typedef enum _E_MS928X_DVIN_SYNC_TYPE_
{
    MS928X_DVIN_PORT_SYNC_TYPE_HSVS      = 0x00, //default
    MS928X_DVIN_PORT_SYNC_TYPE_HSVSDE    = 0x01,
    MS928X_DVIN_PORT_SYNC_TYPE_VSDE      = 0x02,
    MS928X_DVIN_PORT_SYNC_TYPE_EMBEDED   = 0x03,
    MS928X_DVIN_PORT_ITU_SDR_BT601       = 0x04,
    MS928X_DVIN_PORT_ITU_SDR_BT656       = 0x05,
    MS928X_DVIN_PORT_ITU_SDR_BT1120      = 0x06,
    MS928X_DVIN_PORT_ITU_DDR             = 0x07
}DVIN_SYPC_TYPE_E;

typedef enum _E_MS928X_DVIN_COLOR_SPACE_
{
    MS928X_DVIN_24BIT_RGB     = 0x00, //default
    MS928X_DVIN_24BIT_YUV444  = 0x01,
    MS928X_DVIN_16BIT_YUV422  = 0x02,
    MS928X_DVIN_20BIT_YUV422  = 0x03,
    MS928X_DVIN_24BIT_YUV422  = 0x04,
    MS928X_DVIN_08BIT_YUV422  = 0x05,
    MS928X_DVIN_10BIT_YUV422  = 0x06,
    MS928X_DVIN_12BIT_YUV422  = 0x07
}DVIN_COLOR_SPACE_E;

typedef enum _E_MS928X_DVIN_DATA_SEL_
{
    MS928X_DVIN_DATA_SEL_0  = 0x00, //default
    MS928X_DVIN_DATA_SEL_1  = 0x01,
    MS928X_DVIN_DATA_SEL_2  = 0x02,
    MS928X_DVIN_DATA_SEL_3  = 0x03,
    MS928X_DVIN_DATA_SEL_4  = 0x04,
    MS928X_DVIN_DATA_SEL_5  = 0x05,
    MS928X_DVIN_DATA_SEL_6  = 0x06,
    MS928X_DVIN_DATA_SEL_7  = 0x07
}DVIN_DATA_SEL_E;

/*----------------------------------------------------------------------------------------------------|             
|            |             |                           dgin_data_sel[2:0]                             |             
| Input Mode | Source Data |--------------------------------------------------------------------------|             
|            |             | (default)0  |     1     |      2     |     3     |     4     |     5     |           
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  24-bit    | Y/G[7:0]    |  D[15:8]    |  D[15:8]  | D[23:16]   |  D[23:16] | D[7:0]    | D[7:0]    | 
|  RGB/YUV   | U/B[7:0]    |  D[7:0]     |  D[23:16] | D[15:8]    |  D[7:0]   | D[15:8]   | D[23:16]  |     
|            | V/R[7:0]    |  D[23:16]   |  D[7:0]   | D[7:0]     |  D[15:8]  | D[23:16]  | D[15:8]   |       
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  16-bit    | Y[7:0]      |  D[23:16]   |  D[15:8]  | D[23:16]   |  D[11:4]  | D[15:8]   | D[7:0]    |     
|  YUV422    | UV[7:0]     |  D[15:8]    |  D[23:16] | D[11:4]    |  D[23:16] | D[7:0]    | D[15:8]   | 
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  20-bit    | Y[9:0]      |  D[23:14]   |  D[11:2]  |            |           |           |           |         
|  YUV422    | V[9:0]      |  D[11:2]    |  D[23:14] |　          |           |           |           |           
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  24-bit    | Y[11:0]     |  D[23:12]   |  D[11:0]  |  　  　  　|           |           |           |         
|  YUV422    | UV[11:0]    |  D[11:0]    |  D[23:12] |    　  　  | 　        |           |           |     
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  8-bit     |             |             |           |            |           |           |           |             
|  YUV422    | YUV[7:0]    |  D[23:16]   |  D[11:4]  |            |           |           |           |         
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  10-bit    |             |             |           |            |           |           |           |             
|  YUV422    | YUV[9:0]    |  D[23:14]   |  D[11:2]  |            |           |           |           |         
|------------|-------------|-------------|-----------|------------|-----------|-----------|-----------|             
|  12-bit    |             |             |           |            |           |           |           |             
|  YUV422    |  YUV[11:0]  |  D[23:12]   |  D[11:0]  |  　  　    |           |           |           |         
|-----------------------------------------------------------------------------------------------------*/      

//digital input data msb/lsb swap. 1: [23:0]-->[0:23]
#define MS928X_DVIN_DATA_SWAP_ENABLE     (0x01 << 3)

//DDR mode 1st pixel fetched by falling edge. 1: falling edge; 0: rising edge
#define MS928X_DVIN_DDR_EDGE_INV_ENABLE  (0x01 << 4)

//DDR mode digital input Y/C swap. 0: YCYC; 1: CYCY
#define MS928X_DVIN_DDR_YC_SWAP_ENABLE   (0x01 << 5)

//Range selction in rgb to ycbcr or ycbcr to rgb conversion. 0: 0~255 for 8-bit; 1: 16~235 for 8-bit
typedef enum _E_MS928X_DVIN_YUV_RANGE_SEL_
{
    MS928X_DVIN_YUV_RANGE_0_255_SEL_0   = (0x00 << 6), //default
    MS928X_DVIN_YUV_RANGE_16_235_SEL_1  = (0x01 << 6)
}DVIN_YUV_RANGE_SEL_E;

//YCbCr 709 color space selection. 0: 601 color space; 1: 709 color space
typedef enum _E_MS928X_DVIN_YUV_COLOR_SAPCE_SEL_
{
    MS928X_DVIN_YUV_601  = (0x00 << 7), //default
    MS928X_DVIN_YUV_709  = (0x01 << 7)
}DVIN_YUV_CS_SEL_E;

//Y and C data swap in 8/10/12-bit SDR mode digital in. 0: CYCY; 1: YCYC
#define MS928X_DVIN_SDR_YC_SWAP_ENABLE   (0x01 << 8)

//U and V swap in 422to444 or 444to422 conversion. 0: UVUV; 1: VUVU
#define MS928X_DVIN_UV_FLIP_ENABLE       (0x01 << 9)

//if dvin is double video clk input, need set the flag 
#define MS928X_DVIN_CLK_DOUBLE_FLAG      (0x01 << 10)

//if dvin clk need adjust, phase value x from 0~31
//bit11 is enable clk adjust; bit12 - bit16 is x
#define MS928X_DVIN_CLK_PHASE_CONFIG(x)  (((UINT32)x << 12) | (0x01UL << 11))


//HDMI video
typedef enum _E_MS928X_HDMI_VIDEO_CLK_REPEAT_
{
    MS928X_HDMI_X1CLK      = 0x00,
    MS928X_HDMI_X2CLK      = 0x01,
    MS928X_HDMI_X3CLK      = 0x02,
    MS928X_HDMI_X4CLK      = 0x03
}MS928X_HDMI_CLK_RPT_E;

typedef enum _E_MS928X_HDMI_VIDEO_ASPECT_RATIO_
{
    MS928X_HDMI_4X3     = 0x00,
    MS928X_HDMI_16X9    = 0x01
}MS928X_HDMI_ASPECT_RATIO_E;

typedef enum _E_MS928X_HDMI_VIDEO_SCAN_INFO_
{
    MS928X_HDMI_OVERSCAN     = 0x01,    //television type
    MS928X_HDMI_UNDERSCAN    = 0x02     //computer type
}MS928X_HDMI_SCAN_INFO_E;

typedef enum _E_MS928X_HDMI_COLOR_SPACE_
{
    MS928X_HDMI_RGB        = 0x00,
    MS928X_HDMI_YCBCR422   = 0x01,
    MS928X_HDMI_YCBCR444   = 0x02,
    MS928X_HDMI_XVYCC444   = 0x03
}MS928X_HDMI_CS_E;

typedef enum _E_MS928X_HDMI_COLOR_DEPTH_
{
    MS928X_HDMI_COLOR_DEPTH_8BIT    = 0x00,
    MS928X_HDMI_COLOR_DEPTH_10BIT   = 0x01,
    MS928X_HDMI_COLOR_DEPTH_12BIT   = 0x02,
    MS928X_HDMI_COLOR_DEPTH_16BIT   = 0x03
}MS928X_HDMI_COLOR_DEPTH_E;

typedef enum _E_MS928X_HDMI_COLORIMETRY_
{
    MS928X_HDMI_COLORIMETRY_601    = 0x00,
    MS928X_HDMI_COLORIMETRY_709    = 0x01,
    MS928X_HDMI_COLORIMETRY_656    = 0x02,
    MS928X_HDMI_COLORIMETRY_1120   = 0x03,
    MS928X_HDMI_COLORIMETRY_SMPTE  = 0x04,
    MS928X_HDMI_COLORIMETRY_XVYCC601 = 0x05,
    MS928X_HDMI_COLORIMETRY_XVYCC709 = 0x06
}MS928X_HDMI_COLORIMETRY_E;


//HDMI audio
typedef enum _E_MS928X_HDMI_AUDIO_MODE_
{
    MS928X_HDMI_AUD_MODE_I2S      = 0x00,
    MS928X_HDMI_AUD_MODE_SPDIF    = 0x01,
    MS928X_HDMI_AUD_MODE_HBR      = 0x02,
    MS928X_HDMI_AUD_MODE_DSD      = 0x03
}MS928X_HDMI_AUDIO_MODE_E;

typedef enum _E_MS928X_HDMI_AUDIO_I2S_RATE_
{
    MS928X_HDMI_AUD_RATE_44K1  = 0x00,
    MS928X_HDMI_AUD_RATE_48K   = 0x02,
    MS928X_HDMI_AUD_RATE_32K   = 0x03,
    MS928X_HDMI_AUD_RATE_88K2  = 0x08,
    MS928X_HDMI_AUD_RATE_96K   = 0x0A,
    MS928X_HDMI_AUD_RATE_176K4 = 0x0C,
    MS928X_HDMI_AUD_RATE_192K  = 0x0E
}MS928X_HDMI_AUDIO_RATE_E;

typedef enum _E_MS928X_HDMI_AUDIO_LENGTH_
{
    MS928X_HDMI_AUD_LENGTH_16BITS    = 0x00,
    MS928X_HDMI_AUD_LENGTH_24BITS    = 0x01
}MS928X_HDMI_AUDIO_LENGTH_E;

typedef enum _E_MS928X_HDMI_AUDIO_CHANNEL_
{
    MS928X_HDMI_AUD_2CH    = 0x01,
    MS928X_HDMI_AUD_3CH    = 0x02,
    MS928X_HDMI_AUD_4CH    = 0x03,
    MS928X_HDMI_AUD_5CH    = 0x04,
    MS928X_HDMI_AUD_6CH    = 0x05,
    MS928X_HDMI_AUD_7CH    = 0x06,
    MS928X_HDMI_AUD_8CH    = 0x07
}MS928X_HDMI_AUDIO_CHN_E;


typedef struct _T_MS928X_HDMI_TX_CONFIG_PARA_
{
    UINT8  u8_dvin_sync_type;     // refer to DVIN_SYPC_TYPE_E
    UINT8  u8_dvin_data_type;     // refer to DVIN_COLOR_SPACE_E
    UINT32 u32_dvin_flag;         // bit0 - bit2: refer to DVIN_DATA_SEL_E
                                  // bit3:  refer to MS928X_DVIN_DATA_SWAP_ENABLE
                                  // bit4: refer to MS928X_DVIN_DDR_EDGE_INV_ENABLE
                                  // bit5: refer to MS928X_DVIN_DDR_YC_SWAP_ENABLE
                                  // bit6: refer to DVIN_YUV_RANGE_SEL_E
                                  // bit7: refer to DVIN_YUV_CS_SEL_E
                                  // bit8: refer to MS928X_DVIN_SDR_YC_SWAP_ENABLE
                                  // bit9: refer to MS928X_DVIN_UV_FLIP_ENABLE
                                  // bit10: refer to MS928X_DVIN_CLK_DOUBLE_FLAG
                                  // bit11 - bit16: refer to MS928X_DVIN_CLK_PHASE_CONFIG(x)
    VIDEOTIMING_T pt_dvin_timing; //refer to VIDEOTIMING_T
    
    //
    UINT8  u8_hdmi_out;           // FALSE = dvi out;  TRUE = hdmi out
    UINT8  u8_vic;                // reference to CEA-861 VIC, VESA mode and unknown mode set to 0
    UINT8  u8_clk_rpt;            // enum refer to MS928X_HDMI_CLK_RPT_E. X2CLK = 480i/576i, others = X1CLK
    UINT8  u8_scan_info;          // enum refer to MS928X_HDMI_SCAN_INFO_E
    UINT8  u8_aspect_ratio;       // enum refer to MS928X_HDMI_ASPECT_RATIO_E
    UINT8  u8_color_space;        // enum refer to MS928X_HDMI_CS_E
    UINT8  u8_color_depth;        // enum refer to MS928X_HDMI_COLOR_DEPTH_E
    UINT8  u8_colorimetry;        // enum refer to MS928X_HDMI_COLORIMETRY_E. MS928X_HDMI_COLORIMETRY_601 = 480i/576i/480p/576p and VESA and unknown mode, MS928X_HDMI_COLORIMETRY_709 = CEA HD mode
    //
    UINT8  u8_audio_mode;         // enum refer to MS928X_HDMI_AUDIO_MODE_E
    UINT8  u8_audio_rate;         // enum refer to MS928X_HDMI_AUDIO_RATE_E
    UINT8  u8_audio_bits;         // enum refer to MS928X_HDMI_AUDIO_LENGTH_E
    UINT8  u8_audio_channels;     // enum refer to MS928X_HDMI_AUDIO_CHN_E
    UINT8  u8_audio_speaker_locations;  // 0~255, refer to CEA-861 audio infoframe, BYTE4
}MS928X_HDMITX_CONFIG_T;



/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_phy_output_enable
*  Description:     hdmi tx module output timing on/off
*  Entry:           [in]b_enable: if true turn on 
*                                 else turn off
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms928xdrv_hdmi_tx_phy_output_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_shell_video_mute_enable
*  Description:     hdmi tx shell module video mute
*  Entry:           [in]b_enable, if true mute screen else normal video
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms928xdrv_hdmi_tx_shell_video_mute_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_shell_audio_mute_enable
*  Description:     hdmi tx shell module audio mute
*  Entry:           [in]b_enable, if true mute audio else normal audio
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms928xdrv_hdmi_tx_shell_audio_mute_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_config
*  Description:     hdmi tx config
*  Entry:           [in]pt_hdmi_tx: struct refer to HDMITX_CONFIG_T
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms928xdrv_hdmi_tx_config(MS928X_HDMITX_CONFIG_T *pt_hdmi_tx);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_set_pixel_shift
*  Description:     set video out windows H position pixel shift
*  Entry:           [in]i16value, range from -htotal ~ +htotal
*                                                          
*  Returned value:  None
*  Remark:
***************************************************************/  
VOID ms928xdrv_hdmi_tx_set_pixel_shift(INT16 i16value);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_set_line_shift
*  Description:     set video out windows V position line shift
*  Entry:           [in]i16value, range from -vtotal ~ +vtotal
*                                                          
*  Returned value:  None
*  Remark:
***************************************************************/  
VOID ms928xdrv_hdmi_tx_set_line_shift(INT16 i16value);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_phy_power_down
*  Description:     hdmi tx module power down
*  Entry:           [in]None
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms928xdrv_hdmi_tx_phy_power_down(VOID);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_shell_hpd
*  Description:     hdmi tx hot plug detection
*  Entry:           [in]
*               
*  Returned value:  if hdmi cable plug in return true, else return false
*  Remark:
***************************************************************/ 
BOOL ms928xdrv_hdmi_tx_shell_hpd(VOID);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_ddc_enable
*  Description:     hdmi tx module DDC enable
*  Entry:           [in]b_enable, if true enable ddc, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms928xdrv_hdmi_tx_ddc_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_hdcp_enable
*  Description:     hdmi hdcp enable
*  Entry:           [in]b_enable, if true enable hdcp, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms928xdrv_hdmi_tx_hdcp_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_hdcp_init
*  Description:     hdmi hdcp init
*  Entry:           [in]p_u8_key, 280 bytes hdmi tx key
*                       p_u8_ksv, 5 bytes hdmi tx ksv
*
*  Returned value:  if hdcp init success return true, else return false
*  Remark:
***************************************************************/
BOOL ms928xdrv_hdmi_tx_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv);


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_hdcp_get_status
*  Description:     hdmi hdcp get tx/rx Ri verify result, 2s period polled
*  Entry:           [in]None
*
*  Returned value:  if verify sucess return 0x01, else return 0x00
*  Remark:
***************************************************************/
UINT8 ms928xdrv_hdmi_tx_hdcp_get_status(VOID);


//HDMI EDID
typedef struct _T_MS928X_HDMI_EDID_FLAG_
{   
    UINT8 u8_hdmi_sink;     //1 = HDMI sink, 0 = dvi
    UINT8 u8_color_space;   //color space support flag, flag 1 valid. BIT5: YCBCR444 flag; BIT4: YCBCR422 flag.(RGB must be support)
}MS928X_HDMI_EDID_FLAG_T;


/***************************************************************
*  Function name:   ms928xdrv_hdmi_tx_parse_edid
*  Description:     parse hdmi sink edid
*  Entry:           [out]pt_edid, refer to MS928X_HDMI_EDID_FLAG_T
*
*  Returned value:  if parse sucess return 0x01, else return 0x00
*  Remark:
***************************************************************/
BOOL ms928xdrv_hdmi_tx_parse_edid(MS928X_HDMI_EDID_FLAG_T *pt_edid);

#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS928X_DRV_HDMI_TX_H__
