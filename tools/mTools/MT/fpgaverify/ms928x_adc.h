/**
******************************************************************************
* @file    ms928x_adc.h
* @author  
* @version V1.0.0
* @date    15-Oct-2014
* @brief   MacroSilicon(MS183X) ADC driver
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS928X_DRV_ADC_H__
#define __MACROSILICON_MS928X_DRV_ADC_H__

#define MS928X_DEBUG_LOG   (0)



#ifdef __cplusplus
extern "C" {
#endif

typedef enum _E_MS9281B_INPORT_
{
    MS9281B_VGA_IN     = 0,
    MS9281B_YPBPR_IN   = 1,  
    MS9281B_TVD_IN     = 2
}MS9281B_INPORT_E;

#define MS9281B_TVD_54M_FLAG (0x10)

#if MS2160_FPGA_VERIFY
VOID ms928x_adc_init_for_fpga(UINT8 u8_inport); //refer to MS9281B_INPORT_E
BOOL ms928x_addpll_config_for_fpga(VOID);
VOID ms928x_set_phase_for_fpga(UINT8 u8_value); //range: 0~31

#else

#define ms928x_adc_init_for_fpga(X)
#define ms928x_addpll_config_for_fpga
#define ms928x_set_phase_for_fpga(X)
#define ms928x_set_clamp(X)

#endif


#if MS928X_DEBUG_LOG
#define MS928X_LOG     MS2160_LOG
#define MS928X_LOG1    MS2160_LOG1
#define MS928X_LOG2    MS2160_LOG2
#else
#define MS928X_LOG 
#define MS928X_LOG1 
#define MS928X_LOG2
#endif



VOID   MS928X_SetChipAddr(UINT8 u8_address);
UINT8  MS928X_ReadByte(UINT16 u16_index);
VOID   MS928X_WriteByte(UINT16 u16_index, UINT8 u8_value);
UINT16 MS928X_ReadWord(UINT16 u16_index);
VOID   MS928X_WriteWord(UINT16 u16_index, UINT16 u16_value);
VOID   MS928X_ModBits(UINT16 u16_index, UINT8 u8_mask, UINT8 u8_value);
VOID   MS928X_SetBits(UINT16 u16_index, UINT8 u8_mask);
VOID   MS928X_ClrBits(UINT16 u16_index, UINT8 u8_mask);
VOID   MS928X_ToggleBits(UINT16 u16_index, UINT8 u8_mask, BOOL b_set);

//
UINT32 MS928X_ReadDWord(UINT16 u16_index);
VOID   MS928X_WriteDWord(UINT16 u16_index, UINT32 u32_value);

//I2C read/write with length
VOID   MS928X_ReadBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value);
VOID   MS928X_WriteBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value);

//
UINT16 MS928X_ReadRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length);
VOID   MS928X_WriteRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length, UINT16 u16_value);

//
VOID MS928X_IOStream(UINT8* pStream);




#ifdef __cplusplus
}
#endif

#endif // __MACROSILICON_MS928X_DRV_ADC_H__
