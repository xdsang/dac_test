
#include "stdafx.h"
#include "mainfrm.h"
#include "FileView.h"
#include "Resource.h"
#include "MT.h"
#include <map>
using std::map;

//********** dialog header **********
#include "DebugDlg.h"
//for FPGA system verification 
#include "PageRegisterRW.h"
//#include "PageDacTest.h"
#include "PageDebugVBE.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const int FIRLVL_IMAGE = 0;
const int FIRLVL_SELED_IMAGE = 3;
const int SECLVL_IMAGE = FIRLVL_IMAGE;
const int SECLVL_SELED_IMAGE = FIRLVL_SELED_IMAGE;
const int ITEM_IMAGE = 6;
const int ITEM_SELED_IMAGE = 7;
map<UINT, CRuntimeClass*> dlgMap = {
  {IDD_PAGE_REGISTER_RW, RUNTIME_CLASS(CPageRegisterRW)},
  {IDD_DEBUG, RUNTIME_CLASS(CDebugDlg)},
  { IDD_PAGE_DEBUG_VBE, RUNTIME_CLASS(CPageDebugVBE)},
  //{ IDD_PAGE_DAC_TEST, RUNTIME_CLASS(CPageDacTest)},
};
/////////////////////////////////////////////////////////////////////////////
// CFileView

CFileView::CFileView()
{
}

CFileView::~CFileView()
{
}

BEGIN_MESSAGE_MAP(CFileView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_PROPERTIES, OnProperties)
	ON_COMMAND(ID_OPEN, OnFileOpen)
	ON_COMMAND(ID_OPEN_WITH, OnFileOpenWith)
	ON_COMMAND(ID_DUMMY_COMPILE, OnDummyCompile)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar 消息处理程序

int CFileView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    return -1;

  CRect rectDummy;
  rectDummy.SetRectEmpty();

  // 创建视图: 
  const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS;

  if (!m_wndFileView.Create(dwViewStyle, rectDummy, this, 4))
  {
    TRACE0("未能创建文件视图\n");
    return -1;      // 未能创建
  }

  m_font.CreateFont(
	  m_LabelFontSize, 0, 0, 0,
	  FW_NORMAL, FALSE, FALSE, 0,
	  0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
	  DEFAULT_PITCH, m_LabelFontName);

  // 加载视图图像: 
  m_FileViewImages.Create(IDB_FILE_VIEW, 16, 0, RGB(255, 0, 255));
  m_wndFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);
  m_wndFileView.SetFont(&m_font);

  m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_EXPLORER);
  m_wndToolBar.LoadToolBar(IDR_EXPLORER, 0, 0, TRUE /* 已锁定*/);

  OnChangeVisualStyle();

  m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);

  m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

  m_wndToolBar.SetOwner(this);

  // 所有命令将通过此控件路由，而不是通过主框架路由: 
  m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

  // 填入一些静态树视图数据(此处只需填入虚拟代码，而不是复杂的数据)
  FillFileView();
  AdjustLayout();

  return 0;
}

void CFileView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CFileView::FillFileView()
{
  // insert App level pages
  HTREEITEM hTop = m_wndFileView.InsertItem(_T("APP LEVEL"), FIRLVL_IMAGE, FIRLVL_IMAGE);
  HTREEITEM hItem;
  hItem = m_wndFileView.InsertItem(_T("SYSTEM"), ITEM_IMAGE, ITEM_SELED_IMAGE, hTop);
  m_wndFileView.SetItemData(hItem, IDD_DEBUG);

  hItem = m_wndFileView.InsertItem(_T("Register R/W"), ITEM_IMAGE, ITEM_SELED_IMAGE, hTop);
  m_wndFileView.SetItemData(hItem, IDD_PAGE_REGISTER_RW);


  m_wndFileView.SetItemState(hTop, TVIS_BOLD, TVIS_BOLD);
  m_wndFileView.Expand(hTop, TVE_EXPAND);
  // end of App level

  // insert Debug Pages 
  hTop = m_wndFileView.InsertItem(_T("Debug Pages"), FIRLVL_IMAGE, FIRLVL_IMAGE);
  m_wndFileView.SetItemState(hTop, TVIS_BOLD, TVIS_BOLD);
  HTREEITEM hSec;

  // VBE module
  hSec = m_wndFileView.InsertItem(_T("VBE"), SECLVL_IMAGE, SECLVL_IMAGE, hTop);
  hItem = m_wndFileView.InsertItem(_T("VBE"), ITEM_IMAGE, ITEM_SELED_IMAGE, hSec);
  m_wndFileView.SetItemData(hItem, IDD_PAGE_DEBUG_VBE);

  m_wndFileView.Expand(hTop, TVE_EXPAND);
// m_wndFileView.Expand(hSec, TVE_EXPAND);
// m_wndFileView.Expand(hInc, TVE_EXPAND);
}

void CFileView::OnContextMenu(CWnd* pWnd, CPoint point)
{
  CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndFileView;
  ASSERT_VALID(pWndTree);

  if (pWnd != pWndTree)
  {
    CDockablePane::OnContextMenu(pWnd, point);
    return;
  }
#if 0
  if (point != CPoint(-1, -1))
  {
    // 选择已单击的项: 
    CPoint ptTree = point;
    pWndTree->ScreenToClient(&ptTree);

    UINT flags = 0;
    HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
    if (hTreeItem != NULL)
    {
      pWndTree->SelectItem(hTreeItem);
    }
  }

  pWndTree->SetFocus();
  theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EXPLORER, point.x, point.y, this, TRUE);
#endif  
}

void CFileView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndFileView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CFileView::OnProperties()
{
  // AfxMessageBox(_T("属性...."));

}

void CFileView::OnFileOpen()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnFileOpenWith()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnDummyCompile()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnEditCut()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnEditCopy()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnEditClear()
{
	// TODO: 在此处添加命令处理程序代码
}

void CFileView::OnPaint()
{
	CPaintDC dc(this); // 用于绘制的设备上下文

	CRect rectTree;
	m_wndFileView.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CFileView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	m_wndFileView.SetFocus();
}

void CFileView::OnChangeVisualStyle()
{
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_EXPLORER_24 : IDR_EXPLORER, 0, 0, TRUE /* 锁定*/);

	m_FileViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_FILE_VIEW_24 : IDB_FILE_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("无法加载位图: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_FileViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_FileViewImages.Add(&bmp, RGB(255, 0, 255));

	m_wndFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);
}


void CFileView::LoadPage(UINT nID, LPCTSTR nTitile)
{
  CRuntimeClass*  pDlg = dlgMap[nID];
  if (pDlg) {
    CMainFrame* pMain = (CMainFrame*)AfxGetApp()->m_pMainWnd;
    pMain->activatePage(nID, nTitile, NULL, pDlg);
  }
}


void CFileView::UpdateTreeImage(NMTREEVIEW *lpNM)
{
  int nImage,nImage1;
  if (lpNM->itemNew.hItem)
  {
    CTreeCtrl* pTree = (CTreeCtrl*)CWnd::FromHandle(lpNM->hdr.hwndFrom);

    pTree->GetItemImage(lpNM->itemNew.hItem,nImage,nImage1);

    if (nImage == FIRLVL_IMAGE && lpNM->action == TVE_EXPAND)
    {
      pTree->SetItemImage(lpNM->itemNew.hItem, FIRLVL_SELED_IMAGE, FIRLVL_SELED_IMAGE);
    }
    else if (lpNM->action == TVE_COLLAPSE && nImage == FIRLVL_SELED_IMAGE)
    {
      pTree->SetItemImage(lpNM->itemNew.hItem, FIRLVL_IMAGE, FIRLVL_IMAGE);
    }
  }
}  

BOOL CFileView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
  NMHDR* pNMHDR = (NMHDR*)lParam;
  ASSERT(pNMHDR != NULL);
  HTREEITEM hItem;
  CString itemStr;
  NMTREEVIEW* lpNM;
  CTreeCtrl* pTree;

  lpNM = (NMTREEVIEW*)lParam;
  switch (pNMHDR->code)
  {
  case TVN_SELCHANGED:
    hItem = m_wndFileView.GetSelectedItem();
    itemStr = m_wndFileView.GetItemText(hItem);
    TRACE(_T("\nSelectItem: %s\n"), itemStr);
    pTree = (CTreeCtrl*)CWnd::FromHandle(lpNM->hdr.hwndFrom);
    LoadPage(0xFFFF & pTree->GetItemData(lpNM->itemNew.hItem), itemStr);
    break;
  case TVN_ITEMEXPANDED:
    UpdateTreeImage(lpNM);
    break;
  default:
    break;
  }

  return CWnd::OnNotify(wParam, lParam, pResult);
}
