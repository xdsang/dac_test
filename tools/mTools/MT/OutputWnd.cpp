
#include "stdafx.h"

#include "OutputWnd.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString LogFontName = _T("Courier New");
const int LogFontSize = 14;
/////////////////////////////////////////////////////////////////////////////
// COutputBar

COutputWnd::COutputWnd()
{
}

COutputWnd::~COutputWnd()
{
}

BEGIN_MESSAGE_MAP(COutputWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

int COutputWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 创建选项卡窗口: 
	if (!m_wndTabs.Create(CMFCTabCtrl::STYLE_FLAT, rectDummy, this, 1))
	{
		TRACE0("未能创建输出选项卡窗口\n");
		return -1;      // 未能创建
	}

	// 创建输出窗格: 
	const DWORD dwStyle = LBS_NOINTEGRALHEIGHT | WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | LBS_EXTENDEDSEL | LBS_HASSTRINGS;

	if (!m_wndOutputBuild.Create(dwStyle, rectDummy, &m_wndTabs, 2) ||
		!m_wndOutputDebug.Create(dwStyle, rectDummy, &m_wndTabs, 3) ||
		!m_wndOutputFind.Create(dwStyle, rectDummy, &m_wndTabs, 4))
	{
		TRACE0("未能创建输出窗口\n");
		return -1;      // 未能创建
	}

	// 修改Log字体
	m_LogFont.CreateFontW(
		LogFontSize, 0, 0, 0,
		FW_NORMAL, FALSE, FALSE, 0,
		0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH, LogFontName);

	UpdateFonts();

	CString strTabName;
	BOOL bNameValid;

	// 将列表窗口附加到选项卡: 
	bNameValid = strTabName.LoadString(IDS_BUILD_TAB);
	ASSERT(bNameValid);
	m_wndTabs.AddTab(&m_wndOutputBuild, strTabName, (UINT)0);
	bNameValid = strTabName.LoadString(IDS_DEBUG_TAB);
	ASSERT(bNameValid);
	m_wndTabs.AddTab(&m_wndOutputDebug, strTabName, (UINT)0);
	// bNameValid = strTabName.LoadString(IDS_FIND_TAB);
	// ASSERT(bNameValid);
	// m_wndTabs.AddTab(&m_wndOutputFind, strTabName, (UINT)2);

	// 使用一些虚拟文本填写输出选项卡(无需复杂数据)
	FillBuildWindow();
	FillDebugWindow();
	// FillFindWindow();

	return 0;
}

void COutputWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	// 选项卡控件应覆盖整个工作区: 
	m_wndTabs.SetWindowPos (NULL, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

void COutputWnd::AdjustHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	//CFont* pOldFont = dc.SelectObject(&afxGlobalData.fontBold);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		cxExtentMax = max(cxExtentMax, (int)dc.GetTextExtent(strItem).cx);
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(&m_LogFont);
}

void COutputWnd::FillBuildWindow()
{
  m_wndOutputBuild.AddString(_T("生成输出正显示在此处。"));
}

void COutputWnd::FillDebugWindow()
{
  m_wndOutputDebug.AddString(_T("调试输出正显示在此处。"));

}

void COutputWnd::FillFindWindow()
{
#if 0
  m_wndOutputFind.AddString(_T("查找输出正显示在此处。"));
  m_wndOutputFind.AddString(_T("输出正显示在列表视图的行中"));
  m_wndOutputFind.AddString(_T("但您可以根据需要更改其显示方式..."));
#endif
}

// clear log window
void COutputWnd::ClearLogWindow()
{
  while (m_wndOutputBuild.GetCount())
    {
	  m_wndOutputBuild.DeleteString(0);
    }

  while (m_wndOutputDebug.GetCount())
    {
	  m_wndOutputDebug.DeleteString(0);
     }
}
// save log
void COutputWnd::SaveLog()
{
	CString filename, extension, log;
	FILE *fp = NULL;

	LPCTSTR szFilter = _T("Text File (*.txt)|*.txt|Batch File (*.bat)|*.bat|C File (*.c)|*.c|All Files (*.*)|*.*||");
	CFileDialog filedlg(false, _T("txt"), _T("Log"), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilter, AfxGetMainWnd());


	if (filedlg.DoModal() == IDOK) {
		filename = filedlg.GetPathName();
		extension = filedlg.GetFileExt();
		size_t origsize = wcslen(filename) + 1;
		size_t convertedChars = 0;
		const size_t newsize = origsize * 2;
		char *nstring = new char[newsize];
		wcstombs_s(&convertedChars, nstring, newsize, filename, _TRUNCATE);
		fopen_s(&fp, nstring, "wt+");

		if (fp) {
			size_t origsize_e = wcslen(extension) + 1;
			size_t convertedChars_e = 0;
			const size_t newsize_e = origsize_e * 2;
			char *nstring_e = new char[newsize_e];
			wcstombs_s(&convertedChars_e, nstring_e, newsize_e, extension, _TRUNCATE);
			log = filedlg.GetPathName();
			Log(log);

			if (!_strcmpi(nstring_e, "txt"))
			{
				log.Format(_T("save to txt file -- "));
				log = log + filename;
				Log(log);
				for (int i = 0; i<m_wndOutputBuild.GetCount(); i++)
				{
					m_wndOutputBuild.GetText(i, log);
					// CString -> string
					USES_CONVERSION;
					string log_str(W2A(log));
					fprintf(fp, log_str.c_str());
					fprintf(fp, "\n");
				}
				fclose(fp);
				log.Format(_T("save to txt file done -- "));
				log = log + filename;
				Log(log);
			}
			fclose(fp);
		}
		else
		{
			MessageBox(TEXT("Open file failed"), TEXT("Error"), MB_OK);
		}
	}
}

// log show in build windows
void COutputWnd::LogWindowAppend(CString str)
{
  m_wndOutputBuild.AddString(str);
  m_wndOutputBuild.SetTopIndex(m_wndOutputBuild.GetCount() - 1);
}

void COutputWnd::DebugWindowAppend(CString str)
{
  m_wndOutputDebug.AddString(str);
  m_wndOutputDebug.SetTopIndex(m_wndOutputDebug.GetCount() -1);
}


void COutputWnd::ModifyLatestLog(CString str)
{
  m_wndOutputBuild.DeleteString(m_wndOutputBuild.GetCount()-1);

  LogWindowAppend(str);
}

void COutputWnd::UpdateFonts()
{
	//m_wndOutputBuild.SetFont(&afxGlobalData.fontBold);
	//m_wndOutputDebug.SetFont(&afxGlobalData.fontBold);
	//m_wndOutputFind.SetFont(&afxGlobalData.fontBold);
	m_wndOutputBuild.SetFont(&m_LogFont);
	m_wndOutputDebug.SetFont(&m_LogFont);
	m_wndOutputFind.SetFont(&m_LogFont);
}

/////////////////////////////////////////////////////////////////////////////
// COutputList1

COutputList::COutputList()
{
    m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
}

COutputList::~COutputList()
{
}

BEGIN_MESSAGE_MAP(COutputList, CListBox)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_COPY_ALL, OnEditCopyAll)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_VIEW_OUTPUTWND, OnViewOutput)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// COutputList 消息处理程序

void COutputList::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	CMenu menu;
	menu.LoadMenu(IDR_OUTPUT_POPUP);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}

	SetFocus();
}

void COutputList::writeToClip(CString source)
{
  if(OpenClipboard())  
  {  

    EmptyClipboard();
    size_t cbStr = (source.GetLength() + 1) * sizeof(TCHAR);
    HGLOBAL hData = GlobalAlloc(GMEM_MOVEABLE, cbStr);
    memcpy_s(GlobalLock(hData), cbStr, source.LockBuffer(), cbStr);
    GlobalUnlock(hData);
    source.UnlockBuffer();

    // For the appropriate data formats...
    UINT uiFormat = (sizeof(TCHAR) == sizeof(WCHAR)) ? CF_UNICODETEXT : CF_TEXT;
    if (::SetClipboardData(uiFormat, hData) == NULL)  
    {
      AfxMessageBox(_T("Unable to set Clipboard data"));    
      CloseClipboard();
      return;  
    }  

    CloseClipboard();
  }  
}

void COutputList::OnEditCopy()
{
  CString source;
  CString str;
  int n;
  n= this->GetSelCount();
  if (n == 0)
	  return;
  CArray<int, int> aryListBoxSel;
  aryListBoxSel.SetSize(n);
  GetSelItems(n, aryListBoxSel.GetData());
  for (int i = 0; i < n; i++)
  {
	  int nIndex = aryListBoxSel.GetAt(i);
	  GetText(nIndex, str);
	  source += str +  CString("\r\n");
  }
  writeToClip(source);
}

void COutputList::OnEditCopyAll()
{
  CString source;
  CString str;
  int n;
  for (int i = 0; i < this->GetCount(); i++)
  {
    n = this->GetTextLen(i);
    this->GetText(i, str.GetBuffer(n));
    str.ReleaseBuffer();
    source += str.GetBuffer(0) + CString("\r\n");
  }
  writeToClip(source);
}
  
void COutputList::OnEditClear()
{
  while (this->GetCount())
  {
    this->DeleteString(0);
  }
}

void COutputList::OnViewOutput()
{
  CMainFrame* pMain = (CMainFrame*)AfxGetApp()->m_pMainWnd;
  pMain->getOutputWnd()->ShowPane(false, FALSE, TRUE);
}

BOOL COutputList::PreTranslateMessage(MSG* pMsg)
{
  // TODO: 在此添加专用代码和/或调用基类
  if (WM_KEYFIRST <= pMsg->message&&pMsg->message <= WM_KEYLAST)
  {
    HACCEL hAccel = m_hAccel;
    if (hAccel && ::TranslateAccelerator(m_hWnd, hAccel, pMsg))
      return TRUE;
  }
  return CListBox::PreTranslateMessage(pMsg);
}
