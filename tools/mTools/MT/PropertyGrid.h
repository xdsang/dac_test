
#pragma once

class PropertyGridProperty : public CMFCPropertyGridProperty
{
    DECLARE_DYNAMIC(PropertyGridProperty)
    friend class CMFCPropertyGridCtrl;

public:
	// Group constructor
	PropertyGridProperty(const CString& strGroupName, DWORD_PTR dwData = 0, BOOL bIsValueList = FALSE);

	// Simple property
	PropertyGridProperty(const CString& strName, const COleVariant& varValue, LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0,
		LPCTSTR lpszEditMask = NULL, LPCTSTR lpszEditTemplate = NULL, LPCTSTR lpszValidChars = NULL);
#if 0
    PropertyGridProperty(int itemID, const CString& strGroupName, DWORD_PTR dwData = 0, BOOL bIsValueList = FALSE);

    PropertyGridProperty(int itemID, const CString& strName, const COleVariant& varValue,  int textLimit = 0, LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0,
        LPCTSTR lpszEditMask = NULL, LPCTSTR lpszEditTemplate = NULL, LPCTSTR lpszValidChars = NULL);
#endif
    virtual ~PropertyGridProperty();
    virtual BOOL  OnUpdateValue();
	virtual CString  FormatProperty();
	virtual BOOL OnClickValue(UINT uiMsg, CPoint point);
	virtual void OnClickName(CPoint /*point*/);
	void UpdateValue();
protected:
	DWORD       _hexvalue;
};

