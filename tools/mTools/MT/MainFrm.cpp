
// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "MT.h"

#include "MainFrm.h"
#include "MSRegister.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrames

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;


BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
    ON_WM_CREATE()
    ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
    ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
    ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
    ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
    ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
    ON_WM_SETTINGCHANGE()
    ON_COMMAND(ID_FILE_OPEN_REGS, &CMainFrame::OnFileOpenRegs)
    ON_COMMAND(ID_FILE_SAVE_REGS, &CMainFrame::OnFileSaveRegs)
    ON_COMMAND(ID_MULTI_PAGES, &CMainFrame::OnMultiPages)
    ON_UPDATE_COMMAND_UI(ID_MULTI_PAGES, &CMainFrame::OnUpdateMultiPages)
END_MESSAGE_MAP()

static UINT indicators[] =
{
    ID_SEPARATOR,           // 状态行指示器
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
};

// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
    // TODO: 在此添加成员初始化代码
    theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2008);
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
        return -1;

    BOOL bNameValid;

    if (!m_wndMenuBar.Create(this))
    {
        TRACE0("未能创建菜单栏\n");
        return -1;      // 未能创建
    }

    m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

    // 防止菜单栏在激活时获得焦点
    CMFCPopupMenu::SetForceMenuFocus(FALSE);

    if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
        !m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MS_TOOLBAR_256 : IDR_MS_TOOLBAR))
    {
        TRACE0("未能创建工具栏\n");
        return -1;      // 未能创建
    }

    CString strToolBarName;
    bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
    ASSERT(bNameValid);
    m_wndToolBar.SetWindowText(strToolBarName);

    CString strCustomize;
    bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
    ASSERT(bNameValid);
    // m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

    // 允许用户定义的工具栏操作: 
    InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

    if (!m_wndStatusBar.Create(this))
    {
        TRACE0("未能创建状态栏\n");
        return -1;      // 未能创建
    }
    m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

    // TODO: 如果您不希望工具栏和菜单栏可停靠，请删除这五行
    m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
    m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
    EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndMenuBar);
    DockPane(&m_wndToolBar);


    // 启用 Visual Studio 2005 样式停靠窗口行为
    CDockingManager::SetDockingMode(DT_SMART);
    // 启用 Visual Studio 2005 样式停靠窗口自动隐藏行为
    EnableAutoHidePanes(CBRS_ALIGN_ANY);

    // 加载菜单项图像(不在任何标准工具栏上): 
    CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

    // 创建停靠窗口
    if (!CreateDockingWindows())
    {
        TRACE0("未能创建停靠窗口\n");
        return -1;
    }

    m_wndFileView.EnableDocking(CBRS_ALIGN_ANY);
    m_wndClassView.EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndFileView);
    CDockablePane* pTabbedBar = NULL;
    m_wndClassView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);
    m_wndOutput.EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndOutput);
    m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndProperties);

    // 基于持久值设置视觉管理器和样式
    OnApplicationLook(theApp.m_nAppLook);

    // 启用增强的窗口管理对话框
    EnableWindowsDialog(ID_WINDOW_MANAGER, ID_WINDOW_MANAGER, TRUE);

    // 启用工具栏和停靠窗口菜单替换
    EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, strCustomize, ID_VIEW_TOOLBAR);

    // 启用快速(按住 Alt 拖动)工具栏自定义
    CMFCToolBar::EnableQuickCustomization();

    if (CMFCToolBar::GetUserImages() == NULL)
    {
        // 加载用户定义的工具栏图像
        if (m_UserImages.Load(_T(".\\UserImages.bmp")))
        {
            CMFCToolBar::SetUserImages(&m_UserImages);
        }
    }

#if 0
    // 启用菜单个性化(最近使用的命令)
    // TODO: 定义您自己的基本命令，确保每个下拉菜单至少有一个基本命令。
    CList<UINT, UINT> lstBasicCommands;

    lstBasicCommands.AddTail(ID_FILE_NEW);
    lstBasicCommands.AddTail(ID_FILE_OPEN);
    lstBasicCommands.AddTail(ID_FILE_SAVE);
    lstBasicCommands.AddTail(ID_FILE_PRINT);
    lstBasicCommands.AddTail(ID_APP_EXIT);
    lstBasicCommands.AddTail(ID_EDIT_CUT);
    lstBasicCommands.AddTail(ID_EDIT_PASTE);
    lstBasicCommands.AddTail(ID_EDIT_UNDO);
    lstBasicCommands.AddTail(ID_APP_ABOUT);
    lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
    lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_WINDOWS_7);
    lstBasicCommands.AddTail(ID_SORTING_SORTALPHABETIC);
    lstBasicCommands.AddTail(ID_SORTING_SORTBYTYPE);
    lstBasicCommands.AddTail(ID_SORTING_SORTBYACCESS);
    lstBasicCommands.AddTail(ID_SORTING_GROUPBYTYPE);

    CMFCToolBar::SetBasicCommands(lstBasicCommands);
#endif

    return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
    if( !CMDIFrameWndEx::PreCreateWindow(cs) )
        return FALSE;
    // TODO: 在此处通过修改
    //  CREATESTRUCT cs 来修改窗口类或样式

    return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
    BOOL bNameValid;

    // 创建类视图
    CString strClassView;
    bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
    ASSERT(bNameValid);
    if (!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
    {
        TRACE0("未能创建“类视图”窗口\n");
        return FALSE; // 未能创建
    }

    // 创建文件视图
    CString strFileView;
    bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
    ASSERT(bNameValid);
    if (!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT| CBRS_FLOAT_MULTI))
    {
        TRACE0("未能创建“文件视图”窗口\n");
        return FALSE; // 未能创建
    }

    // 创建输出窗口
    CString strOutputWnd;
    bNameValid = strOutputWnd.LoadString(IDS_OUTPUT_WND);
    ASSERT(bNameValid);
    if (!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUTWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
    {
        TRACE0("未能创建输出窗口\n");
        return FALSE; // 未能创建
    }

    // 创建属性窗口
    CString strPropertiesWnd;
    bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
    ASSERT(bNameValid);
    if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
    {
        TRACE0("未能创建“属性”窗口\n");
        return FALSE; // 未能创建
    }

    SetDockingWindowIcons(theApp.m_bHiColorIcons);
    return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
    HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndFileView.SetIcon(hFileViewIcon, FALSE);

    HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndClassView.SetIcon(hClassViewIcon, FALSE);

    HICON hOutputBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndOutput.SetIcon(hOutputBarIcon, FALSE);

    HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
    CMDIFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
    CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 消息处理程序

void CMainFrame::OnWindowManager()
{
    ShowWindowsDialog();
}

void CMainFrame::OnViewCustomize()
{
    CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* 扫描菜单*/);
    pDlgCust->EnableUserDefinedToolbars();
    pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
    LRESULT lres = CMDIFrameWndEx::OnToolbarCreateNew(wp,lp);
    if (lres == 0)
    {
        return 0;
    }

    CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
    ASSERT_VALID(pUserToolbar);

    BOOL bNameValid;
    CString strCustomize;
    bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
    ASSERT(bNameValid);

    pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
    return lres;
}

void CMainFrame::OnApplicationLook(UINT id)
{
    CWaitCursor wait;

    theApp.m_nAppLook = id;

    switch (theApp.m_nAppLook)
    {
    case ID_VIEW_APPLOOK_WIN_2000:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
        break;

    case ID_VIEW_APPLOOK_OFF_XP:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
        break;

    case ID_VIEW_APPLOOK_WIN_XP:
        CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
        break;

    case ID_VIEW_APPLOOK_OFF_2003:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    case ID_VIEW_APPLOOK_VS_2005:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    case ID_VIEW_APPLOOK_VS_2008:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    case ID_VIEW_APPLOOK_WINDOWS_7:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    default:
        switch (theApp.m_nAppLook)
        {
        case ID_VIEW_APPLOOK_OFF_2007_BLUE:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_BLACK:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_SILVER:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_AQUA:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
            break;
        }

        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
        CDockingManager::SetDockingMode(DT_SMART);
    }

    m_wndOutput.UpdateFonts();
    RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

    theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
    pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}


BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
    // 基类将执行真正的工作

    if (!CMDIFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
    {
        return FALSE;
    }


    // 为所有用户工具栏启用自定义按钮
    BOOL bNameValid;
    CString strCustomize;
    bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
    ASSERT(bNameValid);

    for (int i = 0; i < iMaxUserToolbars; i ++)
    {
        CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
        if (pUserToolbar != NULL)
        {
            pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
        }
    }

    return TRUE;
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
    CMDIFrameWndEx::OnSettingChange(uFlags, lpszSection);
    m_wndOutput.UpdateFonts();
}

CMDIChildWnd* CMainFrame::activatePage(UINT nID, LPCTSTR nTitile, CMDIChildWnd *pFrm, CRuntimeClass *pNewViewClass)
{
  CCreateContext context;
	CMainFrame* pMainFrame = ((CMainFrame*)AfxGetMainWnd());
	CMDIChildWnd *pChildFrm = pMainFrame->MDIGetActive();  //获取正处于激活状态的frame

  if (pChildFrm != (CMDIChildWnd*)NULL) {
        if (!mMultiDebugPageMode) {
    pChildFrm->DestroyWindow();
  }
		//TODO:设置销毁重复打开的window

		CMDIChildWnd* pFirstFrame = pMainFrame->MDIGetActive();
		CMDIChildWnd* pFrame = pMainFrame->MDIGetActive();
		if (pFrame)
		{
			do 
			{
				CString str;
				pFrame->GetWindowText(str);
				if (nTitile == str)
				{
					pFrame->ShowWindow(SW_SHOWMAXIMIZED);
					return pFrame;
				}
				pMainFrame->MDINext();
				pFrame = pMainFrame->MDIGetActive();
			} while (pFirstFrame != pFrame);
		}
	}

  if(!(pChildFrm = new CMDIChildWnd()))
    return NULL;

  context.m_pNewViewClass = pNewViewClass;
        
  if(!pChildFrm->LoadFrame(nID, WS_OVERLAPPEDWINDOW|FWS_ADDTOTITLE,this,&context)){
    delete pChildFrm;
    return NULL;
  }
        
  pChildFrm->ShowWindow(SW_SHOWMAXIMIZED);
		pChildFrm->SetTitle(nTitile);
  pChildFrm->InitialUpdateFrame(NULL,true);

  return  pChildFrm;
}



void CMainFrame::OnFileOpenRegs()
{
    // TODO: 在此添加命令处理程序代码
    CString     filename, extension;


#if MTOOLS_FOR_INTERNAL
    LPCTSTR szFilter = _T("All Files (*.*)|*.*|Text File(*.txt)|*.txt|Batch File(*.bat)|*.bat|");
    CFileDialog filedlg(TRUE, _T("txt"), _T("ms1850"), OFN_FILEMUSTEXIST, szFilter, AfxGetMainWnd());
#else
    LPCTSTR szFilter = _T("All Files (*.*)|*.*|Batch File(*.bat)|*.bat|");
    CFileDialog filedlg(TRUE, _T("bat"), _T("ms1850"), OFN_FILEMUSTEXIST, szFilter, AfxGetMainWnd());
#endif
    FILE *fp = NULL;
    CString log;

    if (filedlg.DoModal() == IDOK) {
        filename = filedlg.GetPathName();
        extension = filedlg.GetFileExt();
        size_t origsize = wcslen(filename) + 1;
        size_t convertedChars = 0;
        const size_t newsize = origsize * 2;
        char *nstring = new char[newsize];
        wcstombs_s(&convertedChars, nstring, newsize, filename, _TRUNCATE);
        fopen_s(&fp, nstring, "rt");
        delete(nstring);

        if (fp) {
            size_t origsize0 = wcslen(extension) + 1;
            size_t convertedChars0 = 0;
            const size_t newsize0 = origsize0 * 2;
            char *nstring0 = new char[newsize0];
            wcstombs_s(&convertedChars0, nstring0, newsize0, extension, _TRUNCATE);
            if (!_strcmpi(nstring0, "txt"))
            {
#if MTOOLS_FOR_INTERNAL
                log.Format(_T("start to load txt file -- "));
                log = log + filename;
                Log(log);
                pMSREG->load_txt(fp);
                fclose(fp);
                log.Format(_T("#### OK ####  load txt file done -- "));
                log = log + filename;
                Log(log);
#endif
            }
            else if (!_strcmpi(nstring0, "bat"))
            {
                log.Format(_T("start to load bat file -- "));
                log = log + filename;
                Log(log);
                pMSREG->load_bat(fp);
                fclose(fp);
                log.Format(_T("#### OK ####  load bat file done -- "));
                log = log + filename;
                Log(log);
            }
            delete(nstring0);
        }
        else {
            MessageBox(TEXT("Open file failed"), TEXT("Error"), MB_OK);
        }
    }
    return;
}


void CMainFrame::OnFileSaveRegs()
{
    // TODO: 在此添加命令处理程序代码
    CString comment = _T("");
    CString filename, extension, log;
    FILE *fp = NULL;
    CString chipname = pMSREG->get_chipname();
    chipname += _T("_saved");

#if MTOOLS_FOR_INTERNAL
    LPCTSTR szFilter = _T("Text File (*.txt)|*.txt|Batch File (*.bat)|*.bat|C File (*.c)|*.c|All Files (*.*)|*.*||");
    CFileDialog filedlg(false, _T("txt"), chipname, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilter, AfxGetMainWnd());
#else
    LPCTSTR szFilter = _T("Batch File (*.bat)|*.bat|All Files (*.*)|*.*||");
    CFileDialog filedlg(false, _T("txt"), chipname, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilter, AfxGetMainWnd());
#endif

    if (filedlg.DoModal() == IDOK) {
        filename = filedlg.GetPathName();
        extension = filedlg.GetFileExt();
        size_t origsize = wcslen(filename) + 1;
        size_t convertedChars = 0;
        const size_t newsize = origsize * 2;
        char *nstring = new char[newsize];
        wcstombs_s(&convertedChars, nstring, newsize, filename, _TRUNCATE);
        fopen_s(&fp, nstring, "wt+");
        delete(nstring);
        
        if (fp) {
            size_t origsize0 = wcslen(extension) + 1;
            size_t convertedChars0 = 0;
            const size_t newsize0 = origsize0 * 2;
            char *nstring0 = new char[newsize0];
            wcstombs_s(&convertedChars0, nstring0, newsize0, extension, _TRUNCATE);
            if (!_strcmpi(nstring0, "txt"))
            {
#if MTOOLS_FOR_INTERNAL
                log.Format(_T("save to txt file -- "));
                log = log + filename;
                Log(log);

                pMSREG->save_txt(fp);

                fclose(fp);
                log.Format(_T("save to txt file done -- "));
                log = log + filename;
                Log(log);
#endif
            }
            else if (!_strcmpi(nstring0, "bat"))
            {
                log.Format(_T("save to bat file -- "));
                log = log + filename;
                Log(log);

                pMSREG->save_bat(fp);

                fclose(fp);
                log.Format(_T("save to bat file done -- "));
                log = log + filename;
                Log(log);
            }

            delete(nstring0);
        }
        else {
            MessageBox(TEXT("Open file failed:" + filename), TEXT("Error"), MB_OK);
        }
    }
}

void CMainFrame::OnAfterInit()
{
  BOOL bNameValid;
  CString strToolBarName;
  bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
  ASSERT(bNameValid);
  m_wndToolBar.SetWindowText(strToolBarName);
}


void CMainFrame::OnMultiPages()
{
    mMultiDebugPageMode = !mMultiDebugPageMode;
}

void CMainFrame::OnUpdateMultiPages(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(mMultiDebugPageMode);
}
