/**
******************************************************************************
* @file    ms2160_app_config.h
* @author  
* @version V1.0.0
* @date    
* @brief   app config header files
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_APP_CONFIG_H__
#define __MACROSILICON_MS2160_APP_CONFIG_H__

/*
 *  ms2160 compiler platform config
 *  support:
 *  _PLATFORM_WINDOWS_, __KEIL_C__
 *  You can define in your compiler macro option
 */
#define _PLATFORM_WINDOWS_


 /*
 *  MS2160 i2c slave
 *  if define that means MS2160 act as an i2c slave(i8051 halt)
 *  else MS2160 act as an i2c master(i8051 is running)
 */
 #define MS2160_I2C_SLAVE


#define MS2160_FPGA_VERIFY          (1)
#define MS2160_DEBUGGING_LOG_ENABLE (1)

#endif  // __MACROSILICON_MS2160_APP_CONFIG_H__