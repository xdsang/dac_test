
#pragma once

enum E_CONFIG_TYPE {
  CFG_CONNECT_MODE,
  CFG_I2C_MODE,
  CFG_I2C_SPEED,
  CFG_NULL                      // alway in last one!
};

// configure type that need to save
class CPropertiesToolBar : public CMFCToolBar
{
public:
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CMFCPropertyGridPropertyButton :  public CMFCPropertyGridProperty
{
public:
  CMFCPropertyGridPropertyButton(const CString& strName, const _variant_t& varValue,
    LPCTSTR lpszDescr=NULL,DWORD_PTR dwData=0,
    LPCTSTR lpszEditMask=NULL,LPCTSTR lpszEditTemplate=NULL,
    LPCTSTR lpszValidChars=NULL) : CMFCPropertyGridProperty(strName, varValue, lpszDescr,
                                  dwData, lpszEditMask, lpszEditTemplate, lpszValidChars) {}
  CMFCPropertyGridPropertyButton(const CString& strGroupName,
                                 DWORD_PTR dwData=0,BOOL bIsValueList=FALSE )
    : CMFCPropertyGridProperty(strGroupName, dwData, bIsValueList) {}


  void OnClickName( CPoint /*point*/ );
  void OnClickButton(CPoint point);
  BOOL OnClickValue( UINT uiMsg, CPoint point );
};


class CIOCtrl;
class CPropertiesWnd : public CDockablePane
{
// 构造
public:
  CPropertiesWnd();

  void AdjustLayout();

// 特性
public:
  void SetVSDotNetLook(BOOL bSet)
  {
    m_wndPropList.SetVSDotNetLook(bSet);
    m_wndPropList.SetGroupNameFullWidth(bSet);
  }

protected:
  CFont m_fntPropList;
  CComboBox m_cboxConnectMode;
  CPropertiesToolBar m_wndToolBar;
  CMFCPropertyGridCtrl m_wndPropList;
  BOOL m_bExpAllPropList;
  CButton m_btnConnect;
  CStatic m_staticStatus;
  CButton m_btnRead;
  CButton m_btnWrite;
  CButton m_btnBit0;
  CButton m_btnBit1;
  CButton m_btnBit2;
  CButton m_btnBit3;
  CButton m_btnBit4;
  CButton m_btnBit5;
  CButton m_btnBit6;
  CButton m_btnBit7;
  CButton m_btnBitAll;
  CButton m_btnSave;
  CButton m_btnClear;
  CButton m_btnLogR;
  CButton m_btnLogW;
  CStatic m_lLogRW;
  BYTE   m_regValue;
  CStatic m_lRegAddr;
  CStatic m_lRegValue;
  CComboBox m_edRegAddr;
  CComboBox m_edRegValue;
  BYTE   m_Value;
  UINT16   m_RegAddr;
  CButton* m_pbtnArray[8] = { &m_btnBit0, &m_btnBit1, &m_btnBit2, &m_btnBit3,
    &m_btnBit4, &m_btnBit5, &m_btnBit6, &m_btnBit7};
  CIOCtrl* m_pI2cCtrl;               /* point object who control i2c */
// 实现
public:
  virtual ~CPropertiesWnd();
  void toggleCheckListBit(int bit);
  void SetCheckList(BYTE value);
  afx_msg void OnConnect();
protected:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnExpandAllProperties();
  afx_msg void OnUpdateExpandAllProperties(CCmdUI* pCmdUI);
  afx_msg void OnSortProperties();
  afx_msg void OnUpdateSortProperties(CCmdUI* pCmdUI);
  afx_msg void OnProperties1();
  afx_msg void OnUpdateProperties1(CCmdUI* pCmdUI);
  afx_msg void OnProperties2();
  afx_msg void OnUpdateProperties2(CCmdUI* pCmdUI);
  afx_msg void OnSetFocus(CWnd* pOldWnd);
  afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
  afx_msg void OnRead();
  afx_msg void OnWrite();
  afx_msg void OnBit0();
  afx_msg void OnBit1();
  afx_msg void OnBit2();
  afx_msg void OnBit3();
  afx_msg void OnBit4();
  afx_msg void OnBit5();
  afx_msg void OnBit6();
  afx_msg void OnBit7();
  afx_msg void OnBitAll();
  afx_msg void OnUpdateButton(CCmdUI *pCmdUI);
  afx_msg void OnLogR();
  afx_msg void OnLogW();
  afx_msg void OnClearLog();
  afx_msg void OnSaveLog();
  DECLARE_MESSAGE_MAP()

  void InitPropList();
  void SetPropListFont();

  int m_nComboHeight;
public:
  afx_msg void OnPaint();
  LRESULT OnPropertyChanged(__in WPARAM wparam, __in LPARAM lparam );
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg void OnRegAddrEditChange();
  afx_msg void OnSelRegAddrCombo();
  afx_msg void OnRegValEditChange();
  afx_msg void OnSelRegValCombo();
  afx_msg void OnConnectModeChange();
  afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
private:
  CFont m_labelFont;
  void saveConfigure(E_CONFIG_TYPE type, int value) const;
  void loadConfigure(void);
  void pushRegAddr2Comb(void);
  void pushRegValue2Comb(void);
};

