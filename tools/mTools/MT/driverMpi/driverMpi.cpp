#include "stdafx.h"
#include "MSLnkU.h"
#include "MT.h"
#include "MainFrm.h"

extern CMSLinkU *puLnk;
extern CMTApp theApp;

extern bool gFlagLogR;
extern bool gFlagLogW;

#ifdef MS_CHIP_ID
const int CHIP_ID = MS_CHIP_ID;
#else
const int CHIP_ID = 0xA8888A; // Please don't modify this default value.
#endif


//windows platform sdk mpi
void Log(CString log)
{
  CMainFrame* pMain = (CMainFrame*)AfxGetApp()->GetMainWnd();
  pMain->getOutputWnd()->LogWindowAppend(log);
}

void debugLog(CString log)
{
  CMainFrame* pMain = (CMainFrame*)AfxGetApp()->GetMainWnd();
  pMain->getOutputWnd()->DebugWindowAppend(log);
}

void modifyLatestLog(CString log)
{
	CMainFrame* pMain = (CMainFrame*)AfxGetApp()->GetMainWnd();
	pMain->getOutputWnd()->ModifyLatestLog(log);
}

//for HEX
void Log1(CString log, UINT16 u16_value)
{
  CString logx;
  logx.Format(_T("0x%x"), u16_value); 
  log += logx;  
  Log(log);
}

//for DEC
void Log2(CString log, UINT16 u16_value)
{
  CString logx;
  logx.Format(_T("%d"), u16_value); 
  log += logx;  
  Log(log);
}

//chip reset
VOID mculib_chip_reset(VOID)
{
  //none support
}

//delay_ms
void mculib_delay_ms(UINT16 u16_ms)
{
  Sleep(u16_ms);
  /*
    do
    {
        mculib_delay_us(250);
        mculib_delay_us(250);
        mculib_delay_us(250);
        mculib_delay_us(240);
        u16_ms--;
    } while (u16_ms);
    */
}

// delay_us
void mculib_delay_us(UINT16 us)
{
    //储存计数的联合
    LARGE_INTEGER fre;
    //获取硬件支持的高精度计数器的频率
    if (QueryPerformanceFrequency(&fre))
    {
        LARGE_INTEGER run, priv, curr;
        run.QuadPart = fre.QuadPart * us / 1000000;//转换为微妙级
        //获取高精度计数器数值
        QueryPerformanceCounter(&priv);
        do
        {
            QueryPerformanceCounter(&curr);
        } while (curr.QuadPart - priv.QuadPart < run.QuadPart);
    }
}


//i2c
UINT8 mculib_i2c_read_16bidx8bval(UINT8 u8_address, UINT16 u16_index)
{
  UINT8 u8_data;
	
  puLnk->i2cr16((int)u8_address, (int)u16_index, &u8_data);
  if (gFlagLogR) {
    CString str;
    str.Format(_T("i2cr16 %02X %04X <- %02X"), u8_address, u16_index, u8_data);
    Log(str);
  }
  return u8_data;
}


BOOL mculib_i2c_write_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT8 u8_value)
{
  if (gFlagLogW) {
    CString str; 
    str.Format(_T("i2cw16 %02X %04X %02X"), u8_address, u16_index, u8_value);
    Log(str);
  }
  return (BOOL)(puLnk->i2cw16((int)u8_address, (int)u16_index, (int)u8_value));
}

//printf log
void mculib_log(char* fmt, ...)
{
	char buf[1024];
	va_list argp;
	va_start(argp, fmt);
	vsprintf(buf, fmt, argp);
	va_end(argp);
	Log((CString)buf);
}

VOID mculib_uart_log(UINT8 *u8_string)
{
  Log((CString)u8_string);
}

VOID mculib_uart_log1(UINT8 *u8_string, UINT16 u16_hex)
{
  Log1((CString)u8_string, u16_hex);
}

VOID mculib_uart_log2(UINT8 *u8_string, UINT16 u16_dec)
{
  Log2((CString)u8_string, u16_dec);
}

BOOL mcu_connect(void)
{
#if MTOOLS_MCU_VERIFY
  BOOL result = FALSE;
  BOOL test_flag = FALSE;
  BYTE buf[10];
  int i;
  BYTE iValue;
  BYTE read_buf[2];
  BYTE cnt;
	
  for(cnt=0; cnt<3; cnt++)
  {
    srand( (unsigned) time(NULL));
			
    for(i=0; i<8; i++)
    {
      buf[i] = rand()%0x100;
      puLnk->i2cw(0x78, (int)i, (int)buf[i]);
    }
    puLnk->i2cw(0x78, 10, 0xBB);
    puLnk->i2cw(0x78, 9, 0x88);
    puLnk->i2cw(0x78, 8, 0x55);
			
    buf[8] = 8*buf[0] + 7*buf[1] + 6*buf[2] + 5*buf[3];
    buf[9] = 4*buf[4] + 3*buf[5] + 2*buf[6] + buf[7];
			
    puLnk->i2cr(0x78, 11, &read_buf[0]);
    puLnk->i2cr(0x78, 12, &read_buf[1]);
			
    if (buf[8] == read_buf[0] && buf[9] == read_buf[1])
    {
      result = TRUE;
      break;
    }			
  }
  
#else
  BOOL result = TRUE;
#endif
	
  return result;
}



bool isTargetChip()
{    
  BYTE temp_1, temp_2, temp_3;
  int id = 0;
  mculib_i2c_write_16bidx8bval(puLnk->GetI2CAddr(), 0xE001, 0x00);
  puLnk->i2cr16(puLnk->GetI2CAddr(), 0xF000, &temp_1);
  puLnk->i2cr16(puLnk->GetI2CAddr(), 0xF001, &temp_2); 
  puLnk->i2cr16(puLnk->GetI2CAddr(), 0xF002, &temp_3);
  id = temp_1 << 16 | temp_2 << 8 | temp_3;
  return (id == CHIP_ID);
}


VOID  mculib_i2c_burstread_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
  puLnk->i2cr16(u8_address, (WORD)u16_index, u16_length, pu8_value);
  if (gFlagLogR) {
      CString str;
      str.Format(_T("i2cr16_burst %02X %04X %04X <- [%02X %02X %02X %02X ...]"), u8_address, u16_index, u16_length, pu8_value[0], pu8_value[1], pu8_value[2], pu8_value[3]);
      Log(str);
  }
}


VOID  mculib_i2c_burstwrite_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
  puLnk->i2cw16(u8_address, (WORD)u16_index, u16_length, pu8_value);
  if (gFlagLogW) {
      CString str;
      str.Format(_T("i2cw16_burst %02X %04X %04X -> [%02X %02X %02X %02X ...]"), u8_address, u16_index, u16_length, pu8_value[0], pu8_value[1], pu8_value[2], pu8_value[3]);
      Log(str);
  }
}

VOID  mculib_i2c_set_speed(UINT8 u8_i2c_speed)
{
  
}

VOID  mculib_i2c_set_ddc(BOOL ddc)
{
}

UINT8 mculib_i2c_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index)
{
  UINT8 u8_data;
  puLnk->i2cr(u8_address, u8_index, &u8_data);
  if (gFlagLogR) {
      CString str;
      str.Format(_T("i2cr %02X %02X <- %02X"), u8_address, u8_index, u8_data);
      Log(str);
  }
  return u8_data;
}

BOOL  mculib_i2c_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value)
{
  BOOL ret;
  ret = (BOOL)(puLnk->i2cw(u8_address, u8_index, u8_value));

  if (gFlagLogW) {
      CString str;
      str.Format(_T("i2cw %02X %02X %02X"), u8_address, u8_index, u8_value);
      Log(str);
  }

  return ret;
}

VOID  mculib_i2c_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value)
{
  puLnk->i2cr(u8_address, u8_index, u8_length, pu8_value);
  if (gFlagLogR) {
      CString str;
      str.Format(_T("i2cw_burst %02X %02X %02X <- ..."), u8_address, u8_index, u8_length);
      Log(str);
}

  return;
}
