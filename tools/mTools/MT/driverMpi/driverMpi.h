#ifndef DRIVERMPI_H
#define DRIVERMPI_H

// Misc functions
bool isTargetChip();
// printf in info page
void Log(CString log);
void Log1(CString log, UINT16 u16_value);
void Log2(CString log, UINT16 u16_value);
// printf in debug page
void debugLog(CString log);
void modifyLatestLog(CString log);


// mculib APIs
void mculib_delay_ms (UINT16 u16_ms);
void mculib_delay_us (UINT16 u16_us);

VOID mculib_chip_reset (VOID);

UINT8 mculib_i2c_read_16bidx8bval (UINT8 u8_address,UINT16 u16_index);
BOOL mculib_i2c_write_16bidx8bval (UINT8 u8_address,UINT16 u16_index,UINT8 u8_value);

VOID mculib_log(char* fmt, ...);
VOID mculib_uart_log (UINT8* u8_string);
VOID mculib_uart_log1 (UINT8* u8_string,UINT16 u16_hex);
VOID mculib_uart_log2 (UINT8* u8_string,UINT16 u16_dec);

VOID  mculib_i2c_burstread_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);
VOID  mculib_i2c_burstwrite_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);
VOID  mculib_i2c_set_speed(UINT8 u8_i2c_speed);
VOID  mculib_i2c_set_ddc(BOOL ddc);
UINT8 mculib_i2c_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index);
BOOL  mculib_i2c_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value);
VOID  mculib_i2c_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);

#endif /* DRIVERMPI_H */
