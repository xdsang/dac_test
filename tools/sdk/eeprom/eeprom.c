/**
******************************************************************************
* @file    eeprom.c
* @author  
* @version V1.0.0
* @date   
* @brief   eeprom interface implementation
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "eeprom.h"

#define EEPROM_I2C_ADDR     (0xA0)

#define RAMCODE_START_ADDR  (0xC830)

#define EEPROM_START_ADDRESS  ((UINT16)0x0000)  //the start address of eeprom

BOOL  g_b_16bit_eeprom = FALSE;
UINT8 g_u8_eeprom_status = EEPROM_STATUSF_NOT_EXIST;
UINT8 g_u8_eeprom_header[2] = {0};
UINT8 g_u8_eeprom_opcode[43] = {0};


//helper declaration
static UINT8 _read_byte(UINT16 u16_index);
static VOID _write_byte(UINT16 u16_index, UINT8 u8_val);
static VOID _read(UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer);
static VOID _write(UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer);
static BOOL _check_header(VOID);
static VOID _check_status(VOID);
static VOID _load_ramcode(VOID);
static VOID _load_opcode(VOID);


//helper definition
static UINT8 _read_byte(UINT16 u16_index)
{
    if (g_b_16bit_eeprom)
    {
        //20150921, for 24c32/64/128 chip, flip high and low byte, because 16 bit eeprom address is diff to mscrosilicon chip,
        u16_index = (u16_index << 8) + (u16_index >> 8);
        return mculib_i2c_read_16bidx8bval(EEPROM_I2C_ADDR, u16_index);
    }
    else
    {
        //for 24c02/04/08/16
        return mculib_i2c_read_8bidx8bval(EEPROM_I2C_ADDR + (UINT8)((u16_index & 0xff00) >> 7), (UINT8)u16_index);
    }
}

static VOID _write_byte(UINT16 u16_index, UINT8 u8_val)
{
    if (g_b_16bit_eeprom)
    {
        //20150921, for 24c32/64/128 chip, flip high and low byte, because 16 bit eeprom address is diff to mscrosilicon chip,
        u16_index = (u16_index << 8) + (u16_index >> 8);
        mculib_i2c_write_16bidx8bval(EEPROM_I2C_ADDR, u16_index, u8_val);
    }
    else
    {
        //for 24c02/04/08/16
        mculib_i2c_write_8bidx8bval(EEPROM_I2C_ADDR + (UINT8)((u16_index & 0xff00) >> 7), (UINT8)u16_index, u8_val);
    }
}

static VOID _read(UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer)
{
    UINT8 u8_i;

    for (u8_i = 0; u8_i < u8_length; u8_i ++)
    {
        *(p_u8_buffer + u8_i) = _read_byte(u16_address + u8_i);
    }
}

static VOID _write(UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer)
{
    UINT16 u8_i;

    for (u8_i = 0; u8_i < u8_length; u8_i ++)
    {
        _write_byte(u16_address + u8_i, *(p_u8_buffer + u8_i));
        mculib_delay_ms(6);
    }
}


static BOOL _check_header(VOID)
{
    UINT16 u16_index = 0;

    _read(u16_index, 2, g_u8_eeprom_header);

    if(g_b_16bit_eeprom)
    {
        if ((0x69 == g_u8_eeprom_header[0]) &&  //24c32/64
            (0x96 == g_u8_eeprom_header[1]))
        {
            return TRUE;
        }
    }
    else
    {
        if ((0x5A == g_u8_eeprom_header[0]) && //24c01/2/4/8/16
            (0xA5 == g_u8_eeprom_header[1]))
        {
            return TRUE;
        }
    }

    return FALSE;
}

static VOID _check_status(VOID)
{
    g_b_16bit_eeprom = FALSE;

    if (_check_header())
    {
        g_u8_eeprom_status = EEPROM_STATUSF_EXIST_VALID;
        return;
    }

    g_b_16bit_eeprom = TRUE;
    
    if (_check_header())
    {
        g_u8_eeprom_status = EEPROM_STATUSF_EXIST_VALID;
        return;
    }
    
    g_u8_eeprom_status = EEPROM_STATUSF_NOT_EXIST;
}

static VOID _load_ramcode(VOID)
{
    UINT16 i = 0;
    UINT16 u16_len = 0;
    UINT8 __XDATA *p_u8_ramcode_addr = NULL;
    UINT8 u8_data[3] = {0};

    _read(EEPROM_ADDR_RAMCODE_INFO, 3, u8_data);
    
    u16_len = ((UINT16)u8_data[0] << 8) | u8_data[1];
    
    p_u8_ramcode_addr = RAMCODE_START_ADDR;

    for(i = 0; i < u16_len; i++)
    {
        *(p_u8_ramcode_addr + i) = _read_byte(EEPROM_ADDR_RAMCODE + i);
    }

    g_u8_eeprom_status |= u8_data[2];
}        

static VOID _load_opcode(VOID)
{
    UINT8  u8_data[43] = {0};
    UINT8  i = 0;
    
    _read(EEPROM_ADDR_OPCODE, 43, u8_data);
    
    // option code
    for (i = 0; i < 43; i++)
    {
        g_u8_eeprom_opcode[i] = u8_data[i];
    }
}


//export
VOID eeprom_init(VOID)
{ 
    mculib_i2c_init();

    _check_status();

    if (g_u8_eeprom_status == EEPROM_STATUSF_EXIST_VALID)
    {
        _load_ramcode();
        _load_opcode();
    }
}

UINT8 eeprom_get_status(VOID)
{
    return g_u8_eeprom_status;
}

UINT8* eeprom_get_opcode(VOID)
{
    return g_u8_eeprom_opcode;
}

VOID eeprom_read(BOOL b_16bit_eeprom, UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer)
{
    g_b_16bit_eeprom = b_16bit_eeprom;
    _read(u16_address, u8_length, p_u8_buffer);
}

VOID eeprom_write(BOOL b_16bit_eeprom, UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer)
{
    g_b_16bit_eeprom = b_16bit_eeprom;
    _write(u16_address, u8_length, p_u8_buffer);
}
