/**
******************************************************************************
* @file    eeprom.h
* @author  
* @version V1.0.0
* @date   
* @brief   eeprom interface declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _EEPROM_H_
#define _EEPROM_H_

#include "mculib.h"

#define EEPROM_STATUSF_NOT_EXIST   0  // eeprom not exist
#define EEPROM_STATUSF_EXIST_VALID 1  // eeprom exist and data valid

#define EEPROM_STATUSF_PATCH0_VALID 0x04 // patch_before_chip_init valid
#define EEPROM_STATUSF_PATCH1_VALID 0x08 // patch_after_chip_init valid
#define EEPROM_STATUSF_PATCH2_VALID 0x10 // patch_in_main_loop valid
#define EEPROM_STATUSF_PATCH3_VALID 0x20 // patch_for_usbtask valid
#define EEPROM_STATUSF_PATCH4_VALID 0x40 // patch_for_usbcmd valid
#define EEPROM_STATUSF_PATCH5_VALID 0x80 // patch_for_timer valid

#define EEPROM_ADDR_EETYPE         (UINT16)0 // 0x5A/0xA5, 0x69/0x96
#define EEPROM_ADDR_RAMCODE_INFO   (UINT16)2 // ramcode length, patch flag
#define EEPROM_ADDR_RAMCODE        (UINT16)48 // ramcode
#define EEPROM_ADDR_OPCODE         (UINT16)5 // option code

VOID eeprom_init(VOID);
UINT8 eeprom_get_status(VOID);
UINT8* eeprom_get_opcode(VOID);
VOID eeprom_read(BOOL b_16bit_eeprom, UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer);
VOID eeprom_write(BOOL b_16bit_eeprom, UINT16 u16_address, UINT8 u8_length, UINT8 *p_u8_buffer);

#endif //_EEPROM_H_

