/**
******************************************************************************
* @file    ms2160_drv_frc.c
* @author  
* @version V1.0.0
* @date    24-5-2018
* @brief   FRC module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_frc.h"

/*******************MODULE REGISTER DEFINITION BEGIN********************/
#define REG_FRC_CLK_ENABLE            (0xF004) //bit3
#define REG_FRC_RSTZ                  (0xF007) //bit2

#define REG_FRC_BASE                  (0xF240)

#define REG_FRC_CAP_CFG0              (REG_FRC_BASE + 0x0001)
#define REG_FRC_CAP_CFG1              (REG_FRC_BASE + 0x0002)
#define REG_FRC_CAP_FF_STATUS         (REG_FRC_BASE + 0x0003)
#define REG_FRC_CAP_BUFA_SAFE_L       (REG_FRC_BASE + 0x0004)
#define REG_FRC_CAP_BUFA_SAFE_M       (REG_FRC_BASE + 0x0005)
#define REG_FRC_CAP_BUFA_SAFE_H       (REG_FRC_BASE + 0x0006)
#define REG_FRC_CAP_BUFB_SAFE_L       (REG_FRC_BASE + 0x0007)
#define REG_FRC_CAP_BUFB_SAFE_M       (REG_FRC_BASE + 0x0008)
#define REG_FRC_CAP_BUFB_SAFE_H       (REG_FRC_BASE + 0x0009)
#define REG_FRC_RD_STATUS             (REG_FRC_BASE + 0x000a)
#define REG_FRC_PB_CFG0               (REG_FRC_BASE + 0x000b)
#define REG_FRC_PB_MASTER_FLAG        (REG_FRC_BASE + 0x000c)
#define REG_FRC_PB_GENERAL_FLAG       (REG_FRC_BASE + 0x000d)
#define REG_FRC_PB_CFG1               (REG_FRC_BASE + 0x000e)
#define REG_FRC_CAP_BUFA_START_L      (REG_FRC_BASE + 0x0011)
#define REG_FRC_CAP_BUFA_START_M      (REG_FRC_BASE + 0x0012)
#define REG_FRC_CAP_BUFA_START_H      (REG_FRC_BASE + 0x0013)
#define REG_FRC_CAP_BUFB_START_L      (REG_FRC_BASE + 0x0014)
#define REG_FRC_CAP_BUFB_START_M      (REG_FRC_BASE + 0x0015)
#define REG_FRC_CAP_BUFB_START_H      (REG_FRC_BASE + 0x0016)
#define REG_FRC_CAP_OFFSET_L          (REG_FRC_BASE + 0x0017)
#define REG_FRC_CAP_OFFSET_H          (REG_FRC_BASE + 0x0018)
#define REG_FRC_PB_FETCH_L            (REG_FRC_BASE + 0x0019)
#define REG_FRC_PB_FETCH_H            (REG_FRC_BASE + 0x001A)
#define REG_FRC_PB_VST_LINE           (REG_FRC_BASE + 0x001C)
#define REG_FRC_PB_VST_PIXEL          (REG_FRC_BASE + 0x001E)
#define REG_FRC_MIRROR_L              (REG_FRC_BASE + 0x0020)
#define REG_FRC_MIRROR_H              (REG_FRC_BASE + 0x0021)
#define REG_FRC_PB_SAFE_LINE_L        (REG_FRC_BASE + 0x0022)
#define REG_FRC_PB_SAFE_LINE_H        (REG_FRC_BASE + 0x0023)
#define REG_FRC_PB_BUFA_START_L       (REG_FRC_BASE + 0x0024)
#define REG_FRC_PB_BUFA_START_M       (REG_FRC_BASE + 0x0025)
#define REG_FRC_PB_BUFA_START_H       (REG_FRC_BASE + 0x0026)
#define REG_FRC_PB_BUFB_START_L       (REG_FRC_BASE + 0x0027)
#define REG_FRC_PB_BUFB_START_M       (REG_FRC_BASE + 0x0028)
#define REG_FRC_PB_BUFB_START_H       (REG_FRC_BASE + 0x0029)
#define REG_FRC_PB_OFFSET_L           (REG_FRC_BASE + 0x002A)
#define REG_FRC_PB_OFFSET_H           (REG_FRC_BASE + 0x002B)
#define REG_FRC_CAP_OFFSET_CTRL       (REG_FRC_BASE + 0x002C)
#define REG_FRC_CAP_DELAY_CTRL        (REG_FRC_BASE + 0x002D)
#define REG_FRC_CAP_FETCH_L           (REG_FRC_BASE + 0x002E)
#define REG_FRC_CAP_FETCH_H           (REG_FRC_BASE + 0x002F)
/*********************MODULE REGISTER DEFINITION END*********************/

// macro enum structure definitions
#define MEM_STORED_VSIZE_MAX  (576)

// variables definitions

// helper decalarations
static VOID _drv_frc_cap_en(BOOL b_enable);
static VOID _drv_frc_pb_en(BOOL b_enable);
static VOID _drv_frc_pb_reqoff(BOOL b_enable);
static VOID _drv_frc_cap_manual_freeze(BOOL b_enable);
static VOID _drv_frc_cap_field_en(BOOL b_enable);
static VOID _drv_frc_cap_field_invert(BOOL b_invert);
static VOID _drv_frc_cap_drop_1_field_en(BOOL b_enable);
static VOID _drv_frc_cap_db_buf_en(BOOL b_enable);
static VOID _drv_frc_pb_db_buf_en(BOOL b_enable);
static VOID _drv_frc_buf_a_st(UINT32 u32_buff_a_start);
static VOID _drv_frc_buf_b_st(UINT32 u32_buff_b_start);
static VOID _drv_frc_buf_a_sp(UINT32 u32_buff_a_stop);
static VOID _drv_frc_buf_b_sp(UINT32 u32_buff_b_stop);
static VOID _drv_frc_cap_safe_guard_en(BOOL b_enable);
static VOID _drv_frc_cap_bandwidth_config(UINT8 u8_cap_ff_status);
static VOID _drv_frc_pb_bandwidth_config(UINT8 u8_pb_master_value);
static BOOL _drv_frc_get_high_to_low(VOID);
static VOID _drv_frc_pb_config_up_to_down(BOOL b_enable);
static VOID _drv_frc_pb_config_h2l_test(BOOL b_enable);
static VOID _drv_frc_cap_config_h2l_test(BOOL b_enable);
static VOID _drv_frc_config_high_to_low(BOOL b_enable);
static VOID _drv_frc_cap_buf_sta_inv(BOOL b_enable);
static VOID _drv_frc_pb_buf_sta_inv(BOOL b_enable);
static UINT16 _drv_frc_cap_get_offset(VOID);
static UINT32 _drv_frc_get_buf_a_st(VOID);
static UINT32 _drv_frc_get_buf_b_st(VOID);
static VOID _drv_frc_pb_shift_in_line(UINT16 u16_shift_lines);
static VOID _drv_frc_pb_shift_in_pixel(UINT16 u16_shift_pixels);
static VOID _drv_frc_set_input_signal_delay(UINT8 u8_delay);
static VOID _drv_frc_block_transmit_mode_en(BOOL b_enable);
static VOID _drv_frc_pack_stop_mode_sel(BOOL b_enable);



// helper definitions
static VOID _drv_frc_cap_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG0, MSRT_BIT0, b_enable);
}

static VOID _drv_frc_pb_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG0, MSRT_BIT7, b_enable);
}

static VOID _drv_frc_pb_reqoff(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG1, MSRT_BIT3, b_enable);
}

static VOID _drv_frc_cap_manual_freeze(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG0, MSRT_BIT4, b_enable);
}

static VOID _drv_frc_cap_field_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG1, MSRT_BIT4, b_enable);
}

static VOID _drv_frc_cap_field_invert(BOOL b_invert)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG1, MSRT_BIT5, b_invert);
}

static VOID _drv_frc_cap_drop_1_field_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG1, MSRT_BIT6, b_enable);
}

static VOID _drv_frc_cap_db_buf_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG0, MSRT_BIT3, b_enable);
}

static VOID _drv_frc_pb_db_buf_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG0, MSRT_BIT5, b_enable);
}

static VOID _drv_frc_buf_a_st(UINT32 u32_buff_a_start)
{
    UINT8 u8_buff_a_start_h = (UINT8)(u32_buff_a_start >> 16) & 0x3f;
    UINT16 u16_u8_buff_a_start_l = (UINT16)u32_buff_a_start;

    //cap buffA start
    HAL_WriteWord(REG_FRC_CAP_BUFA_START_L, u16_u8_buff_a_start_l);
    HAL_WriteByte(REG_FRC_CAP_BUFA_START_H, u8_buff_a_start_h);

    //pb buffA start
    HAL_WriteWord(REG_FRC_PB_BUFA_START_L, u16_u8_buff_a_start_l);
    HAL_WriteByte(REG_FRC_PB_BUFA_START_H, u8_buff_a_start_h);
}

static VOID _drv_frc_buf_b_st(UINT32 u32_buff_b_start)
{
    UINT8 u8_buff_b_start_h = (UINT8)(u32_buff_b_start >> 16) & 0x3f;
    UINT16 u16_u8_buff_b_start_l = (UINT16)u32_buff_b_start;

    //cap buffB start
    HAL_WriteWord(REG_FRC_CAP_BUFB_START_L, u16_u8_buff_b_start_l);
    HAL_WriteByte(REG_FRC_CAP_BUFB_START_H, u8_buff_b_start_h);

    //pb buffB start
    HAL_WriteWord(REG_FRC_PB_BUFB_START_L, u16_u8_buff_b_start_l);
    HAL_WriteByte(REG_FRC_PB_BUFB_START_H, u8_buff_b_start_h);
}

static VOID _drv_frc_buf_a_sp(UINT32 u32_buff_a_stop)
{
    //cap buffA stop
    HAL_WriteWord(REG_FRC_CAP_BUFA_SAFE_L, (UINT16)u32_buff_a_stop);
    HAL_WriteByte(REG_FRC_CAP_BUFA_SAFE_H, (UINT8)(u32_buff_a_stop >> 16) & 0x3f);
}

static VOID _drv_frc_buf_b_sp(UINT32 u32_buff_b_stop)
{
    //cap buffB stop
    HAL_WriteWord(REG_FRC_CAP_BUFB_SAFE_L, (UINT16)u32_buff_b_stop);
    HAL_WriteByte(REG_FRC_CAP_BUFB_SAFE_H, (UINT8)(u32_buff_b_stop >> 16) & 0x3f);
}

static VOID _drv_frc_cap_safe_guard_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG0, MSRT_BIT5, b_enable);
}

static VOID _drv_frc_cap_bandwidth_config(UINT8 u8_cap_ff_status)
{
    HAL_WriteByte(REG_FRC_CAP_FF_STATUS, u8_cap_ff_status);
}

static VOID _drv_frc_pb_bandwidth_config(UINT8 u8_pb_master_value)
{
    HAL_WriteByte(REG_FRC_PB_MASTER_FLAG, u8_pb_master_value);
}

static BOOL _drv_frc_get_high_to_low(VOID)
{
    return ((HAL_ReadByte(REG_FRC_PB_CFG1) & MSRT_BIT1) ? TRUE : FALSE);
}

static VOID _drv_frc_pb_config_up_to_down(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG1, MSRT_BIT1, b_enable);
}

static VOID _drv_frc_pb_config_h2l_test(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG1, MSRT_BIT5, b_enable);
}

static VOID _drv_frc_cap_config_h2l_test(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG1, MSRT_BIT7, b_enable);
}

static VOID _drv_frc_config_high_to_low(BOOL b_enable)
{
    _drv_frc_pb_config_up_to_down(b_enable);
    _drv_frc_pb_config_h2l_test(b_enable);
    _drv_frc_cap_config_h2l_test(b_enable);
}


static VOID _drv_frc_cap_buf_sta_inv(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG0, MSRT_BIT2, b_enable);
}


static VOID _drv_frc_pb_buf_sta_inv(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_PB_CFG1, MSRT_BIT0, b_enable);
}


static UINT16 _drv_frc_cap_get_offset(VOID)
{
    UINT16 u16_offset = HAL_ReadByte(REG_FRC_CAP_OFFSET_L);
    u16_offset |= (((UINT16)HAL_ReadByte(REG_FRC_CAP_OFFSET_H) & MSRT_BITS2_0) << 8);
    
    return u16_offset;
}

static UINT32 _drv_frc_get_buf_a_st(VOID)
{
    UINT32 u32_buff_a_st = HAL_ReadWord(REG_FRC_CAP_BUFA_START_L);
    u32_buff_a_st |= (((UINT32)HAL_ReadByte(REG_FRC_CAP_BUFA_START_H) & 0x3f) << 16);

    return u32_buff_a_st;
}

static UINT32 _drv_frc_get_buf_b_st(VOID)
{
    UINT32 u32_buff_b_st = HAL_ReadWord(REG_FRC_CAP_BUFB_START_L);
    u32_buff_b_st |= (((UINT32)HAL_ReadByte(REG_FRC_CAP_BUFB_START_H) & 0x3f) << 16);
    
    return u32_buff_b_st;
}

static VOID _drv_frc_pb_shift_in_line(UINT16 u16_shift_lines)
{
    HAL_WriteWord(REG_FRC_PB_VST_LINE, u16_shift_lines & 0x07ff);
}

static VOID _drv_frc_pb_shift_in_pixel(UINT16 u16_shift_pixels)
{
    HAL_WriteWord(REG_FRC_PB_VST_PIXEL, u16_shift_pixels & 0x0fff);
}

static VOID _drv_frc_set_input_signal_delay(UINT8 u8_delay)
{
    u8_delay = u8_delay > 3 ? 3 : u8_delay;

    HAL_ModBits(REG_FRC_CAP_DELAY_CTRL, MSRT_BITS5_4, u8_delay << 4);
    HAL_ModBits(REG_FRC_CAP_DELAY_CTRL, MSRT_BITS3_2, u8_delay << 2);
}

static VOID _drv_frc_block_transmit_mode_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_OFFSET_CTRL, MSRT_BIT5, !b_enable);
}

static VOID _drv_frc_pack_stop_mode_sel(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_OFFSET_CTRL, MSRT_BIT1, b_enable);
}



//exported
VOID ms2160drv_frc_clk_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_FRC_CLK_ENABLE, MSRT_BIT3, b_on);
}


VOID ms2160drv_frc_reset_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_FRC_RSTZ, MSRT_BIT2, !b_on); 
}


VOID ms2160drv_frc_pb_field_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_FRC_PB_CFG0, MSRT_BIT4, b_on);
}


VOID ms2160drv_frc_cap_freeze(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_CFG1, MSRT_BIT3, b_enable);
}


VOID ms2160drv_frc_set_offset_fetch(UINT16 u16_in_hactive, UINT16 u16_mem_hactive, UINT8 u8_video_in_color_space)
{
    UINT16 u16_line_offset  = 0;
    UINT16 u16_cap_fetch_number = 0;
    UINT16 u16_pb_fetch_number = 0;

    //calc the line offset and fetch number according to the input info.
    switch(u8_video_in_color_space)
    {
    case COLOR_SPACE_RGB888:
    case COLOR_SPACE_YUV444:
        u16_cap_fetch_number = (u16_in_hactive * 3 + 3) / 4;
        u16_pb_fetch_number = (u16_mem_hactive * 3 + 3) / 4;
        u16_line_offset = (u16_in_hactive + 3) / 4 * 3;
        break;
    default:   
        u16_cap_fetch_number = (u16_in_hactive * 2 + 3) / 4;
        u16_pb_fetch_number = (u16_mem_hactive * 2 + 3) / 4;
        u16_line_offset = (u16_in_hactive + 1) / 2;
        break;
    }

    //config. line offset and fetch number
    HAL_WriteWord(REG_FRC_CAP_OFFSET_L, u16_line_offset);
    HAL_WriteWord(REG_FRC_PB_OFFSET_L, u16_line_offset);
    HAL_WriteWord(REG_FRC_CAP_FETCH_L, u16_cap_fetch_number);
    HAL_WriteWord(REG_FRC_PB_FETCH_L, u16_pb_fetch_number);
}


VOID ms2160drv_frc_vfreq_change(UINT16 u16_in_v_freq, UINT16 u16_out_v_freq)
{
    if(u16_in_v_freq > u16_out_v_freq)
    {
        _drv_frc_config_high_to_low(TRUE);
    }
    else
    {
        _drv_frc_config_high_to_low(FALSE);
    }
}


VOID ms2160drv_frc_hmirror_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_FRC_MIRROR_H, MSRT_BIT3, b_on);
}


VOID ms2160drv_frc_vmirror_enable(VIDEOTIMING_T *pst_in_timing, VIDEOTIMING_T *pst_out_timing)
{
    UINT16 u16_mem_vsize = 0;
    UINT16 u16_v_last_line_num = 0;
    UINT16 u16_safe_line_num = 0;

    //set v_last_line_num
    if (pst_in_timing->u16_vactive >= pst_out_timing->u16_vactive)
    {
        u16_mem_vsize = pst_out_timing->u16_vactive;
    }
    else
    {
        u16_mem_vsize = pst_in_timing->u16_vactive;
    }

    if (u16_mem_vsize > MEM_STORED_VSIZE_MAX)
    {
        u16_mem_vsize = MEM_STORED_VSIZE_MAX;
    }

    u16_v_last_line_num = u16_mem_vsize - 1;

    HAL_WriteByte(REG_FRC_MIRROR_L, (UINT8)u16_v_last_line_num);
    HAL_ModBits(REG_FRC_MIRROR_H, MSRT_BITS2_0, (UINT8)(u16_v_last_line_num >> 8 & 0x07));
    
    //clear pb_h2l_test and set safe_line_num under frame rate high to low conversion case    
    if(_drv_frc_get_high_to_low())
    {
        HAL_ClrBits(REG_FRC_PB_CFG1, MSRT_BIT5);

        //set safe_line_num
        u16_safe_line_num = (UINT16)(1.1f * pst_out_timing->u16_vactive - (pst_out_timing->u16_vtotal * pst_out_timing->u16_vfreq) / (pst_in_timing->u16_vtotal * pst_in_timing->u16_vfreq) * pst_out_timing->u16_vtotal * pst_in_timing->u16_vactive);
        HAL_WriteWord(REG_FRC_PB_SAFE_LINE_L, u16_safe_line_num & 0x07ff);
    }

    //enable v mirror
    HAL_SetBits(REG_FRC_MIRROR_H, MSRT_BIT4);

}


VOID ms2160drv_frc_vmirror_disable(VOID)
{    
    if(_drv_frc_get_high_to_low())
    {
        HAL_SetBits(REG_FRC_PB_CFG1, MSRT_BIT5);
    }

    HAL_ClrBits(REG_FRC_MIRROR_H, MSRT_BIT4);
}


VOID ms2160drv_frc_multipic_ready(VOID)
{
    UINT32 u32_buffa_st  = _drv_frc_get_buf_a_st();
    UINT32 u32_buffb_st  = _drv_frc_get_buf_b_st();
    BOOL b_pb_up_dow_sel = _drv_frc_get_high_to_low();

    if (b_pb_up_dow_sel == FALSE) //input v freq <= output v freq, pb depends on cap
    {        
       _drv_frc_cap_db_buf_en(FALSE); //cap point to buffer A, pb point to buffer B
       _drv_frc_pb_db_buf_en(TRUE);
    }
    else
    {
       _drv_frc_pb_db_buf_en(FALSE);  //input v freq > output v freq, cap depends on pb
       _drv_frc_cap_db_buf_en(TRUE);  //pb point to buffer A, cap point to buffer B
    }

    _drv_frc_buf_b_st(u32_buffa_st); //buffer B point to same start address of buffer A
    _drv_frc_cap_manual_freeze(TRUE);
}


VOID ms2160drv_frc_multipic_active(UINT16 u16_h_start, UINT16 u16_v_start)
{
    UINT32 u32_buff_a_start = 0;
    UINT32 u32_buff_b_start = 0;
    UINT16 u16_frc_offset = 0;
    BOOL b_pb_up_dow_sel = _drv_frc_get_high_to_low();
    UINT32 u32_buffb_st  = _drv_frc_get_buf_b_st();
    UINT32 u32_buffa_st  = _drv_frc_get_buf_a_st();

    u16_frc_offset = _drv_frc_cap_get_offset();

    if (b_pb_up_dow_sel == FALSE) //cap point to buffer A, pb point to buffer B
    { 

        u32_buff_a_start = u32_buffb_st + (UINT32)u16_v_start * u16_frc_offset * 2 + u16_h_start / 2;

        ms2160drv_frc_cap_freeze(TRUE);
        _drv_frc_buf_a_st(u32_buff_a_start);
        ms2160drv_frc_cap_freeze(FALSE);
    }
    else  //pb point to buffer A, cap point to buffer B
    {
        u32_buff_b_start = u32_buffa_st + (UINT32)u16_v_start * u16_frc_offset * 2 + u16_h_start / 2;
        ms2160drv_frc_cap_freeze(TRUE);
        _drv_frc_buf_b_st(u32_buff_b_start);
        ms2160drv_frc_cap_freeze(FALSE);
    }
}

VOID ms2160drv_frc_block_transmit_mode_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_FRC_CAP_OFFSET_CTRL, MSRT_BIT5, !b_enable);
}

VOID ms2160drv_frc_config(UINT8 u8_sdram_size)
{
    //disable frc
    _drv_frc_cap_en(FALSE);
    _drv_frc_pb_en(FALSE);

    //frc buffer allocation, enable cap safeguard
    if(SDRAM_SIZE_2M == u8_sdram_size) //sdram size 2MBytes(1Mx16bits)
    {
        _drv_frc_buf_a_st(0);
        _drv_frc_buf_a_sp((UINT32)0x3FFFF);
        _drv_frc_buf_b_st((UINT32)0x40000);
        _drv_frc_buf_b_sp((UINT32)0x7FFFF);
    }
    else if(SDRAM_SIZE_4M == u8_sdram_size) //sdram size 8MBytes(2Mx32bits)
    {
        _drv_frc_buf_a_st(0);
        _drv_frc_buf_a_sp((UINT32)0x7FFFF);
        _drv_frc_buf_b_st((UINT32)0x80000);
        _drv_frc_buf_b_sp((UINT32)0xFFFFF);
    }
    else if(SDRAM_SIZE_8M == u8_sdram_size)
    {
        _drv_frc_buf_a_st(0);
        _drv_frc_buf_a_sp((UINT32)0xFFFFF);
        _drv_frc_buf_b_st((UINT32)0x100000);
        _drv_frc_buf_b_sp((UINT32)0x1FFFFF);
    }
    else //sdram size 16MBytes(4Mx32bits)
    {
        _drv_frc_buf_a_st(0);
        _drv_frc_buf_a_sp((UINT32)0x1FFFFF);
        _drv_frc_buf_b_st((UINT32)0x200000);
        _drv_frc_buf_b_sp((UINT32)0x3FFFFF);
    }
    _drv_frc_cap_safe_guard_en(TRUE);

    //enable frc double buffer mode
    _drv_frc_cap_db_buf_en(TRUE);
    _drv_frc_pb_db_buf_en(TRUE);

    //disable frc buffer status inversion
    _drv_frc_cap_buf_sta_inv(FALSE);
    _drv_frc_pb_buf_sta_inv(FALSE);

    //frc fifo threshold config.(bandwidth)
    _drv_frc_cap_bandwidth_config(0x10);
    _drv_frc_pb_bandwidth_config(0x30);

    //pb video shift config.
    _drv_frc_pb_shift_in_line(0);
    _drv_frc_pb_shift_in_pixel(0);

    //frc interlace mode config.(disable)
    _drv_frc_cap_field_en(FALSE);
    _drv_frc_cap_field_invert(FALSE);
    _drv_frc_cap_drop_1_field_en(FALSE);
    ms2160drv_frc_pb_field_switch(FALSE);

    //enable frc's sdram access request to dramc
    _drv_frc_pb_reqoff(FALSE);
    _drv_frc_cap_manual_freeze(FALSE);
    ms2160drv_frc_cap_freeze(FALSE);

    //frc input signal delay, transmit mode(frame or block) config.
    _drv_frc_set_input_signal_delay(3);
    _drv_frc_block_transmit_mode_en(FALSE);

    //pack stop mode selection: mode1(as long as cap and pb in the same buffer)
    _drv_frc_pack_stop_mode_sel(0);

    //enable frc
    _drv_frc_cap_en(TRUE);
    _drv_frc_pb_en(TRUE);
}
