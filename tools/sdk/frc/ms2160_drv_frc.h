/**
******************************************************************************
* @file    ms2160_drv_frc.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   FRC module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_FRC_H__
#define __MACROSILICON_MS2160_DRV_FRC_H__

// driver interface for macro enum structure declarations

#ifdef __cplusplus
extern "C" {
#endif

VOID ms2160drv_frc_clk_switch(BOOL b_on);



/***************************************************************
*  Function name:   ms2160drv_frc_reset
*  Description:     Reset the capture and playback module
*  Entry:           [IN]b_reset
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_frc_reset_switch(BOOL b_on);



/***************************************************************
*  Function name:   ms2160drv_frc_pb_field_en
*  Description:     PlayBack Interlace mode output enable
*  Entry:           [IN]b_enable: TRUE when output video format is interlace
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_frc_pb_field_switch(BOOL b_on);



/***************************************************************
*  Function name:   ms2160drv_frc_cap_freeze
*  Description:     freeze the capture 
*  Entry:           [IN]b_enable
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_frc_cap_freeze(BOOL b_enable);



/***************************************************************
*  Function name:   ms2160drv_frc_set_offset_fetch
*  Description:     Set frc offset and fetch num by input line num
*  Entry:           [IN]u16_in_hactive, u16_mem_hactive, ENUM_FRC_INPUT_COLOR_SPACE
*                   
* 
*  Returned value:  None
*  Remark:             
***************************************************************/
VOID ms2160drv_frc_set_offset_fetch(UINT16 u16_in_hactive, UINT16 u16_mem_hactive, UINT8 u8_video_in_color_space);


/***************************************************************
*  Function name:   ms2160drv_frc_vfreq_change
*  Description:     Set register pb_up_dow_rbuf_sel while v frequence change
*  Entry:           [IN]u16_in_v_freq
*                   [IN]u16_out_v_freq
* 
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_frc_vfreq_change(UINT16 u16_in_v_freq, UINT16 u16_out_v_freq);

VOID ms2160drv_frc_hmirror_switch(BOOL b_on);

VOID ms2160drv_frc_vmirror_enable(VIDEOTIMING_T *pst_in_timing, VIDEOTIMING_T *pst_out_timing);

VOID ms2160drv_frc_vmirror_disable(VOID);

VOID ms2160drv_frc_multipic_ready(VOID);

VOID ms2160drv_frc_multipic_active(UINT16 u16_h_start, UINT16 u16_v_start);

VOID ms2160drv_frc_block_transmit_mode_en(BOOL b_enable);

VOID ms2160drv_frc_config(UINT8 u8_sdram_size);


#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_FRC_H__
