/******************************************************************           
*******************************************************************/
#include "common.h"
#include "SCSI.h"
//#include "FAT.h"
#include "hardware.h"
#include "fifo.h"
#include "flash.h"

//定义端点2最大包长度为64字节
//#define EP2_SIZE 64
#define EP2_SIZE 512

uint8 CBW[31];  //保存CBW用的缓冲区
uint8 CSW[13];  //保存CSW用的缓冲区

//处理端点2数据的缓冲区
//uint8 xdata Ep2Buffer[EP2_SIZE];

uint32 ByteAddr;  //字节地址


//INQUIRY命令需要返回的数据
//请对照书中INQUIRY命令响应数据格式
uint8 DiskInf[36]=
{
    0x05,  //   0x05,CD 设备声明 00 磁盘设备      
    0x80,//可以移除
    0x00,//VERSIONS  00The device does not claim conformance to any standard.   02h  Obsolete          
    0x01,//数据响应格式   01  SCSI       02 Obsolete 老式的
    0x1f,
    0x00, 
    0x00,0x00, 
    'H','j','w','d','z',0x20,0x20,0x20,  //厂商标识   8  hjwdz
    'M','S','2','1','6','0',0x20,0x20,       // 产品标识  16   MAXHUB
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x31,0x2E,0x30,0x31 //产品版本号，为1.01
};

//READ_FORMAT_CAPACITIES命令需要返回的数据
//请对照书中READ_FORMAT_CAPACITIES命令响应数据格式
uint8 MaximumCapacity[12]=
{
 0x00, 0x00, 0x00, //保留
 0x08,  //容量列表长度
// 0x01, 0x00, 0x00, 0x00,  //块数(最大支持8GB)
 0x00, 0x00, 0x40, 0x00,  //块数(最大支持8GB)
 0x03, //描述符代码为3，表示最大支持的格式化容量
 (UDISK_SECTOR_SIZE>>16),(UDISK_SECTOR_SIZE>>8),UDISK_SECTOR_SIZE  //每块大小为512字节
};



//REQUEST SENSE命令需要返回的数据，这里固定为无效命令
//请参看书总数据结构的解释
uint8 SenseData[18]=
{
 0x70, //错误代码，固定为0x70
 0x00, //保留
 0x05, //Sense Key为0x05，表示无效请求（ILLEGAL REQUEST）
 0x00, 0x00, 0x00, 0x00, //Information为0
 0x0A, //附加数据长度为10字节
 0x00, 0x00, 0x00, 0x00, //保留
 0x20, //Additional Sense Code(ASC)为0x20，表示无效命令操作码（INVALID COMMAND OPERATION CODE）
 0x00, //Additional Sense Code Qualifier(ASCQ)为0
 0x00, 0x00, 0x00, 0x00 //保留
};

code uint8 read_toc_data[20]=
{
    0x00, 
    0x12, 
    0x01, 
    0x01, 
    0x00, 
    0x16,   //(adr, control)
    0x01,   //(track being described)
    0x00,   //(reserved)
    0x00, 0x00, 0x02, 0x00, //(start logical block address 512)
    0x00,  //(reserved)
    0x16,  //(adr, control)
    0xaa,  //(track being described (leadout))
    0x00,   //(reserved)
    0x00, 0x00, 0x22, 0x34 //(start logical block address 8756)
}    ;


code uint8 mode_sense6[]=
{
    0x0c,  //length sizeof( struct mode_sense6_data )
    0x05,   //media_type 0x05 for CDROM, 0x00 for U disk
    0x0,   //0x00
    0x08,  //block_descriptor_len  sizeof( block_last ) + sizeof( block_len )
    0x00, 0x00, 0x00, 0x00, //block_last
    0x00, 0x00, 0x00, 0x00 //block_len
};

code uint8 mode_sense10[]=
{
    0x00,  //length sizeof( struct mode_sense10_data )
    0x05,   //media_type 0x05 for CDROM, 0x00 for U disk
    0x00,   //write_protect  0x00
    0x00,0x00,  //reserved
    0x00,0x08,  //block_descriptor_len
    0x00, 0x00, 0x00, 0x00, //block_last
    0x00, 0x00, 0x00, 0x00 //block_len
};



/********************************************************************
函数功能：设置sense数据为无效命令。
入口参数：无。
返    回：无。
备    注：无。
********************************************************************/
void SetSenseDataInvalidCommand(void)
{
 uint8 i;
 for(i=0;i<18;i++)
 {
  SenseData[i]=0;
 }
 SenseData[0]=0x70; //错误代码，固定为0x70
 SenseData[2]=0x05; //Sense Key为0x05，表示无效请求（ILLEGAL REQUEST）
 SenseData[7]=0x0A; //附加数据长度为10字节
 SenseData[12]=0x20; //Additional Sense Code(ASC)为0x20，表示无效命令操作码（INVALID COMMAND OPERATION CODE）
}
/////////////////////////End of function/////////////////////////////

/********************************************************************
函数功能：设置sense数据为无错误。
入口参数：无。
返    回：无。
备    注：无。
********************************************************************/
void SetSenseDataNoSense(void)
{
 uint8 i;
 for(i=0;i<18;i++)
 {
  SenseData[i]=0;
 }
 SenseData[0]=0x70; //错误代码，固定为0x70
 SenseData[2]=0x00; //Sense Key为0x00，表示无错误(NO SENSE)
 SenseData[7]=0x0A; //附加数据长度为10字节
 SenseData[12]=0x00; //Additional Sense Code(ASC)为0
}
/////////////////////////End of function/////////////////////////////

uint8 *pEp2SendData;
uint32 Ep2DataLength;

/********************************************************************
函数功能：从CBW中获取传输数据的字节数。
入口参数：无。
返    回：需要传输的字节数。
备    注：无。
********************************************************************/
uint32 GetDataTransferLength(void)
{
 uint32 Len; 
 //CBW[8]~CBW[11]为传输长度（小端结构）
 Len=CBW[11];
 Len=Len*256+CBW[10];
 Len=Len*256+CBW[9];
 Len=Len*256+CBW[8];
 return Len;
}
////////////////////////End of function//////////////////////////////

/********************************************************************
函数功能：从CBW中获取逻辑块地址LBA的字节数。
入口参数：无。
返    回：逻辑块地址LBA。
备    注：无。
********************************************************************/
uint32 GetLba(void)
{
 uint32 Lba; 
 //读和写命令时，CBW[17]~CBW[20]为逻辑块地址（大端结构） 
 Lba=CBW[17];
 Lba=Lba*256+CBW[18];
 Lba=Lba*256+CBW[19];
 Lba=Lba*256+CBW[20];
 return Lba;
}
////////////////////////End of function//////////////////////////////

/********************************************************************
函数功能：填充CSW。
入口参数：Residue：剩余字节数；Status：命令执行的状态。
返    回：无。
备    注：无。
********************************************************************/
void SetCsw(uint32 Residue, uint8 Status)
{
 //设置CSW的签名，其实可以不用每次都设置的，
 //开始初始化设置一次就行了，这里每次都设置
 CSW[0]='U';
 CSW[1]='S';
 CSW[2]='B';
 CSW[3]='S';
 
 //复制dCBWTag到CSW的dCSWTag中去
 CSW[4]=CBW[4];
 CSW[5]=CBW[5];
 CSW[6]=CBW[6];
 CSW[7]=CBW[7];
    
 //剩余字节数
 CSW[8]=Residue&0xFF;
 CSW[9]=(Residue>>8)&0xFF;
 CSW[10]=(Residue>>16)&0xFF;
 CSW[11]=(Residue>>24)&0xFF;
 
 //命令执行的状态，0表示成功，1表示失败。
 CSW[12]=Status;
}
////////////////////////End of function//////////////////////////////

///********************************************************************
//函数功能：获取磁盘数据函数。
//入口参数：无。
//返    回：无。
//备    注：无。
//********************************************************************/
//void GetDiskData(void)
//{
// //判断该返回什么数据
// if(ByteAddr==0) pEp2SendData=Dbr; //返回DBR
// if(ByteAddr==512) pEp2SendData=Fat; //返回FAT
// if((ByteAddr>=576)&&(ByteAddr<16896)) pEp2SendData=Zeros;
// if(ByteAddr==16896) pEp2SendData=Fat; //返回FAT（备份FAT）   33
// if((ByteAddr>=16960)&&(ByteAddr<33280)) pEp2SendData=Zeros;
// if(ByteAddr==33280) pEp2SendData=RootDir; //返回根目录       65  = 32*2+1
// if((ByteAddr>=33344)&&(ByteAddr<49664)) pEp2SendData=Zeros;
// if(ByteAddr==49664) pEp2SendData=TestFileData; //返回文件数据
// if(ByteAddr>50175) pEp2SendData=Zeros;
// ByteAddr+=EP2_SIZE; //调整字节地址，每次发送最大包长度的数据
//}
//void GetDiskData_default(void)
//{
// //判断该返回什么数据
// if(ByteAddr==0)
// {
//   pEp2SendData=Dbr; //返回DBR
// } 
// else if(ByteAddr==512)
// {
//    pEp2SendData=Fat; //返回FAT
// } 
// else if((ByteAddr>=576)&&(ByteAddr<8704)) 
//  {
//     pEp2SendData=Zeros;
// } 
// else if(ByteAddr==8704)         //0x11 * 512    17*512
//  {
//     pEp2SendData=RootDir; //返回FAT（备份FAT）
// } 
// else if((ByteAddr>=8768)&&(ByteAddr<25088))
//  {
//      pEp2SendData=Zeros;
// } 
// else if(ByteAddr==25088) //0x31 * 512
//  {
//    pEp2SendData=TestFileData; //返回文件数据
// } 
// else if(ByteAddr >55088) //0x31 * 512
//  {
//     pEp2SendData=Zeros;
// } 
// ByteAddr+=EP2_SIZE; //调整字节地址，每次发送最大包长度的数据
//}

void GetDiskData(void)  // 每次获取512 Bytes
{
 if(SPI_FLASH_TYPE!=0)
 {
    SPI_Flash_Read_MS2160(ByteAddr+0x2000,512);    // Flash最前面8k 代码作为 程序补丁以及Option使用
    pEp2SendData=Flashbuffer;
    ByteAddr+=EP2_SIZE; //调整字节地址，每次发送最大包长度的数据
 }
// else
// {
//   GetDiskData_default();
// }
}

/********************************************************************
函数功能：将数据通过端点2发送。
入口参数：无。
返    回：无。
备    注：当发送数据长度为0，并且处于数据阶段时，将自动发送CSW。
********************************************************************/
void Ep2SendData(void)
{               
    reg_usb_Index=2;    
    if(Ep2DataLength==0) //如果需要发送的数据长度为0
    {
        if(TransportStage==DATA_STAGE) //并且处于数据阶段
        {
            //则直接进入状态阶段
            TransportStage=STATUS_STAGE;
            Ep2DataLength=sizeof(CSW); //数据长度为CSW的大小
            pEp2SendData=CSW; //返回的数据为CSW
            EP2DataIn((u16)Ep2DataLength,pEp2SendData);
            Ep2DataLength=0;  //传输长度为0 下次要传送的数据长度
            return;
        }
        else
        {  
            TransportStage=COMMAND_STAGE;  //进入到命令阶段           
           return; //如果是状态阶段的数据发送完毕，则返回
        }
    }
 //如果要发送的长度比端点2最大包长要多，则分多个包发送
    if(Ep2DataLength>EP2_SIZE)
    {
        //发送端点2最大长度字节
        EP2DataIn(EP2_SIZE,pEp2SendData);
        //指针移动EP2_SIZE字节
        pEp2SendData+=EP2_SIZE;
        Ep2DataLength-=EP2_SIZE;
        //如果是READ(10)命令，并且是数据阶段，则需要获取磁盘数据
        if((CBW[15]==READ_10)&&(TransportStage==DATA_STAGE))
        {
            GetDiskData(); //获取磁盘数据
        }                     
    }
    else
    {
        EP2DataIn((u16)Ep2DataLength,pEp2SendData);            
        Ep2DataLength=0;  //下次要发送的数据长度为0 ，       
    }
}


////////////////////////End of function//////////////////////////////
/********************************************************************
函数功能：处理SCSI命令的函数。
入口参数：无。
返    回：无。
备    注：虽然叫SCSI命令，但是实际使用的是UFI命令。
********************************************************************/
void ProcScsiCommand(void)
{
 TransportStage=DATA_STAGE; //进入到数据阶段
 //CBW中偏移量为15的字段为命令的类型
 switch(CBW[15])
 {
  case TEST_UNIT_READY: //测试磁盘是否准备好
   Ep2DataLength=0; //设置长度为0，发送数据将返回CSW
   SetSenseDataNoSense();  //设置Sense为无错误
   SetCsw(0,0); //设置CSW为成功
   Ep2SendData(); //返回CSW
  break;

  case INQUIRY:  //INQUIRY命令
   pEp2SendData=DiskInf; //返回磁盘信息
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度

   if(Ep2DataLength>sizeof(DiskInf)) //如果请求的数据比实际的要长
   {
       SetCsw(Ep2DataLength-sizeof(DiskInf),0); //设置剩余字节数以及状态成功
    Ep2DataLength=sizeof(DiskInf); //则只返回实际的长度
   }
   else
   {
       SetCsw(0,0); //设置剩余字节数以及状态成功
   }
   Ep2SendData(); //返回数据
  break;
  
  case READ_FORMAT_CAPACITIES: //读格式化容量
   pEp2SendData=MaximumCapacity; //返回最大格式化容量信息
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(MaximumCapacity),0); //设置剩余字节数以及状态成功
   SetCsw(0,0); //设置剩余字节数以及状态成功
//   if(Ep2DataLength>sizeof(MaximumCapacity)) //如果请求的数据比实际的要长
//   {
//    Ep2DataLength=sizeof(MaximumCapacity); //则只返回实际的长度
//   }
   Ep2SendData(); //返回数据
  break;
  
  case READ_CAPACITY: //读容量命令
   pEp2SendData=DiskCapacity; //返回磁盘容量
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(DiskCapacity),0); //设置剩余字节数以及状态成功
   SetCsw(0,0); //设置剩余字节数以及状态成功
//   if(Ep2DataLength>sizeof(DiskCapacity)) //如果请求的数据比实际的要长
//   {
//    Ep2DataLength=sizeof(DiskCapacity); //则只返回实际的长度
//   }
   Ep2SendData(); //返回数据
  break;
  
  case READ_10: //READ(10)命令
  case READ_12:
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
   ByteAddr=GetLba()*UDISK_SECTOR_SIZE; //获取字节地址，字节地址为逻辑块地址乘以每块大小
   if(Ep2DataLength==0)
   {
     SetCsw(0,1); //设置剩余字节数为0， 
   }
   else
   {
   SetCsw(0,0); //设置剩余字节数为0，状态成功
   SetSenseDataNoSense();  //设置Sense为无错误
   GetDiskData(); //获取需要返回的数据
   }  

   Ep2SendData(); //返回数据
  break;
  
  case WRITE_10: //WRITE(10)命令     
   Ep2DataLength=GetDataTransferLength(); //获取需要写数据的长度
//   pEp2ReceiveBuf=FlashSectorBuf; //准备接收数据
   ByteAddr=GetLba()*UDISK_SECTOR_SIZE; //获取字节地址，字节地址为逻辑块地址乘以每块大小
   SetSenseDataNoSense();  //设置Sense为无错误
   if(Ep2DataLength==0)
   {
     SetCsw(0,1);
     Ep2SendData(); //进入状态阶段
   }
   else
   {
   SetCsw(0,0); //设置剩余字节数为0，状态成功
   }
  break;

  case PREVENT_ALLOW_MEDIUM_REMOVAL:  //是否可移除
   //CBW[19]的最低位表示是否可以移除磁盘，0表示可以，1表示不可以。
   //这里用来控制LED显示。当设置为1时，LED4亮；当设置为0时，LED4灭
   if(CBW[19]&0x01) 
   {
//    OnLed4();
   }
   else
   {
//    OffLed4();
   }
   Ep2DataLength=0; //设置长度为0，发送数据将返回CSW
   SetSenseDataNoSense();  //设置Sense为无错误
   SetCsw(0,0); //设置CSW为成功
   Ep2SendData(); //返回CSW
  break;
  
  case VERIFY:  //校验，这里认为数据总是对的
   Ep2DataLength=0; //设置长度为0，发送数据将返回CSW
   SetSenseDataNoSense();  //设置Sense为无错误
   SetCsw(0,0); //设置CSW为成功
   Ep2SendData(); //返回CSW
  break;
    
  case REQUEST_SENSE: //该命令询问前一个命令执行失败的原因
   pEp2SendData=SenseData; //返回探测数据
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(SenseData),0); //设置剩余字节数以及状态成功
   SetCsw(0,0); //设置剩余字节数以及状态成功
//   if(Ep2DataLength>sizeof(SenseData)) //如果请求的数据比实际的要长
//   {
//    Ep2DataLength=sizeof(SenseData); //则只返回实际的长度
//   }
   Ep2SendData(); //返回数据
  break;
 

   case READ_TOC: // CD ROM 
   pEp2SendData=read_toc_data; // 
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(read_toc_data),0); //设置剩余字节数以及状态成功
    SetCsw(0,0); //设置剩余字节数以及状态成功
   if(Ep2DataLength>sizeof(read_toc_data)) //如果请求的数据比实际的要长
   {
    Ep2DataLength=sizeof(read_toc_data); //则只返回实际的长度
   }
   Ep2SendData(); //返回数据
  break;

   case MODE_SENSE6: //CD ROM 
   pEp2SendData=mode_sense6; // 
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(mode_sense6),0); //设置剩余字节数以及状态成功
   SetCsw(0,0); //设置剩余字节数以及状态成功
   if(Ep2DataLength>sizeof(mode_sense6)) //如果请求的数据比实际的要长
   {
    Ep2DataLength=sizeof(mode_sense6); //则只返回实际的长度
   }
   Ep2SendData(); //返回数据
  break;

   case MODE_SENSE10: //CD ROM 
   pEp2SendData=mode_sense10; // 
   Ep2DataLength=GetDataTransferLength(); //获取需要返回的长度
//   SetCsw(Ep2DataLength-sizeof(mode_sense10),0); //设置剩余字节数以及状态成功
    SetCsw(0,0); //设置剩余字节数以及状态成功
   if(Ep2DataLength>sizeof(mode_sense10)) //如果请求的数据比实际的要长
   {
    Ep2DataLength=sizeof(mode_sense10); //则只返回实际的长度
   }
   Ep2SendData(); //返回数据
  break;

  
  default: //其它命令不认，返回执行失败
   if(CBW[12]&0x80) Ep2DataLength=1; //如果为输入请求，则随便返回1字节
   else Ep2DataLength=0; //否则为输出请求，则设置长度为0，直接返回CSW
   SetCsw(GetDataTransferLength()-Ep2DataLength,1); //设置CSW为失败
   SetSenseDataInvalidCommand();  //设置为无效命令
   Ep2SendData(); //
  break;
 }
}


/********************************************************************
函数功能：处理输出数据。
入口参数：无。
返    回：无。
备    注：无。
********************************************************************/
void ProcScsiOutData(void)
{
    u16 Len;
    Len=EP2DataOut(EP2_SIZE,Flashbuffer);
    if(Ep2DataLength>Len)
    {
    Ep2DataLength-=Len;
    }
    else
    {
        Ep2DataLength = 0;
    }       
// Flash Write 添加在此处
//    SPI_Flash_Write(Ep2Buffer,ByteAddr,Len);//写入flash
    if(SPI_FLASH_TYPE!=0)
    {
//        SPI_Flash_Write_MS2160(Ep2Buffer,ByteAddr+0x8000,Len);//写入flash    前32K 为程序空间地址
    }
    ByteAddr+=Len; //调整地址
    if(Ep2DataLength==0)
    {
        //此时Ep2DataLength为0，并且处于数据阶段，调用发送数据函数将返回CSW
        Ep2SendData();
    }
}

//void ProcScsiOutData(void)
//{
// uint8 Len;
// //读端点2数据
// Len=UsbChipReadEndpointBuffer(2,EP2_SIZE,pEp2ReceiveBuf);
// //清除端点缓冲区
// UsbChipClearBuffer(2); 
// Ep2DataLength-=Len; //调整剩余长度
// pEp2ReceiveBuf+=Len; //调整接收缓冲区
// ByteAddr+=Len; //调整地址
// if(0==(ByteAddr&(UDISK_SECTOR_SIZE-1))) //如果地址是扇区整数倍，则需要写数据到FLASH
// {
//  //写一个扇区数据。注意地址要调整到扇区的开始处
//  FlashWriteOneSector(ByteAddr-UDISK_SECTOR_SIZE,FlashSectorBuf,Ep2DataLength); 
//  pEp2ReceiveBuf=FlashSectorBuf; //设置接收缓冲区地址
// }
// 
// //数据传输完毕，进入到状态阶段
// if(Ep2DataLength==0)
// {
//  Ep1DataLength=0;
//  //此时Ep1DataLength为0，并且处于数据阶段，调用发送数据函数将返回CSW
//  Ep1SendData();
// }
//}



void int_check_cbw(void)
{
    uint8 Len; 
    if(TransportStage==COMMAND_STAGE)
    {
        Len = EP2DataOut(EP2_SIZE,CBW);
//        if(Len==31) //如果接收到的数据长度是31字节，则说明是CBW
//        {
//            //检查CBW的签名是否正确，签名为“USBC”
//            if((CBW[0]=='U')&&(CBW[1]=='S')&&(CBW[2]=='B')&&(CBW[3]=='C'))
//            {
//            //CBW签名正确，处理SCSI命令
//                ProcScsiCommand();
//            }   
//        } 
         if((Len!=31) || (CBW[0]!='U') ||(CBW[1]!='S') ||(CBW[2]!='B') ||(CBW[3]!='C') )
        {
             CBW[15] = 0xFF; //按无效指令处理
         } 
                ProcScsiCommand();
            }   
    else if(TransportStage==DATA_STAGE)
    {
        ProcScsiOutData(); //处理SCSI输出数据
    }
    else
    {
         EP2DataOut(0,0);    //clear fifo 
    }                                                                               
}



