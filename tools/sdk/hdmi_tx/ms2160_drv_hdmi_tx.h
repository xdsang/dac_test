/**
******************************************************************************
* @file    ms2160_drv_hdmi_tx.h
* @author  
* @version V1.0.0
* @date    14-Dec-2017
* @brief   hdmi tx module driver declare
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_HDMI_TX_H__
#define __MACROSILICON_MS2160_DRV_HDMI_TX_H__


#ifdef __cplusplus
extern "C" {
#endif

//hdmi tx register map.


/* hdmi tx phy module register address map */
#define MS2160_HDMI_TX_PHY_REGBASE              (0xF120)

#define MS2160_HDMI_TX_PLL_POWER_REG            (MS2160_HDMI_TX_PHY_REGBASE + 0x0)
#define MS2160_HDMI_TX_PLL_CTRL1_REG            (MS2160_HDMI_TX_PHY_REGBASE + 0x1)
#define MS2160_HDMI_TX_PLL_CTRL4_REG            (MS2160_HDMI_TX_PHY_REGBASE + 0x4)
#define MS2160_HDMI_TX_PLL_CTRL5_REG            (MS2160_HDMI_TX_PHY_REGBASE + 0x5)

#define MS2160_HDMI_TX_PLL_CFG_SEL_REG          (MS2160_HDMI_TX_PHY_REGBASE + 0xF)

#define MS2160_HDMI_TX_PLL_TRIG_REG             (MS2160_HDMI_TX_PHY_REGBASE + 0x10)

#define MS2160_HDMI_TX_PHY_DATA_DRV_REG         (MS2160_HDMI_TX_PHY_REGBASE + 0x1F)

#define MS2160_HDMI_TX_PHY_POWER_REG            (MS2160_HDMI_TX_PHY_REGBASE + 0x20)


//HDMI drive
#define MS2160_HDMI_TX_PHY_POST_PRE_REG         (MS2160_HDMI_TX_PHY_REGBASE + 0x23)

#define MS2160_HDMI_TX_PHY_MAIN_PRE0_1_REG      (MS2160_HDMI_TX_PHY_REGBASE + 0x24)
#define MS2160_HDMI_TX_PHY_MAIN_PREC_2_REG      (MS2160_HDMI_TX_PHY_REGBASE + 0x25)

#define MS2160_HDMI_TX_PHY_MAIN_PO0_1_REG       (MS2160_HDMI_TX_PHY_REGBASE + 0x26)
#define MS2160_HDMI_TX_PHY_MAIN_POC_2_REG       (MS2160_HDMI_TX_PHY_REGBASE + 0x27)

#define MS2160_HDMI_TX_PHY_POST_PO0_1_REG       (MS2160_HDMI_TX_PHY_REGBASE + 0x28)
#define MS2160_HDMI_TX_PHY_POST_PO2_REG         (MS2160_HDMI_TX_PHY_REGBASE + 0x29)
/* end hdmi tx phy module register map */



/* hdmi tx shell module register address map */ 
#define MS2160_HDMI_TX_SHELL_REGBASE            (0xF500)
#define MS2160_HDMI_TX_SHELL_INTERRUPT_REG      (MS2160_HDMI_TX_SHELL_REGBASE + 0x0)
#define MS2160_HDMI_TX_SHELL_INT_MASK_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x01)
#define MS2160_HDMI_TX_SHELL_STATUS_REG         (MS2160_HDMI_TX_SHELL_REGBASE + 0x02)
#define MS2160_HDMI_TX_SHELL_DVI_REG            (MS2160_HDMI_TX_SHELL_REGBASE + 0x03)
//#define MS2160_HDMI_TX_SHELL_DC_DBG             (MS2160_HDMI_TX_SHELL_REGBASE + 0x05)
#define MS2160_HDMI_TX_SHELL_SWRST_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x06)
#define MS2160_HDMI_TX_SHELL_AVMUTE_REG         (MS2160_HDMI_TX_SHELL_REGBASE + 0x07)
#define MS2160_HDMI_TX_SHELL_MODE_REG           (MS2160_HDMI_TX_SHELL_REGBASE + 0x08)
#define MS2160_HDMI_TX_SHELL_INFO_VER_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x09)
#define MS2160_HDMI_TX_SHELL_INFO_TYPE_REG      (MS2160_HDMI_TX_SHELL_REGBASE + 0x0A)
#define MS2160_HDMI_TX_SHELL_INFO_LEN_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x0B)
#define MS2160_HDMI_TX_SHELL_NCTS_REG           (MS2160_HDMI_TX_SHELL_REGBASE + 0x0C)
#define MS2160_HDMI_TX_SHELL_INFO_PACK_REG      (MS2160_HDMI_TX_SHELL_REGBASE + 0x0D)
#define MS2160_HDMI_TX_SHELL_CTRL_REG           (MS2160_HDMI_TX_SHELL_REGBASE + 0x0E)
#define MS2160_HDMI_TX_SHELL_CFG_REG            (MS2160_HDMI_TX_SHELL_REGBASE + 0x0F)
#define MS2160_HDMI_TX_SHELL_R_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x10)
#define MS2160_HDMI_TX_SHELL_G_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x11)
#define MS2160_HDMI_TX_SHELL_B_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x12)
#define MS2160_HDMI_TX_SHELL_RGBEN_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x13)
#define MS2160_HDMI_TX_SHELL_BURST_PREAM_REG    (MS2160_HDMI_TX_SHELL_REGBASE + 0x14)
#define MS2160_HDMI_TX_SHELL_PARRAY_REG         (MS2160_HDMI_TX_SHELL_REGBASE + 0x15)
#define MS2160_HDMI_TX_SHELL_CFG_FLT_REG        (MS2160_HDMI_TX_SHELL_REGBASE + 0x16)
#define MS2160_HDMI_TX_SHELL_INFO_STATUS_REG    (MS2160_HDMI_TX_SHELL_REGBASE + 0x17)
#define MS2160_HDMI_TX_SHELL_SHIFT1_REG         (MS2160_HDMI_TX_SHELL_REGBASE + 0x18)
#define MS2160_HDMI_TX_SHELL_CTS_CTRL_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x19)
#define MS2160_HDMI_TX_SHELL_CTS_SHIFT_REG      (MS2160_HDMI_TX_SHELL_REGBASE + 0x1A)
#define MS2160_HDMI_TX_SHELL_CTS_OLD1_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x1B)      // RO 
#define MS2160_HDMI_TX_SHELL_CTS_OLD2_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x1C)      // RO
#define MS2160_HDMI_TX_SHELL_CTS_STABLE_REG     (MS2160_HDMI_TX_SHELL_REGBASE + 0x1D)      // RO BIT7-BIT4
#define MS2160_HDMI_TX_SHELL_TBUS_SEL_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x24)
#define MS2160_HDMI_TX_SHELL_DOUT_SEL_REG       (MS2160_HDMI_TX_SHELL_REGBASE + 0x25)
#define MS2160_HDMI_TX_SHELL_CNTRL_DATA_REG     (MS2160_HDMI_TX_SHELL_REGBASE + 0x26)
#define MS2160_HDMI_TX_SHELL_LFSR0_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x27)
#define MS2160_HDMI_TX_SHELL_LFSR1_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x28)
#define MS2160_HDMI_TX_SHELL_LFSR2_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x29)
#define MS2160_HDMI_TX_SHELL_LFSR3_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x2A)

//20160729, add new register for i2s lr clk detect
#define MS2160_HDMI_TX_SHELL_I2S_LR_RATE_REG    (MS2160_HDMI_TX_SHELL_REGBASE + 0x21)
/* end hdmi shell phy module register map */


/* audio module register address map */ 
#define MS2160_HDMI_TX_AUDIO_REGBASE             (0xF530)
#define MS2160_HDMI_TX_AUDIO_CFG_REG             (MS2160_HDMI_TX_AUDIO_REGBASE + 0x0)
#define MS2160_HDMI_TX_AUDIO_RATE_REG            (MS2160_HDMI_TX_AUDIO_REGBASE + 0x1)
#define MS2160_HDMI_TX_AUDIO_CH_EN_REG           (MS2160_HDMI_TX_AUDIO_REGBASE + 0x2)
#define MS2160_HDMI_TX_AUDIO_BURST_PREAMBLE_REG  (MS2160_HDMI_TX_AUDIO_REGBASE + 0x3)
#define MS2160_HDMI_TX_AUDIO_OUTSYNC_REG         (MS2160_HDMI_TX_AUDIO_REGBASE + 0x4)
#define MS2160_HDMI_TX_AUDIO_CFG_I2S_REG         (MS2160_HDMI_TX_AUDIO_REGBASE + 0x5)
#define MS2160_HDMI_TX_AUDIO_MODESEL_REG         (MS2160_HDMI_TX_AUDIO_REGBASE + 0x6)
#define MS2160_HDMI_TX_AUDIO_CH_SWITCH_0_REG     (MS2160_HDMI_TX_AUDIO_REGBASE + 0x7)
#define MS2160_HDMI_TX_AUDIO_CH_SWITCH_1_REG     (MS2160_HDMI_TX_AUDIO_REGBASE + 0x8)
#define MS2160_HDMI_TX_AUDIO_CH_SWITCH_2_REG     (MS2160_HDMI_TX_AUDIO_REGBASE + 0x9)
#define MS2160_HDMI_TX_AUDIO_LR_SWTICH_REG       (MS2160_HDMI_TX_AUDIO_REGBASE + 0xA)
#define MS2160_HDMI_TX_AUDIO_SPDIF_SEL_REG       (MS2160_HDMI_TX_AUDIO_REGBASE + 0xB)
#define MS2160_HDMI_TX_AUDIO_SPDIF_DIV_REG       (MS2160_HDMI_TX_AUDIO_REGBASE + 0xC)

#define MS2160_HDMI_TX_AUDIO_BYPASSVOL_REG       (MS2160_HDMI_TX_AUDIO_REGBASE + 0x40)
#define MS2160_HDMI_TX_AUDIO_VOL_0_REG           (MS2160_HDMI_TX_AUDIO_REGBASE + 0x41)
#define MS2160_HDMI_TX_AUDIO_VOL_1_REG           (MS2160_HDMI_TX_AUDIO_REGBASE + 0x42)
#define MS2160_HDMI_TX_AUDIO_VOL_2_REG           (MS2160_HDMI_TX_AUDIO_REGBASE + 0x43)
#define MS2160_HDMI_TX_AUDIO_VS_0_REG            (MS2160_HDMI_TX_AUDIO_REGBASE + 0x44)
#define MS2160_HDMI_TX_AUDIO_VS_1_REG            (MS2160_HDMI_TX_AUDIO_REGBASE + 0x45)
#define MS2160_HDMI_TX_AUDIO_ASYNC_REG           (MS2160_HDMI_TX_AUDIO_REGBASE + 0x46)


#define MS2160_HDMI_TX_MISC_RST_CTRL1_REG        (0xF008)
#define MS2160_HDMI_TX_MISC_HDMI_CLK_REG         (0xF005)
//#define MS2160_HDMI_TX_MISC_HDMI_CTS_CLK_REG     (0xF006)
#define MS2160_HDMI_TX_MISC_HDMI_VID_RST_REG     (0xF007)
#define MS2160_HDMI_TX_MISC_HDMI_AUD_RST_REG     (0xF008)

//#define MS2160_HDMI_TX_MISC_DDC_ENABLE_REG       (0xF012)
//#define MS2160_HDMI_TX_MISC_DDC_CTRL_REG         (0xF013)

#define MS2160_MISC_PAD_CTRL_5                   (0xF016)

#define MS2160_HDMI_TX_MISC_AUDIO_MCLK_MUX_REG          (0xF000) //
#define MS2160_HDMI_TX_MISC_AUDIO_MCLK_OUTPUT_EN_REG    (0xF000) //

//#define MS2160_HDMI_TX_MISC_SRC_CTRL_REG         (0xF026)
#define MS2160_HDMI_TX_MISC_AV_CTRL_REG          (0xF000) //

/* end audio module register map */


//HDCP
#define MS2160_HDMI_TX_HDCP_STATUS_REG           (MS2160_HDMI_TX_SHELL_REGBASE + 0x80)
#define MS2160_HDMI_TX_HDCP_CONTROL_REG          (MS2160_HDMI_TX_SHELL_REGBASE + 0x81)
#define MS2160_HDMI_TX_HDCP_BKSV0_REG            (MS2160_HDMI_TX_SHELL_REGBASE + 0x82)
//
#define MS2160_HDMI_TX_HDCP_AN0_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x87)
//
#define MS2160_HDMI_TX_HDCP_RI0_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x94)
#define MS2160_HDMI_TX_HDCP_RI1_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x95)
#define MS2160_HDMI_TX_HDCP_KEY_REG              (MS2160_HDMI_TX_SHELL_REGBASE + 0x96)
//
#define MS2160_HDMI_TX_HDCP_CFG1_REG             (MS2160_HDMI_TX_SHELL_REGBASE + 0xB7)



//HDMI video typedef

typedef enum _E_HDMI_VIDEO_CLK_REPEAT_
{
    HDMI_X1CLK      = 0x00,
    HDMI_X2CLK      = 0x01,
    HDMI_X3CLK      = 0x02,
    HDMI_X4CLK      = 0x03,
    HDMI_X5CLK      = 0x04,
    HDMI_X6CLK      = 0x05,
    HDMI_X7CLK      = 0x06,
    HDMI_X8CLK      = 0x07,
    HDMI_X9CLK      = 0x08,
    HDMI_X10CLK     = 0x09
}HDMI_CLK_RPT_E;

typedef enum _E_HDMI_VIDEO_ASPECT_RATIO_
{
    HDMI_4X3     = 0x01,
    HDMI_16X9    = 0x02
}HDMI_ASPECT_RATIO_E;


typedef enum _E_HDMI_VIDEO_SCAN_INFO_
{
    HDMI_OVERSCAN     = 0x01,    //television type
    HDMI_UNDERSCAN    = 0x02,   //computer type
}HDMI_SCAN_INFO_E;

typedef enum _E_HDMI_COLOR_SPACE_
{
    HDMI_RGB        = 0x00,
    HDMI_YCBCR422   = 0x01,
    HDMI_YCBCR444   = 0x02,
    HDMI_XVYCC444   = 0x03
}HDMI_CS_E;

typedef enum _E_HDMI_COLOR_DEPTH_
{
    HDMI_COLOR_DEPTH_8BIT    = 0x00,
    HDMI_COLOR_DEPTH_10BIT   = 0x01,
    HDMI_COLOR_DEPTH_12BIT   = 0x02,
    HDMI_COLOR_DEPTH_16BIT   = 0x03
}HDMI_COLOR_DEPTH_E;

typedef enum _E_HDMI_COLORIMETRY_
{
    HDMI_COLORIMETRY_601    = 0x00,
    HDMI_COLORIMETRY_709    = 0x01,
    HDMI_COLORIMETRY_656    = 0x02,
    HDMI_COLORIMETRY_1120   = 0x03,
    HDMI_COLORIMETRY_SMPTE  = 0x04,
    HDMI_COLORIMETRY_XVYCC601 = 0x05,
    HDMI_COLORIMETRY_XVYCC709 = 0x06
}HDMI_COLORIMETRY_E;


//HDMI audio
typedef enum _E_HDMI_AUDIO_MODE_
{
    HDMI_AUD_MODE_AUDIO_SAMPLE  = 0x00,
    HDMI_AUD_MODE_HBR           = 0x01,
    HDMI_AUD_MODE_DSD           = 0x02,
    HDMI_AUD_MODE_DST           = 0x03
}HDMI_AUDIO_MODE_E;

typedef enum _E_HDMI_AUDIO_I2S_RATE_
{
    HDMI_AUD_RATE_44K1  = 0x00,
    HDMI_AUD_RATE_48K   = 0x02,
    HDMI_AUD_RATE_32K   = 0x03,
    HDMI_AUD_RATE_88K2  = 0x08,
    HDMI_AUD_RATE_96K   = 0x0A,
    HDMI_AUD_RATE_176K4 = 0x0C,
    HDMI_AUD_RATE_192K  = 0x0E
}HDMI_AUDIO_RATE_E;

typedef enum _E_HDMI_AUDIO_LENGTH_
{
    HDMI_AUD_LENGTH_16BITS    = 0x00,
    HDMI_AUD_LENGTH_20BITS    = 0x01,
    HDMI_AUD_LENGTH_24BITS    = 0x02
}HDMI_AUDIO_LENGTH_E;

typedef enum _E_HDMI_AUDIO_CHANNEL_
{
    HDMI_AUD_2CH    = 0x01,
    HDMI_AUD_3CH    = 0x02,
    HDMI_AUD_4CH    = 0x03,
    HDMI_AUD_5CH    = 0x04,
    HDMI_AUD_6CH    = 0x05,
    HDMI_AUD_7CH    = 0x06,
    HDMI_AUD_8CH    = 0x07
}HDMI_AUDIO_CHN_E;


typedef struct _T_HDMI_CONFIG_PARA_
{   
    UINT8  u8_hdmi_flag;          // FALSE = dvi out;  TRUE = hdmi out
    UINT8  u8_vic;                // reference to CEA-861 VIC
    UINT16 u16_video_clk;         // TMDS video clk, uint 10000Hz
    UINT8  u8_clk_rpt;            // enum refer to HDMI_CLK_RPT_E. X2CLK = 480i/576i, others = X1CLK
    UINT8  u8_scan_info;          // enum refer to HDMI_SCAN_INFO_E
    UINT8  u8_aspect_ratio;       // enum refer to HDMI_ASPECT_RATIO_E
    UINT8  u8_color_space;        // enum refer to HDMI_CS_E
    UINT8  u8_color_depth;        // enum refer to HDMI_COLOR_DEPTH_E
    UINT8  u8_colorimetry;        // enum refer to HDMI_COLORIMETRY_E. IT601 = 480i/576i/480p/576p, ohters = IT709
    //
    UINT8  u8_audio_mode;         // enum refer to HDMI_AUDIO_MODE_E
    UINT8  u8_audio_rate;         // enum refer to HDMI_AUDIO_RATE_E
    UINT8  u8_audio_bits;         // enum refer to HDMI_AUDIO_LENGTH_E
    UINT8  u8_audio_channels;     // enum refer to HDMI_AUDIO_CHN_E
    UINT8  u8_audio_speaker_locations;  // 0~255, refer to CEA-861 audio infoframe, BYTE4
}HDMI_CONFIG_T;


//hdmi tx drive interface.

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_output_enable
*  Description:     hdmi tx module output timing on/off
*  Entry:           [in]b_enable: if true turn on 
*                                 else turn off
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_output_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_video_mute_enable
*  Description:     hdmi tx shell module video mute
*  Entry:           [in]b_enable, if true mute screen else normal video
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_video_mute_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_audio_mute_enable
*  Description:     hdmi tx shell module audio mute
*  Entry:           [in]b_enable, if true mute audio else normal audio
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_audio_mute_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_set_clk
*  Description:     hdmi tx phy module set video clk
*  Entry:           [in]u16_clk: uint 10000Hz 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_set_clk(UINT16 u16_clk);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_config
*  Description:     hdmi tx config
*  Entry:           [in]pt_hdmi_tx: struct refer to HDMI_CONFIG_T
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_config(HDMI_CONFIG_T *pt_hdmi_tx);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_power_down
*  Description:     hdmi tx module power down
*  Entry:           [in]None
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms2160drv_hdmi_tx_phy_power_down(VOID);


VOID ms2160drv_hdmi_tx_pll_power_down(VOID);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_hpd
*  Description:     hdmi tx hot plug detection
*  Entry:           [in]
*               
*  Returned value:  if hdmi cable plug in return true, else return false
*  Remark:
***************************************************************/ 
BOOL ms2160drv_hdmi_tx_shell_hpd(VOID);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_ddc_enable
*  Description:     hdmi tx module DDC enable
*  Entry:           [in]b_enable, if true enable ddc, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms2160drv_hdmi_tx_ddc_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_enable
*  Description:     hdmi hdcp enable
*  Entry:           [in]b_enable, if true enable hdcp, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_hdcp_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_init
*  Description:     hdmi hdcp init
*  Entry:           [in]p_u8_key, 280 bytes hdmi tx key
*                       p_u8_ksv, 5 bytes hdmi tx ksv
*
*  Returned value:  if hdcp init success return true, else return false
*  Remark:
***************************************************************/
BOOL ms2160drv_hdmi_tx_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv);


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_get_status
*  Description:     hdmi hdcp get tx/rx Ri verify result, 2s period polled
*  Entry:           [in]None
*
*  Returned value:  if verify sucess return 0x01, else return 0x00
*  Remark:
***************************************************************/
UINT8 ms2160drv_hdmi_tx_hdcp_get_status(VOID);


//HDMI EDID
typedef struct _T_HDMI_EDID_FLAG_
{   
    UINT8    u8_hdmi_sink;              //1 = HDMI sink, 0 = dvi
    UINT8    u8_color_space;            //color space support flag, flag 1 valid. BIT5: YCBCR444 flag; BIT4: YCBCR422 flag.(RGB must be support)
    //
    UINT8    u8_edid_total_blocks;      //block numbers, 128bytes in one block
    UINT8    u8_max_pixel_clk;          //Max Supported pixel clock rate in MHz/10, ERROR code is 0xFF 
}HDMI_EDID_FLAG_T;


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_parse_edid
*  Description:     parse hdmi sink edid
*  Entry:           [out]p_u8_edid_buf, buf for EDID, 256bytes
*                        pt_edid, refer to HDMI_EDID_FLAG_T
*
*  Returned value:  if parse sucess return 0x01, else return 0x00
*  Remark:
***************************************************************/
BOOL ms2160drv_hdmi_tx_parse_edid(UINT8 *p_u8_edid_buf, HDMI_EDID_FLAG_T *pt_edid);


#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_HDMI_TX_H__
