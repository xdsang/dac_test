/**
******************************************************************************
* @file    ms2160_drv_hdmi_ddc.c
* @author  
* @version V1.0.0
* @date    12-Jan-2018
* @brief   hdmi tx ddc module driver source file
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _PLATFORM_WINDOWS_
#include "common.h"
#endif
    
#include "ms2160_drv_hdmi_ddc.h"


#ifdef DDC_USE_I2C_BUS
//hdmi tx ddc register map.
#define MS2160_HDMI_DDC_INPUT_DATA_REG      (0xF011)
#define MS2160_HDMI_DDC_OUTPUT_DATA_REG     (0xF012)
#define MS2160_HDMI_DDC_OEN_REG             (0xF013)

static UINT8 g_u8_ddc_delay = 2; //default DDC speed is 100Kbit/s

#define DDC_TIMEOUT     (0x1000) //define DDC wait ack signal time out

#define DDC_SCL_HIGH    (HAL_ClrBits(MS2160_HDMI_DDC_OEN_REG, MSRT_BIT6))
#define DDC_SDA_HIGH    (HAL_ClrBits(MS2160_HDMI_DDC_OEN_REG, MSRT_BIT7))
#define DDC_SCL_LOW     (HAL_SetBits(MS2160_HDMI_DDC_OEN_REG, MSRT_BIT6))
#define DDC_SDA_LOW     (HAL_SetBits(MS2160_HDMI_DDC_OEN_REG, MSRT_BIT7))

#define _ddc_read_sda() ((HAL_ReadByte(MS2160_HDMI_DDC_INPUT_DATA_REG) >> 7) & MSRT_BIT0)

#define _ddc_delay()

#else

#define reg_gpio_data   (0xA0)
#define reg_gpio_oen    (0xB0)


#define DDC_SCL_HIGH    (HAL_ClrBits(reg_gpio_oen, MSRT_BIT6))
#define DDC_SDA_HIGH    (HAL_ClrBits(reg_gpio_oen, MSRT_BIT7))
#define DDC_SCL_LOW     (_i2c_scl_low())
#define DDC_SDA_LOW     (_i2c_sda_low())

static UINT8 g_u8_ddc_delay = 2; //default DDC speed is 100Kbit/s

#define DDC_TIMEOUT     (0x1000) //define DDC wait ack signal time out

static VOID _i2c_scl_low(VOID)
{
    HAL_SetBits(reg_gpio_oen, MSRT_BIT6);
    HAL_ClrBits(reg_gpio_data, MSRT_BIT6);
}

static VOID _i2c_sda_low(VOID)
{
    HAL_SetBits(reg_gpio_oen, MSRT_BIT7);
    HAL_ClrBits(reg_gpio_data, MSRT_BIT7);
}

#define _ddc_read_sda() ((HAL_ReadByte(reg_gpio_data) >> 7) & MSRT_BIT0)

#define _ddc_delay()

#endif





VOID ms2160drv_hdmi_ddc_init(VOID)
{
    DDC_SCL_HIGH;
    DDC_SDA_HIGH;
}

VOID ms2160drv_hdmi_ddc_set_speed(UINT8 u8_ddc_speed)
{
    switch (u8_ddc_speed)
    {
    case DDC_SPEED_20K:
        g_u8_ddc_delay = 15; //measure is 20.7KHz 
        break;
    case DDC_SPEED_100K:
        g_u8_ddc_delay = 2; //measure is 98.6KHz 
        break;
    case DDC_SPEED_400K:
        g_u8_ddc_delay = 1; //measure is 136.9KHz 
        break;
    case DDC_SPEED_750K:
        g_u8_ddc_delay = 1; //measure is 136.9KHz
        break;
    }
}

VOID _ddc_start(VOID)
{
    /* assume here all lines are HIGH */
    DDC_SDA_HIGH;
    DDC_SCL_HIGH;
    _ddc_delay();
    
    DDC_SDA_LOW;
    _ddc_delay();
    
    DDC_SCL_LOW;    
}

VOID _ddc_stop(VOID)
{
    DDC_SDA_LOW;
    _ddc_delay();
    
    DDC_SCL_HIGH;
    _ddc_delay();
    
    DDC_SDA_HIGH;
    _ddc_delay();
}

BOOL _ddc_write_byte(UINT8 value)
{
    UINT8  index;
    BOOL   result = FALSE;
    UINT16 u16Wait;
    
    /* Send 8 bits to the DDC Bus - msb first */
    for (index = 0; index < 8; index++)  
    {
        if (value & 0x80)
            DDC_SDA_HIGH;
        else    
            DDC_SDA_LOW;
        _ddc_delay();
        
        DDC_SCL_HIGH;        
        _ddc_delay();
        
        DDC_SCL_LOW;
        _ddc_delay();
        
        value <<= 1;    
    }
    
    /* wait for ack =  readbit0 */
    DDC_SDA_HIGH;
    _ddc_delay();
    
    DDC_SCL_HIGH;            
    _ddc_delay();
        
    u16Wait = 0;
    while (_ddc_read_sda() && (u16Wait < DDC_TIMEOUT)) 
        u16Wait ++;
    
    if (!_ddc_read_sda())
        result = TRUE;
    
    DDC_SCL_LOW;  
    _ddc_delay();

    return result;   
}

UINT8 _ddc_read_byte(BOOL lastByte)
{
    UINT8 index;
    UINT8 value = 0x00;
    
    DDC_SDA_HIGH;       
    for (index = 0; index < 8; index++) 
    {
        value <<= 1;        
        DDC_SCL_HIGH;           
        _ddc_delay();
        
        value |= _ddc_read_sda();  
        
        DDC_SCL_LOW;
        _ddc_delay();
    }
    
    if (!lastByte)
    {
        DDC_SDA_LOW;
    }
    else 
    {
        DDC_SDA_HIGH;
    }
    _ddc_delay();
    
    DDC_SCL_HIGH;    
    _ddc_delay();
    
    DDC_SCL_LOW;
    DDC_SDA_HIGH;
    _ddc_delay();
    
    return value;
}

// 8-bit index fro eeprom AT24C02..
UINT8 ms2160drv_hdmi_ddc_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index)
{
    UINT8 u8_value = 0;
    
    _ddc_start();
    
    if (!_ddc_write_byte(u8_address))
        goto STOP;      
    
    if (!_ddc_write_byte(u8_index))
        goto STOP;
    
    _ddc_start();
    
    if (!_ddc_write_byte(u8_address | 0x01))
        goto STOP;
    
    u8_value = _ddc_read_byte(TRUE);
    
STOP:
    _ddc_stop();
    return u8_value;
}

BOOL ms2160drv_hdmi_ddc_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value)
{
    BOOL result = FALSE;

    _ddc_start();
    
    if (!_ddc_write_byte(u8_address))
        goto STOP;  
    
    if (!_ddc_write_byte(u8_index))
        goto STOP;  
    
    if (!_ddc_write_byte(u8_value))                       
        goto STOP;  
    
    result = TRUE;

STOP:
    _ddc_stop();
    return result;                 
}

VOID ms2160drv_hdmi_ddc_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value)
{
    UINT8 i;
   
    _ddc_start();
    
    if (!_ddc_write_byte(u8_address))
        goto STOP;
    
    if (!_ddc_write_byte(u8_index))
        goto STOP;
    
    _ddc_start();
    
    if (!_ddc_write_byte(u8_address | 0x01))
        goto STOP;

    for (i = 0; i < (u8_length-1); i ++)
    {
        *pu8_value++ = _ddc_read_byte(FALSE);
    } 

    *pu8_value = _ddc_read_byte(TRUE);
        
STOP:
    _ddc_stop();
}

#if 0
VOID ms2160drv_hdmi_ddc_burstwrite_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value)
{
    UINT8 i;
    
   _ddc_start();
   
    if (!_ddc_write_byte(u8_address))
        goto STOP;
    
    if (!_ddc_write_byte( (UINT8)(u8_index) ))
        goto STOP;
    
    if (!_ddc_write_byte( (UINT8)(u8_index >> 8) ))
        goto STOP;
    
    for(i = 0; i< (u8_length-1); i ++)
    {
        if (!_ddc_write_byte(*pu8_value++))           
            goto STOP;  
    }

    if (!_ddc_write_byte(*pu8_value))           
        goto STOP;  
       
STOP:
    _ddc_stop();
}
#endif


