/**
******************************************************************************
* @file    ms2160_drv_hdmi_tx.c
* @author  
* @version V1.0.0
* @date    14-Dec-2017
* @brief   hdmi tx module driver source file
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_hdmi_tx.h"

#define MS2160_HDMI_TX_EDID         (1) //ms2160 support parse HDMI sink EDID function
#define MS2160_HDMI_TX_HDCP         (1) //ms2160 support hdmi tx HDCP function 

#if 1
#define TPI_EDID_PRINT
#else
#define TPI_EDID_PRINT MS2160_LOG
#endif

#ifdef _PLATFORM_WINDOWS_

#define HDMI_DDC_set_speed      ms2160drv_hdmi_ddc_set_speed
#define HDMI_DDC_ReadByte       ms2160drv_hdmi_ddc_read_8bidx8bval
#define HDMI_DDC_WriteByte      ms2160drv_hdmi_ddc_write_8bidx8bval
#define HDMI_DDC_ReadBytes      ms2160drv_hdmi_ddc_burstread_8bidx8bval

#else

#define HDMI_DDC_set_speed      mculib_i2c_set_speed
#define HDMI_DDC_ReadByte       mculib_i2c_read_8bidx8bval
#define HDMI_DDC_WriteByte      mculib_i2c_write_8bidx8bval
#define HDMI_DDC_ReadBytes      mculib_i2c_burstread_8bidx8bval
#endif


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_power_enable
*  Description:     hdmi tx module power on/off
*  Entry:           [in]b_enable: if true turn on 
*                                 else turn off
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms2160drv_hdmi_tx_phy_power_enable(BOOL b_enable)
{
    #if (!MS2160_PIXCLK_TYPE)
    //PLL power control
    HAL_ToggleBits(MS2160_HDMI_TX_PLL_POWER_REG, MSRT_BITS2_0, !b_enable);
    #endif

    //PHY power control
    HAL_ToggleBits(MS2160_HDMI_TX_PHY_POWER_REG, MSRT_BIT3 | MSRT_BIT1, b_enable);
    HAL_ToggleBits(MS2160_HDMI_TX_PHY_POWER_REG, MSRT_BIT5, !b_enable);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_output_enable
*  Description:     hdmi tx module output timing on/off
*  Entry:           [in]b_enable: if true turn on 
*                                 else turn off
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_output_enable(BOOL b_enable)
{
    //output data drive control
    HAL_ToggleBits(MS2160_HDMI_TX_PHY_DATA_DRV_REG, MSRT_BITS2_0, b_enable);

    //output clk drive control
    HAL_ToggleBits(MS2160_HDMI_TX_PHY_POWER_REG, MSRT_BIT2, b_enable);   
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_init
*  Description:     hdmi tx phy module init
*  Entry:           [in]None 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_init(VOID)
{
    //UINT8 u8_main_po;
    //UINT8 u8_post_po;
    //UINT16 u16_tmds_clk = 7425;
    
    //tmds_clk > 200MHz
    //u8_main_po = (u16_tmds_clk > 20000) ? 9 : 3;
    //u8_post_po = (u16_tmds_clk > 20000) ? 9 : 7;

    //HDMI TX PHY init
    //clk drive
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PREC_2_REG, MSRT_BITS6_4, (4) << 4); //clk main pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_POC_2_REG,  MSRT_BITS7_4, (2) << 4); //clk main po

    //data0 drive
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PRE0_1_REG, MSRT_BITS2_0, (4) << 0); //data main pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PO0_1_REG,  MSRT_BITS3_0, (3) << 0); //data main po
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PRE_REG,    MSRT_BIT0,    (1) << 0); //data post pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PO0_1_REG,  MSRT_BITS3_0, (2) << 0); //data post pre

    //data1 drive
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PRE0_1_REG, MSRT_BITS6_4, (4) << 4); //data main pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PO0_1_REG,  MSRT_BITS7_4, (3) << 4); //data main po
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PRE_REG,    MSRT_BIT1,    (1) << 1); //data post pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PO0_1_REG,  MSRT_BITS7_4, (2) << 4); //data post pre

    //data2 drive
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_PREC_2_REG, MSRT_BITS2_0, (4) << 0); //data main pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_MAIN_POC_2_REG,  MSRT_BITS3_0, (3) << 0); //data main po
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PRE_REG,    MSRT_BIT2,    (1) << 2); //data post pre
    HAL_ModBits(MS2160_HDMI_TX_PHY_POST_PO2_REG,    MSRT_BITS3_0, (2) << 0); //data post pre
}

#if (!MS2160_PIXCLK_TYPE)
/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_set_clk
*  Description:     hdmi tx phy module set video clk
*  Entry:           [in]u16_clk: uint 10000Hz 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_set_clk(UINT16 u16_clk)
{
    UINT8  i;
    BOOL  bFound = FALSE;  

    u16_clk = u16_clk / 100; //uint MHz
    u16_clk = u16_clk * 5;

    //1.step
    //PLL init, 20180312, use Hardware auto mode.
    HAL_ClrBits(MS2160_HDMI_TX_PLL_CFG_SEL_REG, MSRT_BITS2_0); //0xf[2:0] = 0
    HAL_SetBits(MS2160_HDMI_TX_PLL_CTRL1_REG, MSRT_BIT0); //0x1[0] = 1
    HAL_SetBits(MS2160_HDMI_TX_PLL_CTRL4_REG, MSRT_BIT3); //0x4[3] = 1
    //misc PLL reset
    HAL_SetBits(MS2160_HDMI_TX_MISC_RST_CTRL1_REG, MSRT_BIT5); //0xF008[5]
    //TXPLL_REF_CK_SEL=1  //From PIXPLL
    HAL_SetBits(0xF120 + 0x01, MSRT_BIT3);
    
    //FN = 0
    HAL_WriteByte(0xF120 + 0x09, 0);
    HAL_WriteByte(0xF120 + 0x0A, 0);
    HAL_WriteByte(0xF120 + 0x0B, 0);
    //M = 40
    HAL_WriteByte(0xF120 + 0x08, 40);
    //TXPLL_FN_EN = 0
    HAL_ClrBits(0xF120 + 0x07, MSRT_BIT0);
    
    for (i = 0; i < 4; i ++)
    {
        if ((u16_clk >= 750) && (u16_clk <= 1500))
        {
            //TXPLL_FREQ_SET<2:0>
            HAL_ModBits(0xF120 + 0x06, MSRT_BITS2_0, (1 << i) - 1);
            //TXPLL_PREDIV<1:0>
            HAL_ModBits(0xF120 + 0x03, MSRT_BITS1_0, 3 - i);
            bFound = TRUE;
            break;
        }
        u16_clk = u16_clk << 1;
    }

    if (!bFound)
    {
        MS2160_LOG("HDMI TX PLL error.");
        return;
    }
    
    HAL_WriteByte(MS2160_HDMI_TX_PLL_TRIG_REG, MSRT_BIT1);
    HAL_WriteByte(0xF120 + 0x08, 40 / (1 << (3 - i)));
    HAL_WriteByte(MS2160_HDMI_TX_PLL_TRIG_REG, MSRT_BIT1);
    //delay > 100us for PLLV clk stable
    Delay_ms(1);
    HAL_WriteByte(0xF120 + 0x08, 40);
}
#else

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_set_clk
*  Description:     hdmi tx phy module set video clk
*  Entry:           [in]u16_clk: uint 10000Hz 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_phy_set_clk(UINT16 u16_clk)
{
    UINT8  i;
    BOOL  bFound = FALSE;  
    float f_val;
    UINT32 u32_clk;
    //MS2160_LOG2("u16_clk = ", u16_clk);
    
    //1.step, PLL power control
    HAL_ToggleBits(MS2160_HDMI_TX_PLL_POWER_REG, MSRT_BITS2_0, FALSE);
    
    //2.step, f003<2>=0 //vds pix clk use pllv module
    HAL_ClrBits(0xF003, MSRT_BIT2);
    //TXPLL_FN_EN = 0
    HAL_ClrBits(0xF120 + 0x07, MSRT_BIT0);
    
    //3.step, PLL init, 20180312, use Hardware auto mode.
    HAL_ClrBits(MS2160_HDMI_TX_PLL_CFG_SEL_REG, MSRT_BITS2_0); //0xf[2:0] = 0
    HAL_SetBits(MS2160_HDMI_TX_PLL_CTRL1_REG, MSRT_BIT0); //0x1[0] = 1
    HAL_SetBits(MS2160_HDMI_TX_PLL_CTRL4_REG, MSRT_BIT3); //0x4[3] = 1
    //misc PLL reset
    HAL_SetBits(MS2160_HDMI_TX_MISC_RST_CTRL1_REG, MSRT_BIT5); //0xF008[5]
    //TXPLL_REF_CK_SEL=0  //From XTAL 24MHz
    HAL_ClrBits(0xF120 + 0x01, MSRT_BIT3);

    //4.step
    u32_clk = (UINT32)u16_clk * 5;
    for (i = 0; i < 4; i ++)
    {
        if ((u32_clk >= 75000UL) && (u32_clk <= 150000UL))
        {
            //TXPLL_FREQ_SET<2:0>
            HAL_ModBits(0xF120 + 0x06, MSRT_BITS2_0, (1 << i) - 1);
            bFound = TRUE;
            break;
        }
        u32_clk = u32_clk << 1;
    }

    if (!bFound)
    {
        MS2160_LOG("HDMI TX PLL VCO error.");
        return;
    }

    //MS2160_LOG1("u32_clk_H16 = ", u32_clk >> 16);
    //MS2160_LOG1("u32_clk_L16 = ", u32_clk);

    //5.step
    //M
    f_val = (float)u32_clk / 2400;
    HAL_WriteByte(0xF120 + 0x08, (UINT8)f_val);
    //MS2160_LOG1("M = ", (UINT8)f_val);
    //FN
    f_val = f_val - (UINT8)f_val;
    f_val = f_val * (1UL << 20);
    u32_clk = (UINT32)f_val;
    HAL_WriteByte(0xF120 + 0x09, (UINT8)u32_clk);
    HAL_WriteByte(0xF120 + 0x0A, (UINT8)(u32_clk >> 8));
    HAL_WriteByte(0xF120 + 0x0B, (UINT8)(u32_clk >> 16));
    //MS2160_LOG1("FN_H8 = ", (UINT8)(u32_clk >> 16));
    //MS2160_LOG1("FN_L16 = ", (UINT8)u32_clk);

    //6.step
    bFound = FALSE;
    //TXPLL_LOCK_DET_EN = 1
    HAL_SetBits(0xF120 + 0x03, MSRT_BIT7);
    for (i = 0; i < 10; i ++)
    {
        //trigger
        HAL_WriteByte(MS2160_HDMI_TX_PLL_TRIG_REG, MSRT_BIT1);
        Delay_ms(1);
        //at least waiting 200us
        if (HAL_ReadByte(0xF13C) & MSRT_BIT7)
        {
            //TXPLL_FN_EN = 1
            HAL_SetBits(0xF120 + 0x07, MSRT_BIT0);
            bFound = TRUE;
            break;
        }
    }

    if (!bFound)
    {
        MS2160_LOG("HDMI TX PLL unlock.");
        return;
    }

    //8.step
    //f003<2>=1 //vds pix clk use hdmi txpll
    HAL_SetBits(0xF003, MSRT_BIT2);
}
#endif

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_video_mute_enable
*  Description:     hdmi tx shell module video mute
*  Entry:           [in]b_enable, if true mute screen else normal video
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_video_mute_enable(BOOL b_enable)
{
    HAL_ToggleBits(MS2160_HDMI_TX_SHELL_AVMUTE_REG, MSRT_BIT1, b_enable);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_audio_mute_enable
*  Description:     hdmi tx shell module audio mute
*  Entry:           [in]b_enable, if true mute audio else normal audio
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_audio_mute_enable(BOOL b_enable)
{
    HAL_ToggleBits(MS2160_HDMI_TX_SHELL_AVMUTE_REG, MSRT_BIT2, b_enable);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_hdmi_out
*  Description:     hdmi tx shell module set hdmi out
*  Entry:           [in]b_hdmi_out: FALSE = dvi out;  TRUE = hdmi out��
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_hdmi_out(BOOL b_hdmi_out)
{
    HAL_ToggleBits(MS2160_HDMI_TX_SHELL_DVI_REG, MSRT_BIT0, !b_hdmi_out);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_clk_repeat
*  Description:     hdmi tx shell module set out clk repeat times
*  Entry:           [in]u8_times: enum refer to HDMI_CLK_RPT_E
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_clk_repeat(UINT8 u8_times)
{
    if (u8_times >= HDMI_X4CLK) u8_times = HDMI_X1CLK;
        
    HAL_ModBits(MS2160_HDMI_TX_SHELL_MODE_REG, MSRT_BITS5_4, u8_times << 4);

    //if use deep color old method(reg0x505[0] = 0), same as CS5288A, need to config below register
    //tx video write enable divider
    HAL_ModBits(MS2160_HDMI_TX_MISC_AV_CTRL_REG, MSRT_BITS7_6, u8_times << 6);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_color_space
*  Description:     hdmi tx shell module video color space
*  Entry:           [in]u8_cs: enum refer to HDMI_CS_E
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_color_space(UINT8 u8_cs)
{
    HAL_ModBits(MS2160_HDMI_TX_SHELL_MODE_REG, MSRT_BITS7_6, u8_cs << 6);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_color_depth
*  Description:     hdmi tx shell module video color depth. 10bits, 12bits, 16bits no support in ms2160
*  Entry:           [in]u8_depth: enum refer to HDMI_COLOR_DEPTH_E
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_color_depth(UINT8 u8_depth)
{
    HAL_ToggleBits(MS2160_HDMI_TX_SHELL_MODE_REG, MSRT_BIT2, u8_depth > 0);
    HAL_ModBits(MS2160_HDMI_TX_SHELL_MODE_REG, MSRT_BITS1_0, u8_depth);
}


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_audio_mode
*  Description:     hdmi tx shell module set audio mode. HBR, DSD mode no support in ms2160
*  Entry:           [in]u8_audio_mode: enum refer to HDMI_AUDIO_MODE_E
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_audio_mode(UINT8 u8_audio_mode)
{
    switch (u8_audio_mode)
    {
    case HDMI_AUD_MODE_AUDIO_SAMPLE:
        //HAL_ClrBits(MS2160_HDMI_TX_MISC_ACLK_SEL_REG, MSRT_BITS1_0);            // I2S clk
        HAL_ClrBits(MS2160_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);                   // I2S Audio data selection.
        break;
    case HDMI_AUD_MODE_HBR:
        //HAL_SetBits(MS2160_HDMI_TX_MISC_ACLK_SEL_REG, MSRT_BITS1_0);            // SPDIF clk
        HAL_SetBits(MS2160_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);                   // SPDIF Audio data selection.  
        break;
    case HDMI_AUD_MODE_DSD:
        //HAL_ModBits(MS2160_HDMI_TX_MISC_ACLK_SEL_REG, MSRT_BITS1_0, MSRT_BIT0); // dsd clk
        HAL_ClrBits(MS2160_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);                   // I2S Audio data selection.
        break;
    case HDMI_AUD_MODE_DST:
        //HAL_ModBits(MS2160_HDMI_TX_MISC_ACLK_SEL_REG, MSRT_BITS1_0, MSRT_BIT0); // dsd clk
        HAL_SetBits(MS2160_HDMI_TX_AUDIO_CFG_REG, MSRT_BIT0);                   // SPDIF Audio data selection.  
        break;

    default:
        break;
    }
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_audio_rate
*  Description:     hdmi tx shell module set audio I2S rate
*  Entry:           [in]u8_audio_rate: enum refer to HDMI_AUDIO_RATE_E
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_audio_rate(UINT8 u8_audio_rate)
{
    switch (u8_audio_rate)
    {
    case HDMI_AUD_RATE_48K:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT2);
        break;
        
    case HDMI_AUD_RATE_44K1:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT5);
        break;
        
    case HDMI_AUD_RATE_96K:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT1);
        break;
        
    case HDMI_AUD_RATE_32K:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT6);
        break;
        
    case HDMI_AUD_RATE_88K2:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT4);
        break;
        
    case HDMI_AUD_RATE_176K4: 
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT3);
        break;
        
    case HDMI_AUD_RATE_192K:
        HAL_ModBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BITS6_0, MSRT_BIT0);
        break;        
        
    default:
        break; 
    }
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_audio_bits
*  Description:     hdmi tx shell module set audio bit length
*  Entry:           [in]u8_audio_bits: enum refer to HDMI_AUDIO_LENGTH_E
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_audio_bits(UINT8 u8_audio_bits)
{
    HAL_ModBits(MS2160_HDMI_TX_AUDIO_CFG_I2S_REG, MSRT_BITS5_4, u8_audio_bits ? 0x00 : MSRT_BIT4);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_audio_channels
*  Description:     hdmi tx shell module set audio channels
*  Entry:           [in]u8_audio_channels: enum refer to HDMI_AUDIO_CHN_E
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_audio_channels(UINT8 u8_audio_channels)
{
    if (u8_audio_channels <= HDMI_AUD_2CH)
    {
        u8_audio_channels = 0x01;
    }
    else if (u8_audio_channels <= HDMI_AUD_4CH)
    {
        u8_audio_channels = 0x03;
    }
    else if (u8_audio_channels <= HDMI_AUD_6CH)
    {
        u8_audio_channels = 0x07;
    }
    else
    {
        u8_audio_channels = 0x0f;
    }

    HAL_ModBits(MS2160_HDMI_TX_AUDIO_CH_EN_REG, MSRT_BITS3_0, u8_audio_channels);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_set_audio_lrck
*  Description:     hdmi tx shell module set audio lrck
*  Entry:           [in]b_left: if true left channel in, else right channel in
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_set_audio_lrck(BOOL b_left)
{
    HAL_ModBits(MS2160_HDMI_TX_AUDIO_CFG_I2S_REG, MSRT_BITS1_0, b_left ? MSRT_BIT1 : 0x00);
}

static UINT8 arrInfoFrameBuffer[0x0E];

VOID ms2160drv_hdmi_tx_shell_set_video_infoframe(HDMI_CONFIG_T *pt_hdmi_tx)
{
    UINT8 i;
    UINT8 u8tmp = 0;

    HAL_ClrBits(MS2160_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT6); // "AVI packet is enabled, active is high"
    memset(arrInfoFrameBuffer, 0, 0x0E);

    u8tmp = pt_hdmi_tx->u8_color_space;
    arrInfoFrameBuffer[1] = (u8tmp << 5) & MSRT_BITS6_5; 

    arrInfoFrameBuffer[1] |= 0x10;     //Active Format Information Present, Active
    
    // 2013-10-18, Change the SCAN INFO @ CKL's request. 
    // it should Overscan in a CE format and underscan in a IT format.
    arrInfoFrameBuffer[1] |= (pt_hdmi_tx->u8_scan_info & 0x03);

    if (pt_hdmi_tx->u8_colorimetry == HDMI_COLORIMETRY_601)
    {
        arrInfoFrameBuffer[2] = MSRT_BIT6;
    }
    else if ((pt_hdmi_tx->u8_colorimetry == HDMI_COLORIMETRY_709) || (pt_hdmi_tx->u8_colorimetry == HDMI_COLORIMETRY_1120))
    {
        arrInfoFrameBuffer[2] = MSRT_BIT7;
    }
    else
    {
        arrInfoFrameBuffer[2] &= ~MSRT_BITS7_6;
        arrInfoFrameBuffer[3] &= ~MSRT_BITS6_4; 
    }

    //others set to 0
    if (pt_hdmi_tx->u8_aspect_ratio == HDMI_4X3)
    {
        arrInfoFrameBuffer[2] |= 0x10;
    }
    else if (pt_hdmi_tx->u8_aspect_ratio == HDMI_16X9)
    {
        arrInfoFrameBuffer[2] |= 0x20;
    }

    //R3...R0, default to 0x08
    arrInfoFrameBuffer[2] |= 0x08; // Same as coded frame aspect ratio.

    // According to HDMI1.3 spec. VESA mode should be set to 0
    if (pt_hdmi_tx->u8_vic >= 64) //VESA
    {
        arrInfoFrameBuffer[4] = 0;    // 0 for vesa modes.
    }
    else
    {
        arrInfoFrameBuffer[4] = pt_hdmi_tx->u8_vic;
    }
    arrInfoFrameBuffer[5] = pt_hdmi_tx->u8_clk_rpt;

    //Calculate the check-sum
    arrInfoFrameBuffer[0] = 0x82 + 0x02 + 0x0D;    
    for (i = 1; i < 0x0E; i ++)
    {
        arrInfoFrameBuffer[0] += arrInfoFrameBuffer[i];
    }
    arrInfoFrameBuffer[0] = 0x100 - arrInfoFrameBuffer[0];
    // Write data into hdmi shell.
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_TYPE_REG, 0x82);
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_VER_REG, 0x02);
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_LEN_REG, 0x0D);

    for (i = 0; i < 0x0E; i ++)
    {
        HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_PACK_REG, arrInfoFrameBuffer[i]);
    }
    HAL_SetBits(MS2160_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT6);
}

VOID ms2160drv_hdmi_tx_shell_set_audio_infoframe(HDMI_CONFIG_T *pt_hdmi_tx)
{
    UINT8 i;
    // 
    HAL_ClrBits(MS2160_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT5);
    
    memset(arrInfoFrameBuffer, 0, 0x0E);
    arrInfoFrameBuffer[0] = 0x84;
    arrInfoFrameBuffer[1] = 0x01;
    arrInfoFrameBuffer[2] = 0x0A;
    arrInfoFrameBuffer[3] = 0x84 + 0x01 + 0x0A;
    // 20140917 fixed bug for audio inforframe channel config
    arrInfoFrameBuffer[4] = pt_hdmi_tx->u8_audio_channels;
    arrInfoFrameBuffer[5] = 0;
    arrInfoFrameBuffer[7] = pt_hdmi_tx->u8_audio_speaker_locations;
    // Calculate the checksum
    for (i = 4; i < 0x0E; i ++)
    {
        arrInfoFrameBuffer[3] += arrInfoFrameBuffer[i];
    }
    arrInfoFrameBuffer[3] = 0x100 - arrInfoFrameBuffer[3];
    // Write packet info.
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_TYPE_REG, arrInfoFrameBuffer[0]);
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_VER_REG, arrInfoFrameBuffer[1]);
    HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_LEN_REG, arrInfoFrameBuffer[2]);
    
    for (i = 3; i < 0x0E; i ++)
    {
        HAL_WriteByte(MS2160_HDMI_TX_SHELL_INFO_PACK_REG, arrInfoFrameBuffer[i]);
    }
    // open the 'cfg_audio_en_o", tell the hw infoframe is ready.
    HAL_SetBits(MS2160_HDMI_TX_SHELL_CTRL_REG, MSRT_BIT5);
}


/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_init
*  Description:     hdmi tx shell module init
*  Entry:           [in]None 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_init(VOID)
{
    //hdmi shell use new design
    //HAL_SetBits(MS2160_HDMI_TX_SHELL_DC_DBG, MSRT_BIT0);
    
    //video and audio clk enable, 
    //20170412, add video clk for mistable
    HAL_SetBits(MS2160_HDMI_TX_MISC_HDMI_CLK_REG, MSRT_BITS1_0);
    
    //audio
    //HAL_ClrBits(MS2160_HDMI_TX_MISC_HDMI_CTS_CLK_REG, MSRT_BITS2_1);  // CTS_SEL, register default
    //ms2160drv_hdmi_tx_shell_set_audio_lrck(TRUE); //register default

    //video and audio release reset
    //HAL_SetBits(MS2160_HDMI_TX_MISC_HDMI_VID_RST_REG, MSRT_BIT7);
    //HAL_SetBits(MS2160_HDMI_TX_MISC_HDMI_AUD_RST_REG, MSRT_BIT0);

    //20140918 set audio enable
    HAL_SetBits(MS2160_HDMI_TX_AUDIO_RATE_REG, MSRT_BIT7);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_reset_enable
*  Description:     hdmi tx shell module reset
*  Entry:           [in]None 
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_shell_reset_enable(BOOL b_en)
{
    //video and audio reset
    HAL_ToggleBits(MS2160_HDMI_TX_MISC_HDMI_VID_RST_REG, MSRT_BIT7, !b_en);
    HAL_ToggleBits(MS2160_HDMI_TX_MISC_HDMI_AUD_RST_REG, MSRT_BIT0, !b_en);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_config
*  Description:     hdmi tx config
*  Entry:           [in]pt_hdmi_tx: struct refer to HDMI_CONFIG_T
*                      
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_config(HDMI_CONFIG_T *pt_hdmi_tx)
{
    //disable output
    //ms2160drv_hdmi_tx_phy_output_enable(FALSE);

    //PHY
    ms2160drv_hdmi_tx_phy_init();
    ms2160drv_hdmi_tx_phy_power_enable(TRUE); //
    #if (!MS2160_PIXCLK_TYPE)
    //delay > 100us for PLLV power stable
    Delay_ms(10);
    ms2160drv_hdmi_tx_phy_set_clk(pt_hdmi_tx->u16_video_clk);
    #endif
    
    //SHELL
    ms2160drv_hdmi_tx_shell_reset_enable(TRUE);
    ms2160drv_hdmi_tx_shell_init();
    ms2160drv_hdmi_tx_shell_set_hdmi_out(pt_hdmi_tx->u8_hdmi_flag);
    ms2160drv_hdmi_tx_shell_set_clk_repeat(pt_hdmi_tx->u8_clk_rpt);
    ms2160drv_hdmi_tx_shell_set_color_space(pt_hdmi_tx->u8_color_space);
    ms2160drv_hdmi_tx_shell_set_color_depth(pt_hdmi_tx->u8_color_depth);
    //
    ms2160drv_hdmi_tx_shell_set_audio_mode(pt_hdmi_tx->u8_audio_mode);
    ms2160drv_hdmi_tx_shell_set_audio_rate(pt_hdmi_tx->u8_audio_rate);
    ms2160drv_hdmi_tx_shell_set_audio_bits(pt_hdmi_tx->u8_audio_bits);
    ms2160drv_hdmi_tx_shell_set_audio_channels(pt_hdmi_tx->u8_audio_channels);
    //
    ms2160drv_hdmi_tx_shell_reset_enable(FALSE);
    //
    ms2160drv_hdmi_tx_shell_set_video_infoframe(pt_hdmi_tx);
    ms2160drv_hdmi_tx_shell_set_audio_infoframe(pt_hdmi_tx);
    
    //enable output
    //ms2160drv_hdmi_tx_shell_video_mute_enable(FALSE);
    //ms2160drv_hdmi_tx_shell_audio_mute_enable(FALSE);
    //ms2160drv_hdmi_tx_phy_output_enable(TRUE);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_phy_power_down
*  Description:     hdmi tx module power down
*  Entry:           [in]None
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms2160drv_hdmi_tx_phy_power_down(VOID)
{
    ms2160drv_hdmi_tx_phy_power_enable(FALSE);
}

VOID ms2160drv_hdmi_tx_pll_power_down(VOID)
{
    //PLL power 
    HAL_ToggleBits(MS2160_HDMI_TX_PLL_POWER_REG, MSRT_BITS2_0, TRUE);
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_shell_hpd
*  Description:     hdmi tx hot plug detection
*  Entry:           [in]
*               
*  Returned value:  if hdmi cable plug in return true, else return false
*  Remark:
***************************************************************/ 
BOOL ms2160drv_hdmi_tx_shell_hpd(VOID)
{
    HAL_SetBits(MS2160_MISC_PAD_CTRL_5, MSRT_BIT7);
    Delay_us(1);
    return (HAL_ReadByte(MS2160_HDMI_TX_SHELL_STATUS_REG) & MSRT_BIT0);
}

#define MASK_HDMI_DDC_DISABLE (0x01)

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_ddc_enable
*  Description:     hdmi tx module DDC enable
*  Entry:           [in]b_enable, if true enable ddc, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/ 
VOID ms2160drv_hdmi_tx_ddc_enable(BOOL b_enable)
{
    if (b_enable)
    {
        HDMI_DDC_set_speed(0); // Default to 20 kbit/s
        // Prepare the GPIO
        //HAL_WriteByte(MS2160_HDMI_TX_MISC_DDC_CTRL_REG, 0xFF);   
        //HAL_ClrBits(MS2160_HDMI_TX_MISC_DDC_ENABLE_REG, MASK_HDMI_DDC_DISABLE);
        
        #ifdef _PLATFORM_WINDOWS_
        HAL_ClrBits(0xE003, MSRT_BIT0);
        #endif
        mculib_i2c_set_ddc(TRUE);
    }
    else
    {
        //HAL_SetBits(MS2160_HDMI_TX_MISC_DDC_ENABLE_REG, MASK_HDMI_DDC_DISABLE);
        //HAL_WriteByte(MS2160_HDMI_TX_MISC_DDC_CTRL_REG, 0x11);
        HDMI_DDC_set_speed(1); // Default to 100 kbit/s
         #ifdef _PLATFORM_WINDOWS_
        HAL_SetBits(0xE003, MSRT_BIT0);
        #endif
        mculib_i2c_set_ddc(FALSE);
    }
}

//
#if MS2160_HDMI_TX_HDCP

#define HDCP_RX_SLAVE_ADDR      0x74
#define REG_HDCP_RX_BKSV0       0x00
#define REG_HDCP_RX_AKSV0       0x10
#define REG_HDCP_RX_AN0         0x18
#define REG_HDCP_RX_RI0         0x08
#define REG_HDCP_RX_RI1         0x09

#define AKSV_SIZE               5
#define NUM_OF_ONES_IN_KSV      20
#define BYTE_SIZE               0x08
#define TX_KEY_LEN              280

BOOL ms2160drv_hdmi_tx_hdcp_get_bksv_from_rx(UINT8 *p_data)
{
    UINT8 NumOfOnes = 0;
    UINT8 i, j;
    UINT8 u8_temp;
    
    ms2160drv_hdmi_tx_ddc_enable(TRUE);
    HDMI_DDC_ReadBytes(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_BKSV0, AKSV_SIZE, p_data);
    
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        u8_temp = p_data[i];
        MS2160_LOG1("bksv = ", u8_temp);
        for (j = 0; j < BYTE_SIZE; j++)
        {
            if (u8_temp & 0x01)
            {
                NumOfOnes++;
            }
            u8_temp >>= 1;
        }
    }
    ms2160drv_hdmi_tx_ddc_enable(FALSE);
    
    if (NumOfOnes != NUM_OF_ONES_IN_KSV)
    {
        return FALSE;
    }

    MS2160_LOG("bksv ok.");
    
    return TRUE;
}

VOID ms2160drv_hdmi_tx_hdcp_set_bksv_to_tx(UINT8 *p_data)
{
    UINT8 i;
    
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_BKSV0_REG + i, p_data[i]);
    }
    
    //HPD is OK, TX/RX is connected
    //HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG + REG_ADDRESS_OFST, 0x02);
    HAL_WriteByte(MS2160_HDMI_TX_HDCP_CFG1_REG, 0x03);
    //HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x42);
    HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x4a);   
}

//AN is random number
static UINT8 __CODE HDCP_AN_DATA[] =
{
    0x4d,
    0xdb,
    0xd1,
    0x91,
    0xc9,
    0x0d,
    0x0e,
    0xe7,
};

VOID ms2160drv_hdmi_tx_hdcp_set_an(VOID)
{
    UINT8 i;

    for (i = 0; i < 8; i ++)
    {
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_AN0_REG + i, HDCP_AN_DATA[i]);
    }

    ms2160drv_hdmi_tx_ddc_enable(TRUE);
    for (i = 0; i < 8; i ++)
    {
        HDMI_DDC_WriteByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_AN0 + i, HDCP_AN_DATA[i]);
    }
    ms2160drv_hdmi_tx_ddc_enable(FALSE);
}

VOID ms2160drv_hdmi_tx_hdcp_set_aksv_to_rx(UINT8 *p_u8_ksv)
{
    UINT8 i;
    
    ms2160drv_hdmi_tx_ddc_enable(TRUE);
    for (i = 0; i < AKSV_SIZE; i ++)
    {
        HDMI_DDC_WriteByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_AKSV0 + i, p_u8_ksv[i]);
    }
    ms2160drv_hdmi_tx_ddc_enable(FALSE);
}

VOID ms2160drv_hdmi_tx_hdcp_set_key_to_tx(UINT8 *p_u8_key)
{
    UINT16 i;
    
    for (i = 0; i < TX_KEY_LEN; i ++)
    {
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_KEY_REG, p_u8_key[i]);
    }
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_enable
*  Description:     hdmi hdcp enable
*  Entry:           [in]b_enable, if true enable hdcp, else disable
*               
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_hdmi_tx_hdcp_enable(BOOL b_enable)
{
    if (b_enable)
    {
        //HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x4a);
        //delay >= 100us for wait m0/ks/r0 is calculated 
        //Delay_us(100);
        //MS2160_LOG1("KM calc done = ", HAL_ReadByte(MS2160_HDMI_TX_HDCP_STATUS_REG) & MSRT_BIT2);
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x0b);
    }
    else
    {
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x04);
        HAL_WriteByte(MS2160_HDMI_TX_HDCP_CONTROL_REG, 0x00);
    }
}   

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_init
*  Description:     hdmi hdcp init
*  Entry:           [in]p_u8_key, 280 bytes hdmi tx key
*                       p_u8_ksv, 5 bytes hdmi tx ksv
*
*  Returned value:  if hdcp init success return true, else return false
*  Remark:
***************************************************************/
BOOL ms2160drv_hdmi_tx_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv)
{
    UINT8 u8_arr_aksv_buf[AKSV_SIZE];

    if (ms2160drv_hdmi_tx_hdcp_get_bksv_from_rx(u8_arr_aksv_buf))
    {
        ms2160drv_hdmi_tx_hdcp_set_bksv_to_tx(u8_arr_aksv_buf);
        ms2160drv_hdmi_tx_hdcp_set_an();
        ms2160drv_hdmi_tx_hdcp_set_aksv_to_rx(p_u8_ksv);
        ms2160drv_hdmi_tx_hdcp_set_key_to_tx(p_u8_key);

        return TRUE;
    }
    
    return FALSE;
}

/***************************************************************
*  Function name:   ms2160drv_hdmi_tx_hdcp_get_status
*  Description:     hdmi hdcp get tx/rx Ri verify result, 2s period polled
*  Entry:           [in]None
*
*  Returned value:  if verify sucess return 0x01, else return 0x00
*  Remark:
***************************************************************/
UINT8 ms2160drv_hdmi_tx_hdcp_get_status(VOID)
{
    UINT8 i;
    UINT8 TX_Ri0;
    UINT8 TX_Ri1;
    UINT8 RX_Ri0;
    UINT8 RX_Ri1;
    
    for (i = 0; i < 3; i ++)
    {
        TX_Ri0 = HAL_ReadByte(MS2160_HDMI_TX_HDCP_RI0_REG);
        TX_Ri1 = HAL_ReadByte(MS2160_HDMI_TX_HDCP_RI1_REG);
        ms2160drv_hdmi_tx_ddc_enable(TRUE);
        RX_Ri0 = HDMI_DDC_ReadByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_RI0);
        RX_Ri1 = HDMI_DDC_ReadByte(HDCP_RX_SLAVE_ADDR, REG_HDCP_RX_RI1);
        ms2160drv_hdmi_tx_ddc_enable(FALSE);
        MS2160_LOG1("TX_Ri0 = ", TX_Ri0);
        MS2160_LOG1("TX_Ri1 = ", TX_Ri1);
        MS2160_LOG1("RX_Ri0 = ", RX_Ri0);
        MS2160_LOG1("RX_Ri1 = ", RX_Ri1);
        if (TX_Ri0 == RX_Ri0 && TX_Ri1 == RX_Ri1)
        {
            //MS2160_LOG("HDCP success.");
            return 0x01;
        }
        else
        {
            Delay_ms(20);
        }
    }

    //MS2160_LOG("HDCP failed.");
    return 0x00;
}

#endif // MS2160_HDMI_TX_HDCP


//
// 
#if MS2160_HDMI_TX_EDID

// Generic Masks
//====================================================
#define LOW_BYTE                        (0x00FF)

#define LOW_NIBBLE                      (0x0F)
#define HI_NIBBLE                       (0xF0)

#define MSBIT                           (0x80)
#define LSBIT                           (0x01)

#define BIT_0                           (0x01)
#define BIT_1                           (0x02)
#define BIT_2                           (0x04)
#define BIT_3                           (0x08)
#define BIT_4                           (0x10)
#define BIT_5                           (0x20)
#define BIT_6                           (0x40)
#define BIT_7                           (0x80)

#define TWO_LSBITS                      (0x03)
#define THREE_LSBITS                    (0x07)
#define FOUR_LSBITS                     (0x0F)
#define FIVE_LSBITS                     (0x1F)
#define SEVEN_LSBITS                    (0x7F)
#define TWO_MSBITS                      (0xC0)
#define EIGHT_BITS                      (0xFF)
//#define BYTE_SIZE                       (0x08)
#define BITS_1_0                        (0x03)
#define BITS_2_1                        (0x06)
#define BITS_2_1_0                      (0x0C)
#define BITS_4_3_2                      (0x1C)  
#define BITS_5_4                        (0x30)
#define BITS_5_4_3                      (0x38)
#define BITS_6_5                        (0x60)
#define BITS_6_5_4                      (0x70)
#define BITS_7_6                        (0xC0)

//--------------------------------------------------------------------
// EDID Constants Definition
//--------------------------------------------------------------------
#define EDID_BLOCK_0_OFFSET             (0x00)
#define EDID_BLOCK_1_OFFSET             (0x80)

#define EDID_BLOCK_SIZE                 (128)
#define EDID_HDR_NO_OF_FF               (0x06)
#define NUM_OF_EXTEN_ADDR               (0x7E)

#define EDID_TAG_ADDR                   (0x00)
#define EDID_REV_ADDR                   (0x01)
#define EDID_TAG_IDX                    (0x02)
#define LONG_DESCR_PTR_IDX              (0x02)
#define MISC_SUPPORT_IDX                (0x03)

#define ESTABLISHED_TIMING_INDEX        (35)      // Offset of Established Timing in EDID block
#define NUM_OF_STANDARD_TIMINGS         (8)
#define STANDARD_TIMING_OFFSET          (38)
#define LONG_DESCR_LEN                  (18)
#define NUM_OF_DETAILED_DESCRIPTORS     (4)

#define DETAILED_TIMING_OFFSET          (0x36)


// Offsets within a Long Descriptors Block
//====================================================
#define PIX_CLK_OFFSET                  (0)
#define H_ACTIVE_OFFSET                 (2)
#define H_BLANKING_OFFSET               (3)
#define V_ACTIVE_OFFSET                 (5)
#define V_BLANKING_OFFSET               (6)
#define H_SYNC_OFFSET                   (8)
#define H_SYNC_PW_OFFSET                (9)
#define V_SYNC_OFFSET                   (10)
#define V_SYNC_PW_OFFSET                (10)
#define H_IMAGE_SIZE_OFFSET             (12)
#define V_IMAGE_SIZE_OFFSET             (13)
#define H_BORDER_OFFSET                 (15)
#define V_BORDER_OFFSET                 (16)
#define FLAGS_OFFSET                    (17)

#define AR16_10                         (0)
#define AR4_3                           (1)
#define AR5_4                           (2)
#define AR16_9                          (3)


// Data Block Tag Codes
#define AUDIO_D_BLOCK                   (0x01)
#define VIDEO_D_BLOCK                   (0x02)
#define VENDOR_SPEC_D_BLOCK             (0x03)
#define SPKR_ALLOC_D_BLOCK              (0x04)
#define USE_EXTENDED_TAG                (0x07)


// Extended Data Block Tag Codes
//====================================================
#define COLORIMETRY_D_BLOCK             (0x05)

#define HDMI_SIGNATURE_LEN              (0x03)

#define CEC_PHYS_ADDR_LEN               (0x02)
#define EDID_EXTENSION_TAG              (0x02)
#define EDID_REV_THREE                  (0x03)
#define EDID_DATA_START                 (0x04)

#define EDID_BLOCK_0                    (0x00)
#define EDID_BLOCK_2_3                  (0x01) //

#define VIDEO_CAPABILITY_D_BLOCK        (0x00)


// Checking EDID header only
static BOOL EDID_CheckHeader(UINT8* pHeader)
{
    UINT8 i = 0;
    if ((pHeader[0] != 0) || (pHeader[7] != 0))
    {
        return FALSE;
    }

    for (i = 1; i <= 6; i ++)
    {
        if (pHeader[i] != 0xFF)    // Must be 0xFF.
            return FALSE;
    }
    return TRUE;
}

//------------------------------------------------------------------------------
// Function Name: ParseDetailedTiming()
// Function Description: Parse the detailed timing section of EDID Block 0 and
//                  print their decoded meaning to the screen.
//
// Accepts: Pointer to the 128 byte array where the data read from EDID Block0 is stored.
//              Offset to the beginning of the Detailed Timing Descriptor data.
//
//              Block indicator to distinguish between block #0 and blocks #2, #3
// Returns: none
// Globals: EDID data
//------------------------------------------------------------------------------
UINT8 ParseDetailedTiming (UINT8 *Data, UINT8 DetailedTimingOffset, UINT8 Block, HDMI_EDID_FLAG_T *pt_edid)
{
    UINT8 TmpByte;
    UINT8 i;
    UINT16 TmpWord;

    TmpWord = Data[DetailedTimingOffset + PIX_CLK_OFFSET] +
                256 * Data[DetailedTimingOffset + PIX_CLK_OFFSET + 1];

    if (TmpWord == 0x00)            // 18 byte partition is used as either for Monitor Name or for Monitor Range Limits or it is unused
    {
        if (Block == EDID_BLOCK_0)      // if called from Block #0 and first 2 bytes are 0 => either Monitor Name or for Monitor Range Limits
        {
            if (Data[DetailedTimingOffset + 3] == 0xFC) // these 13 bytes are ASCII coded monitor name
            {
                TPI_EDID_PRINT(("Monitor Name: "));

                for (i = 0; i < 13; i++)
                {
                    TPI_EDID_PRINT("%c", Data[DetailedTimingOffset + 5 + i]); // Display monitor name on SiIMon
                }
                TPI_EDID_PRINT("\n");
            }
            else if (Data[DetailedTimingOffset + 3] == 0xFD) // these 13 bytes contain Monitor Range limits, binary coded
            {
                TPI_EDID_PRINT(("Monitor Range Limits:\n\n"));

                i = 0;
                TPI_EDID_PRINT("Min Vertical Rate in Hz: %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                TPI_EDID_PRINT("Max Vertical Rate in Hz: %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                TPI_EDID_PRINT("Min Horizontal Rate in Hz: %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                TPI_EDID_PRINT("Max Horizontal Rate in Hz: %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                pt_edid->u8_max_pixel_clk = Data[DetailedTimingOffset + 5 + i];
                TPI_EDID_PRINT("Max Supported pixel clock rate in MHz/10: %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                TPI_EDID_PRINT("Tag for secondary timing formula (00h=not used): %d\n", (int) Data[DetailedTimingOffset + 5 + i++]); //
                TPI_EDID_PRINT("Min Vertical Rate in Hz %d\n", (int) Data[DetailedTimingOffset + 5 + i]); //
                TPI_EDID_PRINT("\n");
            }
        }
        else if (Block == EDID_BLOCK_2_3)                          // if called from block #2 or #3 and first 2 bytes are 0x00 (padding) then this
        {                                                                                          // descriptor partition is not used and parsing should be stopped
            TPI_EDID_PRINT("No More Detailed descriptors in this block\n");
            TPI_EDID_PRINT("\n");
            return FALSE;
        }
    }
    else                                            // first 2 bytes are not 0 => this is a detailed timing descriptor from either block
    {
        if((Block == EDID_BLOCK_0) && (DetailedTimingOffset == 0x36))
        {
            TPI_EDID_PRINT("\n\n\nParse Results, EDID Block #0, Detailed Descriptor Number 1:\n");
            TPI_EDID_PRINT("===========================================================\n\n");
        }
        else if((Block == EDID_BLOCK_0) && (DetailedTimingOffset == 0x48))
        {
            TPI_EDID_PRINT("\n\n\nParse Results, EDID Block #0, Detailed Descriptor Number 2:\n");
            TPI_EDID_PRINT("===========================================================\n\n");
        }

        TPI_EDID_PRINT("Pixel Clock (MHz * 100): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + H_ACTIVE_OFFSET] +
                    256 * ((Data[DetailedTimingOffset + H_ACTIVE_OFFSET + 2] >> 4) & FOUR_LSBITS);
        TPI_EDID_PRINT("Horizontal Active Pixels: %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + H_BLANKING_OFFSET] +
                    256 * (Data[DetailedTimingOffset + H_BLANKING_OFFSET + 1] & FOUR_LSBITS);
        TPI_EDID_PRINT("Horizontal Blanking (Pixels): %d\n", (int)TmpWord);

        TmpWord = (Data[DetailedTimingOffset + V_ACTIVE_OFFSET] )+
                    256 * ((Data[DetailedTimingOffset + (V_ACTIVE_OFFSET) + 2] >> 4) & FOUR_LSBITS);
        TPI_EDID_PRINT("Vertical Active (Lines): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + V_BLANKING_OFFSET] +
                    256 * (Data[DetailedTimingOffset + V_BLANKING_OFFSET + 1] & LOW_NIBBLE);
        TPI_EDID_PRINT("Vertical Blanking (Lines): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + H_SYNC_OFFSET] +
                    256 * ((Data[DetailedTimingOffset + (H_SYNC_OFFSET + 3)] >> 6) & TWO_LSBITS);
        TPI_EDID_PRINT("Horizontal Sync Offset (Pixels): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + H_SYNC_PW_OFFSET] +
                    256 * ((Data[DetailedTimingOffset + (H_SYNC_PW_OFFSET + 2)] >> 4) & TWO_LSBITS);
        TPI_EDID_PRINT("Horizontal Sync Pulse Width (Pixels): %d\n", (int)TmpWord);

        TmpWord = (Data[DetailedTimingOffset + V_SYNC_OFFSET] >> 4) & FOUR_LSBITS +
                    256 * ((Data[DetailedTimingOffset + (V_SYNC_OFFSET + 1)] >> 2) & TWO_LSBITS);
        TPI_EDID_PRINT("Vertical Sync Offset (Lines): %d\n", (int)TmpWord);

        TmpWord = (Data[DetailedTimingOffset + V_SYNC_PW_OFFSET]) & FOUR_LSBITS +
                    256 * (Data[DetailedTimingOffset + (V_SYNC_PW_OFFSET + 1)] & TWO_LSBITS);
        TPI_EDID_PRINT("Vertical Sync Pulse Width (Lines): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + H_IMAGE_SIZE_OFFSET] +
                    256 * (((Data[DetailedTimingOffset + (H_IMAGE_SIZE_OFFSET + 2)]) >> 4) & FOUR_LSBITS);
        TPI_EDID_PRINT("Horizontal Image Size (mm): %d\n", (int)TmpWord);

        TmpWord = Data[DetailedTimingOffset + V_IMAGE_SIZE_OFFSET] +
                    256 * (Data[DetailedTimingOffset + (V_IMAGE_SIZE_OFFSET + 1)] & FOUR_LSBITS);
        TPI_EDID_PRINT("Vertical Image Size (mm): %d\n", (int)TmpWord);

        TmpByte = Data[DetailedTimingOffset + H_BORDER_OFFSET];
        TPI_EDID_PRINT("Horizontal Border (Pixels): %d\n", (int)TmpByte);

        TmpByte = Data[DetailedTimingOffset + V_BORDER_OFFSET];
        TPI_EDID_PRINT("Vertical Border (Lines): %d\n", (int)TmpByte);

        TmpByte = Data[DetailedTimingOffset + FLAGS_OFFSET];
        if (TmpByte & BIT_7)
            TPI_EDID_PRINT("Interlaced\n");
        else
            TPI_EDID_PRINT("Non-Interlaced\n");

        if (!(TmpByte & BIT_5) && !(TmpByte & BIT_6))
            TPI_EDID_PRINT("Normal Display, No Stereo\n");
        else
            TPI_EDID_PRINT("Refer to VESA E-EDID Release A, Revision 1, table 3.17\n");

        if (!(TmpByte & BIT_3) && !(TmpByte & BIT_4))
            TPI_EDID_PRINT("Analog Composite\n");
        if ((TmpByte & BIT_3) && !(TmpByte & BIT_4))
            TPI_EDID_PRINT("Bipolar Analog Composite\n");
        else if (!(TmpByte & BIT_3) && (TmpByte & BIT_4))
            TPI_EDID_PRINT("Digital Composite\n");

        else if ((TmpByte & BIT_3) && (TmpByte & BIT_4))
            TPI_EDID_PRINT("Digital Separate\n");

      TPI_EDID_PRINT("\n");
    }
    
    return TRUE;
}

//------------------------------------------------------------------------------
// Function Name: ParseBlock_0_TimingDescripors()
// Function Description: Parse EDID Block 0 timing descriptors per EEDID 1.3
//                  standard. printf() values to screen.
//
// Accepts: Pointer to the 128 byte array where the data read from EDID Block0 is stored.
// Returns: none
// Globals: EDID data
//------------------------------------------------------------------------------
VOID ParseBlock_0_TimingDescripors (UINT8 *Data, HDMI_EDID_FLAG_T *pt_edid)
{
        UINT8 i;
        UINT8 Offset;

        //ParseEstablishedTiming(Data);
        //ParseStandardTiming(Data);

        for (i = 0; i < NUM_OF_DETAILED_DESCRIPTORS; i++)
        {
            Offset = DETAILED_TIMING_OFFSET + (LONG_DESCR_LEN * i);
            ParseDetailedTiming(Data, Offset, EDID_BLOCK_0, pt_edid);
        }
}

// Support EDID 1.3 only. Pls refer to HDMI 1.3a specification. EDID block1 128 data
static BOOL EDID_Parse861Extensions(UINT8 *p_u8_edid_buf, HDMI_EDID_FLAG_T *pt_edid)
{
    UINT8 u8tagCode;
    UINT8 u8tagLength;
    
    UINT8 u8dataIdx;
    UINT8 u8longDescriptor = 0;
    UINT8 *p_u8_buf = p_u8_edid_buf;
    
    // 02 03
    if (p_u8_buf[EDID_TAG_ADDR] != EDID_EXTENSION_TAG)
    {
        return TRUE; //20170331, update according to si9022 hdmi tx sdk.
    }
    if (p_u8_buf[EDID_REV_ADDR] != EDID_REV_THREE) //20170330, other version set to DVI device
    {
        return TRUE;    
    }
    
    // Support the misc supported-features.
    pt_edid->u8_color_space  = p_u8_buf[MISC_SUPPORT_IDX] & 0xF0;
    
    u8dataIdx = 0;
    u8longDescriptor = p_u8_buf[LONG_DESCR_PTR_IDX];
    while (u8dataIdx < u8longDescriptor)
    {
        p_u8_buf = &p_u8_edid_buf[(EDID_DATA_START + u8dataIdx) % EDID_BLOCK_SIZE];
        
        // $NOTE:
        // BIT: 7    6    5   4   3   2   1   0
        //      | Tag Code| Length of block   |
        //  
        u8tagCode   = (p_u8_buf[0] >> 5) & 0x07;
        u8tagLength = (p_u8_buf[0]) & 0x1F;
        
        if ((u8dataIdx + u8tagLength) > u8longDescriptor)
        {
            return FALSE;
        }
        
        switch (u8tagCode)
        {
        case VIDEO_D_BLOCK:         
        case AUDIO_D_BLOCK:        
        case SPKR_ALLOC_D_BLOCK:
        case USE_EXTENDED_TAG:           
            u8dataIdx += u8tagLength;    // Increase the size.
            u8dataIdx ++;
            break;
            
        case VENDOR_SPEC_D_BLOCK:
            if ((p_u8_buf[1] == 0x03) && (p_u8_buf[2] == 0x0C) && (p_u8_buf[3] == 0x00))
            {
                pt_edid->u8_hdmi_sink = 0x01;    // Yes, it is HDMI sink
                return TRUE;
            }
            break;               
        }  
    } 
    return FALSE;   
}

BOOL ms2160drv_hdmi_tx_parse_edid(UINT8 *p_u8_edid_buf, HDMI_EDID_FLAG_T *pt_edid)
{      
    BOOL bSucc = TRUE;
    // 
    ms2160drv_hdmi_tx_ddc_enable(TRUE); 

    pt_edid->u8_hdmi_sink = 0;            // Reset variable
    pt_edid->u8_color_space = 0;          // Reset variable
    pt_edid->u8_edid_total_blocks = 0;    // Reset variable
    pt_edid->u8_max_pixel_clk = 0xFF;     //
    
    //Reset variable
    memset(p_u8_edid_buf, 0, 256);
    HDMI_DDC_ReadBytes(0xA0, 0, 8, p_u8_edid_buf);     
        
    if (! EDID_CheckHeader(p_u8_edid_buf))
    {
        MS2160_LOG("EDID header failed.");
        bSucc = FALSE;
        goto NEXT;
    }

    //
    HDMI_DDC_ReadBytes(0xA0, EDID_BLOCK_0_OFFSET, EDID_BLOCK_SIZE, p_u8_edid_buf);
    pt_edid->u8_edid_total_blocks = 1;

    //
    ParseBlock_0_TimingDescripors(p_u8_edid_buf, pt_edid);

    // Read more one byte for extension flag.
    // Checking if the EDID extension
    if (p_u8_edid_buf[NUM_OF_EXTEN_ADDR] != 0)    
    {
        pt_edid->u8_edid_total_blocks = 2;
        HDMI_DDC_ReadBytes(0xA0, EDID_BLOCK_1_OFFSET, EDID_BLOCK_SIZE, &p_u8_edid_buf[128]);
        bSucc = EDID_Parse861Extensions(&p_u8_edid_buf[128], pt_edid);
    }
    
NEXT:
    ms2160drv_hdmi_tx_ddc_enable(FALSE);
    return bSucc;
}
#endif //#if MS2160_HDMI_TX_EDID
