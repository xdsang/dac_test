/**
******************************************************************************
* @file    ms2160_drv_hdmi_ddc.h
* @author  
* @version V1.0.0
* @date    12-Jan-2017
* @brief   hdmi tx ddc module driver declare
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_HDMI_DDC_H__
#define __MACROSILICON_MS2160_DRV_HDMI_DDC_H__


#ifdef __cplusplus
extern "C" {
#endif


typedef enum _HDMI_DDC_SPEED_CHOOSE_INFOMATION
{
    DDC_SPEED_20K  = (UINT8)0,  //20Kbit/s
    DDC_SPEED_100K = (UINT8)1,  //100Kbit/s
    DDC_SPEED_400K = (UINT8)2,  //400Kbit/s
    DDC_SPEED_750K = (UINT8)3   //750Kbit/s
} HDMI_DDC_SPEED_E;


VOID ms2160drv_hdmi_ddc_init(VOID);
VOID ms2160drv_hdmi_ddc_set_speed(UINT8 u8_ddc_speed); //enum to HDMI_DDC_SPEED_E

UINT8 ms2160drv_hdmi_ddc_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index);
BOOL ms2160drv_hdmi_ddc_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value);
VOID ms2160drv_hdmi_ddc_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);
VOID ms2160drv_hdmi_ddc_burstwrite_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);


#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_HDMI_DDC_H__
