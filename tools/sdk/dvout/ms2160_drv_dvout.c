/**
******************************************************************************
* @file    ms2160_drv_dvout.c
* @author  
* @version V1.0
* @date    Apr-2018
* @brief   MacroSilicon DVOUT driver source file
*          v1.0 support : MS2160A
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/

#include "common.h"
#include "ms2160_drv_dvout.h"


/****************************     DVOUT Module CLK & SW    ******************************/
#define MS2160_DVOUT_CLK_REG        0xf005
#define MS2160_DVOUT_CLK_MASK       0x10
#define MS2160_DGO2IO_CLK_INV_MASK  0x20

#define MS2160_DVOUT_SW_REG         0xf008
#define MS2160_DVOUT_SW_MASK        0x10
/*************************************     END     **************************************/


/**************************** DVOUT Module Register Define  *****************************/
//
#define MS2160_DVOUT_REGBASE        0xF035
#define MS2160_DVOUT_CTRL0_REG      (MS2160_DVOUT_REGBASE + 0x00)
#define MS2160_DVOUT_CTRL1_REG      (MS2160_DVOUT_REGBASE + 0x01)
#define MS2160_DVOUT_CTRL2_REG      (MS2160_DVOUT_REGBASE + 0x02)
#define MS2160_DVOUT_CTRL3_REG      (MS2160_DVOUT_REGBASE + 0x03)
#define MS2160_DVOUT_CTRL4_REG      (MS2160_DVOUT_REGBASE + 0x04)
/*************************************     END     **************************************/


/****************************************************************************************/
/**********************************  DVOUT  Module  *************************************/
/****************************************************************************************/
static VOID _drv_dvo_clock_en(BOOL b_enable);
static VOID _drv_dvo_sw_reset(BOOL b_on);
static VOID _drv_dvo2io_clock_invert(BOOL b_on);
static VOID _drv_dvo_yuv6t7_convt_en(BOOL b_enable);
static VOID _drv_dvo_yuv2rgb_byps(BOOL b_sel);
static VOID _drv_dvo_yuv_hd_en(BOOL b_enable);
static VOID _drv_dvo_range_sel(BOOL b_sel);
static VOID _drv_dvo_rgb_bit_flip(BOOL b_sel);
static VOID _drv_dvo_yuv_out(BOOL b_enable);
static VOID _drv_dvo_blk_clip_off(BOOL b_enable);
static VOID _drv_dvo_en(BOOL b_enable);
static VOID _drv_dvo_16bit_sel(BOOL b_sel);
static VOID _drv_dvo_8bit_sel(BOOL b_sel);
static VOID _drv_dvo_bt656_mode_en(BOOL b_enable);
static VOID _drv_dvo_bt1120_mode_en(BOOL b_enable);
static VOID _drv_dvo_uv_flip(BOOL b_sel);
static VOID _drv_dvo_yc_flip(BOOL b_sel);
static VOID _drv_dvo_bit_remap(BOOL b_sel);
static VOID _drv_dvo_byte_sel7_0(UINT8 u8_byte);
static VOID _drv_dvo_byte_sel15_8(UINT8 u8_byte);
static VOID _drv_dvo_byte_sel23_16(UINT8 u8_byte);
static VOID _drv_dvo_yuv444t422_sel_div2(BOOL b_sel);
static VOID _drv_dvo_ddr_mode_en(BOOL b_enable);
static VOID _drv_dvo_hsync_sel(UINT8 u8_hs);
static VOID _drv_dvo_vsync_sel(UINT8 u8_vs);



// dvout 
static VOID _drv_dvo_clock_en(BOOL b_enable)
{
    HAL_ToggleBits(MS2160_DVOUT_CLK_REG, MSRT_BIT4, b_enable);
}

static VOID _drv_dvo_sw_reset(BOOL b_on)
{
    HAL_ToggleBits(MS2160_DVOUT_SW_REG, MSRT_BIT4, !b_on);
}

static VOID _drv_dvo2io_clock_invert(BOOL b_on)
{
    HAL_ToggleBits(MS2160_DVOUT_CLK_REG, MSRT_BIT5, b_on);
}

static VOID _drv_dvo_yuv6t7_convt_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL0_REG, MSRT_BIT0, (b_enable) ? MSRT_BIT0 : 0);
}

static VOID _drv_dvo_yuv2rgb_byps(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT0, (b_sel) ? MSRT_BIT0 : 0);
}

static VOID _drv_dvo_yuv_hd_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT1, (b_enable) ? 0 : MSRT_BIT1);
}

static VOID _drv_dvo_range_sel(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT2, (b_sel) ? MSRT_BIT2 : 0);
}

static VOID _drv_dvo_rgb_bit_flip(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT3, (b_sel) ? MSRT_BIT3 : 0);
}

static VOID _drv_dvo_yuv_out(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT4, (b_enable) ? MSRT_BIT4 : 0);
}

static VOID _drv_dvo_blk_clip_off(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL1_REG, MSRT_BIT5, (b_enable) ? MSRT_BIT5 : 0);
}

static VOID _drv_dvo_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT0, (b_enable) ? MSRT_BIT0 : 0);
}

static VOID _drv_dvo_16bit_sel(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT1, (b_sel) ? MSRT_BIT1 : 0);
}

static VOID _drv_dvo_8bit_sel(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT2, (b_sel) ? MSRT_BIT2 : 0);
}

static VOID _drv_dvo_bt656_mode_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT3, (b_enable) ? MSRT_BIT3 : 0);
}

static VOID _drv_dvo_bt1120_mode_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT4, (b_enable) ? MSRT_BIT4 : 0);
}

static VOID _drv_dvo_uv_flip(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT5, (b_sel) ? MSRT_BIT5 : 0);
}

static VOID _drv_dvo_yc_flip(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT6, (b_sel) ? MSRT_BIT6 : 0);
}

static VOID _drv_dvo_bit_remap(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL2_REG, MSRT_BIT7, (b_sel) ? MSRT_BIT7 : 0);
}

static VOID _drv_dvo_byte_sel7_0(UINT8 u8_byte)
{
    HAL_ModBits(MS2160_DVOUT_CTRL3_REG, MSRT_BITS1_0, (UINT8)u8_byte);
}

static VOID _drv_dvo_byte_sel15_8(UINT8 u8_byte)
{
    HAL_ModBits(MS2160_DVOUT_CTRL3_REG, MSRT_BITS3_2, (UINT8)u8_byte << 2);
}

static VOID _drv_dvo_byte_sel23_16(UINT8 u8_byte)
{
    HAL_ModBits(MS2160_DVOUT_CTRL3_REG, MSRT_BITS5_4, (UINT8)u8_byte << 4);
}

static VOID _drv_dvo_yuv444t422_sel_div2(BOOL b_sel)
{
    HAL_ModBits(MS2160_DVOUT_CTRL4_REG, MSRT_BIT4, (b_sel) ? MSRT_BIT4 : 0);
}

static VOID _drv_dvo_ddr_mode_en(BOOL b_enable)
{
    HAL_ModBits(MS2160_DVOUT_CTRL4_REG, MSRT_BIT5, (b_enable) ? MSRT_BIT5 : 0);
}

static VOID _drv_dvo_hsync_sel(UINT8 u8_hs)
{
    HAL_ModBits(MS2160_DVOUT_CTRL4_REG, MSRT_BITS1_0, (UINT8) u8_hs);
}

static VOID _drv_dvo_vsync_sel(UINT8 u8_vs)
{
    HAL_ModBits(MS2160_DVOUT_CTRL4_REG, MSRT_BITS3_2, (UINT8) u8_vs << 2);
}


/*********************************    DVOUT Module   ***********************************/

VOID ms2160drv_dvo_config(VOID)
{
    DVO_PORT_CONTROL_T pst_port_ctl = {0,  //yc_flip FALSE,
                                       0,  //uv_flip FALSE, 
                                       0,  //bit_remap FALSE, 
                                       1,  //DVO_M_DATA15_8, 
                                       0,  //DVO_H_DATA23_16, 
                                       2,  //DVO_L_DATA7_0, 
                                       0,  //DVO_HS_SEL_HS, 
                                       0,  //DVO_VS_SEL_VS
                                       };
    
    ms2160drv_dvo_blk_data_clip(DVO_BLK_CLIP_OFF);
    ms2160drv_dvo_port_control(&pst_port_ctl);
    ms2160drv_dvo_rgb_bit_flip(FALSE);
    ms2160drv_dvo_ddr_en(DISABLE);
    ms2160drv_dvo_yuv_hd_en(ENABLE);
    ms2160drv_dvo_range_sel(FALSE);
}

VOID ms2160drv_dvo_init(VOID)
{
    // dvo module clk & reset
    _drv_dvo_clock_en(TRUE);
    _drv_dvo_sw_reset(FALSE);

    // dvo module enable
    _drv_dvo_en(ENABLE);

    #if MS2160_FPGA_VERIFY
    // use default configuration on FPGA
    ms2160drv_dvo_config();
    #endif
}

BOOL ms2160drv_dvo_get_clock(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CLK_REG) & MSRT_BIT4) == MSRT_BIT4);
}

BOOL ms2160drv_dvo_get_sw_reset(VOID)
{
    return !((HAL_ReadByte(MS2160_DVOUT_SW_REG) & MSRT_BIT4) == MSRT_BIT4);
}

BOOL ms2160drv_dvo_get_module_en(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BIT0) == MSRT_BIT0);
}

VOID ms2160drv_dvo_power_switch(BOOL b_switch)
{
    _drv_dvo_en(b_switch);
    _drv_dvo_sw_reset(!b_switch);
    _drv_dvo_clock_en(b_switch);
}

// black data clip
VOID ms2160drv_dvo_blk_data_clip(UINT8 u8_blk_data)
{
    BOOL b_clip_off = FALSE;
    BOOL b_yuv_out = TRUE;
    // enum to DVO_BLANK_DATA_E
    switch (u8_blk_data)
    {
    case DVO_BLK_CLIP_YUV008080:
        break;
        
    case DVO_BLK_CLIP_RGB000000:
        b_yuv_out = FALSE;
        break;
        
    case DVO_BLK_CLIP_OFF:
        b_clip_off = TRUE;
        break;
        
    default:
        break;
    }
    _drv_dvo_blk_clip_off(b_clip_off);
    _drv_dvo_yuv_out(b_yuv_out);
}

UINT8 ms2160drv_dvo_get_blk_data_clip(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL1_REG) & MSRT_BITS5_4) >> 4);
}

// yuv2rgb byps
VOID ms2160drv_dvo_yuv2rgb_byps(BOOL b_enable)
{
    _drv_dvo_yuv2rgb_byps(b_enable);
}

BOOL ms2160drv_dvo_get_yuv2rgb_byps(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BIT0) == MSRT_BIT0);
}

// rgb bit flip
VOID ms2160drv_dvo_rgb_bit_flip(BOOL b_enable)
{
    _drv_dvo_rgb_bit_flip(b_enable);
}

BOOL ms2160drv_dvo_get_rgb_bit_flip(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL1_REG) & MSRT_BIT3) == MSRT_BIT3);
}

// yuv hd enable
VOID ms2160drv_dvo_yuv_hd_en(BOOL b_enable) // 1: HD  0: SD
{
    _drv_dvo_yuv_hd_en(b_enable);
}

BOOL ms2160drv_dvo_get_yuv_hd_en(VOID)
{
    return !((HAL_ReadByte(MS2160_DVOUT_CTRL1_REG) & MSRT_BIT1) == MSRT_BIT1);
}

// range select
VOID ms2160drv_dvo_range_sel(BOOL b_sel) // 1: 64~940  0: 0~1023
{
    _drv_dvo_range_sel(b_sel);
}

BOOL ms2160drv_dvo_get_range_sel(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL1_REG) & MSRT_BIT2) == MSRT_BIT2);
}

// Port configuration
VOID ms2160drv_dvo_port_control(DVO_PORT_CONTROL_T *pst_port_ctl)
{
    _drv_dvo_yc_flip((BOOL) pst_port_ctl->u8_yc_flip);
    _drv_dvo_uv_flip((BOOL) pst_port_ctl->u8_uv_flip);
    _drv_dvo_bit_remap((BOOL) pst_port_ctl->u8_bit_remap);
    // data select
    _drv_dvo_byte_sel23_16((UINT8) pst_port_ctl->u8_data23_16);
    _drv_dvo_byte_sel15_8((UINT8) pst_port_ctl->u8_data15_8);
    _drv_dvo_byte_sel7_0((UINT8) pst_port_ctl->u8_data7_0);
    // u8_hs: enum to DVO_HS_SEL_E
    _drv_dvo_hsync_sel((UINT8) pst_port_ctl->u8_hsync);
    // u8_cs: enum to DVO_VS_SEL_E
    _drv_dvo_vsync_sel((UINT8) pst_port_ctl->u8_vsync);
}

VOID ms2160drv_dvo_get_port_control(DVO_PORT_CONTROL_T *pst_port_ctl)
{
    pst_port_ctl->u8_bit_remap =  (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BIT7) >> 7);
    pst_port_ctl->u8_yc_flip = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BIT6) >> 6);
    pst_port_ctl->u8_uv_flip = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BIT5) >> 5);

    pst_port_ctl->u8_data23_16 = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL3_REG) & MSRT_BITS5_4) >> 4);
    pst_port_ctl->u8_data15_8 = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL3_REG) & MSRT_BITS3_2) >> 2);
    pst_port_ctl->u8_data7_0 = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL3_REG) & MSRT_BITS1_0));

    pst_port_ctl->u8_hsync = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL4_REG) & MSRT_BITS1_0));
    pst_port_ctl->u8_vsync = (UINT8)((HAL_ReadByte(MS2160_DVOUT_CTRL4_REG) & MSRT_BITS3_2) >> 2);
}

// output mode
VOID ms2160drv_dvo_standard_mode(UINT8 u8_mode) // enum to DVO_STANDARD_MODE_E
{
    BOOL b_656en = DISABLE;
    BOOL b_1120en = DISABLE;
    BOOL b_6t7en = DISABLE;
    BOOL b_8bit_en = DISABLE;
    BOOL b_16bit_en = DISABLE;
    
    switch (u8_mode)
    {
    case DVO_24BIT_RGB:
        break;

    case DVO_8BIT_BT601:
        b_8bit_en = ENABLE;
        break;

    case DVO_16BIT_BT601:
        b_16bit_en = ENABLE;
        break;

    case DVO_8BIT_BT656:
        b_656en = ENABLE;
        b_8bit_en = ENABLE;
        break;

    case DVO_16BIT_BT656:
        b_656en = ENABLE;
        b_16bit_en = ENABLE;
        break;

    case DVO_8BIT_BT709:
        b_6t7en = ENABLE;
        b_8bit_en = ENABLE;
        break;

    case DVO_16BIT_BT709:
        b_6t7en = ENABLE;
        b_16bit_en = ENABLE;
        break;

    case DVO_8BIT_BT1120:
        b_1120en = ENABLE;
        b_8bit_en = ENABLE;
        break;

    default:
        break;
    }
    _drv_dvo_bt656_mode_en(b_656en);
    _drv_dvo_bt1120_mode_en(b_1120en);
    _drv_dvo_yuv6t7_convt_en(b_6t7en);
    _drv_dvo_8bit_sel(b_8bit_en);
    _drv_dvo_16bit_sel(b_16bit_en);
}

UINT8 ms2160drv_dvo_get_standard_mode(VOID)
{
    UINT8 u8_bit_mode = 0;
    UINT8 u8_6t7en = 0;
    UINT8 u8_standard_mode = 0;
    
    u8_6t7en = HAL_ReadByte(MS2160_DVOUT_CTRL0_REG) & MSRT_BIT0;
    u8_bit_mode = HAL_ReadByte(MS2160_DVOUT_CTRL2_REG) & MSRT_BITS4_1;

    switch (u8_6t7en | u8_bit_mode)
    {
    case 0x00:
        u8_standard_mode = (UINT8) DVO_24BIT_RGB;
        break;

    case 0x04:
        u8_standard_mode = (UINT8) DVO_8BIT_BT601;
        break;

    case 0x02:
        u8_standard_mode = (UINT8) DVO_16BIT_BT601;
        break;

    case 0x0c:
        u8_standard_mode = (UINT8) DVO_8BIT_BT656;
        break;

    case 0x0a:
        u8_standard_mode = (UINT8) DVO_16BIT_BT656;
        break;

    case 0x05:
        u8_standard_mode = (UINT8) DVO_8BIT_BT709;
        break;

    case 0x03:
        u8_standard_mode = (UINT8) DVO_16BIT_BT709;
        break;

    case 0x14:
        u8_standard_mode = (UINT8) DVO_8BIT_BT1120;
        break;

    default:
        break;
    }
    
    return u8_standard_mode;
}

// ddr mode enable
VOID ms2160drv_dvo_ddr_en(BOOL b_enable)
{
    _drv_dvo_ddr_mode_en(b_enable);
}

BOOL ms2160drv_dvo_get_ddr_en(VOID)
{
    return ((HAL_ReadByte(MS2160_DVOUT_CTRL4_REG) & MSRT_BIT5) >> 5);
}

/*************************************     END     **************************************/
