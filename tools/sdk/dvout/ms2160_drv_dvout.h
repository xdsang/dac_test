/**
******************************************************************************
* @file    ms2160_drv_dvout.h
* @author  
* @version V1.0
* @date    Apr-2018
* @brief   MacroSilicon dvout driver header file
*          v1.0 support : MS2160A
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/

#ifndef __MACROSILICON_MS2160_DRV_DVOUT_H__
#define __MACROSILICON_MS2160_DRV_DVOUT_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _E_DVO_HS_SEL_
{
    DVO_HS_SEL_HS    = 0,
    DVO_HS_SEL_HDE   = 1,
    DVO_HS_SEL_DE    = 2,
    DVO_HS_SEL_VS    = 3
}DVO_HS_SEL_E;

typedef enum _E_DVO_VS_SEL_
{
    DVO_VS_SEL_VS    = 0,
    DVO_VS_SEL_VDE   = 1,
    DVO_VS_SEL_VS1   = 2,
    DVO_VS_SEL_HS    = 3
}DVO_VS_SEL_E;

typedef enum _E_DVO_BYTE_
{
    DVO_H_DATA23_16 = 0,
    DVO_M_DATA15_8  = 1,
    DVO_L_DATA7_0   = 2
}DVO_DATA_E;

typedef struct _T_DVO_PORT_CONTROL_
{
    UINT8        u8_yc_flip;    // 1: flip  0: not flip
    UINT8        u8_uv_flip;    // 1: flip  0: not flip
    UINT8        u8_bit_remap;  // 1: remap  0: not remap
    UINT8        u8_data23_16;  //  DVO_DATA_E
    UINT8        u8_data15_8;   // DVO_DATA_E
    UINT8        u8_data7_0;    // DVO_DATA_E
    UINT8        u8_hsync;      // DVO_HS_SEL_E
    UINT8        u8_vsync;      // DVO_VS_SEL_E
} DVO_PORT_CONTROL_T;

typedef enum _E_DVO_BLANK_DATA_
{
    DVO_BLK_CLIP_RGB000000 = 0,
    DVO_BLK_CLIP_YUV008080 = 1,
    DVO_BLK_CLIP_OFF = 2
} DVO_BLANK_DATA_E;

typedef enum _E_DVO_STANDARD_MODE_
{
    DVO_24BIT_RGB   = 0,
    DVO_8BIT_BT601  = 1,
    DVO_16BIT_BT601 = 2,
    DVO_8BIT_BT656  = 3,
    DVO_16BIT_BT656 = 4,
    DVO_8BIT_BT709  = 5,
    DVO_16BIT_BT709 = 6,
    DVO_8BIT_BT1120 = 7
}DVO_STANDARD_MODE_E;


/***************************************************************
 *  Function name:   ms2160drv_dvo_init
 *  Description:     dvout module init
 *  Entry:           None
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_init(VOID);

BOOL ms2160drv_dvo_get_clock(VOID);
BOOL ms2160drv_dvo_get_sw_reset(VOID);
BOOL ms2160drv_dvo_get_module_en(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_power_switch
 *  Description:     dvout module power down
 *  Entry:           None
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_power_switch(BOOL b_switch);

/***************************************************************
 *  Function name:   ms2160drv_dvo_blk_data_clip
 *  Description:     Digital video output turn off blank data clip
 *  Entry:           u8_blk_data: DVO_BLK_CLIP_YUV008080
 *                                DVO_BLK_CLIP_RGB000000           
 *                                DVO_BLK_CLIP_OFF
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_blk_data_clip(UINT8 u8_blk_data);  // enum to DVO_BLANK_DATA_E
UINT8 ms2160drv_dvo_get_blk_data_clip(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_yuv2rgb_byps
 *  Description:     yuv2rgb color space convert 
 *  Entry:           b_enable: 1 enable 
 *                             0 disable                            
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_yuv2rgb_byps(BOOL b_enable);
BOOL ms2160drv_dvo_get_yuv2rgb_byps(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_rgb_bit_flip
 *  Description:     bit flip for rgb out
 *  Entry:           b_enable: 1 enable (0->255,1->254,...)
 *                             0 disable
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_rgb_bit_flip(BOOL b_enable);
BOOL ms2160drv_dvo_get_rgb_bit_flip(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_yuv_hd_en
 *  Description:     yuv hd select
 *  Entry:           b_enable: 1 enable   HD
 *                             0 disable  SD
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_yuv_hd_en(BOOL b_enable); // 1: HD  0: SD
BOOL ms2160drv_dvo_get_yuv_hd_en(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_range_sel
 *  Description:     range select
 *  Entry:           b_enable: 1 enable   64~940
 *                             0 disable  0~1023
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_range_sel(BOOL b_sel); // 1: 64~940  0: 0~1023
BOOL ms2160drv_dvo_get_range_sel(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_port_control
 *  Description:     Port configuration
 *  Entry:           pst_port_ctl: 
 *                                 
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_port_control(DVO_PORT_CONTROL_T *pst_port_ctl);
VOID ms2160drv_dvo_get_port_control(DVO_PORT_CONTROL_T *pst_port_ctl);

/***************************************************************
 *  Function name:   ms2160drv_dvo_standard_mode
 *  Description:     digital video output mode
 *  Entry:           u8_mode: DVO_24BIT_RGB  
 *                            DVO_8BIT_BT601 
 *                            DVO_16BIT_BT601
 *                            DVO_8BIT_BT656 
 *                            DVO_16BIT_BT656
 *                            DVO_8BIT_BT709 
 *                            DVO_16BIT_BT709
 *                            DVO_8BIT_BT1120
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_standard_mode(UINT8 u8_mode); // enum to DVO_STANDARD_MODE_E
UINT8 ms2160drv_dvo_get_standard_mode(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dvo_ddr_en
 *  Description:     digital video output for ddr mode
 *  Entry:           b_enable: 1 enable
 *                             0 disable
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dvo_ddr_en(BOOL b_enable);
BOOL ms2160drv_dvo_get_ddr_en(VOID);

#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_DVOUT_H__