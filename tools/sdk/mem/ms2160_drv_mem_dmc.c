/**
******************************************************************************
* @file    ms2160_drv_mem_dmc.c
* @author  
* @version V1.0.0
* @date    24-Aug-2017
* @brief   DMC module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_mem_dmc.h"

//Module DMC register definitions
#define REG_DMC_CLK_ENABLE           (0xF004)  //bit0
#define REG_DMC_RSTZ                 (0xF007)  //bit0

#define REG_DMC_BASE                 (0xF280)

#define REG_DMC_INIT                 (REG_DMC_BASE + 0x0000)
#define REG_DMC_MODE_CSBA            (REG_DMC_BASE + 0x0001)
#define REG_DMC_MODE_LOW             (REG_DMC_BASE + 0x0002)
#define REG_DMC_MODE_HIG             (REG_DMC_BASE + 0x0003)
#define REG_DMC_CFG_TYPE             (REG_DMC_BASE + 0x0007)
#define REG_DMC_CFG_ADDR             (REG_DMC_BASE + 0x0008)
#define REG_DMC_CFG_CSBA             (REG_DMC_BASE + 0x0009)
#define REG_DMC_CFG_PINMX            (REG_DMC_BASE + 0x000A)
#define REG_DMC_CFG_DQMSW            (REG_DMC_BASE + 0x000B)
#define REG_DMC_TW2R_TR2W            (REG_DMC_BASE + 0x000C)
#define REG_DMC_TRP_TRCD             (REG_DMC_BASE + 0x000D)
#define REG_DMC_TRAS_TWR             (REG_DMC_BASE + 0x000E)
#define REG_DMC_TRFC                 (REG_DMC_BASE + 0x000F)
#define REG_DMC_TMRD_REF             (REG_DMC_BASE + 0x0010)
#define REG_DMC_WCYC_RCYC            (REG_DMC_BASE + 0x0011)
#define REG_DMC_WL_RL                (REG_DMC_BASE + 0x0012)
#define REG_DMC_REF_GEN              (REG_DMC_BASE + 0x0013)
#define REG_DMC_ARB_RST              (REG_DMC_BASE + 0x0014)
#define REG_DMC_ARB_AGENT1           (REG_DMC_BASE + 0x0015)


// macro enum structure definitions

// variables definitions

// helper decalarations

// helper definitions


//exported
VOID ms2160drv_mem_dmc_reset_switch(BOOL b_on)
{
    HAL_ModBits((UINT16)REG_DMC_RSTZ, (UINT8)MSRT_BIT0, b_on ? 0x00 : MSRT_BIT0); 
}


VOID ms2160drv_mem_dmc_clk_switch(BOOL b_on)
{
    HAL_ModBits((UINT16)REG_DMC_CLK_ENABLE, (UINT8)MSRT_BIT0, b_on ? MSRT_BIT0 : 0x00); 
}


VOID ms2160drv_mem_dmc_sdram_basic_config(UINT8 u8_sdram_size)
{
    /*1. select sdram TYPE: SDR*/
    HAL_ClrBits(REG_DMC_CFG_TYPE, MSRT_BITS6_4);

    /*2. density*/
    //chips/banks
    HAL_ClrBits(REG_DMC_CFG_TYPE, MSRT_BITS1_0);
    //row, column address width & precharge all bit(AP)
    if(SDRAM_SIZE_16M == u8_sdram_size)
    {
        HAL_ModBits(REG_DMC_CFG_ADDR, MSRT_BITS6_4, 1 << 4);
    }
    else
    {
        HAL_ClrBits(REG_DMC_CFG_ADDR, MSRT_BITS6_4);
    }
    HAL_ClrBits(REG_DMC_CFG_ADDR, MSRT_BITS2_0);
    HAL_ClrBits(REG_DMC_CFG_ADDR, MSRT_BIT7 | MSRT_BIT3);

    /*3. pin map*/
    //chip/bank selection pin map & mux
    HAL_WriteByte(REG_DMC_CFG_CSBA, 0x24);
    HAL_WriteByte(REG_DMC_CFG_PINMX, 0x01);
    //data-bus width & map
    if(SDRAM_SIZE_2M == u8_sdram_size)
    {
        HAL_ClrBits(REG_DMC_CFG_TYPE, MSRT_BIT2);
        //HAL_WriteByte(REG_DMC_CFG_PINMX, 0xe1);  //use CS/BA pin mux for 1Mx16bits SDRAM
    }
    else
    {
        HAL_SetBits(REG_DMC_CFG_TYPE, MSRT_BIT2);
    }

    /*4. refresh*/
    //refresh mode: self-counter trigger.
    HAL_ModBits(REG_DMC_REF_GEN, MSRT_BITS5_4, 0x30);
    //refresh ratio: 1x
    HAL_ModBits(REG_DMC_TMRD_REF, MSRT_BITS2_0, 0x01);
    //refresh period: TCK_7NS_64MS_4KROW
    HAL_ModBits(REG_DMC_REF_GEN, MSRT_BITS3_0, 0x02);
}



VOID ms2160drv_mem_dmc_sdram_init_trigger(VOID)
{
    HAL_SetBits((UINT16)REG_DMC_INIT, (UINT8)MSRT_BIT0);
}
