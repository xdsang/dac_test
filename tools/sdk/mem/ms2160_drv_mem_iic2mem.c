/**
******************************************************************************
* @file    ms2160_drv_mem_iic2mem.c
* @author  
* @version V1.0.0
* @date    30-Aug-2014
* @brief   i2c read write memory driver source file
* @history    
*
* Copyright (c) 2009 - , MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_mem_iic2mem.h"

// register definitions
#define REG_CPU2MEM_BANK_L         (0xF028)
#define REG_CPU2MEM_BANK_H         (0xF029)
#define REG_CPU2MEM_ADDR_START     (0x8000)

// macro enum structure definitions
#define CPU2MEM_BLOCK_SIZE   (0x0400)
#define BIT_MF_CLEAN_BIT     MSRT_BIT4
#define BIT_MD_LATCH         MSRT_BIT5

// variables definitions

// helper decalarations
static VOID _drv_mem_read_bytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value);
static VOID _drv_access_memory(BOOL b_write_enable, UINT32 startAddr, UINT32 endAddr, UINT8 *pValue);

// helper defenitions
static VOID _drv_mem_read_bytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value)
{
    UINT16 i;

    for (i = 0; i < u16_length; i++)
    {
        *(p_u8_value + i) = HAL_ReadByte(u16_index + i);
        *(p_u8_value + i) = HAL_ReadByte(u16_index + i);
    }
}

static VOID _drv_access_memory(BOOL b_write_enable, UINT32 startAddr, UINT32 endAddr, UINT8 *pValue)
{
    UINT16 i = 0;
    UINT16 offset = 0;
    UINT8 regf028_start = (UINT8)(startAddr / CPU2MEM_BLOCK_SIZE);
    UINT8 regf028_end = (UINT8)(endAddr / CPU2MEM_BLOCK_SIZE);

    if (regf028_end == regf028_start)
    {
        HAL_WriteByte(REG_CPU2MEM_BANK_L, regf028_start);
        HAL_ModBits(REG_CPU2MEM_BANK_H, MSRT_BITS3_0, (UINT8)((regf028_start & 0xF00) >> 8));

        i = startAddr % CPU2MEM_BLOCK_SIZE;
        if (b_write_enable)
        {
            HAL_WriteBytes(i*4+REG_CPU2MEM_ADDR_START, (UINT16)((endAddr-startAddr+1)*4), pValue);
        }
        else
        {
            _drv_mem_read_bytes(i*4+REG_CPU2MEM_ADDR_START, (UINT16)((endAddr-startAddr+1)*4), pValue);
        }

        return;
    }

    // Step 1
    if (regf028_end > regf028_start)
    {
        HAL_WriteByte(REG_CPU2MEM_BANK_L, regf028_start);
        HAL_ModBits(REG_CPU2MEM_BANK_H, MSRT_BITS3_0, (UINT8)((regf028_start & 0xF00) >> 8));

        i = startAddr % CPU2MEM_BLOCK_SIZE;
        if (b_write_enable)
        {
            HAL_WriteBytes(i*4+REG_CPU2MEM_ADDR_START, (CPU2MEM_BLOCK_SIZE-i)*4, pValue);
        }
        else
        {
            _drv_mem_read_bytes(i*4+REG_CPU2MEM_ADDR_START, (CPU2MEM_BLOCK_SIZE-i)*4, pValue);
        }
        offset += ((CPU2MEM_BLOCK_SIZE-i)*4);
    }

    // Step 2
    if (regf028_end - regf028_start > 1)
    {
        for (i=1; i < (regf028_end - regf028_start); i++)
        {
            HAL_WriteByte(REG_CPU2MEM_BANK_L, regf028_start+i);
            HAL_ModBits(REG_CPU2MEM_BANK_H, MSRT_BITS3_0, (UINT8)(((regf028_start + i) & 0xF00) >> 8));

            if (b_write_enable)
            {
                HAL_WriteBytes(REG_CPU2MEM_ADDR_START, CPU2MEM_BLOCK_SIZE*4, pValue+offset);
            }
            else
            {
                _drv_mem_read_bytes(REG_CPU2MEM_ADDR_START, CPU2MEM_BLOCK_SIZE*4, pValue+offset);
            }
            offset += REG_CPU2MEM_ADDR_START;
        }
    }

    // Step 3
    if (regf028_end > regf028_start)
    {
        HAL_WriteByte(REG_CPU2MEM_BANK_L, regf028_end);
        HAL_ModBits(REG_CPU2MEM_BANK_H, MSRT_BITS3_0, (UINT8)((regf028_end & 0xF00) >> 8));

        i = endAddr % CPU2MEM_BLOCK_SIZE;
        if (b_write_enable)
        {
            HAL_WriteBytes(REG_CPU2MEM_ADDR_START, (i+1)*4, pValue+offset);
        }
        else
        {
            _drv_mem_read_bytes(REG_CPU2MEM_ADDR_START, (i+1)*4, pValue+offset);
        }
    }
}

// exported APIs
VOID ms2160drv_mem_iic2mem_write_memory(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value)
{
    _drv_access_memory(1, u32_addr_st, u32_addr_st + u32_length - 1, p_u8_value);
}

VOID ms2160drv_mem_iic2mem_read_memory(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value)
{
    _drv_access_memory(0, u32_addr_st, u32_addr_st + u32_length - 1, p_u8_value);
}
