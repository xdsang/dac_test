/**
******************************************************************************
* @file    ms2160_drv_mem_dmc.h
* @author  
* @version V1.0.0
* @date    24-Aug-2017
* @brief   DMC module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_MEM_DMC_H__
#define __MACROSILICON_MS2160_DRV_MEM_DMC_H__

// driver interface for macro enum structure declarations
//

#ifdef __cplusplus
extern "C" {
#endif
//

//for application
/***************************************************************
*  Function name:   ms2160drv_mem_dmc_clk_switch
*  Description:     Reset the DMC module
*  Entry:           [IN]b_on
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dmc_clk_switch(BOOL b_on);



/***************************************************************
*  Function name:   ms2160drv_mem_dmc_reset_switch
*  Description:     Reset the DMC module
*  Entry:           [IN]b_on
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dmc_reset_switch(BOOL b_on);



/***************************************************************
*  Function name:   ms2160drv_mem_dmc_sdram_basic_config
*  Description:     config sdram BASIC parameters
*  Entry:           UINT8 u8_sdram_size: 2/4/8MBytes sdram
*                  
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dmc_sdram_basic_config(UINT8 u8_sdram_size);



/***************************************************************
*  Function name: ms2160drv_mem_dmc_sdram_init_trigger
*  Description:     trigger sdram initialization.
*  Entry:           None
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/

VOID ms2160drv_mem_dmc_sdram_init_trigger(VOID);



#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_MEM_DMC_H__
