/**
******************************************************************************
* @file    ms2160_drv_mem_iic2mem.h
* @author  
* @version V1.0.0
* @date    30-Aug-2017
* @brief   i2c read write memory driver header file
* @history    
*
* Copyright (c) 2009 - , MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_MEM_RW_H__
#define __MACROSILICON_MS2160_DRV_MEM_RW_H__

#ifdef __cplusplus
extern "C" {
#endif 

//cpu2memory, p_u8_value size = u32_length * 4, because memory is 32bit width
/***************************************************************
*  Function name:   ms2160drv_mem_iic2mem_write_memory
*  Description:     write memory interface
*  Entry:             [IN] u32_addr_st: start address
*                        [IN] u32_length: length
*                        [IN] p_u8_value: data pointer
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_iic2mem_write_memory(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value);

/***************************************************************
*  Function name:  ms2160drv_mem_iic2mem_read_memory
*  Description:     read memory interface
*  Entry:             [IN] u32_addr_st: start address
*                        [IN] u32_length: length
*                        [OUT] p_u8_value: read out data pointer
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_iic2mem_read_memory(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value);

#endif

#ifdef __cplusplus
}
#endif  // __MACROSILICON_MS2160_DRV_MEM_RW_H__
