/**
******************************************************************************
* @file    ms2160_drv_mem_dfi.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   DFI module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_MEM_DFI_H__
#define __MACROSILICON_MS2160_DRV_MEM_DFI_H__

// driver interface for macro enum structure declarations

typedef enum
{
    DFI_SDRAM_DQ_WIDTH_32BIT = 0,
    DFI_SDRAM_DQ_WIDTH_16BIT_LOW   = 2,
    DFI_SDRAM_DQ_WIDTH_16BIT_HIGH  = 3
}DFI_SDRAM_DQ_WIDTH_E;


#ifdef __cplusplus
extern "C" {
#endif
//

/***************************************************************
*  Function name:   ms2160drv_mem_dfi_reset_switch
*  Description:     Reset the DFI module
*  Entry:           [IN]b_on
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dfi_reset_switch(BOOL b_on);


/***************************************************************
*  Function name:   ms2160drv_mem_dfi_soft_reset_switch
*  Description:     Soft-reset the DFI module
*  Entry:           [IN]b_on
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dfi_soft_reset_switch(BOOL b_on);


/***************************************************************
*  Function name:   ms2160drv_mem_dfi_ldo_switch
*  Description:     LDO which used for PA output and over drive enable
*  Entry:           [IN]b_on, b_over_drv_en
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dfi_ldo_switch(BOOL b_on, BOOL b_over_drv_en);


/***************************************************************
*  Function name:   ms2160drv_mem_dfi_sdram_dq_map
*  Description:     sdram dq bus map
*  Entry:           [IN]u8_dq_width, refer to DFI_SDRAM_DQ_WIDTH_E
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_mem_dfi_sdram_dq_map(UINT8 u8_dq_width);


/***************************************************************
*  Function name:   ms2160drv_mem_dfi_ktiming
*  Description:     do dfi k-timing
*  Entry:           [IN]b_mode_auto
*                   
* 
*  Returned value:  None
*  Remark:
***************************************************************/
BOOL ms2160drv_mem_dfi_ktiming(BOOL b_mode_auto);


#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_MEM_DFI_H__
