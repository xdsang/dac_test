/**
******************************************************************************
* @file    ms2160_drv_mem_dfi.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   DFI module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_mem_dfi.h"

#define REG_DFI_RSTZ                 (0xF007) //bit1
#define REG_DFI_SOFT_RST             (0xF294) //bit1

#define REG_DFI_BASE                 (0xF300)

#define REG_DFI_SELFT                (REG_DFI_BASE + 0x0000)
#define REG_DFI_PA_SEL               (REG_DFI_BASE + 0x0001)
#define REG_DFI_DFIRD                (REG_DFI_BASE + 0x0002)
#define REG_DFI_IESEL                (REG_DFI_BASE + 0x0003)
#define REG_DFI_DLYCKO               (REG_DFI_BASE + 0x0004)
#define REG_DFI_DLYDSO               (REG_DFI_BASE + 0x0005)
#define REG_DFI_DLYCTO               (REG_DFI_BASE + 0x0006)
#define REG_DFI_DLYDSI               (REG_DFI_BASE + 0x0007)
#define REG_DFI_DLYDQI               (REG_DFI_BASE + 0x0008)
#define REG_DFI_NP_CACK              (REG_DFI_BASE + 0x0009)
#define REG_DFI_NP_DSDT              (REG_DFI_BASE + 0x000A)
#define REG_DFI_TX_CK                (REG_DFI_BASE + 0x000B)
#define REG_DFI_TX_CA                (REG_DFI_BASE + 0x000C)
#define REG_DFI_TX_DQ                (REG_DFI_BASE + 0x000D)
#define REG_DFI_TX_DS                (REG_DFI_BASE + 0x000E)
#define REG_DFI_PA_CTL               (REG_DFI_BASE + 0x000F)
#define REG_DFI_PAD_CTL4             (REG_DFI_BASE + 0x0014)
#define REG_DFI_DRAM_CL              (REG_DFI_BASE + 0x0015)
#define REG_DFI_IOEN_CTL             (REG_DFI_BASE + 0x0016)
#define REG_DFI_DLYCMDI              (REG_DFI_BASE + 0x0017)
#define REG_DFI_RO_REG18             (REG_DFI_BASE + 0x0018)
#define REG_DFI_AK_EN                (REG_DFI_BASE + 0x0020)
#define REG_DFI_AK_PHYR              (REG_DFI_BASE + 0x0021)
#define REG_DFI_AK_CKPHS             (REG_DFI_BASE + 0x0022)
#define REG_DFI_AK_CKPHE             (REG_DFI_BASE + 0x0023)
#define REG_DFI_AK_PSLM1             (REG_DFI_BASE + 0x0024)
#define REG_DFI_AK_DMCR              (REG_DFI_BASE + 0x0025)
#define REG_DFI_AK_DSPHS             (REG_DFI_BASE + 0x0026)
#define REG_DFI_AK_DSPHE             (REG_DFI_BASE + 0x0027)
#define REG_DFI_AK_PSLM2             (REG_DFI_BASE + 0x0028)
#define REG_DFI_AK_IE                (REG_DFI_BASE + 0x0029)
#define REG_DFI_SELFT_CFG            (REG_DFI_BASE + 0x002A)
#define REG_DFI_RESERVED             (REG_DFI_BASE + 0x002B)
#define REG_DFI_PA_TEST              (REG_DFI_BASE + 0x002C)
#define REG_DFI_PA_REV               (REG_DFI_BASE + 0x002D)
#define REG_DFI_PAD_CTL5             (REG_DFI_BASE + 0x002E)
#define REG_DFI_PAD_CTL6             (REG_DFI_BASE + 0x002F)

// macro enum structure definitions

// variables definitions

// helper decalarations
static BOOL _drv_dfi_manuk(VOID);
static BOOL _drv_dfi_autok(VOID);
static VOID _drv_dfi_autok_default_config(VOID);
static BOOL _drv_dfi_autok_get_result(VOID);
static UINT8 _drv_dfi_pam_get_pa_sel(VOID);
static UINT8 _drv_dfi_pam_get_dmc_rd_sel(VOID);
static UINT8 _drv_dfi_pam_get_dfi_rd_sel(VOID);
static UINT8 _drv_dfi_pam_get_dfi_ie_sel(VOID);
static UINT8 _drv_dfi_pam_get_oe_ctrl(VOID);
static UINT8 _drv_dfi_pam_get_ie_ctrl(VOID);
static VOID _drv_dfi_pam_power_on(BOOL b_on);
static VOID _drv_dfi_pam_bypass(BOOL b_bypass);
static VOID _drv_dfi_pam_phase_trigger(VOID);
static VOID _drv_dfi_selftest_trigger(VOID);
static BOOL _drv_dfi_selftest_get_result(VOID);
static VOID _drv_dfi_autok_enable(BOOL b_enable);
static VOID _drv_dfi_autok_trigger(VOID) ;
static BOOL _drv_dfi_autok_get_done(VOID);
static VOID _drv_dfi_pam_set_pa_sel(UINT8 u8_pasel);
static VOID _drv_dfi_pam_set_dmc_rd_sel(UINT8 u8_dmc_rd_sel);
static VOID _drv_dfi_pam_set_dfi_rd_sel(UINT8 u8_dfi_rd_sel);
static VOID _drv_dfi_pam_set_dfi_ie_sel(UINT8 u8_dfi_ie_sel);
static VOID _drv_dfi_pam_set_oe_ctrl(UINT8 u8_oe_ctrl);
static VOID _drv_dfi_pam_set_ie_ctrl(UINT8 u8_ie_ctrl);
static VOID _drv_dfi_pam_icp_config(UINT8 u8_reg_value);
static VOID _drv_dfi_autok_set_dfi_rd_range(UINT8 u8_begin, UINT8 u8_end);
static VOID _drv_dfi_autok_set_dmc_rd_range(UINT8 u8_begin, UINT8 u8_end);
static VOID _drv_dfi_autok_set_phase_range(UINT8 u8_begin, UINT8 u8_end);



// helper definitions
static VOID _drv_dfi_autok_set_dmc_rd_range(UINT8 u8_begin, UINT8 u8_end)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_begin > 0x0f ? 0x0f : u8_begin;
    u8_tmp |= (u8_end > 0x0f ? 0x0f : u8_end) << 4;

    HAL_WriteByte(REG_DFI_AK_DMCR, u8_tmp);
}


static VOID _drv_dfi_autok_set_phase_range(UINT8 u8_begin, UINT8 u8_end)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_begin > 0x1f ? 0x1f : u8_begin;
    HAL_ModBits(REG_DFI_AK_CKPHS, MSRT_BITS4_0, u8_tmp);

    u8_tmp = u8_end > 0x1f ? 0x1f : u8_end;
    HAL_ModBits(REG_DFI_AK_CKPHE, MSRT_BITS4_0, u8_tmp);
}

static VOID _drv_dfi_autok_set_dfi_rd_range(UINT8 u8_begin, UINT8 u8_end)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_begin > 0x0f ? 0x0f : u8_begin;
    u8_tmp |= (u8_end > 0x0f ? 0x0f : u8_end) << 4;

    HAL_WriteByte(REG_DFI_AK_PHYR, u8_tmp);
}

static VOID _drv_dfi_pam_icp_config(UINT8 u8_reg_value)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_reg_value > 0x0f ? 0x0f : u8_reg_value;

    HAL_ModBits(REG_DFI_PA_CTL, MSRT_BITS3_0, u8_tmp);
}

static VOID _drv_dfi_pam_set_pa_sel(UINT8 u8_pasel)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_pasel > 0x1f ? 0x1f : u8_pasel;
    
    HAL_ModBits((UINT16)REG_DFI_PA_SEL, (UINT8)MSRT_BITS4_0, u8_tmp);
}


static VOID _drv_dfi_pam_set_dmc_rd_sel(UINT8 u8_dmc_rd_sel)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_dmc_rd_sel > 6 ? 6 : u8_dmc_rd_sel;
    
    HAL_ModBits((UINT16)REG_DFI_DFIRD, MSRT_BITS7_4, u8_tmp << 4);
}


static VOID _drv_dfi_pam_set_dfi_rd_sel(UINT8 u8_dfi_rd_sel)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_dfi_rd_sel > 6 ? 6 : u8_dfi_rd_sel;

    HAL_ModBits((UINT16)REG_DFI_DFIRD, (UINT8)MSRT_BITS3_0, u8_tmp);
}


static VOID _drv_dfi_pam_set_dfi_ie_sel(UINT8 u8_dfi_ie_sel)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_dfi_ie_sel > 3 ? 3 : u8_dfi_ie_sel;

    HAL_ModBits(REG_DFI_IESEL, MSRT_BITS3_0, u8_tmp);
}


static VOID _drv_dfi_pam_set_oe_ctrl(UINT8 u8_oe_ctrl)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_oe_ctrl > 3 ? 3 : u8_oe_ctrl;

    HAL_ModBits(REG_DFI_IOEN_CTL, MSRT_BITS7_4, u8_tmp << 4);
}


static VOID _drv_dfi_pam_set_ie_ctrl(UINT8 u8_ie_ctrl)
{
    UINT8 u8_tmp = 0;

    u8_tmp = u8_ie_ctrl > 3 ? 3 : u8_ie_ctrl;

    HAL_ModBits(REG_DFI_IOEN_CTL, MSRT_BITS3_0, u8_tmp);
}

static VOID _drv_dfi_autok_enable(BOOL b_enable)
{
    HAL_WriteByte((UINT16)REG_DFI_AK_EN, (b_enable) ? 0x80 : 0x00);
}


static VOID _drv_dfi_autok_trigger(VOID) 
{
    HAL_SetBits((UINT16)REG_DFI_AK_EN, (UINT8)MSRT_BIT0);
}


static BOOL _drv_dfi_autok_get_done(VOID)
{
    if ((HAL_ReadByte((UINT16)REG_DFI_AK_EN) & (UINT8)MSRT_BIT0) == (UINT8)MSRT_BIT0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static VOID _drv_dfi_pam_phase_trigger(VOID)
{
    HAL_SetBits((UINT16)REG_DFI_PA_CTL, (UINT8)MSRT_BIT7);
    Delay_us(1);
    HAL_ClrBits((UINT16)REG_DFI_PA_CTL, (UINT8)MSRT_BIT7);
}


static VOID _drv_dfi_selftest_trigger(VOID)
{
    HAL_SetBits((UINT16)REG_DFI_SELFT, (UINT8)MSRT_BIT0);
}


static BOOL _drv_dfi_selftest_get_result(VOID)
{
    return (HAL_ReadByte((UINT16)REG_DFI_SELFT) & (UINT8)MSRT_BIT0);
}


static VOID _drv_dfi_pam_power_on(BOOL b_on)
{
    HAL_ModBits((UINT16)REG_DFI_PA_CTL, MSRT_BIT5, b_on ? 0x00 : (UINT8)MSRT_BIT5);
}

static VOID _drv_dfi_pam_bypass(BOOL b_bypass)
{
    HAL_ModBits(REG_DFI_PA_CTL, MSRT_BIT4, b_bypass ? MSRT_BIT4 : 0x00);
}

static BOOL _drv_dfi_manuk(VOID)
{
    BOOL   b_loop = FALSE;
    UINT8  u8_phaseHold, u8_phase;
    UINT8  st1 = 0, len0 = 0, len1 = 0, st2 = 0, len2 = 0;
    u8_phaseHold = _drv_dfi_pam_get_pa_sel();
    _drv_dfi_pam_power_on(TRUE);
    _drv_dfi_autok_enable(FALSE);
    // PA power on after 100 us stability
    Delay_us(200);
    for(u8_phase=0;u8_phase<0x20;u8_phase++)
    {
        _drv_dfi_pam_set_pa_sel(u8_phase);
        _drv_dfi_pam_phase_trigger();
        _drv_dfi_selftest_trigger();
        //108MHz memory clock need delay 20us at least;
        Delay_us(60);
        if(_drv_dfi_selftest_get_result())
        {
            if (st2==0)
            {
                len0++;
            }
            len2++;
            b_loop = 1;
        }
        else
        {
            if(len2 > len1)
            {
                st1 = st2; 
                len1 = len2;
            }
            st2 = u8_phase+1;
            len2 = 0;
            b_loop = 0;
        }
    }
    if(b_loop)
    {
        if((len0+len2) > len1)
        {
            st1 = st2;
            len1 = (len2+len0);
        }
    }
    if(len1 == 0)
    {
        _drv_dfi_pam_set_pa_sel(u8_phaseHold);
        _drv_dfi_pam_phase_trigger();
        return FALSE;
    }
    else
    {
        u8_phase = (st1+ (len1>>1) )%0x20;
        _drv_dfi_pam_set_pa_sel(u8_phase);
        _drv_dfi_pam_phase_trigger();
        return TRUE;
    }
}


static BOOL _drv_dfi_autok(VOID)
{
    _drv_dfi_pam_power_on(TRUE);
    Delay_us(200);
    _drv_dfi_pam_phase_trigger();
 
    _drv_dfi_autok_enable(TRUE);
    
    _drv_dfi_autok_trigger();
    
    Delay_ms(10);
    
    _drv_dfi_autok_enable(FALSE);
    
    if(!_drv_dfi_autok_get_result())
    {
        _drv_dfi_autok_default_config();
        return FALSE;
    }

    return TRUE;
}



static VOID _drv_dfi_autok_default_config(VOID)
{
    //enable autok tck_table from register
    HAL_ClrBits(REG_DFI_AK_EN, MSRT_BITS6_4);

    //tck table config
    _drv_dfi_pam_set_dfi_rd_sel(0);
    _drv_dfi_pam_set_dmc_rd_sel(3);
    _drv_dfi_pam_set_dfi_ie_sel(0);
    _drv_dfi_pam_set_ie_ctrl(3);
    _drv_dfi_pam_set_oe_ctrl(0);
    _drv_dfi_pam_icp_config(2);
    _drv_dfi_pam_set_pa_sel(0);
    
    //calibration loop selection: dfi_rd_range, dmc_rd_range and pa_range
    _drv_dfi_autok_set_dfi_rd_range(0, 3);
    _drv_dfi_autok_set_dmc_rd_range(0, 5);
    _drv_dfi_autok_set_phase_range(0, 31);

    _drv_dfi_pam_phase_trigger();
}


static BOOL _drv_dfi_autok_get_result(VOID)
{
    if ((HAL_ReadByte((UINT16)REG_DFI_AK_EN) & (UINT8)MSRT_BITS1_0) == (UINT8)MSRT_BITS1_0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static UINT8 _drv_dfi_pam_get_pa_sel(VOID)
{
    return (HAL_ReadByte((UINT16)REG_DFI_PA_SEL) & (UINT8)MSRT_BITS4_0);
}


static UINT8 _drv_dfi_pam_get_dmc_rd_sel(VOID)
{
    return ((HAL_ReadByte((UINT16)REG_DFI_DFIRD) & (UINT8)MSRT_BITS7_4) >> 4);
}


static UINT8 _drv_dfi_pam_get_dfi_rd_sel(VOID)
{
    return (HAL_ReadByte((UINT16)REG_DFI_DFIRD) & (UINT8)MSRT_BITS3_0);
}


static UINT8 _drv_dfi_pam_get_dfi_ie_sel(VOID)
{
    return (HAL_ReadByte(REG_DFI_IESEL) & MSRT_BITS3_0);
}


static UINT8 _drv_dfi_pam_get_oe_ctrl(VOID)
{
    return ((HAL_ReadByte(REG_DFI_IOEN_CTL) & MSRT_BITS7_4) >> 4);
}

static UINT8 _drv_dfi_pam_get_ie_ctrl(VOID)
{
    return (HAL_ReadByte(REG_DFI_IOEN_CTL) & MSRT_BITS3_0);
}


//exported
VOID ms2160drv_mem_dfi_soft_reset_switch(BOOL b_on)
{
    HAL_ModBits((UINT16)REG_DFI_SOFT_RST, (UINT8)MSRT_BIT1, b_on ? MSRT_BIT1 : 0x00); 
}


VOID ms2160drv_mem_dfi_reset_switch(BOOL b_on)
{
    HAL_ModBits((UINT16)REG_DFI_RSTZ, (UINT8)MSRT_BIT1, b_on ? 0x00 : MSRT_BIT1); 
}


VOID ms2160drv_mem_dfi_ldo_switch(BOOL b_on, BOOL b_over_drv_en)
{
    //ldo output switch
    HAL_ToggleBits(REG_DFI_PA_REV, MSRT_BIT0, b_on);

    //ldo over driver switch
    HAL_ToggleBits(REG_DFI_PA_REV, MSRT_BIT1, b_over_drv_en);    
}


VOID ms2160drv_mem_dfi_sdram_dq_map(UINT8 u8_dq_width)
{
    HAL_ModBits(REG_DFI_SELFT_CFG, MSRT_BITS2_1, u8_dq_width << 1);
}


BOOL ms2160drv_mem_dfi_ktiming(BOOL b_mode_auto)
{
    if(b_mode_auto)
    {
        return _drv_dfi_autok();
    }
    else
    {
        return _drv_dfi_manuk();
    }
}
