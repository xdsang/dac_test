/**
******************************************************************************
* @file    ms928x_adc.h
* @author  
* @version V1.0.0
* @date    15-Oct-2014
* @brief   MacroSilicon(MS183X) ADC driver
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS928X_DRV_HDMI_PHY_H__
#define __MACROSILICON_MS928X_DRV_HDMI_PHY_H__


#ifdef __cplusplus
extern "C" {
#endif

VOID ms9281b_HAL_IOStream(UINT8* pStream);

//i2c for ms9281b
UINT8 ms9281b_HAL_ReadByte(UINT16 u16_index);

VOID ms9281b_HAL_WriteByte(UINT16 u16_index, UINT8 u8_value);

VOID ms9281b_HAL_ModBits(UINT16 u16_index, UINT8 u8_mask, UINT8 u8_value);

VOID ms9281b_HAL_SetBits(UINT16 u16_index, UINT8 u8_mask);

VOID ms9281b_HAL_ClrBits(UINT16 u16_index, UINT8 u8_mask);

UINT16 ms9281b_HAL_ReadRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length);

VOID ms9281b_HAL_WriteRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length, UINT16 u16_value);

VOID ms928x_hdmi_phy_init_for_fpga(VOID);

BOOL ms928x_hdmi_phy_config_for_fpga(VOID);

#ifdef __cplusplus
}
#endif

#endif // __MACROSILICON_MS928X_DRV_HDMI_PHY_H__