/**
******************************************************************************
* @file    ms2160_drv_vcrsff.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   vcrsff module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_VCRSFF_H__
#define __MACROSILICON_MS2160_DRV_VCRSFF_H__

// driver interface for macro enum structure declarations

#ifdef __cplusplus
extern "C" {
#endif

VOID ms2160drv_vcrsff_clk_switch(BOOL b_on);
VOID ms2160drv_vcrsff_reset(BOOL b_reset);
VOID ms2160drv_vcrsff_config(VOID);

#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_VCRSFF_H__
