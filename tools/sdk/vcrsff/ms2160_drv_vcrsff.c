/**
******************************************************************************
* @file    ms2160_drv_vcrsff.c
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   vcrsff module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_vcrsff.h"

#define REG_VCRSFF_CLK_EN              (0xF004) //bit4
#define REG_VCRSFF_RSTB                (0xF007) //bit6

#define REG_VCRSFF_BASE                (0xF340)

#define REG_VCRSFF_EN                  (REG_VCRSFF_BASE + 0x0000)
#define REG_VCRSFF_SIZE                (REG_VCRSFF_BASE + 0x0001)
#define REG_VCRSFF_EMPTY_TH_L          (REG_VCRSFF_BASE + 0x0002)
#define REG_VCRSFF_FULL_TH_L           (REG_VCRSFF_BASE + 0x0004)
#define REG_VCRSFF_STATUS              (REG_VCRSFF_BASE + 0x0006)
#define REG_VCRSFF_RD_LAT              (REG_VCRSFF_BASE + 0x0007)
#define REG_VCRSFF_DATA_REMAIN_L       (REG_VCRSFF_BASE + 0x0008)
#define REG_VCRSFF_DATA_REMAIN_H       (REG_VCRSFF_BASE + 0x000A)
#define REG_VCRSFF_DATA_REMAIN_MAX_L   (REG_VCRSFF_BASE + 0x000B)
#define REG_VCRSFF_DATA_REMAIN_MIN_L   (REG_VCRSFF_BASE + 0x000D)


// macro enum structure definitions
typedef enum
{
    VCRSFF_SIZE_16K = 0,
    VCRSFF_SIZE_32K,
    VCRSFF_SIZE_48K,
    VCRSFF_SIZE_64K,
    VCRSFF_SIZE_80K,
    VCRSFF_SIZE_96K,
    VCRSFF_SIZE_112K,
    VCRSFF_SIZE_128K
}VCRSFF_SIZE_E;

// variables definitions

// helper decalarations


// helper definitions


//exported
VOID ms2160drv_vcrsff_clk_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VCRSFF_CLK_EN, MSRT_BIT4, b_on);
}

VOID ms2160drv_vcrsff_reset(BOOL b_reset)
{
    HAL_ToggleBits(REG_VCRSFF_RSTB, MSRT_BIT6, !b_reset); 
}

VOID ms2160drv_vcrsff_config(VOID)
{
    //disable vcrsff
    HAL_ClrBits(REG_VCRSFF_EN, MSRT_BIT0);

    //size selection: keep default 128KBytes
    HAL_ModBits(REG_VCRSFF_SIZE, MSRT_BITS2_0, VCRSFF_SIZE_128K);

    //clear fifo error status
    HAL_SetBits(REG_VCRSFF_EN, MSRT_BIT4);
    HAL_ClrBits(REG_VCRSFF_EN, MSRT_BIT4);

    //enable vcrsff
    HAL_SetBits(REG_VCRSFF_EN, MSRT_BIT0);
}

