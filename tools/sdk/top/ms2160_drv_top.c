/**
******************************************************************************
* @file    ms2160_drv_top.c
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   top module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160top.h"
#include "ms2160_drv_top.h"

// macro enum structure definitions
#define  MS2160_CHIP_ID0     (0xA7)
#define  MS2160_CHIP_ID1     (0x16)
#define  MS2160_CHIP_ID2     (0x0A)


// variables definitions
static UINT8 g_u8_top_video_in_color_space = 1; //refer to VIDEO_COLOR_SPACE_E
static UINT8 g_u8_top_video_out_color_space = 1; //refer to VIDEO_COLOR_SPACE_E

// helper decalarations
static BOOL _drv_top_chipisvalid(VOID);
static VOID _drv_top_video_path_mem_bypas_switch(BOOL b_on);
static VOID _drv_top_bgref_switch(BOOL b_on);
static VOID _drv_top_hdmi_rgb_output_en(BOOL b_enable);
static VOID _drv_top_display_clock_sel_2x(BOOL b_sel_2x);
static VOID _drv_top_pad_ctrl_sel_1Mx16bits_dram(BOOL b_sel_1mx16bits);


// helper definitions
static BOOL _drv_top_chipisvalid(VOID)
{
    UINT8 u8_chipid0;
    UINT8 u8_chipid1;
    UINT8 u8_chipid2;

    u8_chipid0 = HAL_ReadByte(CHIPIDL_F000);
    u8_chipid1 = HAL_ReadByte(CHIPIDM_F001);
    u8_chipid2 = HAL_ReadByte(CHIPIDH_F002);
    
    if (u8_chipid0 == MS2160_CHIP_ID0 &&
        u8_chipid1 == MS2160_CHIP_ID1 &&
        u8_chipid2 == MS2160_CHIP_ID2)
    {
        return TRUE;
    }

    return FALSE;
}

static VOID _drv_top_video_path_mem_bypas_switch(BOOL b_on)
{
    HAL_ToggleBits(MISCSEL, MSRT_BIT0, b_on);
}

static VOID _drv_top_bgref_switch(BOOL b_on)
{
    HAL_ToggleBits(BGREF_EN_CNTRL, MSRT_BITS4_0, b_on);
}

static VOID _drv_top_hdmi_rgb_output_en(BOOL b_enable)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT0, b_enable);
}

static VOID _drv_top_display_clock_sel_2x(BOOL b_sel_2x)
{
    HAL_ToggleBits(CLKCTRL0_F003, MSRT_BIT0, b_sel_2x);
}

static VOID _drv_top_pad_ctrl_sel_1Mx16bits_dram(BOOL b_sel_1mx16bits)
{
    HAL_ToggleBits(PADCTRL5_F016, MSRT_BIT3, b_sel_1mx16bits);
}

//exported
VOID ms2160drv_top_init(VOID)
{
    //dclk source PLL selection: default from PLLV
    HAL_ClrBits(CLKCTRL0_F003, MSRT_BITS2_1);

    //dclk ratio selection: default 1x
    _drv_top_display_clock_sel_2x(FALSE);
    
    //Video path selection: default with DRAM
    _drv_top_video_path_mem_bypas_switch(FALSE);

    //DRAM pin mux selection:default not use 1Mx32bits
    _drv_top_pad_ctrl_sel_1Mx16bits_dram(FALSE);

    //switch on bgref
    _drv_top_bgref_switch(TRUE);
}

VOID ms2160drv_top_video_path_mem_bypas_switch(BOOL b_on)
{
    _drv_top_video_path_mem_bypas_switch(b_on);
}

VOID ms2160drv_top_shutdown_dvout_video(BOOL b_shutdown)
{
    HAL_ToggleBits(PADCTRL6_F017, MSRT_BIT0, !b_shutdown);
    HAL_ToggleBits(DVOPADCTRL0_F01C, MSRT_BITS6_0, !b_shutdown);
}

VOID ms2160drv_top_shutdown_vgaout_timing(BOOL b_shutdown)
{
    HAL_ToggleBits(PADCTRL6_F017, MSRT_BIT0, b_shutdown);
    HAL_ToggleBits(DVOPADCTRL0_F01C, MSRT_BITS1_0, !b_shutdown);
}

VOID ms2160drv_top_shutdown_i2s_output(BOOL b_shutdown)
{
    HAL_ToggleBits(PADCTRL8_F019, MSRT_BITS7_4, !b_shutdown);
    HAL_ToggleBits(PADCTRL5_F016, MSRT_BIT6, !b_shutdown);
}


VOID ms2160drv_top_cav_rgb_value_flip(BOOL b_flip)
{
    HAL_ToggleBits(CAVCTRL, MSRT_BIT2, b_flip);
}

VOID ms2160drv_top_cav_yuv_range_sel(BOOL b_16_to_235)
{
    HAL_ToggleBits(CAVCTRL, MSRT_BIT1, b_16_to_235);
}

VOID ms2160drv_top_cav_ypbpr_output_enable(BOOL b_enable)
{
    HAL_ToggleBits(CAVCTRL, MSRT_BIT0, b_enable);
}

VOID ms2160drv_top_cav_ypbpr_trisync_enable(BOOL b_enable)
{
    HAL_ToggleBits(CAVCTRL, MSRT_BIT4, b_enable);
}

VOID ms2160drv_top_bgref_switch(BOOL b_on)
{
    _drv_top_bgref_switch(b_on);
}

VOID ms2160drv_top_vds_rgb2yuv_range_sel(BOOL b_16_to_235)
{
    HAL_ToggleBits(VDS_RGB2YUV_CFG, MSRT_BIT2, b_16_to_235);
}

VOID ms2160drv_top_vds_rgb2yuv_hdsel(BOOL b_sel_yuv709)
{
    HAL_ToggleBits(VDS_RGB2YUV_CFG, MSRT_BIT1, b_sel_yuv709);
}

VOID ms2160drv_top_vds_rgb2yuv_switch(BOOL b_on)
{
    HAL_ToggleBits(VDS_RGB2YUV_CFG, MSRT_BIT0, b_on);
}

VOID ms2160drv_top_cav_bypass(BOOL b_bypass)
{
    HAL_ToggleBits(CAVCTRL, MSRT_BIT3, b_bypass);
}

VOID ms2160drv_top_hdmi_source_yuv_flip(BOOL b_flip)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT5, b_flip);
}

VOID ms2160drv_top_hdmi_yuv_range_sel(BOOL b_16_to_235)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT4, b_16_to_235);
}

VOID ms2160drv_top_hdmi_clk_double_en(BOOL b_enable)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT3, b_enable);
}

VOID ms2160drv_top_hdmi_yuv709_output_en(BOOL b_enable)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT2, b_enable);
}

VOID ms2160drv_top_hdmi_yuv422_output_en(BOOL b_enable)
{
    HAL_ToggleBits(HDMI_SRC_CNTRL, MSRT_BIT1, b_enable);
}

VOID ms2160drv_top_hdmi_rgb_output_en(BOOL b_enable)
{
    _drv_top_hdmi_rgb_output_en(b_enable);
}

VOID ms2160drv_top_display_clock_sel_2x(BOOL b_sel_2x)
{
    _drv_top_display_clock_sel_2x(b_sel_2x);
}

VOID ms2160drv_top_pad_ctrl_sel_1Mx16bits_dram(BOOL b_sel_1mx16bits)
{
    _drv_top_pad_ctrl_sel_1Mx16bits_dram(b_sel_1mx16bits);
}

VOID ms2160drv_top_pad_ctrl_memio_gpio_mode_switch(BOOL b_on)
{
    //gpio mode
    HAL_ToggleBits(PADCTRL5_F016, MSRT_BIT0, b_on);

    //pull up
    HAL_ToggleBits(MPADCTRL0_F00C, MSRT_BIT6 | MSRT_BIT2, b_on);
    HAL_ToggleBits(MPADCTRL1_F00D, MSRT_BIT6 | MSRT_BIT2, b_on);
    HAL_ToggleBits(MPADCTRL2_F00E, MSRT_BIT6 | MSRT_BIT2, b_on);
    HAL_ToggleBits(MPADCTRL3_F00F, MSRT_BIT6 | MSRT_BIT2, b_on);
    
    HAL_SetBits(MPADCTRL4_F010, MSRT_BIT5 | MSRT_BIT1);
    HAL_ToggleBits(MPADCTRL4_F010, MSRT_BIT0, !b_on);  
    HAL_ToggleBits(MPADCTRL4_F010, MSRT_BIT4, b_on);
}

VOID ms2160drv_top_pad_ctrl_dvoio_gpio_mode_switch(BOOL b_on)
{
    //pull up
    HAL_ToggleBits(DVOPADCTRL1_F01D, MSRT_BIT7, b_on);
}

VOID ms2160drv_top_config_mem_clk_div(UINT8 u8_sdram_clock)
{
    HAL_ModBits(CLKCTRL0_F003, MSRT_BITS7_4, (UINT8)(480/u8_sdram_clock) << 4);
}

