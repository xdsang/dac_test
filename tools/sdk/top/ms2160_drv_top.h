/**
******************************************************************************
* @file    ms2160_drv_top.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   top module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_TOP_H__
#define __MACROSILICON_MS2160_DRV_TOP_H__

// driver interface for macro enum structure declarations

#ifdef __cplusplus
extern "C" {
#endif

VOID ms2160drv_top_init(VOID);

VOID ms2160drv_top_video_path_mem_bypas_switch(BOOL b_on);

VOID ms2160drv_top_shutdown_dvout_video(BOOL b_shutdown);

VOID ms2160drv_top_shutdown_vgaout_timing(BOOL b_shutdown);

VOID ms2160drv_top_shutdown_i2s_output(BOOL b_shutdown);

VOID ms2160drv_top_cav_rgb_value_flip(BOOL b_flip);

VOID ms2160drv_top_cav_yuv_range_sel(BOOL b_16_to_235);

VOID ms2160drv_top_cav_ypbpr_output_enable(BOOL b_enable);

VOID ms2160drv_top_cav_ypbpr_trisync_enable(BOOL b_enable);

VOID ms2160drv_top_bgref_switch(BOOL b_on);

VOID ms2160drv_top_vds_rgb2yuv_range_sel(BOOL b_16_to_235);

VOID ms2160drv_top_vds_rgb2yuv_hdsel(BOOL b_sel_yuv709);

VOID ms2160drv_top_vds_rgb2yuv_switch(BOOL b_on);

VOID ms2160drv_top_cav_bypass(BOOL b_bypass);

VOID ms2160drv_top_hdmi_source_yuv_flip(BOOL b_flip);

VOID ms2160drv_top_hdmi_yuv_range_sel(BOOL b_16_to_235);

VOID ms2160drv_top_hdmi_clk_double_en(BOOL b_enable);

VOID ms2160drv_top_hdmi_yuv709_output_en(BOOL b_enable);

VOID ms2160drv_top_hdmi_yuv422_output_en(BOOL b_enable);

VOID ms2160drv_top_hdmi_rgb_output_en(BOOL b_enable);

VOID ms2160drv_top_display_clock_sel_2x(BOOL b_sel_2x);

VOID ms2160drv_top_pad_ctrl_sel_1Mx16bits_dram(BOOL b_sel_1mx16bits);

VOID ms2160drv_top_pad_ctrl_memio_gpio_mode_switch(BOOL b_on);

VOID ms2160drv_top_pad_ctrl_dvoio_gpio_mode_switch(BOOL b_on);

VOID ms2160drv_top_config_mem_clk_div(UINT8 u8_sdram_clock);


#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_TOP_H__
