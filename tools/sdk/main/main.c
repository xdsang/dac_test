#include "mculib.h"
#include "ms2160.h"
#include "user_setting.h"
#include "usbuser.h"
#include "main.h"

//helper declaration
static void _hdmi_outport_service(void);
static void _analog_outport_service(void);
static void _video_delay(void);
static void _global_var_init(void);
static void _get_video_output_port(void);
static void _usb_suspend(void);

void main(void)
{ 
    UINT8 u8_status = USERCODE_STATUSF_NOT_EXIST;  
    
    //system init.
    sys_init();

    //get system information
    sys_get_necessary_information();

    //spi flash and eeprom detection
    //spi flash and eeprom 共用PIN ，需要软件配置切换
    u8_status = user_init();
    if(u8_status & USERCODE_STATUSF_PATCH0_VALID) 
    {
        #pragma asm 
        patch_before_chip_init();
        #pragma endasm
    }
    
    //optional function config.
    if(u8_status & USERCODE_STATUSF_EXIST_VALID)
    {
        sys_set_user_options(user_get_option_code());
    }

    //ms2160 video path power off
    ms2160_power_off();
    
    //ms2160 audio path init.
    ms2160_audio_init();

    if(u8_status & USERCODE_STATUSF_PATCH1_VALID) 
    {
        #pragma asm 
        patch_after_chip_init();
        #pragma endasm
    }

    //usb interface init.
    usbinit();
    
    //enable USB & Timr0 interrupt
    mculib_interrupt_enable(TRUE);

    //main loop
    while(1)
    {
        if(g_u8_power_on) //normal run mode
        {
            sys_output_port_service(SYS_TIMEOUT_500MS);            
        }
        else //power down mode
        {
            sys_output_port_service(SYS_TIMEOUT_1SEC);
        }
        
        sys_video_transfer_control();

        if(u8_status & USERCODE_STATUSF_PATCH2_VALID) 
        {
            #pragma asm 
            patch_in_main_loop();
            #pragma endasm
        }
    }
}

//helper definition
static void _hdmi_outport_service(void)
{
    UINT8 u8_hdmi_hpd = FALSE;
    MS2160_EDID_FLAG_T st_edid_flag;

    u8_hdmi_hpd = ms2160_hdmi_hotplug_detect();    
    if (g_u8_output_port_hpd != u8_hdmi_hpd)
    {
        if (u8_hdmi_hpd) //plug in
        {
            if (ms2160_hdmi_parse_edid(g_u8_edid_buff, &st_edid_flag))
            {
                g_u8_hdmi_out_color_space = COLOR_SPACE_RGB565;
                g_u8_sink_max_pixel_clock = st_edid_flag.u8_max_pixel_clk;
            }
        }
        else
        {
            //do some handling
        }
        g_u8_output_port_hpd = u8_hdmi_hpd;
    }
}

static void _analog_outport_service(void)
{
    UINT8 u8_analog_hpd = FALSE;
    MS2160_EDID_FLAG_T st_edid_flag;

    u8_analog_hpd = ms2160_analog_hotplug_detect(g_u8_video_output_port);
    
    if (g_u8_output_port_hpd != u8_analog_hpd)
    {
        if (u8_analog_hpd) //plug in
        {
            if(VIDEO_OUTPUT_PORT_VGA == g_u8_video_output_port)
            {
                if (!ms2160_hdmi_parse_edid(g_u8_edid_buff, &st_edid_flag))
                {
                
                }
            }
        }
        else
        {
            //do some handling
        }

        g_u8_output_port_hpd = u8_analog_hpd;
    }
}

static void _video_delay(void)
{
    unsigned int xdata j;
    for(j=0;j<hidbuff[3];j++);
}

static void _global_var_init(void)
{
    #if (!MS2160_FPGA_VERIFY)
    g_u8_sdram_type = SDRAM_SIZE_8M;
    g_u8_dvo_mode = MS2160_DVO_24BIT_RGB;
    g_u8_video_output_port = VIDEO_OUTPUT_PORT_VGA;
    g_u8_out_mode_vic = 2;
    #endif
    g_u8_output_port_hpd = 0;
    g_u8_hdmi_out_color_space = COLOR_SPACE_RGB565;
    g_u8_sink_max_pixel_clock = 0xFF;
    counter_10ms = 0;
}

static void _get_video_output_port(void)
{
    //每个IO 3种 状态 0 1 2 分别表示 接地 。接电源 & 浮空
    UINT8 u8_gpio0_temp, u8_gpio1_temp;

    //default 2 GPIO0，1都是浮空状态
    u8_gpio0_temp = 2;
    u8_gpio1_temp = 2;
    
    //设置GPIO0 1为输入上拉模式
    reg_gpio0_oen = 0; 
    reg_gpio1_oen = 0;  
    reg_gpio_pu |= 0x03; //GPIO0 1 使能上拉；
    reg_gpio_pd &= ~0x03;//GPIO0 1关闭下拉；
    mculib_delay_us(1);
    if(reg_gpio0_data == 0) // GPIO0 外部接地了
    {
        u8_gpio0_temp = 0 ;
    }
    if(reg_gpio1_data == 0) //GPIO1 外部接地了
    {
        u8_gpio1_temp = 0 ;
    }

    //设置GPIO 0 1为输入下拉模式
    reg_gpio_pu &= ~0x03;//关闭GPIO0 ，1上拉；
    reg_gpio_pd |= 0x03;// GPIO01使能下拉；
    mculib_delay_us(1);
    if(reg_gpio0_data == 1) // 外部接电源了
    {
        u8_gpio0_temp = 1 ;
        reg_gpio_pd &= ~0x01; // 关闭GPIO0下拉 ，减少漏电
    }
    if(reg_gpio1_data == 1) // 外部接电源了
    {
        u8_gpio1_temp = 1 ;
        reg_gpio_pd &= ~0x02; // 关闭GPIO1下拉 ，减少漏电
    }

    g_u8_video_output_port = u8_gpio1_temp * 3 +  u8_gpio0_temp ; // g_u8_video_output_port = 0~8
    
    //gpio0 1 状态一致 ，并且都等于2 ,可能是短路情况
    if((u8_gpio0_temp + u8_gpio1_temp) == 4)
    {
       reg_gpio0_oen = 1; //设置GPIO0 为输出模式  ，GPIO1 此时配置为输入下拉模式
       reg_gpio0_data = 1; // 
       mculib_delay_us(1);
       if(reg_gpio1_data==1)
       {
          g_u8_video_output_port = 9 ; //GPIO0，1 位短路状态
       }
       reg_gpio0_oen = 0 ; // gpio0 恢复输入模式，减少漏电
    }

    //get ouput mode vic for tve and ypbpr out
    switch(g_u8_video_output_port)
    {
    case 0:
    case 3: //480i for tve, 480p for ypbpr
        g_u8_out_mode_vic = 2;
        break;
    default: //576i for tve, 576p for ypbpr
        g_u8_out_mode_vic = 17;
        break;
    }
    
    //video port remap
    switch(g_u8_video_output_port)
    {
    case 0:
    case 1: //cvbs
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_CVBS;
        break;
    case 2: //hdmi
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_HDMI;
        break;
    case 3:
    case 4: //s-video
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_SVIDEO;
        break;
    case 5: //ypbpr
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_YPBPR;
        break;
    case 6: //digital
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_DIGITAL;
        break;        
    case 7: //cvbs and s-video
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_CVBS_SVIDEO;
        break; 
    case 8: //vga
        g_u8_video_output_port = VIDEO_OUTPUT_PORT_VGA;
        break;   
    default: //audio only
        break;            
    }
    
    #if (MS2160_FPGA_VERIFY)
    g_u8_out_mode_vic = ms2160_get_video_output_mode();
    g_u8_video_output_port = ms2160_get_video_output_port();
    #endif
}

static void _usb_suspend(void)
{
    reg_mcu_access_usb_mode = 1;
    Delay_us(1);
    reg_usb_Power |= 0x71;       //下一条指令会关闭Clock
    reg_usb_Power &= 0x7e; 
    reg_mcu_access_usb_mode = 0;
}

//system interface
void sys_init(void)
{
    _global_var_init();
    mculib_interrupt_init();
    //disable ms2160 spi master mode(use spi io as gpio) 
    reg_Padctrl5_f016 |= 0x04;
    mculib_gpio_init();
    mculib_timer_init();
}

void sys_get_necessary_information(void)
{
    _get_video_output_port();

    g_u8_dvo_mode = ms2160_get_dvo_mode();
    g_u8_sdram_type = ms2160_get_sdram_type();
}

void sys_set_user_options(UINT8 *p_u8_opcode)
{
    ms2160_set_options(p_u8_opcode);

    if(0 == (p_u8_opcode[6] & MSRT_BIT4))
    {
        g_u8_dvo_mode     = (p_u8_opcode[6] & MSRT_BITS3_1) >> 1;
        g_u8_out_mode_vic = (p_u8_opcode[6] & MSRT_BIT0) ? 17 : 2;
    }

    if(p_u8_opcode[5] != 0xFF)
    {
        g_u8_sdram_type = p_u8_opcode[5];
    }
}

void sys_output_port_service(UINT8 u8_loop_period)
{
    if(counter_10ms == u8_loop_period)
    {
        switch(g_u8_video_output_port)
        {
        case VIDEO_OUTPUT_PORT_DIGITAL:
            break;
        case VIDEO_OUTPUT_PORT_HDMI:
            _hdmi_outport_service();        
            break;
        default:
            _analog_outport_service();
            break;
        }

        counter_10ms = 0;
    }
}

void sys_video_transfer_control(void)
{
    //video transfer start or stop control
    if(g_u8_video_trans_start_cntrl_flag)
    {
        1 == g_u8_video_transfer_start ? ms2160_video_transfer_start() : ms2160_video_transfer_stop();
        g_u8_video_trans_start_cntrl_flag = 0;
    }

    //turn on or turn off output video
    if(g_u8_video_turn_on_cntrl_flag)
    {
        1 == g_u8_video_turn_on ? ms2160_turn_on_output_video() : ms2160_turn_off_output_video();
        g_u8_video_turn_on_cntrl_flag = 0;
    }

    //set video transfer mode
    if(g_u8_video_trans_mode_cntrl_flag)
    {
        ms2160_set_video_transfer_mode(&g_st_video_transfer_mode);
        g_u8_video_trans_mode_cntrl_flag = 0;
    }

    //set video input info.
    if(g_u8_video_in_info_cntrl_flag)
    {
        ms2160_set_video_input_info(&g_st_video_in_info);
        g_u8_video_in_info_cntrl_flag = 0;
    }

    //set video output info.
    if(g_u8_video_out_info_cntrl_flag)
    {
        ms2160_set_video_output_info(&g_st_video_out_info);
        ms2160_set_video_zoom(&g_st_video_in_info, &g_st_video_out_info);
        g_u8_video_out_info_cntrl_flag = 0;
    }

    //frame transfer trigger
    if(g_u8_video_frame_switch_cntrl_flag)
    {
        ms2160_video_transfer_stop();
        ms2160_video_frame_switch(g_u8_video_frame_index);
        _video_delay();
        ms2160_video_transfer_start();
        g_u8_video_frame_switch_cntrl_flag = 0;
    }

    //power on or power off ms2160
    if(g_u8_power_on_cntrl_flag)
    {
        1 == g_u8_power_on ? ms2160_video_init(g_u8_video_output_port) : ms2160_power_off();
        g_u8_power_on_cntrl_flag = 0;
    }

    //usb bus suspend
    if(g_u8_usb_suspend_cntrl_flag)
    {
        g_u8_usb_suspend_cntrl_flag = 0;
        
        ms2160_turn_off_output_video();
        ms2160_power_off();
        
        _usb_suspend();
    }

    //usb bus resume
    if(g_u8_usb_resume_cntrl_flag)
    {
        ms2160_audio_init();
        g_u8_usb_resume_cntrl_flag = 0;
    }
}

