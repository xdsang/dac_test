#ifndef __MAIN_H__
#define __MAIN_H__

//variable definition
UINT8 __DATA  g_u8_sdram_type           _at_  0x30;
UINT8 __DATA  g_u8_video_output_port    _at_  0x31;
UINT8 __DATA  g_u8_dvo_mode             _at_  0x3B;
UINT8 __DATA  g_u8_out_mode_vic         _at_  0x3C;

UINT8 __DATA  g_u8_output_port_hpd      _at_  0x32;
UINT8 __DATA  g_u8_hdmi_out_color_space _at_  0x33;
UINT8 __DATA  g_u8_sink_max_pixel_clock _at_  0x34;

UINT8 __DATA  intr_in          _at_  0x35;    
UINT8 __DATA  intr_usb         _at_  0x36;    
UINT8 __DATA  intr_out         _at_  0x37;
UINT8 __DATA  counter_10ms     _at_  0x38; 
UINT8 __DATA  SPI_FLASH_TYPE   _at_  0x39;
UINT8 __DATA  eeprom_type      _at_  0x3A; 

UINT8 __XDATA g_u8_edid_buff[256]  _at_  0xC000;


//macro enum structure definitions

void sys_init(void);
void sys_get_necessary_information(void);
void sys_set_user_options(UINT8 *op_code);
void sys_output_port_service(UINT8 u8_loop_period);
void sys_video_transfer_control(void);
                                  
#endif