/**
******************************************************************************
* @file    ms2160_drv_tve.c
* @author  
* @version V1.0.0
* @date    May-2018
* @brief   MacroSilicon TVE driver source file
*          v1.0 support : ms2160
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/

#include "common.h"
#include "ms2160_drv_tve.h"

static VOID _drv_tve_clock_switch(BOOL b_on);
static VOID _drv_tve_sw_reset_switch(BOOL b_on);


static VOID _drv_tve_clock_switch(BOOL b_on)
{
    HAL_ToggleBits(MS2160_TVE_CLKEN_REG, MS2160_TVE_CLKEN_MASK, b_on);
}

static VOID _drv_tve_sw_reset_switch(BOOL b_on)
{
    HAL_ToggleBits(MS2160_TVE_SWREST_REG, MS2160_TVE_SWREST_MASK, !b_on);
}

/***************************************************************
*  Function name: ms2160drv_tve_init(VOID)
*  Description:   initilize tve module
*  Input parameters: None
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID ms2160drv_tve_init(VOID)
{
    ms2160drv_tve_power_switch(ON);

    // enable cvbs & s-video
    HAL_SetBits(MS2160_TVE_MISC1_REG, (UINT8) (MSRT_BIT0 | MSRT_BIT1)); 
}

VOID ms2160drv_tve_power_switch(BOOL b_swith)
{
    _drv_tve_sw_reset_switch(!b_swith);
    _drv_tve_clock_switch(b_swith);
}

/***************************************************************
*  Function name: ms2160drv_tve_outmode
*  Description:   set tve out mode
*  Input parameters: [IN] u8_Mode
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID ms2160drv_tve_outmode(UINT8 u8_Mode)
{
    switch (u8_Mode)
    {
    case TVE_NTSC_MJ:
        HAL_ClrBits(MS2160_TVE_MISC1_REG, (UINT8) MSRT_BITS7_5);
        break;
    case TVE_NTSC_443:
        HAL_ModBits(MS2160_TVE_MISC1_REG, (UINT8) MSRT_BITS7_5, (UINT8) MSRT_BITS6_5);
        break;
    case TVE_PAL_M:
        HAL_ModBits(MS2160_TVE_MISC1_REG, (UINT8) MSRT_BITS7_5, (UINT8) MSRT_BIT6);
        break;
    case TVE_PAL_NC:
        HAL_ModBits(MS2160_TVE_MISC1_REG, (UINT8) MSRT_BITS7_5, (UINT8) MSRT_BIT7);
        break;
    case TVE_PAL_NBDHJI:
        HAL_ModBits(MS2160_TVE_MISC1_REG, (UINT8) MSRT_BITS7_5, (UINT8) MSRT_BITS7_6);
        break;    
    }    
}
