/**
******************************************************************************
* @file    ms2160_drv_tve_dac.h
* @author  
* @version V1.0.0
* @date    May-2018
* @brief   MacroSilicon TVE driver header file
*          v1.0 support : ms2160
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_TVE_H__
#define __MACROSILICON_MS2160_DRV_TVE_H__

#define MS2160_TVE_SWREST_REG     0xF007
#define MS2160_TVE_SWREST_MASK    0x20

#define MS2160_TVE_CLKEN_REG      0xF004
#define MS2160_TVE_CLKEN_MASK     0x02

#define MS2160_TVE_REGBASE        0xF480
#define MS2160_TVE_MISC1_REG      (MS2160_TVE_REGBASE + 0x0000)
#define MS2160_TVE_YGAIN_REG      (MS2160_TVE_REGBASE + 0x0012)
#define MS2160_TVE_UGIN1_REG      (MS2160_TVE_REGBASE + 0x0013)
#define MS2160_TVE_UGIN2_REG      (MS2160_TVE_REGBASE + 0x0014)
#define MS2160_TVE_UGIN3_REG      (MS2160_TVE_REGBASE + 0x0015)
#define MS2160_TVE_UGIN4_REG      (MS2160_TVE_REGBASE + 0x0016)
#define MS2160_TVE_VGIN1_REG      (MS2160_TVE_REGBASE + 0x0017)
#define MS2160_TVE_VGIN2_REG      (MS2160_TVE_REGBASE + 0x0018)
#define MS2160_TVE_VGIN3_REG      (MS2160_TVE_REGBASE + 0x0019)
#define MS2160_TVE_VGIN4_REG      (MS2160_TVE_REGBASE + 0x001a)
#define MS2160_TVE_BURST_REG      (MS2160_TVE_REGBASE + 0x001c)
#define MS2160_TVE_BURSP_REG      (MS2160_TVE_REGBASE + 0x001d)
#define MS2160_TVE_BURSL_REG      (MS2160_TVE_REGBASE + 0x001e)
#define MS2160_TVE_CHROG_REG      (MS2160_TVE_REGBASE + 0x0025)
#define MS2160_TVE_CHROO_REG      (MS2160_TVE_REGBASE + 0x0026)

typedef enum _E_TVE_OUTTYPE_
{
    TVE_NTSC_MJ         = 0,   
    TVE_NTSC_443        = 1,
    TVE_PAL_M           = 2,
    TVE_PAL_NC          = 3,
    TVE_PAL_NBDHJI      = 4
}TVEOUT_E;

/***************************************************************
*  Function name: ms2160drv_tve_init(VOID)
*  Description:   initilize tve module
*  Input parameters: None
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID ms2160drv_tve_init(VOID);

/***************************************************************
*  Function name: ms2160drv_tve_power_switch
*  Description:   tve module sw reser & colck disable
*  Input parameters: b_switch    1: switch on
*                                0: switch off
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID ms2160drv_tve_power_switch(BOOL b_switch);

/***************************************************************
*  Function name: ms2160drv_tve_outmode
*  Description:   set tve out mode
*  Input parameters: [IN] u8_Mode
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID ms2160drv_tve_outmode(UINT8 u8_Mode); // enum to TVEOUT_E

#endif //__MACROSILICON_MS2160_DRV_TVE_H__