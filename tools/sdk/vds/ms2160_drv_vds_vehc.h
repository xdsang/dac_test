/**
******************************************************************************
* @file    ms2160_drv_vds_vehc.h
* @author
* @version V1.0.0
* @date    14-sep-2017
* @brief   vds enhance module driver head file
* @history
*
* Copyright (c) 2009 - , MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_VDS_VEHC_H__
#define __MACROSILICON_MS2160_DRV_VDS_VEHC_H__

#ifdef __cplusplus
extern "C" {
#endif

// driver interface for macro enum structure declarations
typedef struct _T_BWLE_
{
    UINT8 blex_gain;
    UINT8 blex_level;
    UINT8 wlex_gain;
    UINT8 wlex_level;
} BWLE_T;

typedef enum
{
    VDS_PURE_BLACK  = 0,
    VDS_PURE_BLUE,
    VDS_PURE_GREEN,
    VDS_PURE_RED,
    VDS_PURE_WHITE,
    VDS_CROSS_HATCH,
    VDS_HOR_RAMP,
    VDS_VER_RAMP,
    VDS_COLOR_BAR,
    VDS_HOR_GRAY_SACLE,
    VDS_VER_GRAY_SACLE,
    VDS_2ND_HOR_GRAY_SACLE,
    VDS_PRIMARY_COLOR,
    VDS_INTERLACE_BLACK,
    VDS_INTERLACE_RED,
    VDS_INTERLACE_GREEN,
    VDS_INTERLACE_BLUE
}VDS_TEST_PATTERN_E;

/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_init
 *  Description:     ms2160 vds video enhance init
 *  Entry:           None
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_init(VOID);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_reset_switch
 *  Description:     vehc reset switch
 *  Entry:           BOOL, on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_reset_switch(BOOL b_on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_clock_switch
 *  Description:     vehc clock switch
 *  Entry:           BOOL, on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_clock_switch(BOOL b_on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_peaking_en
 *  Description:     peaking function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_peaking_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_peaking_gain
 *  Description:     set peaking gain value
 *  Entry:           u8_gain
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_peaking_gain(UINT8 u8_gain);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_peaking_noise_level
 *  Description:     set peaking coring de_noise level
 *  Entry:           u8_gain
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_peaking_noise_level(UINT8 u8_gain);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_dcti_en
 *  Description:     DCTI function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_dcti_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_dcti_gain
 *  Description:     set dcti gain value
 *  Entry:           u8_gain
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_dcti_gain(UINT8 u8_gain);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_blex_en
 *  Description:     black expansion function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_blex_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_wlex_en
 *  Description:     white expansion function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_wlex_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_blex_config
 *  Description:     set black expansion config parmeters include gain and level value
 *  Entry:           pst_blex point to struct BWLE_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_blex_config(BWLE_T *pst_blex);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_wlex_config
 *  Description:     set white expansion config parmeters include gain and level value
 *  Entry:           pst_wlex point to struct BWLE_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_wlex_config(BWLE_T *pst_wlex);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_brightness_switch
 *  Description:     brightness adjust function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_brightness_switch(BOOL on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_contrast_switch
 *  Description:     contrast adjust function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_contrast_switch(BOOL on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_saturation_switch
 *  Description:     saturation adjust function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_saturation_switch(BOOL on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_hue_switch
 *  Description:     hue adjust function switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_hue_switch(BOOL on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_bcsh
 *  Description:     set bcsh ratio
 *  Entry:           bcsh ratio, pst_bcsh point to struct BCSH_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_brightness(INT8 i8_ratio);
VOID ms2160drv_vds_vehc_set_contrast(UINT8 u8_ratio);
VOID ms2160drv_vds_vehc_set_saturation(UINT8 u8_ratio);
VOID ms2160drv_vds_vehc_set_hue(UINT8 u8_ratio);

/***************************************************************
 *  Function name:   ms2160drv_vds_get_bcsh
 *  Description:     get BCSH ratio
 *  Entry:           bcsh ratio, pst_bcsh point to struct BCSH_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
INT8 ms2160drv_vds_vehc_get_brightness(VOID);
UINT8 ms2160drv_vds_vehc_get_contrast(VOID);
UINT8 ms2160drv_vds_vehc_get_saturation(VOID);
UINT8 ms2160drv_vds_vehc_get_hue(VOID);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_tst_pat_switch
 *  Description:     vds test pattern switch
 *  Entry:           on/off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_tst_pat_switch(BOOL on);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_tst_pat_sel
 *  Description:     vds test pattern select, support 17 kinds of pattern
 *  Entry:           u8_pattern, range 0 ~ 16, refer to VDS_TEST_PATTERN_E
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_tst_pat_sel(UINT8 u8_pattern);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_disdb_color_en
 *  Description:     vds display window border color setting enable
 *  Entry:           enable/disable
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_disdb_color_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160drv_vds_vehc_set_disbd_color
 *  Description:     vds display window border color set (YUV component value)
 *  Entry:           u8_col_y, Y value;
                     u8_col_u, U value;
                     u8_col_v, V value;
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_vehc_set_disbd_color(UINT8 u8_col_y, UINT8 u8_col_u, UINT8 u8_col_v);

#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_VDS_VEHC_H__