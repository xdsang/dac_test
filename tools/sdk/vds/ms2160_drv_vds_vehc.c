/**
******************************************************************************
* @file    ms2160_drv_vds_vehc.c
* @author
* @version V1.0.0
* @date    14-sep-2017
* @brief   video enhance module driver source file
* @history
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_vds_vehc.h"

#define REG_VDS_VEHC_CLK_EN               (0xF004) //bit6
#define REG_VDS_VEHC_RSTB                 (0xF007) //bit4

#define REG_VDS_VEHC_BASE                 (0xF400)

#define REG_VDS_VEHC_PK_CTRL              (REG_VDS_VEHC_BASE + 0x00)
#define REG_VDS_VEHC_PK_GAIN_HP           (REG_VDS_VEHC_BASE + 0x04)
#define REG_VDS_VEHC_PK_NKL               (REG_VDS_VEHC_BASE + 0x05)
#define REG_VDS_VEHC_DCTI_CTRL            (REG_VDS_VEHC_BASE + 0x06)
#define REG_VDS_VEHC_DCTI_GAIN            (REG_VDS_VEHC_BASE + 0x07)

#define REG_VDS_VEHC_BWLE_BYPS            (REG_VDS_VEHC_BASE + 0x08)
#define REG_VDS_VEHC_BWLE_USER            (REG_VDS_VEHC_BASE + 0x09)
#define REG_VDS_VEHC_BLEX_LEV             (REG_VDS_VEHC_BASE + 0x0A)
#define REG_VDS_VEHC_BLEX_GAIN            (REG_VDS_VEHC_BASE + 0x0B)
#define REG_VDS_VEHC_WLEX_LEV             (REG_VDS_VEHC_BASE + 0x0C)
#define REG_VDS_VEHC_WLEX_GAIN            (REG_VDS_VEHC_BASE + 0x0D)

#define REG_VDS_VEHC_Y_BYPS               (REG_VDS_VEHC_BASE + 0x0E)
#define REG_VDS_VEHC_C_BYPS               (REG_VDS_VEHC_BASE + 0x0F)
#define REG_VDS_VEHC_BRI_RATIO            (REG_VDS_VEHC_BASE + 0x10)
#define REG_VDS_VEHC_CON_RATIO            (REG_VDS_VEHC_BASE + 0x11)
#define REG_VDS_VEHC_SAT_RATIO            (REG_VDS_VEHC_BASE + 0x12)
#define REG_VDS_VEHC_HUE_RATIO            (REG_VDS_VEHC_BASE + 0x13)

#define REG_VDS_VEHC_TST_PAT_EN           (REG_VDS_VEHC_BASE + 0x39)
#define REG_VDS_VEHC_TST_PAT_SEL          (REG_VDS_VEHC_BASE + 0x3A)
#define REG_VDS_VEHC_TST_PAT_HT_ITLC_L    (REG_VDS_VEHC_BASE + 0x3B)
#define REG_VDS_VEHC_TST_PAT_VT_ITLC_L    (REG_VDS_VEHC_BASE + 0x3D)

#define REG_VDS_VEHC_DISBD_COL_EN         (REG_VDS_VEHC_BASE + 0x43)
#define REG_VDS_VEHC_DISBD_COL_Y          (REG_VDS_VEHC_BASE + 0x44)
#define REG_VDS_VEHC_DISBD_COL_U          (REG_VDS_VEHC_BASE + 0x45)
#define REG_VDS_VEHC_DISBD_COL_V          (REG_VDS_VEHC_BASE + 0x46)

// variables definitions

// helper decalarations

/************************** peaking *******************************/
static VOID _drv_vehc_peaking_adj_switch(BOOL on);
static VOID _drv_vehc_peaking_gain_adj(UINT8 u8_gain);
static VOID _drv_vehc_peaking_coring_level(UINT8 u8_value);
static VOID _drv_vehc_peaking_med_en(BOOL b_enable);


/*************************** DCTI *********************************/
static VOID _drv_vehc_dcti_adj_switch(BOOL on);
static VOID _drv_vehc_dcti_dly_control(UINT8 u8_dly);
static VOID _drv_vehc_dcti_clip_adj(UINT8 u8_clip);
static VOID _drv_vehc_dcti_gain_adj(UINT8 u8_gain);


/****************** Black & White level expansion *****************/
static VOID _drv_vehc_blex_en(BOOL b_enable);
static VOID _drv_vehc_wlex_en(BOOL b_enable);
static VOID _drv_vehc_set_blex_gain(UINT8 u8_gain);
static VOID _drv_vehc_set_blex_level(UINT8 u8_level);
static VOID _drv_vehc_set_wlex_gain(UINT8 u8_gain);
static VOID _drv_vehc_set_wlex_level(UINT8 u8_level );


/*************************** BCSH *********************************/
static VOID _drv_vehc_bri_adj_switch(BOOL on);
static VOID _drv_vehc_set_bri_ratio(INT8 i8_ratio);
static VOID _drv_vehc_con_adj_switch(BOOL on);
static VOID _drv_vehc_set_con_ratio(UINT8 u8_ratio);
static VOID _drv_vehc_sat_adj_switch(BOOL on);
static VOID _drv_vehc_set_sat_ratio(UINT8 u8_ratio);
static VOID _drv_vehc_hue_adj_switch(BOOL on);
static VOID _drv_vehc_set_hue_ratio(UINT8 u8_ratio);


/************************ Test Pattern ****************************/


/************************ DIS_BD Color ***************************/
static VOID _drv_vehc_disbd_color_en(BOOL b_enable);
static VOID _drv_vehc_set_disbd_color_y(UINT8 u8_col_y);
static VOID _drv_vehc_set_disbd_color_u(UINT8 u8_col_u);
static VOID _drv_vehc_set_disbd_color_v(UINT8 u8_col_v);


// helper definitions

/************************** peaking *******************************/

static VOID _drv_vehc_peaking_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_PK_CTRL, MSRT_BIT0, on);
}

static VOID _drv_vehc_peaking_gain_adj(UINT8 u8_gain)
{
    HAL_WriteByte(REG_VDS_VEHC_PK_GAIN_HP, u8_gain);
}

static VOID _drv_vehc_peaking_coring_level(UINT8 u8_level)
{
    HAL_ModBits(REG_VDS_VEHC_PK_NKL, MSRT_BITS7_4, (u8_level<<4));
}

static VOID _drv_vehc_peaking_med_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VDS_VEHC_PK_NKL, MSRT_BIT1, b_enable);
}


/*************************** DCTI *********************************/

static VOID _drv_vehc_dcti_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_DCTI_CTRL, MSRT_BIT0, (on) ? 0 : 1);  // default 1, bypass
}

static VOID _drv_vehc_dcti_dly_control(UINT8 u8_dly)
{
    HAL_ModBits(REG_VDS_VEHC_DCTI_CTRL, MSRT_BITS5_4, (u8_dly<<4));
}

static VOID _drv_vehc_dcti_clip_adj(UINT8 u8_clip)
{
    HAL_ModBits(REG_VDS_VEHC_DCTI_CTRL, MSRT_BITS3_1, (u8_clip<<1));
}

static VOID _drv_vehc_dcti_gain_adj(UINT8 u8_gain)
{
    HAL_ModBits(REG_VDS_VEHC_DCTI_GAIN, MSRT_BITS3_0, u8_gain);
}


/****************** Black & White level expansion *****************/

static VOID _drv_vehc_blex_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VDS_VEHC_BWLE_BYPS, MSRT_BIT0, (b_enable) ? 0 : 1);
}

static VOID _drv_vehc_wlex_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VDS_VEHC_BWLE_BYPS, MSRT_BIT4, (b_enable) ? 0 : 1);
}

static VOID _drv_vehc_set_blex_gain(UINT8 u8_gain)
{
    HAL_WriteByte(REG_VDS_VEHC_BLEX_GAIN, u8_gain);
}

static VOID _drv_vehc_set_blex_level(UINT8 u8_level)
{
    HAL_WriteByte(REG_VDS_VEHC_BLEX_LEV, u8_level);
}

static VOID _drv_vehc_set_wlex_gain(UINT8 u8_gain)
{
    HAL_WriteByte(REG_VDS_VEHC_WLEX_GAIN, u8_gain);
}

static VOID _drv_vehc_set_wlex_level(UINT8 u8_level)
{
    HAL_WriteByte(REG_VDS_VEHC_WLEX_LEV, u8_level);
}


/*************************** BCSH *********************************/

static VOID _drv_vehc_bri_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_Y_BYPS, MSRT_BIT0, (on) ? 0 : 1);
}

static VOID _drv_vehc_set_bri_ratio(INT8 i8_ratio)
{
    HAL_WriteByte(REG_VDS_VEHC_BRI_RATIO, i8_ratio);
}

static VOID _drv_vehc_con_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_Y_BYPS, MSRT_BIT4, (on) ? 0 : 1);
}

static VOID _drv_vehc_set_con_ratio(UINT8 u8_ratio)
{
    HAL_WriteByte(REG_VDS_VEHC_CON_RATIO, u8_ratio);
}

static VOID _drv_vehc_sat_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_C_BYPS, MSRT_BIT0, (on) ? 0 : 1);
}

static VOID _drv_vehc_set_sat_ratio(UINT8 u8_ratio)
{
    HAL_WriteByte(REG_VDS_VEHC_SAT_RATIO, u8_ratio);
}

static VOID _drv_vehc_hue_adj_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_C_BYPS, MSRT_BIT4, (on) ? 0 : 1);
}

static VOID _drv_vehc_set_hue_ratio(UINT8 u8_ratio)
{
    HAL_WriteByte(REG_VDS_VEHC_HUE_RATIO, u8_ratio);
}


/************************ Test Pattern ****************************/


/************************ DIS_BD Color ***************************/
static VOID _drv_vehc_disbd_color_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VDS_VEHC_DISBD_COL_EN, MSRT_BIT0, b_enable);
}

static VOID _drv_vehc_set_disbd_color_y(UINT8 u8_col_y)
{
    HAL_WriteByte(REG_VDS_VEHC_DISBD_COL_Y, u8_col_y);
}

static VOID _drv_vehc_set_disbd_color_u(UINT8 u8_col_u)
{
    HAL_WriteByte(REG_VDS_VEHC_DISBD_COL_U, u8_col_u);
}

static VOID _drv_vehc_set_disbd_color_v(UINT8 u8_col_v)
{
    HAL_WriteByte(REG_VDS_VEHC_DISBD_COL_V, u8_col_v);
}


/******************************************************************
*******************************************************************
****************************exported*******************************
*******************************************************************
******************************************************************/
VOID ms2160drv_vds_vehc_init(VOID)
{
    //reset module vehc
    HAL_ClrBits(REG_VDS_VEHC_RSTB, MSRT_BIT4);
    
    //enable module vehc clock
    HAL_SetBits(REG_VDS_VEHC_CLK_EN, MSRT_BIT6);

    //release module vehc reset
    HAL_SetBits(REG_VDS_VEHC_RSTB, MSRT_BIT4);

    //enable bcsh adj.
    _drv_vehc_bri_adj_switch(TRUE);
    _drv_vehc_con_adj_switch(TRUE);
    _drv_vehc_sat_adj_switch(TRUE);
    _drv_vehc_hue_adj_switch(TRUE);
}

VOID ms2160drv_vds_vehc_reset_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_VEHC_RSTB, MSRT_BIT4, !b_on);
}

VOID ms2160drv_vds_vehc_clock_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_VEHC_CLK_EN, MSRT_BIT6, b_on);
}

/************************** peaking ******************************/
VOID ms2160drv_vds_vehc_peaking_en(BOOL b_enable)
{
    _drv_vehc_peaking_adj_switch(b_enable);
    _drv_vehc_peaking_med_en(FALSE);
}

VOID ms2160drv_vds_vehc_set_peaking_gain(UINT8 u8_gain)
{
    _drv_vehc_peaking_gain_adj(u8_gain);
}

VOID ms2160drv_vds_vehc_set_peaking_noise_level(UINT8 u8_level)
{
    _drv_vehc_peaking_coring_level(u8_level);
}


/*************************** DCTI *********************************/
VOID ms2160drv_vds_vehc_dcti_en(BOOL b_enable)
{
    _drv_vehc_dcti_adj_switch(b_enable);
    _drv_vehc_dcti_dly_control(0);
    _drv_vehc_dcti_clip_adj(0);
}

VOID ms2160drv_vds_vehc_set_dcti_gain(UINT8 u8_gain)
{
    _drv_vehc_dcti_gain_adj(u8_gain);
}



/****************** Black & White level expansion *****************/
VOID ms2160drv_vds_vehc_blex_en(BOOL b_enable)
{
    _drv_vehc_blex_en(b_enable);
}

VOID ms2160drv_vds_vehc_wlex_en(BOOL b_enable)
{
    _drv_vehc_wlex_en(b_enable);
}

VOID ms2160drv_vds_vehc_set_blex_config(BWLE_T *pst_blex)
{
    _drv_vehc_set_blex_gain(pst_blex->blex_gain);
    _drv_vehc_set_blex_level(pst_blex->blex_level);
}

VOID ms2160drv_vds_vehc_set_wlex_config(BWLE_T *pst_wlex)
{
    _drv_vehc_set_wlex_gain(pst_wlex->wlex_gain);
    _drv_vehc_set_wlex_level(pst_wlex->wlex_level);
}


/*************************** BCSH *********************************/
//switch
VOID ms2160drv_vds_vehc_brightness_switch(BOOL on)
{
    _drv_vehc_bri_adj_switch(on);
}

VOID ms2160drv_vds_vehc_contrast_switch(BOOL on)
{
    _drv_vehc_con_adj_switch(on);
}

VOID ms2160drv_vds_vehc_saturation_switch(BOOL on)
{
    _drv_vehc_sat_adj_switch(on);
}

VOID ms2160drv_vds_vehc_hue_switch(BOOL on)
{
    _drv_vehc_hue_adj_switch(on);
}
//set ratio
VOID ms2160drv_vds_vehc_set_brightness(INT8 i8_ratio)
{
    _drv_vehc_set_bri_ratio(i8_ratio);
}

VOID ms2160drv_vds_vehc_set_contrast(UINT8 u8_ratio)
{
    _drv_vehc_set_con_ratio(u8_ratio);
}

VOID ms2160drv_vds_vehc_set_saturation(UINT8 u8_ratio)
{
    _drv_vehc_set_sat_ratio(u8_ratio);
}

VOID ms2160drv_vds_vehc_set_hue(UINT8 u8_ratio)
{
    _drv_vehc_set_hue_ratio(u8_ratio);
}


/************************ Test Pattern ****************************/
VOID ms2160drv_vds_vehc_tst_pat_switch(BOOL on)
{
    HAL_ToggleBits(REG_VDS_VEHC_TST_PAT_EN, MSRT_BIT0, on);
}

VOID ms2160drv_vds_vehc_tst_pat_sel(UINT8 u8_pattern)
{
    u8_pattern = u8_pattern > 16 ? 16 : u8_pattern;
    
    HAL_ModBits(REG_VDS_VEHC_TST_PAT_SEL, MSRT_BITS4_0, u8_pattern);
}


/************************ DIS_win Color ***************************/
VOID ms2160drv_vds_vehc_disdb_color_en(BOOL b_enable)
{
    _drv_vehc_disbd_color_en(b_enable);
}

VOID ms2160drv_vds_vehc_set_disbd_color(UINT8 u8_col_y, UINT8 u8_col_u, UINT8 u8_col_v)
{
    _drv_vehc_set_disbd_color_y(u8_col_y);
    _drv_vehc_set_disbd_color_u(u8_col_u);
    _drv_vehc_set_disbd_color_v(u8_col_v);
}
