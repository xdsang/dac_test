/**
******************************************************************************
* @file    ms2160_drv_vds_timing.h
* @author 
* @version V1.0.0
* @date    24-Aug-2017
* @brief   module VDS_TMG driver declare
* @history
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef  __MACROSILICON_MS2160_DRV_VDS_TIM_H__
#define  __MACROSILICON_MS2160_DRV_VDS_TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _T_VDS_TIMING_
{
    UINT16 htotal;
    UINT16 vtotal;
    UINT16 hsync_width;
    UINT16 vsync_width;
    UINT16 dis_hde_st;
    UINT16 dis_hde_sp;
    UINT16 dis_vde_st;
    UINT16 dis_vde_sp;
} VDS_TIMING_T;

typedef struct _T_VDS_WIN_POSITION_
{
    UINT16 u16_hst;
    UINT16 u16_hsp;
    UINT16 u16_vst;
    UINT16 u16_vsp;
} VDS_WIN_POSITION_T;

/***************************************************************
 *  Function name:   ms2160drv_vds_timing_init
 *  Description:     ms2160 vds timing generation init, timing config use HW auto mode
 *  Entry:           None
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_init (VOID);


/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_clock_switch
 *  Description:     timgen clock control
 *  Entry:           BOOL
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_clock_switch(BOOL b_on);


/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_reset_switch
 *  Description:     timgen reset control
 *  Entry:           BOOL
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_reset_switch(BOOL b_on);


/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_set_video_output_timing
 *  Description:     ms2160 vds set video timing
 *  Entry:           pst_timing, point to VDS_TIMING_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_set_video_output_timing(VDS_TIMING_T *pst_timing);

/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_get_display_timing
 *  Description:     ms2160 vds get display timing
 *  Entry:           pst_timing, point to VDS_TIMING_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_get_display_timing(VDS_TIMING_T *pst_timing);


/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_set_dis_win_position
 *  Description:     Manual mode ms2160 vds set display windows position
 *  Entry:           pst_window, point to VDS_DIS_INT_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_set_dis_win_position(VDS_WIN_POSITION_T *pst_window);


/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_get_dis_win_position
 *  Description:     Manual mode ms2160 vds get display windows position
 *  Entry:           pst_window, point to VDS_DIS_INT_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_get_dis_win_position(VDS_WIN_POSITION_T *pst_window);

/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_set_int_win_position
 *  Description:     Manual mode ms2160 vds set internal windows position
 *  Entry:           pst_window, point to VDS_DIS_INT_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_set_int_win_position(VDS_WIN_POSITION_T *pst_window);

/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_get_int_win_position
 *  Description:     Manual mode ms2160 vds get internal windows position
 *  Entry:           pst_window, point to VDS_DIS_INT_T
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_vds_timgen_get_int_win_position(VDS_WIN_POSITION_T *pst_window);

/***************************************************************
 *  Function name:   ms2160drv_vds_timgen_trigger
 *  Description:     trigger vds timing
 *  Entry:           None
 *  Returned value:  BOOL
 *  Remark:
 ***************************************************************/
BOOL ms2160drv_vds_timgen_trigger(VOID);

#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_VDS_TIM_H__