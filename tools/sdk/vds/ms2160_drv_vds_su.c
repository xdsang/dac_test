/**
******************************************************************************
* @file    ms2160_drv_vds_su.c
* @author
* @version V1.0.0
* @date    21-Aug-2017
* @brief   Scale up module driver source file
* @history
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_vds_su.h"

/*********************Register Definition begin******************/
#define  REG_VDS_SU_BASE            (0xF380)

#define  REG_VDS_SU_PXL_IN_NUM_L    (REG_VDS_SU_BASE + 0x02)
#define  REG_VDS_SU_PXL_OUT_NUM_L   (REG_VDS_SU_BASE + 0x04)
#define  REG_VDS_SU_LN_IN_NUM_L     (REG_VDS_SU_BASE + 0x06)
#define  REG_VDS_SU_LN_OUT_NUM_L    (REG_VDS_SU_BASE + 0x08)

#define  REG_VDS_SU_HSCALE_L        (REG_VDS_SU_BASE + 0x0E)
#define  REG_VDS_SU_VSCALE_L        (REG_VDS_SU_BASE + 0x10)
#define  REG_VDS_SU_HSU_CFG         (REG_VDS_SU_BASE + 0x12)
#define  REG_VDS_SU_VSU_CFG         (REG_VDS_SU_BASE + 0x13)
#define  REG_VDS_SU_VSC_LN_CHG      (REG_VDS_SU_BASE + 0x14)
#define  REG_VDS_SU_HSC_PXL_CHG     (REG_VDS_SU_BASE + 0x15)
#define  REG_VDS_SU_YUVCONVT        (REG_VDS_SU_BASE + 0x16)
#define  REG_VDS_SU_BYTE_SEL        (REG_VDS_SU_BASE + 0x17)
#define  REG_VDS_SU_BD_COL_CFG0     (REG_VDS_SU_BASE + 0x38)
#define  REG_VDS_SU_BD_COL_CFG1     (REG_VDS_SU_BASE + 0x39)
/*********************Register Definition end********************/

// macro enum structure definitions
typedef enum
{
    HSU_AUTO_VSU_AUTO = 0,
    HSU_AUTO_VSU_MANUAL,
    HSU_MANUAL_VSU_AUTO,
    HSU_MANUAL_VSU_MANUAL
}SU_ADJ_MODE_E;

typedef enum
{
    DIRECT_HORIZONTAL = 0,
    DIRECT_VERTICAL,
    DIRECT_ALL,
} SU_DIRECT_E;

#define RATIOMUL (1000UL)
#define RATIO_1X (1000UL)


// variables definitions
//

// helper decalarations
static VOID _drv_su_set_adj_mode(SU_ADJ_MODE_E e_adj_mode);
static VOID _drv_su_set_input_color_space(UINT8 u8_in_mem_color_space);
static VOID _drv_su_input_data_byte_sel(UINT8 u8_in_byte_sel);
static VOID _drv_su_set_input_size(SU_DIRECT_E e_direct, UINT16 u16_size);
static VOID _drv_su_set_output_size(SU_DIRECT_E e_direct, UINT16 u16_size);
static VOID _drv_su_set_ratio(SU_DIRECT_E e_direct, UINT16 u16_ratio);
static VOID _drv_su_uv_interpolation_filter_switch(BOOL b_on);
static VOID _drv_su_uv_flip(BOOL b_enable);
static VOID _drv_su_int_bd_col_y(UINT8 u8_col_y);
static VOID _drv_su_int_bd_col_u(UINT8 u8_col_u);
static VOID _drv_su_int_bd_col_v(UINT8 u8_col_v);
static VOID _drv_su_h_phase_adj_switch(BOOL b_on);
static VOID _drv_su_v_phase_adj_switch(BOOL b_on);

// helper definitions
static VOID _drv_su_set_adj_mode(SU_ADJ_MODE_E e_adj_mode)
{
    BOOL b_hsu_auto = TRUE;
    BOOL b_vsu_auto = TRUE;

    switch(e_adj_mode)
    {
    case HSU_AUTO_VSU_MANUAL:
        b_hsu_auto = TRUE;
        b_vsu_auto = FALSE;
        break;
    case HSU_MANUAL_VSU_AUTO:
        b_hsu_auto = FALSE;
        b_vsu_auto = TRUE;
        break;
    case HSU_MANUAL_VSU_MANUAL:
        b_hsu_auto = FALSE;
        b_vsu_auto = FALSE;
        break;
    default:
        b_hsu_auto = TRUE;
        b_vsu_auto = TRUE;
        break;
    }

    HAL_ToggleBits(REG_VDS_SU_HSU_CFG, MSRT_BIT7, b_hsu_auto);
    HAL_ToggleBits(REG_VDS_SU_VSU_CFG, MSRT_BIT7, b_vsu_auto);
}

static VOID _drv_su_set_input_color_space(UINT8 u8_in_mem_color_space)
{
    UINT8 u8_cs_sel = 0;

    switch(u8_in_mem_color_space)
    {
    case COLOR_SPACE_RGB565:
        u8_cs_sel = 4;
        break;
    case COLOR_SPACE_YUV422:
        u8_cs_sel = 2;
        break;
    case COLOR_SPACE_YUV444:
        u8_cs_sel = 1;
        break;
    default:
        u8_cs_sel = 0;
        break;                
    }

    HAL_ModBits(REG_VDS_SU_YUVCONVT, MSRT_BITS5_3, u8_cs_sel << 3);
}

static VOID _drv_su_input_data_byte_sel(UINT8 u8_in_byte_sel)
{
    HAL_ModBits(REG_VDS_SU_BYTE_SEL, MSRT_BITS2_0, u8_in_byte_sel);
}

static VOID _drv_su_set_input_size(SU_DIRECT_E e_direct, UINT16 u16_size)
{
    switch(e_direct)
    {
    case DIRECT_HORIZONTAL:
        HAL_WriteWord(REG_VDS_SU_PXL_IN_NUM_L, u16_size);
        break;
    case DIRECT_VERTICAL:
        HAL_WriteWord(REG_VDS_SU_LN_IN_NUM_L, u16_size);
        break;
    default:
        HAL_WriteWord(REG_VDS_SU_PXL_IN_NUM_L, u16_size);
        HAL_WriteWord(REG_VDS_SU_LN_IN_NUM_L, u16_size);
        break;
    }
}

static VOID _drv_su_set_output_size(SU_DIRECT_E e_direct, UINT16 u16_size)
{
    switch(e_direct)
    {
    case DIRECT_HORIZONTAL:
        HAL_WriteWord(REG_VDS_SU_PXL_OUT_NUM_L, u16_size);
        break;
    case DIRECT_VERTICAL:
        HAL_WriteWord(REG_VDS_SU_LN_OUT_NUM_L, u16_size);
        break;
    default:
        HAL_WriteWord(REG_VDS_SU_PXL_OUT_NUM_L, u16_size);
        HAL_WriteWord(REG_VDS_SU_LN_OUT_NUM_L, u16_size);
        break;
    }
}

static VOID _drv_su_set_ratio(SU_DIRECT_E e_direct, UINT16 u16_ratio)
{
    UINT16 reg_value;
    reg_value = 4096000 / u16_ratio;

    if (e_direct == DIRECT_HORIZONTAL)
    {
        HAL_WriteWord(REG_VDS_SU_HSCALE_L, reg_value);
    }
    else if (e_direct == DIRECT_VERTICAL)
    {
        HAL_WriteWord(REG_VDS_SU_VSCALE_L, reg_value);
    }
    else
    {
        HAL_WriteWord(REG_VDS_SU_HSCALE_L, reg_value);
        HAL_WriteWord(REG_VDS_SU_VSCALE_L, reg_value);
    }
}

static VOID _drv_su_uv_interpolation_filter_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_SU_YUVCONVT, MSRT_BIT1, !b_on);
}

static VOID _drv_su_uv_flip(BOOL b_enable)
{
    HAL_ToggleBits(REG_VDS_SU_YUVCONVT, MSRT_BIT2, b_enable);
}

static VOID _drv_su_int_bd_col_y(UINT8 u8_col_y)
{
    u8_col_y = u8_col_y > 15 ? 15 : u8_col_y;

    HAL_WriteByte(REG_VDS_SU_BD_COL_CFG0, u8_col_y << 4);
}

static VOID _drv_su_int_bd_col_u(UINT8 u8_col_u)
{
    u8_col_u = u8_col_u > 15 ? 15 : u8_col_u;

    HAL_WriteByte(REG_VDS_SU_BD_COL_CFG1, u8_col_u);
}

static VOID _drv_su_int_bd_col_v(UINT8 u8_col_v)
{
    u8_col_v = u8_col_v > 15 ? 15 : u8_col_v;
    
    HAL_WriteByte(REG_VDS_SU_BD_COL_CFG1, u8_col_v << 4);
}

static VOID _drv_su_h_phase_adj_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_SU_HSU_CFG, MSRT_BIT1, !b_on);
}

static VOID _drv_su_v_phase_adj_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_SU_VSU_CFG, MSRT_BIT1, !b_on);
}


//exported
VOID ms2160drv_vds_su_init(VOID)
{
    _drv_su_set_adj_mode(HSU_AUTO_VSU_AUTO);
    _drv_su_uv_interpolation_filter_switch(FALSE);
    _drv_su_uv_flip(FALSE);
    _drv_su_h_phase_adj_switch(TRUE);
    _drv_su_v_phase_adj_switch(TRUE);
}

VOID ms2160drv_vds_su_set_ratio(VIDEOSIZE_T *p_st_in_size, VIDEOSIZE_T *p_st_out_size)
{
    UINT32 value = 1000;
    
    BOOL b_hsu_auto = (HAL_ReadByte(REG_VDS_SU_HSU_CFG) & MSRT_BIT7) >> 7;
    BOOL b_vsu_auto = (HAL_ReadByte(REG_VDS_SU_VSU_CFG) & MSRT_BIT7) >> 7;
    
    //hsu config.
    if(b_hsu_auto)
    {
        _drv_su_set_input_size(DIRECT_HORIZONTAL, p_st_in_size->u16_h);
        _drv_su_set_output_size(DIRECT_HORIZONTAL, p_st_out_size->u16_h);
    }
    else
    {
        if(p_st_out_size->u16_h > p_st_in_size->u16_h) 
        {
            value = p_st_out_size->u16_h * RATIOMUL;
            value /= p_st_in_size->u16_h;
        }
        else
        {
            value = RATIO_1X;
        } 
        _drv_su_set_ratio(DIRECT_HORIZONTAL, value);        
    }

    //vsu config.
    if(b_vsu_auto)
    {
        _drv_su_set_input_size(DIRECT_VERTICAL, p_st_in_size->u16_v);
        _drv_su_set_output_size(DIRECT_VERTICAL, p_st_out_size->u16_v);
    }
    else
    {
        if(p_st_out_size->u16_v > p_st_in_size->u16_v)
        {
            value = p_st_out_size->u16_v * RATIOMUL;
            value /= p_st_in_size->u16_v;
        }
        else
        {
            value = RATIO_1X;
        }
        _drv_su_set_ratio(DIRECT_VERTICAL, value);
    }
}


VOID ms2160drv_vds_su_set_video_in_info(VIDEO_IN_INFO_T *p_st_in_info)
{
    //input info. config.
    _drv_su_set_input_color_space(p_st_in_info->u8_in_mem_color_space);
    _drv_su_input_data_byte_sel(p_st_in_info->u8_in_data_byte_sel);
}

VOID ms2160drv_vds_su_set_int_border_color(SU_INT_BD_COLOR_T *p_st_int_bd_color)
{
    HAL_ToggleBits(REG_VDS_SU_BD_COL_CFG0, MSRT_BIT0, p_st_int_bd_color->u8_su_bd_col_adj_en);

    if(p_st_int_bd_color->u8_su_bd_col_adj_en)
    {
        _drv_su_int_bd_col_y(p_st_int_bd_color->u8_su_bd_color_yg);
        _drv_su_int_bd_col_u(p_st_int_bd_color->u8_su_bd_color_ub);
        _drv_su_int_bd_col_v(p_st_int_bd_color->u8_su_bd_color_vr);
    }
}
