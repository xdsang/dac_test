/**
******************************************************************************
* @file    ms2160_drv_vds_su.h
* @author
* @version V1.0.0
* @date    22-Aug-2017
* @brief   vds su module driver header file
* @history draft
*
* Copyright (c) 2009 -, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_VDS_SU_H__
#define __MACROSILICON_MS2160_DRV_VDS_SU_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    UINT8 u8_su_bd_col_adj_en;
    UINT8 u8_su_bd_color_yg;
    UINT8 u8_su_bd_color_ub;
    UINT8 u8_su_bd_color_vr;
}SU_INT_BD_COLOR_T;

/***************************************************************
*  Function name:     ms2160drv_vds_su_init(VOID)
*  Description:       vds_su module init
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID   ms2160drv_vds_su_init(VOID);


/***************************************************************
*  Function name:     ms2160drv_vds_su_set_ratio
*  Description:       
*  Input parameters:  VIDEOSIZE_T*, VIDEOSIZE_T*
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160drv_vds_su_set_ratio(VIDEOSIZE_T *p_st_in_size, VIDEOSIZE_T *p_st_out_size);


/***************************************************************
*  Function name:     ms2160drv_vds_su_set_video_in_info
*  Description:       
*  Input parameters:  VIDEO_IN_INFO_T*
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160drv_vds_su_set_video_in_info(VIDEO_IN_INFO_T *p_st_in_info);


/***************************************************************
*  Function name:     ms2160drv_vds_int_border_color(UINT8 u8_col_y, UINT8 u8_col_u, UINT8 u8_col_v);
*  Description:       set internal window border color
*  Input parameters:  SU_INT_BD_COLOR_T*
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID   ms2160drv_vds_su_set_int_border_color(SU_INT_BD_COLOR_T *p_st_int_bd_color);


#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_VDS_SU_H__

