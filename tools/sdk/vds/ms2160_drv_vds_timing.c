/**
******************************************************************************
* @file    ms2160_drv_vds_timing.c
* @author
* @version V1.0.0
* @date    24-Aug-2017
* @brief   VDS Timing generation module driver source file
* @history
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_vds_timing.h"

/**************************Register Definition**********************/
#define REG_VDS_CLK_EN               (0xF004) //bit2
#define REG_VDS_SW_RSTB              (0xF007) //bit3

#define REG_VDS_TIMGEN_BASE          (0xF380)
#define REG_VDS_TIMGEN_TRIG          (REG_VDS_TIMGEN_BASE + 0x00)

#define REG_VDS_TIMGEN_HT            (REG_VDS_TIMGEN_BASE + 0x18)
#define REG_VDS_TIMGEN_VT            (REG_VDS_TIMGEN_BASE + 0x1A)
#define REG_VDS_TIMGEN_HS            (REG_VDS_TIMGEN_BASE + 0x1C)
#define REG_VDS_TIMGEN_VS            (REG_VDS_TIMGEN_BASE + 0x1E)

//DE
#define REG_VDS_TIMGEN_INT_HDE_ST    (REG_VDS_TIMGEN_BASE + 0x20)
#define REG_VDS_TIMGEN_INT_HDE_SP    (REG_VDS_TIMGEN_BASE + 0x22)
#define REG_VDS_TIMGEN_INT_VDE_ST    (REG_VDS_TIMGEN_BASE + 0x24)
#define REG_VDS_TIMGEN_INT_VDE_SP    (REG_VDS_TIMGEN_BASE + 0x26)
#define REG_VDS_TIMGEN_DIS_HDE_ST    (REG_VDS_TIMGEN_BASE + 0x28)
#define REG_VDS_TIMGEN_DIS_HDE_SP    (REG_VDS_TIMGEN_BASE + 0x2A)
#define REG_VDS_TIMGEN_DIS_VDE_ST    (REG_VDS_TIMGEN_BASE + 0x2C)
#define REG_VDS_TIMGEN_DIS_VDE_SP    (REG_VDS_TIMGEN_BASE + 0x2E)
/**********************************end******************************/

// macro enum structure definitions
#define VDS_TRIGGER                 HAL_SetBits(REG_VDS_TIMGEN_TRIG, MSRT_BIT0)
#define VDS_TRIGGER_DONE           (!(HAL_ReadByte(REG_VDS_TIMGEN_TRIG) & MSRT_BIT0))
#define VDS_TRIGGER_TIMER_WAIT_MAX ((UINT8)50)


// variables definitions

// helper decalarations
static VOID _drv_tmg_clock_switch(BOOL b_on);
static VOID _drv_tmg_sw_reset_switch(BOOL b_on);
static VOID _drv_tmg_set_timing_htotal(UINT16 htotal);
static VOID _drv_tmg_set_timing_vtotal(UINT16 vtotal);
static VOID _drv_tmg_set_timing_hs_width(UINT16 hs_width);
static VOID _drv_tmg_set_timing_vs_width(UINT16 vs_width);
static UINT16 _drv_tmg_get_timing_htotal(VOID);
static UINT16 _drv_tmg_get_timing_vtotal(VOID);
static UINT16 _drv_tmg_get_timing_hs_width(VOID);
static UINT16 _drv_tmg_get_timing_vs_width(VOID);
static VOID _drv_tmg_set_dis_hde_st(UINT16 dis_hst);
static VOID _drv_tmg_set_dis_hde_sp(UINT16 dis_hsp);
static VOID _drv_tmg_set_dis_vde_st(UINT16 dis_odd_vst);
static VOID _drv_tmg_set_dis_vde_sp(UINT16 dis_odd_vsp);
static VOID _drv_tmg_set_int_hde_st(UINT16 int_hst);
static VOID _drv_tmg_set_int_hde_sp(UINT16 int_hsp);
static VOID _drv_tmg_set_int_vde_st(UINT16 int_odd_vst);
static VOID _drv_tmg_set_int_vde_sp(UINT16 int_odd_vsp);
static UINT16 _drv_tmg_get_dis_hde_st(VOID);
static UINT16 _drv_tmg_get_dis_hde_sp(VOID);
static UINT16 _drv_tmg_get_dis_vde_st(VOID);
static UINT16 _drv_tmg_get_dis_vde_sp(VOID);
static UINT16 _drv_tmg_get_int_hde_st(VOID);
static UINT16 _drv_tmg_get_int_hde_sp(VOID);
static UINT16 _drv_tmg_get_int_vde_st(VOID);
static UINT16 _drv_tmg_get_int_vde_sp(VOID);

// helper definitions
static VOID _drv_tmg_clock_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_CLK_EN, MSRT_BIT2, b_on);
}

static VOID _drv_tmg_sw_reset_switch(BOOL b_on)
{
    HAL_ToggleBits(REG_VDS_SW_RSTB, MSRT_BIT3, !b_on);
}

// set vds timing
static VOID _drv_tmg_set_timing_htotal(UINT16 htotal)
{
    HAL_WriteWord(REG_VDS_TIMGEN_HT, htotal);
}

static VOID _drv_tmg_set_timing_vtotal(UINT16 vtotal)
{
    HAL_WriteWord(REG_VDS_TIMGEN_VT, vtotal);
}

static VOID _drv_tmg_set_timing_hs_width(UINT16 hs_width)
{
    HAL_WriteWord(REG_VDS_TIMGEN_HS, hs_width);
}

static VOID _drv_tmg_set_timing_vs_width(UINT16 vs_width)
{
    HAL_WriteWord(REG_VDS_TIMGEN_VS, vs_width);
}

// get vds timing
static UINT16 _drv_tmg_get_timing_htotal(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_HT);
}

static UINT16 _drv_tmg_get_timing_vtotal(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_VT);
}

static UINT16 _drv_tmg_get_timing_hs_width(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_HS);
}

static UINT16 _drv_tmg_get_timing_vs_width(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_VS);
}

// Manual mode set
static VOID _drv_tmg_set_dis_hde_st(UINT16 dis_hst)
{
    HAL_WriteWord(REG_VDS_TIMGEN_DIS_HDE_ST, dis_hst);
}

static VOID _drv_tmg_set_dis_hde_sp(UINT16 dis_hsp)
{
    HAL_WriteWord(REG_VDS_TIMGEN_DIS_HDE_SP, dis_hsp);
}

static VOID _drv_tmg_set_dis_vde_st(UINT16 dis_vst)
{
    HAL_WriteWord(REG_VDS_TIMGEN_DIS_VDE_ST, dis_vst);
}

static VOID _drv_tmg_set_dis_vde_sp(UINT16 dis_vsp)
{
    HAL_WriteWord(REG_VDS_TIMGEN_DIS_VDE_SP, dis_vsp);
}

static VOID _drv_tmg_set_int_hde_st(UINT16 int_hst)
{
    HAL_WriteWord(REG_VDS_TIMGEN_INT_HDE_ST, int_hst);
}

static VOID _drv_tmg_set_int_hde_sp(UINT16 int_hsp)
{
    HAL_WriteWord(REG_VDS_TIMGEN_INT_HDE_SP, int_hsp);
}

static VOID _drv_tmg_set_int_vde_st(UINT16 int_vst)
{
    HAL_WriteWord(REG_VDS_TIMGEN_INT_VDE_ST, int_vst);
}

static VOID _drv_tmg_set_int_vde_sp(UINT16 int_vsp)
{
    HAL_WriteWord(REG_VDS_TIMGEN_INT_VDE_SP, int_vsp);
}


// Manual mode get
static UINT16 _drv_tmg_get_dis_hde_st(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_DIS_HDE_ST);
}

static UINT16 _drv_tmg_get_dis_hde_sp(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_DIS_HDE_SP);
}

static UINT16 _drv_tmg_get_dis_vde_st(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_DIS_VDE_ST);
}

static UINT16 _drv_tmg_get_dis_vde_sp(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_DIS_VDE_SP);
}

static UINT16 _drv_tmg_get_int_hde_st(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_INT_HDE_ST);
}

static UINT16 _drv_tmg_get_int_hde_sp(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_INT_HDE_SP);
}

static UINT16 _drv_tmg_get_int_vde_st(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_INT_VDE_ST);
}

static UINT16 _drv_tmg_get_int_vde_sp(VOID)
{
    return HAL_ReadWord(REG_VDS_TIMGEN_INT_VDE_SP);
}


//exported
VOID ms2160drv_vds_timgen_init (VOID)
{
    //reset
    _drv_tmg_sw_reset_switch(TRUE);
    //enable clock
    _drv_tmg_clock_switch(TRUE);
    //release reset
    _drv_tmg_sw_reset_switch(FALSE);
}

VOID ms2160drv_vds_timgen_clock_switch(BOOL b_on)
{
    _drv_tmg_clock_switch(b_on);
}

VOID ms2160drv_vds_timgen_reset_switch(BOOL b_on)
{
    _drv_tmg_sw_reset_switch(b_on);
}

VOID ms2160drv_vds_timgen_set_video_output_timing(VDS_TIMING_T *pst_timing)
{
    _drv_tmg_set_timing_htotal(pst_timing->htotal);
    _drv_tmg_set_timing_vtotal(pst_timing->vtotal);
    _drv_tmg_set_timing_hs_width(pst_timing->hsync_width);
    _drv_tmg_set_timing_vs_width(pst_timing->vsync_width);

    //display window de config.
    _drv_tmg_set_dis_hde_st(pst_timing->dis_hde_st);
    _drv_tmg_set_dis_hde_sp(pst_timing->dis_hde_sp);
    _drv_tmg_set_dis_vde_st(pst_timing->dis_vde_st);
    _drv_tmg_set_dis_vde_sp(pst_timing->dis_vde_sp);

    //internal window de config.
    _drv_tmg_set_int_hde_st(pst_timing->dis_hde_st);
    _drv_tmg_set_int_hde_sp(pst_timing->dis_hde_sp);
    _drv_tmg_set_int_vde_st(pst_timing->dis_vde_st);
    _drv_tmg_set_int_vde_sp(pst_timing->dis_vde_sp);
}

VOID ms2160drv_vds_timgen_get_display_timing(VDS_TIMING_T *pst_timing)
{
    pst_timing->htotal      =  _drv_tmg_get_timing_htotal();
    pst_timing->vtotal      =  _drv_tmg_get_timing_vtotal();
    pst_timing->hsync_width =  _drv_tmg_get_timing_hs_width();
    pst_timing->vsync_width =  _drv_tmg_get_timing_vs_width();
    pst_timing->dis_hde_st  =  _drv_tmg_get_dis_hde_st();
    pst_timing->dis_hde_sp  =  _drv_tmg_get_dis_hde_sp();
    pst_timing->dis_vde_st  =  _drv_tmg_get_dis_vde_st();
    pst_timing->dis_vde_sp  =  _drv_tmg_get_dis_vde_sp();
}

VOID ms2160drv_vds_timgen_set_dis_win_position(VDS_WIN_POSITION_T *pst_dis_window)
{
    _drv_tmg_set_dis_hde_st(pst_dis_window->u16_hst);
    _drv_tmg_set_dis_hde_sp(pst_dis_window->u16_hsp);
    _drv_tmg_set_dis_vde_st(pst_dis_window->u16_vst);
    _drv_tmg_set_dis_vde_sp(pst_dis_window->u16_vsp);
}

VOID ms2160drv_vds_timgen_get_dis_win_position(VDS_WIN_POSITION_T *pst_dis_window)
{
    pst_dis_window->u16_hst  =  _drv_tmg_get_dis_hde_st();
    pst_dis_window->u16_hsp  =  _drv_tmg_get_dis_hde_sp();
    pst_dis_window->u16_vst  =  _drv_tmg_get_dis_vde_st();
    pst_dis_window->u16_vsp  =  _drv_tmg_get_dis_vde_sp();
}

VOID ms2160drv_vds_timgen_set_int_win_position(VDS_WIN_POSITION_T *pst_int_window)
{
    _drv_tmg_set_int_hde_st(pst_int_window->u16_hst);
    _drv_tmg_set_int_hde_sp(pst_int_window->u16_hsp);
    _drv_tmg_set_int_vde_st(pst_int_window->u16_vst);
    _drv_tmg_set_int_vde_sp(pst_int_window->u16_vsp);
}

VOID ms2160drv_vds_timgen_get_int_win_position(VDS_WIN_POSITION_T *pst_int_window)
{
    pst_int_window->u16_hst  =  _drv_tmg_get_int_hde_st();
    pst_int_window->u16_hsp  =  _drv_tmg_get_int_hde_sp();
    pst_int_window->u16_vst  =  _drv_tmg_get_int_vde_st();
    pst_int_window->u16_vsp  =  _drv_tmg_get_int_vde_sp();
}

BOOL ms2160drv_vds_timgen_trigger(VOID)
{
    BOOL b_success = FALSE;
    UINT8 u8_delay = VDS_TRIGGER_TIMER_WAIT_MAX;
    
    VDS_TRIGGER;
    
    do 
    {
        if (VDS_TRIGGER_DONE)
        {
            b_success = TRUE;
            break;
        }
        --u8_delay;
        Delay_ms(1);
    } while(u8_delay);
    
    return b_success;
}
