/**
******************************************************************************
* @file    ms2160_drv_dac.h
* @author  
* @version V1.0.0
* @date    May-2018
* @brief   MacroSilicon vdac driver header file
*          v1.0 support : MS2160
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/

#ifndef __MACROSILICON_MS2160_DRV_VDAC_H__
#define __MACROSILICON_MS2160_DRV_VDAC_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _E_DAC_CHANNEL_MODE_
{
    DAC_CHANNEL_RESET    = 0,
    DAC_CHANNEL_SET      = 1,
    DAC_CHANNEL_NORMAL   = 2
}DAC_CHANNEL_MODE_E;

typedef enum _E_DAC_PORT_VIDEO_OUT_
{
    DAC_AV_OUT         = 0,  // cvbs out
    DAC_SV_OUT,              // s-video out 
    DAC_VGA_OUT,             // vga out
    DAC_YPBPR_OUT,           // ypbpr out
    DAC_SV_AV_OUT            // cvbs & s-video out
}DAC_PORT_VIDEO_OUT_E;

typedef enum _E_DAC_MUX_VIDEO_IN_   // DAC_MUX data select
{
    DAC_MUX_CVBS_IN    = 0,   
    DAC_MUX_SVDO_YIN   = 0,
    DAC_MUX_SVDO_CIN   = 0,
    DAC_MUX_YG_IN      = 1,   
    DAC_MUX_UB_IN      = 1,
    DAC_MUX_VR_IN      = 1
}DAC_MUX_VIDEO_IN_E;

typedef enum _E_DAC_CHANNEL_
{
    DAC_CHANNEL_R       = 0,   
    DAC_CHANNEL_G       = 1,   
    DAC_CHANNEL_B       = 2,   
    DAC_CHANNEL_S       = 3    
}DAC_CHANNEL_E;

typedef enum _E_DAC_VIDEO_OUT_
{
    DAC_CVBS_OUT      = 0,   
    DAC_SVIDEO_Y_OUT  = 1,  
    DAC_SVIDEO_C_OUT  = 2,  
    DAC_YG_OUT        = 3, 
    DAC_UB_OUT        = 4,
    DAC_VR_OUT        = 5,
    DAC_REG_OUT       = 6,
    DAC_NONE_OUT      = 7
}DAC_VIDEO_OUT_E;

typedef struct _T_DAC_CHANNEL_OUT_
{
    DAC_VIDEO_OUT_E    u8_rChannel;
    DAC_VIDEO_OUT_E    u8_gChannel;
    DAC_VIDEO_OUT_E    u8_bChannel;
}DAC_CHANNEL_OUT_T;


typedef enum _E_HPD_SIGNAL_DETE_   // Video signal detection
{
    HPD_CVBS_NTSC    = 0, // cvbs out
    HPD_S_VIDEO,          // s-video out 
    HPD_VGA,              // vga out
    HPD_YPBPR,            // ypbpr out
    HPD_CVBS_S_VIDEO      // cvbs & s-video out
}HPD_SIGNAL_E;

typedef struct _T_HPD_MANUAL_PARA_   // Manual mode VGA set
{
    UINT16 u16_BgnLine;
    UINT16 u16_EndLine;
    UINT16 u16_SamLine;
    UINT16 u16_1stPixel;
    UINT16 u16_2ndPixel;
    UINT16 u16_vga_v_begin;
    UINT16 u16_vga_v_end;
    UINT16 u16_vga_h_begin;
    UINT16 u16_vga_h_end;
    UINT16 u16_vga_val;
}HPD_PARA_T;

typedef enum _E_HPD_CHANNEL_  // Select signal to detect
{
    HPD_CHANNEL_R       = 0,   
    HPD_CHANNEL_G       = 1,
    HPD_CHANNEL_B       = 2
}HPD_CHANNEL_E;


// VDAC Module

/***************************************************************
 *  Function name:   ms2160drv_dac_init
 *  Description:     dac module init
 *  Entry:           None
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dac_init(VOID);

/***************************************************************
 *  Function name:   ms2160drv_dac_power_switch
 *  Description:     dac moduel power down
 *  Entry:           b_switch  1: switch on
 *                             0: switch off
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dac_power_switch(BOOL b_switch);


// VDAC_MUX

/***************************************************************
 *  Function name:   ms2160drv_dac_set_test_data
 *  Description:     Programmable data for VDAC test
 *  Entry:           u16_value: 10bits  0~1023
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dac_set_test_data(UINT16 u16_value);

/***************************************************************
 *  Function name:   ms2160drv_dac_video_out
 *  Description:     set video signal to channel out
 *  Entry:           DAC_CHANNEL_OUT_T: 
 *                               pst_video->u8_rChannel     
 *                               pst_video->u8_gChannel        
 *                               pst_video->u8_bChannel    
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dac_video_out(DAC_CHANNEL_OUT_T *pst_video);

/***************************************************************
 *  Function name:   ms2160drv_dac_set_video_out_channel
 *  Description:     set dac_mux channel in video
 *  Entry:           u8_video_mode: 0: DAC_AV_OUT
 *                                  1: DAC_SV_OUT
 *                                  2: DAC_VGA_OUT
 *                                  3: DAC_YPBPR_OUT
 *                                  4: NoneDAC_SV_AV_OUT
 *  Returned value:  
 *  Remark:
 ***************************************************************/
VOID ms2160drv_dac_set_video_out_channel(UINT8 u8_video_mode); // enum to DAC_PORT_VIDEO_OUT_E



// HPD Module
/***************************************************************
 *  Function name:   ms2160drv_hpd_init
 *  Description:     dac_hpd module init
 *  Entry:           u8_Signal: 0 HPD_CVBS 
 *                              1 HPD_S_VIDEO
 *                              2 HPD_YPBPR
 *                              3 HPD_VGA
 *                              4 HPD_CVBS_SVIDEO
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_hpd_init(UINT8 u8_Signal);

/***************************************************************
 *  Function name:   ms2160drv_hpd_read_signal_status
 *  Description:     select detect signal
 *  Entry:           u8_Signal: 0 HPD_CVBS 
 *                              1 HPD_S_VIDEO
 *                              2 HPD_YPBPR
 *                              3 HPD_VGA
 *                              4 HPD_CVBS_SVIDEO
 *  Returned value:  BOOL: 1 ����
 *                         0 �Ͽ�
 *  Remark:
 ***************************************************************/
BOOL ms2160drv_hpd_read_signal_status(UINT8 u8_Signal); // enum to HPD_SIGNAL_E

/***************************************************************
 *  Function name:   ms2160drv_hpd_manual_para
 *  Description:     Set test information 
 *  Entry:           pst_para
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_hpd_manual_para(HPD_PARA_T *pst_para);

/***************************************************************
 *  Function name:   ms2160drv_hpd_read_status
 *  Description:     Read Channel test results 
 *  Entry:           u8_channel: 0 HPD_CHANNEL_R
 *                               1 HPD_CHANNEL_G
 *                               2 HPD_CHANNEL_B
 *  Returned value:  BOOL: 1 connection
 *                         0 disconnect
 *  Remark:
 ***************************************************************/
BOOL ms2160drv_hpd_read_channel_status(UINT8 u8_channel); // enum to HPD_CHANNEL_E

/***************************************************************
 *  Function name:   ms2160drv_hpd_vga_squ_wave_channel_bypass
 *  Description:     vga square wave on channel bypass
 *  Entry:           u8_channel: bit0 YG channel, bit1 UB channel, bit2 VR channel
 *                   
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_hpd_vga_squ_wave_channel_bypass(UINT8 u8_channel);

/***************************************************************
 *  Function name:   ms2160drv_hpd_set_squ_wave
 *  Description:     Set the amplitude of the added square wave
 *  Entry:           u8_value: 0~255
 *                   u8_voltage: u8_voltage 0x0:300mv  0x1f:600mv
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160drv_hpd_set_squ_wave(UINT8 u8_value, UINT8 u8_voltage);

#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_VDAC_H__
