/**
******************************************************************************
* @file    ms2160_drv_dac.c
* @author  
* @version V1.0.0
* @date    May-2018
* @brief   MacroSilicon HPD driver source file
*          v1.0 support : ms2160
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/

#include "common.h"
#include "ms2160_drv_dac.h"

/****************************     VDAC CLK     ******************************/
#define MS2160_VDAC_CLKEN_REG       (0xf004)
#define MS2160_VDAC_CLKEN_MASK      (0x80)

#define MS2160_VDAC_SW_RSTB         (0xf161)  // BIT5
#define MS2160_VDAC_SW_SETB         (0xf161)  // BIT4


/***************************    TOP Misc Register Define     ************************/
#define MS2160_MISC_REGBASE         0xf030
#define MS2160_CAV_CONTROL_REG      (MS2160_MISC_REGBASE + 0x0000)
#define MS2160_VDAC_MUX_CHANNEL_REG (MS2160_MISC_REGBASE + 0x0001)
#define MS2160_VDAC_MUX_DATA_REG    (MS2160_MISC_REGBASE + 0x0002)
#define MS2160_VDAC_MUX_TEST_REG    (MS2160_MISC_REGBASE + 0x0003)

// VDAC MUX RGB CHANNEL SELECT Bit Define 
#define REG_DAC_RGB_CHANNEL_SEL     MS2160_VDAC_MUX_CHANNEL_REG
#define BITS_DAC_G_CH_MASK          (0x03)
#define BITS_DAC_B_CH_MASK          (0x0c)
#define BITS_DAC_R_CH_MASK          (0x30)
#define BITS_DAC_S_CH_MASK          (0xc0)


/**************************   VDAC Module Register Define   ******************************/
#define MS2160_VDAC_REGBASE         0xf160
#define MS2160_VDAC_PWR_REG         (MS2160_VDAC_REGBASE + 0x0000)
#define MS2160_VDAC_HPD_REG         (MS2160_VDAC_REGBASE + 0x0001)
#define MS2160_VDAC_HPD_VTH_REG     (MS2160_VDAC_REGBASE + 0x0002)

// VDAC PWR Register Bit Define
#define BIT_DAC_CLK_INV             (0x10)
#define BIT_DAC_R_PWR               (0x08)
#define BIT_DAC_G_PWR               (0x04)
#define BIT_DAC_B_PWR               (0x02)
#define BIT_DAC_PWR                 (0X01)

// VDAC HPD Register Bit 
#define BIT_DAC_HPD_RESET           (0x20)
#define BIT_DAC_HPD_SET             (0x10)
#define BIT_DAC_HPD_R_CHL           (0x08)
#define BIT_DAC_HPD_G_CHL           (0x04)
#define BIT_DAC_HPD_B_CHL           (0x02)
#define BIT_DAC_HPD_EN              (0x01)


/************************     HPD Module Register Define     *****************************/
#define MS2160_HPD_REGBASE          0xF600
#define MS2160_HPD_MISC_REG         (MS2160_HPD_REGBASE + 0x0000)
#define MS2160_HPD_BEG_LINE_REG     (MS2160_HPD_REGBASE + 0x0001)
#define MS2160_HPD_END_LINE_REG     (MS2160_HPD_REGBASE + 0x0003)
#define MS2160_HPD_SAMP_LINE_REG    (MS2160_HPD_REGBASE + 0x0005)
#define MS2160_HPD_FST_SAMP_REG     (MS2160_HPD_REGBASE + 0x0007)
#define MS2160_HPD_SEC_SAMP_REG     (MS2160_HPD_REGBASE + 0x0009)
#define MS2160_HPD_SQU_V_BEG_REG    (MS2160_HPD_REGBASE + 0x000b)
#define MS2160_HPD_SQU_V_END_REG    (MS2160_HPD_REGBASE + 0x000d)
#define MS2160_HPD_SQU_H_BEG_REG    (MS2160_HPD_REGBASE + 0x000f)
#define MS2160_HPD_SQU_H_END_REG    (MS2160_HPD_REGBASE + 0x0011)
#define MS2160_HPD_DIG_STATUS_REG   (MS2160_HPD_REGBASE + 0x0013)
#define MS2160_HPD_ANA_STATUS_REG   (MS2160_HPD_REGBASE + 0x0014)
#define MS2160_HPD_VGA_SQU_VAL_REG  (MS2160_HPD_REGBASE + 0x0015)
#define MS2160_HPD_VGA_SQU_BYPS_REG (MS2160_HPD_REGBASE + 0x0016)



/********************************************************************************
******************************** DAC Module *********************************
*********************************************************************************/
static VOID _drv_dac_clock_switch(BOOL b_on);
static VOID _drv_dac_channel_reset_switch(BOOL b_on);
static VOID _drv_dac_channel_set_switch(BOOL b_on);
static VOID _drv_dac_b_power(BOOL on);
static VOID _drv_dac_g_power(BOOL on);
static VOID _drv_dac_r_power(BOOL on);
static VOID _drv_dac_power(BOOL on);
static VOID _drv_dac_clk_invert(BOOL on);
static VOID _drv_dac_channel_mode(UINT8 u8_channel_mode);
static VOID _drv_dac_channel_power(DAC_CHANNEL_E e_channel, BOOL b_on);
static VOID _drv_dac_test_sel(UINT8 u8_data);
static VOID _drv_dac_vr_sel(UINT8 u8_data);
static VOID _drv_dac_ub_sel(UINT8 u8_data);
static VOID _drv_dac_yg_sel(UINT8 u8_data);
static VOID _drv_dac_video_signal(DAC_CHANNEL_E e_channel, DAC_VIDEO_OUT_E e_sinal);



static VOID _drv_dac_clock_switch(BOOL b_on)
{
    HAL_ToggleBits(MS2160_VDAC_CLKEN_REG, MS2160_VDAC_CLKEN_MASK, b_on);
}

static VOID _drv_dac_channel_reset_switch(BOOL b_on) // 1: reset(output channel set to 0V )    0: normal
{
    HAL_ToggleBits(MS2160_VDAC_SW_RSTB, MSRT_BIT5, !b_on);
}

static VOID _drv_dac_channel_set_switch(BOOL b_on) // 1: set(output channel set to full V)   0: normal
{
    HAL_ToggleBits(MS2160_VDAC_SW_SETB, MSRT_BIT4, b_on);
}

static VOID _drv_dac_b_power(BOOL on)
{
    HAL_ModBits(MS2160_VDAC_PWR_REG, BIT_DAC_B_PWR,\
                (on) ? BIT_DAC_B_PWR : 0 );
}

static VOID _drv_dac_g_power(BOOL on)
{
    HAL_ModBits(MS2160_VDAC_PWR_REG, BIT_DAC_G_PWR,\
                (on) ? BIT_DAC_G_PWR : 0 );
}

static VOID _drv_dac_r_power(BOOL on)
{
    HAL_ModBits(MS2160_VDAC_PWR_REG, BIT_DAC_R_PWR,\
                (on) ? BIT_DAC_R_PWR : 0 );
}

static VOID _drv_dac_power(BOOL on)
{
    HAL_ModBits(MS2160_VDAC_PWR_REG, BIT_DAC_PWR,\
                (on) ? BIT_DAC_PWR : 0);
}

static VOID _drv_dac_clk_invert(BOOL on)
{
    HAL_ModBits(MS2160_VDAC_PWR_REG, BIT_DAC_CLK_INV,\
                (on) ? BIT_DAC_CLK_INV : 0);
}

static VOID _drv_dac_channel_mode(UINT8 u8_channel_mode)
{
    // enum to DAC_CHANNEL_MODE_E
    switch (u8_channel_mode)
    {
    case DAC_CHANNEL_RESET:
        _drv_dac_channel_reset_switch(TRUE);
        _drv_dac_channel_set_switch(FALSE);
        break;

    case DAC_CHANNEL_SET:
        _drv_dac_channel_reset_switch(FALSE);
        _drv_dac_channel_set_switch(TRUE);
        break;

    case DAC_CHANNEL_NORMAL:
        _drv_dac_channel_reset_switch(FALSE);
        _drv_dac_channel_set_switch(FALSE);
        break;

    default:
        break;
    }
}

static VOID _drv_dac_channel_power(DAC_CHANNEL_E e_channel, BOOL b_on)
{
    switch ((UINT8)e_channel)
    {
    case DAC_CHANNEL_R:
        _drv_dac_r_power(b_on);
        break;
    case DAC_CHANNEL_G:
        _drv_dac_g_power(b_on);
        break;
    case DAC_CHANNEL_B:
        _drv_dac_b_power(b_on);
        break;
    default:
        break;
    }
}

static VOID _drv_hpd_manual_config(VOID)
{
    HPD_PARA_T pst_para = {0x0,0x23,0x7,0x8,0x18,0x1,0x19,0x0,0x1f,0x6d};

    ms2160drv_hpd_manual_para(&pst_para);
}



// 
VOID ms2160drv_dac_init(VOID)
{
    // VDAC clock enable
    ms2160drv_dac_power_switch(ON); 

    _drv_dac_channel_mode(DAC_CHANNEL_NORMAL);
}


VOID ms2160drv_dac_power_switch(BOOL b_switch)
{
    _drv_dac_clock_switch(b_switch);
    _drv_dac_channel_power(DAC_CHANNEL_R, b_switch);
    _drv_dac_channel_power(DAC_CHANNEL_G, b_switch);
    _drv_dac_channel_power(DAC_CHANNEL_B, b_switch);
    _drv_dac_power(b_switch);
}



/**************************** VDAC_MUX MODEL FUNCTION ************************************/
static VOID _drv_dac_test_sel(UINT8 u8_data)
{
    HAL_ModBits(MS2160_VDAC_MUX_DATA_REG, MSRT_BITS7_6, u8_data << 6);
}

static VOID _drv_dac_vr_sel(UINT8 u8_data)
{
    HAL_ModBits(MS2160_VDAC_MUX_DATA_REG, MSRT_BITS5_4, u8_data << 4);
}

static VOID _drv_dac_ub_sel(UINT8 u8_data)
{
    HAL_ModBits(MS2160_VDAC_MUX_DATA_REG, MSRT_BITS3_2, u8_data << 2);
}

static VOID _drv_dac_yg_sel(UINT8 u8_data)
{
    HAL_ModBits(MS2160_VDAC_MUX_DATA_REG, MSRT_BITS1_0, u8_data);
}

static VOID _drv_dac_video_signal(DAC_CHANNEL_E e_channel, DAC_VIDEO_OUT_E e_sinal)
{
    UINT8 u8_channel_mask = 0x03; // select channel mask
    UINT8 u8_channel_data = 0x00; // select channel data

    // select out video signal
    switch (e_sinal)
    {
    case DAC_CVBS_OUT: // S data
        u8_channel_data = 0x03;
        _drv_dac_test_sel(DAC_MUX_CVBS_IN);
        break;
    case DAC_SVIDEO_Y_OUT: // YG data
        _drv_dac_yg_sel(DAC_MUX_SVDO_YIN);
        break;
    case DAC_SVIDEO_C_OUT: // UB data
        u8_channel_data = 0x01;
        _drv_dac_ub_sel(DAC_MUX_SVDO_CIN);
        break;
    case DAC_YG_OUT: // YG data
        _drv_dac_yg_sel(DAC_MUX_YG_IN);
        break;
    case DAC_UB_OUT: // UB data
        u8_channel_data = 0x01;
        _drv_dac_ub_sel(DAC_MUX_UB_IN);
        break;
    case DAC_VR_OUT: // VR data
        u8_channel_data = 0x02;
        _drv_dac_vr_sel(DAC_MUX_VR_IN);
        break;
    case DAC_REG_OUT: // S data
        u8_channel_data = 0x03;
        _drv_dac_test_sel(3); // data from register
        break;
    case DAC_NONE_OUT: 
        _drv_dac_channel_power(e_channel, OFF);
        break;
    }

    // select channel for used
    switch (e_channel)
    {
    case DAC_CHANNEL_R:
        u8_channel_mask <<= 4;
        u8_channel_data <<= 4;
        break;
    case DAC_CHANNEL_G:
        break;
    case DAC_CHANNEL_B:
        u8_channel_mask <<= 2;
        u8_channel_data <<= 2;
        break;
    }

    HAL_ModBits(MS2160_VDAC_MUX_CHANNEL_REG, u8_channel_mask, u8_channel_data);
}

VOID ms2160drv_dac_set_test_data(UINT16 u16_value)
{
    HAL_WriteWord(MS2160_VDAC_MUX_TEST_REG, u16_value);
}


VOID ms2160drv_dac_video_out(DAC_CHANNEL_OUT_T *pst_video)
{
    if (pst_video == NULL)
        return;

    _drv_dac_video_signal(DAC_CHANNEL_G, pst_video->u8_gChannel);
    
    #if MS2160_FPGA_VERIFY
        _drv_dac_video_signal(DAC_CHANNEL_R, pst_video->u8_rChannel);
        _drv_dac_video_signal(DAC_CHANNEL_B, pst_video->u8_bChannel);
    #else
        _drv_dac_video_signal(DAC_CHANNEL_R, pst_video->u8_bChannel);        
        _drv_dac_video_signal(DAC_CHANNEL_B, pst_video->u8_rChannel);
    #endif
}



VOID ms2160drv_dac_set_video_out_channel(UINT8 u8_video_mode)
{  
    DAC_CHANNEL_OUT_T pst_video;

    pst_video.u8_rChannel = DAC_NONE_OUT;
    pst_video.u8_gChannel = DAC_NONE_OUT;
    pst_video.u8_bChannel = DAC_NONE_OUT;

    // enum to DAC_PORT_VIDEO_OUT_E
    switch (u8_video_mode)
    {
    case DAC_AV_OUT:  //CVBS  Out
        pst_video.u8_gChannel = DAC_CVBS_OUT;
        
        HAL_SetBits(MS2160_VDAC_PWR_REG, MSRT_BIT5);
        HAL_WriteByte(MS2160_VDAC_HPD_VTH_REG, 0); //设置HPD检测电平
        break;

    case DAC_SV_AV_OUT: // S-Video + CVBS Out
        pst_video.u8_rChannel = DAC_CVBS_OUT;

    case DAC_SV_OUT:   //S-Video Out
        pst_video.u8_gChannel = DAC_SVIDEO_Y_OUT;
        pst_video.u8_bChannel = DAC_SVIDEO_C_OUT;
        
        HAL_SetBits(MS2160_VDAC_PWR_REG, MSRT_BIT5);
        HAL_WriteByte(MS2160_VDAC_HPD_VTH_REG, 0); //设置HPD检测电平
        break;

    case DAC_VGA_OUT: // VGA Out

    case DAC_YPBPR_OUT: // YPBPR Out
    
        pst_video.u8_rChannel = DAC_VR_OUT;
        pst_video.u8_gChannel = DAC_YG_OUT;
        pst_video.u8_bChannel = DAC_UB_OUT;
        break;

    default:
        break;
    }

    ms2160drv_dac_video_out(&pst_video);
}

/********************************************************************************
******************************** DAC_HPD Module *********************************
*********************************************************************************/
static VOID _drv_hpd_manual_mode_en(BOOL b_enable);
static VOID _drv_hpd_detect_begin_line(UINT16 u16_beg_line);
static VOID _drv_hpd_detect_end_line(UINT16 u16_end_line);
static VOID _drv_hpd_samp_line(UINT16 u16_samp_line);
static VOID _drv_hpd_detect_first_pixel(UINT16 u16_1stPixel);
static VOID _drv_hpd_detect_second_pixel(UINT16 u16_2ndPixel);
static VOID _drv_hpd_vga_wave_on_v_begin(UINT16 u16_v_begin);
static VOID _drv_hpd_vga_wave_on_v_end(UINT16 u16_v_end);
static VOID _drv_hpd_vga_wave_on_h_begin(UINT16 u16_h_begin);
static VOID _drv_hpd_vga_wave_on_h_end(UINT16 u16_h_end);
static VOID _drv_hpd_vga_squ_wave_value(UINT8 u8_value);
static UINT8 _drv_hpd_read_dig_status(UINT8 u8_mask);
static UINT8 _drv_hpd_read_ana_status(UINT8 u8_mask);
static VOID _drv_hpd_vga_squ_wave_voltage_sel(UINT8 u8_voltage);
static VOID _drv_hpd_set_detect_signal(UINT8 u8_Signal);



// 
static VOID _drv_hpd_manual_mode_en(BOOL b_enable)
{
    HAL_ToggleBits(MS2160_HPD_MISC_REG, MSRT_BIT3, b_enable);
}


static VOID _drv_hpd_detect_begin_line(UINT16 u16_beg_line)
{
    HAL_WriteWord(MS2160_HPD_BEG_LINE_REG, u16_beg_line);
}

static VOID _drv_hpd_detect_end_line(UINT16 u16_end_line)
{
    HAL_WriteWord(MS2160_HPD_END_LINE_REG, u16_end_line);
}

static VOID _drv_hpd_samp_line(UINT16 u16_samp_line)
{
    HAL_WriteWord(MS2160_HPD_SAMP_LINE_REG, u16_samp_line);
}

static VOID _drv_hpd_detect_first_pixel(UINT16 u16_1stPixel)
{
    HAL_WriteWord(MS2160_HPD_FST_SAMP_REG, u16_1stPixel);
}

static VOID _drv_hpd_detect_second_pixel(UINT16 u16_2ndPixel)
{
    HAL_WriteWord(MS2160_HPD_SEC_SAMP_REG, u16_2ndPixel);
}

static VOID _drv_hpd_vga_wave_on_v_begin(UINT16 u16_v_begin)
{
    HAL_WriteWord(MS2160_HPD_SQU_V_BEG_REG, u16_v_begin);
}

static VOID _drv_hpd_vga_wave_on_v_end(UINT16 u16_v_end)
{
    HAL_WriteWord(MS2160_HPD_SQU_V_END_REG, u16_v_end);
}

static VOID _drv_hpd_vga_wave_on_h_begin(UINT16 u16_h_begin)
{
    HAL_WriteWord(MS2160_HPD_SQU_H_BEG_REG, u16_h_begin);
}

static VOID _drv_hpd_vga_wave_on_h_end(UINT16 u16_h_end)
{
    HAL_WriteWord(MS2160_HPD_SQU_H_END_REG, u16_h_end);
}

static VOID _drv_hpd_vga_squ_wave_value(UINT8 u8_value)
{
    HAL_WriteByte(MS2160_HPD_VGA_SQU_VAL_REG, u8_value);
}

static UINT8 _drv_hpd_read_dig_status(UINT8 u8_mask)
{
    return HAL_ReadByte(MS2160_HPD_DIG_STATUS_REG) & u8_mask;
}

static UINT8 _drv_hpd_read_ana_status(UINT8 u8_mask)
{
    return HAL_ReadByte(MS2160_HPD_ANA_STATUS_REG) & u8_mask;
}

static VOID _drv_hpd_vga_squ_wave_voltage_sel(UINT8 u8_voltage)
{
    HAL_WriteByte(MS2160_VDAC_HPD_VTH_REG, u8_voltage);
}

static VOID _drv_hpd_set_detect_signal(UINT8 u8_Signal)
{
    switch (u8_Signal)
    {        
    case HPD_YPBPR:
        HAL_ModBits(MS2160_HPD_MISC_REG, MSRT_BITS2_1, (UINT8) (MSRT_BIT2));
        break;
        
    case HPD_VGA:
        HAL_ClrBits(MS2160_HPD_MISC_REG, (UINT8) (MSRT_BITS2_1)); // set VGA to detect
        #if MS2160_FPGA_VERIFY
        _drv_hpd_manual_config(); //set manual mode config
        #endif
        break;

    default:
        break;
    }
}


// 
VOID ms2160drv_hpd_init(UINT8 u8_Signal)
{
    UINT8 mark = BIT_DAC_HPD_R_CHL | BIT_DAC_HPD_G_CHL | BIT_DAC_HPD_B_CHL | BIT_DAC_HPD_EN;
    HAL_SetBits(MS2160_VDAC_HPD_REG, mark); // VDAC hot-plug enable
    HAL_SetBits(MS2160_HPD_MISC_REG, (UINT8) (MSRT_BIT0)); // hotplug function enable
    
    _drv_hpd_set_detect_signal(u8_Signal);
}


BOOL ms2160drv_hpd_read_signal_status(UINT8 u8_Signal)
{
    BOOL b_r_channel_status = FALSE;
    BOOL b_g_channel_status = FALSE;
    BOOL b_status = FALSE;

    if (u8_Signal == HPD_CVBS_S_VIDEO)
    {
        b_r_channel_status = ms2160drv_hpd_read_channel_status(HPD_CHANNEL_R);
    }
 
    b_g_channel_status = ms2160drv_hpd_read_channel_status(HPD_CHANNEL_G);
    b_status = b_r_channel_status || b_g_channel_status;
    return b_status;
}


VOID ms2160drv_hpd_manual_para(HPD_PARA_T *pst_para)
{
    _drv_hpd_manual_mode_en(ENABLE);
    _drv_hpd_detect_begin_line(pst_para->u16_BgnLine);
    _drv_hpd_detect_end_line(pst_para->u16_EndLine);
    _drv_hpd_samp_line(pst_para->u16_SamLine);
    _drv_hpd_detect_first_pixel(pst_para->u16_1stPixel);
    _drv_hpd_detect_second_pixel(pst_para->u16_2ndPixel);
    _drv_hpd_vga_wave_on_v_begin(pst_para->u16_vga_v_begin);
    _drv_hpd_vga_wave_on_v_end(pst_para->u16_vga_v_end);
    _drv_hpd_vga_wave_on_h_begin(pst_para->u16_vga_h_begin);
    _drv_hpd_vga_wave_on_h_end(pst_para->u16_vga_h_end);
    _drv_hpd_vga_squ_wave_value(pst_para->u16_vga_val);
}


BOOL ms2160drv_hpd_read_channel_status(UINT8 u8_channel)
{
    BOOL b_channel_status = FALSE;
    // enum to HPD_CHANNEL_E
    switch (u8_channel)
    {
    case HPD_CHANNEL_R:
        b_channel_status = (BOOL) (_drv_hpd_read_dig_status(0x04) >> 2);
        break;
        
    case HPD_CHANNEL_G:
        b_channel_status = (BOOL) (_drv_hpd_read_dig_status(0x02) >> 1);
        break;
        
    case HPD_CHANNEL_B:
        b_channel_status = (BOOL) (_drv_hpd_read_dig_status(0x01));
        break;
        
    default:
        break;
    }
    return (!b_channel_status); // 1:连接 0:断开
}


VOID ms2160drv_hpd_vga_squ_wave_channel_bypass(UINT8 u8_channel)
{
    if (u8_channel > MSRT_BITS2_0)
        ;
    else
        HAL_SetBits(MS2160_HPD_VGA_SQU_VAL_REG, u8_channel); 
}


VOID ms2160drv_hpd_set_squ_wave(UINT8 u8_value, UINT8 u8_voltage)
{
    // u8_voltage 0x0:300mv  0x1f:600mv
    _drv_hpd_vga_squ_wave_value(u8_value);
    _drv_hpd_vga_squ_wave_voltage_sel(u8_voltage);
}

/***************************************   END   *******************************************/

