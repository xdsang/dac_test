#include "common.h"
#include "usb_standard.h"
#include "fifo.h"
#include "hardware.h"
#include "descript.h"
#include "usb_audio10class.h"
#include "usbuser.h"
#include "ms2160_drv_audio.h"

// To do audio mute 处理
#define reg_audiomute_f18d   (*(volatile unsigned char xdata*)0xf18d)
#define reg_i2s2usb_ctrl_f880   (*(volatile unsigned char __XDATA*)0xf880) 

void set_cur()
{
    reg_EP0_SevOutPktRdy = 1;
    while (!reg_EP0_OutPktRdy) ;
    setupDataOut(Usb_request.wLength, comdata);  // 接收数据，可能为1 Bytes 或者 3 Bytes , 1 bytes 为静音 ，3 Bytes 为Endpoit 
    switch (Usb_request.wValue >> 8)
    {
        case 0x01:      // Mute  只支持静音调节 
            if(((Usb_request.wIndex>>8) == 9)  ||  ((Usb_request.wIndex>>8) == 2))   //speaker ,ms2160 SID = 2 MS2108 SID = 9
            {
                reg_audiomute_f18d =  comdata[0];
            }
            if((Usb_request.wIndex>>8) == 0x0a)
            {
               if((comdata[0]<<6)&0x40)
               {
                  reg_i2s2usb_ctrl_f880 |= 0x40;
               }
               else
               {
                  reg_i2s2usb_ctrl_f880 &= ~0x40;
               }
            }
            break;
        default:
            Error = 1;
            break;
    }   
     NoData = 1;
}

void get_cur()
{
    switch (Usb_request.wValue >> 8)
    {
        case 0x01:
            if(((Usb_request.wIndex>>8) == 9)  ||  ((Usb_request.wIndex>>8) == 2))   //speaker ,ms2160 SID = 2 MS2108 SID = 9
            {
                comdata[0] = reg_audiomute_f18d & 0x01 ;
            }
            if((Usb_request.wIndex>>8) == 0x0a)
            {
               comdata[0] =   (reg_i2s2usb_ctrl_f880>>6)&0x01;
            }
            setupDataIn(1,comdata);
            break;           
        default:
            comdata[0] = 0x01;
            setupDataIn(Usb_request.wLength,comdata);
            break;
    }
}


void get_min()
{
    switch (Usb_request.wValue >> 8)
    {
        case 0x01:
            comdata[0] = 0x00;
            setupDataIn(1,comdata);
            break;
        case 0x02:
            comdata[0] = 0x00;
            comdata[1] = 0x00;
            setupDataIn(2,comdata);
            break;           
        default:
            Error = 1;
            break;
    }
}

void get_max()
{
    switch (Usb_request.wValue >> 8)
    {
        case 0x01:
            comdata[0] = 0x01;
            setupDataIn(1,comdata);
            break;
        case 0x02:
            comdata[0] = 0xFF;
            comdata[1] = 0x1F;
            setupDataIn(2,comdata);
            break;           
        default:
            Error = 1;
            break;
    }
}

void get_res()
{
    switch (Usb_request.wValue >> 8)
    {
        case 0x01:
            comdata[0] = 0x01;
            setupDataIn(1,comdata);
            break;
        case 0x02:
            comdata[0] = 0x01;
            comdata[1] = 0x00;
            setupDataIn(2,comdata);
            break;               
        default:
            Error = 1;
            break;
    }
}



void audioclassctrol_process()
{
    switch(Usb_request.bRequest)
    {
       case SET_CUR:
            set_cur();
            break;
    
       case GET_CUR:
            get_cur();
            break;
    
       case GET_MIN:
            get_min();
            break;
    
       case GET_MAX:
            get_max();
            break;
    
       case GET_RES:
            get_res();
            break;
      
       default :
            Error = 1;
            break;                      
    }   
}