#include "common.h"
#include "descript.h"
xdata BYTE ManufacturerString[0x20];
xdata BYTE ProduceString[0x20];


code BYTE byTestPacket[53] = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0xAA, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 
        0xEE, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xBF, 0xDF,
        0xEF, 0xF7, 0xFB, 0xFD, 0xFC, 0x7E, 0xBF, 0xDF,
        0xFE, 0xF7, 0xFB, 0xFD, 0x7E };

 
code BYTE LANGIDString[4] = 
{
    0x04,0x03,0x09,0x04
};


code BYTE ManufacturerString_rom[] = 
{                                                  
    0x1A,0x03,//length & type
    0x4D,0x00, //M
    0x41,0x00, //A
    0x43,0x00, //C
    0x52,0x00,//R
    0x4F,0x00, //O
    0x53,0x00,// S
    0x49,0x00,//I
    0x4C,0x00, //L
    0x49,0x00,//I
    0x43,0x00,//C
    0x4F,0x00, //O
    0x4E,0x00, //N
};


code BYTE ProduceString_rom[] = 
{
    28,0x03,//length & type
    'u',0x00,      // u
    's',0x00,      // s
    'b',0x00,      // b
    ' ',0x00,      //  
    'e',0x00,      // e
    'x',0x00,      // x
    't',0x00,      // t
    's',0x00,      // s
    'c',0x00,      // c
    'r',0x00,      // r
    'e',0x00,      // e
    'e',0x00,      // e
    'n',0x00,      // n
};

//audio descript
xdata BYTE  ProduceString1[30] = 
{
    30,0x03,//length & type
    0x55,0x00,      // U
    0x53,0x00,      // S
    0x42,0x00,      // B
    0x32,0x00,      // 2
    0x2E,0x00,      // .
    0x30,0x00,      // 0
    0x20,0x00,      //
    'S',0x00,       // S
    'p',0x00,       // p
    'e',0x00,       // e
    'a',0x00,       // a
    'k',0x00,       // k
    'e',0x00,       // e
    'r',0x00        // r
};

//video descript 5
xdata BYTE uvideoString[24] = 
{
    24,0x03,//length & type
    'm',0x00,      // m
    's',0x00,      // s
    'u',0x00,      // u
    's',0x00,      // s
    'b',0x00,      // b
    ' ',0x00,      //  
    'v',0x00,      // v
    'i',0x00,      // i
    'd',0x00,      // d
    'e',0x00,      // e
    'o',0x00,      // o
}  ;


//udisk descript  6
xdata BYTE  ProduceString2[12] = 
{
    12,0x03,//length & type
    'u',0x00,       // U
    'd',0x00,       // d
    'i',0x00,       // i
    's',0x00,       // s
    'k',0x00        // k
};


// 序列号必须是 0~9，或者A~F
xdata BYTE SerialnumString[26] = 
{
    26,0x03,//length & type
    '2',0x00,//2
    '0',0x00,//0
    '1',0x00,//1
    '8',0x00,//8
    'B',0x00,//b
    'A',0x00,//s
    '7',0x00,//7
    '1',0x00,//1
    '6',0x00,//6
    '0',0x00,//0
    'A',0x00,//6
    '0',0x00 //0
};


code BYTE HQualifierData[0x0a] = 
{
    0x0a,
    0x06,
    0x00,0x02,
    0xEF,
    0x02,
    0x01,
    0x40,
    0x01,
    0x00
};

code BYTE FQualifierData[0x0a] = 
{
    0x0a,
    0x06,
    0x10,0x01,
    0xEF,
    0x02,
    0x01,
    0x40,
    0x01,
    0x00
};


xdata BYTE dev_desc[0x12] =   
{   
    0x12,
    0x01,
    0x00,0x02,  //USB2.0    
    0xEF,          //Class
    0x02,          //SubClass
    0x01,          //Protocol
    0x40,          //MaxPacket
    0x4d,0x53,         //VID   534d
    0x21,0x60,         //PID   6021
    0x00,0x01,          //bcdDevice     //  版本号0121
    0x01,          //Manufacturer
    0x02,          //Product
    0x03,          //serial
    0x01           //numconfigration
};

unsigned char code  ReportDescriptor[]=
{
    0x06, 0x00, 0xFF,   // usage page 用户自定义
    0x09, 0x01,                        // usage(0) undefined
    0xA1, 0x01,                        // Collection(Application)
//  0x85, 0x01,                         // REPORT_ID (1)
    0x15, 0x00,                        // Logical Minimum (0)
    0x26, 0xFF, 0x00,                // Logical Maximum (255)
    0x19, 0x01,                        // Usage Minimum(0x01)
    0x29, 0x02,                       // Usage Maximum(0x05)
    0x75, 0x08,                        // REPORT SIZE (8)
    0x95, 0x08,                       // REPORT COUNT(8)
    0xB1, 0x02,                      // FEATURE(DATA,VARIABLE,ABSOLUTE)
    0xC0 ,                               // END COLLECTION
};      


unsigned char code  ConfigDescript_rom[] = 
{
//配置描述符号
    0x09,   //长度          
    0x02,   //bDescriptorType 02表示配置         
    (UINT8)DESCRIPT_LENGTH,(UINT8)(DESCRIPT_LENGTH>>8),   // 0x6d+9+0x19 + 0x17 +16
    0x05,   //该配置支持的接口总数 5 个 Audio 2个 ，HID 一个 , udisk 1个  ，自定义1个
    0x01,   //配置的值，一般都只有一种配置，此值为1 
    0x00,   //描述该配置的字符串索引0 ，表示没有         
    0x80,   //bit7 必须1， bit6 供电方式 1表示自供电，bit5指示是否支持远程唤醒，bit0-4保留
    0xFA,   //表示从总线获取的最大电流量 每个单位2mA 0XFA = 250 500mA
            
// HID Class 接口0   HID 描述符总长度 9+9+7 = 0x19
    /*******************接口描述符*********************/   
    0x09,  //bLength字段。接口描述符的长度为9字节。    
    0x04,  //bDescriptorType字段。接口描述符的编号为0x04。        
    0x00,  //bInterfaceNumber字段。该接口的编号，第一个接口，编号为0  3      
    0x00,  //bAlternateSetting字段。该接口的备用编号，为0。         
    0x01,   //bNumEndpoints字段。非0端点的数目。//1个 ，为兼容HID CV 测试必须大于1 ，       
    0x03,  //bInterfaceClass字段。该接口所使用的类。本实例是HID类， //HID类的编码为0x03。           
    0x00, //bInterfaceSubClass字段。该接口所使用的子类。在HID1.1协议中，//自定义的HID设备，所以不使用子类。    
    0x00,      //bInterfaceProtocol字段。如果子类为支持引导启动的子类，
                //则协议可选择鼠标和键盘。键盘代码为0x01，鼠标代码为0x02。
                //自定义的HID设备，也不使用协议。     
    0x00,  //iConfiguration字段。该接口的字符串索引值。这里没有，为0。
    
    /******************HID描述符************************/   
    0x09,    //bLength字段。本HID描述符下只有一个下级描述符。所以长度为9字节。     
    0x21,    //bDescriptorType字段。HID描述符的编号为0x21。     
    0x10,0x01,      //bcdHID字段。本协议使用的HID1.1协议。注意低字节在先。    
    0x21,    //bCountyCode字段。设备适用的国家代码，这里选择为美国，代码0x21。      
    0x01,    //bNumDescriptors字段。下级描述符的数目。我们只有一个报告描述符。       
    0x22, //bDescriptorType字段。下级描述符的类型，为报告描述符，编号为0x22。   
    //bDescriptorLength字段。下级描述符的长度。下级描述符为报告描述符。
    sizeof(ReportDescriptor)&0xFF,(sizeof(ReportDescriptor)>>8)&0xFF,
       
   /*******************HID端点描述符*********************/
    0x07,          /*bLength: Endpoint Descriptor size*/
    0x05,           /*bDescriptorType: endpoint*/  
    0x81,          /*bEndpointAddress: Endpoint Address (IN)*/
    0x03,          /*bmAttributes: Interrupt endpoint*/
    0x04, 0x00,    /*wMaxPacketSize: 4 Byte max */ 
    0x10,          /*bInterval: Polling Interval (2^16-1)*/ 
//    0x4,          /*bInterval: Polling Interval (2^16-1)*/    

////----------------------------------------------------------------------------------------    
//     Speaker Audio 描述符总长度       8+9+40+9+9+ 7+ 11+ 9+7 = 109 6d
// AUDIO IAD  descriptors    
    0x08,           //长度 8 
    0x0B,           //bDescriptorType 接口联合描述符号 
    0x01,           // bFirstInterface 0x01
    0x02,           // bInterfaceCount 0x02
    0x01,           //  bFunctionClass      AUDIO
    0x00,           //  bFunctionSubClass   01 AUDIO CONTROL     02streaming
    0x00,           //  bFunctionProtocol
    0x04,           //  iFunction,       d

// Audio control interface standard descriptor  接口1 
   0x09, // 长度 9                            9+40+9+9+ 7+ 11+ 9+7
   0x04, // GenericDescriptor_INTERFACE 04,
   0x01, // 接口编号1,
   0,    // This is alternate setting #0  
   0,    // This interface uses no endpoint
   0x01, // CLASS  01  AUDIO
   0x01, // SUBCLASS     AUDIOCONTROL 01
   0x00, // PROTOCOL,  00
   0, // No string descriptor

   //AUDIO CONTROL  Header descriptor    40+9+9+ 7+ 11+ 9+7
    0x09, //长度 9
    0x24, //CS_INTERFACE 0x24,
    0x01, //HEADER
    0x00,0x01,//Vertion BCD AUD1.0
//    sizeof(AUDDSpeakerDriverAudioControlDescriptors),
    40,0,   // 9 + 12+ 9+10
    1,// One streaming interface
    2,// Interface number of the first AudioStreaming or MIDIStreaming interface in the Collection.

    // Input terminal descriptor
    12,//长度 12
    0x24,//CS_INTERFACE
    0x02,//INPUT_TERMINAL
    0x00,//  ID of this  iput terminal
    0x01,0x01,//UDInputTerminalDescriptor_USBSTREAMING   0x0101
    0x01,//ID of the Output Terminal to which thisInput Terminal is associated 0x01.
    0x02,//NUMCHANNELS, 01 mono   02 stereo 02
    0x03,0x00,//AUDInputTerminalDescriptor_LEFTFRONT|RIGHTFRONT,
    0x00,// No string descriptor for channels
    0x00,// No string descriptor for input terminal

// Output terminal descriptor
    0x09,//长度 9
    0x24,//CS_INTERFACE
    0x03,//OUTPUTTERMINAL
    0x01,//UID ID of this Terminal 01
    0x01,0x03,//TerminalDescriptor_SPEAKER 0x0301
    0x00,//AUDDSpeakerDriverDescriptors_INPUTTERMINAL
    0x02,//AUDDSpeakerDriverDescriptors_FEATUREUNIT  SID input   02
    0, // No string descriptor

//  Feature unit descriptor
    10,  //长度10
    0x24,//CS_INTERFACE
    0x06,//FEATUREUNIT
    0x02,///UID ID of this Terminal 02 FEATUREUNIT
    0x00,//bSourceID 00 INPUTTERMINAL
    1, //bControlSize 1
    1, //bmaControls(0) master channel0   bit0 mute bit 1 volume
    0, // Right channel controls
    0, // Left channel controls
    0, // No string descriptor

//  Audio streaming interface with 0 endpoints  接口2   alternate setting #0
    9,  //长度9                9+9+ 7+ 11+ 9+7
    4,  //USBGenericDescriptor_INTERFACE
    2,  // 接口编号2,
    0, // This is alternate setting #0
    0, // This interface uses no endpoints
    1, //bFunctionClass         AUDIO
    2, //bFunctionSubClass  01 AUDIO CONTROL     02streaming
    0, //bFunctionProtocol
    0, // No string descriptor

//  Audio streaming interface with data endpoint  接口2   alternate setting #1
    9,//长度9                     9+ 7+ 11+ 9+7
    4,//USBGenericDescriptor_INTERFACE
    2,// 接口编号2,
    1,// This is alternate setting #1
    1,// This interface uses 1 endpoints
    1, //bFunctionClass         AUDIO
    2, //bFunctionSubClass  01 AUDIO CONTROL     02streaming
    0, //bFunctionProtocol
    0, // No string descriptor

//  Audio streaming class-specific descriptor Endpoint   
    7, //长度9                 7+ 11+ 9+7
    0x24,//CS_INTERFACE
    0x01,//AS_GENERAL,
    0x00,//bTerminalLink The Terminal ID of the Terminal INPUTTERMINAL,
    0, // No internal delay because of data path
    0x01,0x00,//AUDFormatTypeOneDescriptor_PCM 0x0001
 
    // Format type I descriptor          
    11,//长度11                  11+ 9+7
    0x24,//CS_INTERFACE
    0x02,//FORMAT_TYPE
    0x01,//AUDFormatTypeOneDescriptor_FORMATTYPEONE,
    0x02,//Number of physical channel 2,

//    0x01,8,//1 bytes 8bit
    0x02,16,// 2bytes, 16bit    //the number of bytes occupied by one audio subfram bitResolution 16  
//    0x03,24,//3 bytes 24bit
    1, // One discrete frequency supported 

//    0x00,        // 32K
//    0X7D,  
//    0x00,     
     
//    0x44,        // 44.1K
//    0xac,  
//    0x00,                  
    0x80,        // 48K
    0xBB,    
    0x00,
//    0x00,        // 96kK
//    0x77,  
//    0x01,

// Audio streaming endpoint standard descriptor    9+7
    9,//长度9
    5,//USBGenericDescriptor_ENDPOINT,
    0x03,//端点3 out 
    0x01,//ISO 
    0x00,0x02,//max packet 256
//    200,0,   //max packet 256
    4, // Polling interval = 2^(x-1)/8 milliseconds (1 ms)  4
    0, // This is not a synchronization endpoint
    0, // No associated synchronization endpoint

// ISO EP       7
    0x07,
    0x25,//CS_ENDPOINT,
    0x01,//EP_GENERAL,
    0x00,
    0x00,
    0x00,0x00, 

//     自定义描述符  长度9+7 = 16    
     /*****************接口描述符*******************/     //  
     0x09,   //bLength字段。接口描述符的长度为9字节。           
     0x04,   //bDescriptorType字段。接口描述符的编号为0x04。          
     0x03,   //bInterfaceNumber字段。该接口的编号，第4个接口，编号为3。           
     0x00,    //bAlternateSetting字段。该接口的备用编号，为0。       
     0x01,    //bNumEndpoints字段。非0端点的数目。该接口有2个批量端点         
     0xff,     //bInterfaceClass字段。该接口所使用的类。 自定义类         
     0x00,    //bInterfaceSubClass字段。该接口所使用的子类。          
     0x00,     //bInterfaceProtocol字段。     
     0x05,     //iConfiguration字段。 
         
     /*************标准批量数据输出端点描述符****************/   
     0x07,   //bLength字段。端点描述符长度为7字节。       
     0x05,   //bDescriptorType字段。端点描述符编号为0x05。                
     0x04,   //D7位表示数据方向，输出端点D7为0。所以输出端点2的地址为0x04。             
     0x02,   //该端点为批端点。批量端点的编号为2。其它位保留为0。    
     0x00, 0x02, //wMaxPacketSize字段。该端点的最大包长。端点2的最大包长为512字节。
//     0x00, 0x01, //wMaxPacketSize字段。该端点的最大包长。端点2的最大包长为512字节。                
     0x00, //bInterval字段。端点查询的时间，此处无意义。

////----------------------------------------------------------------------------------------    
//     Udisk 描述符总长度   9+7+7 =  23 = 0x17   
     /*****************接口描述符*******************/     // for u disk    
     0x09,   //bLength字段。接口描述符的长度为9字节。           
     0x04,   //bDescriptorType字段。接口描述符的编号为0x04。          
     0x04,   //bInterfaceNumber字段。该接口的编号，第5个接口，编号为4。           
     0x00,    //bAlternateSetting字段。该接口的备用编号，为0。       
     0x02,    //bNumEndpoints字段。非0端点的数目。该接口有2个批量端点         
     0x08,     //bInterfaceClass字段。该接口所使用的类。大容量存储设备接口类的代码为0x08。         
     0x06,    //bInterfaceSubClass字段。该接口所使用的子类。SCSI透明命令集的子类代码为0x06。         
     0x50,     //bInterfaceProtocol字段。协议为仅批量传输，代码为0x50。      
     0x06,     //iConfiguration字段。该接口的字符串索引值.
     
     /*************标准批量数据输入端点描述符****************/     
     0x07,   //bLength字段。端点描述符长度为7字节。       
     0x05,   //bDescriptorType字段。端点描述符编号为0x05。         
     0x82,    //D7位表示数据方向，输入端点D7为1。所以输入端点2的地址为0x82。             
     0x02,     //该端点为批端点。批量端点的编号为2。其它位保留为0。       
     0x00, 0x02,    //wMaxPacketSize字段。该端点的最大包长。端点2的最大包长为512字节。                 
     0x00,  //bInterval字段。端点查询的时间，此处无意义。     

     /*************标准批量数据输出端点描述符****************/   
     0x07,   //bLength字段。端点描述符长度为7字节。       
     0x05,   //bDescriptorType字段。端点描述符编号为0x05。                
     0x02,   //D7位表示数据方向，输出端点D7为0。所以输出端点2的地址为0x02。             
     0x02,   //该端点为批端点。批量端点的编号为2。其它位保留为0。    
     0x00, 0x02, //wMaxPacketSize字段。该端点的最大包长。端点2的最大包长为512字节。    
     0x00 //bInterval字段。端点查询的时间，此处无意义。
};

unsigned char code  ConfigDescript_rom_audio[] = 
{
//配置描述符号
    0x09,   //长度          
    0x02,   //bDescriptorType 02表示配置         
    (UINT8)DESCRIPT_LENGTH_AUDIO,(UINT8)(DESCRIPT_LENGTH_AUDIO>>8),     // Audio 184 ,HID  25 ,Cconfig = 9
    0x04,   //该配置支持的接口总数 4 个，Audio 3 个 ，HID 一个 
    0x01,   //配置的值，一般都只有一种配置，此值为1 
    0x00,   //描述该配置的字符串索引0 ，表示没有         
    0x80,   //bit7 必须1， bit6 供电方式 1表示自供电，bit5指示是否支持远程唤醒，bit0-4保留
    0xFA,   //表示从总线获取的最大电流量 每个单位2mA 0XFA = 250 500mA
            
// HID Class 接口0   HID 描述符总长度 9+9+7 = 0x19
    /*******************接口描述符*********************/   
    0x09,  //bLength字段。接口描述符的长度为9字节。    
    0x04,  //bDescriptorType字段。接口描述符的编号为0x04。        
    0x00,  //bInterfaceNumber字段。该接口的编号，第一个接口，编号为0  3      
    0x00,  //bAlternateSetting字段。该接口的备用编号，为0。         
    0x01,   //bNumEndpoints字段。非0端点的数目。//1个 ，为兼容HID CV 测试必须大于1 ，       
    0x03,  //bInterfaceClass字段。该接口所使用的类。本实例是HID类， //HID类的编码为0x03。           
    0x00, //bInterfaceSubClass字段。该接口所使用的子类。在HID1.1协议中，//自定义的HID设备，所以不使用子类。    
    0x00,      //bInterfaceProtocol字段。如果子类为支持引导启动的子类，
                //则协议可选择鼠标和键盘。键盘代码为0x01，鼠标代码为0x02。
                //自定义的HID设备，也不使用协议。     
    0x00,  //iConfiguration字段。该接口的字符串索引值。这里没有，为0。
    
    /******************HID描述符************************/   
    0x09,    //bLength字段。本HID描述符下只有一个下级描述符。所以长度为9字节。     
    0x21,    //bDescriptorType字段。HID描述符的编号为0x21。     
    0x10,0x01,      //bcdHID字段。本协议使用的HID1.1协议。注意低字节在先。    
    0x21,    //bCountyCode字段。设备适用的国家代码，这里选择为美国，代码0x21。      
    0x01,    //bNumDescriptors字段。下级描述符的数目。我们只有一个报告描述符。       
    0x22, //bDescriptorType字段。下级描述符的类型，为报告描述符，编号为0x22。   
    //bDescriptorLength字段。下级描述符的长度。下级描述符为报告描述符。
    sizeof(ReportDescriptor)&0xFF,(sizeof(ReportDescriptor)>>8)&0xFF,
       
   /*******************HID端点描述符*********************/
    0x07,          /*bLength: Endpoint Descriptor size*/
    0x05,           /*bDescriptorType: endpoint*/  
    0x81,          /*bEndpointAddress: Endpoint Address (IN)*/
    0x03,          /*bmAttributes: Interrupt endpoint*/
    0x04, 0x00,    /*wMaxPacketSize: 4 Byte max */ 
    0x10,          /*bInterval: Polling Interval (2^16-1)*/ 
//    0x4,          /*bInterval: Polling Interval (2^16-1)*/    

 // 接口描述符 接口 1  71+ 9 = 80
 0x09, //长度
 0x04, //04 接口
 0x01, //InterfaceNumber= 0x01;
 0x00, //AlternateSetting = 0x00;
 0x00, //该接口所使用的端点数（不含端点0）
 0x01, //该接口使用的类 0x01 Audio 
 0x01, //子类  
 0x00, // 
 0x00,

    // HEADER       
    0x0A,
    0x24,           // CS_INTERFACE,         
    0x01,           // HEADER,
    0x00,0x01,          // Revision of class specification - 1.0
    0x47,0x00,          //Total size of class specific descriptors. 0x47 = 71
    0x02,           //Number of streaming interfaces.02
    0x02,           //AudioStreaming interface 1 belongs to this AudioControl interface.
    0x03,           //AudioStreaming interface 2  
 
    // INPUT TERMINAL       
    0x0C,
    0x24,           // CS_INTERFACE,         
    0x02,           //INPUT_TERMINAL,
    0x01,           //   ID of this  iput terminal
    0x01,0x01,          //  Terminal is USB Streaming Out.
    0x00,           // no association
    0x02,           //  01 mono   02 stereo 02
    0x03,0x00,          // Mono sets no position bits
    0x00,           //index
    0x00,           //index

    // INPUT TERMINAL       
    0x0C,
    0x24,           // CS_INTERFACE,         
    0x02,           //INPUT_TERMINAL,
    0x02,           //   ID of this  iput terminal
    0x01,0x02,          // 0x0201  Terminal is Microphone.
    0x00,           // no association
    0x01,           //  01 One channel.  
    0x01,0x00,          // Mono sets no position bits
    0x00,           //index
    0x00,           //index
  
    // OUTPUT TERMINAL          page41 basicaudio.pdf
    0x09,
    0x24,           //CS_INTERFACE,
    0x03,           //OUTPUT_TERMINAL,
    0x06,   //UID ID of this output Terminal  
    0x01,0x03,      // Output Terminal Types :0x0301 Speaker
    0x00,
    0x09,           //SID input   09
    0x00,           //index


    // OUTPUT TERMINAL          page41 basicaudio.pdf
    0x09,
    0x24,           //CS_INTERFACE,
    0x03,           //OUTPUT_TERMINAL,
    0x07,//     //UID ID of this input Terminal  
    0x01,0x01,      //  USB Streaming.
    0x00,
    0x0a,           //SID input   0a
    0x00,           //index

  //类规范 VC FEATURE_UNIT
    0x0a,   //长度10
    0x24,   //bDescriptorType CS_INTERFACE  
    0x06,   //子类 06 FEATURE_UNIT
    0x09,   //bUnitID 09 
    0x01,   //bSourceID 01
    0x01,   //bControlSize 1
    0x01,   //bmaControls(0) master channel0   bit0 mute bit 1 volume
    0x00,   //channel1 bit 1 volume
    0x00,   //channel2 bit 1 volume
    0x00,   //  
    
 
  //类规范 VC FEATURE_UNIT
    0x09,   //长度10
    0x24,   //bDescriptorType CS_INTERFACE  
    0x06,   //子类 06 FEATURE_UNIT
    0x0a,   //bUnitID 0a 
    0x02,   //bSourceID 02
    0x01,   //bControlSize 1
    0x01,   //bmaControls(0) master channel0   bit0 mute bit 1 volume
    0x00,   //channel1 bit 1 volume
    0x00, 

  // 接口描述符 接口2      9+9+ 7+b+9+7 = 0x34  =52 *2 = 104
  0x09, //长度
  0x04, //04 接口
  0x02, //InterfaceNumber= 0x002;
  0x00, //AlternateSetting = 0x00;
  0x00, //该接口所使用的端点数（不含端点0） 2
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00, // 

  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x02, //InterfaceNumber= 0x02;
  0x01, //AlternateSetting = 0x01;
  0x01, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00, //

    // CS AS GENERAL
    0x07,
    0x24,           //CS_INTERFACE,
    0x01,           //AS_GENERAL,
    0x01,                //Terminal ID 1
    0x00,//0x01       //Total interface delay,expressed in frames, not used shall set 0x00
    0x01,0x00,      // 0x0000 type_undefined, 0x0001 PCM , 0x0002 PCM8 ,0x0003IEEE_FLOAT , 0x0004 ALAW , 0x0005 Mulaw
            
    // FORMAT TYPE
    0x0b,
    0x24,           //CS_INTERFACE,
    0x02,           //FORMAT_TYPE,
    0x01,           //type
    0x02,           //Number of physical channel
    0x02,           //subframeSize  the number of bytes occupied by one audio subframe ,can be 1,2,3,4 
    0x10,           //bitResolution     the num of effectively used bits from the available bits in an audio subframe
    0x01,            //1 frequency supported.
    0x80,        // 48K
    0xBB,    
    0x00,   
//  0x44,        // 44.1kK
//  0xac,    
//  0x00,
        
    // STD EP                
    0x09,
    0x05,
    0x03,  // EP3 OUT
    0x01,  // 0D isochronous    无同步
    0x00,0x01,   //max 256   ,,实际192 bytes/ms
    0x04,   //interval 1ms      
    0x00,        //bRefresh unused
    0x00,        //bSynchAddress unused

    // ISO EP
    0x07,
    0x25,//CS_ENDPOINT,
    0x01,//EP_GENERAL,
    0x00,     //Sampling Frequency
    0x00,    //bLockDelayUnits 1: Milliseconds
    0x00,0x00,//wLockDelay 0x01 1ms



//09 04 02 00 00 01 02 00 00
  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x03, //InterfaceNumber= 0x03;
  0x00, //AlternateSetting = 0x01;
  0x00, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00, //


//09 04 02 01 01 01 02 00 00 
  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x03, //InterfaceNumber= 0x03;
  0x01, //AlternateSetting = 0x01;
  0x01, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00, //

//07 24 01 07 01 01 00
    // CS AS GENERAL
    0x07,
    0x24,           //CS_INTERFACE,
    0x01,           //AS_GENERAL,
    0x07,                 //Terminal ID 7
    0x00,//0x01       //Total interface delay,expressed in frames, not used shall set 0x00
    0x01,0x00,      // 0x0000 type_undefined, 0x0001 PCM ,

//0e 24 02 01 01 02 10 02 80 bb 00 44 ac 00 
    // FORMAT TYPE
    0x0b,
    0x24,           //CS_INTERFACE,
    0x02,           //FORMAT_TYPE,
    0x01,           //type
    0x01,           //Number of physical channel 1
    0x02,           //subframeSize  the number of bytes occupied by one audio subframe ,can be 1,2,3,4 
    0x10,           //bitResolution     the num of effectively used bits from the available bits in an audio subframe
//    0x03,             //24 bit mode
//    0x18,
        
    0x01,            //1 frequency supported.

    0x80,        // 48K
    0xBB,    
    0x00,   
//  0x44,        // 44.1kK
//  0xac,    
//  0x00,

//09 05 82 09 64 00 01 00 00
// STD EP
    0x09,
    0x05,
    0x83,  // EP2 IN
    0x01,  // 09 isochronous    无同步
//    0x80,0x00,   //max 128 ,实际96 bytes/ms
    0xf0,0x00,   //max 240 ,实际96 bytes/ms
    0x04,   //interval 1ms      
    0x00,        //bRefresh unused
    0x00,        //bSynchAddress unused
 
//07 25 01 01 00 00 00 
    // ISO EP
    0x07,
    0x25,//CS_ENDPOINT,
    0x01,//EP_GENERAL,
    0x01,     //Sampling Frequency
    0x00,    //bLockDelayUnits 1: Milliseconds
    0x00,0x00,//wLockDelay 0x01 1ms
};

unsigned char code  ConfigDescript_rom_otherspeed[] = 
{
//配置描述符号
    0x09,   //长度          
    0x07,   //other speed config
    (UINT8)DESCRIPT_LENGTH_AUDIO,(UINT8)(DESCRIPT_LENGTH_AUDIO>>8),     // Audio 184 ,HID  25 ,Cconfig = 9
    0x04,   //该配置支持的接口总数 4 个，Audio 3 个 ，HID 一个 
    0x01,   //配置的值，一般都只有一种配置，此值为1 
    0x00,   //描述该配置的字符串索引0 ，表示没有         
    0x80,   //bit7 必须1， bit6 供电方式 1表示自供电，bit5指示是否支持远程唤醒，bit0-4保留
    0xFA,   //表示从总线获取的最大电流量 每个单位2mA 0XFA = 250 500mA
            
// HID Class 接口0   HID 描述符总长度 9+9+7 = 0x19
    /*******************接口描述符*********************/   
    0x09,  //bLength字段。接口描述符的长度为9字节。    
    0x04,  //bDescriptorType字段。接口描述符的编号为0x04。        
    0x00,  //bInterfaceNumber字段。该接口的编号，第一个接口，编号为0  3      
    0x00,  //bAlternateSetting字段。该接口的备用编号，为0。         
    0x01,   //bNumEndpoints字段。非0端点的数目。//1个 ，为兼容HID CV 测试必须大于1 ，       
    0x03,  //bInterfaceClass字段。该接口所使用的类。本实例是HID类， //HID类的编码为0x03。           
    0x00, //bInterfaceSubClass字段。该接口所使用的子类。在HID1.1协议中，//自定义的HID设备，所以不使用子类。    
    0x00,      //bInterfaceProtocol字段。如果子类为支持引导启动的子类，
                //则协议可选择鼠标和键盘。键盘代码为0x01，鼠标代码为0x02。
                //自定义的HID设备，也不使用协议。     
    0x00,  //iConfiguration字段。该接口的字符串索引值。这里没有，为0。
    
    /******************HID描述符************************/   
    0x09,    //bLength字段。本HID描述符下只有一个下级描述符。所以长度为9字节。     
    0x21,    //bDescriptorType字段。HID描述符的编号为0x21。     
    0x10,0x01,      //bcdHID字段。本协议使用的HID1.1协议。注意低字节在先。    
    0x21,    //bCountyCode字段。设备适用的国家代码，这里选择为美国，代码0x21。      
    0x01,    //bNumDescriptors字段。下级描述符的数目。我们只有一个报告描述符。       
    0x22, //bDescriptorType字段。下级描述符的类型，为报告描述符，编号为0x22。   
    //bDescriptorLength字段。下级描述符的长度。下级描述符为报告描述符。
    sizeof(ReportDescriptor)&0xFF,(sizeof(ReportDescriptor)>>8)&0xFF,
       
   /*******************HID端点描述符*********************/
    0x07,          /*bLength: Endpoint Descriptor size*/
    0x05,           /*bDescriptorType: endpoint*/  
    0x81,          /*bEndpointAddress: Endpoint Address (IN)*/
    0x03,          /*bmAttributes: Interrupt endpoint*/
    0x04, 0x00,    /*wMaxPacketSize: 4 Byte max */ 
    0x10,          /*bInterval: Polling Interval (2^16-1)*/ 
//    0x4,          /*bInterval: Polling Interval (2^16-1)*/    

 // 接口描述符 接口 1  71+ 9 = 80
 0x09, //长度
 0x04, //04 接口
 0x01, //InterfaceNumber= 0x01;
 0x00, //AlternateSetting = 0x00;
 0x00, //该接口所使用的端点数（不含端点0）
 0x01, //该接口使用的类 0x01 Audio 
 0x01, //子类  
 0x00, // 
 0x00,

    // HEADER		
    0x0A,
    0x24,			// CS_INTERFACE,		 
    0x01,			// HEADER,
    0x00,0x01, 			// Revision of class specification - 1.0
    0x47,0x00,	   		//Total size of class specific descriptors. 0x47 = 71
    0x02,			//Number of streaming interfaces.02
    0x02,			//AudioStreaming interface 1 belongs to this AudioControl interface.
    0x03,			//AudioStreaming interface 2  
 
    // INPUT TERMINAL		
    0x0C,
    0x24,			// CS_INTERFACE,		 
    0x02,			//INPUT_TERMINAL,
    0x01,	 		//	 ID of this  iput terminal
    0x01,0x01, 			//  Terminal is USB Streaming Out.
    0x00,	   		// no association
    0x02,			//  01 mono   02 stereo 02
    0x03,0x00,			// Mono sets no position bits
    0x00,			//index
    0x00,			//index

    // INPUT TERMINAL		
    0x0C,
    0x24,			// CS_INTERFACE,		 
    0x02,			//INPUT_TERMINAL,
    0x02,	 		//	 ID of this  iput terminal
    0x01,0x02, 			// 0x0201  Terminal is Microphone.
    0x00,	   		// no association
    0x01,			//  01 One channel.  
    0x01,0x00,			// Mono sets no position bits
    0x00,			//index
    0x00,			//index
  
    // OUTPUT TERMINAL			page41 basicaudio.pdf
    0x09,
    0x24,			//CS_INTERFACE,
    0x03,			//OUTPUT_TERMINAL,
    0x06, 	//UID ID of this output Terminal  
    0x01,0x03,		// Output Terminal Types :0x0301 Speaker
    0x00,
    0x09,			//SID input   09
    0x00,			//index


    // OUTPUT TERMINAL			page41 basicaudio.pdf
    0x09,
    0x24,			//CS_INTERFACE,
    0x03,			//OUTPUT_TERMINAL,
    0x07,// 	//UID ID of this input Terminal  
    0x01,0x01,		//  USB Streaming.
    0x00,
    0x0a,			//SID input   0a
    0x00,			//index

  //类规范 VC FEATURE_UNIT
    0x0a,	//长度10
    0x24,	//bDescriptorType CS_INTERFACE	
    0x06,	//子类 06 FEATURE_UNIT
    0x09,	//bUnitID 09 
    0x01,	//bSourceID 01
    0x01,	//bControlSize 1
    0x01,	//bmaControls(0) master channel0   bit0 mute bit 1 volume
    0x00,	//channel1 bit 1 volume
    0x00, 	//channel2 bit 1 volume
    0x00, 	// 	
	
 
  //类规范 VC FEATURE_UNIT
    0x09,	//长度10
    0x24,	//bDescriptorType CS_INTERFACE	
    0x06,	//子类 06 FEATURE_UNIT
    0x0a,	//bUnitID 0a 
    0x02,	//bSourceID 02
    0x01,	//bControlSize 1
    0x01,	//bmaControls(0) master channel0   bit0 mute bit 1 volume
    0x00,	//channel1 bit 1 volume
    0x00, 

  // 接口描述符 接口2      9+9+ 7+b+9+7 = 0x34  =52 *2 = 104
  0x09, //长度
  0x04, //04 接口
  0x02, //InterfaceNumber= 0x002;
  0x00, //AlternateSetting = 0x00;
  0x00, //该接口所使用的端点数（不含端点0） 2
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00,	// 

  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x02, //InterfaceNumber= 0x02;
  0x01, //AlternateSetting = 0x01;
  0x01, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00,	//

    // CS AS GENERAL
    0x07,
    0x24,			//CS_INTERFACE,
    0x01,			//AS_GENERAL,
    0x01,                //Terminal ID 1
    0x00,//0x01		  //Total interface delay,expressed in frames, not used shall set 0x00
    0x01,0x00,	 	// 0x0000 type_undefined, 0x0001 PCM , 0x0002 PCM8 ,0x0003IEEE_FLOAT , 0x0004 ALAW , 0x0005 Mulaw

    // FORMAT TYPE
    0x0b,
    0x24,			//CS_INTERFACE,
    0x02,			//FORMAT_TYPE,
    0x01, 			//type
    0x02, 			//Number of physical channel
    0x02,  			//subframeSize  the number of bytes occupied by one audio subframe ,can be 1,2,3,4 
    0x10, 			//bitResolution 	the num of effectively used bits from the available bits in an audio subframe
    0x01,            //1 frequency supported.
	0x80,        // 48K
	0xBB,	 
	0x00,	
//	0x44,        // 44.1kK
//	0xac,	 
// 0x00,
		
    // STD EP				 
    0x09,
    0x05,
    0x03,  // EP3 OUT
    0x01,  // 0D isochronous	无同步
    0x00,0x01,   //max 256   ,,实际192 bytes/ms
    0x01,	//interval 1ms		
    0x00,		 //bRefresh unused
    0x00,		 //bSynchAddress unused

    // ISO EP
    0x07,
    0x25,//CS_ENDPOINT,
    0x01,//EP_GENERAL,
    0x00,     //Sampling Frequency
    0x00,    //bLockDelayUnits 1: Milliseconds
    0x00,0x00,//wLockDelay 0x01 1ms



//09 04 02 00 00 01 02 00 00
  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x03, //InterfaceNumber= 0x03;
  0x00, //AlternateSetting = 0x01;
  0x00, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00,	//


//09 04 02 01 01 01 02 00 00 
  // 接口描述符 接口2
  0x09, //长度
  0x04, //04 接口
  0x03, //InterfaceNumber= 0x03;
  0x01, //AlternateSetting = 0x01;
  0x01, //该接口所使用的端点数（不含端点0） 1
  0x01,  //该接口使用的类 0x01 Audio 
  0x02,  //子类
  0x00, // 协议
  0x00,	//

//07 24 01 07 01 01 00
    // CS AS GENERAL
    0x07,
    0x24,			//CS_INTERFACE,
    0x01,			//AS_GENERAL,
    0x07,                 //Terminal ID 7
    0x00,//0x01		  //Total interface delay,expressed in frames, not used shall set 0x00
    0x01,0x00,	 	// 0x0000 type_undefined, 0x0001 PCM ,

//0e 24 02 01 01 02 10 02 80 bb 00 44 ac 00 
    // FORMAT TYPE
    0x0b,
    0x24,			//CS_INTERFACE,
    0x02,			//FORMAT_TYPE,
    0x01, 			//type
    0x01, 			//Number of physical channel 1
    0x02,  			//subframeSize  the number of bytes occupied by one audio subframe ,can be 1,2,3,4 
    0x10, 			//bitResolution 	the num of effectively used bits from the available bits in an audio subframe
//    0x03,             //24 bit mode
//    0x18,
     	
    0x01,            //1 frequency supported.

	0x80,        // 48K
	0xBB,	 
	0x00,	
//	0x44,        // 44.1kK
//	0xac,	 
//  0x00,   

//09 05 82 09 64 00 01 00 00
// STD EP
    0x09,
    0x05,
    0x83,  // EP2 IN
    0x01,  // 09 isochronous	无同步
//    0x80,0x00,   //max 128 ,实际96 bytes/ms
    0xf0,0x00,   //max 240 ,实际96 bytes/ms
    0x01,	//interval 1ms		
    0x00,		 //bRefresh unused
    0x00,		 //bSynchAddress unused
 
//07 25 01 01 00 00 00 
    // ISO EP
    0x07,
    0x25,//CS_ENDPOINT,
    0x01,//EP_GENERAL,
    0x01,     //Sampling Frequency
    0x00,    //bLockDelayUnits 1: Milliseconds
    0x00,0x00,//wLockDelay 0x01 1ms
};



