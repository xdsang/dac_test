#include "common.h"
#include "usb_standard.h"
#include "fifo.h"
#include "hardware.h"
#include "usbhidclass.h"
#include "flash.h"
#include "eeprom.h"
#include "sfrrw.h"
#include "ms2160.h"
#include "usbuser.h"

//hid  related 
BYTE IDLE_TIME = 0;


//helper declaration
static void _parse_video_transfer_control_command(void);


//helper definition
static void _parse_video_transfer_control_command(void)
{
    VIDEO_IN_INFO_T *p_st_video_in_info = &g_st_video_in_info;
    VIDEO_OUT_INFO_T *p_st_video_out_info = &g_st_video_out_info;
    VIDEO_TRANSFER_MODE_T * p_st_transfer_mode = &g_st_video_transfer_mode;
    
    switch(hidbuff[1])
    {
    case 1: //set video input info.
        p_st_video_in_info->u8_in_color_space        = hidbuff[6] & 0x0F;
        p_st_video_in_info->u8_in_mem_color_space    = (hidbuff[6] & 0xF0) >> 4;
        p_st_video_in_info->u8_in_data_byte_sel      = hidbuff[7];
        p_st_video_in_info->st_in_video_size.u16_h   = (UINT16)hidbuff[2] << 8 | hidbuff[3];
        p_st_video_in_info->st_in_video_size.u16_v   = (UINT16)hidbuff[4] << 8 | hidbuff[5];
        g_u8_video_in_info_cntrl_flag = 1;
        break;
    case 2: //set video output info.
        p_st_video_out_info->u8_out_mode_index       = hidbuff[2];
        p_st_video_out_info->u8_out_color_space      = hidbuff[3];
        p_st_video_out_info->st_video_mem_size.u16_h = (UINT16)hidbuff[4] << 8 | hidbuff[5];
        p_st_video_out_info->st_video_mem_size.u16_v = (UINT16)hidbuff[6] << 8 | hidbuff[7];
        p_st_video_out_info->st_video_out_size.u16_h = p_st_video_in_info->st_in_video_size.u16_h;
        p_st_video_out_info->st_video_out_size.u16_v = p_st_video_in_info->st_in_video_size.u16_v;
        g_u8_video_out_info_cntrl_flag = 1;
        break;    
    case 3: //set video transfer mode
        p_st_transfer_mode->u8_trans_mode = hidbuff[2];
        p_st_transfer_mode->st_blk_info.u16_blk_st_h = 0;
        p_st_transfer_mode->st_blk_info.u16_blk_st_v = 0;
        if(TRANSFER_MODE_FIXED_BLOCK_NUM == p_st_transfer_mode->u8_trans_mode)
        {
            p_st_transfer_mode->st_blk_info.u_blk_def_mode.st_blk_num.u8_column    = hidbuff[3];
            p_st_transfer_mode->st_blk_info.u_blk_def_mode.st_blk_num.u8_row = hidbuff[4];
        }
        else if(TRANSFER_MODE_FIXED_BLOCK_SIZE == p_st_transfer_mode->u8_trans_mode)
        {
            p_st_transfer_mode->st_blk_info.u_blk_def_mode.st_blk_size.u16_h = (UINT16)hidbuff[3] << 8 | hidbuff[4];
            p_st_transfer_mode->st_blk_info.u_blk_def_mode.st_blk_size.u16_v = (UINT16)hidbuff[5] << 8 | hidbuff[6];
        }
        g_u8_video_trans_mode_cntrl_flag = 1;
        break;   
    case 4: //video transfer start or stop control
        g_u8_video_transfer_start = hidbuff[2];
        g_u8_video_trans_start_cntrl_flag = 1;
        break;   
    case 5: //turn on or turn off output video
        g_u8_video_turn_on = hidbuff[2];
        g_u8_video_turn_on_cntrl_flag = 1;
        break;
    case 7: //ms2160 power on or power off control
        g_u8_power_on = hidbuff[2];
        g_u8_power_on_cntrl_flag = 1;
    default: //frame transfer trigger
        g_u8_video_frame_index = hidbuff[2];
        g_u8_video_frame_switch_cntrl_flag = 1;
        break;
    }
}

void set_idle()
{
    IDLE_TIME =  Usb_request.wValue>>8;  
    NoData = 1;  
}

void get_idle()
{
    setupDataIn(1, &IDLE_TIME);  
}

void set_reg_rw_cmd()
{
    unsigned char xdata *xdataptr ;   //2
    unsigned char data *dataptr ;  //1
    unsigned char i,j;
    uint32 faddr;

    if((hidbuff[0]&0xf0) == 0x10)   //xdata 连续写指令
    {
        j= hidbuff[0]&0x0f;
        if(hidbuff[1]==0)  //write data
        {
           dataptr =  hidbuff[2] ;
           for(i=0;i<j;i++)
           {
             *dataptr = hidbuff[3+i];
              dataptr++;
           }           
        }
        else     //read xdata
        {               
            xdataptr =  ((UINT16)hidbuff[1]<<8)|hidbuff[2];
            for(i=0;i<j;i++)
            {
               *xdataptr = hidbuff[3+i];
               xdataptr++;
            }         
        }
    }
    else
    {   
        switch(hidbuff[0])
        {
            case 0xA6: // Video frame 传输，特殊使用
                _parse_video_transfer_control_command();
                break;
            case 0xb5:     // 读取Xdata
                if(hidbuff[1]==0)  //read data
                {
                   dataptr =  hidbuff[2] ;
                   for(i=0;i<4;i++)
                   {
                     hidbuff[3+i] =  *dataptr ;
                     dataptr++;
                   }                   
                }
                else     //read xdata
                {               
                    xdataptr =  ((UINT16)hidbuff[1]<<8)|hidbuff[2];
                    for(i=0;i<4;i++)
                    {
                      hidbuff[3+i] =  *xdataptr ;
                      xdataptr++;
                    }              
                }
                break;  
            case 0xb6: // 写Xdata
                if(hidbuff[1]==0)  //write data
                {
                   dataptr =  hidbuff[2] ;
                   *dataptr = hidbuff[3];
                }
                else     //read xdata
                {               
                    xdataptr =  ((UINT16)hidbuff[1]<<8)|hidbuff[2];
                    *xdataptr = hidbuff[3];
                }
                break;
            case 0xd5:  // 读取SDRAM 数据, 8 Bytes
            case 0xd6: //sdram write, 4 Bytes
                faddr =   hidbuff[1];
                faddr =   faddr * 256 + hidbuff[2];
                faddr =   faddr * 256 + hidbuff[3];
                if(0xD5 == hidbuff[0])
                {
                    ms2160_sdram_read(faddr, 2, hidbuff);
                }
                else
                {
                    ms2160_sdram_write(faddr, 1, &hidbuff[4]);
                }            
                break;  
            case 0xe5:  // 读取EEPROM 数据, 5 Bytes
            case 0xe6: //eeprom write, 1 Bytes
                faddr = hidbuff[1];
                faddr = faddr * 256 + hidbuff[2];
                if(0xE5 == hidbuff[0])
                {
                    eeprom_read(1 == hidbuff[7], faddr, 5, &hidbuff[3]);    
                }
                else
                {
                    eeprom_write(1 == hidbuff[7], faddr, 1, &hidbuff[3]);
                }
                break;  
            case 0xf5:    // flash read
               faddr =   hidbuff[1];
               faddr =   faddr*256 + hidbuff[2];
               faddr =   faddr*256 + hidbuff[3];
               SPI_Flash_Read_MS2160(faddr,8);
               for(i=0;i<8;i++)
               {
                 hidbuff[i]= Flashbuffer[i];
               }
                break;

            case 0xf6: // flash write 1 Bytes
               faddr =   hidbuff[1];
               faddr =   faddr*256 + hidbuff[2];
               faddr =   faddr*256 + hidbuff[3];
               Flashbuffer[0] = hidbuff[4];
               SPI_Flash_Write_NoCheck(Flashbuffer,faddr,1);
               break;

            case 0x34: // flash write 4 Bytes
               faddr =   hidbuff[1];
               faddr =   faddr*256 + hidbuff[2];
               faddr =   faddr*256 + hidbuff[3];
               for(i=0;i<4;i++)
               {
                 Flashbuffer[i] = hidbuff[i+4];
               }               
               SPI_Flash_Write_NoCheck(Flashbuffer,faddr,4);
               break;
            case 0xFE: // flash Erase_Chip 
               SPI_Flash_Erase_Chip(); 
               break;

            case 0xFB: //  
               faddr =   hidbuff[1];
               faddr =   faddr*256 + hidbuff[2];
               SPI_Flash_Erase_Sector(faddr); 
               break;

            case 0xC5:  //SFR read
               sfr_read();
            break;
            case 0xC6:   //SFR write
               sfr_write();
            break;

            default :
                break;           
        }
    }
}


void get_report(void)
{
    setupDataIn(8,hidbuff);   
}



void set_report(void)
{
    reg_EP0_SevOutPktRdy = 1;
    while (!reg_EP0_OutPktRdy) ;
    setupDataOut(Usb_request.wLength, hidbuff);                 
    set_reg_rw_cmd();
    NoData = 1;
}

void hidclasscommand_process()
{
    switch(Usb_request.bRequest)
    {
        case GET_REPORT:          //  0x01
        get_report() ;
        break;   
                                             
        case SET_REPORT:           // 0x09
        set_report();
        break;
        
        case GET_IDLE:          //  0x02
        get_idle() ;
        break;   
                                             
        case SET_IDLE:           // 0x0a
        set_idle();
        break;
        
        default:
        Error = 1;
        break;
    }
}












