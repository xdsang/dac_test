#ifndef __USBUSER_H__
#define __USBUSER_H__

typedef struct _USB_REQUEST
{
    UINT8   bmRequestType;
    UINT8   bRequest;
    UINT16  wValue;
    UINT16  wIndex;
    UINT16  wLength;    
}USB_REQUEST;


extern bit Error ;
extern bit NoData ;
extern bit ResetFlag ;
extern bit TestCMD ;
extern bit Ep0IntFlag ;
extern bit TestFlag ;
extern unsigned char data intr_in  ;     
extern unsigned char data intr_usb ;      
extern unsigned char data intr_out ;
extern unsigned char data audioflag ;

extern xdata USB_REQUEST  Usb_request;
extern xdata WORD dscrLength;
extern xdata WORD BytesLeft ;
extern xdata BYTE DevState;
extern xdata BYTE NewAddress ;
extern xdata BYTE comdata[];
extern data  BYTE hidbuff[];

extern unsigned char g_u8_video_turn_on_cntrl_flag;
extern unsigned char g_u8_video_in_info_cntrl_flag;
extern unsigned char g_u8_video_out_info_cntrl_flag;
extern unsigned char g_u8_video_trans_mode_cntrl_flag;
extern unsigned char g_u8_video_trans_start_cntrl_flag;
extern unsigned char g_u8_video_frame_switch_cntrl_flag;
extern unsigned char g_u8_power_on_cntrl_flag;
extern unsigned char g_u8_usb_suspend_cntrl_flag;
extern unsigned char g_u8_usb_resume_cntrl_flag;

extern unsigned char g_u8_video_transfer_start;
extern unsigned char g_u8_power_on;
extern unsigned char g_u8_video_turn_on;
extern unsigned char g_u8_video_frame_index;
extern VIDEO_IN_INFO_T g_st_video_in_info;
extern VIDEO_OUT_INFO_T g_st_video_out_info;
extern VIDEO_TRANSFER_MODE_T g_st_video_transfer_mode;

void usbinit();
void int_endpoint0_service(void);
void usb_bus_resume();
void usb_bus_suspend();
void usb_bus_reset();
void int_service_isr0_Rom();
void endpoint_initial();
void ep2init();


#endif