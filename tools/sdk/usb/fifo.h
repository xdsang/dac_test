#ifndef __FIFO_H__
#define __FIFO_H__

void setupDataIn(BYTE nBytes, BYTE *p);
void setupDataOut(BYTE nBytes, BYTE *p);
void EP2DataIn(u16 Len,uint8 * Buf);
u16 EP2DataOut(u16 Len, uint8 *Buf);

#endif