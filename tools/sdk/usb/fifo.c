#include "hardware.h"
#include "descript.h"
#include "datatype.h"
#include "usbrequest.h"
#include "usb_standard.h"
#include "usbuser.h"


    

void setupDataIn(BYTE nBytes, BYTE *p)
{
    WORD data num = 0;
    if(BytesLeft != 0)
    {
        if(Usb_request.wLength <= dscrLength)
        {
             num = Usb_request.wLength - BytesLeft;
        }
        else
        {
             num = dscrLength - BytesLeft;
        }
    }
    while(nBytes)
    {
        if(nBytes > 1)
        {
                reg_usb_End0 = *(p + num);
                num++;
                reg_usb_End0 = *(p + num);
                num++;
                nBytes = nBytes - 2;
        }
        else
        {
                reg_usb_End0 = *(p + num);
                nBytes = nBytes - 1;
                num++;
        }
    }
}                                                                                                                          

void setupDataOut(BYTE nBytes, BYTE *p)
{
    BYTE data num = 0;
    while(nBytes)
    {
        if(nBytes > 1)
        {
            *(p + num) = reg_usb_End0;
            num++;
            *(p + num) = reg_usb_End0;
            num++;
            nBytes = nBytes - 2;
        }
        else
        {
            *(p + num) = reg_usb_End0;
            nBytes = nBytes - 1;
        }
    }
}


/********************************************************************
函数功能：将数据写入端点缓冲区函数。
入口参数：；Len：需要发送的长度；Buf：保存数据的缓冲区。
返    回：无。
备    注：无。
********************************************************************/
//void EP1DataIn(uint8 Len,uint8 * Buf)
//{
//    uint8 i;  
//    reg_usb_Index=1;         // 
//  //  reg_IntPktRdy = 0;
//    reg_usb_InCSRL &= 0xFE;
////    reg_InMode = 1;          // switch to in mode 
//   reg_usb_InCSRH |= 0x20;
////    reg_InFlushFifo = 1;     //clearfifi
//  reg_usb_InCSRL |= 0x08;
//    for(i=0;i<Len;i++)
//    {
//        reg_usb_End1 = *(Buf+i)  ;
//    }
////    reg_IntPktRdy = 1; //  
//
//   reg_usb_InCSRL |= 0x01;
//}

void EP2DataIn(u16 Len,uint8 * Buf)
{
    u16 i;  
    reg_usb_Index=2;         // 
//    reg_usb_InCSRH |= 0x20;  // switch to in mode
//    reg_InFlushFifo = 1;     //
//    reg_usb_InCSRL |= 0x08;
    reg_usb_InCSRL = 0x08;
    for(i=0;i<Len;i++)
    {
        reg_usb_End2 = *(Buf+i)  ;
    }
    reg_IntPktRdy = 1;   
//   reg_usb_InCSRL |= 0x01;
}





u16 EP2DataOut(u16 Len, uint8 *Buf)
{
    u16 i,j;
    reg_usb_Index=2;         //
    j =  reg_usb_CountH;
    j=j*256+  reg_usb_CountL;
    if(j>Len) //如果实际接收到的数据比 要读的字节数 长
    {
        j=Len;  //则只读指定的长度数据
    }
    for(i=0;i<j;i++)
    {
        *(Buf+i)= reg_usb_End2; //读一字节数据
    }
    reg_usb_OutCSRL |=0x10;  //flush fifo
    reg_usb_OutCSRL&=0XFE;
    return j; //返回实际读取的字节数。
}




