#include "datatype.h"
#ifndef _USBREQUEST_H
#define _USBREQUEST_H


//extern xdata WORD BytesLeft;
//extern xdata BYTE NewAddress;
//extern xdata BYTE DevState;
//extern bit Ep0IntFlag;

void get_status(void);
void set_feature(void);
void clear_feature(void);
void set_address(void);
void get_descriptor(void);
void get_configuration(void);
void set_configuration(void);
void get_interface(void);
void set_interface(void);


void CMDRead(void);
void Cmd_Process(void);


#endif