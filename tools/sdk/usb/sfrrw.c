#include "common.h"
#include "hardware.h"
#include "sfrrw.h"

extern data BYTE hidbuff[8];

void sfr_write()
{
    switch(hidbuff[1])
    {
     //spi 
      case  0xc7:
      SPCR  = hidbuff[2] ;
      break;
      case  0xc8:
      SPSR  = hidbuff[2] ;
      break;
      case  0xc9:
      SPDR  = hidbuff[2] ;
      break;
      case  0xca:
      SPER  = hidbuff[2] ;
      break;

      //Timer1 
      case  0xbc:
      T1LL  = hidbuff[2] ;
      break;
      case  0xBD:
      T1L   = hidbuff[2] ;
      break;
      case  0xBE:
      T1H  = hidbuff[2] ;
      break;
      case  0xBF:
      T1HH  = hidbuff[2] ;
      break;
      case  0x88:
      TCON  = hidbuff[2] ;
      break;
      case  0x89:
      TPS  = hidbuff[2] ;
      break;

      //Uart0 
      case  0x98:
      S0CON  = hidbuff[2] ;
      break;
      case  0x99:
      S0BUF   = hidbuff[2] ;
      break;
      case  0xAA:
      S0RELL  = hidbuff[2] ;
      break;
      case  0xBA:
      S0RELH  = hidbuff[2] ;
      break;

      //USB
      case  0x93:
      reg_usb_Index  = hidbuff[2] ;
      break;
      case  0x94:
      reg_usb_Faddr   = hidbuff[2] ;
      break;
      case  0x95:
      reg_usb_Power  = hidbuff[2] ;
      break;
      case  0x96:
      reg_usb_IntrIn  = hidbuff[2] ;
      break;
      case  0x97:
      reg_usb_IntrOut  = hidbuff[2] ;
      break;
      case  0xEC:
      reg_usb_IntrInE   = hidbuff[2] ;
      break;
      case  0x9B:
      reg_usb_IntrOutE  = hidbuff[2] ;
      break;
      case  0x9C:
      reg_usb_IntrUSB  = hidbuff[2] ;
      break;
      case  0x9D:
      reg_usb_IntrUSBE  = hidbuff[2] ;
      break;
      case  0x9E:
      reg_usb_FrameL   = hidbuff[2] ;
      break;
      case  0x9F:
      reg_usb_FrameH  = hidbuff[2] ;
      break;
      case  0xA1:
      reg_usb_Testmode  = hidbuff[2] ;
      break;
      case  0xA2:
      reg_usb_InMaxPL  = hidbuff[2] ;
      break;
      case  0xA3:
      reg_usb_InMaxPH   = hidbuff[2] ;
      break;
      case  0xA4:
      reg_usb_OutMaxPL  = hidbuff[2] ;
      break; 
      case  0xA5:
      reg_usb_OutMaxPH  = hidbuff[2] ;
      break;
      case  0xA6:
      reg_usb_CountL  = hidbuff[2] ;
      break;
      case  0xA7:
      reg_usb_CountH   = hidbuff[2] ;
      break;
      case  0xAB:
      reg_usb_End0  = hidbuff[2] ;
      break;
      case  0xAC:
      reg_usb_End1  = hidbuff[2] ;
      break;
      case  0xAD:
      reg_usb_End2   = hidbuff[2] ;
      break;
      case  0xAE:
      reg_usb_End3  = hidbuff[2] ;
      break;
      case  0xAF:
      reg_usb_End4  = hidbuff[2] ;
      break;
      case  0x80:
      reg_usb_CSR0  = hidbuff[2] ;
      break;
      case  0x90:
      reg_usb_InCSRH  = hidbuff[2] ;
      break;
      case  0x8B:
      reg_usb_OutCSRL   = hidbuff[2] ;
      break;
      case  0x8C:
      reg_usb_OutCSRH  = hidbuff[2] ;
      break;

      //Other
      case  0xe9:
      reg_usb_BusRest  = hidbuff[2] ;
      break;
      case  0xea:
      reg_mcu_access_usb_mode  = hidbuff[2] ;
      break;
      case  0xeb:
      reg_soc_test_mode  = hidbuff[2] ;
      break;
      case  0xa0:
      reg_gpio_data   = hidbuff[2] ;
      break;
      case  0xb0:
      reg_gpio_oen  = hidbuff[2] ;
      break;

      case  0xb1:
      reg_gpio_pu   = hidbuff[2] ;
      break;
      case  0xb2:
      reg_gpio_pd  = hidbuff[2] ;
      break;
      case  0x8e:
      CKCON  = hidbuff[2] ;
      break;

      default :
      break;
    }
}



void sfr_read()
{
    switch(hidbuff[1])
    {
     //spi 
      case  0xc7:
      hidbuff[2]  =  SPCR;
      break;
      case  0xc8:
      hidbuff[2]  =  SPSR;
      break;
      case  0xc9:
      hidbuff[2]  =  SPDR;
      break;
      case  0xca:
      hidbuff[2]  =  SPER;
      break;

      //Timer1 
      case  0xbc:
      hidbuff[2]   =  T1LL;
      break;
      case  0xBD:
      hidbuff[2]   =  T1L;
      break;
      case  0xBE:
      hidbuff[2]  =  T1H;
      break;
      case  0xBF:
      hidbuff[2]  =  T1HH;
      break;
      case  0x88:
      hidbuff[2]  = TCON ;
      break;
      case  0x89:
      hidbuff[2]  =  TPS;
      break;

      //Uart0 
      case  0x98:
      hidbuff[2]  = S0CON ;
      break;
      case  0x99:
      hidbuff[2]   = S0BUF ;
      break;
      case  0xAA:
      hidbuff[2]  =  S0RELL;
      break;
      case  0xBA:
      hidbuff[2]  = S0RELH ;
      break;

      //USB
      case  0x93:
      hidbuff[2]  = reg_usb_Index ;
      break;
      case  0x94:
      hidbuff[2]   = reg_usb_Faddr ;
      break;
      case  0x95:
      hidbuff[2]  =  reg_usb_Power;
      break;
      case  0x96:
      hidbuff[2]  =  reg_usb_IntrIn;
      break;
      case  0x97:
      hidbuff[2]  = reg_usb_IntrOut ;
      break;
      case  0xEC:
      hidbuff[2]   = reg_usb_IntrInE ;
      break;
      case  0x9B:
      hidbuff[2] =  reg_usb_IntrOutE ;
      break;
      case  0x9C:
      hidbuff[2]  =  reg_usb_IntrUSB;
      break;
      case  0x9D:
      hidbuff[2]  =  reg_usb_IntrUSBE;
      break;
      case  0x9E:
      hidbuff[2]   = reg_usb_FrameL ;
      break;
      case  0x9F:
      hidbuff[2]   = reg_usb_FrameH ;
      break;
      case  0xA1:
      hidbuff[2]  =  reg_usb_Testmode;
      break;
      case  0xA2:
      hidbuff[2]  =  reg_usb_InMaxPL;
      break;
      case  0xA3:
      hidbuff[2]  = reg_usb_InMaxPH  ;
      break;
      case  0xA4:
      hidbuff[2]  =  reg_usb_OutMaxPL;
      break; 
      case  0xA5:
      hidbuff[2]  = reg_usb_OutMaxPH ;
      break;
      case  0xA6:
      hidbuff[2]  = reg_usb_CountL ;
      break;
      case  0xA7:
      hidbuff[2]   = reg_usb_CountH ;
      break;
      case  0xAB:
      hidbuff[2]   = reg_usb_End0;
      break;
      case  0xAC:
      hidbuff[2]  =  reg_usb_End1;
      break;
      case  0xAD:
      hidbuff[2]   = reg_usb_End2 ;
      break;
      case  0xAE:
      hidbuff[2]  =  reg_usb_End3;
      break;
      case  0xAF:
      hidbuff[2]  = reg_usb_End4 ;
      break;
      case  0x80:
      hidbuff[2]  =  reg_usb_CSR0;
      break;
      case  0x90:
      hidbuff[2]  =  reg_usb_InCSRH;
      break;
      case  0x8B:
      hidbuff[2]   = reg_usb_OutCSRL ;
      break;
      case  0x8C:
      hidbuff[2]  =  reg_usb_OutCSRH;
      break;

      //Other
      case  0xe9:
      hidbuff[2]  =  reg_usb_BusRest;
      break;
      case  0xea:
      hidbuff[2]  =  reg_mcu_access_usb_mode;
      break;
      case  0xeb:
      hidbuff[2] = reg_soc_test_mode;
      break;
      case  0xa0:
      hidbuff[2]   =  reg_gpio_data;
      break;
      case  0xb0:
      hidbuff[2]   = reg_gpio_oen;
      break;

      case  0xb1:
      hidbuff[2]   = reg_gpio_pu;
      break;
      case  0xb2:
      hidbuff[2]   = reg_gpio_pd;
      break;
      case  0x8e:
      hidbuff[2] = CKCON;
      break;
      default :
      break;
     }
}