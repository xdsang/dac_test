#include "common.h"
#include "hardware.h"
#include "usbrequest.h"
#include "descript.h"
#include "usb_standard.h"
#include "SCSI.h"
#include "ms2160top.h"
#include "user_setting.h"
#include "usbuser.h"
#include "flash.h" 

// usb phy reg 
#define reg_usbsq_f10f   (*(volatile unsigned char __XDATA*)0xf10f) 
#define reg_usbsq_f10e   (*(volatile unsigned char __XDATA*)0xf10e) 

//I2S2USB
#define reg_i2s2usb_ctrl_f880   (*(volatile unsigned char __XDATA*)0xf880) 
#define reg_f020   (*(volatile unsigned char __XDATA*)0xf020) 

xdata BYTE ConfigDescript[0x100];

bit Error = 0;
bit NoData = 0;
bit ResetFlag = 0;
 
bit TestCMD = 0;
bit Ep0IntFlag = 0;
bit TestFlag = 0;
extern UINT8 __DATA intr_in  ;    
extern UINT8 __DATA intr_usb ;     
extern UINT8 __DATA intr_out ;
extern UINT8 __DATA g_u8_video_output_port  ;

unsigned char data audioflag = 0 ;

xdata USB_REQUEST  Usb_request;
xdata WORD dscrLength;
xdata WORD BytesLeft = 0;
xdata BYTE DevState = DEVSTATE_DEFAULT;
xdata BYTE NewAddress = 0xff;
xdata BYTE comdata[8];
data  BYTE hidbuff[8];

//video control related variable definition
unsigned char g_u8_video_turn_on_cntrl_flag = 0;
unsigned char g_u8_video_in_info_cntrl_flag = 0;
unsigned char g_u8_video_out_info_cntrl_flag = 0;
unsigned char g_u8_video_trans_mode_cntrl_flag = 0;
unsigned char g_u8_video_trans_start_cntrl_flag = 0;
unsigned char g_u8_video_frame_switch_cntrl_flag = 0;
unsigned char g_u8_power_on_cntrl_flag = 0;
unsigned char g_u8_usb_suspend_cntrl_flag = 0;
unsigned char g_u8_usb_resume_cntrl_flag = 0;

unsigned char g_u8_video_transfer_start = 0;
unsigned char g_u8_power_on = 0;
unsigned char g_u8_video_turn_on = 0;
unsigned char g_u8_video_frame_index = 0;
VIDEO_IN_INFO_T g_st_video_in_info;
VIDEO_OUT_INFO_T g_st_video_out_info;
VIDEO_TRANSFER_MODE_T g_st_video_transfer_mode;

void fullspeedinit()
{
    u8 i;
    reg_i2s2usb_ctrl_f880 = 0x15; // 使能16 BIT I2S mode 
//        reg_i2s2usb_ctrl_f880 = 0x37; // 使能24 BIT I2S mode 
    reg_Clkctrl2_f005 |= 0x08;
//        reg_Rstctrl1_f008 |= 0x02;
    reg_f020 |= 0x01; //Enable audio mic

    dev_desc[11] = 0x08;// PID 更改为2108
    dev_desc[15] = 0;// Product  
    dev_desc[16] = 0;// serial 
    for(i=0;i<DESCRIPT_LENGTH_AUDIO;i++)  // ROM 的总长度
    {
       ConfigDescript[i] = ConfigDescript_rom_otherspeed[i];
    } 
    ConfigDescript[1] = 2;       
}


void usb_descript_setting()
{
    UINT8 i ;
    //  产品信息以及
    if(g_u8_patchStringMINFO_rom[0] == 0xFF)
    {
        for(i=0;i<ManufacturerString_rom[0];i++)
        {
            ManufacturerString[i] = ManufacturerString_rom[i];
        }        
    }
    else
    {
        for(i=1;i<g_u8_patchStringMINFO_rom[0];i++)
        {
            ManufacturerString[2*i]   = g_u8_patchStringMINFO_rom[i];
            ManufacturerString[2*i+1] = 0;
        } 
        ManufacturerString[0] = g_u8_patchStringMINFO_rom[0] * 2;
        ManufacturerString[1] = 3;
    }

    if(g_u8_patchStringPINFO_rom[0] == 0xFF)
    {
        for(i=0;i<ProduceString_rom[0];i++)
        {
           ProduceString[i] =  ProduceString_rom[i];
        }    
    }
    else
    {
        for(i=1;i<g_u8_patchStringPINFO_rom[0];i++)
        {
           ProduceString[2*i]   =  g_u8_patchStringPINFO_rom[i];
           ProduceString[2*i+1] =  0;
        }
        ProduceString[0] = g_u8_patchStringPINFO_rom[0] * 2;
        ProduceString[1] = 3;
    }
             
    if(g_u8_video_output_port ==9)
    {   
        reg_i2s2usb_ctrl_f880 = 0x15; // 使能16 BIT I2S mode 
//        reg_i2s2usb_ctrl_f880 = 0x37; // 使能24 BIT I2S mode 
        reg_Clkctrl2_f005 |= 0x08;
//        reg_Rstctrl1_f008 |= 0x02;
        reg_f020 |= 0x01; //Enable audio mic
         
        dev_desc[11] = 0x08;// PID 更改为2108
        dev_desc[15] = 0;// Product  
        dev_desc[16] = 0;// serial 
        for(i=0;i<DESCRIPT_LENGTH_AUDIO;i++)  // ROM 的总长度
        {
           ConfigDescript[i] = ConfigDescript_rom_audio[i];
        } 
    }
    else
    {
        for(i=0;i<DESCRIPT_LENGTH;i++)  // ROM 的总长度
        {
           ConfigDescript[i] = ConfigDescript_rom[i];
        }   
        if(SPI_FLASH_TYPE==0 || g_u8_usbdisken == 0)// 如果没有Flash  则不要U 盘功能
        {
           ConfigDescript[2] = 0xb6 - 23;  //DESCRIPT_LENGTH -23 总数据长度-3
           ConfigDescript[4] = 4;  // 接口数量减1
        }
    }
    if((reg_usb_Power&0x10) == 0) //full speed mode
    {
       fullspeedinit();  
    }
    if(g_u16_usbvid != 0xFFFF)
    {
        dev_desc[8] = (UINT8)g_u16_usbvid;
        dev_desc[9] = (UINT8)(g_u16_usbvid >> 8);
    }
    if(g_u16_usbpid != 0xFFFF)
    {
        dev_desc[10] = (UINT8)g_u16_usbpid;
        dev_desc[11] = (UINT8)(g_u16_usbpid >> 8);
    }
    
    
}

void usbinit(void)
{
// usb phy init ,增加USB Device接收灵敏度
    reg_usbsq_f10f = 0xb8;
    reg_usbsq_f10e = 0x04;

    reg_mcu_access_usb_mode = 1;// disable dma
    reg_Rstctrl3_f00a = 0x07;
    Delay_us(1);
    reg_Rstctrl3_f00a = 0x00;
//    usb_descript_setting();     // update config table   
    reg_usb_Power = 0x70;// isoupdate  softcon  hsenab hsmode  rst resume suspendmode  enablesuspendM   
    reg_usb_Index = 0;            // Init interrupt 
    reg_usb_IntrUSBE = 0x04;      // 上电复位后支持Reset 中断
    reg_usb_IntrInE = SET_BIT0|SET_BIT2 ;  // ep0,ep2     
    reg_usb_IntrOutE = SET_BIT2 ;  //   EP2 Udisk 
    reg_mcu_access_usb_mode = 0;
}

void int_service_usb () interrupt 0     //usb interrupt
{
    int_service_isr0_Rom(); 
}

void int_service_isr0_Rom()    
{

 reg_mcu_access_usb_mode = 1; 
 while(1)      // add 20180314 xinyue.yang
 {
   if(reg_mcu_access_usb_mode&BIT4)
   {break;}
 }                                                                         
    reg_usb_Index=0;        
    intr_usb = reg_usb_IntrUSB;
    intr_in = reg_usb_IntrIn;
    intr_out = reg_usb_IntrOut; 
    if(user_get_usercode_status() & USERCODE_STATUSF_PATCH3_VALID)
    {
        #pragma asm
        patch_for_usbtask();
        #pragma endasm
    }
    if(intr_usb & 0x01)                         //suspend  bit0
    {
        usb_bus_suspend();
        g_u8_usb_suspend_cntrl_flag = 1; 
    } 
    if(intr_usb & 0x02)                         //resume   bit1
    {
        usb_bus_resume();    
        g_u8_usb_resume_cntrl_flag = 1;
    } 
    if(intr_usb & 0x04)                         //reset bit2
        usb_bus_reset();
    if (intr_in & 0x04)     //ep2 in 
    {   
        Ep2SendData();
    }
    if (intr_out & 0x04)        //ep2 out 
    {   
       int_check_cbw();
    }   
    if (intr_in & 0x01)                         //ep0   bit0
        {
            int_endpoint0_service();// 
        }
 reg_mcu_access_usb_mode = 0;                                                                                                   
}



void int_endpoint0_service(void)
{
    BYTE data indextemp = 0;
    reg_usb_Index   = 0x00;     
    if(reg_usb_CSR0 & 0x04)     //SentStall
    {
        reg_usb_CSR0 &= 0xFB;//(~0x04);  
        Ep0IntFlag = 0;
    }
    if(reg_usb_CSR0 & 0x10)   //SetupEnd
    {
        reg_usb_CSR0 |= 0x80;
        Ep0IntFlag = 0; 
    }
    if(NewAddress != 0xff)
    {
        reg_usb_Faddr = NewAddress;//  SET_ADDRESS : NewAddress = (BYTE)(wValue)
        if((DevState == DEVSTATE_DEFAULT) && NewAddress)
        {
            DevState = DEVSTATE_ADDRESS;//DevState = DEVSTATE_ADDRESS;
        }
        else if((DevState == DEVSTATE_ADDRESS) && !NewAddress)//
        {
            DevState = DEVSTATE_DEFAULT;
        }
            NewAddress = 0xff;
    }
    if(TestCMD == 1)
    {
        TestFlag = 1;
        TestCMD = 0;
        Ep0IntFlag = 1;
    }
    if(reg_EP0_OutPktRdy == 1)
        {
            CMDRead();//寄存器值拷贝，跳转到 main 函数的while(Ep0IntFlag)
            Ep0IntFlag = 1;
        }
    else 
        {
            if(BytesLeft != 0)
            {
                Ep0IntFlag = 1;
            }
        
        }
    if(Ep0IntFlag ==1)
        {
          indextemp = reg_usb_Index;
          reg_usb_Index = 0x00;
          if(user_get_usercode_status() & USERCODE_STATUSF_PATCH4_VALID)
          {
              #pragma asm
              patch_for_usbcmd();
              #pragma endasm
          }
          else
          {
              Cmd_Process();
          }
          Ep0IntFlag = 0;
          reg_usb_Index = indextemp;    
        }
}



void usb_bus_resume(void)
{
    reg_usb_Power = reg_usb_Power & 0x7e;
}


void usb_and_req_reset()
{
    Ep0IntFlag = 0;                                                             //USB RESET
    DevState = DEVSTATE_DEFAULT;                                                        //2.SET Index to 0
    reg_EP0_SevOutPktRdy = 1;       //Set 1 use to clear  OutPktRdy //3.flush all ep fifo
}


void usb_bus_reset(void)
{
    while(reg_usb_BusRest);
    reg_usb_Index = 0;
//    reg_usb_IntrUSBE = SET_BIT0|SET_BIT1|SET_BIT2;            //d3 sof ,d2 reset ,d1 resume ,d0 suspend 
    reg_usb_IntrUSBE = SET_BIT0|SET_BIT1|SET_BIT2;          //d3 sof ,d2 reset ,d1 resume ,d0 suspend 
    reg_usb_IntrInE  = SET_BIT0|SET_BIT2 ;    //  
    reg_usb_IntrOutE = SET_BIT2 ;   //    Udisk
    usb_and_req_reset();
    reg_usb_Power = 0x70;  //   InitialPowerOn();
    endpoint_initial();
    usb_descript_setting();
}
//
void usb_bus_suspend(void)
{

}

void ep2init()
{
// mass storage ep2 
    reg_usb_Index = 0x02;
    reg_usb_InMaxPL = 0x00;   //最大包长度512 
    reg_usb_InMaxPH = 0x02;
    reg_usb_InCSRH  = 0x20;   //        
    reg_usb_InCSRL  = 0x48;   // 
    
    reg_usb_OutMaxPL = 0x00;     //最大包长度512 
    reg_usb_OutMaxPH = 0x02;    //最大包长度512 
    reg_usb_OutCSRL = 0x90;
    reg_usb_OutCSRH = 0x00;     
    TransportStage=COMMAND_STAGE ; 
}

void endpoint_initial()
{
// HID endpoint ep1 
    reg_usb_Index = 0x01;
    reg_usb_InMaxPL = 0x40;
    reg_usb_InMaxPH = 0x00;
    reg_usb_InCSRH  = 0x20;   //        
    reg_usb_InCSRL  = 0x04;   // always nack 
    
// mass storage ep2 
    ep2init();
 
// audio endpoint  ep3 iso out  speaker
        reg_usb_Index = 0x03;
        reg_usb_OutMaxPL = 0x00;
        reg_usb_OutMaxPH = 0x02;    //最大包长度512
        reg_usb_OutCSRL = 0;
        reg_usb_OutCSRH = 0x60; // bit6 iso mode ,bit5 dma enable,bit4,disnyet

// audio endpoint  ep3 iso in   microphone
        reg_usb_InMaxPL = 0x60;    //96 bytes/ms
//        reg_usb_InMaxPL = 0X90;    //96 bytes/ms
        reg_usb_InMaxPH = 0x00;    // 48K 单声道自动模式
        reg_usb_InCSRL  = 0x04;
        reg_usb_InCSRH  = 0xF0;   //自动模式
        reg_IntPktRdy = 1;

// Video endpoint  ep4 Video bulk out 
        reg_usb_Index = 0x04;
        reg_usb_OutMaxPL = 0x00;
        reg_usb_OutMaxPH = 0x02;    //最大包长度512
        reg_usb_OutCSRL = 0;
        reg_usb_OutCSRH = 0x20; //dma enable                          
}




