#ifndef __USB_STANDARD_H__
#define __USB_STANDARD_H__



#define USB_CLASS_AUDIO                     0x01
#define USB_SUBCLASS_AUDIOCONTROL           0x01
#define USB_SUBCLASS_AUDIOSTREAM            0x02
#define USB_DESC_SUBTYPE_AC_HEADER          0x01
#define USB_DESC_SUBTYPE_AC_INPUT_TERM      0x02
#define USB_DESC_SUBTYPE_AC_OUTPUT_TERM     0x03
#define USB_DESC_SUBTYPE_AC_FEAT_UNIT       0x06
#define USB_DESC_SUBTYPE_AS_GENERAL         0x01
#define USB_DESC_SUBTYPE_AS_FORMAT          0x02
#define USB_DESC_SUBTYPE_AS_EP_GENERAL      0x01

//
// USB defined descriptor types
//
#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define USB_CONFIGURATION_DESCRIPTOR_TYPE       0x02
#define USB_STRING_DESCRIPTOR_TYPE              0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05
//#define USB_POWER_DESCRIPTOR_TYPE             0x06
#define USB_DEVICE_QUALIFIER_DESCRIPTOR_TYPE    0x06
#define USB_OTHER_SPEED_CONFIGURATION           0x07
#define USB_CS_INTERFACE_DESCRIPTOR_TYPE        0x24
#define USB_CS_ENDPOINT_DESCRIPTOR_TYPE         0x25

//
// USB defined request codes
//
#define USB_REQUEST_GET_STATUS                    0x00
#define USB_REQUEST_CLEAR_FEATURE                 0x01
#define USB_REQUEST_SET_FEATURE                   0x03
#define USB_REQUEST_SET_ADDRESS                   0x05
#define USB_REQUEST_GET_DESCRIPTOR                0x06
#define USB_REQUEST_SET_DESCRIPTOR                0x07
#define USB_REQUEST_GET_CONFIGURATION             0x08
#define USB_REQUEST_SET_CONFIGURATION             0x09
#define USB_REQUEST_GET_INTERFACE                 0x0A
#define USB_REQUEST_SET_INTERFACE                 0x0B
#define USB_REQUEST_SYNC_FRAME                    0x0C

//
// request mask
//
#define USB_RECIPIENT               (unsigned char)0x1F
#define USB_RECIPIENT_DEVICE        (unsigned char)0x00
#define USB_RECIPIENT_INTERFACE     (unsigned char)0x01
#define USB_RECIPIENT_ENDPOINT      (unsigned char)0x02

#define USB_REQUEST_TYPE_MASK       (unsigned char)0x60
#define USB_REQUEST_DIR_IN          (unsigned char)0x80
#define USB_REQUEST_DIR_OUT         (unsigned char)0x00
#define USB_STANDARD_REQUEST        (unsigned char)0x00
#define USB_CLASS_REQUEST           (unsigned char)0x20
#define USB_VENDOR_REQUEST          (unsigned char)0x40



#define DEV_CFG_SELF_POWER_EN           0x01
#define DEV_CFG_WAKEUP_EN               0x02
#define DEV_CFG_MAX_POWER_EN            0x04
#define DEV_CFG_HUAYA_SUSPEND_EN        0x08
#define DEV_CFG_U_DISK_DEV              0x00
#define DEV_CFG_A_D_DEV                 0x10
#define DEV_CFG_D_D_DEV                 0x20

 


#endif //__USB_STANDARD_H__


