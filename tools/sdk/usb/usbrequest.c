#include "common.h"
#include "descript.h"
#include "hardware.h"
#include "usbrequest.h"
#include "usb_standard.h"
#include "fifo.h"
#include "usbhidclass.h"
#include "usb_audio10class.h"
#include "usb_massStorageclass.h" 
#include <stdio.h>
#include "usbuser.h"
#include "ms2160_drv_audio.h"
#include "ms2160top.h"
#include "SCSI.h"

extern UINT8 __DATA g_u8_video_output_port ;

void Cmd_Process()
{
   BYTE indextemp;  
   indextemp = reg_usb_Index;
    if((Usb_request.bmRequestType & 0x80) == bmBIT7)
    {
        reg_EP0_SevOutPktRdy = 1;
    }       
    if((Usb_request.bmRequestType & 0x60) == 0x00)//standard
        {
          
          switch(Usb_request.bRequest)
          {
             case GET_STATUS:
                    get_status();
                    break;
             case SET_FEATURE:
                    set_feature();
                    break;
             case CLEAR_FEATURE:
                    clear_feature();
                    break;
             case SET_ADDRESS:
                    set_address();
                    break;
             case GET_DESCRIPTOR:
                    get_descriptor();
                    break;
             case GET_CONFIGURATION:
                    get_configuration();
                    break;
             case SET_CONFIGURATION:                    
                    set_configuration();
                    break;
             case GET_INTERFACE:
                    get_interface();
                    break;
             case SET_INTERFACE:
                    set_interface();
                    break;
            default:
                    Error = 1;
                    break;    
          }     
        }   
    else if (Usb_request.bmRequestType & 0x20)        //类请求
        {
            if((Usb_request.bmRequestType & 0x03) == 0x01) //接口
            {
                if ((BYTE)(Usb_request.wIndex) == 0)         //HID 类请求 接口0 用作自定义HID设备，控制寄存器读写
                {
                    hidclasscommand_process();
                }
                else  if ((BYTE)(Usb_request.wIndex) == 1)  // 接口1 Audio Audio Control
                {
                   audioclassctrol_process();       
                }
                else  if ((BYTE)(Usb_request.wIndex) == 4)   // 接口4 massstorage class
                {
                   massstorage_process();       
                }
                else
                {
                  Error = 1;
                }
            }
            else if((Usb_request.bmRequestType & 0x03) == 0x02) //端点
            {
//                if ((BYTE)(Usb_request.wIndex)&0x07 == 3) // 端点3 Audio 
//                {
                   audioclassctrol_process();  
//                }
//                else
//                {
//                  Error = 1;
//                }
            }
            else
            {
               Error = 1;
            }
        }
        else
        {
            Error = 1;
        }

    reg_usb_Index = 0;
    if((ResetFlag == 0)&&(TestFlag == 0))
    {
        if(Error)
        {
            reg_EP0_SendStall = 1;
            Error = 0;
        }
        else if(NoData)
        {
            reg_EP0_DataEnd = 1;
            NoData = 0;
        }
        else
        {
            reg_EP0_InPktRdy = 1;
            if(!BytesLeft)
            {
                reg_EP0_DataEnd = 1;
            }
        }
        if((Usb_request.bmRequestType & 0x80) != 0x80)  // out mode 
        {
            reg_EP0_SevOutPktRdy = 1;
        }
    }
    TestFlag = 0;
    ResetFlag = 0;       
    reg_usb_Index = indextemp;
}

/************************GET_STATUS************************************/
void get_status(void)
{
     BYTE data  tempdata[8];

        if(DevState == DEVSTATE_CONFIG) 
        {
            if((Usb_request.bmRequestType == M_CMD_STDDEVIN) || (Usb_request.bmRequestType == M_CMD_STDIFIN))//dev&&if 设备或者接口 0x80 || 0x81
            {
                tempdata[0] = 0x00;
                tempdata[1] = 0x00;
                setupDataIn(2, tempdata);                   
            }
            else if(Usb_request.bmRequestType == M_CMD_STDEPIN)     //0x82  端点 
            {
                if((BYTE)(Usb_request.wIndex) == 0x00)//ep0
                {
                    tempdata[0] = 0x00;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else if((BYTE)(Usb_request.wIndex) == 0x81)//ep1 IN 
                {
                    reg_usb_Index = 0x01;
                    tempdata[0] = reg_InSendStall;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else if((BYTE)(Usb_request.wIndex) == 0x02)//ep2 OUT
                {
                    reg_usb_Index = 0x02;
                    tempdata[0] = (reg_usb_OutCSRL & BIT5)>>5;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else if((BYTE)(Usb_request.wIndex) == 0x82)//ep2 
                {
                    reg_usb_Index = 0x02;
                    tempdata[0] = reg_InSendStall;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else if((BYTE)(Usb_request.wIndex) == 0x03)//ep3
                {
                    reg_usb_Index = 0x03;
                    tempdata[0] = (reg_usb_OutCSRL & BIT5)>>5;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else if((BYTE)(Usb_request.wIndex) == 0x04)//ep4
                {
                    reg_usb_Index = 0x04;
                    tempdata[0] = (reg_usb_OutCSRL & BIT5)>>5;
                    tempdata[1] = 0x00;
                    setupDataIn(2, tempdata);                       
                }
                else
                {
                    Error = 1;
                }
                reg_usb_Index = 0x00;
            }
            else
            {
                Error =1;                  
            }
        }
        else if((DevState == DEVSTATE_ADDRESS)  \
                    &&(((Usb_request.bmRequestType == M_CMD_STDEPIN)&&(Usb_request.wIndex == 0x00))     \
                    ||(Usb_request.bmRequestType == M_CMD_STDDEVIN)))
        {
            tempdata[0] = 0x00;
            tempdata[1] = 0x00;
            setupDataIn(2, tempdata);   
        }
        else
        {
            Error = 1;
        }
}



/***********************SET_FEATURE************************************/
void set_feature(void)
{
        if((DevState==DEVSTATE_CONFIG)
            &&(Usb_request.bmRequestType==M_CMD_STDEPOUT)//just endpoint can do this
            &&(Usb_request.wValue == 0x0000))//just halt can do
        {
            if((BYTE)(Usb_request.wIndex) == 0x81)  //ep1in
            {
                reg_usb_Index = 0x01;
                reg_InSendStall = 1;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x02)// ep2 out
            {
                reg_usb_Index = 0x02;
                reg_usb_OutCSRL |= BIT5;  // reg_OutSendStall = 1;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x82)//  ep2 in 
            {
                reg_usb_Index = 0x02;
                reg_InSendStall = 1;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x03)// ep3 out
            {
                reg_usb_Index = 0x03;
                reg_usb_OutCSRL |= BIT5;  // reg_OutSendStall = 1;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x83)// ep3 in
            {
                reg_usb_Index = 0x03;
                reg_InSendStall = 1;
                NoData = 1;
            }

            else if((BYTE)(Usb_request.wIndex) == 0x04)// ep4 out
            {
                reg_usb_Index = 0x04;
                reg_usb_OutCSRL |= BIT5;  // reg_OutSendStall = 1;
                NoData = 1;
            }
            else
            {
                Error = 1;
            }
            reg_usb_Index = 0x00;
        }
//---------------------------test mode begin--------------------------------//
                else if((Usb_request.bmRequestType==0x00) //just DEV
                        && ((Usb_request.wValue == 0x0002))//test mode
                        && ((BYTE)(Usb_request.wIndex)==0x00)
                        && ((BYTE)(Usb_request.wIndex>>8)<=0x05)
                        && ((BYTE)(Usb_request.wIndex>>8)!=0x00)
                        && (TestFlag == 0)
                        )
                {
                    TestCMD = 1;
                    NoData = 1;
                }
                else if(TestFlag == 1)
                {
                    if((BYTE)(Usb_request.wIndex>>8) == 0x01)//test_j
                    {
                        reg_usb_Testmode |= 0x02;
                    }
                    else if((BYTE)(Usb_request.wIndex>>8) == 0x02)//test_k
                    {
                        reg_usb_Testmode |= 0x04;
                    }
                    else if((BYTE)(Usb_request.wIndex>>8) == 0x03)//test_se0_nak
                    {
                        reg_usb_Testmode |= 0x01;   
                    }
                    else if((BYTE)(Usb_request.wIndex>>8) == 0x04)//test_packet
                    {
                        setupDataIn(53, byTestPacket);
                        reg_EP0_InPktRdy = 1;
                        reg_usb_Testmode |= 0x08;   
                    }
                    else if((BYTE)(Usb_request.wIndex>>8) == 0x05)//test_force_enable
                    {
                        reg_usb_Testmode |= 0x10;
                    }
                }
//--------------------------------end---------------------------------------//
        else
        {
            Error = 1;
        }
}

/************************CLEAR_FEATURE***********************************/
void clear_feature(void)
{
        if((DevState == DEVSTATE_DEFAULT) 
            ||(Usb_request.bmRequestType != M_CMD_STDEPOUT)//just endpoint can do this
            ||(Usb_request.wValue != 0x0000))//just halt can do
        {
            Error = 1;
        }
        else if(DevState == DEVSTATE_CONFIG)
        {
            if((BYTE)(Usb_request.wIndex) == 0x00)//ep0
            {
                reg_EP0_SendStall = 0;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x01)//ep1out
            {
                reg_usb_Index = 0x01;
                reg_usb_OutCSRL &= (~BIT5); //  reg_OutSendStall = 0
                reg_usb_OutCSRL |= BIT7;    //reg_OutClrDataTog = 1
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x81)    //ep1in
            {
                reg_usb_Index = 0x01;
                reg_InSendStall = 0;
                reg_InClrDataTog = 1;
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x02)//ep2out
            {
                reg_usb_Index = 0x02;
                reg_usb_OutCSRL &= (~BIT5); //  reg_OutSendStall = 0
                reg_usb_OutCSRL |= BIT7;    //reg_OutClrDataTog = 1
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x82)    //ep2in
            {
                reg_usb_Index = 0x02;
                reg_InSendStall = 0;
                reg_InClrDataTog = 1;        
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x03)//ep3out
            {
                reg_usb_Index = 0x03;
                reg_usb_OutCSRL &= (~BIT5); //  reg_OutSendStall = 0
                reg_usb_OutCSRL |= BIT7;    //reg_OutClrDataTog = 1
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x83)//ep3 IN
            {
                reg_usb_Index = 0x03;
                reg_InSendStall = 0;
                reg_InClrDataTog = 1; 
                NoData = 1;
            }
            else if((BYTE)(Usb_request.wIndex) == 0x04)//ep4out
            {
                reg_usb_Index = 0x04;
                reg_usb_OutCSRL &= (~BIT5); //  reg_OutSendStall = 0
                reg_usb_OutCSRL |= BIT7;    //reg_OutClrDataTog = 1
                NoData = 1;
            }
            else
            {
                Error = 1;
            }
            reg_usb_Index = 0x00;
        }
        else if(DevState == DEVSTATE_ADDRESS)
        {
            if((BYTE)(Usb_request.wIndex) == 0x00)//ep0
            {
                reg_EP0_SendStall = 0;
                NoData = 1;
            }
            else
            {
                Error = 1;
            }
        }
        else
        {
            Error = 1;
        }
}
/***********************SET_ADDRESS************************************/
void set_address(void)
{
        //if((DevState <= DEVSTATE_ADDRESS) && (bmRequestType == M_CMD_STDDEVOUT))
        if((Usb_request.bmRequestType == M_CMD_STDDEVOUT) && (Usb_request.wValue < 0x80))
        {
            NewAddress = (BYTE)(Usb_request.wValue);
            NoData = 1;
        }
        else
        {
            Error = 1;
        }

}

// 增加了HID 描述符解析
/************************GET_DESCRIPTOR************************************/
void get_descriptor(void)
{
        if(Usb_request.bmRequestType == M_CMD_STDDEVIN)
        {
            // Init the dev descript
            //Initialise_Dev(&stddevdsc);

//          dscrLength = 0x79;//sizeof(Config_Descript[]);

            switch((BYTE)(Usb_request.wValue >> 8))     
            {
                case 0x01:// Descriptor Types--DEVICE
                    if(Usb_request.wLength <= 0x12)
                    {
                        //setupDataIn(Usb_request.wLength, Device_Descript);
                        setupDataIn(Usb_request.wLength, (UINT8 *)(&dev_desc));
                    }
                    else
                    {
                        //setupDataIn(0x12, Device_Descript);
                        setupDataIn(0x12, (UINT8 *)(&dev_desc));
                    }
                    break;                          
                case 0x02:// Descriptor Types--CONFIGURATION      配置
                    if((BYTE)Usb_request.wValue > 0x01)
                    {
                        Error = 1;
                    }
                    else
                    {
                        if(Usb_request.wLength <= 0x40)
                        {
                            setupDataIn(Usb_request.wLength, ConfigDescript);
                        }
                        else
                        {   
                            if (BytesLeft == 0)
                            {
                                setupDataIn(0x40, ConfigDescript);
                                if(Usb_request.wLength <= 0x40)
                                {
                                    BytesLeft = Usb_request.wLength - 0x40;
                                }
                                else
                                {
                                    BytesLeft = dscrLength - 0x40;
                                }
                            }
                            else
                            {
                                if(BytesLeft >= 0x40)
                                {                               
                                    setupDataIn(0x40, ConfigDescript);
                                    BytesLeft = BytesLeft - 0x40;
                                }
                                else
                                {
                                     setupDataIn(BytesLeft, ConfigDescript);
                                    BytesLeft = 0;
                                }
                            }
                        }

                    }
                    break;
                case 0x03:// Descriptor Types--STRING           字符串
                    if((BYTE)(Usb_request.wValue) == 0x00)
                    {
                        if(Usb_request.wLength <= LANGIDString[0])
                        {
                           setupDataIn(Usb_request.wLength, (UINT8 *)(&LANGIDString));
                        }
                        else
                        {
                            setupDataIn(LANGIDString[0], (UINT8 *)(&LANGIDString));
                        }
                    }
                    else if(Usb_request.wIndex == 0x0409)//LANGID==ENGLISH_US
                    {
                        if((BYTE)(Usb_request.wValue) == 0x01)//Manufacturer
                        {
                            if(Usb_request.wLength <= ManufacturerString[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&ManufacturerString));
                            }
                            else
                            {
                                setupDataIn(ManufacturerString[0], (UINT8 *)(&ManufacturerString));
                            }
                        }
                        else if((BYTE)(Usb_request.wValue) == 0x02)//Produce
                        {
                            if(Usb_request.wLength <= ProduceString[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&ProduceString));
                            }
                            else
                            {
                                setupDataIn(ProduceString[0], (UINT8 *)(&ProduceString));
                            }
                        }
                        else if((BYTE)(Usb_request.wValue) == 0x03)//Serialnum
                        {
                            if(Usb_request.wLength <= SerialnumString[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&SerialnumString));                      
                            }
                            else
                            {
                                setupDataIn(SerialnumString[0], (UINT8 *)(&SerialnumString));                       
                            }
                        }
                        else if((BYTE)(Usb_request.wValue) == 0x04)//Audio dev string
                        {
                            if(Usb_request.wLength <= ProduceString1[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&ProduceString1));                       
                            }
                            else
                            {
                                setupDataIn(ProduceString1[0], (UINT8 *)(&ProduceString1));                     
                            }
                        }
                        else if((BYTE)(Usb_request.wValue) == 0x05)//usb video
                        {
                            if(Usb_request.wLength <= uvideoString[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&uvideoString));                     
                            }
                            else
                            {
                                setupDataIn(uvideoString[0], (UINT8 *)(&uvideoString));                     
                            }                                               
                        }
                        else if((BYTE)(Usb_request.wValue) == 0x06)//udisk
                        {
                            if(Usb_request.wLength <= ProduceString2[0])
                            {
                                setupDataIn(Usb_request.wLength, (UINT8 *)(&ProduceString2));                       
                            }
                            else
                            {
                                setupDataIn(ProduceString2[0], (UINT8 *)(&ProduceString2));                     
                            }                                               
                        }
                        else
                        {
                            Error = 1;
                        }
                    }
                    else
                    {
                        Error = 1;
                    }
                    break;

                case 0x06://Descriptor Types--Qualifier
                    if(Usb_request.wLength  > 0x0a)
                    {
                      Usb_request.wLength = 0x0a;
                    }
                    if(reg_usb_Power & bmBIT4)
                            setupDataIn(Usb_request.wLength, (UINT8 *)(&HQualifierData));
                        else
                            setupDataIn(Usb_request.wLength, (UINT8 *)(&FQualifierData));
                    break;

                case 0x07:// Descriptor Types--CONFIGURATION      other speed
                    if(Usb_request.wLength <= 0x40)
                    {
                        setupDataIn(Usb_request.wLength, ConfigDescript_rom_otherspeed);
                    }
                    else
                    {   
                        if (BytesLeft == 0)
                        {
                            setupDataIn(0x40, ConfigDescript_rom_otherspeed);
                            if(Usb_request.wLength <= 0x40)
                            {
                                BytesLeft = Usb_request.wLength - 0x40;
                    }
                    else
                    {
                                BytesLeft = dscrLength - 0x40;
                            }
                        }
                        else
                        {
                            if(BytesLeft >= 0x40)
                            {                               
                                setupDataIn(0x40, ConfigDescript_rom_otherspeed);
                                BytesLeft = BytesLeft - 0x40;
                            }
                        else
                            {
                                 setupDataIn(BytesLeft, ConfigDescript_rom_otherspeed);
                                 BytesLeft = 0;
                            }
                        }
                    }
                    break;
                default:
                    Error = 1;
                    break;
            }           
        }
        else if(Usb_request.bmRequestType == M_CMD_STDIFIN)   //接口
        {
            switch((BYTE)(Usb_request.wValue >> 8))     
            {
            case 0x21:  //HID  DESCRIPTION
                 setupDataIn(9, &ConfigDescript[18]);      // 第18 Bytes 开始 为HID 描述符
                break;
            case 0x22: // hid report DESCRIPTION request
                setupDataIn(23, ReportDescriptor);
                break;
             }
        }
        else
        {
            Error = 1;
        }
}
/************************GET_CONFIGURATION************************************/
void get_configuration(void)
{
    BYTE tempdata[8];
        if(DevState == DEVSTATE_ADDRESS)
        {
            tempdata[0] = 0x00;
            setupDataIn(1, tempdata);           
        }
        else if(DevState == DEVSTATE_CONFIG)
        {
            tempdata[0] = 0x01;
            setupDataIn(1, tempdata);
        }
        else 
        {
            Error = 1;
        }
}

/************************SET_CONFIGURATION************************************/
void set_configuration(void)
{
        if((DevState == DEVSTATE_DEFAULT) || ((BYTE)Usb_request.wValue > 0x01))//just one config
        {
            Error = 1;
        }
        else if((BYTE)Usb_request.wValue == 0x00)//set cfg 0
        {               
            DevState = DEVSTATE_ADDRESS;
            NoData = 1;
        }
        else if((BYTE)Usb_request.wValue == 0x01)//set cfg 1
        {
            DevState = DEVSTATE_CONFIG;
            NoData = 1;
            endpoint_initial(); 
        }
        else
        {
           Error = 1;
        }
//      clear halt
//      reg_usb_Index = 0x01;
//      reg_InSendStall = 0;
//      reg_OutSendStall = 0;
//      reg_usb_Index = 0x02;
//      reg_InSendStall = 0;
//      reg_usb_Index = 0x00;
}
/************************GET_INTERFACE************************************/
void get_interface(void)
{
    BYTE tempdata[2];
    if((DevState == DEVSTATE_CONFIG) && ((BYTE)(Usb_request.wIndex) < 0x05))
    {
        if((BYTE)Usb_request.wIndex == 0x02)
        {
           tempdata[0] = audioflag;     // 有效接口 全部应答0
        }
        else 
        {
           if(g_u8_video_output_port==9)
           {
                tempdata[0] = reg_Rstctrl1_f008&0x02;     // 有效接口 全部应答0
           }
           else
           {
        tempdata[0] = 0x00;     // 有效接口 全部应答0
           }
        }
        setupDataIn(1, tempdata);
    }
    else
    {
        Error = 1;
    }
}

/************************SET_INTERFACE************************************/
void set_interface(void)
{
    if((BYTE)Usb_request.wValue == 0x01)     // 打开模块
    {
       if((BYTE)Usb_request.wIndex == 0x02)    //  interface2  audio stream
       {
            // todo audio open  
            audioflag =0x01;
       }
       if(g_u8_video_output_port==9)
       {
          if((BYTE)Usb_request.wIndex == 0x03)  //audio only mode interface 3
          {
            reg_usb_Index = 0x03;
            reg_usb_InCSRL |=0X09;
            reg_usb_InCSRL |=0X09;
            reg_usb_Index = 0x00;
            reg_Rstctrl1_f008 |= 0x02;
          }
       }
    }
    else
    {
       if((BYTE)Usb_request.wIndex == 0x02)    //  interface2  audio stream
       {
          // todo audio close 
          audioflag = 0x00;         
       }
       if(g_u8_video_output_port==9)
       {
          if((BYTE)Usb_request.wIndex == 0x03)  //audio only mode interface 3
          {
            reg_Rstctrl1_f008 &= ~0x02; 
          }
       }   
    }
    if((BYTE)Usb_request.wIndex == 0x04)  //udisk init
    {
        ep2init();      
    }

    NoData = 1;
}

void CMDRead()
{
    BYTE  ep0l;
    Usb_request.bmRequestType = reg_usb_End0;
    Usb_request.bRequest = reg_usb_End0;
    ep0l = reg_usb_End0;
    Usb_request.wValue = ((WORD)reg_usb_End0 << 8) + ep0l;
    ep0l = reg_usb_End0;
    Usb_request.wIndex = ((WORD)reg_usb_End0 << 8) + ep0l;
    ep0l = reg_usb_End0;
    Usb_request.wLength = ((WORD)reg_usb_End0 << 8) + ep0l;
    dscrLength =   Usb_request.wLength ;
    // 修改在每次取描述表的时候清零BytesLeft 
     if ((Usb_request.bmRequestType == 0x80) && (Usb_request.bRequest == 0x06))
            BytesLeft = 0x0000;
}
