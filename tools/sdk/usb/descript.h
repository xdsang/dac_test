#include "datatype.h"
//#include "usb_standard.h"
#ifndef _DESCRIPT_H_
#define _DESCRIPT_H_


//����Udisk
#define DESCRIPT_LENGTH                     0xb6
#define DESCRIPT_LENGTH_AUDIO               0xDA
//������UDISK
//#define DESCRIPT_LENGTH                       0x9f

extern xdata BYTE ConfigDescript[];
extern code BYTE ConfigDescript_rom[];
extern code BYTE ConfigDescript_rom_audio[];

extern code BYTE byTestPacket[];
extern code BYTE   ReportDescriptor[];
extern xdata BYTE dev_desc[] ;
extern code BYTE LANGIDString[];
extern xdata BYTE ManufacturerString[];
extern code BYTE ManufacturerString_rom[];
extern xdata BYTE  ProduceString[]; 
extern code  BYTE  ProduceString_rom[]; 
extern xdata BYTE  ProduceString1[] ; 
extern xdata BYTE  ProduceString2[];
extern xdata BYTE SerialnumString[] ;
extern xdata BYTE uvideoString[];
//extern xdata USB_DESCRIPTOR_HQualifierData HQualifierData ; 
//extern xdata USB_DESCRIPTOR_FQualifierData FQualifierData ; 
extern code BYTE HQualifierData[] ; 
extern code BYTE FQualifierData[] ; 
extern code BYTE  ConfigDescript_rom_otherspeed[];



 


/* Request Type Field */
#define M_CMD_TYPEMASK  0x60
#define M_CMD_STDREQ    0x00
#define M_CMD_CLASSREQ  0x20
#define M_CMD_VENDREQ   0x40
#define M_CMD_STDDEVIN  0x80
#define M_CMD_STDDEVOUT 0x00
#define M_CMD_STDIFIN   0x81
#define M_CMD_STDIFOUT  0x01
#define M_CMD_STDEPIN   0x82
#define M_CMD_STDEPOUT  0x02

/* Standard Request Codes */
#define GET_STATUS          0x00
#define CLEAR_FEATURE       0x01
#define SET_FEATURE         0x03
#define SET_ADDRESS         0x05
#define GET_DESCRIPTOR      0x06
#define SET_DESCRIPTOR      0x07
#define GET_CONFIGURATION   0x08
#define SET_CONFIGURATION   0x09
#define GET_INTERFACE       0x0A
#define SET_INTERFACE       0x0B
#define SYNCH_FRAME         0x0C

/* Define device states */   
#define DEVSTATE_DEFAULT        0
#define DEVSTATE_ADDRESS        1
#define DEVSTATE_CONFIG         2   
#define DEVSTATE_DOWNLOAD_START 3   
#define DEVSTATE_DOWNLOAD_END   4
#define DEVSTATE_VIDEO_DATA     5
#define DEVSTATE_AUDIO_DATA     6
#define DEVSTATE_CMD_UP         7   
#define DEVSTATE_CMD_DOWN       8


/*Class-Specific Request Codes*/
#define SET_CUR          0x01
#define GET_CUR          0x81 
#define SET_MIN          0x02          
#define GET_MIN          0x82
#define SET_MAX          0x03
#define GET_MAX          0x83
#define SET_RES          0x04
#define GET_RES          0x84
#define SET_MEM          0x05
#define GET_LEN          0x85      // video
#define GET_MEM          0x85      // audio
#define GET_INFO         0x86
#define GET_DEF          0x87
#define GET_STAT         0xFF


/*VideoStreaming Interface Control Selectors*/
#define VS_CONTROL_UNDEFINED                        0x00
#define VS_PROBE_CONTROL                            0x01
#define VS_COMMIT_CONTROL                           0x02
#define VS_STILL_PROBE_CONTROL                      0x03
#define VS_STILL_COMMIT_CONTROL                     0x04
#define VS_STILL_IMAGE_TRIGGER_CONTROL              0x05
#define VS_STREAM_ERROR_CODE_CONTROL                0x06
#define VS_GENERATE_KEY_FRAME_CONTROL               0x07
#define VS_UPDATE_FRAME_SEGMENT_CONTROL             0x08
#define VS_SYNCH_DELAY_CONTROL                      0x09



/*VideoControl Interface Control Selectors*/
#define VC_CONTROL_UNDEFINED                   0x00
#define VC_VIDEO_POWER_MODE_CONTROL              0x01
#define VC_REQUEST_ERROR_CODE_CONTROL              0x02

/*Selector Unit Control Selectors*/
#define SU_CONTROL_UNDEFINED                 0x00
#define SU_INPUT_SELECT_CONTROL               0x01

/*Camera Temrminal Control Selectors*/
#define CT_CONTROL_UNDEFINED                 0x00
#define CT_SCANNING_MODE_CONTROL               0x01
#define CT_AE_MODE_CONTROL                      0x02
#define CT_AE_PRIORITY_CONTROL                       0x03
#define CT_EXPOSURE_TIME_ABSOLUTE_CONTROL             0x04
#define CT_EXPOSURE_TIME_RELATIVE_CONTROL             0x05
#define CT_FOCUS_ABSOLUTE_CONTROL                    0x06
#define CT_FOCUS_RELATIVE_CONTROL                    0x07
#define CT_FOCUS_AUTO_CONTROL                        0x08 
#define CT_IRIS_ABSOLUTE_CONTROL                     0x09
#define CT_IRIS_RELATIVE_CONTROL                     0x0A
#define CT_ZOOM_ABSOLUTE_CONTROL                     0x0B
#define CT_ZOOM_RELATIVE_CONTROL                     0x0C
#define CT_PANTILT_ABSOLUTE_CONTROL                  0x0D
#define CT_PANTILT_RELATIVE_CONTROL                  0x0E
#define CT_ROLL_ABSOLUTE_CONTROL                     0x0F
#define CT_ROLL_RELATIVE_CONTROL                     0x10
#define CT_PRIVACY_CONTROL                           0x11

/*Processing Unit Control Selectors*/
#define PU_CONTROL_UNDEFINED                        0x00
#define PU_BACKLIGHT_COMPENSATION_CONTROL              0x01
#define PU_BRIGHTNESS_CONTROL                          0x02
#define PU_CONTRAST_CONTROL                            0x03
#define PU_GAIN_CONTROL                                0x04
#define PU_POWER_LINE_FREQUENCY_CONTROL                0x05
#define PU_HUE_CONTROL                                 0x06
#define PU_SATURATION_CONTROL                          0x07
#define PU_SHARPNESS_CONTROL                           0x08
#define PU_GAMMA_CONTROL                               0x09
#define PU_WHITE_BALANCE_TEMPERATURE_CONTROL                     0x0A
#define PU_WHITE_BALANCE_TEMPERATURE_AUTO_CONTROL                0x0B
#define PU_WHITE_BALANCE_COMPONENT_CONTROL                       0x0C
#define PU_WHITE_BALANCE_COMPONENT_AUTO_CONTROL                  0x0D
#define PU_DIGITAL_MULTIPLIER_CONTROL                            0x0E
#define PU_DIGITAL_MULTIPLIER_LIMIT_CONTROL                      0x0F
#define PU_HUE_AUTO_CONTROL                                      0x10
#define PU_ANALOG_VIDEO_STANDARD_CONTROL                         0x11
#define PU_ANALOG_LOCK_STATUS_CONTROL                            0x12




#endif /* _DESCRIPT_H_ */

