#include "common.h"

#ifdef MS2160_I2C_SLAVE
static UINT8 s_u8chipAddr = MS2160_I2C_SLAVE_ADDR;

VOID HAL_SetChipAddr(UINT8 u8_address)
{
    s_u8chipAddr = u8_address;
}
#endif

UINT8 HAL_ReadByte(UINT16 u16_index)
{
#ifdef MS2160_I2C_SLAVE
        return mculib_i2c_read_16bidx8bval(s_u8chipAddr, u16_index);
#else
        return (*(UINT8 __IO __XDATA*)u16_index);
#endif
}

VOID HAL_WriteByte(UINT16 u16_index, UINT8 u8_value)
{
#ifdef MS2160_I2C_SLAVE
        mculib_i2c_write_16bidx8bval(s_u8chipAddr, u16_index, u8_value);
#else
        (*(UINT8 __IO __XDATA*)u16_index) = u8_value;    
#endif
}

VOID HAL_ClrBits(UINT16 u16_index, UINT8 u8_mask)
{
    HAL_ModBits(u16_index, u8_mask, 0x00);  
}

VOID HAL_SetBits(UINT16 u16_index, UINT8 u8_mask)
{
    HAL_ModBits(u16_index, u8_mask, u8_mask); 
}

VOID HAL_ToggleBits(UINT16 u16_index, UINT8 u8_mask, BOOL b_set)
{
    (b_set) ? HAL_SetBits(u16_index, u8_mask) : HAL_ClrBits(u16_index, u8_mask);
}

VOID HAL_ModBits(UINT16 u16_index, UINT8 u8_mask, UINT8 u8_value)
{
    UINT8 tmp;
    tmp  = HAL_ReadByte(u16_index);
    tmp &= ~u8_mask;
    tmp |= (u8_value & u8_mask);
    HAL_WriteByte(u16_index, tmp); 
}

VOID HAL_WriteWord(UINT16 u16_index, UINT16 u16_value)
{
    HAL_WriteByte(u16_index, (UINT8)u16_value);
    HAL_WriteByte(u16_index + 1, (UINT8)(u16_value >> 8));
}

UINT16 HAL_ReadWord(UINT16 u16_index)
{
    return (HAL_ReadByte(u16_index) + ((UINT16)HAL_ReadByte(u16_index + 1) << 8));
}

VOID HAL_WriteBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value)
{
    UINT16 i;

    for (i = 0; i < u16_length; i++)
    {
        HAL_WriteByte(u16_index + i, *(p_u8_value + i));
    }
}

VOID HAL_ReadBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value)
{
    UINT16 i;

    for (i = 0; i < u16_length; i++)
    {
        *(p_u8_value + i) = HAL_ReadByte(u16_index + i);
    }
}

UINT32 HAL_ReadDWord(UINT16 u16_index)
{
    return (HAL_ReadWord(u16_index) + ((UINT32)HAL_ReadWord(u16_index + 2) << 16));
}

VOID HAL_WriteDWord(UINT16 u16_index, UINT32 u32_value)
{
    HAL_WriteWord(u16_index, (UINT16)u32_value);
    HAL_WriteWord(u16_index + 2, (UINT16)(u32_value >> 16));
}

UINT32 HAL_ReadRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length)
{
    UINT32 u32_data = 0;    
    UINT32 u32_mask = 0xffffffff >> (32 - u8_length);
    
    u32_mask <<= u8_bitpos;
    
    u32_data = HAL_ReadDWord(u16_index);
    u32_data &= u32_mask;
    u32_data >>= u8_bitpos;    
    
    return u32_data;
}

VOID HAL_WriteRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length, UINT32 u32_value)
{
    UINT32 u32_data = HAL_ReadDWord(u16_index);    
    UINT32 u32_val = u32_value << u8_bitpos;
    UINT32 u32_mask = 0xffffffff >> (32 - u8_length);
    
    u32_mask <<= u8_bitpos;
    
    u32_data &= ~u32_mask;  
    u32_val  &= u32_mask; 
    u32_data |= u32_val;
    
    HAL_WriteDWord(u16_index, u32_data);    
}
