#ifndef _COMMON_H
#define _COMMON_H

#include "ms2160_config.h"

#if defined (_PLATFORM_WINDOWS_)
#include <stdio.h>  
#include <math.h>
#include <string.h>
#include <windows.h>
#elif defined (__KEIL_C__)
#include <intrins.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <absacc.h>
#endif

#include "datatype.h"


/********************************************************************************************************************/
/****************************************begin of external function declaration************************************************/
/*******************************************************************************************************************/
// external function need to be implemented by user
#if MS2160_DEBUGGING_LOG_ENABLE
extern VOID mculib_uart_log(UINT8 *u8_string); //log string
extern VOID mculib_uart_log1(UINT8 *u8_string, UINT16 u16_hex); //log Y in hexadecimal format
extern VOID mculib_uart_log2(UINT8 *u8_string, UINT16 u16_dec); //log Y in decimal format
#endif

/***************************************************************
*  Function name:   mculib_i2c_read_16bidx8bval
*  Description:     read back 8 bits register value with 16 bits specified index
*  Entry:           [in]u8_address: 8 bits I2C slave address
*                   [in]u16_index:  16 bits register index
*
*  Returned value: UINT8 type register value
*  Remark:
***************************************************************/
extern UINT8 mculib_i2c_read_16bidx8bval(UINT8 u8_address, UINT16 u16_index);

/***************************************************************
*  Function name:   mculib_i2c_write_16bidx8bval
*  Description:     write 8 bits register value to 16 bits specified index
*  Entry:           [in]u8_address: 8 bits I2C slave address
*                   [in]u16_index:  16 bits register index
*                   [in]u8_value:   8 bits register value
*
*  Returned value: if write success return TRUE, else return FALSE
*  Remark:
***************************************************************/
extern BOOL mculib_i2c_write_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT8 u8_value);

extern VOID mculib_i2c_burstread_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);

extern VOID mculib_i2c_burstwrite_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);

//8bits I2C slave address for HDMI DDC
extern VOID mculib_i2c_set_speed(UINT8 u8_i2c_speed);
extern VOID mculib_i2c_set_ddc(BOOL ddc);
extern UINT8 mculib_i2c_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index);
extern BOOL mculib_i2c_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value);
extern VOID mculib_i2c_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);
extern VOID mculib_i2c_burstwrite_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);

extern VOID mculib_delay_us(UINT16 u16_count_us);
extern VOID mculib_delay_ms(UINT16 u16_count_ms);
/********************************************************************************************************************/
/*****************************************end of external function declaration************************************************/
/*******************************************************************************************************************/




/********************************************************************************************************************/
/*****************************************begin of common interface declaration**********************************************/
/*******************************************************************************************************************/
#ifdef MS2160_I2C_SLAVE
/***************************************************************
*  Function name: HAL_SetChipAddr
*  Description:   change I2C slave u8_address 
*  Input parameters: UINT8 u8_address: chip slave u8_address
*                    
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID HAL_SetChipAddr(UINT8 u8_address);
#endif

VOID HAL_WriteByte(UINT16 u16_index, UINT8 u8_value);
VOID HAL_ClrBits(UINT16 u16_index, UINT8 u8_mask);
VOID HAL_ModBits(UINT16 u16_index, UINT8 u8_mask, UINT8 u8_value);
VOID HAL_SetBits(UINT16 u16_index, UINT8 u8_mask);
UINT8 HAL_ReadByte(UINT16 u16_index);
VOID HAL_ToggleBits(UINT16 u16_index, UINT8 u8_mask, BOOL b_set);
VOID HAL_WriteWord(UINT16 u16_index, UINT16 u16_value);
UINT16 HAL_ReadWord(UINT16 u16_index);
VOID HAL_WriteBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value);
VOID HAL_ReadBytes(UINT16 u16_index, UINT16 u16_length, UINT8 *p_u8_value);


/***************************************************************
*  Function name:  HAL_ReadDWord
*  Description: read 32 bits value 
*  Input parameters: UINT16 u16_index: 16 bits register index
*  Output parameters: None
*  Returned value: 32 bits value
***************************************************************/
UINT32 HAL_ReadDWord(UINT16 u16_index);


/***************************************************************
*  Function name:  HAL_WriteDWord
*  Description: write 32 bits value 
*  Input parameters: UINT16 u16_index: 16 bits register index
*                    UINT32 u32_value: 32 bits value
*  Output parameters: None
*  Returned value: 32 bits value
***************************************************************/
VOID HAL_WriteDWord(UINT16 u16_index, UINT32 u32_value);


/***************************************************************
*  Function name: UINT32 HAL_ReadRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length)
*  Description:   read back 32 bits register value by specified index with start bit and length
*                 in case of 16 bits index and 8 bits value
*  Input parameters: UINT16 u16_index: 16 bits register index
*                    UINT8 u8_bitpos: start bit of fisrt index register, should be less than 8
*                    UINT8 u8_length: register length , should less than 33
*  Output parameters: None
*  Returned value: UINT32 type register value
***************************************************************/
UINT32 HAL_ReadRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length);


/***************************************************************
*  Function name: VOID HAL_WriteRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length, UINT32 u32_value)
*  Description:   write 32 bits register value to specified index with start bit and length
*                 in case of 16 bits index and 8 bits value
*  Input parameters: UINT16 u16_index: 16 bits register index
*                    UINT8 u8_bitpos: start bit of fisrt index register, should be less than 8
*                    UINT8 u8_length: register length , should less than 33
*                    UINT32 u32_value: 32 bits register value
*  Output parameters: None
*  Returned value: None
***************************************************************/
VOID HAL_WriteRange(UINT16 u16_index, UINT8 u8_bitpos, UINT8 u8_length, UINT32 u32_value);


#define Delay_ms mculib_delay_ms
#define Delay_us mculib_delay_us


#if MS2160_DEBUGGING_LOG_ENABLE
#define MS2160_LOG(X)       mculib_uart_log((UINT8*)X)
#define MS2160_LOG1(X, Y)   mculib_uart_log1((UINT8*)X, Y)
#define MS2160_LOG2(X, Y)   mculib_uart_log2((UINT8*)X, Y)
#else
#define MS2160_LOG(X)
#define MS2160_LOG1(X, Y)
#define MS2160_LOG2(X, Y)
#endif
/********************************************************************************************************************/
/*****************************************end of common interface declaration***********************************************/
/*******************************************************************************************************************/

#endif
