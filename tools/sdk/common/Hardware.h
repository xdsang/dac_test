#ifndef __HARDWARE_H__
#define __HARDWARE_H__
/*************************************************************************
  8051 Mcu Register
**************************************************************************/


//SPI 
sfr SPCR = 0xC7;
sfr SPSR = 0xC8;
    sbit SPIF      = SPSR^7;
    sbit WCOL      = SPSR^6;
    sbit WFFILL    = SPSR^3;
    sbit WFEMPTY   = SPSR^2;
    sbit RFFULL    = SPSR^1;
    sbit RFEMPTY   = SPSR^0;
sfr SPDR = 0xC9;
sfr SPER = 0xCA;



//Timer1 
sfr T1LL    = 0xBC;
sfr T1L     = 0xBD;
sfr T1H     = 0xBE;
sfr T1HH    = 0xBF;
sfr TCON    = 0x88;
sfr TPS     = 0x89;


//sfr TEST_REG = 0xFF;
sfr SP      = 0x81;
sfr DPL     = 0x82;
sfr DPH     = 0x83;
sfr PCON    = 0x87;
sfr IEN0    = 0xA8;
sfr IP0     = 0xA9;//0xB8;
sfr PSW     = 0xD0;
sfr ACC     = 0xE0;
sfr B       = 0xF0;
sfr WDCON   = 0xD8;

/* Extensions  */
sfr DPL1    = 0x84;
sfr DPH1    = 0x85;
sfr DPS     = 0x92;//0x86;
sfr CKCON   = 0x8E;
//sfr IEN2  = 0x8F;
sfr IEN2    = 0x9A;
sfr IRCON   = 0xC0; 
sfr IEN1    = 0xB8; 
sfr IP1     = 0xB9; 


// Uart0
sfr S0CON  = 0x98;
sbit SM0      = S0CON^7;
sbit SM1      = S0CON^6;
sbit SM20     = S0CON^5;
sbit REN0     = S0CON^4;
sbit TB80     = S0CON^3;
sbit RB80     = S0CON^2;
sbit TI0      = S0CON^1;
sbit RI0      = S0CON^0;

sfr S0BUF  = 0x99;
sfr S0RELL  = 0xAA;
sfr S0RELH  = 0xBA;

/*  BIT Registers  */
/*  PSW */
sbit CY     = PSW^7;
sbit AC     = PSW^6;
sbit F0     = PSW^5;
sbit RS1    = PSW^4;
sbit RS0    = PSW^3;
sbit OV     = PSW^2;
sbit FL     = PSW^1;
sbit P      = PSW^0;

/*  IEN0  */
sbit EA     = IEN0^7;
sbit ES1    = IEN0^6;
sbit ET2    = IEN0^5;
sbit ES0    = IEN0^4;
sbit ET1    = IEN0^3;
sbit EX1    = IEN0^2;
sbit ET0    = IEN0^1;
sbit EX0    = IEN0^0;

/*  TCON  */
sbit TF1    = TCON^7;
sbit TR1    = TCON^6;
sbit TF0    = TCON^5;
sbit TR0    = TCON^4;
sbit IE1    = TCON^3;
sbit IT1    = TCON^2;
sbit IE0    = TCON^1;
sbit IT0    = TCON^0;

/*  IRCON  */
sbit IEX6   = IRCON^5;
sbit IEX5   = IRCON^4;
sbit IEX4   = IRCON^3;
sbit IEX3   = IRCON^2;
sbit IEX2   = IRCON^1;

/*  IEN1  */
sbit EX6    = IEN1^5;
sbit EX5    = IEN1^4;
sbit EX4    = IEN1^3;
sbit EX3    = IEN1^2;
sbit EX2    = IEN1^1;



/*************************************************************************
  SIE Register
**************************************************************************/
sfr reg_usb_Index              = 0x93;   
sfr reg_usb_Faddr              = 0x94;  
sfr reg_usb_Power              = 0x95;
sfr reg_usb_IntrIn             = 0x96;
sfr reg_usb_IntrOut            = 0x97;
//sfr reg_usb_IntrInE            = 0x9A;
sfr reg_usb_IntrInE            = 0xEC;//2018/1/18  change

sfr reg_usb_IntrOutE           = 0x9B;
sfr reg_usb_IntrUSB            = 0x9C;
sfr reg_usb_IntrUSBE           = 0x9D;
sfr reg_usb_FrameL             = 0x9E;
sfr reg_usb_FrameH             = 0x9F;
sfr reg_usb_Testmode           = 0xA1;
sfr reg_usb_InMaxPL            = 0xA2;
sfr reg_usb_InMaxPH            = 0xA3;
sfr reg_usb_OutMaxPL           = 0xA4;    
sfr reg_usb_OutMaxPH           = 0xA5;
sfr reg_usb_CountL             = 0xA6;        
sfr reg_usb_CountH             = 0xA7;    
sfr reg_usb_End0               = 0xAB;    
sfr reg_usb_End1               = 0xAC; 
sfr reg_usb_End2               = 0xAD; 
sfr reg_usb_End3               = 0xAE;  
sfr reg_usb_End4               = 0xAF;  


/*****************************
the usb_CSR0 regs only can be used
when usb_Index = 0
*****************************/

sfr reg_usb_CSR0               = 0x80;
    sbit reg_EP0_OutPktRdy     = 0x80+0;
    sbit reg_EP0_InPktRdy      = 0x80+1;
    sbit reg_EP0_SentStall     = 0x80+2;
    sbit reg_EP0_DataEnd       = 0x80+3;
    sbit reg_EP0_SetupEnd      = 0x80+4;
    sbit reg_EP0_SendStall     = 0x80+5;
    sbit reg_EP0_SevOutPktRdy  = 0x80+6;
    sbit reg_EP0_SevSetupEnd   = 0x80+7;
//////////////////////////////  

sfr reg_usb_InCSRL             = 0x80;
    sbit reg_IntPktRdy         = 0x80+0;
    sbit reg_FifoNotEmpty      = 0x80+1;
    sbit reg_UnderRun          = 0x80+2;
    sbit reg_InFlushFifo       = 0x80+3;
    sbit reg_InSendStall       = 0x80+4;
    sbit reg_InSentStall       = 0x80+5;
    sbit reg_InClrDataTog      = 0x80+6;
    sbit reg_IncompTx          = 0x80+7;
    
sfr reg_usb_InCSRH             = 0x90;
    sbit reg_InFrcDataTog      = 0x90+3;
    sbit reg_InDmaEnab         = 0x90+4;
    sbit reg_InMode            = 0x90+5;
    sbit reg_InIso             = 0x90+6;
    sbit reg_AutoSet           = 0x90+7;
    
sfr reg_usb_OutCSRL            = 0x8B;                                     
sfr reg_usb_OutCSRH            = 0x8C;  


/*************************************************************************
 Global Register
**************************************************************************/
sfr reg_usb_BusRest                = 0xe9;          //default 0x00 1:busreset staus  0:other
sfr reg_mcu_access_usb_mode            = 0xEA;         //default 0x00  1:MCU����USB 0��Video audio дUSB
sfr reg_soc_test_mode            = 0xEB;
//gpio   
sfr reg_gpio_data                  = 0xA0;          //default 0x00
  sbit reg_gpio7_data               = 0xA0+7;   
  sbit reg_gpio6_data               = 0xA0+6;   
  sbit reg_gpio5_data               = 0xA0+5;   
  sbit reg_gpio4_data               = 0xA0+4;
  sbit reg_gpio3_data               = 0xA0+3;   
  sbit reg_gpio2_data               = 0xA0+2;   
  sbit reg_gpio1_data               = 0xA0+1;   
  sbit reg_gpio0_data               = 0xA0+0;   
  
sfr reg_gpio_oen                   = 0xB0;          //default 0xff    0:input 1:output  ��MS2100�߼��෴
  sbit reg_gpio7_oen                = 0xB0+7;   
  sbit reg_gpio6_oen                = 0xB0+6;   
  sbit reg_gpio5_oen                = 0xB0+5;   
  sbit reg_gpio4_oen                = 0xB0+4;
  sbit reg_gpio3_oen                = 0xB0+3;   
  sbit reg_gpio2_oen                = 0xB0+2;   
  sbit reg_gpio1_oen                = 0xB0+1;   
  sbit reg_gpio0_oen                = 0xB0+0;   
   
sfr reg_gpio_pu                   = 0xB1; 
sfr reg_gpio_pd                   = 0xB2; 

#endif
