#ifndef __MACROSILICON_MS2160_CONFIG_H__
#define __MACROSILICON_MS2160_CONFIG_H__

#include "ms2160_app_config.h"

//compiler platform check
#if !defined (_PLATFORM_WINDOWS_) && !defined (__KEIL_C__)
#error "Unknown compiler!"
#endif


//MS2160 i2c slave address config
#ifdef  MS2160_I2C_SLAVE
#define MS2160_I2C_SLAVE_ADDR       (0x16)
#endif


#ifndef MS2160_PIXCLK_TYPE
#define MS2160_PIXCLK_TYPE       (1) //1:from hdmi txpll; 0:pllv moudle
#endif

#ifndef MS2160_FPGA_VERIFY
#define MS2160_FPGA_VERIFY  (1)
#endif

#ifndef MS2160_DEBUGGING_LOG_ENABLE
#define MS2160_DEBUGGING_LOG_ENABLE (0)
#endif

#endif
