#ifndef __DATATYPE_H__
#define __DATATYPE_H__

/*
** Global typedefs.
*/

#ifndef NULL
#define NULL ((void*)0)
#endif


#if defined (_PLATFORM_WINDOWS_)
#define  __CODE
#define  __XDATA
#define  __DATA
#define __IDATA
#define  __NEAR
#define  __IO
#elif defined (__KEIL_C__)
#define __CODE code
#define __XDATA xdata
#define __DATA data
#define __IDATA idata
#define __NEAR
#define __IO volatile


//bool bype
typedef bit BOOL;
#endif


//unsigned integer type
typedef unsigned char     u8;
typedef unsigned char     uint8;
typedef unsigned char     UINT8;
typedef char              CHAR;
typedef unsigned short    u16 ;
typedef unsigned short    UINT16;
typedef unsigned char     BYTE;
typedef unsigned short    WORD;


//signed integer type
typedef signed char       INT8;
typedef signed short      INT16;


//32bit type
#if defined (_PLATFORM_WINDOWS_)
typedef unsigned int UINT32;
typedef signed int INT32;
#else
typedef unsigned long u32;
typedef unsigned long uint32;
typedef unsigned long int UINT32;
typedef signed long int INT32;
#endif


#define VOID void

#define FALSE   0
#define TRUE    1

//#define false   0
//#define true    1

#define DISABLE 0
#define ENABLE  1

#define LOW     0
#define HIGH    1

#define OFF     0
#define ON      1


// Helper macros.
#define _UNUSED_(arg)     ((arg) = (arg))

#ifndef _countof
#define _countof(ARRAY) (sizeof(ARRAY) / sizeof(ARRAY[0]))
#endif

#ifndef max
#define max(a, b)   (((a)>(b))?(a):(b)) 
#endif

#ifndef min
#define min(a, b)   (((a)<(b))?(a):(b))
#endif

//
#define SWAP(x)   ((((x)&0xFF)<<8)|(((x)>>8)&0xFF))
#define MSB(x)    (UINT8)(((x)>>8)&0xff)



// 优先使用这种定义
#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

//兼容其他Code
#define SET_BIT0 0x01
#define SET_BIT1 0x02
#define SET_BIT2 0x04
#define SET_BIT3 0x08
#define SET_BIT4 0x10
#define SET_BIT5 0x20
#define SET_BIT6 0x40
#define SET_BIT7 0x80

#define bmBIT0  0x01
#define bmBIT1  0x02
#define bmBIT2  0x04
#define bmBIT3  0x08
#define bmBIT4  0x10
#define bmBIT5  0x20
#define bmBIT6  0x40
#define bmBIT7  0x80


/*
* generic mask macro definitions
*/
#define MSRT_BIT0                   (0x01)
#define MSRT_BIT1                   (0x02)
#define MSRT_BIT2                   (0x04)
#define MSRT_BIT3                   (0x08)
#define MSRT_BIT4                   (0x10)
#define MSRT_BIT5                   (0x20)
#define MSRT_BIT6                   (0x40)
#define MSRT_BIT7                   (0x80)
    
#define MSRT_MSB8BITS               MSRT_BIT7
#define MSRT_LSB                    MSRT_BIT0
    
// Bit7 ~ Bit0
#define MSRT_BITS7_6                (0xC0)
#define MSRT_BITS7_5                (0xE0)
#define MSRT_BITS7_4                (0xF0)
#define MSRT_BITS7_3                (0xF8)
#define MSRT_BITS7_1                (0xFE)
#define MSRT_BITS7_0                (0xff)

#define MSRT_BITS6_5                (0x60)
#define MSRT_BITS6_4                (0x70)
#define MSRT_BITS6_2                (0x7c)
#define MSRT_BITS6_1                (0x7e)
#define MSRT_BITS6_0                (0x7f)

#define MSRT_BITS5_4                (0x30)
#define MSRT_BITS5_3                (0x38)
#define MSRT_BITS5_2                (0x3c)
#define MSRT_BITS5_0                (0x3f)

#define MSRT_BITS4_3                (0x18)
#define MSRT_BITS4_2                (0x1c)
#define MSRT_BITS4_1                (0x1e)
#define MSRT_BITS4_0                (0x1f)

#define MSRT_BITS3_2                (0x0C)
#define MSRT_BITS3_1                (0x0E)
#define MSRT_BITS3_0                (0x0F)

#define MSRT_BITS2_1                (0x06)
#define MSRT_BITS2_0                (0x07)

#define MSRT_BITS1_0                (0x03)

//
#define USE1BIT 0x01
#define USE2BIT 0x03
#define USE3BIT 0x07
#define USE4BIT 0x0f
#define USE5BIT 0x1f
#define USE6BIT 0x3f
#define USE7BIT 0x7f
#define USE8BIT 0xff

typedef enum
{
    SDRAM_SIZE_2M = 0,
    SDRAM_SIZE_4M,
    SDRAM_SIZE_8M,
    SDRAM_SIZE_16M,
    SDRAM_SIZE_0M
}SDRAM_SIZE_E;

typedef struct
{
    UINT8           u8_polarity;
    UINT16          u16_htotal;
    UINT16          u16_vtotal;
    UINT16          u16_hactive;
    UINT16          u16_vactive;
    UINT16          u16_pixclk;     /*10000hz*/
    UINT16          u16_vfreq;      /*0.01hz*/
    UINT16          u16_hoffset;    /* h sync start to h active*/
    UINT16          u16_voffset;    /* v sync start to v active*/
    UINT16          u16_hsyncwidth;
    UINT16          u16_vsyncwidth;
} VIDEOTIMING_T;

typedef struct
{
    UINT16 u16_h;
    UINT16 u16_v;
} VIDEOSIZE_T;

typedef enum
{
    COLOR_SPACE_RGB565 = 0,
    COLOR_SPACE_RGB888,
    COLOR_SPACE_YUV422,
    COLOR_SPACE_YUV444,
    COLOR_SPACE_OTHER
}VIDEO_COLOR_SPACE_E;

typedef enum
{
    VIDEO_OUTPUT_PORT_CVBS = 0,
    VIDEO_OUTPUT_PORT_SVIDEO,   
    VIDEO_OUTPUT_PORT_VGA,
    VIDEO_OUTPUT_PORT_YPBPR,
    VIDEO_OUTPUT_PORT_CVBS_SVIDEO,
    VIDEO_OUTPUT_PORT_HDMI, 
    VIDEO_OUTPUT_PORT_DIGITAL
}VIDEO_OUTPUT_PORT_E;

typedef struct
{
    UINT8 u8_in_color_space;
    UINT8 u8_in_mem_color_space;
    UINT8 u8_in_data_byte_sel;
    VIDEOSIZE_T st_in_video_size;
}VIDEO_IN_INFO_T;

typedef struct
{
    UINT8 u8_out_mode_index;
    UINT8 u8_out_color_space;
    VIDEOSIZE_T st_video_out_size;
    VIDEOSIZE_T st_video_mem_size;
}VIDEO_OUT_INFO_T;

typedef enum
{
    TRANSFER_MODE_FRAME = 0,
    TRANSFER_MODE_FIXED_BLOCK_NUM,
    TRANSFER_MODE_FIXED_BLOCK_SIZE,
    TRANSFER_MODE_MANUAL_BLOCK,
    TRANSFER_MODE_MEM_BYPS_FRAME,
    TRANSFER_MODE_MEM_BYPS_MANUAL_BLOCK
}VIDEO_TRANSFER_MODE_E;

typedef struct
{
    UINT8 u8_column;
    UINT8 u8_row;
}VIDEO_BLOCK_NUM_T;

typedef union
{
    VIDEO_BLOCK_NUM_T st_blk_num;
    VIDEOSIZE_T st_blk_size;
}VIDEO_BLOCK_DEF_MODE_U;

typedef struct
{
    UINT16 u16_blk_st_h;
    UINT16 u16_blk_st_v;
    VIDEO_BLOCK_DEF_MODE_U u_blk_def_mode;
}VIDEO_BLOCK_INFO_T;

typedef struct
{
    UINT8 u8_trans_mode; //refer to VIDEO_TRANSFER_MODE_E
    VIDEO_BLOCK_INFO_T st_blk_info;
}VIDEO_TRANSFER_MODE_T;

#endif    
