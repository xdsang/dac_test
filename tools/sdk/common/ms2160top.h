#ifndef __MS2160TOP_H__
#define __MS2160TOP_H__
/*************************************************************************
    Global Register
**************************************************************************/
#define GLOBAL_BASE                 0xF000
//chip id read only 
#define CHIPIDL_F000  (GLOBAL_BASE+0x00)
#define CHIPIDM_F001  (GLOBAL_BASE+0x01)
#define CHIPIDH_F002  (GLOBAL_BASE+0x02)
#define reg_Chipidl_f000   (*(volatile unsigned char __XDATA*)CHIPIDL_F000)   //0xa7
#define reg_Chipidm_f001   (*(volatile unsigned char __XDATA*)CHIPIDM_F001)   //0x16
#define reg_Chipidh_f002   (*(volatile unsigned char __XDATA*)CHIPIDH_F002)   //0x0a

//clk&reset
#define CLKCTRL0_F003  (GLOBAL_BASE+0x03)
#define CLKCTRL1_F004  (GLOBAL_BASE+0x04)
#define CLKCTRL2_F005  (GLOBAL_BASE+0x05)
#define RSTCTRL0_F007  (GLOBAL_BASE+0x07)
#define RSTCTRL1_F008  (GLOBAL_BASE+0x08)
#define RSTCTRL3_F00a  (GLOBAL_BASE+0x0a)
#define reg_Clkctrl0_f003   (*(volatile unsigned char __XDATA*)CLKCTRL0_F003) 
#define reg_Clkctrl1_f004   (*(volatile unsigned char __XDATA*)CLKCTRL1_F004) 
#define reg_Clkctrl2_f005   (*(volatile unsigned char __XDATA*)CLKCTRL2_F005) 
#define reg_Rstctrl0_f007   (*(volatile unsigned char __XDATA*)RSTCTRL0_F007) 
#define reg_Rstctrl1_f008   (*(volatile unsigned char __XDATA*)RSTCTRL1_F008) 
#define reg_Rstctrl3_f00a   (*(volatile unsigned char __XDATA*)RSTCTRL3_F00a) 

//pad ctrl
#define MPADCTRL0_F00C   (GLOBAL_BASE+0x0c)
#define MPADCTRL1_F00D   (GLOBAL_BASE+0x0d)
#define MPADCTRL2_F00E   (GLOBAL_BASE+0x0e)
#define MPADCTRL3_F00F   (GLOBAL_BASE+0x0f)
#define MPADCTRL4_F010   (GLOBAL_BASE+0x10)
#define PADCTRL0_F011    (GLOBAL_BASE+0x11)
#define PADCTRL1_F012    (GLOBAL_BASE+0x12)
#define PADCTRL2_F013    (GLOBAL_BASE+0x13)
#define PADCTRL3_F014    (GLOBAL_BASE+0x14)
#define PADCTRL4_F015    (GLOBAL_BASE+0x15)
#define PADCTRL5_F016    (GLOBAL_BASE+0x16)
#define PADCTRL6_F017    (GLOBAL_BASE+0x17)
#define PADCTRL7_F018    (GLOBAL_BASE+0x18)
#define PADCTRL8_F019    (GLOBAL_BASE+0x19)
#define PADCTRL9_F01A    (GLOBAL_BASE+0x1a)
#define PADCTRL10_F01B   (GLOBAL_BASE+0x1b)
#define DVOPADCTRL0_F01C (GLOBAL_BASE+0x1c)
#define DVOPADCTRL1_F01D (GLOBAL_BASE+0x1d)
#define DVOPADCTRL2_F01E (GLOBAL_BASE+0x1e)
#define DVOPADCTRL3_F01F (GLOBAL_BASE+0x1f)
#define SPIMCTRL3_F021   (GLOBAL_BASE+0x21)
#define reg_Mpadctrl0_f00c   (*(volatile unsigned char __XDATA*)MPADCTRL0_F00C)
#define reg_Mpadctrl1_f00d   (*(volatile unsigned char __XDATA*)MPADCTRL1_F00D)
#define reg_Mpadctrl2_f00e   (*(volatile unsigned char __XDATA*)MPADCTRL2_F00E)
#define reg_Mpadctrl3_f00f   (*(volatile unsigned char __XDATA*)MPADCTRL3_F00F)
#define reg_Mpadctrl4_f010   (*(volatile unsigned char __XDATA*)MPADCTRL4_F010)
#define reg_Padctrl0_f011    (*(volatile unsigned char __XDATA*)PADCTRL0_F011)
#define reg_Padctrl1_f012    (*(volatile unsigned char __XDATA*)PADCTRL1_F012)
#define reg_Padctrl2_f013    (*(volatile unsigned char __XDATA*)PADCTRL2_F013)
#define reg_Padctrl3_f014    (*(volatile unsigned char __XDATA*)PADCTRL3_F014)
#define reg_Padctrl4_f015    (*(volatile unsigned char __XDATA*)PADCTRL4_F015)
#define reg_Padctrl5_f016    (*(volatile unsigned char __XDATA*)PADCTRL5_F016)
#define reg_Padctrl6_f017    (*(volatile unsigned char __XDATA*)PADCTRL6_F017)
#define reg_Padctrl7_f018    (*(volatile unsigned char __XDATA*)PADCTRL7_F018)
#define reg_Padctrl8_f019    (*(volatile unsigned char __XDATA*)PADCTRL8_F019)
#define reg_Padctrl9_f01a    (*(volatile unsigned char __XDATA*)PADCTRL9_F01A)
#define reg_Padctrl10_f01b   (*(volatile unsigned char __XDATA*)PADCTRL10_F01B)
#define reg_Dvopadctr0_f01c  (*(volatile unsigned char __XDATA*)DVOPADCTRL0_F01C)
#define reg_Dvopadctr1_f01d  (*(volatile unsigned char __XDATA*)DVOPADCTRL1_F01D)
#define reg_Dvopadctr2_f01e  (*(volatile unsigned char __XDATA*)DVOPADCTRL2_F01E)
#define reg_Dvopadctr3_f01f  (*(volatile unsigned char __XDATA*)DVOPADCTRL3_F01F)
#define reg_Spinmctr_f021    (*(volatile unsigned char __XDATA*)SPIMCTRL3_F021)

//Timer 0 
#define TIMEREN_F022   (GLOBAL_BASE+0x22)
#define TIMERCNTL_F023 (GLOBAL_BASE+0x23)
#define TIMERCNTH_F024 (GLOBAL_BASE+0x24)
#define TIMERINT_F025  (GLOBAL_BASE+0x25)
#define reg_TimerEn_f022     (*(volatile unsigned char __XDATA*)TIMEREN_F022) //1b0  0timer_cnt清为26h0000000；1：timer_cnt累加
#define reg_TimerCntL_f023   (*(volatile unsigned char __XDATA*)TIMERCNTL_F023)   //8hff  软件初始化CNT寄存器，当高16bit = {TIMER_CNTH,TIMER_CNTH}产生int 1
#define reg_TimerCntH_f024   (*(volatile unsigned char __XDATA*)TIMERCNTH_F024)   //
#define reg_Timer_int_f025   (*(volatile unsigned char __XDATA*)TIMERINT_F025)    //中断标志位，需软件清0

//Video Top Ctrl 
#define CAVCTRL           (GLOBAL_BASE+0x30)
#define DACMUXCTRL0       (GLOBAL_BASE+0x31)
#define DACMUXCTRL1       (GLOBAL_BASE+0x32)
#define DACMUXCTRL2       (GLOBAL_BASE+0x33)
#define MISCSEL           (GLOBAL_BASE+0x40)
#define TBUS_TOP          (GLOBAL_BASE+0x42)
#define BGREF_EN_CNTRL    (GLOBAL_BASE+0x50)
#define LDO_EN_CNTRL      (GLOBAL_BASE+0x54)
#define VDS_RGB2YUV_CFG   (GLOBAL_BASE+0x58)
#define HDMI_SRC_CNTRL    (GLOBAL_BASE+0x59)
#define reg_Cavctrl       (*(volatile unsigned char __XDATA*)CAVCTRL)  
#define reg_Dacmuxctrl0   (*(volatile unsigned char __XDATA*)DACMUXCTRL0)   
#define reg_Dacmuxctrl1   (*(volatile unsigned char __XDATA*)DACMUXCTRL1)  
#define reg_Dacmuxctrl2   (*(volatile unsigned char __XDATA*)DACMUXCTRL2)
#define reg_Miscsel       (*(volatile unsigned char __XDATA*)MISCSEL)   
#define reg_Tbus_Top      (*(volatile unsigned char __XDATA*)TBUS_TOP) 
#define reg_Bgref_Cntrl   (*(volatile unsigned char __XDATA*)BGREF_EN_CNTRL)  
#define reg_Ldo_Cntrl     (*(volatile unsigned char __XDATA*)LDO_EN_CNTRL) 
#define reg_Vds_Csc_Cfg   (*(volatile unsigned char __XDATA*)VDS_RGB2YUV_CFG)  
#define reg_Hdmi_Cntrl    (*(volatile unsigned char __XDATA*)HDMI_SRC_CNTRL) 

//Special
#define reg_f202   (*(volatile unsigned char __XDATA*)0xf202)  
#define reg_f203   (*(volatile unsigned char __XDATA*)0xf203) 



#endif