/**
******************************************************************************
* @file    ms2160_timing_table.c
* @author  
* @version V1.0.0
* @date    
* @brief   input/output timing define source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_timing_table.h"

MISCTIMING_T __CODE g_arrTimingTable[] =
{
    //===============================================================================================================================================
    //        VIC                                             Total           Active             Freq          Offset        Sync
    //                                      Polarity        H       V        H      V      pixclk     V       H      V      H     V
    //===============================================================================================================================================
    {VFMT_CEA_02_720x480P_60HZ,           ProgrVNegHNeg,    858,    525,    720,    480,    2700,    6000,   122,    36,    62,   6}, // 2 - 720  x 480p@60                   
    //{VFMT_CEA_04_1280x720P_60HZ,          ProgrVPosHPos,   1650,    750,   1280,    720,    7425,    6000,   260,    25,    40,   5}, // 4 - 1280 x 720p@60Hz            
    //{VFMT_CEA_05_1920x1080I_60HZ,         InterVPosHPos,   2200,   1125,   1920,   1080,    7425,    6000,   192,    20,    44,   5}, // 5 - 1920 x 1080i@60             
    //{VFMT_CEA_06_720x480I_60HZ,           InterVNegHNeg,   1716,    525,   1440,    480,    2700,    6000,   238,    18,   124,   3}, // 6 - 1440 x 480i@60              
    //{VFMT_CEA_16_1920x1080P_60HZ,         ProgrVPosHPos,   2200,   1125,   1920,   1080,   14835,    6000,   192,    41,    44,   5}, // 16 - 1920 x 1080p@60            
    {VFMT_CEA_17_720x576P_50HZ,           ProgrVNegHNeg,    864,    625,    720,    576,    2700,    5000,   132,    44,    64,   5}, // 17 - 720  x 576p@50             
    {VFMT_CEA_19_1280x720P_50HZ,          ProgrVPosHPos,   1980,    750,   1280,    720,    7425,    5000,   260,    25,    40,   5}, // 19 - 1280 x 720p@50Hz           
    //{VFMT_CEA_20_1920x1080I_50HZ,         InterVPosHPos,   2640,   1125,   1920,   1080,    7425,    5000,   192,    20,    44,   5}, // 20 - 1920 x 1080i@50Hz          
    //{VFMT_CEA_21_720x576I_50HZ,           InterVNegHNeg,   1728,    625,   1440,    576,    2700,    5000,   264,    22,   126,   3}, // 21 - 1440 x 576i@50             
    {VFMT_CEA_31_1920x1080P_50HZ,         ProgrVPosHPos,   2640,   1125,   1920,   1080,   14850,    5000,   192,    41,    44,   5}, // 31 - 1920 x 1080p@50            
    //{VFMT_CEA_32_1920x1080P_24HZ,         ProgrVPosHPos,   2750,   1125,   1920,   1080,    7417,    2400,   192,    41,    44,   5}, // 32 - 1920 x 1080p@24            
    //{VFMT_CEA_33_1920x1080P_25HZ,         ProgrVPosHPos,   2640,   1125,   1920,   1080,    7425,    2500,   192,    41,    44,   5}, // 33 - 1920 x 1080p@25            
    {VFMT_CEA_34_1920x1080P_30HZ,         ProgrVPosHPos,   2200,   1125,   1920,   1080,    7417,    3000,   192,    41,    44,   5}, // 34 - 1920 x 1080p@30            
    //                                                                                  
    {VFMT_VESA_64_640X480_60,             ProgrVNegHNeg,    800,    525,    640,    480,    2517,    5994,   144,    35,    96,   2}, // 64 - 640x480@60 DMT2008 Draft3                         
    //{VFMT_VESA_65_640X480_75,             ProgrVNegHNeg,    840,    500,    640,    480,    3150,    7500,   184,    19,    64,   3}, // 65 - 640x480@75 DMT2008 Draft3                         
    {VFMT_VESA_66_800X600_60,             ProgrVPosHPos,   1056,    628,    800,    600,    4000,    6032,   216,    27,   128,   4}, // 66 - 800x600@60 DMT2008 Draft3                         
    //{VFMT_VESA_67_800X600_72,             ProgrVPosHPos,   1040,    666,    800,    600,    5000,    7219,   184,    29,   120,   6}, // 67 - 800x600@72 DMT2008 Draft3                         
    {VFMT_VESA_68_800X600_75,             ProgrVPosHPos,   1056,    625,    800,    600,    4950,    7500,   240,    24,    80,   3}, // 68 - 800x600@75 DMT2008 Draft3                         
    //{VFMT_VESA_69_800X600_85,             ProgrVPosHPos,   1048,    631,    800,    600,    5625,    8506,   216,    30,    64,   3}, // 69 - 800x600@85 DMT2008 Draft3                         
    //{VFMT_VESA_70_800X600_120_RB,         ProgrVNegHPos,    960,    636,    800,    600,    7325,   11972,   112,    33,    32,   4}, // 70 - 800x600@120 Reduce Blanking DMT2008 Draft3        
    {VFMT_VESA_71_1024X768_60,            ProgrVNegHNeg,   1344,    806,   1024,    768,    6500,    6000,   296,    35,   136,   6}, // 71 - 1024x768@60 DMT2008 Draft3                        
    //{VFMT_VESA_72_1024X768_70,            ProgrVNegHNeg,   1328,    806,   1024,    768,    7500,    7007,   280,    35,   136,   6}, // 72 - 1024x768@70 DMT2008 Draft3                        
    {VFMT_VESA_73_1024X768_75,            ProgrVPosHPos,   1312,    800,   1024,    768,    7875,    7503,   272,    31,    96,   3}, // 73 - 1024x768@75 DMT2008 Draft3                        
    //{VFMT_VESA_74_1024X768_85,            ProgrVPosHPos,   1376,    808,   1024,    768,    9450,    8500,   304,    39,    96,   3}, // 74 - 1024x768@85 DMT2008 Draft3                        
    //{VFMT_VESA_75_1024X768_120_RB,        ProgrVNegHPos,   1184,    813,   1024,    768,   11550,   11999,   112,    42,    32,   4}, // 75 - 1024x768@120 Reduce Blanking DMT2008 Draft3       
    {VFMT_VESA_76_1152X864_60,            ProgrVPosHNeg,   1520,    895,   1152,    864,    8162,    6000,   304,    30,   120,   3}, // 76 - 1152x864@60 DMT2008 Draft3                        
    //{VFMT_VESA_77_1152X864_75,            ProgrVPosHPos,   1600,    900,   1152,    864,   10800,    7500,   384,    35,   128,   3}, // 77 - 1152x864@75 DMT2008 Draft3                        
    {VFMT_VESA_78_1280X600_60,            ProgrVPosHNeg,   1648,    622,   1280,    600,    6150,    6000,   312,    21,   128,   3}, // 78 - 1280x600@60 DMT2008 Draft3                        
    {VFMT_VESA_79_1280X720_60_DMT,        ProgrVPosHPos,   1650,    750,   1280,    720,    7425,    6000,   260,    25,    40,   5}, // 79 - 1280x720@60 DMT2008 Draft3                        
    //{VFMT_VESA_80_1280X720_60_CVT,        ProgrVPosHNeg,   1664,    746,   1280,    720,    7437,    5991,   328,    25,   136,   3}, // 80 - 1280x720@60 CVT2002 Draft6                        
    //{VFMT_VESA_81_1280X720_60_CVT_RB,     ProgrVNegHPos,   1408,    741,   1280,    720,    6250,    5990,    96,    20,    32,   3}, // 81 - 1280x720@60 Reduce Blanking CVT2002 Draft6        
    //{VFMT_VESA_82_1280X720_75_CVT,        ProgrVPosHNeg,   1696,    752,   1280,    720,    9562,    7498,   260,    25,    40,   5}, // 82 - 1280x720@75 CVT2002 Draft6                        
    //{VFMT_VESA_83_1280X720_85_CVT,        ProgrVPosHNeg,   1712,    756,   1280,    720,   11000,    8499,   352,    35,   136,   3}, // 83 - 1280x720@85 CVT2002 Draft6                        
    {VFMT_VESA_84_1280X768_60,            ProgrVPosHNeg,   1664,    798,   1280,    768,    7950,    5987,   320,    27,   128,   7}, // 84 - 1280x768@60 DMT2008 Draft3                        
    //{VFMT_VESA_85_1280X768_60_RB,         ProgrVNegHPos,   1440,    790,   1280,    768,    6825,    6000,   112,    19,    32,   7}, // 85 - 1280x768@59.95 Reduce Blanking                    
    {VFMT_VESA_86_1280X768_75,            ProgrVPosHNeg,   1696,    805,   1280,    768,   10225,    7489,   336,    34,   128,   7}, // 86 - 1280x768@75 DMT2008 Draft3                        
    {VFMT_VESA_87_1280X800_60,            ProgrVPosHNeg,   1680,    831,   1280,    800,    8350,    5981,   328,    28,   128,   6}, // 87 - 1280x800@60 DMT2008 Draft3                        
    //{VFMT_VESA_88_1280X800_75,            ProgrVPosHNeg,   1696,    838,   1280,    800,   10650,    7493,   336,    35,   128,   6}, // 88 - 1280x800@75 DMT2008 Draft3                        
    //{VFMT_VESA_89_1280X800_85,            ProgrVPosHNeg,   1712,    843,   1280,    800,   12250,    8488,   352,    40,   136,   6}, // 89 - 1280x800@85 DMT2008 Draft3                        
    //{VFMT_VESA_90_1280X800_120_RB,        ProgrVNegHPos,   1440,    847,   1280,    800,   14625,   11991,   112,    44,    32,   6}, // 90 - 1280x800@120 Reduce Blanking DMT2008 Draft3       
    {VFMT_VESA_91_1280X960_60_DMT,        ProgrVPosHPos,   1800,   1000,   1280,    960,   10800,    6000,   424,    39,   112,   3}, // 91 - 1280x960@60 DMT2008 Draft3                        
    //{VFMT_VESA_92_1280X960_60_CVT,        ProgrVPosHNeg,   1712,    994,   1280,    960,   10200,    5994,   352,    33,   136,   3}, // 92 - 1280x960@60 CVT2002 Draft6                        
    //{VFMT_VESA_93_1280X960_75_CVT,        ProgrVPosHNeg,   1728,   1002,   1280,    960,   12987,    7501,   360,    41,   136,   3}, // 93 - 1280x960@75 CVT2002 Draft6                        
    //{VFMT_VESA_94_1280X960_85,            ProgrVPosHPos,   1728,   1011,   1280,    960,   14850,    8500,   384,    50,   160,   3}, // 94 - 1280x960@85 DMT2008 Draft3                        
    //{VFMT_VESA_95_1280X960_120_RB,        ProgrVNegHPos,   1440,   1017,   1280,    960,   17550,   11984,   112,    54,    32,   4}, // 95 - 1280x960@120 Reduce Blanking DMT2008 Draft3       
    {VFMT_VESA_96_1280X1024_60,           ProgrVPosHPos,   1688,   1066,   1280,   1024,   10800,    6002,   360,    41,   112,   3}, // 96 - 1280x1024@60 DMT2008 Draft3                       
    {VFMT_VESA_97_1280X1024_75,           ProgrVPosHPos,   1688,   1066,   1280,   1024,   13500,    7502,   392,    41,   144,   3}, // 97 - 1280x1024@75 DMT2008 Draft3                       
    //{VFMT_VESA_98_1280X1024_85,           ProgrVPosHPos,   1728,   1072,   1280,   1024,   15750,    8502,   384,    47,   160,   3}, // 98 - 1280x1024@85 DMT2008 Draft3                       
    //{VFMT_VESA_99_1280X1024_120_RB,       ProgrVNegHPos,   1440,   1084,   1280,   1024,   18725,   11996,   112,    57,    32,   7}, // 99 - 1280x1024@120 Reduce Blanking  DMT2008 Draft3     
    {VFMT_VESA_100_1360X768_60,           ProgrVPosHPos,   1792,    795,   1360,    768,    8550,    6002,   368,    24,   112,   6}, // 100 - 1360x768@60 DMT2008 Draft3                       
    //{VFMT_VESA_101_1360X768_120_RB,       ProgrVNegHPos,   1520,    813,   1360,    768,   14825,   11997,   112,    42,    32,   5}, // 101 - 1360x768@120 Reduce Blanking DMT2008 Draft3      
    {VFMT_VESA_102_1366X768_60,           ProgrVPosHPos,   1792,    798,   1366,    768,    8550,    5979,   356,    27,   143,   3}, // 102 - 1366x768@60 DMT2008 Draft3                       
    {VFMT_VESA_103_1400X1050_60,          ProgrVPosHNeg,   1864,   1089,   1400,   1050,   12175,    5998,   376,    36,   144,   4}, // 103 - 1400x1050@60 DMT2008 Draft3                      
    //{VFMT_VESA_104_1400X1050_75,          ProgrVPosHNeg,   1896,   1099,   1400,   1050,   15600,    7487,   392,    46,   144,   4}, // 104 - 1400x1050@75 DMT2008 Draft3                      
    //{VFMT_VESA_105_1400X1050_85,          ProgrVPosHNeg,   1912,   1105,   1400,   1050,   17950,    8496,   408,    52,   152,   4}, // 105 - 1400x1050@85 DMT2008 Draft3                      
    //{VFMT_VESA_106_1400X1050_120_RB,      ProgrVNegHPos,   1560,   1112,   1400,   1050,   20800,   11990,   112,    59,    32,   4}, // 106 - 1400x1050@120 Reduce Blanking DMT2008 Draft3     
    {VFMT_VESA_107_1440X900_60_DMT,       ProgrVPosHNeg,   1904,    934,   1440,    900,   10650,    5989,   384,    31,   152,   6}, // 107 - 1440x900@60 DMT2008 Draft3                       
    //{VFMT_VESA_108_1440X900_75,           ProgrVPosHNeg,   1936,    942,   1440,    900,   13675,    7498,   400,    39,   152,   6}, // 108 - 1440x900@75 DMT2008 Draft3                       
    //{VFMT_VESA_109_1440X900_85,           ProgrVPosHNeg,   1952,    948,   1440,    900,   15700,    8484,   408,    45,   152,   6}, // 109 - 1440x900@85 DMT2008 Draft3                       
    //{VFMT_VESA_110_1440X900_120_RB,       ProgrVNegHPos,   1600,    953,   1440,    900,   18275,   11985,   112,    50,    32,   6}, // 110 - 1440x900@120 Reduce Blanking DMT2008 Draft3      
    //{VFMT_VESA_111_1600X900_60_CVT,       ProgrVPosHNeg,   2128,    932,   1600,    900,   11888,    5994,   432,    31,   168,   3}, // 111 - 1600x900@60 CVT2002 Draft6                       
    //{VFMT_VESA_112_1600X900_60_DMT_RB,    ProgrVPosHPos,   1800,   1000,   1600,    900,   10800,    6000,   176,    99,    80,   3}, // 112 - 1600x900@60 Reduce Blanking DMT2008 Draft3       
    //{VFMT_VESA_113_1600X900_75_CVT,       ProgrVPosHNeg,   2160,    940,   1600,    900,   15212,    7492,   456,    39,   176,   3}, // 113 - 1600x900@75 CVT2002 Draft6                       
    //{VFMT_VESA_114_1600X900_85_CVT,       ProgrVPosHNeg,   2176,    945,   1600,    900,   17475,    8498,   464,    44,   176,   3}, // 114 - 1600x900@85 CVT2002 Draft6                       
    {VFMT_VESA_115_1600X1200_60,          ProgrVPosHPos,   2160,   1250,   1600,   1200,   16200,    6000,   496,    49,   192,   3}, // 115 - 1600x1200@60 DMT2008 Draft3                      
    //{VFMT_VESA_116_1600X1200_70,          ProgrVPosHPos,   2160,   1250,   1600,   1200,   18900,    7000,   496,    49,   192,   3}, // 116 - 1600x1200@70 DMT2008 Draft3                      
    //{VFMT_VESA_117_1600X1200_75,          ProgrVPosHPos,   2160,   1250,   1600,   1200,   20250,    7500,   496,    49,   192,   3}, // 117 - 1600x1200@75 DMT2008 Draft3                      
    //{VFMT_VESA_118_1600X1200_85,          ProgrVPosHPos,   2160,   1250,   1600,   1200,   22950,    8500,   496,    49,   192,   3}, // 118 - 1600x1200@85 DMT2008 Draft3                      
    //{VFMT_VESA_119_1600X1200_120_RB,      ProgrVNegHPos,   1760,   1271,   1600,   1200,   26825,   11992,   112,    68,    32,   4}, // 119 - 1600x1200@120 Reduce Blanking  DMT2008 Draft3    
    {VFMT_VESA_120_1680X1050_60,          ProgrVPosHNeg,   2240,   1089,   1680,   1050,   14625,    5995,   456,    36,   176,   6}, // 120 - 1680x1050@60 DMT2008 Draft3                      
    //{VFMT_VESA_121_1680X1050_60_RB,       ProgrVNegHPos,   1840,   1080,   1680,   1050,   11900,    5988,   112,    27,    32,   6}, // 121 - 1680x1050@60 Reduce Blanking DMT2008 Draft3      
    //{VFMT_VESA_122_1680X1050_75,          ProgrVPosHNeg,   2272,   1099,   1680,   1050,   18700,    7489,   472,    46,   176,   6}, // 122 - 1680x1050@75 DMT2008 Draft3                      
    //{VFMT_VESA_123_1680X1050_85,          ProgrVPosHNeg,   2288,   1105,   1680,   1050,   21475,    8491,   480,    52,   176,   6}, // 123 - 1680x1050@85 DMT2008 Draft3                      
    //{VFMT_VESA_124_1680X1050_120_RB,      ProgrVNegHPos,   1840,   1112,   1680,   1050,   24550,   11999,   112,    59,    32,   6}, // 124 - 1680x1050@120 Reduce Blanking DMT2008 Draft3     
    //{VFMT_VESA_125_1792X1344_60,          ProgrVPosHNeg,   2448,   1394,   1792,   1344,   20475,    6000,   528,    49,   200,   3}, // 125 - 1792x1344@60 DMT2008 Draft3                      
    //{VFMT_VESA_126_1792X1344_75,          ProgrVPosHNeg,   2456,   1417,   1792,   1344,   26100,    7500,   568,    72,   216,   3}, // 126 - 1792x1344@75 DMT2008 Draft3                      
    //{VFMT_VESA_127_1856X1392_60,          ProgrVPosHNeg,   2528,   1439,   1856,   1392,   21825,    5999,   576,    46,   224,   3}, // 127 - 1856x1392@60 DMT2008 Draft3                      
    //{VFMT_VESA_128_1856X1392_75,          ProgrVPosHNeg,   2560,   1500,   1856,   1392,   28800,    7500,   576,   107,   224,   3}, // 128 - 1856x1392@75 DMT2008 Draft3                      
    {VFMT_VESA_129_1920X1080_60_DMT,      ProgrVPosHPos,   2200,   1125,   1920,   1080,   14850,    6000,   192,    41,    44,   5}, // 129 - 1920x1080p@60 DMT2008 Draft3                     
    //{VFMT_VESA_130_1920X1080_60_CVT,      ProgrVPosHNeg,   2576,   1118,   1920,   1080,   17275,    5998,   536,    37,   208,   3}, // 130 - 1920x1080p@60 CVT2002 Draft6                     
    //{VFMT_VESA_131_1920X1080_60_CVT_RB,   ProgrVNegHPos,   2048,   1111,   1920,   1080,   13650,    5999,    96,    30,    32,   3}, // 131 - 1920x1080p@60 Reduce Blanking CVT2002 Draft6     
    //{VFMT_VESA_132_1920X1200_60_CVT,      ProgrVPosHNeg,   2592,   1242,   1920,   1200,   19312,    5999,   544,    41,   208,   3}, // 132 - 1920x1200@60 CVT2002 Draft6                      
    //{VFMT_VESA_133_1920X1200_60_DMT_RB,   ProgrVNegHPos,   2080,   1235,   1920,   1200,   15400,    5995,   112,    32,    32,   6}, // 133 - 1920x1200@60 DMT2008 Draft3 Reduce Blanking      
    //{VFMT_VESA_134_1920X1200_75,          ProgrVPosHNeg,   2608,   1255,   1920,   1200,   24525,    7493,   552,    52,   208,   6}, // 134 - 1920x1200@75 DMT2008 Draft3                      
    //{VFMT_VESA_135_1920X1200_85,          ProgrVPosHNeg,   2624,   1262,   1920,   1200,   28125,    8493,   560,    59,   208,   6}, // 135 - 1920x1200@85 DMT2008 Draft3                      
    //{VFMT_VESA_136_1920X1440_60,          ProgrVPosHNeg,   2600,   1500,   1920,   1440,   23400,    6000,   552,    59,   208,   3}, // 136 - 1920x1440@60 DMT2008 Draft3                      
    //{VFMT_VESA_137_1920X1440_75,          ProgrVPosHNeg,   2640,   1500,   1920,   1440,   29700,    7500,   576,    59,   224,   3}, // 137 - 1920x1440@75 DMT2008 Draft3                      
    //{VFMT_VESA_138_1920X1440_85_CVT,      ProgrVPosHNeg,   2640,   1514,   1920,   1440,   33900,    8494,   568,    71,   208,   4}, // 138 - 1920x1440@85 CVT2002 Draft1                      
    //{VFMT_VESA_139_2048X1536_60_CVT,      ProgrVPosHNeg,   2800,   1589,   2048,   1536,   26700,    6001,   600,    52,   224,   3}, // 139 - 2048x1536@60 CVT2002 Draft6                      
    //{VFMT_VESA_140_2048X1536_75_CVT,      ProgrVPosHNeg,   2832,   1603,   2048,   1536,   34050,    7500,   616,    66,   224,   3}, // 140 - 2048x1536@75 CVT2002 Draft6                      
    //{VFMT_VESA_141_2048X1536_85_CVT,      ProgrVPosHNeg,   2832,   1612,   2048,   1536,   38812,    8502,   616,    75,   224,   3}, // 141 - 2048x1536@85 CVT2002 Draft6                      
    //{VFMT_VESA_142_2560X1600_60,          ProgrVPosHNeg,   3504,   1658,   2560,   1600,   34850,    5999,   752,    55,   280,   6}, // 142 - 2560x1600@60 DMT2008 Draft3                        
};

/***************************************************************
*  Function name:   ms2160_get_std_timing
*  Description:     get CEA/VESA video timing information
*  Entry:           [in]u8_vic, enum to ms2160_VIDEOFORMAT_E
*                   [out]ptTiming, point to VIDEOTIMING_T
* 
*  Returned value:  if input u8_vic valid return true,else return false
*  Remark:
***************************************************************/
BOOL ms2160_get_std_timing(UINT8 u8_vic, VIDEOTIMING_T * ptTiming)
{
    UINT8 u8_index = 0;

    for (u8_index = 0; u8_index < _countof(g_arrTimingTable); u8_index ++)
    {
        if (u8_vic == g_arrTimingTable[u8_index].u8_vic)
        {
            * ptTiming = g_arrTimingTable[u8_index].st_timing;
            return TRUE;
        }
    }
    
    return FALSE;
}

#define MDT_HTOTAL_MARGIN           (10)    // uint: 1 pixel, by FOSC sample
#define MDT_VTOTAL_MARGIN           (3)     // uint: 1 line
#define MDT_HSYNC_MARGIN            (2)     // uint: 1 pixel, by FOSC sample
#define MDT_VSYNC_MARGIN            (1)     // uint: 1 line
#define MDT_VFREQ_MARGIN            (500)   // uint: 0.01Hz
#define MDT_STEP5_MARGIN            (100)   // define step5 vtotal margin

static INT16 _abs(INT16 i)
{      /* compute absolute value of int argument */
    return (i < 0 ? -i : i);
}

/***************************************************************
*  Function name:   ms2160_match_std_timing
*  Description:     ms2160 match input timing from CEA/VESA
*  Entry:           [in]ptTiming
*                        u8_polarity, enum to MDTSYNCPOLARITY_E
*                        u16_htotal, sample by fosc clk 27M
*                        u16_vtotal, uint line
*                        u16_hsyncwidth, sample by fosc clk 27M
*                        u16_vsyncwidth, uint 1 line
*                        u16_pixclk, uint 10000Hz, mdt sample clk,const is fosc clk 27M 
*                        u16_vfreq, uint 0.01Hz
*                        others para, reserve
*
*                   [out]ptTiming
*                        u8_polarity, same to in para
*                        u16_htotal, sample by std pixel clk
*                        u16_vtotal, uint line, same to in para
*                        u16_hsyncwidth, sample by std pixel clk
*                        u16_vsyncwidth, uint 1 line, same to in para
*                        u16_pixclk, uint 10000Hz, std pixel clk
*                        u16_vfreq, uint 0.01Hz, same to in para
*                        u16_hactive, sample by std pixel clk
*                        u16_vactive, uint 1 line, from std table
*                        u16_hoffset, smaple by std pixel clk
*                        u16_voffset, uint 1 line, from std table
*
*  Returned value:  if match success return mode vic, else return VFMT_INVALID
*  Remark:
***************************************************************/
UINT8 ms2160_match_std_timing(VIDEOTIMING_T * ptTiming)
{
    UINT8       i;
    UINT8       u8index = 0xff;
    UINT16      u16tmp;
    UINT16      u16fosc;
    UINT16      u16detHTotal;
    UINT16      u16detVTotal;
    UINT16      u16detHsync;
    UINT16      u16detVsync;
    UINT16      u16detVFreq;
    UINT8       u8detPolarity;
    BOOL        bdetIsProgr;
    UINT16      u16stdHTotal; // std htotal by FOSC sample
    UINT16      u16stdVTotal;
    UINT16      u16stdHsync;  // std hsync width by FOSC sample
    UINT16      u16stdVsync;
    UINT16      u16stdVFreq;
    UINT8       u8stdPolarity;
    BOOL        bstdIsProgr;
    UINT16      u16VTOfst = MDT_STEP5_MARGIN;
    UINT8       u8TableCount;
    UINT8       u8commonCount = 0;
    UINT8       u8step = 0;
    UINT8       u8_vic = VFMT_INVALID;
    VIDEOTIMING_T * pstTimingTable;

    //get input timing from mdt detect
    u16fosc       = ptTiming->u16_pixclk;
    u8detPolarity = ptTiming->u8_polarity;
    u16detHTotal  = ptTiming->u16_htotal;
    u16detVTotal  = ptTiming->u16_vtotal;
    u16detHsync   = ptTiming->u16_hsyncwidth;
    u16detVsync   = ptTiming->u16_vsyncwidth;
    u16detVFreq   = ptTiming->u16_vfreq;
    //
    bdetIsProgr = (u8detPolarity & MSRT_BIT0) ? 1 : 0;

    u8TableCount = _countof(g_arrTimingTable);
    
    while(1)
    {
        //reset each step common count
        u8commonCount = 0;
        for (i = 0; i < u8TableCount; i++)
        {
            pstTimingTable  = (VIDEOTIMING_T *)(&(g_arrTimingTable[i].st_timing));
            u8stdPolarity   = pstTimingTable->u8_polarity;
            bstdIsProgr     = (u8stdPolarity & MSRT_BIT0) ? 1 : 0;
            u16stdHTotal    = (UINT16)((UINT32)u16fosc * pstTimingTable->u16_htotal / pstTimingTable->u16_pixclk);
            u16stdVTotal    = pstTimingTable->u16_vtotal;
            u16stdHsync     = (UINT16)((UINT32)u16fosc * pstTimingTable->u16_hsyncwidth / pstTimingTable->u16_pixclk);
            u16stdVsync     = pstTimingTable->u16_vsyncwidth;
            u16stdVFreq     = pstTimingTable->u16_vfreq;
            if (u8step == 0)
            {
                if (bdetIsProgr == bstdIsProgr &&
                _abs(u16detHTotal - u16stdHTotal) <= MDT_HTOTAL_MARGIN &&
                _abs(u16detVTotal - u16stdVTotal) <= MDT_VTOTAL_MARGIN &&
                _abs(u16detVFreq  - u16stdVFreq)  <= MDT_VFREQ_MARGIN)
                {
                    if (u8commonCount == 0)
                    {
                        u8index = i;
                    }
                    u8commonCount ++;
                }
            }

            if (u8step == 1)
            {
                // add hv sync polarity judge
                if (u8detPolarity == u8stdPolarity &&
                _abs(u16detHTotal - u16stdHTotal) <= MDT_HTOTAL_MARGIN &&
                _abs(u16detVTotal - u16stdVTotal) <= MDT_VTOTAL_MARGIN &&
                _abs(u16detVFreq  - u16stdVFreq)  <= MDT_VFREQ_MARGIN)
                {
                    if (u8commonCount == 0)
                    {
                        u8index = i;
                    }
                    u8commonCount ++;
                }
            }
            
            if (u8step == 2)
            {
                // add vsync width
                if (u8detPolarity == u8stdPolarity &&
                _abs(u16detHTotal - u16stdHTotal) <= MDT_HTOTAL_MARGIN &&
                _abs(u16detVTotal - u16stdVTotal) <= MDT_VTOTAL_MARGIN &&
                _abs(u16detVFreq  - u16stdVFreq)  <= MDT_VFREQ_MARGIN &&
                _abs(u16detVsync  - u16stdVsync)  <= MDT_VSYNC_MARGIN)
                {
                    if (u8commonCount == 0)
                    {
                        u8index = i;
                    }
                    u8commonCount ++;
                }
            }

            if (u8step == 3)
            {
                // add h sync width
                if (u8detPolarity == u8stdPolarity &&
                _abs(u16detHTotal - u16stdHTotal) <= MDT_HTOTAL_MARGIN &&
                _abs(u16detVTotal - u16stdVTotal) <= MDT_VTOTAL_MARGIN &&
                _abs(u16detVFreq  - u16stdVFreq)  <= MDT_VFREQ_MARGIN &&
                _abs(u16detVsync  - u16stdVsync)  <= MDT_VFREQ_MARGIN &&
                _abs(u16detHsync  - u16stdHsync)  <= MDT_HSYNC_MARGIN)
                {
                    if (u8commonCount == 0)
                    {
                        u8index = i;
                    }
                    u8commonCount ++;
                }
            }

            if (u8step == 4)
            {
                // 
                if (bdetIsProgr == bstdIsProgr &&
                _abs(u16detVTotal - u16stdVTotal) <= MDT_STEP5_MARGIN &&
                _abs(u16detVFreq  - u16stdVFreq)  <= MDT_VFREQ_MARGIN)
                {
                    u16tmp = _abs(u16detVTotal - u16stdVTotal);
                    if (u16tmp < u16VTOfst)
                    {
                        u8index = i;
                        u16VTOfst = u16tmp;
                    }
                    u8commonCount ++;
                }
            }               
        }// end of for(;;)

        // process step
        if (u8step == 0)
        {
            if (u8commonCount == 0)
            {
                u8step = 4;
            }
            else if (u8commonCount == 1)
            {
                u8step = 1;
                break;
            }
            else
            {
                u8step = 1;
            }
        }
        else if (u8step == 1 || u8step == 2)
        {
            if (u8commonCount == 0)
            {
                break;
            }
            else if (u8commonCount == 1)
            {
                u8step += 1;
                break;
            }
            else
            {
                u8step += 1;
            }
        }
        else if (u8step == 3)
        {
            if (u8commonCount == 0)
            {
                break;
            }
            else
            {
                u8step = 4;
                break;
            }
        }
        else if (u8step == 4) // step = 4 or thers
        {
            if (u8commonCount == 0)
            {
                u8step = 0;
            }
            else
            {
                u8step = 5;
            }
            break;
        }
    }// end while(1)
        
    if (u8index == 0xff)
    {
        u8index = 0;
    }
    else
    {
        u8_vic = g_arrTimingTable[u8index].u8_vic;
    }

    ptTiming->u16_htotal     = g_arrTimingTable[u8index].st_timing.u16_htotal;
    ptTiming->u16_hsyncwidth = g_arrTimingTable[u8index].st_timing.u16_hsyncwidth;
    ptTiming->u16_hactive    = g_arrTimingTable[u8index].st_timing.u16_hactive;
    ptTiming->u16_vactive    = g_arrTimingTable[u8index].st_timing.u16_vactive;
    ptTiming->u16_hoffset    = g_arrTimingTable[u8index].st_timing.u16_hoffset;
    ptTiming->u16_voffset    = g_arrTimingTable[u8index].st_timing.u16_voffset;
    //pixClk / 10000
    ptTiming->u16_pixclk     = (UINT16)((UINT32)u16fosc * ptTiming->u16_htotal / u16detHTotal);

    return u8_vic;
}
