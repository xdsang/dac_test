/**
******************************************************************************
* @file    ms2160.c
* @author  
* @version
* @date   
* @brief   ms2160 SDK Library interfaces implementation
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "ms2160.h"

#include "ms2160_drv_top.h"
#include "ms2160_drv_audio.h"
#include "ms2160_drv_dac.h"
#include "ms2160_drv_dvout.h"
#include "ms2160_drv_frc.h"
#include "ms2160_drv_hdmi_tx.h"
#include "ms2160_drv_mem_dfi.h"
#include "ms2160_drv_mem_dmc.h"
#include "ms2160_drv_mem_iic2mem.h"
#include "ms2160_drv_tve.h"
#include "ms2160_drv_vcrsff.h"
#include "ms2160_drv_vds_su.h"
#include "ms2160_drv_vds_timing.h"
#include "ms2160_drv_vds_vehc.h"
#include "ms2160_drv_vpack.h"
#include "ms2160_drv_pllvm.h"
#include "ms928x_hdmi_phy.h"

// macro enum structure definitions
#if MS2160_FPGA_VERIFY
#define REG_FPGA_DCLK_SEL  (0xF07E)
#define BIT_FPGA_DCLK_SEL  (MSRT_BITS2_0)
#endif

// variables definitions
#if (MS2160_FPGA_VERIFY && defined(__KEIL_C__))
static UINT8 __XDATA g_u8_ms2160_sdram_size _at_  0xD270;
static UINT8 __XDATA g_u8_ms2160_video_output_port _at_ 0xD271;
static UINT8 __XDATA g_u8_ms2160_dvo_mode _at_ 0xD272;
static UINT8 __XDATA g_u8_ms2160_out_mode_vic _at_ 0xD273;
#else
static UINT8 g_u8_ms2160_sdram_size = 2; //refer to SDRAM_SIZE_E
static UINT8 g_u8_ms2160_video_output_port = 2; //refer to VIDEO_OUTPUT_PORT_E
static UINT8 g_u8_ms2160_dvo_mode = 0; //refer to MS2160_DVO_STANDARD_MODE_E
static UINT8 g_u8_ms2160_out_mode_vic = 0; //refer to MS2160_VIDEOFORMAT_E
#endif

static UINT8 g_u8_ms2160_sdram_clock = 120; //MHz
static UINT8 g_u8_ms2160_video_in_color_space = 0; //refer to VIDEO_COLOR_SPACE_E
static UINT8 g_u8_ms2160_video_in_mem_color_space = 0; //refer to VIDEO_COLOR_SPACE_E
static UINT8 g_u8_ms2160_video_out_color_space = 0; //refer to VIDEO_COLOR_SPACE_E
static UINT8 g_u8_ms2160_audio_mode = 0; //refer to MS2160_HDMI_AUDIO_MODE_E
static UINT8 g_u8_ms2160_audio_rate = 2; //refer to MS2160_HDMI_AUDIO_RATE_E
static UINT8 g_u8_ms2160_hdmi_out_flag = 1;

static UINT16 g_u16_ms2160_video_in_vfreq = 3000; //0.01Hz
static UINT16 g_u16_ms2160_video_out_vfreq = 6000;

static UINT8 g_u8_ms2160_power_mode = 0; //= 1 for power on, = 0 for power off

static VIDEOTIMING_T g_st_ms2160_video_out_timing;
static VDS_TIMING_T g_st_ms2160_vds_timing;



// helper decalarations
static VOID _top_init(VOID);
static VOID _pll_init(VOID);
static UINT8 _video_path_init(UINT8 u8_output_port);
static VOID _audio_path_init(VOID);
static BOOL _mem_init(VOID);
static VOID _video_path_mem_bypas_switch(BOOL b_on);
static VOID _frc_init(VOID);
static VOID _vcrsff_init(VOID);
static VOID _pllvm_init(VOID);
static VOID _vds_init(VOID);
static VOID _dvout_init(VOID);
static VOID _analog_hpd_init(VOID);
static VOID _analog_hpd_deinit(VOID);
static VOID _vbe_init(VOID);
static VOID _dvo_csc_config(VOID);
static VOID _hdmi_out_csc_config(VOID);
static VOID _vga_out_csc_config(VOID);
static VOID _ypbpr_out_csc_config(VOID);
static VOID _tve_out_csc_config(VOID);
static VOID _video_color_space_config(VOID);
static VOID _output_port_config(VOID);


// helper definitions
static VOID _top_init(VOID)
{
    ms2160drv_top_init();

    //memory clock and io mode config. if included
    if(SDRAM_SIZE_0M != g_u8_ms2160_sdram_size)
    {
        ms2160drv_top_config_mem_clk_div(g_u8_ms2160_sdram_clock);
        ms2160drv_top_pad_ctrl_memio_gpio_mode_switch(OFF);
    }

    //dvo io mode config. if included
    #if MS2160_FPGA_VERIFY
    ms2160drv_top_pad_ctrl_dvoio_gpio_mode_switch(OFF);
    #else
    if(VIDEO_OUTPUT_PORT_DIGITAL == g_u8_ms2160_video_output_port)
    {
        ms2160drv_top_pad_ctrl_dvoio_gpio_mode_switch(OFF);
    }
    #endif
}

static VOID _pll_init(VOID)
{
    #if(MS2160_PIXCLK_TYPE) //display clock from hdmi txpll
    if(VIDEO_OUTPUT_PORT_HDMI != g_u8_ms2160_video_output_port &&
       VIDEO_OUTPUT_PORT_DIGITAL != g_u8_ms2160_video_output_port)
    {
        ms2160drv_hdmi_tx_phy_set_clk(5400);
    }
    #else
    _pllvm_init();
    #endif
}

static UINT8 _video_path_init(UINT8 u8_output_port)
{
    UINT8 u8_init_result = MS2160SDK_RETURN_SUCCESS;
    
    //set power mode to power on
    g_u8_ms2160_power_mode = 1;
    
    //get the video output port
    g_u8_ms2160_video_output_port = u8_output_port;
    
    //top init
    _top_init();

    //vpack init
    ms2160drv_vpack_init();

    //pll init
    _pll_init();

    //mem, frc or vcrsff init based on sdram size
    if(SDRAM_SIZE_0M == g_u8_ms2160_sdram_size) //no sdram
    {
        _vcrsff_init();
        _video_path_mem_bypas_switch(TRUE);
    }
    else
    {
        _video_path_mem_bypas_switch(FALSE);

        ms2160drv_top_pad_ctrl_sel_1Mx16bits_dram(SDRAM_SIZE_2M == g_u8_ms2160_sdram_size);
        
        if(!_mem_init())
        {
            u8_init_result = MS2160SDK_RETURN_MEMORY_INIT_FAILED;
        }
        _frc_init();
    }

    //vds init
    _vds_init();

    //vbe init
    _vbe_init();
    
    return u8_init_result;
}

static VOID _audio_path_init(VOID)
{
    AUDIO_CONFIG_T st_config;
    UINT8 u8_audio_mode = 0;

    //disable i2s output
    ms2160drv_top_shutdown_i2s_output(TRUE);
    
    switch(g_u8_ms2160_audio_rate)
    {
    case MS2160_HDMI_AUD_RATE_44K1:
        u8_audio_mode = 0x02;
        break;
    case MS2160_HDMI_AUD_RATE_32K:
        u8_audio_mode = 0x00;
        break;
    case MS2160_HDMI_AUD_RATE_88K2:
        u8_audio_mode = 0x0A;
        break;
    case MS2160_HDMI_AUD_RATE_96K:
        u8_audio_mode = 0x0C;
        break;        
    case MS2160_HDMI_AUD_RATE_176K4:
        u8_audio_mode = 0x10;
        break;        
    case MS2160_HDMI_AUD_RATE_192K: 
        u8_audio_mode = 0x12;
        break;        
    default:
        u8_audio_mode = 0x05;
        break;
    }

    //audio default config: 48K, 16bits, 2 channels
    st_config.u8_audio_mode     = u8_audio_mode;
    st_config.u8_audio_bits     = AUD_LENGTH_16BITS;
    st_config.u8_audio_channels = AUD_2CH;
    st_config.u8_audio_ch0_mux  = 0;
    st_config.u8_audio_ch1_mux  = 1;
    st_config.u8_audio_ch2_mux  = 2;
    st_config.u8_audio_ch3_mux  = 3;
    st_config.u8_audio_ch4_mux  = 4;
    st_config.u8_audio_ch5_mux  = 5;
    st_config.u8_audio_ch6_mux  = 6;
    st_config.u8_audio_ch7_mux  = 7;
    
    ms2160drv_audio_init(&st_config);

    ms2160drv_audio_enable(TRUE);

    if(VIDEO_OUTPUT_PORT_DIGITAL != g_u8_ms2160_video_output_port)
    {
        //enable i2s output
        ms2160drv_top_shutdown_i2s_output(FALSE);
    }
    else
    {
        //disable i2s output
        ms2160drv_top_shutdown_i2s_output(TRUE);
    }
}

static BOOL _mem_init(VOID)
{
    BOOL b_init_result = TRUE;

    //reset dramc and dram phy
    ms2160drv_mem_dmc_reset_switch(TRUE);
    ms2160drv_mem_dfi_reset_switch(TRUE);
    ms2160drv_mem_dfi_soft_reset_switch(TRUE);
    
    //enable mem module clock
    ms2160drv_mem_dmc_clk_switch(TRUE);

    //release dramc and dram phy
    ms2160drv_mem_dmc_reset_switch(FALSE);
    ms2160drv_mem_dfi_reset_switch(FALSE);
    ms2160drv_mem_dfi_soft_reset_switch(FALSE);

    //enable ldo for pa
    ms2160drv_mem_dfi_ldo_switch(TRUE, TRUE);
    
    #if MS2160_FPGA_VERIFY
    //invert pa sample mode
    //HAL_SetBits(0xF300, 0x04);
    #endif

    if(SDRAM_SIZE_0M != g_u8_ms2160_sdram_size) //with sdram
    {
        //sdram configurations preparation
        ms2160drv_mem_dmc_sdram_basic_config(g_u8_ms2160_sdram_size);
        ms2160drv_mem_dfi_sdram_dq_map((UINT8)(g_u8_ms2160_sdram_size ? DFI_SDRAM_DQ_WIDTH_32BIT : DFI_SDRAM_DQ_WIDTH_16BIT_LOW));
        
        //module others configuration: keep default
        
        //dram phy calibration
        b_init_result = ms2160drv_mem_dfi_ktiming(FALSE);
        
        //sdram initialization
        ms2160drv_mem_dmc_sdram_init_trigger();
    }
    
    return b_init_result;
}

static VOID _video_path_mem_bypas_switch(BOOL b_on)
{
    ms2160drv_top_video_path_mem_bypas_switch(b_on);
}

static VOID _frc_init(VOID)
{
    //reset frc
    ms2160drv_frc_reset_switch(TRUE);

    //enable frc clock
    ms2160drv_frc_clk_switch(TRUE);

    //frc config.
    ms2160drv_frc_config(g_u8_ms2160_sdram_size);

    //release frc
    ms2160drv_frc_reset_switch(FALSE);
}

static VOID _vcrsff_init(VOID)
{
    //reset vcrsff
    ms2160drv_vcrsff_reset(TRUE);

    //enable vcrsff clock
    ms2160drv_vcrsff_clk_switch(TRUE);

    //vcrsff config.
    ms2160drv_vcrsff_config();

    //release vcrsff
    ms2160drv_vcrsff_reset(FALSE);
}

static VOID _pllvm_init(VOID)
{
    ms2160drv_pllv_init();

    if(VIDEO_OUTPUT_PORT_HDMI != g_u8_ms2160_video_output_port &&
       VIDEO_OUTPUT_PORT_DIGITAL != g_u8_ms2160_video_output_port)
    {
        ms2160drv_pllv_config(5400);
    }
}

static VOID _vds_init(VOID)
{
    //vds_timgen init.
    ms2160drv_vds_timgen_init();

    //vds_su init.
    ms2160drv_vds_su_init();

    //vds_vehc init.
    ms2160drv_vds_vehc_init();

    //mute pattern sel: default pure blue
    ms2160drv_vds_vehc_tst_pat_sel(1);
}

static VOID _dvout_init(VOID)
{
    //power down
    ms2160drv_dvo_power_switch(OFF);

    //init
    ms2160drv_dvo_init();
}

static VOID _analog_hpd_init(VOID)
{
    if(0 == g_u8_ms2160_power_mode) //power off mode
    {
        //power on bgref
        ms2160drv_top_bgref_switch(ON);
            
        //power on txpll or pllv
        #if(MS2160_PIXCLK_TYPE) //display clock from txpll
        ms2160drv_hdmi_tx_phy_set_clk(5400);
        #else
        _pllvm_init();
        #endif
            
        //power on vds
        ms2160drv_vds_timgen_clock_switch(ON);
        ms2160drv_vds_vehc_clock_switch(ON);
        ms2160drv_vds_timgen_reset_switch(OFF);
        ms2160drv_vds_vehc_reset_switch(OFF);
        ms2160drv_vds_timgen_trigger();

        //power on dvo for FPGA
        #if MS2160_FPGA_VERIFY
        _dvout_init();
        #endif
        
        //power on dac
        ms2160drv_dac_init();
        //set dac_mux channel based on current analog output port
        ms2160drv_dac_set_video_out_channel(g_u8_ms2160_video_output_port);
    
        //power on tve
        if(g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_VGA &&
           g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_YPBPR)
        {
            ms2160drv_tve_init();
        }
    }
    
    ms2160drv_hpd_init(g_u8_ms2160_video_output_port);
}

static VOID _analog_hpd_deinit(VOID)
{
    if(0 == g_u8_ms2160_power_mode) //power off mode
    {
        //power off tve
        if(g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_VGA &&
           g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_YPBPR)
        {
            ms2160drv_tve_power_switch(OFF);
        }

        //power off dac
        ms2160drv_dac_power_switch(OFF);
        
        //power off vds
        ms2160drv_vds_vehc_reset_switch(ON);
        ms2160drv_vds_timgen_reset_switch(ON);
        ms2160drv_vds_timgen_clock_switch(OFF);
        ms2160drv_vds_vehc_clock_switch(OFF);

        //power off txpll or pllv
        #if(MS2160_PIXCLK_TYPE) //display clock from txpll
        ms2160drv_hdmi_tx_pll_power_down();
        #else
        ms2160drv_pllv_uninit();
        #endif
        
        //power off bgref
        ms2160drv_top_bgref_switch(OFF);
    }
}


static VOID _vbe_init(VOID)
{
    if((UINT8)VIDEO_OUTPUT_PORT_DIGITAL == g_u8_ms2160_video_output_port) //digital video out
    {
        _dvout_init();
        ms2160drv_top_shutdown_dvout_video(FALSE);
    }
    else if((UINT8)VIDEO_OUTPUT_PORT_HDMI == g_u8_ms2160_video_output_port) //hdmi out
    {
        #if MS2160_FPGA_VERIFY
        //ms928x_hdmi_phy_init_for_fpga();
        #endif
        ms2160drv_hdmi_tx_shell_video_mute_enable(FALSE);
        ms2160drv_hdmi_tx_shell_audio_mute_enable(FALSE);
    }
    else //analog video out
    {
        #if MS2160_FPGA_VERIFY
        _dvout_init();
        ms2160drv_top_shutdown_dvout_video(FALSE);
        //enable the data send to digital output
        HAL_SetBits(MISCSEL, MSRT_BIT1);
        //dvo yuv2rgb bypass
        ms2160drv_dvo_yuv2rgb_byps(TRUE);
        //dvo clock invert
        HAL_SetBits(CLKCTRL2_F005, MSRT_BIT5);
        //dvo turn off blank data clip
        ms2160drv_dvo_blk_data_clip(2);
        #endif
        
        //dac power down
        ms2160drv_dac_power_switch(OFF);

        //dac init
        ms2160drv_dac_init();

        //dac hpd init
        #if MS2160_FPGA_VERIFY
        HAL_SetBits(TBUS_TOP, MSRT_BIT4); //enable vdac hpd test bus
        #endif
        _analog_hpd_init();

        //power down tve
        ms2160drv_tve_power_switch(OFF);
        
        switch(g_u8_ms2160_video_output_port)
        {       
        case VIDEO_OUTPUT_PORT_VGA: //analog vga out
            ms2160drv_top_cav_ypbpr_output_enable(FALSE);
            ms2160drv_top_shutdown_vgaout_timing(TRUE);
            break;
        case VIDEO_OUTPUT_PORT_YPBPR: //analog YPbPr out  
            ms2160drv_top_cav_ypbpr_output_enable(FALSE);
            break;
        default: //analog tve out
            //top dclk sel 2x
            ms2160drv_top_display_clock_sel_2x(TRUE);
            ms2160drv_tve_init();
            break;             
        }

        //set dac_mux channel based on current analog output port
        ms2160drv_dac_set_video_out_channel(g_u8_ms2160_video_output_port);
    }
}

static VOID _dvo_csc_config(VOID)
{
    switch(g_u8_ms2160_video_in_mem_color_space)
    {
    case COLOR_SPACE_YUV422:
    case COLOR_SPACE_YUV444:
        if(COLOR_SPACE_YUV422 == g_u8_ms2160_video_out_color_space ||
           COLOR_SPACE_YUV444 == g_u8_ms2160_video_out_color_space)
        {
            ms2160drv_dvo_yuv2rgb_byps(TRUE);
        }
        else
        {
            ms2160drv_dvo_yuv2rgb_byps(FALSE);
        }
        break;
    default:
        if(COLOR_SPACE_YUV422 == g_u8_ms2160_video_out_color_space ||
           COLOR_SPACE_YUV444 == g_u8_ms2160_video_out_color_space)
        {
            ms2160drv_top_vds_rgb2yuv_switch(TRUE);
            ms2160drv_dvo_yuv2rgb_byps(TRUE);
        }
        else
        {
            ms2160drv_top_vds_rgb2yuv_switch(FALSE);
            ms2160drv_dvo_yuv2rgb_byps(TRUE);
        }
        break;                
    }
}

static VOID _hdmi_out_csc_config(VOID)
{
    switch(g_u8_ms2160_video_in_mem_color_space)
    {
    case COLOR_SPACE_YUV422:
    case COLOR_SPACE_YUV444:
        if(COLOR_SPACE_YUV422 == g_u8_ms2160_video_out_color_space ||
           COLOR_SPACE_YUV444 == g_u8_ms2160_video_out_color_space)
        {
            ms2160drv_top_hdmi_rgb_output_en(FALSE);
        }
        else
        {
            ms2160drv_top_hdmi_rgb_output_en(TRUE);
        }
        break;
    default:
        if(COLOR_SPACE_YUV422 == g_u8_ms2160_video_out_color_space ||
           COLOR_SPACE_YUV444 == g_u8_ms2160_video_out_color_space)
        {
            ms2160drv_top_vds_rgb2yuv_switch(TRUE);
            ms2160drv_top_hdmi_rgb_output_en(FALSE);
        }
        else
        {
            ms2160drv_top_vds_rgb2yuv_switch(FALSE);
            ms2160drv_top_hdmi_rgb_output_en(FALSE);
        }
        break;                
    }
}

static VOID _vga_out_csc_config(VOID)
{
    switch(g_u8_ms2160_video_in_mem_color_space)
    {
    case COLOR_SPACE_YUV422:
    case COLOR_SPACE_YUV444:
        ms2160drv_top_cav_bypass(FALSE);
        break;
    default:
        ms2160drv_top_vds_rgb2yuv_switch(FALSE);
        ms2160drv_top_cav_bypass(TRUE);
        break;                
    }
}

static VOID _ypbpr_out_csc_config(VOID)
{
    switch(g_u8_ms2160_video_in_mem_color_space)
    {
    case COLOR_SPACE_YUV422:
    case COLOR_SPACE_YUV444:
        ms2160drv_top_vds_rgb2yuv_switch(FALSE);
        ms2160drv_top_cav_bypass(FALSE);
        break;
    default:
        ms2160drv_top_vds_rgb2yuv_switch(TRUE);
        ms2160drv_top_cav_bypass(FALSE);
        break;                
    }
}

static VOID _tve_out_csc_config(VOID)
{
    switch(g_u8_ms2160_video_in_mem_color_space)
    {
    case COLOR_SPACE_YUV422:
    case COLOR_SPACE_YUV444:
        ms2160drv_top_vds_rgb2yuv_switch(FALSE);
        break;
    default:
        ms2160drv_top_vds_rgb2yuv_range_sel(TRUE);
        ms2160drv_top_vds_rgb2yuv_switch(TRUE);
        break;                
    }
}

VOID _video_color_space_config(VOID)
{
    if(COLOR_SPACE_YUV422 == g_u8_ms2160_video_in_mem_color_space || //input color space is yuv
       COLOR_SPACE_YUV444 == g_u8_ms2160_video_in_mem_color_space)
    {
        ms2160drv_top_vds_rgb2yuv_switch(FALSE);
    }

    switch(g_u8_ms2160_video_output_port)
    {
    case VIDEO_OUTPUT_PORT_DIGITAL: //digital out
        _dvo_csc_config();
        break;
    case VIDEO_OUTPUT_PORT_HDMI: //hdmi out
        _hdmi_out_csc_config();
        break;
    case VIDEO_OUTPUT_PORT_VGA: //vga out
        _vga_out_csc_config();
        break;
    case VIDEO_OUTPUT_PORT_YPBPR: //ypbpr out
        _ypbpr_out_csc_config();
        break;
    default: //tve out
        _tve_out_csc_config();
        break;        
    }
}

static VOID _hdmi_output_port_config(VOID)
{
    MS2160_HDMI_CONFIG_T st_hdmi_timing;

    st_hdmi_timing.u8_hdmi_flag = g_u8_ms2160_hdmi_out_flag;
    st_hdmi_timing.u8_vic = g_u8_ms2160_out_mode_vic;
    st_hdmi_timing.u16_video_clk = g_st_ms2160_video_out_timing.u16_pixclk;
    st_hdmi_timing.u8_clk_rpt = MS2160_HDMI_X1CLK;
    st_hdmi_timing.u8_scan_info = MS2160_HDMI_UNDERSCAN;
    st_hdmi_timing.u8_aspect_ratio = ((10 * g_st_ms2160_video_out_timing.u16_hactive / g_st_ms2160_video_out_timing.u16_vactive) < 15) ? MS2160_HDMI_4X3 : MS2160_HDMI_16X9;
    st_hdmi_timing.u8_color_space = MS2160_HDMI_RGB;
    st_hdmi_timing.u8_color_depth = MS2160_HDMI_COLOR_DEPTH_8BIT;
    st_hdmi_timing.u8_colorimetry = (st_hdmi_timing.u8_vic == VFMT_CEA_02_720x480P_60HZ ||
                                     st_hdmi_timing.u8_vic == VFMT_CEA_17_720x576P_50HZ) ? MS2160_HDMI_COLORIMETRY_601 : MS2160_HDMI_COLORIMETRY_709;
    st_hdmi_timing.u8_audio_mode = g_u8_ms2160_audio_mode;
    st_hdmi_timing.u8_audio_rate = g_u8_ms2160_audio_rate;
    st_hdmi_timing.u8_audio_bits = MS2160_HDMI_AUD_LENGTH_16BITS;
    st_hdmi_timing.u8_audio_channels = MS2160_HDMI_AUD_2CH;
    st_hdmi_timing.u8_audio_speaker_locations = 0;

    ms2160drv_hdmi_tx_config((HDMI_CONFIG_T *)&st_hdmi_timing);
}

static VOID _output_port_config(VOID)
{
    switch(g_u8_ms2160_video_output_port)
    {
    case VIDEO_OUTPUT_PORT_VGA: //vga out
        break;
    case VIDEO_OUTPUT_PORT_YPBPR: //ypbpr out
        if(g_st_ms2160_video_out_timing.u16_vactive >= 720 || //HD mode
           g_st_ms2160_video_out_timing.u16_pixclk  >= 7425)
        {
            ms2160drv_top_cav_ypbpr_trisync_enable(TRUE);
        }
        else
        {
            ms2160drv_top_cav_ypbpr_trisync_enable(FALSE);
        }
        break;
    case VIDEO_OUTPUT_PORT_HDMI: //hdmi out
        _hdmi_output_port_config();
        #if MS2160_FPGA_VERIFY
        //ms928x_hdmi_phy_config_for_fpga();
        #endif
        break;
    case VIDEO_OUTPUT_PORT_DIGITAL: //digital out
        break;
    default: //tve out
        if(VFMT_CEA_02_720x480P_60HZ == g_u8_ms2160_out_mode_vic) //ntsc
        {
            ms2160drv_tve_outmode(MS2160_TV_MODE_NTSC_MJ);
        }
        else //pal
        {
            ms2160drv_tve_outmode(MS2160_TV_MODE_PAL_NBDHJI);
        }
        break;
    }
}


//exported
UINT8 ms2160_init(UINT8 u8_output_port)
{
    UINT8 u8_init_result = MS2160SDK_RETURN_SUCCESS;

    //video path init.
    u8_init_result = _video_path_init(u8_output_port);
    
    //audio path init.
    _audio_path_init();

    return u8_init_result;
}

UINT8 ms2160_video_init(UINT8 u8_output_port)
{
    return _video_path_init(u8_output_port);
}

VOID ms2160_audio_init(VOID)
{
    _audio_path_init();
}

VOID ms2160_power_off(VOID)
{
    //set power mode to power off
    g_u8_ms2160_power_mode = 0;
    
    //module memory and frc if involved
    if(g_u8_ms2160_sdram_size != SDRAM_SIZE_0M)
    {
        //module frc reset, clock disable
        ms2160drv_frc_reset_switch(ON);    
        ms2160drv_frc_clk_switch(OFF);
    
        //module dmc and dfi reset, clock disable
        ms2160drv_mem_dmc_reset_switch(ON);
        ms2160drv_mem_dfi_reset_switch(ON);
        ms2160drv_mem_dfi_soft_reset_switch(ON);
        ms2160drv_mem_dmc_clk_switch(OFF);

        //ldo for pa power off
        ms2160drv_mem_dfi_ldo_switch(OFF, OFF);
    }
    else
    {
        //module vcrsff reset and clock disable
        ms2160drv_vcrsff_reset(ON);
        ms2160drv_vcrsff_clk_switch(OFF);
    }

    //module vds
    ms2160drv_vds_vehc_reset_switch(ON);
    ms2160drv_vds_timgen_reset_switch(ON);
    ms2160drv_vds_vehc_clock_switch(OFF);
    ms2160drv_vds_timgen_clock_switch(OFF);

    //module pll
    #if(MS2160_PIXCLK_TYPE) //display clock from hdmi txpll
    //tx pll power off
    ms2160drv_hdmi_tx_pll_power_down();
    #else
    //pllv power off
    ms2160drv_pllv_uninit();
    #endif
    
    //module vbe
    ms2160drv_hdmi_tx_phy_power_down();
    
    switch(g_u8_ms2160_video_output_port)
    {
    case VIDEO_OUTPUT_PORT_DIGITAL:   
        ms2160drv_dvo_power_switch(OFF);
        break;
    case VIDEO_OUTPUT_PORT_HDMI:
        break;
    default:
        ms2160drv_dac_power_switch(OFF);
        if(g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_VGA &&
           g_u8_ms2160_video_output_port != VIDEO_OUTPUT_PORT_YPBPR)
        {
            ms2160drv_tve_power_switch(OFF);
        }
        break;
    }

    //module top: bgref switch off, enable memory io as gpio mode with pull up input status, and enable dvo io pull up
    ms2160drv_top_bgref_switch(OFF);
    ms2160drv_top_pad_ctrl_memio_gpio_mode_switch(ON);
    ms2160drv_top_pad_ctrl_dvoio_gpio_mode_switch(ON);
}

VOID ms2160_set_video_input_info(VIDEO_IN_INFO_T *p_st_video_in_info)
{
    g_u8_ms2160_video_in_color_space     = p_st_video_in_info->u8_in_color_space;
    g_u8_ms2160_video_in_mem_color_space = p_st_video_in_info->u8_in_mem_color_space;
    
    //vpack config.
    ms2160drv_vpack_set_video_in_info(p_st_video_in_info);

    //vds_su config.
    ms2160drv_vds_su_set_video_in_info(p_st_video_in_info);

    //frc config.
    ms2160drv_frc_vfreq_change(g_u16_ms2160_video_in_vfreq, g_u16_ms2160_video_out_vfreq);

    //csc config.
    _video_color_space_config();
}

VOID ms2160_set_video_output_info(VIDEO_OUT_INFO_T *p_st_video_out_info)
{
    #if MS2160_FPGA_VERIFY
    UINT16 u16_fpga_dclk = 0;
    #endif
    
    VIDEOTIMING_T *p_st_video_timing = &g_st_ms2160_video_out_timing;
    VDS_TIMING_T *p_st_vds_timing = &g_st_ms2160_vds_timing;

    g_u8_ms2160_out_mode_vic = p_st_video_out_info->u8_out_mode_index;
    g_u8_ms2160_video_out_color_space = p_st_video_out_info->u8_out_color_space;

    //csc config.
    _video_color_space_config();
    
    //get standard timing parameters
    ms2160_get_std_timing(g_u8_ms2160_out_mode_vic, p_st_video_timing);
        
    //timing conversion
    #if MS2160_FPGA_VERIFY
    if (p_st_video_timing->u16_pixclk == 2500)
    {
        u16_fpga_dclk = 2500;    
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x03); //25MHz
    }
    else if (p_st_video_timing->u16_pixclk < 4000) 
    {
        u16_fpga_dclk = 2700;
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x02); //27MHz
    }
    else if (p_st_video_timing->u16_pixclk == 4000) 
    {
        u16_fpga_dclk = 4000;
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x01); //40MHz
    }
    else if (p_st_video_timing->u16_pixclk < 6400) 
    {
        u16_fpga_dclk = 5400;
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x00); //54MHz
    }
    else
    {
        u16_fpga_dclk = 7425;
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x04); //74.25MHz
    }

    if(VIDEO_OUTPUT_PORT_HDMI != g_u8_ms2160_video_output_port &&
       VIDEO_OUTPUT_PORT_DIGITAL != g_u8_ms2160_video_output_port)
    {
        p_st_video_timing->u16_htotal = (UINT32)p_st_video_timing->u16_htotal * u16_fpga_dclk / p_st_video_timing->u16_pixclk;
        p_st_video_timing->u16_hsyncwidth = (UINT32)p_st_video_timing->u16_hsyncwidth * u16_fpga_dclk / p_st_video_timing->u16_pixclk;
        p_st_video_timing->u16_hoffset = (UINT32)p_st_video_timing->u16_hoffset * u16_fpga_dclk / p_st_video_timing->u16_pixclk;
        p_st_video_timing->u16_hactive = (UINT32)p_st_video_timing->u16_hactive * u16_fpga_dclk / p_st_video_timing->u16_pixclk;
    }    
    #endif

    g_u16_ms2160_video_out_vfreq = p_st_video_timing->u16_vfreq;

    //video out size info.
    p_st_video_out_info->st_video_out_size.u16_h = p_st_video_timing->u16_hactive;
    p_st_video_out_info->st_video_out_size.u16_v = p_st_video_timing->u16_vactive;

    p_st_vds_timing->htotal       =   p_st_video_timing->u16_htotal;
    p_st_vds_timing->vtotal       =   p_st_video_timing->u16_vtotal;
    p_st_vds_timing->hsync_width  =   p_st_video_timing->u16_hsyncwidth;
    p_st_vds_timing->vsync_width  =   p_st_video_timing->u16_vsyncwidth;
    p_st_vds_timing->dis_hde_st   =   p_st_video_timing->u16_hoffset;
    p_st_vds_timing->dis_hde_sp   =   p_st_video_timing->u16_hoffset + p_st_video_timing->u16_hactive;
    p_st_vds_timing->dis_vde_st   =   p_st_video_timing->u16_voffset;
    p_st_vds_timing->dis_vde_sp   =   p_st_vds_timing->dis_vde_st + p_st_video_timing->u16_vactive;
    
    //video output timing config.    
    ms2160drv_vds_timgen_set_video_output_timing(p_st_vds_timing);
    
    //hdmi txpll or pllv config. for display clock
    if(VIDEO_OUTPUT_PORT_VGA == g_u8_ms2160_video_output_port ||
       VIDEO_OUTPUT_PORT_HDMI == g_u8_ms2160_video_output_port ||
       VIDEO_OUTPUT_PORT_YPBPR == g_u8_ms2160_video_output_port ||
       VIDEO_OUTPUT_PORT_DIGITAL == g_u8_ms2160_video_output_port)
    {
        #if MS2160_PIXCLK_TYPE
        ms2160drv_hdmi_tx_phy_set_clk(p_st_video_timing->u16_pixclk);
        #else
        ms2160drv_pllv_config(p_st_video_timing->u16_pixclk);
        #endif
    }
    else //tve out
    {
        #if MS2160_FPGA_VERIFY
        //FPGA display clock sel 54MHz
        HAL_ModBits(REG_FPGA_DCLK_SEL, BIT_FPGA_DCLK_SEL, 0x00); //54MHz
        #endif
    }

    //vds timing trigger
    ms2160drv_vds_timgen_trigger();

    //video output port config
    _output_port_config();
}

VOID ms2160_set_video_zoom(VIDEO_IN_INFO_T *p_st_video_in_info, VIDEO_OUT_INFO_T *p_st_video_out_info)
{
    VIDEOSIZE_T *p_st_in_size = &p_st_video_in_info->st_in_video_size;
    VIDEOSIZE_T *p_st_out_size = &p_st_video_out_info->st_video_out_size;
    VIDEOSIZE_T *p_st_mem_size = &p_st_video_out_info->st_video_mem_size;

    ms2160drv_frc_set_offset_fetch(p_st_in_size->u16_h, p_st_mem_size->u16_h, p_st_video_in_info->u8_in_mem_color_space);
    ms2160drv_vds_su_set_ratio(p_st_mem_size, p_st_out_size);
    ms2160drv_vds_timgen_trigger();
}


VOID ms2160_set_video_transfer_mode(VIDEO_TRANSFER_MODE_T *p_st_transfer_mode)
{
    //vpack config.
    ms2160drv_vpack_set_video_transfer_mode(p_st_transfer_mode);

    //frc config.
    ms2160drv_frc_block_transmit_mode_en(p_st_transfer_mode->u8_trans_mode);
}

VOID ms2160_video_transfer_start(VOID)
{
    ms2160drv_vpack_video_transfer_start();
}

VOID ms2160_video_transfer_stop(VOID)
{
    ms2160drv_vpack_video_transfer_stop();
}

VOID ms2160_video_frame_switch(BOOL b_frame_index)
{
    ms2160drv_vpack_frame_switch(b_frame_index);
}

VOID ms2160_video_mute_switch(BOOL b_on)
{
    ms2160drv_vds_vehc_tst_pat_switch(b_on);
}

VOID ms2160_audio_mute_switch(BOOL b_on)
{
    ms2160drv_audio_mute(b_on);
}

VOID ms2160_video_mute_pattern_sel(UINT8 u8_pattern)
{
    ms2160drv_vds_vehc_tst_pat_sel(u8_pattern);
}

VOID ms2160_shutdown_dvout_timing(BOOL b_shutdown)
{
    ms2160drv_top_shutdown_dvout_video(b_shutdown);
}

VOID ms2160_shutdown_vgaout_timing(BOOL b_shutdown)
{
    ms2160drv_top_shutdown_vgaout_timing(b_shutdown);
}

VOID ms2160_turn_on_output_video(VOID)
{
    if((UINT8)VIDEO_OUTPUT_PORT_DIGITAL == g_u8_ms2160_video_output_port) //digital video out
    {
        ms2160drv_top_shutdown_dvout_video(FALSE);
    }
    else if((UINT8)VIDEO_OUTPUT_PORT_HDMI == g_u8_ms2160_video_output_port) //hdmi out
    {
        ms2160drv_hdmi_tx_phy_output_enable(TRUE);
    }
    else //analog video out
    {
        switch(g_u8_ms2160_video_output_port)
        {                      
        case VIDEO_OUTPUT_PORT_VGA: //analog vga out
            ms2160drv_top_shutdown_vgaout_timing(FALSE);
            break;
        case VIDEO_OUTPUT_PORT_YPBPR: //analog YPbPr out
            ms2160drv_top_cav_ypbpr_output_enable(TRUE);
            break;
        default: //analog tve out
            break; 
        }
    }    
}

VOID ms2160_turn_off_output_video(VOID)
{
    if((UINT8)VIDEO_OUTPUT_PORT_DIGITAL == g_u8_ms2160_video_output_port) //digital video out
    {
        ms2160drv_top_shutdown_dvout_video(TRUE);
    }
    else if((UINT8)VIDEO_OUTPUT_PORT_HDMI == g_u8_ms2160_video_output_port) //hdmi out
    {
        ms2160drv_hdmi_tx_phy_output_enable(FALSE);
    }
    else //analog video out
    {
        switch(g_u8_ms2160_video_output_port)
        {       
        case VIDEO_OUTPUT_PORT_VGA: //analog vga out
            ms2160drv_top_shutdown_vgaout_timing(TRUE);
            break;
        case VIDEO_OUTPUT_PORT_YPBPR: //analog YPbPr out
            ms2160drv_top_cav_ypbpr_output_enable(FALSE);
            break;
        default: //analog tve out
            break;
        }
    }    
}

UINT8 ms2160_get_sdram_type(VOID)
{
    return g_u8_ms2160_sdram_size;
}

UINT8 ms2160_get_video_output_mode(VOID)
{
    return g_u8_ms2160_out_mode_vic;
}

UINT8 ms2160_get_video_output_port(VOID)
{
    return g_u8_ms2160_video_output_port;
}

BOOL ms2160_analog_hotplug_detect(UINT8 u8_analog_port)
{
    BOOL b_status = FALSE;
    
    if(0 == g_u8_ms2160_power_mode) //power off mode
    {
        _analog_hpd_init();
        Delay_ms(255); //delay for hpd status stable        
    }
    
    b_status = ms2160drv_hpd_read_signal_status(u8_analog_port);

    if(0 == g_u8_ms2160_power_mode) //power off mode
    {
        _analog_hpd_deinit();
    }
    
    return b_status;
}


//HDMI interface
VOID ms2160_hdmi_output_enable(BOOL b_enable)
{
    ms2160drv_hdmi_tx_phy_output_enable(b_enable);
}

VOID ms2160_hdmi_video_mute_enable(BOOL b_enable)
{
    ms2160drv_hdmi_tx_shell_video_mute_enable(b_enable);
}

VOID ms2160_hdmi_audio_mute_enable(BOOL b_enable)
{
    ms2160drv_hdmi_tx_shell_audio_mute_enable(b_enable);
}

BOOL ms2160_hdmi_parse_edid(UINT8 *p_u8_edid_buf, MS2160_EDID_FLAG_T *pt_edid)
{ 
    BOOL b_parse_result = FALSE;
    
    b_parse_result = ms2160drv_hdmi_tx_parse_edid(p_u8_edid_buf, (HDMI_EDID_FLAG_T *) pt_edid);
    g_u8_ms2160_hdmi_out_flag = b_parse_result ? pt_edid->u8_hdmi_sink : 1;

    return b_parse_result;
}

VOID ms2160_hdmi_set_audio_mode(UINT8 u8_audio_mode)
{
    g_u8_ms2160_audio_mode = u8_audio_mode;
}

VOID ms2160_hdmi_set_audio_rate(UINT8 u8_audio_rate)
{
    g_u8_ms2160_audio_rate = u8_audio_rate;
}


VOID ms2160_hdmi_power_down(VOID)
{
    ms2160drv_hdmi_tx_phy_power_down();
}

BOOL ms2160_hdmi_hotplug_detect(VOID)
{
    return ms2160drv_hdmi_tx_shell_hpd();
}

VOID ms2160_hdmi_ddc_enable(BOOL b_enable)
{
    ms2160drv_hdmi_tx_ddc_enable(b_enable);
}

VOID ms2160_hdmi_hdcp_enable(BOOL b_enable)
{
    ms2160drv_hdmi_tx_hdcp_enable(b_enable);
}

BOOL ms2160_hdmi_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv)
{
    return ms2160drv_hdmi_tx_hdcp_init(p_u8_key, p_u8_ksv);
}

UINT8 ms2160_hdmi_hdcp_get_status(VOID)
{
    return ms2160drv_hdmi_tx_hdcp_get_status();
}


//TVE interface
VOID ms2160_set_tv_output_mode(UINT8 u8_tv_mode)
{
    ms2160drv_tve_outmode(u8_tv_mode);
}


//Digital out interface
VOID ms2160_dvo_blk_data_clip(UINT8 u8_blk_data)
{
    ms2160drv_dvo_blk_data_clip(u8_blk_data);
}

// yuv2rgb byps
VOID ms2160_dvo_yuv2rgb_byps(BOOL b_enable)
{
    ms2160drv_dvo_yuv2rgb_byps(b_enable);
}

// rgb bit flip
VOID ms2160_dvo_rgb_bit_flip(BOOL b_enable)
{
    ms2160drv_dvo_rgb_bit_flip(b_enable);
}

// yuv hd enable
VOID ms2160_dvo_yuv_hd_en(BOOL b_enable)
{
    ms2160drv_dvo_yuv_hd_en(b_enable);
}

// range select
VOID ms2160_dvo_range_sel(BOOL b_sel)
{
    ms2160drv_dvo_range_sel(b_sel);
}

// Port configuration
VOID ms2160_dvo_port_control(MS2160_DVO_PORT_CONTROL_T *p_st_port_ctl)
{
    ms2160drv_dvo_port_control((DVO_PORT_CONTROL_T*)p_st_port_ctl);
}

// output mode
VOID ms2160_dvo_standard_mode(UINT8 u8_mode)
{
    ms2160drv_dvo_standard_mode(u8_mode);
}

// ddr mode enable
VOID ms2160_dvo_ddr_en(BOOL b_enable)
{
    ms2160drv_dvo_ddr_en(b_enable);
}

UINT8 ms2160_get_dvo_mode(VOID)
{
    return g_u8_ms2160_dvo_mode;
}

VOID ms2160_sdram_read(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value)
{
    ms2160drv_mem_iic2mem_read_memory(u32_addr_st, u32_length, p_u8_value);
}

VOID ms2160_sdram_write(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value)
{
    ms2160drv_mem_iic2mem_write_memory(u32_addr_st, u32_length, p_u8_value);
}

VOID ms2160_set_options(UINT8* pu8_opcode)
{
    if(0 == (pu8_opcode[6] & MSRT_BIT4))
    {
        g_u8_ms2160_dvo_mode     = (pu8_opcode[6] & MSRT_BITS3_1) >> 1;
        g_u8_ms2160_out_mode_vic = (pu8_opcode[6] & MSRT_BIT0) ? 17 : 2;
    }

    if(pu8_opcode[5] != 0xFF)
    {
        g_u8_ms2160_sdram_size = pu8_opcode[5];    
    }
}

