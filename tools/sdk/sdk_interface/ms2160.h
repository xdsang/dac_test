/**
******************************************************************************
* @file    ms2160.h
* @author  
* @version
* @date   
* @brief   ms2160 SDK Library interfaces declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_H__
#define __MACROSILICON_MS2160_H__

#include "common.h"
#include "ms2160top.h"
#include "ms2160_timing_table.h"


// driver interface for macro enum structure declarations

//Define MS2160 SDK return code
#define MS2160SDK_RETURN_SUCCESS               ((UINT8)0x00)   
#define MS2160SDK_RETURN_FATAL_ERROR           ((UINT8)0x01)
#define MS2160SDK_RETURN_CHIP_NOT_EXIST        ((UINT8)0x02)
#define MS2160SDK_RETURN_EDID_READ_ERROR       ((UINT8)0x03)
#define MS2160SDK_RETURN_MODE_NOT_CHANGED      ((UINT8)0x04)
#define MS2160SDK_RETURN_INPUT_NO_SIGNAL       ((UINT8)0x05)
#define MS2160SDK_RETURN_PARAMETER_INVALID     ((UINT8)0x06)
#define MS2160SDK_RETURN_MEMORY_INIT_FAILED    ((UINT8)0x07)
#define MS2160SDK_RETURN_UNKNOWN_STATUS        ((UINT8)0xFF)

typedef enum
{
    MS2160_PATTERN_PURE_BLACK           = ((UINT8)0x00),                       
    MS2160_PATTERN_PURE_BLUE            = ((UINT8)0x01),  
    MS2160_PATTERN_PURE_GREEN           = ((UINT8)0x02),  
    MS2160_PATTERN_PURE_RED             = ((UINT8)0x03),  
    MS2160_PATTERN_PURE_WHITE           = ((UINT8)0x04),  
    MS2160_PATTERN_CROSS_HATCH          = ((UINT8)0x05),  
    MS2160_PATTERN_HOR_RAMP             = ((UINT8)0x06),  
    MS2160_PATTERN_VER_RAMP             = ((UINT8)0x07),  
    MS2160_PATTERN_COLOR_BAR            = ((UINT8)0x08),  
    MS2160_PATTERN_HOR_GRAY_SACLE       = ((UINT8)0x09),  
    MS2160_PATTERN_VER_GRAY_SACLE       = ((UINT8)0x0A),  
    MS2160_PATTERN_2ND_HOR_GRAY_SACLE   = ((UINT8)0x0B),  
    MS2160_PATTERN_PRIMARY_COLOR        = ((UINT8)0x0C),  
    MS2160_PATTERN_INTERLACE_BLACK      = ((UINT8)0x0D),  
    MS2160_PATTERN_INTERLACE_RED        = ((UINT8)0x0E),  
    MS2160_PATTERN_INTERLACE_GREEN      = ((UINT8)0x0F),  
    MS2160_PATTERN_INTERLACE_BLUE       = ((UINT8)0x10)
}MS2160_VIDEO_MUTE_PATTERN_E;    

typedef enum
{
    MS2160_TV_MODE_NTSC_MJ = 0,   
    MS2160_TV_MODE_NTSC_443,
    MS2160_TV_MODE_PAL_M,
    MS2160_TV_MODE_PAL_NC,
    MS2160_TV_MODE_PAL_NBDHJI
}MS2160_TV_OUTPUT_MODE_E;

//HDMI typedef
typedef enum
{
    MS2160_HDMI_X1CLK      = 0x00,
    MS2160_HDMI_X2CLK      = 0x01,
    MS2160_HDMI_X3CLK      = 0x02,
    MS2160_HDMI_X4CLK      = 0x03,
    MS2160_HDMI_X5CLK      = 0x04,
    MS2160_HDMI_X6CLK      = 0x05,
    MS2160_HDMI_X7CLK      = 0x06,
    MS2160_HDMI_X8CLK      = 0x07,
    MS2160_HDMI_X9CLK      = 0x08,
    MS2160_HDMI_X10CLK     = 0x09
}MS2160_HDMI_CLK_RPT_E;

typedef enum
{
    MS2160_HDMI_4X3     = 0x01,
    MS2160_HDMI_16X9    = 0x02
}MS2160_HDMI_ASPECT_RATIO_E;

typedef enum
{
    MS2160_HDMI_OVERSCAN     = 0x01,    //television type
    MS2160_HDMI_UNDERSCAN    = 0x02,   //computer type
}MS2160_HDMI_SCAN_INFO_E;

typedef enum
{
    MS2160_HDMI_RGB        = 0x00,
    MS2160_HDMI_YCBCR422   = 0x01,
    MS2160_HDMI_YCBCR444   = 0x02,
    MS2160_HDMI_XVYCC444   = 0x03
}MS2160_HDMI_CS_E;

typedef enum
{
    MS2160_HDMI_COLOR_DEPTH_8BIT    = 0x00,
    MS2160_HDMI_COLOR_DEPTH_10BIT   = 0x01,
    MS2160_HDMI_COLOR_DEPTH_12BIT   = 0x02,
    MS2160_HDMI_COLOR_DEPTH_16BIT   = 0x03
}MS2160_HDMI_COLOR_DEPTH_E;

typedef enum
{
    MS2160_HDMI_COLORIMETRY_601      = 0x00,
    MS2160_HDMI_COLORIMETRY_709      = 0x01,
    MS2160_HDMI_COLORIMETRY_656      = 0x02,
    MS2160_HDMI_COLORIMETRY_1120     = 0x03,
    MS2160_HDMI_COLORIMETRY_SMPTE    = 0x04,
    MS2160_HDMI_COLORIMETRY_XVYCC601 = 0x05,
    MS2160_HDMI_COLORIMETRY_XVYCC709 = 0x06
}MS2160_HDMI_COLORIMETRY_E;


//HDMI audio
typedef enum
{
    MS2160_HDMI_AUD_MODE_AUDIO_SAMPLE  = 0x00,
    MS2160_HDMI_AUD_MODE_HBR           = 0x01,
    MS2160_HDMI_AUD_MODE_DSD           = 0x02,
    MS2160_HDMI_AUD_MODE_DST           = 0x03
}MS2160_HDMI_AUDIO_MODE_E;

typedef enum
{
    MS2160_HDMI_AUD_RATE_44K1  = 0x00,
    MS2160_HDMI_AUD_RATE_48K   = 0x02,
    MS2160_HDMI_AUD_RATE_32K   = 0x03,
    MS2160_HDMI_AUD_RATE_88K2  = 0x08,
    MS2160_HDMI_AUD_RATE_96K   = 0x0A,
    MS2160_HDMI_AUD_RATE_176K4 = 0x0C,
    MS2160_HDMI_AUD_RATE_192K  = 0x0E
}MS2160_HDMI_AUDIO_RATE_E;

typedef enum
{
    MS2160_HDMI_AUD_LENGTH_16BITS    = 0x00,
    MS2160_HDMI_AUD_LENGTH_20BITS    = 0x01,
    MS2160_HDMI_AUD_LENGTH_24BITS    = 0x02
}MS2160_HDMI_AUDIO_LENGTH_E;

typedef enum
{
    MS2160_HDMI_AUD_2CH    = 0x01,
    MS2160_HDMI_AUD_3CH    = 0x02,
    MS2160_HDMI_AUD_4CH    = 0x03,
    MS2160_HDMI_AUD_5CH    = 0x04,
    MS2160_HDMI_AUD_6CH    = 0x05,
    MS2160_HDMI_AUD_7CH    = 0x06,
    MS2160_HDMI_AUD_8CH    = 0x07
}MS2160_HDMI_AUDIO_CHN_E;

typedef struct
{   
    UINT8  u8_hdmi_flag;          // FALSE = dvi out;  TRUE = hdmi out
    UINT8  u8_vic;                // reference to CEA-861 VIC
    UINT16 u16_video_clk;         // TMDS video clk, uint 10000Hz
    UINT8  u8_clk_rpt;            // enum refer to HDMI_CLK_RPT_E. X2CLK = 480i/576i, others = X1CLK
    UINT8  u8_scan_info;          // enum refer to HDMI_SCAN_INFO_E
    UINT8  u8_aspect_ratio;       // enum refer to HDMI_ASPECT_RATIO_E
    UINT8  u8_color_space;        // enum refer to HDMI_CS_E
    UINT8  u8_color_depth;        // enum refer to HDMI_COLOR_DEPTH_E
    UINT8  u8_colorimetry;        // enum refer to HDMI_COLORIMETRY_E. IT601 = 480i/576i/480p/576p, ohters = IT709
    //
    UINT8  u8_audio_mode;         // enum refer to HDMI_AUDIO_MODE_E
    UINT8  u8_audio_rate;         // enum refer to HDMI_AUDIO_RATE_E
    UINT8  u8_audio_bits;         // enum refer to HDMI_AUDIO_LENGTH_E
    UINT8  u8_audio_channels;     // enum refer to HDMI_AUDIO_CHN_E
    UINT8  u8_audio_speaker_locations;  // 0~255, refer to CEA-861 audio infoframe, BYTE4
} MS2160_HDMI_CONFIG_T;

typedef struct
{   
    UINT8 u8_hdmi_sink;         //= 1 for hdmi sink, = 0 for dvi sink
    UINT8 u8_color_space;       //color space support flag, flag 1 valid. BIT5: YCBCR444 flag; BIT4: YCBCR422 flag.(RGB must be support)
    UINT8 u8_edid_total_blocks; //block numbers, 128bytes in one block
    UINT8 u8_max_pixel_clk;     //Max Supported pixel clock rate in MHz/10, ERROR code is 0xFF 
} MS2160_EDID_FLAG_T;


//Digital out typedef
typedef enum
{
    MS2160_DVO_HS_SEL_HS    = 0,
    MS2160_DVO_HS_SEL_HDE   = 1,
    MS2160_DVO_HS_SEL_DE    = 2,
    MS2160_DVO_HS_SEL_VS    = 3
}MS2160_DVO_HS_SEL_E;

typedef enum
{
    MS2160_DVO_VS_SEL_VS    = 0,
    MS2160_DVO_VS_SEL_VDE   = 1,
    MS2160_DVO_VS_SEL_VS1   = 2,
    MS2160_DVO_VS_SEL_HS    = 3
}MS2160_DVO_VS_SEL_E;

typedef enum
{
    MS2160_DVO_H_DATA23_16 = 0,
    MS2160_DVO_M_DATA15_8  = 1,
    MS2160_DVO_L_DATA7_0   = 2
}MS2160_DVO_DATA_E;

typedef struct
{
    UINT8 u8_yc_flip;    // 1: flip  0: not flip
    UINT8 u8_uv_flip;    // 1: flip  0: not flip
    UINT8 u8_bit_remap;  // 1: remap  0: not remap
    UINT8 u8_data23_16;  //refer to enum MS2160_DVO_DATA_E
    UINT8 u8_data15_8;   //refer to enum MS2160_DVO_DATA_E
    UINT8 u8_data7_0;    //refer to enum MS2160_DVO_DATA_E
    UINT8 u8_hsync;      //refer to enum MS2160_DVO_HS_SEL_E
    UINT8 u8_vsync;      //refer to enum MS2160_DVO_VS_SEL_E
} MS2160_DVO_PORT_CONTROL_T;

typedef enum
{
    MS2160_DVO_BLK_CLIP_RGB000000 = 0,
    MS2160_DVO_BLK_CLIP_YUV008080 = 1,
    MS2160_DVO_BLK_CLIP_OFF = 2
} MS2160_DVO_BLANK_DATA_E;

typedef enum
{
    MS2160_DVO_24BIT_RGB   = 0,
    MS2160_DVO_8BIT_BT601  = 1,
    MS2160_DVO_16BIT_BT601 = 2,
    MS2160_DVO_8BIT_BT656  = 3,
    MS2160_DVO_16BIT_BT656 = 4,
    MS2160_DVO_8BIT_BT709  = 5,
    MS2160_DVO_16BIT_BT709 = 6,
    MS2160_DVO_8BIT_BT1120 = 7
}MS2160_DVO_STANDARD_MODE_E;


//SDK interface
/***************************************************************
*  Function name:     ms2160_init
*  Description:       ms2160 chip init, include video and audio path, module parameters config. and power on
*  Input parameters:  u8_output_port, refer to MS2160_VIDEO_OUTPUT_PORT_E
*  Output parameters: None
*  Returned value:    UINT8
***************************************************************/
UINT8 ms2160_init(UINT8 u8_output_port);


/***************************************************************
*  Function name:     ms2160_video_init
*  Description:       ms2160 video path init.
*  Input parameters:  u8_output_port, refer to MS2160_VIDEO_OUTPUT_PORT_E
*  Output parameters: None
*  Returned value:    UINT8
***************************************************************/
UINT8 ms2160_video_init(UINT8 u8_output_port);


/***************************************************************
*  Function name:     ms2160_audio_init
*  Description:       ms2160 audio path init.
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_audio_init(VOID);


/***************************************************************
*  Function name:     ms2160_power_off
*  Description:       ms2160 module reset and clock disable, current only power off video path
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_power_off(VOID);


/***************************************************************
*  Function name:     ms2160_set_video_input_info
*  Description:       ms2160 set input video information
*  Input parameters:  VIDEO_IN_INFO_T *
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_set_video_input_info(VIDEO_IN_INFO_T *p_st_video_in_info);


/***************************************************************
*  Function name:     ms2160_set_video_output_info
*  Description:       ms2160 set output video information
*  Input parameters:  VIDEO_OUT_INFO_T *
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_set_video_output_info(VIDEO_OUT_INFO_T *p_st_video_out_info);


/***************************************************************
*  Function name:     ms2160_set_video_zoom
*  Description:       ms2160 set video zoom
*  Input parameters:  VIDEO_OUT_INFO_T *, VIDEO_OUT_INFO_T *
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_set_video_zoom(VIDEO_IN_INFO_T *p_st_video_in_info, VIDEO_OUT_INFO_T *p_st_video_out_info);


/***************************************************************
*  Function name:     ms2160_set_video_transfer_mode
*  Description:       ms2160 set video transfer mode
*  Input parameters:  VIDEO_TRANSFER_MODE_T *
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_set_video_transfer_mode(VIDEO_TRANSFER_MODE_T *p_st_transfer_mode);


/***************************************************************
*  Function name:     ms2160_video_transfer_start
*  Description:       ms2160 video transfer start
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_video_transfer_start(VOID);


/***************************************************************
*  Function name:     ms2160_video_transfer_stop
*  Description:       ms2160 video transfer stop
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_video_transfer_stop(VOID);


/***************************************************************
*  Function name:     ms2160_video_frame_switch
*  Description:       ms2160 video frame switch
*  Input parameters:  BOOL b_frame_index
*  Output parameters: None
*  Returned value:    None
***************************************************************/

VOID ms2160_video_frame_switch(BOOL b_frame_index);


/***************************************************************
*  Function name:     ms2160_video_mute_switch
*  Description:       ms2160 video mute switch
*  Input parameters:  b_on
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_video_mute_switch(BOOL b_on);


/***************************************************************
*  Function name:     ms2160_audio_mute_switch
*  Description:       ms2160 audio mute switch
*  Input parameters:  b_on
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_audio_mute_switch(BOOL b_on);


/***************************************************************
*  Function name:     ms2160_video_mute_pattern_sel
*  Description:       ms2160 video mute pattern sel
*  Input parameters:  u8_pattern, refer to MS2160_VIDEO_MUTE_PATTERN_E
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_video_mute_pattern_sel(UINT8 u8_pattern);


/***************************************************************
*  Function name:     ms2160_shutdown_dvout_timing
*  Description:       ms2160 disable digital out
*  Input parameters:  BOOL b_shutdown
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_shutdown_dvout_timing(BOOL b_shutdown);


/***************************************************************
*  Function name:     ms2160_shutdown_vgaout_timing
*  Description:       ms2160 diable vga out
*  Input parameters:  BOOL b_shutdown
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_shutdown_vgaout_timing(BOOL b_shutdown);


/***************************************************************
*  Function name:     ms2160_turn_on_output_video
*  Description:       ms2160 enable video out
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_turn_on_output_video(VOID);


/***************************************************************
*  Function name:     ms2160_turn_off_output_video
*  Description:       ms2160 disable video out
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_turn_off_output_video(VOID);


/***************************************************************
*  Function name:     ms2160_get_sdram_type
*  Description:       get ms2160 sdram type used
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    UINT8, refer to enum SDRAM_SIZE_E
***************************************************************/
UINT8 ms2160_get_sdram_type(VOID);


/***************************************************************
*  Function name:     ms2160_get_video_output_mode
*  Description:       get ms2160 video output mode vic
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    UINT8, refer to enum MS2160_VIDEOFORMAT_E
***************************************************************/
UINT8 ms2160_get_video_output_mode(VOID);


/***************************************************************
*  Function name:     ms2160_get_video_output_port
*  Description:       get ms2160 video output port
*  Input parameters:  None
*  Output parameters: None
*  Returned value:    UINT8, refer to enum VIDEO_OUTPUT_PORT_E
***************************************************************/
UINT8 ms2160_get_video_output_port(VOID);


/***************************************************************
*  Function name:     ms2160_analog_hotplug_detect
*  Description:       get ms2160 analog out port hpd status
*  Input parameters:  UINT8 u8_analog_port, refer to VIDEO_OUTPUT_PORT_E
*  Output parameters: None
*  Returned value:    BOOL
***************************************************************/
BOOL ms2160_analog_hotplug_detect(UINT8 u8_analog_port);


//HDMI interface
VOID ms2160_hdmi_output_enable(BOOL b_enable);

VOID ms2160_hdmi_video_mute_enable(BOOL b_enable);

VOID ms2160_hdmi_audio_mute_enable(BOOL b_enable);

BOOL ms2160_hdmi_parse_edid(UINT8 *p_u8_edid_buf, MS2160_EDID_FLAG_T *pt_edid);

VOID ms2160_hdmi_power_down(VOID);

BOOL ms2160_hdmi_hotplug_detect(VOID);

VOID ms2160_hdmi_ddc_enable(BOOL b_enable);

VOID ms2160_hdmi_hdcp_enable(BOOL b_enable);

BOOL ms2160_hdmi_hdcp_init(UINT8 *p_u8_key, UINT8 *p_u8_ksv);

UINT8 ms2160_hdmi_hdcp_get_status(VOID);


//TVE interface
/***************************************************************
*  Function name:     ms2160_set_tv_output_mode
*  Description:       ms2160 TV out mode set
*  Input parameters:  u8_tv_mode, refer to MS2160_TV_OUTPUT_MODE_E
*  Output parameters: None
*  Returned value:    None
***************************************************************/
VOID ms2160_set_tv_output_mode(UINT8 u8_tv_mode);


//Digital out interface
/***************************************************************
 *  Function name:   ms2160_dvo_blk_data_clip
 *  Description:     Digital video output turn off blank data clip
 *  Entry:           u8_blk_data(refer to MS2160_DVO_BLANK_DATA_E):
 *                                DVO_BLK_CLIP_YUV008080
 *                                DVO_BLK_CLIP_RGB000000           
 *                                DVO_BLK_CLIP_OFF
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_blk_data_clip(UINT8 u8_blk_data);


/***************************************************************
 *  Function name:   ms2160_dvo_yuv2rgb_byps
 *  Description:     yuv2rgb color space convert 
 *  Entry:           b_enable: 1 enable 
 *                             0 disable                            
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_yuv2rgb_byps(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160_dvo_rgb_bit_flip
 *  Description:     bit flip for rgb out
 *  Entry:           b_enable: 1 enable (0->255,1->254,...)
 *                             0 disable
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_rgb_bit_flip(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160_dvo_yuv_hd_en
 *  Description:     yuv hd select
 *  Entry:           b_enable: 1 enable   HD
 *                             0 disable  SD
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_yuv_hd_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160_dvo_range_sel
 *  Description:     range select
 *  Entry:           b_enable: 1 enable   64~940
 *                             0 disable  0~1023
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_range_sel(BOOL b_sel);


/***************************************************************
 *  Function name:   ms2160_dvo_port_control
 *  Description:     Port configuration
 *  Entry:           p_st_port_ctl: 
 *                                 
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_port_control(MS2160_DVO_PORT_CONTROL_T *p_st_port_ctl);


/***************************************************************
 *  Function name:   ms2160_dvo_standard_mode
 *  Description:     digital video output mode
 *  Entry:           u8_mode(refer to MS2160_DVO_STANDARD_MODE_E):
 *                            DVO_24BIT_RGB  
 *                            DVO_8BIT_BT601 
 *                            DVO_16BIT_BT601
 *                            DVO_8BIT_BT656 
 *                            DVO_16BIT_BT656
 *                            DVO_8BIT_BT709 
 *                            DVO_16BIT_BT709
 *                            DVO_8BIT_BT1120
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_standard_mode(UINT8 u8_mode);


/***************************************************************
 *  Function name:   ms2160_dvo_ddr_en
 *  Description:     digital video output for ddr mode
 *  Entry:           b_enable: 1 enable
 *                             0 disable
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_dvo_ddr_en(BOOL b_enable);


/***************************************************************
 *  Function name:   ms2160_get_dvo_mode
 *  Description:     get digital video output standard mode
 *  Entry:           None
 *  
 *  Returned value:  UINT8
 *  Remark:
 ***************************************************************/
UINT8 ms2160_get_dvo_mode(VOID);


/***************************************************************
 *  Function name:   ms2160_sdram_read
 *  Description:     ms2160 read sdram by iic2sdram interface
 *  Entry:          u32_addr_st: sdram unit address(4Bytes/unit)
 *                    u32_length: read sdram unit number constantly
 *                    p_u8_value: read back buffer 
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_sdram_read(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value);


/***************************************************************
 *  Function name:   ms2160_sdram_write
 *  Description:     ms2160 write sdram by iic2sdram interface
 *  Entry:          u32_addr_st: sdram unit address(4Bytes/unit)
 *                    u32_length: write sdram unit number constantly
 *                    p_u8_value: write buffer 
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_sdram_write(UINT32 u32_addr_st, UINT32 u32_length, UINT8 *p_u8_value);


/***************************************************************
 *  Function name:   ms2160_set_options
 *  Description:     optional function set
 *  Entry:           UINT8* pu8_opcode
 *  Returned value:  None
 *  Remark:
 ***************************************************************/
VOID ms2160_set_options(UINT8* pu8_opcode);

#endif //__MACROSILICON_MS2160_H__
