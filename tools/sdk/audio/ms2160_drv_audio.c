/**
******************************************************************************
* @file    ms2160_drv_audio.c
* @author  
* @version V1.0.0
* @date    14-Dec-2017
* @brief   audio module driver source file
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"    
#include "ms2160_drv_audio.h"

#define MS2160_AUD_BUF_FIXED_BUG_0_ENABLE      (0)
#define MS2160_AUD_BUF_FIXED_BUG_1_ENABLE      (0)

typedef struct _T_AUDIO_TIMING_
{
    UINT8   u8_id;
    UINT32  u32_ws;
    UINT32  u32_sclk;
    UINT32  u32_mclk;
    UINT8   u8_x;
    UINT8   u8_mclk_sel;
    UINT8   u8_sclk_sel;
    UINT16  u16_dst_freq;
}ADUIO_TIMING_T;

ADUIO_TIMING_T __CODE g_arrAudioTimingTable[] =
{
    //==========================================================================================================
    //   Mode              WS(Hz)        SCLK(Hz)      MCLK(Hz)      X      mclk_sel     sclk_sel      dst_freq
    //==========================================================================================================
    {AUD_MODE_32K_0,       32000UL,     2048000UL,    8192000UL,     4,       1,           2,          20480},                 
    {AUD_MODE_32K_1,       32000UL,     2048000UL,   32768000UL,    16,       0,           4,          40960},
    {AUD_MODE_44K1_0,      44100UL,     2822400UL,   11289600UL,     4,       0,           2,          14112},
    {AUD_MODE_44K1_1,      44100UL,     2822400UL,   22579200UL,     8,       0,           3,          28224},
    {AUD_MODE_44K1_2,      44100UL,     2822400UL,   45158000UL,    16,       0,           4,          56448},
    {AUD_MODE_48K_0,       48000UL,     3072000UL,   12288000UL,     4,       0,           2,          15360},
    {AUD_MODE_48K_1,       48000UL,     3072000UL,   24576000UL,     8,       0,           3,          30720},
    {AUD_MODE_48K_2,       48000UL,     3072000UL,   49152000UL,    16,       0,           4,          61440},
    {AUD_MODE_64K_0,       64000UL,     4096000UL,    8192000UL,     2,       1,           1,          20480},
    {AUD_MODE_64K_1,       64000UL,     4096000UL,   32768000UL,     8,       0,           3,          40960},
    {AUD_MODE_88K2_0,      88200UL,     5644800UL,   11289600UL,     2,       0,           1,          14112},
    {AUD_MODE_88K2_1,      88200UL,     5644800UL,   22579200UL,     4,       0,           2,          28224},
    {AUD_MODE_96K_0,       96000UL,     6144000UL,   12288000UL,     2,       0,           1,          15360},
    {AUD_MODE_96K_1,       96000UL,     6144000UL,   24576000UL,     4,       0,           2,          30720},
    {AUD_MODE_128K_0,     128000UL,     8192000UL,    8192000UL,     1,       1,           0,          20480},                       
    {AUD_MODE_128K_1,     128000UL,     8192000UL,   32768000UL,     4,       0,           2,          40960},                       
    {AUD_MODE_176K4_0,    176400UL,    11289600UL,   11289600UL,     1,       0,           0,          14112},                       
    {AUD_MODE_176K4_1,    176400UL,    11289600UL,   22579200UL,     2,       0,           1,          28224},                       
    {AUD_MODE_192K_0,     192000UL,    12288000UL,   12288000UL,     1,       0,           0,          15360},                       
    {AUD_MODE_192K_1,     192000UL,    12288000UL,   24576000UL,     2,       0,           1,          30720},                       
};

static BOOL _get_audio_timing(ADUIO_TIMING_T * ptTiming)
{
    UINT8 u8_index = 0;

    for (u8_index = 0; u8_index < _countof(g_arrAudioTimingTable); u8_index ++)
    {
        if (ptTiming->u8_id == g_arrAudioTimingTable[u8_index].u8_id)
        {
            * ptTiming = g_arrAudioTimingTable[u8_index];
            return TRUE;
        }
    }
    
    MS2160_LOG("audio mode invalid.");
    return FALSE;
}

#if MS2160_AUD_BUF_FIXED_BUG_0_ENABLE
VOID _aud_buf_fixed_0(BOOL b_enable)
{
    HAL_ToggleBits(AUDIO_MISC_RESET_REG, MSRT_BIT0, b_enable);
    HAL_ToggleBits(AUD_ACR_CFG_REG, MSRT_BIT1, b_enable);
    HAL_ToggleBits(AUD_ENABLE_REG, MSRT_BIT0, !b_enable);
}
#endif

BOOL ms2160drv_audio_init(AUDIO_CONFIG_T *pt_config)
{
    BOOL b_flag = FALSE;
    ADUIO_TIMING_T t_timing;

    //##################################
    //freq_delta_th[11:0]:
    HAL_WriteWord(0xF19B, 0x10);
    //freq_inc[7:0]:
    HAL_WriteByte(0xF19A, 0x01);
    //ff_size_sel[7:0]: 
    //20181012, 
    HAL_WriteByte(0xF18b, 0xFF);
    //ff_diff_max_th[7:0]:
    HAL_WriteByte(0xF198, 0x30);
    //ff_diff_min_th[7:0]:
    HAL_WriteByte(0xF199, 0x20);
    //diff_intv_sel[3:0]:
    HAL_ModBits(0xF197, MSRT_BITS7_4, 0xB << 4);

    //##################################
    //reset audio moudle
    ms2160drv_audio_enable(FALSE);
    
    HAL_SetBits(AUDIO_MISC_CLK_REG, MSRT_BIT5);
    HAL_SetBits(AUD_ENABLE_REG, MSRT_BIT1);
    #if MS2160_AUD_BUF_FIXED_BUG_0_ENABLE
    _aud_buf_fixed_0(FALSE);
    #else
    HAL_ClrBits(AUD_ENABLE_REG, MSRT_BIT0);
    #endif
    
    t_timing.u8_id = pt_config->u8_audio_mode;
    if (_get_audio_timing(&t_timing))
    {
        b_flag = TRUE;
        HAL_ModBits(AUD_DST_CLK_SEL_REG, MSRT_BITS1_0, t_timing.u8_mclk_sel);
        HAL_ModBits(AUD_DST_CLK_SEL_REG, MSRT_BITS6_4, t_timing.u8_sclk_sel << 4);
        #if MS2160_FPGA_VERIFY
        HAL_WriteWord(AUD_DST_FREQ_L_REG, t_timing.u16_dst_freq * 2);
        #else
        HAL_WriteWord(AUD_DST_FREQ_L_REG, t_timing.u16_dst_freq);
        #endif
    }

    HAL_ModBits(AUD_OUT_FMT_REG, MSRT_BITS2_0, pt_config->u8_audio_channels);
    HAL_ModBits(AUD_OUT_FMT_REG, MSRT_BITS6_4, pt_config->u8_audio_bits << 4);

    HAL_WriteByte(AUD_CH0_DATA_SEL_REG, pt_config->u8_audio_ch0_mux + pt_config->u8_audio_ch1_mux << 4);
    HAL_WriteByte(AUD_CH1_DATA_SEL_REG, pt_config->u8_audio_ch2_mux + pt_config->u8_audio_ch3_mux << 4);
    HAL_WriteByte(AUD_CH2_DATA_SEL_REG, pt_config->u8_audio_ch4_mux + pt_config->u8_audio_ch5_mux << 4);
    HAL_WriteByte(AUD_CH3_DATA_SEL_REG, pt_config->u8_audio_ch6_mux + pt_config->u8_audio_ch7_mux << 4);

    #if MS2160_AUD_BUF_FIXED_BUG_0_ENABLE
    _aud_buf_fixed_0(TRUE);
    #endif
    
    return b_flag;
}

VOID ms2160drv_audio_enable(BOOL b_enable)
{
    #if !MS2160_AUD_BUF_FIXED_BUG_0_ENABLE
    HAL_ToggleBits(AUDIO_MISC_RESET_REG, MSRT_BIT0, b_enable);
    #else
    b_enable = FALSE;
    #endif
}

VOID ms2160drv_audio_mute(BOOL b_mute)
{
    HAL_ToggleBits(AUD_MUTE_REG, MSRT_BIT0, b_mute);
}



