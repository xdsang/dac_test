/**
******************************************************************************
* @file    ms2160_drv_audio.h
* @author  
* @version V1.0.0
* @date    31-May-2018
* @brief   audio module driver declare
* @history 
*
* Copyright (c) 2009 - 2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_AUDIO_H__
#define __MACROSILICON_MS2160_DRV_AUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

//hdmi tx register map.



/* Audio buffer module register address map */ 
#define AUDIO_REGBASE                    (0xF180)
#define AUD_ENABLE_REG                   (AUDIO_REGBASE + 0x00)
#define AUD_DST_FREQ_L_REG               (AUDIO_REGBASE + 0x01)
#define AUD_DST_FREQ_H_REG               (AUDIO_REGBASE + 0x02)
#define AUD_DST_CLK_SEL_REG              (AUDIO_REGBASE + 0x03)
#define AUD_FETCH_NUM_L_REG              (AUDIO_REGBASE + 0x04)
#define AUD_FETCH_NUM_H_REG              (AUDIO_REGBASE + 0x05)
#define AUD_OUT_FMT_REG                  (AUDIO_REGBASE + 0x06)
#define AUD_CH0_DATA_SEL_REG             (AUDIO_REGBASE + 0x07)
#define AUD_CH1_DATA_SEL_REG             (AUDIO_REGBASE + 0x08)
#define AUD_CH2_DATA_SEL_REG             (AUDIO_REGBASE + 0x09)
#define AUD_CH3_DATA_SEL_REG             (AUDIO_REGBASE + 0x0A)
#define AUD_FF_SIZE_REG                  (AUDIO_REGBASE + 0x0B)
#define AUD_OUT_SWAP_REG                 (AUDIO_REGBASE + 0x0C)
#define AUD_MUTE_REG                     (AUDIO_REGBASE + 0x0D)
#define AUD_FF_STATUS_REG                (AUDIO_REGBASE + 0x0E)
#define AUD_FF_NUM_RD_L_REG              (AUDIO_REGBASE + 0x0F)
#define AUD_FF_NUM_RD_H_REG              (AUDIO_REGBASE + 0x10)
#define AUD_FF_NUM_MAX_RD_L_REG          (AUDIO_REGBASE + 0x11)
#define AUD_FF_NUM_MAX_RD_H_REG          (AUDIO_REGBASE + 0x12)
#define AUD_FF_NUM_MIN_RD_L_REG          (AUDIO_REGBASE + 0x13)
#define AUD_FF_NUM_MIN_RD_H_REG          (AUDIO_REGBASE + 0x14)
#define AUD_XXX0_REG                     (AUDIO_REGBASE + 0x15)
#define AUD_XXX1_REG                     (AUDIO_REGBASE + 0x16)
#define AUD_ACR_CFG_REG                  (AUDIO_REGBASE + 0x17)
#define AUD_ACR_FF_NUM_MAX_TH_REG        (AUDIO_REGBASE + 0x18)
#define AUD_ACR_FF_NUM_MIN_TH_REG        (AUDIO_REGBASE + 0x19)
#define AUD_ACR_FREQ_INC_REG             (AUDIO_REGBASE + 0x1A)
#define AUD_ARC_FREQ_DELTA_TH_L_REG      (AUDIO_REGBASE + 0x1B)
#define AUD_ARC_FREQ_DELTA_TH_H_REG      (AUDIO_REGBASE + 0x1C)
#define AUD_ERR_EFFECT_REG               (AUDIO_REGBASE + 0x1D)
#define AUD_DST_FREQ_RD_L_REG            (AUDIO_REGBASE + 0x1E)
#define AUD_DST_FREQ_RD_H_REG            (AUDIO_REGBASE + 0x1F)
#define AUD_NSDET_CFG_REG                (AUDIO_REGBASE + 0x20)
#define AUD_NSDET_NUM_TH_L_REG           (AUDIO_REGBASE + 0x21)
#define AUD_NSDET_NUM_TH_H_REG           (AUDIO_REGBASE + 0x22)
#define AUD_NS_EFFECT_REG                (AUDIO_REGBASE + 0x23)
#define AUD_NSDET_STATUS_REG             (AUDIO_REGBASE + 0x24)
#define AUD_NSDET_DIN_NUM_RD_L_REG       (AUDIO_REGBASE + 0x25)
#define AUD_NSDET_DIN_NUM_RD_H_REG       (AUDIO_REGBASE + 0x26)
#define AUD_XXX2_REG                     (AUDIO_REGBASE + 0x27)
#define AUD_FF_DBG_RD_EN_REG             (AUDIO_REGBASE + 0x28)
#define AUD_FF_DBG_RD_ADDR_L_REG         (AUDIO_REGBASE + 0x29)
#define AUD_FF_DBG_RD_ADDR_H_REG         (AUDIO_REGBASE + 0x2A)
#define AUD_FF_DBG_RD_DATA_REG           (AUDIO_REGBASE + 0x2B)


#define AUDIO_MISC_CLK_REG               (0xF004) //20180809, resolve for mistake
#define AUDIO_MISC_RESET_REG             (0xF008)



//
//audio
typedef enum _E_AUDIO_I2S_CLK_MODE_
{
    AUD_MODE_32K_0   = 0x00,
    AUD_MODE_32K_1   = 0x01,
    AUD_MODE_44K1_0  = 0x02,
    AUD_MODE_44K1_1  = 0x03,
    AUD_MODE_44K1_2  = 0x04,
    AUD_MODE_48K_0   = 0x05,
    AUD_MODE_48K_1   = 0x06,
    AUD_MODE_48K_2   = 0x07,
    AUD_MODE_64K_0   = 0x08,
    AUD_MODE_64K_1   = 0x09,
    AUD_MODE_88K2_0  = 0x0A,
    AUD_MODE_88K2_1  = 0x0B,
    AUD_MODE_96K_0   = 0x0C,
    AUD_MODE_96K_1   = 0x0D,
    AUD_MODE_128K_0  = 0x0E,
    AUD_MODE_128K_1  = 0x0F,
    AUD_MODE_176K4_0 = 0x10,
    AUD_MODE_176K4_1 = 0x11,
    AUD_MODE_192K_0  = 0x12,
    AUD_MODE_192K_1  = 0x13
}AUDIO_MODE_E;


typedef enum _E_AUDIO_LENGTH_
{
    AUD_LENGTH_08BITS    = 0x00,
    AUD_LENGTH_16BITS    = 0x01,
    AUD_LENGTH_24BITS    = 0x02,
    AUD_LENGTH_32BITS    = 0x03
}AUDIO_LENGTH_E;

typedef enum _E_AUDIO_CHANNEL_
{
    AUD_1CH    = 0x00,
    AUD_2CH    = 0x01,
    AUD_6CH    = 0x02,
    AUD_8CH    = 0x03
}AUDIO_CHN_E;

typedef struct _T_AUDIO_CONFIG_PARA_
{   
    UINT8  u8_audio_mode;         // enum refer to AUDIO_MODE_E
    UINT8  u8_audio_bits;         // enum refer to AUDIO_LENGTH_E
    UINT8  u8_audio_channels;     // enum refer to AUDIO_CHN_E
    //
    UINT8 u8_audio_ch0_mux; //L channel, range from 0~7
    UINT8 u8_audio_ch1_mux; //R channel, range from 0~7
    UINT8 u8_audio_ch2_mux; //L channel, range from 0~7
    UINT8 u8_audio_ch3_mux; //R channel, range from 0~7
    UINT8 u8_audio_ch4_mux; //L channel, range from 0~7
    UINT8 u8_audio_ch5_mux; //R channel, range from 0~7
    UINT8 u8_audio_ch6_mux; //L channel, range from 0~7
    UINT8 u8_audio_ch7_mux; //R channel, range from 0~7
}AUDIO_CONFIG_T;



/***************************************************************
*  Function name:   ms2160drv_audio_init
*  Description:     audio buffer module parameter init
*  Entry:           [in]pt_config, refer to AUDIO_CONFIG_T
*
*  Returned value:  if init sucess return true, else return false
*  Remark:
***************************************************************/
BOOL ms2160drv_audio_init(AUDIO_CONFIG_T *pt_config);


/***************************************************************
*  Function name:   ms2160drv_audio_enable
*  Description:     audio buffer module enable
*  Entry:           [in]b_enable, if true enable the moudle, else reset the moudle
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_audio_enable(BOOL b_enable);


/***************************************************************
*  Function name:   ms2160drv_audio_mute
*  Description:     audio buffer module mute volume
*  Entry:           [in]b_mute, if true mute, else normal volume
*
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_audio_mute(BOOL b_mute);



#ifdef __cplusplus
}
#endif

#endif //__MACROSILICON_MS2160_DRV_AUDIO_H__
