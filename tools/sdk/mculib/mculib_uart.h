#ifndef _MCULIB_UART_H_
#define _MCULIB_UART_H_

void mculib_uart_init(void);

char putchar (char c);

/***************************************************************
*  Function name:   uart_log
*  Description:     printf log thru uart
*  Entry:           [in]u8_string:  string to printf
*
*  Returned value: if write success return TRUE, else return FALSE
*  Remark:
***************************************************************/
VOID mculib_uart_log(UINT8 *u8_string);


/***************************************************************
*  Function name:   uart_log1
*  Description:     printf log thru uart with string & hex value
*  Entry:           [in]u8_string:  string to printf
*                   [in]u16_hex:  hex value to printf
*
*  Returned value: if write success return TRUE, else return FALSE
*  Remark:
***************************************************************/
VOID mculib_uart_log1(UINT8 *u8_string, UINT16 u16_hex);


/***************************************************************
*  Function name:   uart_log2
*  Description:     printf log thru uart with string & decimal value
*  Entry:           [in]u8_string:  string to printf
*                   [in]u16_dec:  decimal value to printf
*
*  Returned value: if write success return TRUE, else return FALSE
*  Remark:
***************************************************************/
VOID mculib_uart_log2(UINT8 *u8_string, UINT16 u16_dec);

#endif //_MCULIB_UART_H_