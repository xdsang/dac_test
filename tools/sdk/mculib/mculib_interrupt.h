/**
******************************************************************************
* @file    mculib_interrupt.h
* @author  
* @version V1.0.0
* @date    
* @brief  This file declares the interrupt interface driver function.
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _MCULIB_INTERRUPT_H_
#define _MCULIB_INTERRUPT_H_

VOID mculib_interrupt_init(VOID);
VOID mculib_interrupt_enable(BOOL b_enable);

#endif //_MCULIB_INTERRUPT_H_