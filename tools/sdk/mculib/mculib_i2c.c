/**
******************************************************************************
* @file    mculib_i2c.c
* @author  
* @version V1.0.0
* @date   
* @brief   i2c interface driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "mculib.h"

static UINT8 g_u8_i2c_delay = 2; //default I2C speed is 100Kbit/s
static BOOL  g_b_ddc = FALSE;

#define I2C_TIMEOUT     (0x1000) //define I2C wait ack signal time out

#define I2C_SCL_HIGH    (g_b_ddc ? (reg_gpio6_oen = 0) : (reg_gpio4_oen = 0))
#define I2C_SDA_HIGH    (g_b_ddc ? (reg_gpio7_oen = 0) : (reg_gpio3_oen = 0))
#define I2C_SCL_LOW     _i2c_scl_low()
#define I2C_SDA_LOW     _i2c_sda_low()

#define _i2c_read_sda() ((g_b_ddc ? reg_gpio7_data : reg_gpio3_data))

#define _i2c_delay()    mculib_delay_us(g_u8_i2c_delay)

static VOID _i2c_scl_low(VOID)
{
    if(g_b_ddc)
    {
        reg_gpio6_oen  = 1;
        reg_gpio6_data = 0;
    }
    else
    {
        reg_gpio4_oen  = 1;
        reg_gpio4_data = 0;
    }
}

static VOID _i2c_sda_low(VOID)
{
    if(g_b_ddc)
    {
        reg_gpio7_oen  = 1;
        reg_gpio7_data = 0;
    }
    else
    {
        reg_gpio3_oen  = 1;
        reg_gpio3_data = 0;
    }
}

VOID mculib_i2c_set_ddc(BOOL ddc)
{
    g_b_ddc = ddc;
}

VOID mculib_i2c_init(VOID)
{
    reg_gpio_pu |= 0x18; //gpio3, gpio4 pull up enable
}

VOID mculib_i2c_set_speed(UINT8 u8_i2c_speed)
{
    switch (u8_i2c_speed)
    {
    case I2C_SPEED_20K:
        g_u8_i2c_delay = 15; //measure is 20.7KHz 
        break;
    case I2C_SPEED_100K:
        g_u8_i2c_delay = 2; //measure is 98.6KHz 
        break;
    case I2C_SPEED_400K:
        g_u8_i2c_delay = 1; //measure is 136.9KHz 
        break;
    case I2C_SPEED_750K:
        g_u8_i2c_delay = 1; //measure is 136.9KHz
        break;
    }
}

VOID _i2c_start(VOID)
{
    /* assume here all lines are HIGH */
    I2C_SDA_HIGH;
    I2C_SCL_HIGH;
    _i2c_delay();
    
    I2C_SDA_LOW;
    _i2c_delay();
    
    I2C_SCL_LOW;    
}

VOID _i2c_stop(VOID)
{
    I2C_SDA_LOW;
    _i2c_delay();
    
    I2C_SCL_HIGH;
    _i2c_delay();
    
    I2C_SDA_HIGH;
    _i2c_delay();
}

BOOL _i2c_write_byte(UINT8 value)
{
    UINT8  index;
    BOOL   result = FALSE;
    UINT16 u16Wait;
    
    /* Send 8 bits to the I2C Bus - msb first */
    for (index = 0; index < 8; index++)  
    {
        if (value & 0x80)
            I2C_SDA_HIGH;
        else    
            I2C_SDA_LOW;
        _i2c_delay();
        
        I2C_SCL_HIGH;        
        _i2c_delay();
        
        I2C_SCL_LOW;
        _i2c_delay();
        
        value <<= 1;    
    }
    
    /* wait for ack =  readbit0 */
    I2C_SDA_HIGH;
    _i2c_delay();
    
    I2C_SCL_HIGH;            
    _i2c_delay();
        
    u16Wait = 0;
    while (_i2c_read_sda() && (u16Wait < I2C_TIMEOUT)) 
        u16Wait ++;
    
    if (!_i2c_read_sda())
        result = TRUE;
    
    I2C_SCL_LOW;  
    _i2c_delay();

    return result;   
}

UINT8 _i2c_read_byte(BOOL lastByte)
{
    UINT8 index;
    UINT8 value = 0x00;
    
    I2C_SDA_HIGH;       
    for (index = 0; index < 8; index++) 
    {
        value <<= 1;        
        I2C_SCL_HIGH;           
        _i2c_delay();
        
        value |= _i2c_read_sda();  
        
        I2C_SCL_LOW;
        _i2c_delay();
    }
    
    if (!lastByte)
    {
        I2C_SDA_LOW;
    }
    else 
    {
        I2C_SDA_HIGH;
    }
    _i2c_delay();
    
    I2C_SCL_HIGH;    
    _i2c_delay();
    
    I2C_SCL_LOW;
    I2C_SDA_HIGH;
    _i2c_delay();
    
    return value;
}

// 16-bit index for macrosilicon chip
UINT8 mculib_i2c_read_16bidx8bval(UINT8 u8_address, UINT16 u16_index)
{
    UINT8 u8_value = 0;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;      
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;
    
    u8_value = _i2c_read_byte(TRUE);
    
STOP:
    _i2c_stop();
    return u8_value;
}

BOOL mculib_i2c_write_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT8 u8_value)
{
    BOOL result = FALSE;

    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;  
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;  
    
    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    if (!_i2c_write_byte(u8_value))                       
        goto STOP;  
    
    result = TRUE;

STOP:
    _i2c_stop();
    return result;                 
}

#if 1
VOID mculib_i2c_burstread_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
    UINT16 i;
   
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;

    for (i = 0; i < (u16_length-1); i ++)
    {
        *pu8_value++ = _i2c_read_byte(FALSE);
    } 

    *pu8_value = _i2c_read_byte(TRUE);
        
STOP:
    _i2c_stop();
}

VOID mculib_i2c_burstwrite_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
    UINT16 i;
    
   _i2c_start();
   
    if (!_i2c_write_byte(u8_address))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    for(i = 0; i< (u16_length-1); i ++)
    {
        if (!_i2c_write_byte(*pu8_value++))           
            goto STOP;  
    }

    if (!_i2c_write_byte(*pu8_value))           
        goto STOP;  
       
STOP:
    _i2c_stop();
}
#endif

#if 1
// 8-bit index fro eeprom AT24C02..
UINT8 mculib_i2c_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index)
{
    UINT8 u8_value = 0;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;      
    
    if (!_i2c_write_byte(u8_index))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;
    
    u8_value = _i2c_read_byte(TRUE);
    
STOP:
    _i2c_stop();
    return u8_value;
}

BOOL mculib_i2c_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value)
{
    BOOL result = FALSE;

    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;  
    
    if (!_i2c_write_byte(u8_index))
        goto STOP;  
    
    if (!_i2c_write_byte(u8_value))                       
        goto STOP;  
    
    result = TRUE;

STOP:
    _i2c_stop();
    return result;                 
}
#endif

VOID mculib_i2c_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value)
{
    UINT8 i;
   
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;
    
    if (!_i2c_write_byte(u8_index))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;

    for (i = 0; i < (u8_length-1); i ++)
    {
        *pu8_value++ = _i2c_read_byte(FALSE);
    } 

    *pu8_value = _i2c_read_byte(TRUE);
        
STOP:
    _i2c_stop();
}

#if 1
VOID mculib_i2c_burstwrite_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value)
{
    UINT8 i;
    
   _i2c_start();
   
    if (!_i2c_write_byte(u8_address))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u8_index) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u8_index >> 8) ))
        goto STOP;
    
    for(i = 0; i< (u8_length-1); i ++)
    {
        if (!_i2c_write_byte(*pu8_value++))           
            goto STOP;  
    }

    if (!_i2c_write_byte(*pu8_value))           
        goto STOP;  
       
STOP:
    _i2c_stop();
}
#endif

#if 0
// 16-bit index for eeprom AT24C04..
UINT8 mculib_i2c_read_eeprom_16bidx8bval(UINT8 u8_address, UINT16 u16_index)
{
    UINT8 u8_value = 0;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;      

    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;
    
    u8_value = _i2c_read_byte(TRUE);
    
STOP:
    _i2c_stop();
    return u8_value;
}

BOOL mculib_i2c_write_eeprom_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT8 u8_value)
{
    BOOL result = FALSE;

    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;  

    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;  
    
    if (!_i2c_write_byte(u8_value))                       
        goto STOP;  
    
    result = TRUE;

STOP:
    _i2c_stop();
    return result;                 
}

VOID mculib_i2c_burstread_eeprom_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
    UINT16 i;
   
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address))
        goto STOP;

    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    _i2c_start();
    
    if (!_i2c_write_byte(u8_address | 0x01))
        goto STOP;

    for (i = 0; i < (u16_length-1); i ++)
    {
        *pu8_value++ = _i2c_read_byte(FALSE);
    } 

    *pu8_value = _i2c_read_byte(TRUE);
        
STOP:
    _i2c_stop();
}

VOID mculib_i2c_burstwrite_eeprom_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value)
{
    UINT16 i;
    
   _i2c_start();
   
    if (!_i2c_write_byte(u8_address))
        goto STOP;

    if (!_i2c_write_byte( (UINT8)(u16_index >> 8) ))
        goto STOP;
    
    if (!_i2c_write_byte( (UINT8)(u16_index) ))
        goto STOP;
    
    for(i = 0; i< (u16_length-1); i ++)
    {
        if (!_i2c_write_byte(*pu8_value++))           
            goto STOP;  
    }

    if (!_i2c_write_byte(*pu8_value))           
        goto STOP;  
       
STOP:
    _i2c_stop();
}

//general I2C interfalce
VOID mculib_i2c_rw_register(MS_I2CX_RW_REGISTER_T *pst_i2cx_to_register)
{
    UINT16 u16_i = 0;

    if (pst_i2cx_to_register == NULL || pst_i2cx_to_register->p_u8_addr_buffer == NULL)
    {
        return;
    }

    _i2c_start();

    _i2c_write_byte(*(pst_i2cx_to_register->p_u8_addr_buffer) & 0xfe);
    
    for (u16_i = 0; u16_i < pst_i2cx_to_register->u8_addr_num - 1; u16_i++)
    {
        _i2c_write_byte(*(pst_i2cx_to_register->p_u8_addr_buffer + u16_i + 1));
    }

    if (pst_i2cx_to_register->u16_i2c_write_bytes_num != 0)
    {
        if ((pst_i2cx_to_register->p_u8_i2c_write_buffer == NULL))
        {
            return;
        }
        for (u16_i = 0; u16_i < pst_i2cx_to_register->u16_i2c_write_bytes_num; u16_i++)
        {
            _i2c_write_byte(*(pst_i2cx_to_register->p_u8_i2c_write_buffer + u16_i));
        }
    }

    if (pst_i2cx_to_register->u16_i2c_read_bytes_num == 0)
    {
        _i2c_stop();
    }
    else
    {
        if ((pst_i2cx_to_register->p_u8_i2c_read_buffer == NULL))
        {
            return;
        }
        
        _i2c_start();
        _i2c_write_byte(*(pst_i2cx_to_register->p_u8_addr_buffer) | 0x01);
        
        for (u16_i = 0; u16_i < pst_i2cx_to_register->u16_i2c_read_bytes_num; u16_i++)
        {
            if (u16_i < pst_i2cx_to_register->u16_i2c_read_bytes_num - 1)
            {
                *(pst_i2cx_to_register->p_u8_i2c_read_buffer + u16_i) = _i2c_read_byte(FALSE);
            }           
            else
            {
                *(pst_i2cx_to_register->p_u8_i2c_read_buffer + u16_i) = _i2c_read_byte(TRUE);
            }
        }
        
        _i2c_stop();
    }
}
#endif

