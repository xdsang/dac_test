/**
******************************************************************************
* @file    mculib_interrupt.c
* @author  
* @version V1.0.0
* @date    
* @brief   this file implements the interrupt interface function
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "mculib.h"

static VOID _interrupt_enable(BOOL b_enable);

static VOID _interrupt_enable(BOOL b_enable)
{
    EA = EX0 = ET0 = b_enable ? 1 : 0;
}

VOID mculib_interrupt_init(VOID)
{
    _interrupt_enable(FALSE);
}

VOID mculib_interrupt_enable(BOOL b_enable)
{
    _interrupt_enable(b_enable);
}

