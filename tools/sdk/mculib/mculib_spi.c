/**
******************************************************************************
* @file    mculib_spi.c
* @author  
* @version V1.0.0
* @date   
* @brief  spi interface driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "mculib.h"

VOID mculib_spi_init(VOID)
{
    CS_HIGH;
    SPCR =  0xd0;    
    SPER =  0x00;
    SPSR =  0xc0;     
}

UINT8 mculib_spi_readwritebyte(UINT8 u8_TxData)
{  
    UINT8 i;
    SPDR = u8_TxData;
    while(!SPIF);
    SPIF = 1; 
    i = SPDR;                
    return i ;      
}
