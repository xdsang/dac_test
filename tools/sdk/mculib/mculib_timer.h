/**
******************************************************************************
* @file    mculib_timer.h
* @author  
* @version V1.0.0
* @date    
* @brief  This file declares the TIMER interface driver function.
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _MCULIB_TIMER_H_
#define _MCULIB_TIMER_H_

// TIMER_BASE_10MS
#define SYS_TIMEOUT_50MS    (5)
#define SYS_TIMEOUT_100MS   (10)   
#define SYS_TIMEOUT_200MS   (20)
#define SYS_TIMEOUT_300MS   (30)

#define SYS_TIMEOUT_500MS   (50)
#define SYS_TIMEOUT_1SEC    (100)  
#define SYS_TIMEOUT_2SEC    (200)

extern UINT8 data counter_10ms; 

VOID mculib_timer_init(VOID);
VOID mculib_delay_us(UINT16 u16_count_us);
VOID mculib_delay_ms(UINT16 u16_count_ms);

#endif //_MCULIB_TIMER_H_