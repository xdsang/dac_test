/**
******************************************************************************
* @file    mculib_gpio.h
* @author  
* @version V1.0.0
* @date    
* @brief  This file declares the GPIO interface driver function.
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _MCULIB_GPIO_H_
#define _MCULIB_GPIO_H_

VOID mculib_gpio_init(VOID);

#endif //_MCULIB_GPIO_H_