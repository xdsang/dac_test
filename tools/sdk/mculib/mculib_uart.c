#include "mculib.h"

//helper declaration
static VOID _send_ascii(UINT8 u8_byte);
static VOID _print_dec(UINT16 u16_word);
static VOID _print_hex(UINT16 u16_word);
static VOID _print_string(UINT8 *u8_string);


// 固定波特率115200
void mculib_uart_init(void)
{
    S0CON = 0X40;  // uart mode 1 ,关闭接收功能
    PCON = 0;
    WDCON = 0X80;
    // 波特率计算公式 ：S0REL = 1024 -1500000/baud rate 
    S0RELH = 0x03;S0RELL = 0xF3; // 比特率115200    1024-13   =1011 = 0x3f3
    //S0RELH = 0x03;S0RELL = 0xe6; // 比特率57600    1024-26  = 998 = 0x03e6
    //S0RELH = 0x03;S0RELL = 0xd9; // 比特率38400    1024-39  = 985  = 0x03d9
    //S0RELH = 0x03;S0RELL = 0x64; // 比特率9600    1024-156  = 864 = 0x0364  
}

//add this to support printf funtion  <stdio.h>
char putchar (char c)   
{        
    S0BUF = c;      
    while(TI0==0);      
    TI0=0;             
    return 0;
}

static VOID _send_ascii(UINT8 u8_byte)
{
    if (u8_byte > 0x09)
    {
        u8_byte = (u8_byte - 10) + 0x41;    // 'A' -> 0x41
    }
    else
    {
        u8_byte = u8_byte + 0x30;           // '0' -> 0x30
    }
    putchar(u8_byte);   
}

static VOID _print_dec(UINT16 u16_word)
{
    UINT8 u8_buff[5];
    UINT8 u8_i = 0;
    BOOL  b_start = FALSE;

    while(u8_i < 5)
    {
        u8_buff[u8_i] =  u16_word % 10;
        u16_word /= 10;
        u8_i ++;
    }

    while(u8_i)
    {
        if (u8_buff[u8_i - 1] != 0)
        {
            b_start = TRUE;
        }

        if (b_start)
        {
            putchar(u8_buff[u8_i - 1] + 0x30);     
        }
        
        u8_i --;
    }

    if (!b_start)
    {
        putchar(0x30);
    }
}

static VOID _print_hex(UINT16 u16_word)
{
    UINT8 value;
    UINT8 i;
    
    i = (u16_word > 0xFF) ? 4 : 2;
    // High nibble    
    while (i --)
    {
        value = (u16_word >> (i * 4)) & 0x0F;
        _send_ascii(value);
    }
}

static VOID _print_string(UINT8 *u8_string)
{
    while (*u8_string)
    {
        putchar(*u8_string++);
    }
}

VOID mculib_uart_log(UINT8 *u8_string)
{
    _print_string(u8_string);
    _print_string("\r\n");
}

VOID mculib_uart_log1(UINT8 *u8_string, UINT16 u16_hex)
{
    _print_string(u8_string);
    _print_string("0x");
    _print_hex(u16_hex);
    _print_string("\r\n");
}

VOID mculib_uart_log2(UINT8 *u8_string, UINT16 u16_dec)
{
    _print_string(u8_string);
    _print_dec(u16_dec);
    _print_string("\r\n");
}
