/**
******************************************************************************
* @file    mculib_spi.h
* @author  
* @version V1.0.0
* @date    
* @brief   spi interface driver declaration
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _MCULIB_SPI_H_
#define _MCULIB_SPI_H_


#define CS_HIGH        (reg_gpio2_data = 1)
#define CS_LOW         (reg_gpio2_data = 0)

#define mculib_spi_cs_high()  CS_HIGH
#define mculib_spi_cs_low()   CS_LOW

VOID mculib_spi_init(VOID);
UINT8 mculib_spi_readwritebyte(UINT8 u8_TxData);

#endif //_MCULIB_SPI_H_