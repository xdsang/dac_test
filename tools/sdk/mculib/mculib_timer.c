/**
******************************************************************************
* @file    mculib_timer.c
* @author  
* @version V1.0.0
* @date    
* @brief   this file implements the timer interface function
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "ms2160top.h"
#include "user_setting.h"
#include "mculib.h"

extern UINT8 __DATA counter_10ms; 

// 10ms 定时
VOID mculib_timer_init(VOID)
{
    reg_TimerCntL_f023 = 0xd6;  //1ms 1d6 = 470 1024/48/*470 uS  = 10ms  
    reg_TimerCntH_f024 = 0x01;
    reg_TimerEn_f022 = 0;
    reg_Timer_int_f025 = 0;
    reg_TimerEn_f022 = 1; //开始计数
}

// 定时器中断，提供10ms 定时计数
VOID int_service_timer (VOID) interrupt 1     //timer0 interrupt
{
    if(user_get_usercode_status() & USERCODE_STATUSF_PATCH5_VALID)
    {
        #pragma asm
        patch_for_timer();
        #pragma endasm
    }
    else
    {
        counter_10ms ++;
        reg_Timer_int_f025 = 0;
    }
}

VOID mculib_delay_us(UINT16 u16_count_us)
{
    UINT16 __XDATA j;
    for(j=0;j<u16_count_us;j++)
    {
      _nop_();  _nop_();  _nop_();_nop_();  _nop_(); _nop_();  _nop_(); _nop_();
      _nop_();  _nop_();  _nop_();_nop_();  _nop_(); _nop_();  _nop_(); _nop_();
      _nop_();  _nop_();  _nop_();_nop_();  _nop_(); _nop_();  _nop_(); _nop_();                     
    } 
}

VOID mculib_delay_ms(UINT16 u16_count_ms)
{
    UINT16 __XDATA i,j;
    for(i=0;i<u16_count_ms;i++)
    {
      for(j=0;j<5000;j++);  
    }
}
