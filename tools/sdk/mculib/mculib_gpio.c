/**
******************************************************************************
* @file    mculib_gpio.c
* @author  
* @version V1.0.0
* @date    
* @brief   this file implements the gpio interface function
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "mculib.h"

VOID mculib_gpio_init(VOID)
{
    reg_gpio_oen |= 0xDC; // gpio2 = cs ,gpio3 =sck ,gpio4 = mosi
    reg_gpio_pu &= ~0x18; //gpio3, gpio4 pull up disable
    reg_gpio_data = 0;
}
