/**
******************************************************************************
* @file    mculib_i2c.h
* @author  
* @version V1.0.0
* @date    
* @brief   i2c interface driver declaration
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef _MCULIB_I2C_H_
#define _MCULIB_I2C_H_

//MCU I2C module typedef
typedef enum _I2C_SPEED_CHOOSE_INFOMATION
{
    I2C_SPEED_20K  = (UINT8)0,  //20Kbit/s
    I2C_SPEED_100K = (UINT8)1,  //100Kbit/s
    I2C_SPEED_400K = (UINT8)2,  //400Kbit/s
    I2C_SPEED_750K = (UINT8)3   //750Kbit/s
} I2C_SPEED_E;


VOID mculib_i2c_init(VOID);
VOID mculib_i2c_set_ddc(BOOL ddc);
VOID mculib_i2c_set_speed(UINT8 u8_i2c_speed); //enum to I2C_SPEED_E

// 16-bit index for macrosilicon chip
UINT8 mculib_i2c_read_16bidx8bval(UINT8 u8_address, UINT16 u16_index);
BOOL  mculib_i2c_write_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT8 u8_value);
VOID  mculib_i2c_burstread_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);
VOID  mculib_i2c_burstwrite_16bidx8bval(UINT8 u8_address, UINT16 u16_index, UINT16 u16_length, UINT8 *pu8_value);

// 8-bit index fro eeprom AT24C02..
UINT8 mculib_i2c_read_8bidx8bval(UINT8 u8_address, UINT8 u8_index);
BOOL  mculib_i2c_write_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_value);
VOID  mculib_i2c_burstread_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);
VOID  mculib_i2c_burstwrite_8bidx8bval(UINT8 u8_address, UINT8 u8_index, UINT8 u8_length, UINT8 *pu8_value);

#endif //_MCULIB_I2C_H_