/**
****************************************************************
* @file    ms2160_drv_vds_pllvm.h
* @author  Joshua
* @version V1.0.0
* @date    11-Nov-2014
* @brief   pllvm module driver declare
* @history    
*
* Copyright (c) 2009-2014, MacroSilicon Technology Co.,Ltd.
****************************************************************/
#ifndef  __MACROSILICON_MS2160_DRV_PLLVM_H__
#define  __MACROSILICON_MS2160_DRV_PLLVM_H__
     

#ifdef __cplusplus
extern "C" {
#endif


/***************************************************************
*  Function name:   ms2160drv_pllv_init
*  Description:     pllv init
*  Entry:           None
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_pllv_init(VOID);

/***************************************************************
*  Function name:   ms2160drv_pllv_uninit
*  Description:     pllv unint
*  Entry:           None
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_pllv_uninit(VOID);

/***************************************************************
*  Function name:   ms2160drv_pllv_config
*  Description:     pllv vclk config
*  Entry:           [in]u16Vclk   10kHz
*  Returned value:  None
*  Remark:
***************************************************************/
VOID ms2160drv_pllv_config(UINT16 u16Fclk);

    
#ifdef __cplusplus
}
#endif
    
    
#endif //__MACROSILICON_MS2160_DRV_VDS_PLLVM_H__
