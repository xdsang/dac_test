/**
******************************************************************************
* @file    ms2160_drv_pllvm.c
* @author  Weiwei Cheng
* @version V1.0.0
* @date    12-Jun-2018
* @brief   pllvm module driver source file
* @history    
*
* Copyright (c) 2009-2014, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_pllvm.h"

#ifndef MS2160_EXT_XTAL
#define MS2160_PLL_XTAL (2400ul)
#else
#define MS2160_PLL_XTAL MS2160_EXT_XTAL /10000    //10K
#endif

#ifndef PLLV_FNEN_ENABLE
#define PLLV_FNEN_ENABLE (1)
#endif


//register definitions
#define REG_PLLV_BASE                (0xf0a0)

#define REG_PLLV_PIXPLL_CTRL0        (REG_PLLV_BASE + 0x00)
#define REG_PLLV_PIXPLL_CTRL1        (REG_PLLV_BASE + 0x01)
#define REG_PLLV_PIXPLL_CTRL2        (REG_PLLV_BASE + 0x02)
#define REG_PLLV_PIXPLL_CTRL3        (REG_PLLV_BASE + 0x03)
#define REG_PLLV_PIXPLL_CTRL4        (REG_PLLV_BASE + 0x04)
#define REG_PLLV_PIXPLL_CTRL5        (REG_PLLV_BASE + 0x05)
#define REG_PLLV_PIXPLL_CTRL6        (REG_PLLV_BASE + 0x06)
#define REG_PLLV_PIXPLL_CTRL7        (REG_PLLV_BASE + 0x07)
#define REG_PLLV_PIXPLL_CTRL8        (REG_PLLV_BASE + 0x08)
#define REG_PLLV_PIXPLL_CTRL9        (REG_PLLV_BASE + 0x09)
#define REG_PLLV_PIXPLL_CTRLA        (REG_PLLV_BASE + 0x0a)
#define REG_PLLV_PIXPLL_CTRLB        (REG_PLLV_BASE + 0x0b)
#define REG_PLLV_PIXPLL_FREQ_P       (REG_PLLV_BASE + 0x0d)
#define REG_PLLV_PIXPLL_CFG_SEL      (REG_PLLV_BASE + 0x0f)
#define REG_PLLV_PIXPLL_CAL_TRIG     (REG_PLLV_BASE + 0x10)
#define REG_PLLV_PIXPLL_FREQM_CTRL   (REG_PLLV_BASE + 0x11)
#define REG_PLLV_PIXPLL_FREQ_RNG1    (REG_PLLV_BASE + 0x12)
#define REG_PLLV_PIXPLL_FREQ_RNG2    (REG_PLLV_BASE + 0x14)
#define REG_PLLV_PIXPLL_STS          (REG_PLLV_BASE + 0x16)
#define REG_PLLV_TMDS_FREQ           (REG_PLLV_BASE + 0x17)
#define REG_PLLV_CAL_FREQ            (REG_PLLV_BASE + 0x19)
#define REG_PLLV_PLL_CAL_RESULT0     (REG_PLLV_BASE + 0x1b)
#define REG_PLLV_PLL_CAL_RESULT1     (REG_PLLV_BASE + 0x1c)
#define REG_PLLV_PLL_CAL_RESULT2     (REG_PLLV_BASE + 0x1d)


// helper decalarations
static VOID _drv_pllv_software_mode_enable(BOOL bEnable);
static VOID _drv_pllv_power_enable(BOOL bEnable);
static VOID _drv_pllvm_fnen_enable(BOOL bEnable);
static UINT32 _drv_pllv_postdiv_config(UINT16 u16Fclk);
static UINT16 _drv_pllv_refclk_get(VOID);
static VOID _drv_pllv_fbdiv_config(UINT32 u32Fvco);


static VOID _drv_pllv_software_mode_enable(BOOL bEnable)
{
    HAL_ToggleBits(REG_PLLV_PIXPLL_CFG_SEL, MSRT_BITS1_0, bEnable);
}

static VOID _drv_pllv_power_enable(BOOL bEnable)
{
    HAL_ToggleBits(REG_PLLV_PIXPLL_CTRL0, MSRT_BITS2_0, !bEnable);
}

static VOID _drv_pllvm_fnen_enable(BOOL bEnable)
{
    HAL_ToggleBits(REG_PLLV_PIXPLL_CTRL7, MSRT_BIT0, bEnable);
}

UINT16 __CODE u16FvcoBand[4][2] = {   //500M ~ 1G
    { 750, 850 }, { 850, 1000 }, { 600, 750 }, { 500, 600 }
};
UINT8 __CODE u16FvcoBandNum = (sizeof(u16FvcoBand) / sizeof(u16FvcoBand[0]));

static UINT32 _drv_pllv_postdiv_config(UINT16 u16Fclk)
{
    UINT8 u8Div2;
    UINT8 u8Band;
    UINT32 u32Fvco;
    UINT8 u8PostDiv;
    if (u16Fclk < 500)
    {
        return 0;
    }
    u8Div2 = ((HAL_ReadByte(REG_PLLV_PIXPLL_CTRL7) & MSRT_BIT5) >> 5) + 1;
    for(u8Band = 0; u8Band < u16FvcoBandNum; u8Band++)
    {
        u32Fvco = (UINT32)u16FvcoBand[u8Band][1] * 100;   //1MHz -> 10kHz
        u8PostDiv = u32Fvco / u8Div2 / u16Fclk;
        u32Fvco = (UINT32)u16Fclk * u8PostDiv * u8Div2;
        if (u32Fvco >= (UINT32)u16FvcoBand[u8Band][0] * 100)
        {
            break;
        }
    }
    HAL_WriteByte(REG_PLLV_PIXPLL_CTRL5, u8PostDiv);
    return u32Fvco;
}

static UINT16 _drv_pllv_refclk_get(VOID)
{
    UINT16 u16RefClk;     /*10000hz*/
    UINT8 u8PreDiv;
    u8PreDiv = HAL_ReadByte(REG_PLLV_PIXPLL_CTRL3) & MSRT_BITS1_0;
    switch(u8PreDiv)
    {
    case 0:
        u16RefClk = MS2160_PLL_XTAL;
        break;
    case 1:
        u16RefClk = MS2160_PLL_XTAL / 2;
        break;
    case 2:
        u16RefClk = MS2160_PLL_XTAL / 4;
        break;
    case 3:
        u16RefClk = MS2160_PLL_XTAL / 8;
        break;
    }
    return u16RefClk;
}

static VOID _drv_pllv_fbdiv_config(UINT32 u32Fvco)
{
    UINT16 u16RefClk;
    UINT8 u8FbDiv;
    UINT32 u32FbFn;
    u16RefClk = _drv_pllv_refclk_get();
    u8FbDiv = u32Fvco / u16RefClk;
    HAL_WriteByte(REG_PLLV_PIXPLL_CTRL8, u8FbDiv);
    u32FbFn = u32Fvco - (UINT32)u16RefClk * u8FbDiv;
    u32FbFn = (u32FbFn << 20) / u16RefClk;
    HAL_WriteWord(REG_PLLV_PIXPLL_CTRL9, u32FbFn);
    HAL_WriteByte(REG_PLLV_PIXPLL_CTRLB, u32FbFn >> 16);
}

VOID ms2160drv_pllv_init(VOID)
{
    //init config
    HAL_SetBits(REG_PLLV_PIXPLL_CTRL7, MSRT_BIT5);
    HAL_SetBits(REG_PLLV_PIXPLL_CTRL1, MSRT_BIT1);
    HAL_SetBits(REG_PLLV_PIXPLL_CTRL1, MSRT_BIT2);

    _drv_pllv_software_mode_enable(FALSE);
    _drv_pllv_power_enable(TRUE);
    _drv_pllvm_fnen_enable(PLLV_FNEN_ENABLE);
    
    //misc PLL reset
    HAL_SetBits(0xF008, MSRT_BIT6); //0xF008[6]
    
    //PLL_LOCK_DET_EN = 1
    HAL_SetBits(REG_PLLV_PIXPLL_CTRL3, MSRT_BIT7);
}

VOID ms2160drv_pllv_uninit(VOID)
{
    _drv_pllv_power_enable(FALSE);
}

VOID ms2160drv_pllv_config(UINT16 u16Fclk)
{
    UINT32 u32Fvco;
    UINT8  i;
    BOOL  bFound = FALSE;  
    
    u32Fvco = _drv_pllv_postdiv_config(u16Fclk);
    _drv_pllv_fbdiv_config(u32Fvco);

    bFound = FALSE;
    for (i = 0; i < 10; i ++)
    {
        //trigger
        HAL_WriteByte(REG_PLLV_PIXPLL_CAL_TRIG, MSRT_BIT1);
        Delay_ms(1);
        //at least waiting 200us
        if (HAL_ReadByte(REG_PLLV_PLL_CAL_RESULT1) & MSRT_BIT7)
        {
            //TXPLL_FN_EN = 1
            HAL_SetBits(REG_PLLV_PIXPLL_CTRL7, MSRT_BIT0);
            bFound = TRUE;
            break;
        }
    }

    if (!bFound)
    {
        MS2160_LOG("PLLV unlock.");
    }
}

