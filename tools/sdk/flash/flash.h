/******************************************************************

*******************************************************************/
#ifndef __FLASH_H__
#define __FLASH_H__

#include "mculib.h"

//#define W25Q40    0XEF12
//#define W25Q80    0XEF13  
//#define W25Q16    0XEF14
//#define W25Q32    0XEF15
//#define W25Q64    0XEF16

extern UINT8 __DATA SPI_FLASH_TYPE;      //定义我们使用的flash芯片型号   
extern uint8 Flashbuffer[];
extern uint8 DiskCapacity[8];
               
 
//指令表
#define W25X_WriteEnable        0x06 
#define W25X_WriteDisable       0x04 
#define W25X_ReadStatusReg      0x05 
#define W25X_WriteStatusReg     0x01 
#define W25X_ReadData           0x03 
#define W25X_FastReadData       0x0B 
#define W25X_FastReadDual       0x3B 
#define W25X_PageProgram        0x02 
#define W25X_BlockErase         0xD8 
#define W25X_SectorErase        0x20 
#define W25X_ChipErase          0xC7 
#define W25X_PowerDown          0xB9 
#define W25X_ReleasePowerDown   0xAB 
#define W25X_DeviceID           0xAB 
#define W25X_ManufactDeviceID   0x90 
#define W25X_JedecDeviceID      0x9F 

void SPI_Flash_Init(void);
u8  SPI_Flash_ReadID(void);         //读取FLASH ID
u8   SPI_Flash_ReadSR(void);        //读取状态寄存器 
void SPI_FLASH_Write_SR(u8 sr);     //写状态寄存器
void SPI_FLASH_Write_Enable(void);  //写使能 
void SPI_FLASH_Write_Disable(void); //写保护
void SPI_Flash_Write_NoCheck(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);
void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead);   //读取flash
void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);//写入flash
void SPI_Flash_Erase_Chip(void);          //整片擦除
void SPI_Flash_Erase_Sector(u32 Dst_Addr);//扇区擦除
void SPI_Flash_Wait_Busy(void);           //等待空闲
void SPI_Flash_PowerDown(void);           //进入掉电模式
void SPI_Flash_WAKEUP(void);              //唤醒


void SPI_Flash_Read_MS2160(u32 ReadAddr,u16 NumByteToRead);   //ms2160 专用读取flash
void SPI_Flash_Write_MS2160(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)  ; // MS2160 专用写Flash ,与读数据相等的则不用写，避免重复写0

UINT8 SPI_Flash_Get_Status(void);
UINT8* SPI_Flash_Get_Opcode(void);


//扇区大小固定为0x200, 注意不是实际Flash 扇区大小，而是生命Udisk分块SIZE
#define UDISK_SECTOR_SIZE 0x200

#define FLASH_STATUSF_NOT_EXIST   0  // flash not exist
#define FLASH_STATUSF_EXIST_VALID 1  // flash exist and data valid

#define FLASH_STATUSF_PATCH0_VALID 0x04 // patch_before_chip_init valid
#define FLASH_STATUSF_PATCH1_VALID 0x08 // patch_after_chip_init valid
#define FLASH_STATUSF_PATCH2_VALID 0x10 // patch_in_main_loop valid
#define FLASH_STATUSF_PATCH3_VALID 0x20 // patch_for_usbtask valid
#define FLASH_STATUSF_PATCH4_VALID 0x40 // patch_for_usbcmd valid
#define FLASH_STATUSF_PATCH5_VALID 0x80 // patch_for_timer valid

#define FLASH_ADDR_EETYPE         (UINT16)0 // 0x5A/0xA5, 0x69/0x96
#define FLASH_ADDR_RAMCODE_INFO   (UINT16)2 // ramcode length, patch flag
#define FLASH_ADDR_RAMCODE        (UINT16)48 // ramcode
#define FLASH_ADDR_OPCODE         (UINT16)5 // option code

#endif