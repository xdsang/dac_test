#include "flash.h" 

#define PATCH_CODE_START_ADDR  (0xC830)
//u16 SPI_FLASH_TYPE=W25Q64;//默认就是25Q64
//SPI_FLASH_TYPE   可能的配置为0x12,0x13,0x14,0x15,0x16 ,如果为0 表示没有接Flash
extern UINT8 __DATA SPI_FLASH_TYPE;
uint8 Flashbuffer[512];
uint8 FlashOpcode[43];
uint8 DiskCapacity[8];
uint32 FLASH_MAX_ADDR  ;
//分别表示512K ,1 ，2,4,8 M
code uint8  Flashsize[5] = {1,2,4,8,16};

static UINT8 g_u8_flash_status = FLASH_STATUSF_NOT_EXIST;

//helper declaration
static VOID _check_data(VOID);
static VOID _load_patch_code(VOID);
static VOID _load_option_code(VOID);

//helper definition
static VOID _check_data(VOID)
{
    SPI_Flash_Read_MS2160(0, 2);

    if(0x3C == Flashbuffer[0] &&
       0xC3 == Flashbuffer[1])
    {
        g_u8_flash_status = FLASH_STATUSF_EXIST_VALID;
    }
}

static VOID _load_patch_code(VOID)
{
    UINT16 i = 0, j = 0;
    UINT16 u16_len = 0;
    UINT8 __XDATA *p_u8_ramcode_addr = NULL;

    SPI_Flash_Read_MS2160(FLASH_ADDR_RAMCODE_INFO, 3);
    
    u16_len = ((UINT16)Flashbuffer[0] << 8) | Flashbuffer[1];
    g_u8_flash_status |= Flashbuffer[2];
    
    p_u8_ramcode_addr = PATCH_CODE_START_ADDR;

    for(; i < u16_len / 512; i++)
    {
        SPI_Flash_Read_MS2160(FLASH_ADDR_RAMCODE + 512 * i, 512);

        for(; j < 512; j++)
        {
            *(p_u8_ramcode_addr + j + 512 * i) = Flashbuffer[j];
        }
    }

    SPI_Flash_Read_MS2160(FLASH_ADDR_RAMCODE + 512 * i, u16_len % 512);
        
    for(j = 0; j < u16_len % 512; j++)
    {
        *(p_u8_ramcode_addr + j + 512 * i) = Flashbuffer[j];
    }
}

static VOID _load_option_code(VOID)
{
    UINT8 i = 0;
    
    SPI_Flash_Read_MS2160(FLASH_ADDR_OPCODE, 43);

    for(; i < 43; i++)
    {
        FlashOpcode[i] = Flashbuffer[i];
    }
}


//4Kbytes为一个Sector
//16个扇区为1个Block
//W25Q64
//容量为8M字节,共有128个Block,2048个Sector 

//初始化SPI FLASH的IO口, 需要先初始化SPI1
void SPI_Flash_Init(void)
{
    mculib_spi_init();
    
    SPI_FLASH_TYPE=SPI_Flash_ReadID();//读取FLASH ID.

    if((SPI_FLASH_TYPE>0x16)||(SPI_FLASH_TYPE<0x12)) //无效的Flash或者没有接入Flash
    {
        SPI_FLASH_TYPE = 0;
        FLASH_MAX_ADDR = 0x7FFFFF; //8M
        g_u8_flash_status = FLASH_STATUSF_NOT_EXIST;
    }
    else
    {
        FLASH_MAX_ADDR = Flashsize[SPI_FLASH_TYPE-0x12];//0x12,0x13,0x14,0x15,0x16 分别对应512k ,1,2,4,8M  
        FLASH_MAX_ADDR = FLASH_MAX_ADDR*512*1024 - 0x2000 - 1; // 保留4K For rom code  8*1024 = 0x2000 32k 

        _check_data();

        if(FLASH_STATUSF_EXIST_VALID == g_u8_flash_status)
        {
            _load_patch_code();
            _load_option_code();
        }
    }
    DiskCapacity[0] =  (FLASH_MAX_ADDR/UDISK_SECTOR_SIZE)>>24;
    DiskCapacity[1] =  (FLASH_MAX_ADDR/UDISK_SECTOR_SIZE)>>16;
    DiskCapacity[2] =  (FLASH_MAX_ADDR/UDISK_SECTOR_SIZE)>>8;
    DiskCapacity[3] =   FLASH_MAX_ADDR/UDISK_SECTOR_SIZE;
//  DiskCapacity[4] =  (UDISK_SECTOR_SIZE>>24);
//  DiskCapacity[5] =  (UDISK_SECTOR_SIZE>>16);
    DiskCapacity[4] =  0;
    DiskCapacity[5] =  0;
    DiskCapacity[6] =  (UDISK_SECTOR_SIZE>>8);
    DiskCapacity[7] =   UDISK_SECTOR_SIZE;
}  

//读取SPI_FLASH的状态寄存器
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
u8 SPI_Flash_ReadSR(void)   
{  
    u8 byte=0;   
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_ReadStatusReg);    //发送读取状态寄存器命令    
    byte=mculib_spi_readwritebyte(0Xff);             //读取一个字节  
    mculib_spi_cs_high();                            //取消片选     
    return byte;   
} 
//写SPI_FLASH状态寄存器
//只有SPR,TB,BP2,BP1,BP0(bit 7,5,4,3,2)可以写!!!
void SPI_FLASH_Write_SR(u8 sr)   
{   
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_WriteStatusReg);   //发送写取状态寄存器命令    
    mculib_spi_readwritebyte(sr);               //写入一个字节  
    mculib_spi_cs_high();                            //取消片选            
}
   
//SPI_FLASH写使能   
//将WEL置位   
void SPI_FLASH_Write_Enable(void)   
{
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_WriteEnable);      //发送写使能  
    mculib_spi_cs_high();                            //取消片选            
}
 
//SPI_FLASH写禁止   
//将WEL清零  
void SPI_FLASH_Write_Disable(void)   
{  
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_WriteDisable);     //发送写禁止指令    
    mculib_spi_cs_high();                            //取消片选            
}
                
//读取芯片ID W25X16的ID:0XEF14
u8 SPI_Flash_ReadID(void)
{
    u8 Temp = 0; 
    mculib_spi_cs_low();                 
    mculib_spi_readwritebyte(0x90);//发送读取ID命令        
    mculib_spi_readwritebyte(0x00);        
    mculib_spi_readwritebyte(0x00);        
    mculib_spi_readwritebyte(0x00);                       
//  Temp|=mculib_spi_readwritebyte(0xFF)<<8;  
//  Temp|=mculib_spi_readwritebyte(0xFF);   
    mculib_spi_readwritebyte(0xFF);  
    Temp = mculib_spi_readwritebyte(0xFF); 
    mculib_spi_cs_high();                            
    return Temp;
}
            
//读取SPI FLASH  
//在指定地址开始读取指定长度的数据
//pBuffer:数据存储区
//ReadAddr:开始读取的地址(24bit)
//NumByteToRead:要读取的字节数(最大65535)
void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead)   
{ 
//  u16 i;                                              
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_ReadData);         //发送读取命令   
//    mculib_spi_readwritebyte((u8)((ReadAddr)>>16));  //发送24bit地址    
//    mculib_spi_readwritebyte((u8)((ReadAddr)>>8));   
//    mculib_spi_readwritebyte((u8)ReadAddr);  
    /* Send ReadAddr high nibble address byte to read from */
    mculib_spi_readwritebyte((ReadAddr & 0xFF0000) >> 16);
    /* Send ReadAddr medium nibble address byte to read from */
    mculib_spi_readwritebyte((ReadAddr& 0xFF00) >> 8);
    /* Send ReadAddr low nibble address byte to read from */
    mculib_spi_readwritebyte(ReadAddr & 0xFF); 
    while (NumByteToRead--) /* while there is data to be read */
    {
        /* Read a byte from the FLASH */
        *pBuffer = mculib_spi_readwritebyte(0xFF);
        /* Point to the next location where the byte read will be saved */
        pBuffer++;
    }
//    for(i=0;i<NumByteToRead;i++)
//  { 
//        pBuffer[i]=mculib_spi_readwritebyte(0XFF);   //循环读数  
//    }
    mculib_spi_cs_high();                            //取消片选            
}  

void SPI_Flash_Read_MS2160(u32 ReadAddr,u16 NumByteToRead)
{   
    u16 i;                                       
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_ReadData);         //发送读取命令   
    /* Send ReadAddr high nibble address byte to read from */
    mculib_spi_readwritebyte((ReadAddr & 0xFF0000) >> 16);
    /* Send ReadAddr medium nibble address byte to read from */
    mculib_spi_readwritebyte((ReadAddr& 0xFF00) >> 8);
    /* Send ReadAddr low nibble address byte to read from */
    mculib_spi_readwritebyte(ReadAddr & 0xFF); 
    for(i=0;i<NumByteToRead;i++)
    { 
        Flashbuffer[i]=mculib_spi_readwritebyte(0XFF);   //循环读数  
    }
    mculib_spi_cs_high();                            //取消片选            
}  

//SPI在一页(0~65535)内写入少于256个字节的数据
//在指定地址开始写入最大256字节的数据
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!   
void SPI_Flash_Write_Page(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)
{
    u16 i;  
    SPI_FLASH_Write_Enable();                  //SET WEL 
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_PageProgram);      //发送写页命令   
//    mculib_spi_readwritebyte((u8)((WriteAddr)>>16)); //发送24bit地址    
//    mculib_spi_readwritebyte((u8)((WriteAddr)>>8));   
//    mculib_spi_readwritebyte((u8)WriteAddr);   
    mculib_spi_readwritebyte((WriteAddr & 0xFF0000) >> 16);
    /* Send WriteAddr medium nibble address byte to write to */
    mculib_spi_readwritebyte((WriteAddr & 0xFF00) >> 8);
    /* Send WriteAddr low nibble address byte to write to */
    mculib_spi_readwritebyte(WriteAddr & 0xFF);
    for(i=0;i<NumByteToWrite;i++)mculib_spi_readwritebyte(pBuffer[i]);//循环写数  
    mculib_spi_cs_high();                            //取消片选 
    SPI_Flash_Wait_Busy();                     //等待写入结束
} 



//无检验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//具有自动换页功能 
//在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//CHECK OK
void SPI_Flash_Write_NoCheck(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
{                    
    u16 pageremain;    
    pageremain=256-WriteAddr%256; //单页剩余的字节数                
    if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//不大于256个字节
    while(1)
    {      
        SPI_Flash_Write_Page(pBuffer,WriteAddr,pageremain);
        if(NumByteToWrite==pageremain)break;//写入结束了
        else //NumByteToWrite>pageremain
        {
            pBuffer+=pageremain;
            WriteAddr+=pageremain;  

            NumByteToWrite-=pageremain;           //减去已经写入了的字节数
            if(NumByteToWrite>256)pageremain=256; //一次可以写入256个字节
            else pageremain=NumByteToWrite;       //不够256个字节了
        }
    };      
}

 
//写SPI FLASH  
//在指定地址开始写入指定长度的数据
//该函数带擦除操作!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)             
//u8 SPI_FLASH_BUF[4096];
//void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
//{ 
//    u32 secpos;
//    u16 secoff;
//    u16 secremain;     
//    u16 i;    
//
//    secpos=WriteAddr/4096;//扇区地址  
//    secoff=WriteAddr%4096;//在扇区内的偏移
//    secremain=4096-secoff;//扇区剩余空间大小   
//
//    if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
//    while(1) 
//    {   
//        SPI_Flash_Read(SPI_FLASH_BUF,secpos*4096,4096);//读出整个扇区的内容
//        for(i=0;i<secremain;i++)//校验数据
//        {
//            if(SPI_FLASH_BUF[secoff+i]!=0XFF)break;//需要擦除     
//        }
//        if(i<secremain)//需要擦除
//        {
//            SPI_Flash_Erase_Sector(secpos);//擦除这个扇区
//            for(i=0;i<secremain;i++)       //复制
//            {
//                SPI_FLASH_BUF[i+secoff]=pBuffer[i];   
//            }
//            SPI_Flash_Write_NoCheck(SPI_FLASH_BUF,secpos*4096,4096);//写入整个扇区  
//
//        }else SPI_Flash_Write_NoCheck(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间.                  
//        if(NumByteToWrite==secremain)break;//写入结束了
//        else//写入未结束
//        {
//            secpos++;//扇区地址增1
//            secoff=0;//偏移位置为0   
//
//            pBuffer+=secremain;  //指针偏移
//            WriteAddr+=secremain;//写地址偏移      
//            NumByteToWrite-=secremain;              //字节数递减
//            if(NumByteToWrite>4096)secremain=4096;  //下一个扇区还是写不完
//            else secremain=NumByteToWrite;          //下一个扇区可以写完了
//        }    
//    };       
//}
//
//void SPI_Flash_Write_MS2160(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
//{ 
//    u32 secpos;
//    u16 secoff;
//    u16 secremain;     
//    u16 i;    
//
//
//    SPI_Flash_Read_MS2160(WriteAddr,NumByteToWrite);
//    for(i=0;i<NumByteToWrite;i++) // 如果需要写的数据与读的数据相等，则直接退出不用写了
//    {
//       if(Flashbuffer[i] != pBuffer[i])   // 实际MS2100 写不到100，最多64
//       {
//          i = 100;
//          break;
//       }
//    }
//    if(i!=100)    // 未检测到不相等的数据，不需要写
//    {
//       return;
//    }
//
//    secpos=WriteAddr/4096;//扇区地址  
//    secoff=WriteAddr%4096;//在扇区内的偏移
//    secremain=4096-secoff;//扇区剩余空间大小   
//
//    if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
//    while(1) 
//    {   
//        SPI_Flash_Read(SPI_FLASH_BUF,secpos*4096,4096);//读出整个扇区的内容
//        for(i=0;i<secremain;i++)//校验数据
//        {
//            if(SPI_FLASH_BUF[secoff+i]!=0XFF)break;//需要擦除     
//        }
//        if(i<secremain)//需要擦除
//        {
//            SPI_Flash_Erase_Sector(secpos);//擦除这个扇区
//            for(i=0;i<secremain;i++)       //复制
//            {
//                SPI_FLASH_BUF[i+secoff]=pBuffer[i];   
//            }
//            SPI_Flash_Write_NoCheck(SPI_FLASH_BUF,secpos*4096,4096);//写入整个扇区  
//
//        }else SPI_Flash_Write_NoCheck(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间.                  
//        if(NumByteToWrite==secremain)break;//写入结束了
//        else//写入未结束
//        {
//            secpos++;//扇区地址增1
//            secoff=0;//偏移位置为0   
//
//            pBuffer+=secremain;  //指针偏移
//            WriteAddr+=secremain;//写地址偏移      
//            NumByteToWrite-=secremain;              //字节数递减
//            if(NumByteToWrite>4096)secremain=4096;  //下一个扇区还是写不完
//            else secremain=NumByteToWrite;          //下一个扇区可以写完了
//        }    
//    };       
//}


//擦除整个芯片
//整片擦除时间:
//W25X16:25s 
//W25X32:40s 
//W25X64:40s 
//等待时间超长...
void SPI_Flash_Erase_Chip(void)   
{                                             
    SPI_FLASH_Write_Enable();                  //SET WEL 
    SPI_Flash_Wait_Busy();   
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_ChipErase);        //发送片擦除命令  
    mculib_spi_cs_high();                            //取消片选            
    SPI_Flash_Wait_Busy();                     //等待芯片擦除结束
}   
//擦除一个扇区
//Dst_Addr:扇区地址 0~2047 for W25Q64
//擦除一个山区的最少时间:150ms
void SPI_Flash_Erase_Sector(u32 Dst_Addr)   
{   
    Dst_Addr*=4096;
    SPI_FLASH_Write_Enable();                  //SET WEL     
    SPI_Flash_Wait_Busy();   
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_SectorErase);      //发送扇区擦除指令 
    mculib_spi_readwritebyte((u8)((Dst_Addr)>>16));  //发送24bit地址    
    mculib_spi_readwritebyte((u8)((Dst_Addr)>>8));   
    mculib_spi_readwritebyte((u8)Dst_Addr);  
    mculib_spi_cs_high();                            //取消片选            
    SPI_Flash_Wait_Busy();                     //等待擦除完成
}
  
//等待空闲
void SPI_Flash_Wait_Busy(void)   
{   
    while ((SPI_Flash_ReadSR()&0x01)==0x01);   // 等待BUSY位清空
}
  
//进入掉电模式
void SPI_Flash_PowerDown(void)   
{ 
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_PowerDown);        //发送掉电命令  
    mculib_spi_cs_high();                            //取消片选            
    mculib_delay_us(3);                               //等待TPD  
}   
//唤醒
void SPI_Flash_WAKEUP(void)   
{  
    mculib_spi_cs_low();                            //使能器件   
    mculib_spi_readwritebyte(W25X_ReleasePowerDown); //  send W25X_PowerDown command 0xAB    
    mculib_spi_cs_high();                            //取消片选            
    mculib_delay_us(3);                               //等待TRES1
}   

UINT8 SPI_Flash_Get_Status(void)
{
    return g_u8_flash_status;
}

UINT8* SPI_Flash_Get_Opcode(void)
{
    return FlashOpcode;
}





















