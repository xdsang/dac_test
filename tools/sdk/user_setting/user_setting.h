/*******************************************************************
* Copyright (c) 2009-2012, MacroSilicon Technology Co.,Ltd.
* File name: mculib_user_setting.h
* File abstract: This file defines the user setting data process interface middle ware function.
*                    Use eeprom driver.
* The current version: V1.0
* The modifier: Fengjiao Wang
* Completion date: 2014-6-9
*
* Replace version: 
* The author: 
* Completion date: 
*******************************************************************/
#ifndef _USER_SETTING_H
#define _USER_SETTING_H

#include "common.h"
#include "flash.h" 
#include "eeprom.h"

#define USERCODE_STATUSF_NOT_EXIST   0  // eeprom or flash not exist
#define USERCODE_STATUSF_EXIST_VALID 1  // eeprom or flash exist and data valid

#define USERCODE_STATUSF_PATCH0_VALID 0x04 // patch_before_chip_init valid
#define USERCODE_STATUSF_PATCH1_VALID 0x08 // patch_after_chip_init valid
#define USERCODE_STATUSF_PATCH2_VALID 0x10 // patch_in_main_loop valid
#define USERCODE_STATUSF_PATCH3_VALID 0x20 // patch_for_usbtask valid
#define USERCODE_STATUSF_PATCH4_VALID 0x40 // patch_for_usbcmd valid
#define USERCODE_STATUSF_PATCH5_VALID 0x80 // patch_for_timer valid

//ramcode entry
//patch before ms2160 init.
#define patch_before_chip_init()   LCALL 0C830H
//patch after ms2160 init.
#define patch_after_chip_init()    LCALL 0C840H
//patch in the end of main loop
#define patch_in_main_loop()       LCALL 0C850H
//patch in int_service_isr0_Rom
#define patch_for_usbtask()        LCALL 0C860H
//patch in int_endpoint0_service
#define patch_for_usbcmd()         LCALL 0C870H
//patch in int_service_timer
#define patch_for_timer()          LCALL 0C880H

//option code declaration
extern UINT8 __XDATA g_u8_usbdisken;
extern UINT16 __XDATA g_u16_usbvid;
extern UINT16 __XDATA g_u16_usbpid;
extern UINT8  __XDATA g_u8_mem_size;
extern UINT8  __XDATA g_u8_default_video_mode;
extern BYTE __XDATA g_u8_patchStringMINFO_rom[16]; //manufacture info. from external user code
extern BYTE __XDATA g_u8_patchStringPINFO_rom[16]; //product info. from external user code

UINT8 user_init(VOID);

UINT8 *user_get_option_code(VOID);

UINT8 user_get_usercode_status(VOID);


#endif
