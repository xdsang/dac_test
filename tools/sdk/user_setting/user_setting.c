#include "ms2160top.h"
#include "user_setting.h"


//option code definition
UINT8  __XDATA g_u8_usbdisken _at_ 0xC805;
UINT16 __XDATA g_u16_usbvid   _at_ 0xC806;
UINT16 __XDATA g_u16_usbpid   _at_ 0xC808;
UINT8  __XDATA g_u8_mem_size  _at_ 0xC80A;
UINT8  __XDATA g_u8_default_video_mode _at_ 0xC80B;
UINT8  __XDATA g_u8_patchStringMINFO_rom[16] _at_ 0xC810; //manufacture info. from external user code
UINT8  __XDATA g_u8_patchStringPINFO_rom[16] _at_ 0xC820; //product info. from external user code
    
//
static UINT8 g_u8_usercode_status = USERCODE_STATUSF_NOT_EXIST;
static UINT8 *p_u8_option_code = NULL;

//helper delcaration
static VOID _option_code_init(VOID);
static VOID _parse_option_code(VOID);

//helper definition
static VOID _option_code_init(VOID)
{
    g_u8_usbdisken = 0;
    g_u16_usbpid = 0xFFFF;
    g_u16_usbvid = 0xFFFF;
    g_u8_patchStringMINFO_rom[0] = 0xFF;
    g_u8_patchStringPINFO_rom[0] = 0xFF;
}

static VOID _parse_option_code(VOID)
{
    UINT8 i = 0;
    g_u8_usbdisken = p_u8_option_code[0];
    g_u16_usbvid = p_u8_option_code[1];
    g_u16_usbvid = g_u16_usbvid << 8 | p_u8_option_code[2];
    g_u16_usbpid = p_u8_option_code[3];
    g_u16_usbpid = g_u16_usbpid << 8 | p_u8_option_code[4];
    g_u8_mem_size = p_u8_option_code[5];
    g_u8_default_video_mode = p_u8_option_code[6];

    for(; i < 16; i++)
    {
        g_u8_patchStringMINFO_rom[i] = p_u8_option_code[11 + i];
        g_u8_patchStringPINFO_rom[i] = p_u8_option_code[27 + i];
    }
}


//export
UINT8 user_init(VOID)
{
    UINT8 u8_status = USERCODE_STATUSF_NOT_EXIST;

    //global variable init.
    _option_code_init();
    
    //enable ms2160 spi master mode
    reg_Padctrl5_f016 &= ~0x04;
    
    SPI_Flash_Init();

    if(SPI_FLASH_TYPE != 0) //SPI flash exists
    {
        u8_status = SPI_Flash_Get_Status();
        
        if(u8_status & USERCODE_STATUSF_EXIST_VALID)
        {
            p_u8_option_code = SPI_Flash_Get_Opcode();
        }
    }
    else
    {
        //disable ms2160 spi master mode(use spi io as gpio)
        reg_Padctrl5_f016 |= 0x04;
        
        eeprom_init();
        u8_status = eeprom_get_status();
        if(u8_status & USERCODE_STATUSF_EXIST_VALID)
        {
            p_u8_option_code = eeprom_get_opcode();
        }
    }

    if(u8_status & USERCODE_STATUSF_EXIST_VALID)
    {
        _parse_option_code();
    }

    g_u8_usercode_status = u8_status;
    
    return u8_status;
}

UINT8 *user_get_option_code(VOID)
{
    return p_u8_option_code;
}

UINT8 user_get_usercode_status(VOID)
{
    return g_u8_usercode_status;
}
