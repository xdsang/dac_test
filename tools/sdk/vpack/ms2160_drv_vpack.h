/**
******************************************************************************
* @file    ms2160_drv_vpack.h
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   vpack module driver declare
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#ifndef __MACROSILICON_MS2160_DRV_VPACK_H__
#define __MACROSILICON_MS2160_DRV_VPACK_H__

// driver interface for macro enum structure declarations


#ifdef __cplusplus
extern "C" {
#endif

VOID ms2160drv_vpack_init(VOID);
VOID ms2160drv_vpack_frame_switch(BOOL b_frame_index);
VOID ms2160drv_vpack_set_video_in_info(VIDEO_IN_INFO_T *p_st_in_info);
VOID ms2160drv_vpack_set_video_transfer_mode(VIDEO_TRANSFER_MODE_T *p_st_trans_mode);
VOID ms2160drv_vpack_video_transfer_start(VOID);
VOID ms2160drv_vpack_video_transfer_stop(VOID);

#ifdef __cplusplus
}
#endif

#endif  //__MACROSILICON_MS2160_DRV_VPACK_H__
