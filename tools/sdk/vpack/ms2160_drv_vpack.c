/**
******************************************************************************
* @file    ms2160_drv_vpack.c
* @author  
* @version V1.0.0
* @date    24-August-2017
* @brief   vpack module driver source file
* @history    
*
* Copyright (c) 2009-, MacroSilicon Technology Co.,Ltd.
******************************************************************************/
#include "common.h"
#include "ms2160_drv_vpack.h"

#define  REG_VPACK_BASE                (0xF200)

#define  REG_VPACK_CLEAR               (REG_VPACK_BASE + 0x0000)
#define  REG_VPACK_AUTO_CFT            (REG_VPACK_BASE + 0x0001)
#define  REG_VPACK_TRANSFER            (REG_VPACK_BASE + 0x0002)
#define  REG_VPACK_SWITCH              (REG_VPACK_BASE + 0x0003)
#define  REG_VPACK_FRM_HW_L            (REG_VPACK_BASE + 0x0004)
#define  REG_VPACK_FRM_VH_L            (REG_VPACK_BASE + 0x0006)
#define  REG_VPACK_BLK_ST_H_L          (REG_VPACK_BASE + 0x0008)
#define  REG_VPACK_BLK_ST_V_L          (REG_VPACK_BASE + 0x000A)
#define  REG_VPACK_BLK_HW_L            (REG_VPACK_BASE + 0x000C)
#define  REG_VPACK_BLK_VH_L            (REG_VPACK_BASE + 0x000E)
#define  REG_VPACK_FETCH_L             (REG_VPACK_BASE + 0x0010)
#define  REG_VPACK_CSC                 (REG_VPACK_BASE + 0x0012)
#define  REG_VPACK_BLK_DEF             (REG_VPACK_BASE + 0x0013)
#define  REG_VPACK_MISC                (REG_VPACK_BASE + 0x0014)
#define  REG_VPACK_STATUS              (REG_VPACK_BASE + 0x0015)
#define  REG_VPACK_DATA_NUM_RD_L       (REG_VPACK_BASE + 0x0016)


// macro enum structure definitions
#define  VPACK_FETCH_NUM               (0x200)
#define  MEM_HSIZE_PIXEL_BASED         (1) //= 0 for using double word

// variables definitions

// helper decalarations
static VOID _drv_vpack_set_video_color_space(UINT8 u8_in_color_space, UINT8 u8_in_mem_color_space);
static VOID _drv_vpack_set_frame_size(VIDEOSIZE_T *p_st_frame_size);
static VOID _drv_vpack_set_block_start_pos(UINT16 u16_st_h, UINT16 u16_st_v);
static VOID _drv_vpack_set_block_num(VIDEO_BLOCK_NUM_T *p_st_blk_num);
static VOID _drv_vpack_set_block_size(VIDEOSIZE_T *p_st_blk_size);
static VOID _drv_vpack_transfer_active(BOOL b_active);
static VOID _drv_vpack_fetchnum_auto_en(BOOL b_enable);
static VOID _drv_vpack_start_ctrl_en(BOOL b_enable);
static VOID _drv_vpack_blkinfo_auto_en(BOOL b_enable);
static VOID _drv_vpack_switchinfo_auto_en(BOOL b_enable);
static VOID _drv_vpack_block_size_sel(BOOL b_manu_blk_size);
static VOID _drv_vpack_fixed_block_size_sel(BOOL b_hv_size);
static VOID _drv_vpack_block_method_auto_en(BOOL b_enable);

// helper definitions
static VOID _drv_vpack_set_video_color_space(UINT8 u8_in_color_space, UINT8 u8_in_mem_color_space)
{
    if(COLOR_SPACE_YUV422 == u8_in_mem_color_space)
    {
        switch(u8_in_color_space)
        {
        case COLOR_SPACE_RGB888:
            HAL_SetBits(REG_VPACK_CSC, MSRT_BIT7);
            HAL_ClrBits(REG_VPACK_CSC, MSRT_BIT6);
            break;
        case COLOR_SPACE_YUV444:
            HAL_ClrBits(REG_VPACK_CSC, MSRT_BIT7);
            HAL_SetBits(REG_VPACK_CSC, MSRT_BIT6);
            break;
        default:
            HAL_ClrBits(REG_VPACK_CSC, MSRT_BITS7_6);
            break;
        }
    }
    else
    {
        HAL_ClrBits(REG_VPACK_CSC, MSRT_BITS7_6);
    }

    switch(u8_in_mem_color_space)
    {
    case COLOR_SPACE_RGB565:
        u8_in_mem_color_space = 4;
        break;
    case COLOR_SPACE_YUV422:
        u8_in_mem_color_space = 2;        
        break;
    case COLOR_SPACE_YUV444:
        u8_in_mem_color_space = 1;        
        break;
    default:
        u8_in_mem_color_space = 0;        
        break;
    }

    HAL_ModBits(REG_VPACK_CSC, MSRT_BITS5_3, u8_in_mem_color_space << 3);
}

static VOID _drv_vpack_set_frame_size(VIDEOSIZE_T *p_st_frame_size)
{
    HAL_WriteWord(REG_VPACK_FRM_HW_L, p_st_frame_size->u16_h);
    HAL_WriteWord(REG_VPACK_FRM_VH_L, p_st_frame_size->u16_v);
}

static VOID _drv_vpack_set_block_start_pos(UINT16 u16_st_h, UINT16 u16_st_v)
{
    HAL_WriteWord(REG_VPACK_BLK_ST_H_L, u16_st_h);
    HAL_WriteWord(REG_VPACK_BLK_ST_V_L, u16_st_v);
}

static VOID _drv_vpack_set_block_num(VIDEO_BLOCK_NUM_T *p_st_blk_num)
{
    if (p_st_blk_num->u8_column == 1)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x00);
    }
    else if (p_st_blk_num->u8_column == 2)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x01);
    }
    else if (p_st_blk_num->u8_column == 4)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x02);
    }
    else if (p_st_blk_num->u8_column == 8)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x03);
    }
    else if (p_st_blk_num->u8_column == 16)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x04);
    }
    else if (p_st_blk_num->u8_column == 32)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x05);
    }
    else if (p_st_blk_num->u8_column == 64)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x06);
    }
    else
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS2_0, 0x03);
    }

    if (p_st_blk_num->u8_row == 1)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x00);
    }
    else if (p_st_blk_num->u8_row == 2)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x10);
    }
    else if (p_st_blk_num->u8_row == 4)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x20);
    }
    else if (p_st_blk_num->u8_row == 8)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x30);
    }
    else if (p_st_blk_num->u8_row == 16)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x40);
    }
    else if (p_st_blk_num->u8_row == 32)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x50);
    }
    else if (p_st_blk_num->u8_row == 64)
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x60);
    }
    else
    {
        HAL_ModBits(REG_VPACK_BLK_DEF, MSRT_BITS6_4, 0x30);
    }
    
}

static VOID _drv_vpack_set_block_size(VIDEOSIZE_T *p_st_blk_size)
{
    HAL_WriteWord(REG_VPACK_BLK_HW_L, p_st_blk_size->u16_h);
    HAL_WriteWord(REG_VPACK_BLK_VH_L, p_st_blk_size->u16_v);
}

static VOID _drv_vpack_transfer_active(BOOL b_active)
{
    HAL_ToggleBits(REG_VPACK_TRANSFER, MSRT_BIT0, b_active);
}

static VOID _drv_vpack_fetchnum_auto_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT3, b_enable);
}

static VOID _drv_vpack_start_ctrl_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT6, b_enable);
}

static VOID _drv_vpack_blkinfo_auto_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT1, b_enable);
}

static VOID _drv_vpack_switchinfo_auto_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT2, b_enable);
}

static VOID _drv_vpack_block_size_sel(BOOL b_manu_blk_size)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT5, b_manu_blk_size);
}

static VOID _drv_vpack_fixed_block_size_sel(BOOL b_hv_size)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT0, b_hv_size);
}

static VOID _drv_vpack_block_method_auto_en(BOOL b_enable)
{
    HAL_ToggleBits(REG_VPACK_AUTO_CFT, MSRT_BIT4, b_enable);
}


//exported
VOID ms2160drv_vpack_init(VOID)
{
    //vpack fetch num. set
    HAL_WriteWord(REG_VPACK_FETCH_L, (UINT16)VPACK_FETCH_NUM);

    //vpack_hsize_sel_mem
    HAL_ToggleBits(REG_VPACK_MISC, MSRT_BIT0, !MEM_HSIZE_PIXEL_BASED);
}

VOID ms2160drv_vpack_frame_switch(BOOL b_frame_index)
{
    HAL_ToggleBits(REG_VPACK_SWITCH, MSRT_BIT0, b_frame_index);
}

VOID ms2160drv_vpack_set_video_in_info(VIDEO_IN_INFO_T *p_st_in_info)
{
    VIDEOSIZE_T st_video_in_size;
    
    //frame size    
    switch(p_st_in_info->u8_in_mem_color_space)
    {
    case COLOR_SPACE_RGB888:
    case COLOR_SPACE_YUV444:
        st_video_in_size.u16_h = (p_st_in_info->st_in_video_size.u16_h + 3) / 4 * 4;
        break;
    default:  
        st_video_in_size.u16_h = (p_st_in_info->st_in_video_size.u16_h + 1) / 2 * 2;
        break;
    }
    st_video_in_size.u16_v = p_st_in_info->st_in_video_size.u16_v;
    _drv_vpack_set_frame_size(&st_video_in_size);

    //color space
    _drv_vpack_set_video_color_space(p_st_in_info->u8_in_color_space, p_st_in_info->u8_in_mem_color_space);
}

VOID ms2160drv_vpack_set_video_transfer_mode(VIDEO_TRANSFER_MODE_T *p_st_trans_mode)
{
    switch(p_st_trans_mode->u8_trans_mode)
    {
    case TRANSFER_MODE_FIXED_BLOCK_NUM:        
        //2.5.4.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.4.2) set vpack_start_cntrl_en to 0
        _drv_vpack_start_ctrl_en(FALSE);
        //2.5.4.3) set vpack_blkinfo_auto to 1, set vpack_switchinfo_auto to 1
        _drv_vpack_blkinfo_auto_en(TRUE);
        _drv_vpack_switchinfo_auto_en(TRUE);        
        //2.5.4.4) set vpack_manu_block_size to 0
        _drv_vpack_block_size_sel(FALSE);
        _drv_vpack_fixed_block_size_sel(FALSE);
        _drv_vpack_set_block_num(&p_st_trans_mode->st_blk_info.u_blk_def_mode.st_blk_num);
        break;
    case TRANSFER_MODE_FIXED_BLOCK_SIZE:        
        //2.5.4.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.4.2) set vpack_start_cntrl_en to 0
        _drv_vpack_start_ctrl_en(FALSE);
        //2.5.4.3) set vpack_blkinfo_auto to 1, set vpack_switchinfo_auto to 1
        _drv_vpack_blkinfo_auto_en(TRUE);
        _drv_vpack_switchinfo_auto_en(TRUE);        
        //2.5.4.4) set vpack_manu_block_size to 0
        _drv_vpack_block_size_sel(FALSE);
        _drv_vpack_fixed_block_size_sel(TRUE);
        _drv_vpack_set_block_size(&p_st_trans_mode->st_blk_info.u_blk_def_mode.st_blk_size);
        break;        
    case TRANSFER_MODE_MANUAL_BLOCK:
        //2.5.3.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.3.2) set vpack_start_cntrl_en to 0
        _drv_vpack_start_ctrl_en(FALSE);
        //2.5.3.3) set vpack_blkinfo_auto to 1, set vpack_switchinfo_auto to 1
        _drv_vpack_blkinfo_auto_en(TRUE);
        _drv_vpack_switchinfo_auto_en(TRUE);
        //2.5.3.4) set vpack_manu_block_size to 1
        _drv_vpack_block_size_sel(TRUE);
        break;        
    case TRANSFER_MODE_MEM_BYPS_FRAME:        
        //2.5.1.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.1.2) set vpack_start_cntrl_en to 1
        _drv_vpack_start_ctrl_en(TRUE);
        //2.5.1.3) set vpack_blkinfo_auto to 0, set vpack_switchinfo_auto to 0
        _drv_vpack_blkinfo_auto_en(FALSE);
        _drv_vpack_switchinfo_auto_en(FALSE);
        break;
    case TRANSFER_MODE_MEM_BYPS_MANUAL_BLOCK:        
        //2.5.3.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.1.2) set vpack_start_cntrl_en to 1
        _drv_vpack_start_ctrl_en(TRUE);
        //2.5.3.3) set vpack_blkinfo_auto to 1, set vpack_switchinfo_auto to 1
        _drv_vpack_blkinfo_auto_en(TRUE);
        _drv_vpack_switchinfo_auto_en(TRUE);
        //2.5.3.4) set vpack_manu_block_size to 1
        _drv_vpack_block_size_sel(TRUE);
        break;        
    default:
        //2.5.2.1) set vpack_fetchnum_auto to 1. 
        _drv_vpack_fetchnum_auto_en(TRUE);
        //2.5.2.2) set vpack_start_cntrl_en to 0
        _drv_vpack_start_ctrl_en(FALSE);
        //2.5.2.3) set vpack_blkinfo_auto to 0, set vpack_switchinfo_auto to 0
        _drv_vpack_blkinfo_auto_en(FALSE);
        _drv_vpack_switchinfo_auto_en(FALSE);
        break;
    }
    
}

VOID ms2160drv_vpack_video_transfer_start(VOID)
{
    //clear vpack status
    HAL_SetBits(REG_VPACK_CLEAR, MSRT_BIT0);
    HAL_ClrBits(REG_VPACK_CLEAR, MSRT_BIT0);

    //transfer activate
    _drv_vpack_transfer_active(TRUE);
}

VOID ms2160drv_vpack_video_transfer_stop(VOID)
{
    //transfer de-activate
    _drv_vpack_transfer_active(FALSE);
}

